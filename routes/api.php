<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//logincontroller
Route::post('/register','LoginController@register')->name('register');
Route::post('/checkuser','LoginController@checkuser')->name('checkuser');
Route::post('/adminforgotpassword_check','LoginController@adminforgotpassword_check')->name('adminforgotpassword_check');
Route::post('/adminresetpassword_check','LoginController@adminresetpassword_check')->name('adminresetpassword_check');
// mainapi
Route::get('/index','ApiHotelcontroller@index')->name('index');
Route::get('/demo','ApiHotelcontroller@demo')->name('demo');
Route::get('/get_current_location','ApiHotelcontroller@get_current_location')->name('get_current_location');
Route::get('/ios_get_current_location','ApiHotelcontroller@ios_get_current_location')->name('ios_get_current_location');
//hotel
//city search
Route::get('/getcities','ApiHotelcontroller@getcities')->name('getcities');
Route::get('/iosgetcities','ApiHotelcontroller@iosgetcities')->name('iosgetcities');
//hotel search
Route::post('/apigethotels','ApiHotelcontroller@apigethotels')->name('apigethotels');

Route::post('/ios_apigethotels','ApiHotelcontroller@ios_apigethotels')->name('ios_apigethotels');
Route::post('/hotelinfo','ApiHotelcontroller@hotel_info')->name('hotelinfo');

Route::post('/ios_hotel_info','ApiHotelcontroller@ios_hotel_info')->name('ios_hotel_info');

Route::post('/hotelbook','ApiHotelcontroller@hotelbook')->name('hotelbook');

Route::post('/hotelbooking','ApiHotelcontroller@hotelbooking')->name('hotelbooking');
Route::post('/apihotelinvoice','ApiHotelcontroller@apihotelinvoice')->name('apihotelinvoice');
Route::get('/apihotelinvoicedownload','ApiHotelcontroller@apihotelinvoicedownload')->name('apihotelinvoicedownload');
//flight api
Route::post('/apiflightsearch','ApiFlightController@apiflightsearch')->name('apiflightsearch');
Route::post('/apiflightbook','ApiFlightController@apiflightbook')->name('apiflightbook');
Route::post('/apiflight_bookview','ApiFlightController@apiflight_bookview')->name('apiflight_bookview');
Route::post('/newapiflight_bookview','ApiFlightController@newapiflight_bookview')->name('newapiflight_bookview');
Route::post('/apiflight_invoice','ApiFlightController@apiflight_invoice')->name('apiflight_invoice');

Route::get('/flightdownloadpdf','ApiFlightController@flightdownloadpdf')->name('flightdownloadpdf');

//login details
Route::post('/api_register','ApiFlightController@api_register')->name('api_register');
Route::post('/api_checkuser','ApiFlightController@api_checkuser')->name('api_checkuser');
Route::get('/api_mytrip','ApiFlightController@api_mytrip')->name('api_mytrip');