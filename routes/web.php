<?php
ini_set("display_errors", true);
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes fofr your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('pages.index');
// });

// Auth::routes();

// Route::get('/', function(){
//   return abort(404);
// });
Route::get('/','FlightController@index')->name('/');
Route::post('/','FlightController@index')->name('/');

Route::get('/results',function()
{
	return view('pages.results');
});
Route::get('/services',function()
{
	return view('pages.services');
});
Route::get('/about',function()
{
	return view('pages.about-us');
});
Route::get('/faq',function()
{
	return view('pages.faqs');
});
Route::get('/privacy-policy',function()
{
	return view('pages.privacy-policy');
});
Route::get('/refund-policy',function()
{
	return view('pages.refund-policy');
});
Route::get('/disclaimer-policy',function()
{
	return view('pages.disclaimer-policy');
});
Route::get('/hotel-results',function()
{
	return view('pages.hotel-results');
});
Route::get('/flight-book',function()
{
	return view('pages.flight-book');
});
Route::get('/flight-price',function()
{
	return view('pages.flight-price');
});
Route::get('/flight_invoicepdf',function()
{
	return view('pages.flight_invoicepdf');
});
Route::get('/flight_testbook',function()
{
	return view('pages.flight_testbook');
});
Route::get('/hotel-book',function()
{
	return view('pages.hotel-book');
});

Route::get('/newpriceshow',function()
{
	return view('pages.newpriceshow');
});
Route::get('/cancel_details',function()
{
	return view('pages.cancel_details');
});
Route::get('/invoice_hotel',function()
{
	return view('pages.invoice_hotel');
});
Route::get('/invoice_flight',function()
{
	return view('pages.invoice_flight');
});
Route::get('/forgotpassword',function()
{
	return view('pages.forgotpassword');
});
Route::get('/demotest',function()
{
	return view('pages.demotest');
});
// Route::get('/package_print',function()
// {
// 	return view('pages.package_print');
// });
Route::get('/package_invoice',function()
{
	return view('pages.package_invoice');
});
Route::get('/pdftest','HotelController@pdftest');
Route::get('/send/email', 'HotelController@pdftest');
//testflight
// Route::get('/flight_testbook','FlightBookController@flight_testbook')->name('flight_testbook');
//endtestflight
//flightcontroller
Route::get('/index','FlightController@index')->name('index');
//checkmultipcity
Route::get('/showmulticity','FlightController@showmulticity')->name('showmulticity');

Route::post('/results','FlightController@flightsearch')->name('flightsearch');
Route::get('/calendar_fare','FlightController@calendar_fare')->name('calendar_fare');

//flightbook
Route::match(['get', 'post'],'/flightbook','FlightBookController@flightbook')->name('flightbook');
Route::match(['get', 'post'],'/flight_bookview','FlightBookController@flight_bookview')->name('flight_bookview');
//flightinvoice
Route::get('/flightinvoice','FlightBookController@flightinvoice')->name('flightinvoice');

// newinvoice
Route::get('/flightnewinvoice','FlightBookController@flight_invoicebooking')->name('flightnewinvoice');
//end search
Route::get('/checkrefunable','FlightController@checkrefunable')->name('checkrefunable');
Route::get('/checkstopflight','FlightController@checkstopflight')->name('checkstopflight');
Route::get('/checkflightcodesearch','FlightController@checkflightcodesearch')->name('checkflightcodesearch');

//logincontroller
Route::post('/register','LoginController@register')->name('register');
Route::post('/checkuser','LoginController@checkuser')->name('checkuser');
Route::post('/gusestuser','LoginController@guestuser')->name('guestuser');
Route::get('/logout','LoginController@logout')->name('logout');
//queryemail
Route::post('/query_insert','LoginController@query_insert')->name('query_insert');
//endqueryemail

Route::post('/adminforgotpassword_check','LoginController@adminforgotpassword_check')->name('adminforgotpassword_check');
Route::post('/adminresetpassword_check','LoginController@adminresetpassword_check')->name('adminresetpassword_check');
//profile
Route::get('/profile','LoginController@profile')->name('profile');
Route::get('/userdetailupdate','LoginController@userdetailupdate')->name('userdetailupdate');
Route::match(['get', 'post'],'/adminimage_upload','LoginController@adminimage_upload')->name('adminimage_upload');
//passwordcheck
Route::get('/oldpasswordcheck','LoginController@oldpasswordcheck')->name('oldpasswordcheck');
Route::get('/usernewpassword','LoginController@usernewpassword')->name('usernewpassword');
//facebook start
Route::get('/login/{social}','LoginController@socialLogin')->where('social','twitter|facebook|linkedin|google|github|bitbucket');
Route::get('/login/{social}/callback','LoginController@handleProviderCallback')->where('social','twitter|facebook|linkedin|google|github|bitbucket');
//facebook end
//googlelogin
Route::get('/login/{social}','LoginController@socialLogin')->where('social','twitter|facebook|linkedin|google|github|bitbucket');
Route::get('/login/{social}/callback','LoginController@handleProviderCallback')->where('social','twitter|facebook|linkedin|google|github|bitbucket');
Route::get('/redirect', 'LoginController@redirect');
Route::get('/callback', 'LoginController@callback');
//endgooglelogin

//pdf
Route::get('/flight_pdf/{pnrno}','FlightBookController@flight_pdf');

//city search
Route::get('/getcities','HotelController@getcities')->name('getcities');

//hotel search
Route::post('/hotelresult','HotelController@gethotels')->name('gethotels');

//hotel info page
Route::post('/hotelinfo','HotelController@hotel_info')->name('hotelinfo');

//search hotels
Route::get('/searchhotels','HotelController@searchhotels')->name('searchhotels');
Route::get('/searchhotelname','HotelController@searchhotelname')->name('searchhotelname');

//book hotels
Route::post('/hotelbook','HotelController@hotelbook')->name('hotelbook');
Route::match(['get', 'post'],'/hotelbooking','HotelController@hotelbooking')->name('hotelbooking');
Route::get('/hotelinvoice','HotelController@hotelinvoice')->name('hotelinvoice');
Route::get('/hotelcancel','HotelController@hotelcancel')->name('hotelcancel');
//cancellation


//mytrip hotels
Route::get('/mytrip','LoginController@mytrip')->name('mytrip');
Route::get('/hotelviewinvoice/{bookingid}','HotelController@hotelviewinvoice')->name('hotelviewinvoice');
Route::get('/hotelcanceldetail/{chnagerequestid}','HotelController@hotelcanceldetail')->name('cancel_details');

//hotelpdf
Route::get('/hotel_pdf','HotelController@hotel_pdf');
Route::get('/hotel_pdf/{hotelbook}','HotelController@hotel_pdf');

//package details
// Route::get('/package-result',function()
// 	{
// 		return view('pages.package-result');
// 	});
// Route::get('/package-detail',function()
// 	{
// 		return view('pages.package-detail');
// 	});
//packages start
Route::get('/chkpckgtype','Packagecontroller@chkpckgtype')->name('chkpckgtype');
Route::match(['get', 'post'], '/packageresult','Packagecontroller@packageresult')->name('packageresult');
Route::match(['get', 'post'], '/packagedetails/{mainid}','Packagecontroller@packagedetails')->name('packagedetails');
Route::get('/package_enquiry','Packagecontroller@package_enquiry')->name('package_enquiry');
Route::get('/enqdatalast','Packagecontroller@enqdatalast')->name('enqdatalast');
Route::get('/request_package','Packagecontroller@request_package')->name('request_package');
Route::get('/packages_pay','Packagecontroller@packages_pay')->name('packages_pay');
Route::match(['get', 'post'],'/packagestripepayment','Packagecontroller@packagestripepayment')->name('packagestripepayment');

Route::post('/package_response','Packagecontroller@package_response');

Route::get('/package_print','Packagecontroller@package_print')->name('package_print');
Route::get('/package_invoice','Packagecontroller@package_invoice')->name('package_invoice');

Route::get('/package_pdf/{packid}','Packagecontroller@package_pdf');

//search package
Route::get('/search_package','Packagecontroller@search_package')->name('search_package');
//package end
Route::group(['prefix' => 'admin'], function() 
{
	Route::get('/','AdminController@index')->name('/');
	Route::post('/login_check','AdminController@login_check')->name('login_check');
	Route::get('/adminlogout','AdminController@adminlogout')->name('adminlogout');

	Route::get('/dashboard','AdminController@dashboard')->name('dashboard');
	//genderal setting
	Route::get('/general-setting','AdminController@generalsetting')->name('general-setting');
	Route::get('/smsrequest','AdminController@smsrequest')->name('smsrequest');
	Route::get('/hotelmargin','AdminController@hotelmargin')->name('hotelmargin');
	Route::get('/flightmargin','AdminController@flightmargin')->name('flightmargin');
	// Route::get('/general-setting',function()
	// {
	// 	return view('admin.general-setting');
	// });
	//user
	Route::get('/user','AdminController@user')->name('user');
	Route::match(['get', 'post'],'/user','AdminController@adminuserlist')->name('joining-products-list');
	Route::post('/adminuserlist','AdminController@adminuserlist')->name('adminuserlist');
	//flightsearch
	Route::get('/flightbooking','AdminController@flightbooking')->name('flightbooking');
	Route::match(['get', 'post'],'/flightbooking','AdminController@flightbooklist');
	Route::post('/flightbooklist','AdminController@flightbooklist')->name('flightbooklist');
	//booking details
	//hotelbookingdetails
		Route::get('/hotel-booking-details',function()
	{
		return view('admin.hotel-booking-details');
	});
		Route::get('/hotel-booking-details/{bookid}','AdminController@hotelbookingdetail');
	//endhotelbookingdetails
	//flightcancel
	Route::get('/flightcancel','FlightBookController@flightcancel')->name('flightcancel');
	
	//flightcancel
	Route::get('/booking-details',function()
	{
		return view('admin.booking-details');
	});
		Route::get('/booking-details/{bookid}','AdminController@bookingdetail');

		Route::get('/hotelbooking','AdminController@hotelbooking')->name('hotelbooking');
		Route::match(['get', 'post'],'/hotelbooking','AdminController@hotelbooklist');
		Route::post('/hotelbooklist','AdminController@hotelbooklist')->name('hotelbooklist');
		Route::get('/hotel-details',function()
		{
			return view('admin.hotel-details');
		});
	//canceldetail
		Route::get('/canceldetail','AdminController@canceldetail')->name('canceldetail');
		Route::match(['get', 'post'],'/canceldetail','AdminController@canceldetaillist');
		Route::post('/canceldetaillist','AdminController@canceldetaillist')->name('canceldetaillist');
		Route::get('/flight-booking-cancel-details/{bookid}','AdminController@flightbookingcanceldetail');
		Route::get('/hotelrefund','AdminController@hotelrefund')->name('hotelrefund');
	//hotelcanceldetail
		Route::get('/adminhotelcancel','AdminController@adminhotelcancel')->name('adminhotelcancel');
		Route::match(['get', 'post'],'/adminhotelcancel','AdminController@adminhotelcancellist');
		Route::post('/adminhotelcancellist','AdminController@adminhotelcancellist')->name('adminhotelcancellist');
		Route::get('/hotel-booking-cancel-details/{bookid}','AdminController@hotelbookingcanceldetail');
		Route::get('/hotelrefund','AdminController@hotelrefund')->name('hotelrefund');
	//endhotelcanceldetail
	//endcanceldetail
	Route::get('/paymentdetail','AdminController@paymentdetail')->name('paymentdetail');
	Route::match(['get', 'post'],'/paymentdetail','AdminController@paymentdetaillist');
	Route::post('/paymentdetaillist','AdminController@paymentdetaillist')->name('paymentdetaillist');
	//forgotpassword
	Route::get('/forgot-password',function()
	{
		return view('admin.forgot-password');
	});
	Route::get('/change-password',function()
	{
		return view('admin.change-password');
	});
	//endforgotpassword
	//advertisment banner
	Route::get('/advertisement-banner','AdminController@advertisment')->name('/advertisement-banner');
	Route::match(['get', 'post'],'/advertisment_image_upload','AdminController@advertisment_image_upload')->name('advertisment_image_upload');
	Route::get('/insert_banner','AdminController@insert_banner')->name('insert_banner');
	// Route::get(['get', 'post'],'/advertisement-banner-list','AdminController@advertisement_banner_list')->name('advertisement-banner-list');
	Route::match(['get', 'post'],'/advertisement-banner-list','AdminController@advertisement_banner_list')->name('advertisement-banner-list');
	Route::post('/advlist','AdminController@advlist')->name('advlist');
	Route::get('/advertisementedit/{advid}','AdminController@advertismentedit')->name('advertisementedit');
	Route::get('/adv_edit_banner','AdminController@adv_edit_banner')->name('adv_edit_banner');
	//packages country
	Route::get('/package-country','AdminController@packagecountry')->name('/package-country');
	Route::get('/insert_package_country','AdminController@insert_package_country')->name('insert_package_country');
	Route::match(['get', 'post'],'/package-country-list','AdminController@packagecountrylist')->name('package-country-list');
	Route::post('/pckgcounttylist','AdminController@pckgcounttylist')->name('pckgcounttylist');
	Route::get('/pckgcountrylistedit/{pckgcounty}','AdminController@pckgcountrylistedit')->name('pckgcountrylistedit');
	Route::get('/update_package_country','AdminController@update_package_country')->name('update_package_country');

	//packages add
	Route::get('/package-add','AdminController@packageadd')->name('/package-add');
	Route::match(['get', 'post'],'/package_image_upload','AdminController@package_image_upload')->name('package_image_upload');
	Route::match(['get', 'post'],'/sightseen_image_upload','AdminController@sightseen_image_upload')->name('sightseen_image_upload');
	Route::match(['get', 'post'],'/packgae_pdf_upload','AdminController@packgae_pdf_upload')->name('packgae_pdf_upload');
	Route::get('/checkpackagetype','AdminController@checkpackagetype')->name('checkpackagetype');
	Route::post('/packageadd','AdminController@package_add')->name('packageadd');
	Route::match(['get', 'post'],'/package_list','AdminController@package_list')->name('package_list');
	Route::post('/sortpackagelist','AdminController@sortpackagelist')->name('sortpackagelist');
	Route::get('/package_edit/{pckgid}','AdminController@package_edit')->name('package_edit');
	Route::match(['get', 'post'],'/package_update','AdminController@package_update')->name('package_update');
	// Route::post('/package_list','AdminController@package_list')->name('package_list');
	Route::get('/demos/sortabledatatable','DemoController@showDatatable')->name('sortabledatatable');
	Route::post('/demos/sortabledatatable','DemoController@updateOrder')->name('sortabledatatable');

	Route::get('/package_enquiry_list','AdminController@package_enquiry_list')->name('package_enquiry_list');
	Route::match(['get', 'post'],'/package_enquiry_list','AdminController@package_enquiry_list1')->name('package_enquiry_list');
	Route::get('/package-enq-details/{pckid}','AdminController@pckgenqdetails');

	Route::get('/offerprice_insert','AdminController@offerprice_insert')->name('offerprice_insert');
	Route::get('/chkcityname','AdminController@chkcityname')->name('chkcityname');
	Route::get('/tour_delete_pack','AdminController@tour_delete_pack')->name('tour_delete_pack');

	// query list
	Route::get('/query-list','AdminController@query_list')->name('query-list');
	Route::match(['get', 'post'],'/query-list','AdminController@query_list1')->name('query-list');
	
	//testiminol
	Route::get('/add-testimonial','AdminController@add_testimonial')->name('add-testimonial');
	Route::get('/insert_testi','AdminController@insert_testi')->name('insert_testi');
	Route::get('/list-testimonial','AdminController@testimonial_list')->name('list-testimonial');
	Route::match(['get', 'post'],'/list-testimonial','AdminController@testimonial_list1')->name('list-testimonial');
	Route::get('/testi_active','AdminController@testi_active')->name('testi_active');
	Route::get('/edit-testimonial','AdminController@edit_testimonial')->name('edit-testimonial');
	Route::get('/update_testi','AdminController@update_testi')->name('update_testi');
});
//paymentcheck
Route::get('/payment',function()
{
	return view('pages.payment');
});
Route::get('/request',function()
{
	return view('pages.request');
});
Route::get('/request1','FlightBookController@request1')->name('request1');
//hotelpayment
Route::get('/request_hotel','HotelController@request_hotel')->name('request_hotel');
Route::post('/hotel_response','HotelController@hotel_response');
Route::post('/response','FlightBookController@response');
//stripe paymentgateway
Route::match(['get', 'post'],'/stripepayment','HotelController@stripepayment')->name('stripepayment');

//flight
Route::match(['get', 'post'],'/flightstripepayment','FlightBookController@flightstripepayment')->name('flightstripepayment');


// Route::get('/flightstripepayment',function()
// {
// 	return view('pages.flightstripepayment');
// });
// Route::get('/response','FlightBookController@response');

// Route::get('/hotel-booking','HotelController@hotel_booking')->name('hotel-booking');

//daman
Route::get('/aboutus',function()
{
	return view('pages.aboutus');
});
Route::get('terms-and-conditions',function()
{
	return view('pages.terms-and-conditions');
});
Route::get('privacy-and-policy',function()
{
	return view('pages.privacy-and-policy');
});
Route::get('disclaimer',function()
{
	return view('pages.disclaimer');
});
Route::get('privacy-and-policy',function()
{
	return view('pages.privacy-and-policy');
});
Route::get('feedback',function()
{
	return view('pages.feedback');
});
Route::get('holiday-packages',function()
{
return view('pages.holiday-packages');
});
Route::get('airport-codes',function()
{
return view('pages.airport-codes');
});
Route::get('domestic-airlines',function()
{
return view('pages.domestic-airlines');
});
Route::get('international-airlines',function()
{
return view('pages.international-airlines');
});
Route::get('newsletter',function()
{
	return view('pages.newsletter');
});
Route::get('pdf-preview',function()
{
	return view('pages.pdf-preview');
});




Route::get('our-profile',function()
{
return view('pages.our-profile');
});
Route::get('faqs',function()
{
return view('pages.faqs');
});
Route::get('partner-with-travoweb',function()
{
return view('pages.partner-with-travoweb');
});
Route::get('travoweb-business',function()
{
return view('pages.travoweb-business');
});
Route::get('contact-us',function()
{
return view('pages.contact-us');
});
Route::get('blog',function()
{
return view('pages.blog');
});
Route::get('blog1',function()
{
return view('pages.blog1');
});
Route::get('blog2',function()
{
return view('pages.blog2');
});
Route::get('blog3',function()
{
return view('pages.blog3');
});
Route::get('blog4',function()
{
return view('pages.blog4');
});
Route::get('services',function()
{
return view('pages.services');
});

Route::get('/sendEmailReminder','Packagecontroller@sendEmailReminder');