<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class countrypackage extends Model
{
    protected $table = 'tbl_country_package';
    protected $fillable = [
        'id', 'statename','packge_type', 'package_country_status','order_id',
    ];
}
