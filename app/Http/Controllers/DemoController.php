<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\countrypackage;
class DemoController extends Controller
{
    public function showDatatable()
    {
        $tasks = countrypackage::orderBy('order_id','ASC')->select('id','statename','packge_type','package_country_status')->get();

        return view('admin.demos.sortabledatatable',compact('tasks'));
    }

    public function updateOrder(Request $request)
    {
        $tasks = countrypackage::all();
        echo "tasks";
        foreach ($tasks as $task) {
            $task->timestamps = false; // To disable update_at field updation
            $id = $task->id;

            foreach ($request->order as $order) {
                print_r($order);
                if ($order['id  '] == $id) {
                    $task->update(['order_id' => $order['position']]);
                }
            }
        }
        
        return response('Update Successfully.', 200);
    }
}
