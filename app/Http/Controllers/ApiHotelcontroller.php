<?php

namespace App\Http\Controllers;


use session;
use DB;
use Cookie;
use App\hotel_city;
use App\tbl_hotel_book;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\tbl_mytrip;
use PDF;
use App;
use Dompdf\Dompdf;
use App\tbl_payment_order;
use File;
use Mail;
use App\Mail\SendMailable;
use App\Price;
use \Stripe;
use App\tbl_general_setting;
use App\tbl_token;
use App\token_api;

class ApiHotelcontroller extends Controller
{
    public function getcities(Request $request)
    {

       
        $citykeyword=$request->get('citykeyword');
        $citieslist=hotel_city::where('Destination','LIKE',$citykeyword."%")->limit(7)->get();
       
         $data=array();
         $i=0;
           foreach($citieslist as $list)
        {
            
            $data[$i]['cityid'] =$list->cityid;
            $data[$i]['destination']=$list->Destination;
            $data[$i]['country']=$list->country;
            
            
         $i++;
         }
         echo json_encode($data);
        // foreach($citieslist as $list)
        // {
          
        //     $data['cityid'] =$list->cityid;
        //     $data['destination']=$list->Destination;
        //     $data['country']=$list->country;
        //      $maindata =   json_encode($data);
        //      $newdata[]=$maindata;
        //  }
         
        //  print_r($newdata);
       
       // echo json_encode($data);

         
        // if($data=='')
        // {
        //     $data[]="No cities found";
        // }
       
     
        

    }


    public function iosgetcities(Request $request)
    {

       
        // $citykeyword=$request->get('citykeyword');
        $citieslist=hotel_city::get();
       
         $data=array();

         $i=0;
           foreach($citieslist as $list)
        {
            
            $data[$i]['cityid'] =$list->cityid;
            $data[$i]['destination']=$list->Destination;
            $data[$i]['country']=$list->country;
            
            
         $i++;
        
         }
         
        echo json_encode([
            'key' =>$data,
        ]);
          
     }

	public function index(Request $request)
    {

        $gensetting=tbl_general_setting::orderBy('id', 'DESC')->first();
        Cookie::queue('gensetting',$gensetting, 1440);
        //$this->price('2000');
            $sql_token=token_api::where('id','1')->orderBy('id','DESC')->first();
            $todaydate = date("Y-m-d") ;
            $create_time=date('H:i:s');
            $todaynewdate=strtotime($todaydate);
            $tokendate=strtotime($sql_token->create_date);
            if($todaynewdate==$tokendate)
            {
              $newtokenid=$sql_token->token_id;
              $memberid=$sql_token->MemberId;
                $agencyid=$sql_token->AgencyId;
                $loginname=$sql_token->LoginName;
                $localip=$sql_token->localip;

            }
            else
            {
        	$ClientId ='ApiIntegrationNew';
            $UserName = 'Sekap';
            $Password = 'Sekap@123';
            $EndUserIp = '192.168.11.120';
            $form_data = array(
                'ClientId'=>$ClientId,
                'UserName'=>$UserName,
                'Password'=>$Password,
                'EndUserIp'=>$EndUserIp,
            );
            $data_string = json_encode($form_data);

            $ch = curl_init('http://api.tektravels.com/SharedServices/SharedData.svc/rest/Authenticate');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );

            $result = curl_exec($ch);
            $result1=json_decode($result,true);
               $res = explode(':',$result1['Member']['LoginDetails']);
              // $request->session()->put('localip', trim($res[3]));
                $sql_update=token_api::where('id','1')->update(['token_id' => $result1['TokenId'],'create_date' => $todaydate,'create_time'=>$create_time,'MemberId'=>$result1['Member']['MemberId'],'AgencyId'=>$result1['Member']['AgencyId'],'LoginName'=>$result1['Member']['LoginName'],'localip'=>$res[3] ]);
                    $newtokenid=$result1['TokenId'];
                $memberid=$result1['Member']['MemberId'];
                $agencyid=$result1['Member']['AgencyId'];
                $loginname=$result1['Member']['LoginName'];
                $localip=$res[3];


               
            }
            $result1['TokenId']=$newtokenid;
            $result1['MemberId']=$memberid;
            $result1['AgencyId']=$agencyid;
            $result1['LoginName']=$loginname;
            $result1['localip']=trim($localip);
            $result1['gensetting']=$gensetting;
            echo json_encode($result1);
           
    }
    public function apigethotels(Request $request)
	{
		
		$NoOfAdults1=0;
		$NoOfChild1=0;
		$NoOfAdultsarray=array();
		$NoOfchildarray=array();
		$childagearray=array();

		$TokenId=$request->get('TokenId'); //cokkie change
		$EndUserIp=$request->get('localip');
		$destination= $request->get('destination');
		$CheckInDate=$request->get('check_in');
		$newchkdate = explode('/',$request->get('check_in'));
		$check_in_format=$newchkdate[2].'-'.$newchkdate[1].'-'.$newchkdate[0];
		$check_out=$request->get('check_out');
		$newchkdateout = explode('/',$request->get('check_out'));
		$check_out_format=$newchkdateout[2].'-'.$newchkdateout[1].'-'.$newchkdateout[0];
		$check_in_day = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $check_in_format.' 0:00:00');
		$check_out_day = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $check_out_format.' 0:00:00');
		$NoOfNights = $check_in_day->diffInDays($check_out_day);
		
		$ChildAge='';
		$PreferredHotel='';
		$MaxRating ='5';
		$MinRating='0';
		$ReviewScore='0';
		$IsNearBySearchAllowed='false';
		$deti = explode(' (',$request->get('destination'));
		$destination=$deti[0];
		$destination_country=str_replace(')','',$deti[1]);
		$destilist=hotel_city::where('Destination',$destination)->where('country',$destination_country)->first();
		$CityId =  $destilist->cityid;
		$CountryCode =  $destilist->countrycode;
		$statename= $destilist->stateprovince;
		$countryname= $destilist->country;
		$ResultCount ='0';
		$PreferredCurrency="INR";
		$GuestNationality=$destilist->countrycode;
		 $NoOfRooms= $request->get('newroom');
		 $roomguests_array=array();
		for($st=1;$st<=$NoOfRooms;$st++)
		{
			
			$roomadult = $request->get('roomadult-'.$st);
			$children = $request->get('children-'.$st);
			$childage = $request->get('childage-'.$st);
			if(!empty($childage))
			{
				// $ChildAge1=implode(',',$childage);
				$ChildAge=explode(',',$childage);
			}
			else
			{
				$ChildAge="";
			}
			
			
			  // print_r($ChildAge1);
			 $roomguests_array[]=array('NoOfAdults' =>$roomadult,
				'NoOfChild' =>$children,
				'ChildAge' =>$ChildAge);
			
			$NoOfAdults1+=$roomadult;
			$NoOfChild1+=$children;
			$NoOfAdultsarray[]=$roomadult;
			$NoOfchildarray[]=$children;
			$childagearray[]=$childage;
			// echo count($roomadult);
		}
		
		$form_data = array(
			'CheckInDate'=>$CheckInDate,
			'NoOfNights'=>$NoOfNights,
			'CountryCode' => $CountryCode,
			'CityId' =>$CityId,
			'ResultCount'=>$ResultCount,
			'PreferredCurrency' => $PreferredCurrency,
			'GuestNationality' =>$GuestNationality,
			'NoOfRooms' =>$NoOfRooms,
			'RoomGuests' =>$roomguests_array,
			'PreferredHotel'=>$PreferredHotel,
			'MaxRating'=>$MaxRating,
			'MinRating'=>$MinRating,
			'ReviewScore'=>null,
			'IsNearBySearchAllowed'=>$IsNearBySearchAllowed,
			'EndUserIp'=>$EndUserIp,
			'TokenId'=>$TokenId,
		);


		$data_string = json_encode($form_data);
		$ch = curl_init('http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelResult/');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
	);                                                                                                                   

		  $result = curl_exec($ch);
		$result1=json_decode($result,true);
	      

		$hotelarray = !empty($result1['HotelSearchResult'])?$result1['HotelSearchResult']:array();
		$apihotelarray=array();
		if(!empty($hotelarray))
       {
	       	if($hotelarray['Error']['ErrorCode']==0)
	       	{
	             for($ht=0;$ht<count($hotelarray['HotelResults']);$ht++)
	            {
	                $mainp = $hotelarray['HotelResults'][$ht]['Price']['PublishedPriceRoundedOff'];
	                $margin = ($mainp * 3)/100;
	                $mainprice = $mainp + $margin;
                    $curr=$request->get('maincurreny');
	                $price= $this->price($mainprice,0,0,0,0,$curr);
                   
	                $hotelmainprice = $price[0] ;
	                $hotelmaincurrecny = $price[1];
	                $hotelarray['HotelResults'][$ht]['hotelmainprice']=$hotelmainprice;
	                $hotelarray['HotelResults'][$ht]['hotelmaincurrecny']=$hotelmaincurrecny;
	                $hotelarray['HotelResults'][$ht]['hotelmargin']=$mainprice;
	                $hotelarray['HotelResults'][$ht]['hotellastprice']=$mainp;
	                $hotelarray['HotelResults'][$ht]['hotelconvercurr']=$price[2];
	              	
	              	$apihotelarray['HotelResults'][$ht]['ResultIndex']=$hotelarray['HotelResults'][$ht]['ResultIndex'];
	              	$apihotelarray['HotelResults'][$ht]['HotelCode']=$hotelarray['HotelResults'][$ht]['HotelCode'];
	              	$apihotelarray['HotelResults'][$ht]['HotelName']=$hotelarray['HotelResults'][$ht]['HotelName'];
	              	$apihotelarray['HotelResults'][$ht]['StarRating']=$hotelarray['HotelResults'][$ht]['StarRating'];
	              	$apihotelarray['HotelResults'][$ht]['HotelPicture']=$hotelarray['HotelResults'][$ht]['HotelPicture'];
	              	$apihotelarray['HotelResults'][$ht]['HotelAddress']=$hotelarray['HotelResults'][$ht]['HotelAddress'];
	              	$apihotelarray['HotelResults'][$ht]['hotelmainprice']=$hotelmainprice;;
	              	$apihotelarray['HotelResults'][$ht]['hotelmaincurrecny']=$hotelmaincurrecny;




	                
	            }
                $apihotelarray['TraceId']=$result1['HotelSearchResult']['TraceId']; 
	       }
            else
           {
                $apihotelarray['chkval']=$hotelarray['Error']['ErrorMessage'];
           }
            
       }
      
      
    
       echo json_encode($apihotelarray);
	
	}


    //ios code

    public function ios_apigethotels(Request $request)
    {
        
        $NoOfAdults1=0;
        $NoOfChild1=0;
        $NoOfAdultsarray=array();
        $NoOfchildarray=array();
        $childagearray=array();

        $TokenId=$request->get('TokenId'); //cokkie change
        $EndUserIp=$request->get('localip');
      $destination= $request->get('destination');
         $CheckInDate=$request->get('check_in');
        $newchkdate = explode('/',$request->get('check_in'));
       $check_in_format=$newchkdate[2].'-'.$newchkdate[1].'-'.$newchkdate[0];
        $check_out=$request->get('check_out');
        $newchkdateout = explode('/',$request->get('check_out'));
        $check_out_format=$newchkdateout[2].'-'.$newchkdateout[1].'-'.$newchkdateout[0];
        $check_in_day = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $check_in_format.' 0:00:00');
        $check_out_day = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $check_out_format.' 0:00:00');
        $NoOfNights = $check_in_day->diffInDays($check_out_day);
        
        $ChildAge='';
        $PreferredHotel='';
        $MaxRating ='5';
        $MinRating='0';
        $ReviewScore='0';
        $IsNearBySearchAllowed='false';
        $deti = explode('(',$request->get('destination'));
        $destination=$deti[0];
        $destination_country=str_replace(')','',$deti[1]);
        $destilist=hotel_city::where('Destination',$destination)->where('country',$destination_country)->first();
        $CityId =  $destilist->cityid;
        $CountryCode =  $destilist->countrycode;
        $statename= $destilist->stateprovince;
        $countryname= $destilist->country;
        $ResultCount ='0';
        $PreferredCurrency="INR";
        $GuestNationality=$destilist->countrycode;
         $NoOfRooms= $request->get('newroom');
         $mainmargin=$request->get('mainmargin');
         $roomguests_array=array();
        for($st=1;$st<=$NoOfRooms;$st++)
        {
            
            $roomadult = $request->get('roomadult-'.$st);
            $children = $request->get('children-'.$st);
            $childage = $request->get('childage-'.$st);
            if(!empty($childage))
            {
                // $ChildAge1=implode(',',$childage);
                $ChildAge=explode(',',$childage);
            }
            else
            {
                $ChildAge="";
            }
            
            
              // print_r($ChildAge1);
             $roomguests_array[]=array('NoOfAdults' =>$roomadult,
                'NoOfChild' =>$children,
                'ChildAge' =>$ChildAge);
            
            $NoOfAdults1+=$roomadult;
            $NoOfChild1+=$children;
            $NoOfAdultsarray[]=$roomadult;
            $NoOfchildarray[]=$children;
            $childagearray[]=$childage;
            // echo count($roomadult);
        }
        
        $form_data = array(
            'CheckInDate'=>$CheckInDate,
            'NoOfNights'=>$NoOfNights,
            'CountryCode' => $CountryCode,
            'CityId' =>$CityId,
            'ResultCount'=>$ResultCount,
            'PreferredCurrency' => $PreferredCurrency,
            'GuestNationality' =>$GuestNationality,
            'NoOfRooms' =>$NoOfRooms,
            'RoomGuests' =>$roomguests_array,
            'PreferredHotel'=>$PreferredHotel,
            'MaxRating'=>$MaxRating,
            'MinRating'=>$MinRating,
            'ReviewScore'=>null,
            'IsNearBySearchAllowed'=>$IsNearBySearchAllowed,
            'EndUserIp'=>$EndUserIp,
            'TokenId'=>$TokenId,
        );


        $data_string = json_encode($form_data);
        $ch = curl_init('http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelResult/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
    );                                                                                                                   

          $result = curl_exec($ch);
        $result1=json_decode($result,true);
          

        $hotelarray = !empty($result1['HotelSearchResult'])?$result1['HotelSearchResult']:array();
        $apihotelarray=array();
        if(!empty($hotelarray))
       {
            if($hotelarray['Error']['ErrorCode']==0)
            {
                 for($ht=0;$ht<count($hotelarray['HotelResults']);$ht++)
                {
                    $mainp = $hotelarray['HotelResults'][$ht]['Price']['PublishedPriceRoundedOff'];
                    $margin = ($mainp * $mainmargin)/100;
                    $mainprice = $mainp + $margin;
                    $curr=$request->get('maincurreny');

                    $price= $this->price($mainprice,0,0,0,0,$curr);
                 
                    $hotelmainprice = $price[0] ;
                    $hotelmaincurrecny = $price[1];
                    $hotelarray['HotelResults'][$ht]['hotelmainprice']=round($hotelmainprice, 2);
                    $hotelarray['HotelResults'][$ht]['hotelmaincurrecny']=$hotelmaincurrecny;
                    $hotelarray['HotelResults'][$ht]['hotelmargin']=$mainprice;
                    $hotelarray['HotelResults'][$ht]['hotellastprice']=$mainp;
                    $hotelarray['HotelResults'][$ht]['hotelconvercurr']=$price[2];
                    
                    $apihotelarray['HotelResults'][$ht]['ResultIndex']=$hotelarray['HotelResults'][$ht]['ResultIndex'];
                    $apihotelarray['HotelResults'][$ht]['HotelCode']=$hotelarray['HotelResults'][$ht]['HotelCode'];
                    $apihotelarray['HotelResults'][$ht]['HotelName']=$hotelarray['HotelResults'][$ht]['HotelName'];
                    $apihotelarray['HotelResults'][$ht]['StarRating']=$hotelarray['HotelResults'][$ht]['StarRating'];
                    $apihotelarray['HotelResults'][$ht]['HotelPicture']=$hotelarray['HotelResults'][$ht]['HotelPicture'];
                    $apihotelarray['HotelResults'][$ht]['HotelAddress']=$hotelarray['HotelResults'][$ht]['HotelAddress'];
                    $apihotelarray['HotelResults'][$ht]['hotelmainprice']=round($hotelmainprice);
                    $apihotelarray['HotelResults'][$ht]['hotelmaincurrecny']=$hotelmaincurrecny;

                 


                    
                }
                $apihotelarray['TraceId']=$result1['HotelSearchResult']['TraceId']; 
                 $result = array('response'=>0,'status'=>'success','message'=>$apihotelarray);
           }
            else
           {
                // $apihotelarray['chkval']=$hotelarray['Error']['ErrorMessage'];
                $result = array('response'=>1,'status'=>'fail','message'=>$hotelarray['Error']['ErrorMessage']);  
           }
           
       }
      
      else
      {
        $result = array('response'=>1,'status'=>'fail','message'=>'Hotel Not Found');    
      }
      
    
       echo json_encode($result);
    
    }
    //end ios
	//hotelinfo
	 public function hotel_info(Request $request)
    {
        $hotelname=$request->get('hotelname');
        $hotelindex=$request->get('hotelindex');
        $hotelcode=$request->get('hotelcode');
        $hotelname=$request->get('hotelname');
        $traceid= $request->get('TraceId');
        $TokenId=$request->get('TokenId');
        $EndUserIp= $request->get('EndUserIp');
        $form_data = array(
            'EndUserIp'=>$EndUserIp,
            'TokenId'=>$TokenId,
            'TraceId'=>$traceid,
            'ResultIndex'=>$hotelindex,
            'HotelCode'=>$hotelcode
        );

        $data_string = json_encode($form_data);
        
        $ch = curl_init('http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelInfo/');


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result_info = curl_exec($ch);
        $result_info1=json_decode($result_info,true);

        $hotelinfoarray= !empty($result_info1['HotelInfoResult'])?$result_info1['HotelInfoResult']:array();

        $data_string1 = json_encode($form_data);

        $ch = curl_init('http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelRoom/');


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result_rooms = curl_exec($ch);
        $result_rooms1=json_decode($result_rooms,true);

       //  $hotelmargin1=Cookie::get('gensetting');
       // $hotelmargin = $hotelmargin1->hotel_margin;
        $hotelmargin=$request->get('hotelmargin');
        $hotelroomsarray= !empty($result_rooms1['GetHotelRoomResult'])?$result_rooms1['GetHotelRoomResult']:array();

        if(!empty($hotelroomsarray))
        {
            if($hotelinfoarray['Error']['ErrorCode']==0)
            {
                for($hr=0;$hr<count($hotelroomsarray['HotelRoomsDetails']);$hr++)
                {
                    $mainp = $hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['RoomPrice'];

                    $ServiceTax = $hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['Tax']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['AgentCommission']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['AgentMarkUp']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['ServiceTax']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['TDS']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['TotalGSTAmount'];

                    $tdsa =$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['ExtraGuestCharge']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['ChildCharge']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['OtherCharges'];

                    $gsta = $hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['TotalGSTAmount'];


                    $margin = ($mainp * $hotelmargin)/100;
                    $mainprice = $mainp + $margin;
                    $curr=$request->get('maincurreny');
                    $price= $this->price($mainprice,$ServiceTax,$tdsa,$gsta,0,$curr);


                    $hotelmainprice = $price[0];
                    $hotelmaincurrecny = $price[1];


                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmainprice']=$hotelmainprice;
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmaincurrecny']=$hotelmaincurrecny;
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmargin']=$mainprice;
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotellastprice']=$mainp;
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelconvercurr']=$price[2];
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelservicetax']=$price[3];
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelothercharges']=$price[4];
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelgst']=$price[5];

                }
            }
        }
        $bookingarray=array('hotelresultindex'=>$hotelindex,
                            'hotelcode'=>$hotelcode,
                            'hotelname'=>$hotelname);
	  $main['hotelinfoarray']=$hotelinfoarray;
      $main['hotelroomsarray']=$hotelroomsarray;
      $main['bookingarray']=$bookingarray;
    
     echo json_encode($main);

    }

    //ios hotelbook
     public function ios_hotel_info(Request $request)
    {
        $hotelname=$request->get('hotelname');
        $hotelindex=$request->get('hotelindex');
        $hotelcode=$request->get('hotelcode');
        $hotelname=$request->get('hotelname');
        $traceid= $request->get('TraceId');
        $TokenId=$request->get('TokenId');
        $EndUserIp= $request->get('EndUserIp');
        $form_data = array(
            'EndUserIp'=>$EndUserIp,
            'TokenId'=>$TokenId,
            'TraceId'=>$traceid,
            'ResultIndex'=>$hotelindex,
            'HotelCode'=>$hotelcode
        );

        $data_string = json_encode($form_data);
        
        $ch = curl_init('http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelInfo/');


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result_info = curl_exec($ch);
        $result_info1=json_decode($result_info,true);

        $hotelinfoarray= !empty($result_info1['HotelInfoResult'])?$result_info1['HotelInfoResult']:array();

        $data_string1 = json_encode($form_data);

        $ch = curl_init('http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelRoom/');


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result_rooms = curl_exec($ch);
        $result_rooms1=json_decode($result_rooms,true);

       //  $hotelmargin1=Cookie::get('gensetting');
       // $hotelmargin = $hotelmargin1->hotel_margin;
        $hotelmargin=$request->get('hotelmargin');
        $hotelroomsarray= !empty($result_rooms1['GetHotelRoomResult'])?$result_rooms1['GetHotelRoomResult']:array();

        if(!empty($hotelroomsarray))
        {
            if($hotelinfoarray['Error']['ErrorCode']==0)
            {
                for($hr=0;$hr<count($hotelroomsarray['HotelRoomsDetails']);$hr++)
                {
                    $mainp = $hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['RoomPrice'];

                    $ServiceTax = $hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['Tax']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['AgentCommission']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['AgentMarkUp']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['ServiceTax']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['TDS']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['TotalGSTAmount'];

                    $tdsa =$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['ExtraGuestCharge']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['ChildCharge']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['OtherCharges'];

                    $gsta = $hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['TotalGSTAmount'];


                    $margin = ($mainp * $hotelmargin)/100;
                    $mainprice = $mainp + $margin;
                    $curr=$request->get('maincurreny');
                    $price= $this->price($mainprice,$ServiceTax,$tdsa,$gsta,0,$curr);


                    $hotelmainprice = $price[0];
                    $hotelmaincurrecny = $price[1];


                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmainprice']=$hotelmainprice;
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmaincurrecny']=$hotelmaincurrecny;
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmargin']=$mainprice;
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotellastprice']=$mainp;
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelconvercurr']=$price[2];
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelservicetax']=$price[3];
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelothercharges']=$price[4];
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelgst']=$price[5];

                }
                $bookingarray=array('hotelresultindex'=>$hotelindex,
                            'hotelcode'=>$hotelcode,
                            'hotelname'=>$hotelname);
                          $main['hotelinfoarray']=$hotelinfoarray;
                          $main['hotelroomsarray']=$hotelroomsarray;
                          $main['bookingarray']=$bookingarray;
                          $result = array('response'=>0,'status'=>'success','message'=>$main);

            }
            else
            {
               
                $result = array('response'=>1,'status'=>'fail','message'=>$hotelinfoarray['Error']['ErrorMessage']);
            }
        }
        else
        {
            $result = array('response'=>1,'status'=>'fail','message'=>"No Data");
        }
        
    
     echo json_encode($result);

    }
    //hotel book
    public function hotelbook(Request $request)
    {
        $hotelname=$request->get('hotelname');
        
        $hotelroomtype=$request->get('hotelroomname');
      

        $TokenId=$request->get('TokenId');
        $EndUserIp=$request->get('EndUserIp');
        $TraceId=$request->get('TraceId');

        $resultindex=$request->get('ResultIndex');
        $hotelcode=$request->get('HotelCode');
        $roomindex=explode(",",$request->get('roomindex'));
        $RoomTypeCode1=explode(",",$request->get('RoomTypeCode'));
       
        $RoomTypeName1=explode(",",$request->get('RoomTypeName'));
        $RatePlanCode1=explode(",",$request->get('RatePlanCode'));
        $CurrencyCode=$request->get('CurrencyCode');
        $RoomPrice1=explode(",",$request->get('RoomPrice'));

        $Tax1=explode(",",$request->get('Tax'));
        $ExtraGuestCharge1=explode(",",$request->get('ExtraGuestCharge'));
        $ChildCharge1=explode(",",$request->get('ChildCharge'));
        $OtherCharges1=explode(",",$request->get('OtherCharges'));
        $Discount1=explode(",",$request->get('Discount'));
        $PublishedPrice1=explode(",",$request->get('PublishedPrice'));

        $PublishedPriceRoundedOff1=explode(",",$request->get('PublishedPriceRoundedOff'));
        $OfferedPrice1=explode(",",$request->get('OfferedPrice'));
        $OfferedPriceRoundedOff1=explode(",",$request->get('OfferedPriceRoundedOff'));
        $AgentCommission1=explode(",",$request->get('AgentCommission'));
        $AgentMarkUp1=explode(",",$request->get('AgentMarkUp'));

        $ServiceTax1=explode(",",$request->get('ServiceTax'));
        $TDS1=explode(",",$request->get('TDS'));

        // $roomindexarray=explode('-',$roomindex);
        $hotelname=$request->get('HotelName');
        $selectedhotel=array();
        $selectedroom=array();
        $roomdetails=array();

        //hotel details code
        // if(session()->has('resultarray'))
        // {
        //     $resultarray=session()->get('resultarray');
        //     for($i=0;$i<count($resultarray['HotelResults']);$i++)
        //     {
        //         if($resultarray['HotelResults'][$i]['HotelName']==$hotelname)
        //         {
        //             $selectedhotel=$resultarray['HotelResults'][$i];
        //         }
        //     }
        // }
        //end of hotel details code
        // if(session()->has('hotelroomsdata'))
        // {
        //     $resultarray=session()->get('hotelroomsdata');
        // }
        // for($i=0;$i<count($resultarray['HotelRoomsDetails']);$i++)
        // {
            // for($room=0;$room<count($roomindexarray);$room++)
            // {
            //     if($roomindexarray[$room]!='')
            //     {
            //         if($resultarray['HotelRoomsDetails'][$i]['RoomIndex']== $roomindexarray[$room])
            //         {
            //             $selectedroom[]=$resultarray['HotelRoomsDetails'][$i];
            //         }
            //     }
            // }
        // }
        $GuestNationality=$request->get('GuestNationality');
        $NoOfRooms=$request->get('NoOfRooms');
        $ClientReferenceNo=0;
        $IsVoucherBooking=true;
        for($roomdetailcount=0;$roomdetailcount<count($roomindex);$roomdetailcount++)
        {
        	$RoomIndex=$roomindex[$roomdetailcount];
        	$RoomTypeCode=$RoomTypeCode1[$roomdetailcount];
        	$RoomTypeName=$RoomTypeName1[$roomdetailcount];
        	$RatePlanCode=$RatePlanCode1[$roomdetailcount];

        	$RoomPrice=$RoomPrice1[$roomdetailcount];
        	$Tax=$Tax1[$roomdetailcount];
        	$ExtraGuestCharge=$ExtraGuestCharge1[$roomdetailcount];
        	$ChildCharge=$ChildCharge1[$roomdetailcount];
        	$OtherCharges=$OtherCharges1[$roomdetailcount];
        	$Discount=$Discount1[$roomdetailcount];
        	$PublishedPrice=$PublishedPrice1[$roomdetailcount];
        	$PublishedPriceRoundedOff=$PublishedPriceRoundedOff1[$roomdetailcount];
        	$OfferedPrice=$OfferedPrice1[$roomdetailcount];

        	$OfferedPriceRoundedOff=$OfferedPriceRoundedOff1[$roomdetailcount];
        	$AgentCommission=$AgentCommission1[$roomdetailcount];
        	$AgentMarkUp=$AgentMarkUp1[$roomdetailcount];
        	$ServiceTax=$ServiceTax1[$roomdetailcount];
        	$TDS=$TDS1[$roomdetailcount];
        	 $BedTypeCode=null;

            $SmokingPreference=0;
            $Supplements=null;
            $roomdetails[]=array(
                'RoomIndex' =>$RoomIndex,
                'RoomTypeCode' =>$RoomTypeCode,
                'RoomTypeName' =>$RoomTypeName,
                'RatePlanCode' =>$RatePlanCode,
                'BedTypeCode' =>$BedTypeCode,
                'SmokingPreference' =>$SmokingPreference,
                'RoomTypeName' =>$RoomTypeName,
                'Price' =>array(
                    'CurrencyCode' =>$CurrencyCode,
                    'RoomPrice' =>$RoomPrice,
                    'RoomTypeName' =>$RoomTypeName,
                    'Tax' =>$Tax,
                    'ExtraGuestCharge' =>$ExtraGuestCharge,
                    'ChildCharge' =>$ChildCharge,
                    'OtherCharges' =>$OtherCharges,
                    'Discount' =>$Discount,
                    'PublishedPrice' =>$PublishedPrice,
                    'PublishedPriceRoundedOff' =>$PublishedPriceRoundedOff,
                    'OfferedPrice' =>$OfferedPrice,
                    'OfferedPriceRoundedOff' =>$OfferedPriceRoundedOff,
                    'AgentCommission' =>$AgentCommission,
                    'AgentMarkUp' =>$AgentMarkUp,
                    'ServiceTax' =>$ServiceTax,
                    'TDS' =>$TDS,

                )
            );


        }

       
        $form_data = array(
            'ResultIndex'=>$resultindex,
            'HotelCode'=>$hotelcode,
            'HotelName' => $hotelname,
            'GuestNationality' =>$GuestNationality,
            'NoOfRooms'=>$NoOfRooms,
            'ClientReferenceNo' => $ClientReferenceNo,
            'IsVoucherBooking' =>$IsVoucherBooking,
            'HotelRoomsDetails' => $roomdetails,
            'EndUserIp'=>$EndUserIp,
            'TokenId'=>$TokenId,
            'TraceId'=>$TraceId
        );

        $data_string = json_encode($form_data);
       
      
        $ch = curl_init('http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/BlockRoom/');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        $result1=json_decode($result,true);


        $hotelblockroomarray = !empty($result1['BlockRoomResult'])?$result1['BlockRoomResult']:array();
        $hotelmargin=$request->get('hotel_margin');
       
        $mainp=0;
        $ServiceTax=0;
        

        if(!empty($hotelblockroomarray))
        {
            if($hotelblockroomarray['Error']['ErrorCode']==0)
            {
                for($i=0;$i<count($hotelblockroomarray['HotelRoomsDetails']);$i++)
                {
                    $mainp+= $hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['RoomPrice'];
                    $maintax = $hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['Tax']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['ExtraGuestCharge']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['ChildCharge']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['OtherCharges']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['AgentCommission']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['AgentMarkUp']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['ServiceTax']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['TDS']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['TotalGSTAmount'];
                    $ServiceTax+=$maintax;



                    $margin = ($mainp * $hotelmargin)/100;
                    $mainprice = $mainp + $margin;
                     $curr=$request->get('maincurreny');
                    $price= $this->price($mainprice,$ServiceTax,0,0,$mainp,$curr);


                    $hotelmainprice = $price[0];
                    $hotelmaincurrecny = $price[1];



                    $hotelblockroomarray['hotelmainprice']=$hotelmainprice;
                    $hotelblockroomarray['hotelmaincurrecny']=$hotelmaincurrecny;
                    $hotelblockroomarray['hotelmargin']=$hotelmainprice;
                    $hotelblockroomarray['hotellastprice']=$price[7];
                    $hotelblockroomarray['hotelconvercurr']=$price[2];
                    $hotelblockroomarray['hotelservicetax']=$price[3];
                    $hotelblockroomarray['hotelmaingst']=$price[6];
                    // $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelothercharges']=$price[4];
                    // $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelgst']=$price[5];
                }
            }
        }

        echo json_encode($hotelblockroomarray);

        // return view('pages.hotel-book')->with(compact('selectedhotel'))->with(compact('hotelblockroomarray'))->with('roomindexes',$roomindex);
    }
    //endbook
    //hotel mail booking
     public function hotelbooking(Request $request)
    {
        

      
        // $formmyarray= session()->get('myhotelbooking');
        // //wit for json
        
        // parse_str($formmyarray, $formdata);

        // $paymentorder=$formdate['paymentorder'];
        // $paymentref=$formdata['paymentref'];
        $insertarray=array();
        $lead_passenger_title=$request->get('lead_passenger_title');
        $lead_passenger_first=$request->get('lead_passenger_first');
        $lead_passenger_middle=$request->get('lead_passenger_middle');
        $lead_passenger_last=$request->get('lead_passenger_last');
        $lead_passenger_age=$request->get('lead_passenger_age');
        $lead_passenger_email= $request->get('lead_passenger_email');
        $lead_passenger_contact=$request->get('lead_passenger_contact');
        $posts = tbl_payment_order::orderBy('id', 'DESC')->first();
        if(count($posts) == 0)
        {
            $orderid="TRAstt-01";
        }
        else
        {
            $chkid =  $posts->id + 1;
            $orderid="TRAstt-0".$chkid;
        }
        $create_date=date('Y-m-d');
        $create_time=date('H:i:s');
        $username=strtolower($lead_passenger_first.substr($lead_passenger_last,0,3).mt_rand(1000,9999));
       
            $checkdata=DB::table('tbl_user')->where('user_email',$lead_passenger_email)->first();
            if(count($checkdata)>0)
            {
                $user_id=$checkdata->user_id;
                $fname1=$checkdata->user_fname;
                $lname1=$checkdata->user_lname;
                $username1=$checkdata->user_name;
                $email=$checkdata->user_email;
                $fullname=$fname1." ".$lname1;
                
                $useremail=$lead_passenger_email;
            }
            else
            {
                $insertdata=array(
                    "user_fname"=>$lead_passenger_first,
                    "user_lname"=>$lead_passenger_last,
                    "user_phone"=>$lead_passenger_contact,
                    "user_email"=>$lead_passenger_email,
                    "user_name"=>$username,
                    "user_status"=>1,
                    "user_create_date"=>$create_date,
                    "user_create_time"=>$create_time);
                $insert=DB::table('tbl_user')->insert($insertdata);
                if($insert)
                {
                    $fullname=$lead_passenger_first." ".$lead_passenger_last;
                   
                    $useremail=$lead_passenger_email;
                }
            }
            $orderCurrency='INR';
            $hotelpayment = new tbl_payment_order;
            $hotelpayment->orderid=$orderid;
            $hotelpayment->orderdate=$create_date;
            $hotelpayment->ordertime=$create_time;
            $hotelpayment->loginemail=$useremail;
            $hotelpayment->save();
            $paymentorder=$orderid;
             $paymentref=$request->get('trascationid');
             


       $TokenId=$request->get('TokenId');

        $EndUserIp=$request->get('EndUserIp');
        $TraceId=$request->get('TraceId');

        $resultindex= $request->get('resultindex');
        $hotelcode=$request->get('hotelcode');
        $roomindex=explode(",",$request->get('roomindexes'));
       
        // $roomindexarray=explode('-',$roomindex);
        $hotelname=$request->get('HotelName');
        $NoOfRooms=$request->get('NoOfRooms');
        $selectedroom=array();
        $roomdetails=array();
        $passengerdetails=array();

        //newarray
        $RoomTypeCode1=explode(",",$request->get('RoomTypeCode'));
        $RoomTypeName1=explode(",",$request->get('RoomTypeName'));
        $RatePlanCode1=explode(",",$request->get('RatePlanCode'));
        $CurrencyCode1=$request->get('CurrencyCode');
        $RoomPrice1=explode(",",$request->get('RoomPrice'));
        $Tax1=explode(",",$request->get('Tax'));
        $ExtraGuestCharge1=explode(",",$request->get('ExtraGuestCharge'));
        $ChildCharge1=explode(",",$request->get('ChildCharge'));
        $OtherCharges1=explode(",",$request->get('OtherCharges'));
        $Discount1=explode(",",$request->get('Discount'));
        $PublishedPrice1=explode(",",$request->get('PublishedPrice'));
        $PublishedPriceRoundedOff1=explode(",",$request->get('PublishedPriceRoundedOff'));
        $OfferedPrice1=explode(",",$request->get('OfferedPrice'));
        $OfferedPriceRoundedOff1=explode(",",$request->get('OfferedPriceRoundedOff'));
        $AgentCommission1=explode(",",$request->get('AgentCommission'));
        $AgentMarkUp1=explode(",",$request->get('AgentMarkUp'));
        $ServiceTax1=explode(",",$request->get('ServiceTax'));
        $TDS1=explode(",",$request->get('TDS'));

        //newmargin
        $hotelmarginprice=$request->get('hotelmarginprice');
        $hotelmainprice=$request->get('hotellastprice');
        $hotelcurrecny=$request->get('hotelconvcurr');
        $hotelmargingst=$request->get('hotelmaingst');
        $hotelfaicon=$request->get('currencyicon');
        $hotelservice_tax=$request->get('hotelservicetax');
        $hotelamount =$request->get('amount');
        //endnewmargin
        
        //newnode
        $passportno=null;
    	$PassportExpDate="0001-01-01T00:00:00";
    	$pan=null;
    		 $passvalid=$request->get('lead_passvalid');
		if($passvalid=='1')
		{
		    $pan1=$request->get('lead_passenger_pancard');
		    if($pan1=="")
    		{
    		    $pan=null;
    		}
    		else
    		{
    		    $pan=$pan1;
    		}
		}
		else
		{
		    $passportno1=$request->get('lead_passenger_passport');
		    $passportdata=$request->get('lead_passenger_passport_date');
		    if($passportno=="")
    		{
    		    $passportno=null;
    		   
    		    $PassportExpDate="0001-01-01T00:00:00";
    		   
    		}
    		else
    		{
    		    $passportno=$passportno1;
    		    
    		    $PassportExpDate=$passportdata[2]."-".$passportdata[1]."-".$passportdata[0]."T00:00:00";
    		}
		}
        //endnewnode

        $gst_company_address=$request->get('gst_company_address');
        $gst_companyemail=$request->get('gst_companyemail');
        $gst_number=$request->get('gst_number');
        $gst_company_name= $request->get('gst_company_name');
        $gst_company_contact=$request->get('gst_company_contact');
        //emergency
        $eme_name=$request->get('eme_name');
        $eme_phone=$request->get('eme_phone');
        $eme_relation=$request->get('eme_relation');

        //endemergency

        $adultcount_per_room=explode(",",$request->get('adultcount_per_room'));
        $childcount_per_room= explode(",",$request->get('childcount_per_room'));
        if(!empty($request->get('room_adult_title')))
        {
            $room_adult_title=explode(",",$request->get('room_adult_title'));
            $room_adult_first=explode(",",$request->get('room_adult_first'));
            $room_adult_last= explode(",",$request->get('room_adult_last'));
            $room_adult_age=explode(",",$request->get('room_adult_age'));
        }
        if(!empty($request->get('room_child_title')))
        {
            $room_child_title=explode(",",$request->get('room_child_title'));
            $room_child_first=explode(",",$request->get('room_child_first'));
            $room_child_last=explode(",",$request->get('room_child_last'));
            $room_child_age=explode(",",$request->get('room_child_age'));
        }
       

        $adultbackindex=0;
        $childbackindex=0;
   
        for($rooms=0;$rooms< $NoOfRooms;$rooms++)
        {
          

            $adultarray=array();
            $childarray=array();
            $current_room_adult_count=$adultcount_per_room[$rooms];
           
            $current_room_child_count=$childcount_per_room[$rooms];
            for($adults=0;$adults<$current_room_adult_count;$adults++)
            {

                if($rooms==1 && $adults==0)
                {
                    $leadpassenger=true;
                }
                else
                {
                    $leadpassenger=false;
                }
               
                $adultarray[]=array("Title"=>$room_adult_title[$adultbackindex],
                    "FirstName"=>$room_adult_first[$adultbackindex],
                    "MiddleName"=>null,
                    "LastName"=>$room_adult_last[$adultbackindex],
                    "Phoneno"=> null,
                    "Email"=> null,
                    "PaxType"=>1,
                    "LeadPassenger"=>$leadpassenger,
                    "GSTCompanyAddress"=>null,
                    "GSTCompanyContactNumber"=>null,
                    "GSTCompanyEmail"=>null,
                    "GSTCompanyName"=>null,
                    "GSTNumber"=>null,
                    "Age"=>$room_adult_age[$adultbackindex],
                    "PassportNo"=> $passportno,
				    "PassportExpDate"=> $PassportExpDate,
					"PAN"=>$pan);
                $adultbackindex++;


            }
           
            for($child=0;$child<$current_room_child_count;$child++)
            {

                $childarray[]=array("Title"=>$room_child_title[$childbackindex],
                    "FirstName"=>$room_child_first[$childbackindex],
                    "MiddleName"=>null,
                    "LastName"=>$room_child_last[$childbackindex],
                    "Phoneno"=> null,
                    "Email"=> null,
                    "PaxType"=>2,
                    "LeadPassenger"=>false,
                    "GSTCompanyAddress"=>null,
                    "GSTCompanyContactNumber"=>null,
                    "GSTCompanyEmail"=>null,
                    "GSTCompanyName"=>null,
                    "GSTNumber"=>null,
                    "Age"=>$room_child_age[$childbackindex],
                    "PassportNo"=> $passportno,
				    "PassportExpDate"=> $PassportExpDate,
					"PAN"=>$pan);

                $childbackindex++;

            }
          

            if($rooms==0)
            {
                if(count($childarray)>0 && count($adultarray)>0)
                {
                    $passengerdetails[$rooms]=[array("Title"=> $lead_passenger_title,
                        "FirstName"=>$lead_passenger_first,
                        "MiddleName"=>$lead_passenger_middle,
                        "LastName"=>$lead_passenger_last,
                        "Phoneno"=>$lead_passenger_contact,
                        "Email"=>$lead_passenger_email,
                        "PaxType"=>1,
                        "LeadPassenger"=>true,
                        "GSTCompanyAddress"=>"$gst_company_address",
                        "GSTCompanyContactNumber"=>"$gst_company_contact",
                        "GSTCompanyEmail"=>"$gst_companyemail",
                        "GSTCompanyName"=>"$gst_company_name",
                        "GSTNumber"=>"$gst_number",
                        "Age"=> $lead_passenger_age,
                        "PassportNo"=> $passportno,
    				    "PassportExpDate"=> $PassportExpDate,
    					"PAN"=>$pan
                    )];
                    $adult_with_child=array_merge($adultarray,$childarray);
                    $passengerdetails[$rooms]=array_merge($passengerdetails[$rooms],$adult_with_child);
                }
                else if(count($adultarray)>0)
                {
                    $passengerdetails[$rooms]=[array("Title"=> $lead_passenger_title,
                        "FirstName"=>$lead_passenger_first,
                        "MiddleName"=>$lead_passenger_middle,
                        "LastName"=>$lead_passenger_last,
                        "Phoneno"=>$lead_passenger_contact,
                        "Email"=>$lead_passenger_email,
                        "PaxType"=>1,
                        "LeadPassenger"=>true,
                        "GSTCompanyAddress"=>"$gst_company_address",
                        "GSTCompanyContactNumber"=>"$gst_company_contact",
                        "GSTCompanyEmail"=>"$gst_companyemail",
                        "GSTCompanyName"=>"$gst_company_name",
                        "GSTNumber"=>"$gst_number",
                        "Age"=> $lead_passenger_age,
                        "PassportNo"=> $passportno,
    				    "PassportExpDate"=> $PassportExpDate,
    					"PAN"=>$pan
                    )];
                    $passengerdetails[$rooms]=array_merge($passengerdetails[$rooms],$adultarray);
                }
                else if(count($childarray)>0)
                {
                    $passengerdetails[$rooms]=[array("Title"=> $lead_passenger_title,
                        "FirstName"=>$lead_passenger_first,
                        "MiddleName"=>$lead_passenger_middle,
                        "LastName"=>$lead_passenger_last,
                        "Phoneno"=>$lead_passenger_contact,
                        "Email"=>$lead_passenger_email,
                        "PaxType"=>1,
                        "LeadPassenger"=>true,
                        "GSTCompanyAddress"=>"$gst_company_address",
                        "GSTCompanyContactNumber"=>"$gst_company_contact",
                        "GSTCompanyEmail"=>"$gst_companyemail",
                        "GSTCompanyName"=>"$gst_company_name",
                        "GSTNumber"=>"$gst_number",
                        "Age"=> $lead_passenger_age,
                        "PassportNo"=> $passportno,
    				    "PassportExpDate"=> $PassportExpDate,
    					"PAN"=>$pan
                    )];
                    $passengerdetails[$rooms]=array_merge($passengerdetails[$rooms],$childarray);
                }
                else
                {
                    $passengerdetails[$rooms]=[array("Title"=> $lead_passenger_title,
                        "FirstName"=>$lead_passenger_first,
                        "MiddleName"=>$lead_passenger_middle,
                        "LastName"=>$lead_passenger_last,
                        "Phoneno"=>$lead_passenger_contact,
                        "Email"=>$lead_passenger_email,
                        "PaxType"=>1,
                        "LeadPassenger"=>true,
                        "Age"=> $lead_passenger_age,
                        "GSTCompanyAddress"=>"$gst_company_address",
                        "GSTCompanyContactNumber"=>"$gst_company_contact",
                        "GSTCompanyEmail"=>"$gst_companyemail",
                        "GSTCompanyName"=>"$gst_company_name",
                        "GSTNumber"=>"$gst_number",
                        "PassportNo"=> $passportno,
    				    "PassportExpDate"=> $PassportExpDate,
    					"PAN"=>$pan
                    )];
                }
            }
            else
            {
                if(count($childarray)>0)
                {
                    $adult_with_child=array_merge($adultarray,$childarray);
                    $passengerdetails[$rooms]=[];
                    $passengerdetails[$rooms]=array_merge($passengerdetails[$rooms],$adult_with_child);

                }
                else
                {
                    $passengerdetails[$rooms]=[];
                    $passengerdetails[$rooms]=array_merge($passengerdetails[$rooms],$adultarray);
                }

            }

        }

    
        // if(session()->has('hotelroomsdata'))
        // {
        //     $resultarray=session()->get('hotelroomsdata');
        // }
        // for($i=0;$i<count($resultarray['HotelRoomsDetails']);$i++)
        // {
        //     for($room=0;$room<count($roomindexarray);$room++)
        //     {
        //         if($roomindexarray[$room]!='')
        //         {
        //             if($resultarray['HotelRoomsDetails'][$i]['RoomIndex']== $roomindexarray[$room])
        //             {
        //                 $selectedroom[]=$resultarray['HotelRoomsDetails'][$i];
        //             }
        //         }
        //     }
        // }
        $GuestNationality=$request->get('guest_nationality');
        
        $ClientReferenceNo="0";
        $IsVoucherBooking="true";
        if($request->has('smoking_preference'))
        {
            $SmokingPreference=2;
        }
        else
        {
            $SmokingPreference=0;
        }
        $BedTypeCode=null;
        // for($roomdetailcount=0;$roomdetailcount<$NoOfRooms;$roomdetailcount++)
        // {

        //     $RoomIndex=$roomindex[$roomdetailcount];
        //     $RoomTypeCode=$RoomTypeCode1[$roomdetailcount];
        //     $RoomTypeName=$RoomTypeName1[$roomdetailcount];
        //     $RatePlanCode=$RatePlanCode1[$roomdetailcount];
        //     $BedTypeCode=null;


        //     $Supplements=null;
        //     $CurrencyCode=$CurrencyCode1[$roomdetailcount];
        //     $RoomPrice=$RoomPrice1[$roomdetailcount];

        //     $Tax=$Tax1[$roomdetailcount];
        //     $ExtraGuestCharge=$ExtraGuestCharge1[$roomdetailcount];
        //     $ChildCharge=$ChildCharge1[$roomdetailcount];
        //     $OtherCharges=$OtherCharges1[$roomdetailcount];

        //     $Discount=$Discount1[$roomdetailcount];
        //     $PublishedPrice=$PublishedPrice1[$roomdetailcount];
        //     $PublishedPriceRoundedOff=$PublishedPriceRoundedOff1[$roomdetailcount];
        //     $OfferedPrice=$OfferedPrice1[$roomdetailcount];

        //     $OfferedPriceRoundedOff=$OfferedPriceRoundedOff1[$roomdetailcount];
        //     $AgentCommission=$AgentCommission1[$roomdetailcount];
        //     $AgentMarkUp=$AgentMarkUp1[$roomdetailcount];
        //     $ServiceTax=$ServiceTax1[$roomdetailcount];
        //     $TDS=$TDS1[$roomdetailcount];
             $NoOfRooms-1;
            for($rooms=0;$rooms< $NoOfRooms;$rooms++)
            {
            $roomdetails[]=array(
                'RoomIndex' =>$roomindex[$rooms],
                'RoomTypeCode' =>$RoomTypeCode1[$rooms],
                'RoomTypeName' =>$RoomTypeName1[$rooms],
                'RatePlanCode' =>$RatePlanCode1[$rooms],
                'BedTypeCode' =>$BedTypeCode,
                'SmokingPreference' =>$SmokingPreference,
                
                'Price' =>array(
                    'CurrencyCode' =>$CurrencyCode1,
                    'RoomPrice' =>"$RoomPrice1[$rooms]",
                    'RoomTypeName' =>$RoomTypeName1[$rooms],
                    'Tax' =>"$Tax1[$rooms]",
                    'ExtraGuestCharge' =>number_format($ExtraGuestCharge1[$rooms],1),
                    'ChildCharge' =>number_format($ChildCharge1[$rooms],1),
                    'OtherCharges' =>$OtherCharges1[$rooms],
                    'Discount' =>number_format($Discount1[$rooms],1),
                    'PublishedPrice' =>"$PublishedPrice1[$rooms]",
                    'PublishedPriceRoundedOff' =>"$PublishedPriceRoundedOff1[$rooms]",
                    'OfferedPrice' =>"$OfferedPrice1[$rooms]",
                    'OfferedPriceRoundedOff' =>"$OfferedPriceRoundedOff1[$rooms]",
                    'AgentCommission' =>"$AgentCommission1[$rooms]",
                    'AgentMarkUp' =>number_format($AgentMarkUp1[$rooms],1),
                    'ServiceTax' =>"$ServiceTax1[$rooms]",
                    'TDS' =>"$TDS1[$rooms]",

                ),
                'HotelPassenger'=>$passengerdetails[$rooms]
            );

        }
      
        $form_data = array(
            'ResultIndex'=>$resultindex,
            'HotelCode'=>$hotelcode,
            'HotelName' => $hotelname,
            'GuestNationality' =>$GuestNationality,
            'NoOfRooms'=>$NoOfRooms,
            'ClientReferenceNo' => $ClientReferenceNo,
            'IsVoucherBooking' =>$IsVoucherBooking,
            'HotelRoomsDetails' => $roomdetails,
            'EndUserIp'=>$EndUserIp,
            'TokenId'=>$TokenId,
            'TraceId'=>$TraceId
        );

        $data_string = json_encode($form_data);
       
        
        $ch = curl_init('http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/Book/');


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        $result1=json_decode($result,true);
       

        //erroshow pending
        $errorcode = $result1['BookResult']['Error']['ErrorCode'];
        $errormsg = $result1['BookResult']['Error']['ErrorMessage'];
        $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode,'response_msg' => $errormsg]);
        if($result1['BookResult']['Error']['ErrorCode']!=0)
        {
           
           
            $datass['status']=$errormsg;
            echo json_encode($datass);
            // echo "<script>alert('".$errormsg."');</script>";
            // return redirect()->intended('/index');
        }
        $hotelbookingarray = !empty($result1['BookResult'])?$result1['BookResult']:array();

        // die();
        $bookingid=$hotelbookingarray['BookingId'];
        //  echo "<pre>";
        //  echo "<br>";
        //  print_r($result1);
        // $bookingid=1478154;
        $form_data1 = array(
            'EndUserIp'=>$EndUserIp,
            'TokenId'=>$TokenId,
            'BookingId'=>"$bookingid"
        );

        $data_string = json_encode($form_data1);

        $ch = curl_init('http://api.tektravels.com/BookingEngineService_Hotel/HotelService.svc/rest/GetBookingDetail/');


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        $result1=json_decode($result,true);
       
        $errorcode = $result1['GetBookingDetailResult']['Error']['ErrorCode'];
        $errormsg = $result1['GetBookingDetailResult']['Error']['ErrorMessage'];
        $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode,'response_msg' => $errormsg]);
        if($result1['GetBookingDetailResult']['Error']['ErrorCode']!=0)
        {
             $datass['status']=$errormsg;
            echo json_encode($datass);
           
            
        }
        $hotelbookdetailsarray = !empty($result1['GetBookingDetailResult'])?$result1['GetBookingDetailResult']:array();
        if($hotelbookdetailsarray['Error']['ErrorCode']==0)
        {
            $bookingid=$hotelbookdetailsarray['BookingId'];
            $hotel_booking_status=$hotelbookdetailsarray['HotelBookingStatus'];
            $confirmation_no=$hotelbookdetailsarray['ConfirmationNo'];
            $booking_ref_no=$hotelbookdetailsarray['BookingRefNo'];
            $booking_date=$hotelbookdetailsarray['BookingDate'];
            $total_adult_count=count($request->get('adultcount_per_room'));
            $total_child_count=count($request->get('childcount_per_room'));
            $lead_name=$hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['FirstName']." ".$hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['LastName'];
            $lead_email=$hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['Email'];
            $lead_mobile=$hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['Phoneno'];
            $lead_pax=$hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['PaxType'];
            $lead_title=$hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['Title'];
            $hotel_rooms_details=serialize($hotelbookdetailsarray['HotelRoomsDetails']);
            $invoice_no=$hotelbookdetailsarray['InvoiceNo'];
            $invoice_date_time=explode('T',$hotelbookdetailsarray['InvoiceCreatedOn']);
            $invoice_date=$invoice_date_time[0];
            $invoice_time=$invoice_date_time[1];
            $invoice_amount=$hotelbookdetailsarray['InvoiceAmount'];
            $hotel_name=$hotelbookdetailsarray['HotelName'];
            
            // if(session()->has('resultarray'))
            // {
            //     $resultarray=session()->get('resultarray');
            //     for($i=0;$i<count($resultarray['HotelResults']);$i++)
            //     {
            //         if($resultarray['HotelResults'][$i]['HotelName']==$hotelbookname)
            //         {
            //             $currenthotel=$resultarray['HotelResults'][$i];
            //         }
            //     }
            // }
            $hotelbookname=$request->get('HotelName');;
            $hotel_picture=$request->get('HotelPicture');
            $hotel_star=$request->get('StarRating');
            $hotel_address1=$request->get('AddressLine1');
            $hotel_address2=$request->get('AddressLine2');
            $hotel_countrycode=$request->get('CountryCode');

            $hotel_latitude=$request->get('Latitude');
            $hotel_longitude=$request->get('Longitude');
            $hotel_city=$request->get('City');
            // $checkin_datetime=explode('T',$hotelbookdetailsarray['CheckInDate']);
             $check_in_date=$request->get('CheckInDate');
            // $checkout_datetime=explode('T',$hotelbookdetailsarray['CheckOutDate']);
            $check_out_date=$request->get('CheckOutDate');
            $no_of_rooms=$request->get('NoOfRooms');
            $special_request= $request->get('SpecialRequest');
            $is_domestic=$request->get('IsDomestic');
            $hotel_policy_detail=$request->get('HotelPolicyDetail');
            $hotel_confirmation_no=$request->get('HotelConfirmationNo');
            $whole_response=serialize($hotelbookdetailsarray);
            date_default_timezone_set("Asia/Kolkata");
            $dateime=date('Y-m-d H:i:s');
            $create_date=date('Y-m-d');
            $create_time=date('H:i:s');
            $username='';
            $user_id='';
            $userphone='';
            $useremail="";
            // if(session()->has('travo_username'))
            // {
            //     $username=session()->get('travo_username');
            // }

            // if(session()->has('travo_userid'))
            // {
            //     $user_id=session()->get('travo_userid');
            // }
            // if(session()->has('travo_userphone'))
            // {
            //     $userphone=session()->get('travo_userphone');
            // }

            // $useremail=session()->get('travo_useremail');

            $bookingdate1 = explode('T',$booking_date);
            $bookindate_new=$bookingdate1[0];
            $bookindate_time=$bookingdate1[1];

            $insertarray=array("bookingid"=>$bookingid,
                "hotel_booking_status"=>$hotel_booking_status,
                "confirmation_no"=>$confirmation_no,
                "booking_ref_no"=>$booking_ref_no,
                "booking_date"=>$bookindate_new,
                "booking_time"=>$bookindate_time,
                "total_adult_count"=>$total_adult_count,
                "total_child_count"=>$total_child_count,

                "lead_name"=>$lead_name,
                "lead_email"=>$lead_email,
                "lead_mobile"=>$lead_mobile,
                "lead_pax"=>$lead_pax,
                "lead_title"=>$lead_title,

                "hotel_rooms_details"=>$hotel_rooms_details,

                "invoice_no"=>$invoice_no,
                "invoice_date"=>$invoice_date,
                "invoice_time"=>$invoice_time,
                "invoice_amount"=>$invoice_amount,

                "hotel_name"=>$hotel_name,
                "hotel_picture"=>$hotel_picture,
                "hotel_star"=>$hotel_star,
                "hotel_address1"=>$hotel_address1,
                "hotel_address2"=>$hotel_address2,
                "hotel_countrycode"=>$hotel_countrycode,
                "hotel_latitude"=>$hotel_latitude,
                "hotel_longitude"=>$hotel_longitude,
                "hotel_city"=>$hotel_city,

                "check_in_date"=>$check_in_date,
                "check_out_date"=>$check_out_date,

                "no_of_rooms"=>$no_of_rooms,
                "special_request"=>$special_request,
                "is_domestic"=>$is_domestic,
                "hotel_policy_detail"=>$hotel_policy_detail,
                "hotel_confirmation_no"=>$hotel_confirmation_no,

                "whole_response"=>$whole_response,
                "login_id"=>$user_id,
                "login_username"=>$username,
                "login_guest_email"=>$useremail,
                "create_date"=>$create_date,
                "create_time"=>$create_time,
                "guestphone"=>$userphone,
                "order_id"=>$paymentorder,
                "order_ref"=>$paymentref,
                "hotelmarginprice"=>$hotelmarginprice,

                "hotelmainprice"=>$hotelmainprice,
                "hotelcurrecny"=>$hotelcurrecny,
                "hotelmargingst"=>$hotelmargingst,
                "hotelservice_tax"=>$hotelservice_tax,
                "hotelfaicon"=>$hotelfaicon,
                "hotelamount"=>$hotelamount,
                "eme_name"=>$eme_name,
                "eme_phone"=>$eme_phone,
                "eme_relation"=>$eme_relation,


            );

        }

        $insertquery=tbl_hotel_book::insert($insertarray);



        if($insertquery)
        {
            
            $id = DB::getPdo()->lastInsertId();
            // $request->session()->put('bookingid',$bookingid);
            $mytrip = new tbl_mytrip;
            $mytrip->triptype='Hotel';
            $mytrip->bookingid=$bookingid;

            $mytrip->tableid=$id;
            $mytrip->booking_date=$check_in_date;
            $mytrip->booking_time="00:00:00";
            $mytrip->trip_date=$create_date;
            $mytrip->trip_time=$create_time;
            $mytrip->login_id=$user_id;
            $mytrip->login_username=$username;
            $mytrip->login_guest_email=$useremail;
            $mytrip->guestphone=$userphone;
            $mytrip->save();
            $paymenttable = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['ticket_id' => $id,
                'payment_code'=>'Hotel',
            ]);
            //smsunit
            // $chdata = session()->get('postData');
            $lead_passenger_title=$request->get('lead_passenger_title');
        $lead_passenger_first=$request->get('lead_passenger_first');
        $lead_passenger_middle=$request->get('lead_passenger_middle');
        $lead_passenger_last=$request->get('lead_passenger_last');
        $lead_passenger_age=$request->get('lead_passenger_age');
        $lead_passenger_email= $request->get('lead_passenger_email');
        $lead_passenger_contact=$request->get('lead_passenger_contact');

            $customerName = $request->get('lead_passenger_first');
            $orderCurrency=$request->get('CurrencyCode');
            $customerPhone = $lead_passenger_contact;
            $customerEmail =  $lead_passenger_email;
            // $hotelsms=Cookie::get('gensetting');
            // $hl = explode("-", $hotelsms->hotel_sms_active);
            // $sms_user=$hotelsms->sms_user_id;
            // $sms_password=$hotelsms->sms_password;
            // $sms_sender=$hotelsms->sms_sender;
            // $sms_url=$hotelsms->sms_url;
            // if($hl[1]=='1')
            // {

            //     $leadphone1=$customerPhone;
            //     $user=$sms_user;
            //     $password=$sms_password;
            //     $sender=$sms_sender;

            //     $text="Dear ".$customerName.",Your hotel ".$hotel_name." booking has been confirmed. booking Id: ".$bookingid.", No of room ".$no_of_rooms.", Total Member (".$total_adult_count.",".$total_child_count."). Check your email for more details. -Travo Web.";
            //     $otp=urlencode("$text");
            //     $data = array(
            //         'usr' => $user,
            //         'pwd' => $password,
            //         'ph'  => $leadphone1,
            //         'sndr'  => $sender,
            //         'text'  => $otp
            //     );
            //     $ch = curl_init($sms_url);
            //     curl_setopt($ch, CURLOPT_POST, true);
            //     curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            //     curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
            //     curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            //     $result = curl_exec($ch);
            // }

            // echo "<script>alert('Successfully booked')</script>";
            // echo "Successfully booked your bookind id ".$bookingid;
            $datass['status']="success";
            $datass['bookingid']=$bookingid;
            echo json_encode($datass);
           // print_r($data_string);
        die();
        }
        else
        {
            // echo "<script>Error while booking</script>";
          
          $datass['status']="nodata";
            echo json_encode($datass);
        }



    }
    public function apihotelinvoice(Request $request)
    {
        $booking_id=$request->get('bookingid');
        $getdata=tbl_hotel_book::where("bookingid",$booking_id)->first();
        $hotelbookdetailsarray=unserialize($getdata->whole_response);

        $booking_creation_date_time=$getdata->created_at;
        $hotelpicture=$getdata->hotel_picture;
        $hotelamount=$getdata->hotelamount;
        $hotelfaicon=$getdata->hotelfaicon;
        $hotelmargingst=$getdata->hotelmargingst;
        $hotelservice_tax=$getdata->hotelservice_tax;
        $hotelmarginprice=$getdata->hotelmarginprice;

        $hotelbookdetailsarray['hotelmarginprice']=$hotelmarginprice;
        $hotelbookdetailsarray['created_on']=$booking_creation_date_time;
        $hotelbookdetailsarray['hotelpicture']=$hotelpicture;
        $hotelbookdetailsarray['hotelamount']=$hotelamount;
        $hotelbookdetailsarray['hotelfaicon']=$hotelfaicon;
        $hotelbookdetailsarray['hotelmargingst']=$hotelmargingst;
        $hotelbookdetailsarray['hotelservice_tax']=$hotelservice_tax;
        echo json_encode($hotelbookdetailsarray);
    }
    //hotel end booking
     public function apihotelinvoicedownload(Request $request)
    {
        $booking_id=$request->get('bookingid');
        $getdata=tbl_hotel_book::where("bookingid",$booking_id)->first();
        $hotelbookdetailsarray=unserialize($getdata->whole_response);

        $booking_creation_date_time=$getdata->created_at;
        $hotelpicture=$getdata->hotel_picture;
        $hotelamount=$getdata->hotelamount;
        $hotelfaicon=$getdata->hotelfaicon;
        $hotelmargingst=$getdata->hotelmargingst;
        $hotelservice_tax=$getdata->hotelservice_tax;
        $hotelmarginprice=$getdata->hotelmarginprice;

        $hotelbookdetailsarray['hotelmarginprice']=$hotelmarginprice;
        $hotelbookdetailsarray['created_on']=$booking_creation_date_time;
        $hotelbookdetailsarray['hotelpicture']=$hotelpicture;
        $hotelbookdetailsarray['hotelamount']=$hotelamount;
        $hotelbookdetailsarray['hotelfaicon']=$hotelfaicon;
        $hotelbookdetailsarray['hotelmargingst']=$hotelmargingst;
        $hotelbookdetailsarray['hotelservice_tax']=$hotelservice_tax;
        // echo json_encode($hotelbookdetailsarray);
        $pdf = PDF::loadView('pages.hotel_pdf',compact('hotelbookdetailsarray'));
        $pdfsave="hotelpdf/".$booking_id.".pdf";
       return $pdf->stream('booking.pdf');
    }


	public function price($amount,$servicetax,$tdsa,$gsta,$mainp,$curr)
    {
    
      $pricecurrency = 'fa-inr';  //$ fa-usd
    
       $maincurrency=$curr;

       if($maincurrency == 'IN'){
           $priceamount = $amount;
           $pservicetax = $servicetax;
           $ptds=$tdsa;
           $pgsta=$gsta;
           $pricecurr = 'IN';
           $gsthotel=($amount*5)/100;
           $mprice=$mainp;

       }
       else
       {
           if($maincurrency == 'AU' || $maincurrency == 'NZ'){
            
               // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'AUD')->first();
               // if(!empty($dataExsists)){
                  
               //     $dataExsists->amount = $this->convert('AUD','1');
               //     $dataExsists->save();
               // }
               $price = Price::where('currency', '=', 'AUD')->first();
               $priceamount = ($price->amount*$amount)*100;
               $pservicetax = ($price->amount*$servicetax)*100;
               $ptds=($price->amount*$tdsa)*100;
               $pgsta=($price->amount*$gsta)*100;
               $gsthotel1=($amount*10)/100;
               $gsthotel = ($price->amount*$gsthotel1)*100;
              
               $mprice = ($price->amount*$mainp)*100;
               $pricecurr = 'AU';
           }
           else
           {
               // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'USD')->first();
               // if(!empty($dataExsists)){
               //     //$price = Price::where('currency', '=', 'USD')->first();
               //     $dataExsists->amount = $this->convert('USD','1');
               //     $dataExsists->save();
               // }
               $price = Price::where('currency', '=', 'USD')->first();
               //echo $price->amount; die;
               $priceamount = ($price->amount*$amount)*100;
               $pservicetax = ($price->amount*$servicetax)*100;
               $ptds=($price->amount*$tdsa)*100;
               $pgsta=($price->amount*$gsta)*100;
               $gsthotel1=($amount*10)/100;
               $gsthotel = ($price->amount*$gsthotel1)*100;
               $mprice = ($price->amount*$mainp)*100;
               $pricecurr = 'US';
           }
           $pricecurrency = 'fa-usd';
       }
       //echo $priceamount;  die;
      return [$priceamount,$pricecurrency,$pricecurr,$pservicetax,$ptds,$pgsta,$gsthotel,$mprice];
    }
    
    public function get_current_location(Request $request){
        //return "USD";
        if(Cookie::get('country')!== null){
            return Cookie::get('country');
         }else
         {
             $ip=$request->get('ip');
            $url = "http://www.geoplugin.net/php.gp?ip=".$ip;
             $request = curl_init();
             $timeOut = 0;
             curl_setopt ($request, CURLOPT_URL, $url);
             curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
             curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
             curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
             $response = curl_exec($request);

             curl_close($request);
             if(!empty($response)){
                 $response = unserialize($response); 
                 if(!empty($response['geoplugin_status']) && $response['geoplugin_status'] == '200')
                 {
                     Cookie::queue('country',$response['geoplugin_countryCode'], 1440);

                 return $response['geoplugin_countryCode'];

                  echo json_encode($response);
                 }
                 else{
                     Cookie::queue('country','IN', 1440);
                     return "  ";
                 }
             }
             else{
                 Cookie::queue('country','IN', 1440);
                 return "IN";
             }
         }
         
         
        //$loc = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
        //print_r($response); die;
    }
    public function request_hotel(Request $request)
    {
        $posts = tbl_payment_order::orderBy('id', 'DESC')->first();
        if(count($posts) == 0)
        {
            $orderid="TRAstt-01";
        }
        else
        {
            $chkid =  $posts->id + 1;
            $orderid="TRAstt-0".$chkid;
        }
    }

     public function ios_get_current_location(Request $request){
        //return "USD";

    
        if(Cookie::get('country')!== null){
            return Cookie::get('country');
            $newval=Cookie::get('country');
             $result = array('response'=>0,'status'=>'success','message'=>$newval);
         }else
         {
             $ip=$request->get('ip');
            $url = "http://www.geoplugin.net/php.gp?ip=".$ip;
             $request = curl_init();
             $timeOut = 0;
             curl_setopt ($request, CURLOPT_URL, $url);
             curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
             curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
             curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
             $response = curl_exec($request);

             curl_close($request);
             if(!empty($response)){
                 $response = unserialize($response); 
                 if(!empty($response['geoplugin_status']) && $response['geoplugin_status'] == '200')
                 {
                     Cookie::queue('country',$response['geoplugin_countryCode'], 1440);
                      $result = array('response'=>0,'status'=>'success','message'=>$response['geoplugin_countryCode']);
                 
               

                  // echo json_encode($response);
                 }
                 else{
                     Cookie::queue('country','IN', 1440);
              
                     $result = array('response'=>1,'status'=>'success','message'=>"IN");

                 }
             }
             else{
                 Cookie::queue('country','IN', 1440);
               
                 $result = array('response'=>1,'status'=>'success','message'=>"IN");
             }
            
         }
         
          echo  json_encode($result);
        //$loc = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
        //print_r($response); die;
    }
   
}
