<?php
namespace App\Http\Controllers;
use session;
use DB;
use Cookie;
use App\flight;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\tbl_flight_book;
use PDF;
use App;
use Dompdf\Dompdf;
use App\tbl_payment_order;
use App\tbl_mytrip;
use File;
use Mail;
use App\Mail\SendMailable;
use App\Price;
use App\tbl_isocountry;
class FlightBookController extends Controller
{
    public $pnr;
    public function __construct()
    {
        date_default_timezone_set('Asia/Kolkata');
        $pnr='';
        $this->pnr=$pnr;
    }
    public function flightbook(Request $request)
    {
        $flightorign=$request->get('flightorign');
        $flightdep=$request->get('flightdep');
        $flightname=$request->get('flightname');
        echo $modal_show ='
	    <!doctype html>
                       <html lang="en">
                       <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                       
                                    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
                                                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                                    <title>TravoWeb</title>
                       <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">             
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>

        .l-img{
            width: 165px;
            display: block;
            margin: auto;
        }
        .s-note,p.request-p {
            text-align: center;
            color: #381967;
            padding-bottom: 10px;
            font-size: 20px;
            font-weight: 500;
            font-family: calibri;
        }
        .c-in1 {
            background: #381967;
            text-align: center;
            color: white;
            font-size: 18px;
            font-weight: 500;
        }

        .loadernew {
            font-size: 8px;
            margin: 0px auto 30px auto;
            width: 11em;
            height: 11em;
            border-radius: 50%;
            background: #ffffff;
            border-top: 6px solid #fa9e1b;
            border-left: 6px solid #fa9e1b;
            border-right: 6px solid #fa9e1b;
            position: relative;
            animation: load3 1.4s linear infinite;
        }



        @keyframes load3 {
            0% {

                transform: rotate(0deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;

            }
            10%{
                transform: rotate(36deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            20%{
                transform: rotate(72deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            30%{
                transform: rotate(108deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            40% {
                transform: rotate(144deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            50% {

                transform: rotate(180deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;

            }
            60%{
                transform: rotate(216deg);

                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            70%{
                transform: rotate(252deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            80%{
                transform: rotate(288deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            90% {

                transform: rotate(324deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            100% {

                transform: rotate(360deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
        }
        body{
            margin: 0;
            background: transparent;
        }


        div.modal-content.lc{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
      /* p.g-fl:after {
            position: relative;
            content: "\\f072";
            font-family: FontAwesome;
            font-weight: 900;
            color: #FF904B;
            left: 47%;
            transform: rotate(45deg) translateX(-50%);
            top: -18.3%; 
            bottom: -3px;
            font-size: 29px;
        }*/
     img.fl-img {
    height: 31.1px;
    position: absolute;
    top: 48% !important;
    left: 48%;
}
        .multi-div{
            overflow: visible !important;
        }
        
        @media screen and (max-width:992px){
.modal-content.lc {
    width: 43% !important;
}
}

@media screen and (max-width:775px){
.modal-content.lc {
    width: 50% !important;
}
} 
@media screen and (max-width: 650px){
            .modal-content.lc {
    padding-top: 50px !important;
    width: 90% !important;
    box-shadow: none !important;
}
         
        } 
    </style>
                       </head>
                       <body>
      <div class="modal fade myModal" id="myModal1" style="height: 100%;font-family: \'Open Sans\', sans-serif;background: aliceblue; margin: 0;box-sizing: border-box;">

                            <div class="modal-dialog " style="">
                            <div class="modal-content lc" style="background:white;padding-top:60px;width:32%; height:auto;margin:0 auto;border:none;border-radius:7px;box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.2)">
                                <div class="modal-header" style="border: none;padding-bottom: 0">
                                      <img src="'.asset('assets/images/logo.png').'" class="l-img">
                                 </div>
                                  <div class="modal-body">
                                  <p class="request-p">Please Do Not Close Or Refresh The Window While We Are Searching The Best Flights For You</p>
                                    
                                    <div class="row" style="width:100%">
                                        <div class="c-in1" style="width:50%;display:block;float:left;">

                                            <p style="margin: 0; padding: 11px;font-size: 16px;font-weight: 500;" class="g-fl">'.$flightorign.'</p>
                                        </div>
                                        <div class="c-in1" style="width:50%;display:block;float:left;margin-left: -1px;">
                                            <p style="margin: 0; padding: 11px;font-size: 16px;font-weight: 500;">'.$flightdep.'</p>
                                        </div>
                                            <img src="'.asset('assets/images/airplane 1.png').'" class="fl-img">
                                    </div>

                                   <p style="font-size: 17px;font-weight: 600;padding-top: 60px;text-align: center; margin-top: 20px;" class="p-wait">Please Wait...</p>

                                </div>
                                <div class="loadernew"></div>
                            </div>
                                  
                            </div>
                          </div>
                          <script>
                $(document).ready(function(){
                  
                  $("#myModal1").modal("show");
                  });
            </script>
             </body>
                       </html>';
        $pricechange_return="";
        if(session()->has('flighttraceid'))
        {
            $tcknid=$request->get('_token');
            $ResultIndex=$request->get('resultindex');
            $ResultIndex1=$request->get('resultindex1');
            $journytype=$request->get('journytype');
            $TraceId = session()->get('flighttraceid');
            $flightcabinclass=$request->get('flightcabinclass');
            $TokenId=Cookie::get('TokenId');
            $EndUserIp=Cookie::get('localip');
            $form_data = array(
                'EndUserIp'=>$EndUserIp,
                'TokenId'=>$TokenId,
                'TraceId'=>$TraceId,
                'ResultIndex'=>$ResultIndex,
            );
            $data_string = json_encode($form_data);
            $ch = curl_init('https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/FareQuote/');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );
            $result = curl_exec($ch);

            //inbound index
            $form_data_return = array(
                'EndUserIp'=>$EndUserIp,
                'TokenId'=>$TokenId,
                'TraceId'=>$TraceId,
                'ResultIndex'=>$ResultIndex1,
            );
            $data_string_return = json_encode($form_data_return);

            $ch = curl_init('https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/FareQuote/');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_return);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string_return))
            );
            $result_return = curl_exec($ch);


            // $resu_return[]='';
            if(!empty($result_return))
            {
                $result1_return=json_decode($result_return,true);
                $pricechange_return=$result1_return['Response']['IsPriceChanged'];
                $ch = curl_init('https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/SSR/');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_return);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string_return))
                );

                $mealresult_return = curl_exec($ch);
                $mealresult1_return=json_decode($mealresult_return,true);
                $meal_return = !empty($mealresult1_return['Response'])?$mealresult1_return['Response']:array();
                if(!empty($meal_return['MealDynamic']))
                {
                    $mealmarginpr=Cookie::get('gensetting');
                    $mealpmargin = $mealmarginpr->flight_meal_margin;
                    for($mealc=0;$mealc<count($meal_return['MealDynamic'][0]);$mealc++)
                    {
                        $dmealprice = $meal_return['MealDynamic'][0][$mealc]['Price'];
                        $totaldmealprice = ($dmealprice * $mealpmargin) /100;
                        $cmealprice=$dmealprice + $totaldmealprice;
                        $mprice= $this->mealprice($cmealprice,$dmealprice);
                        $meal_return['MealDynamic'][0][$mealc]['mealmainprice']=round($mprice[2]);
                        $meal_return['MealDynamic'][0][$mealc]['mealmarginprice']=round($cmealprice);
                        $meal_return['MealDynamic'][0][$mealc]['mealfprice']=round($mprice[0]);
                        $meal_return['MealDynamic'][0][$mealc]['mealcur']=$mprice[1];


                    }
                }
                if(!empty($meal_return['Baggage']))
                {
                    for($bag=0;$bag<count($meal_return['Baggage'][0]);$bag++)
                    {
                        $bagmarginpr=Cookie::get('gensetting');
                        $bagmargin = $bagmarginpr->flight_bag_margin;

                        $bagmainprice = $meal_return['Baggage'][0][$bag]['Price'];
                        $totalbagprice = ($bagmainprice * $bagmargin) /100;
                        $bagmarginprice=$bagmainprice + $totalbagprice;
                        $mprice= $this->mealprice($bagmarginprice,$bagmainprice);
                        $meal_return['Baggage'][0][$bag]['bagmainprice']=round($mprice[2]);
                        $meal_return['Baggage'][0][$bag]['bagmarginprice']=round($bagmarginprice);
                        $meal_return['Baggage'][0][$bag]['bagfprice']=round($mprice[0]);
                        $meal_return['Baggage'][0][$bag]['bagcur']=$mprice[1];


                    }
                }
                foreach($result1_return as $flightretrun)
                {
                    $resu_return = !empty($flightretrun['Results'])?$flightretrun['Results']:array();
                    if(!empty($resu_return))
                    {
                        $flightmargin1=Cookie::get('gensetting');

                        $flightmargin = $flightmargin1->flight_margin;
                        $othervharges = $resu_return['Fare']['OtherCharges'] + $resu_return['Fare']['TdsOnCommission'] + $resu_return['Fare']['TdsOnPLB'] + $resu_return['Fare']['TdsOnIncentive'] + $resu_return['Fare']['ServiceFee'];
                        $taxs=$resu_return['Fare']['Tax'];

                        $mainp=$resu_return['Fare']['BaseFare'];

                        $margin = ($mainp * $flightmargin)/100;
                        $mainprice = $mainp + $margin;

                        $price= $this->price($mainprice,$taxs,$othervharges,$mainp);
                        $resu_return['Fare']['rflightmainprice']=$price[5];
                        $resu_return['Fare']['rflightmmargin']=$mainprice;
                        $resu_return['Fare']['rflightfare']=$price[0];
                        $resu_return['Fare']['rflighttax']=$price[2];
                        $resu_return['Fare']['rflightcurrency']=$price[1];
                        $resu_return['Fare']['rflightothercharges']=$price[3];
                        $resu_return['Fare']['rflightconvr']=$price[4];
                    }


                }



            }

            //endinbound index

            $result1=json_decode($result,true);
            if($result1['Response']['Error']['ErrorCode']!='0')
            {
                $message = $result1['Response']['Error']['ErrorMessage'];
                echo "<script>alert('".$message."');</script>";
                return redirect()->intended('/index');
                die();
            }
            else
            {


                $pricechangeoneway=$result1['Response']['IsPriceChanged'];

                //mealcode and seat code
                $ch = curl_init('https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/SSR/');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );

                $mealresult = curl_exec($ch);
                $mealresult1=json_decode($mealresult,true);
                $meal = !empty($mealresult1['Response'])?$mealresult1['Response']:array();
                if(!empty($meal['MealDynamic']))
                {
                    $mealmarginpr=Cookie::get('gensetting');
                    $mealpmargin = $mealmarginpr->flight_meal_margin;
                    for($mealc=0;$mealc<count($meal['MealDynamic'][0]);$mealc++)
                    {
                        $dmealprice = $meal['MealDynamic'][0][$mealc]['Price'];
                        $totaldmealprice = ($dmealprice * $mealpmargin) /100;
                        $cmealprice=$dmealprice + $totaldmealprice;
                        $mprice= $this->mealprice($cmealprice,$dmealprice);
                        $meal['MealDynamic'][0][$mealc]['mealmainprice']=round($mprice[2]);
                        $meal['MealDynamic'][0][$mealc]['mealmarginprice']=round($cmealprice);
                        $meal['MealDynamic'][0][$mealc]['mealfprice']=round($mprice[0]);
                        $meal['MealDynamic'][0][$mealc]['mealcur']=$mprice[1];


                    }
                }
                if(!empty($meal['Baggage']))
                {
                    for($bag=0;$bag<count($meal['Baggage'][0]);$bag++)
                    {
                        $bagmarginpr=Cookie::get('gensetting');
                        $bagmargin = $bagmarginpr->flight_bag_margin;

                        $bagmainprice = $meal['Baggage'][0][$bag]['Price'];
                        $totalbagprice = ($bagmainprice * $bagmargin) /100;
                        $bagmarginprice=$bagmainprice + $totalbagprice;
                        $mprice= $this->mealprice($bagmarginprice,$bagmainprice);
                        $meal['Baggage'][0][$bag]['bagmainprice']=round($mprice[2]);
                        $meal['Baggage'][0][$bag]['bagmarginprice']=round($bagmarginprice);
                        $meal['Baggage'][0][$bag]['bagfprice']=round($mprice[0]);
                        $meal['Baggage'][0][$bag]['bagcur']=$mprice[1];


                    }
                }
                //end mealcode
                foreach($result1 as $flightbook)
                {
                    $resu = !empty($flightbook['Results'])?$flightbook['Results']:array();
                    $totalbasefare=0;
                    if(!empty($resu))
                    {
                        $flightmargin1=Cookie::get('gensetting');

                        $flightmargin = $flightmargin1->flight_margin;
                        $othervharges = $resu['Fare']['OtherCharges'] + $resu['Fare']['TdsOnCommission'] + $resu['Fare']['TdsOnPLB'] + $resu['Fare']['TdsOnIncentive'] + $resu['Fare']['ServiceFee'];
                        $taxs=$resu['Fare']['Tax'];

                        $mainp=$resu['Fare']['BaseFare'];

                        $margin = ($mainp * $flightmargin)/100;
                        $mainprice = $mainp + $margin;

                        $price= $this->price($mainprice,$taxs,$othervharges,$mainp);
                        $resu['Fare']['flightmainprice']=$price[5];
                        $resu['Fare']['flightmmargin']=$mainprice;
                        $resu['Fare']['flightfare']=$price[0];
                        $resu['Fare']['flighttax']=$price[2];
                        $resu['Fare']['flightcurrency']=$price[1];
                        $resu['Fare']['flightothercharges']=$price[3];
                        $resu['Fare']['flightconvr']=$price[4];
                        $resu['Fare']['curencycode']=Cookie::get('currencycode');


                    }

                    for($seg_i=0;$seg_i<count($flightbook['Results']['Segments'][0]);$seg_i++)
                    {


                        $airportdename1= $flightbook['Results']['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'];
                        $airportdeptname =  $flightbook['Results']['Segments'][0][$seg_i]['Destination']['Airport']['AirportName'];
                        $airportdeptcityname =  $flightbook['Results']['Segments'][0][$seg_i]['Destination']['Airport']['CityName'];


                    }
                }
                $airlinename =  $flightbook['Results']['Segments'][0][0]['Airline']['AirlineName'];
                $airportorgincode=  $result1['Response']['Results']['Segments'][0][0]['Origin']['Airport']['AirportCode'];
                $airportorignname =  $result1['Response']['Results']['Segments'][0][0]['Origin']['Airport']['AirportName'];
                $airportorigncityname =  $result1['Response']['Results']['Segments'][0][0]['Origin']['Airport']['CityName'];
                $airportorigndepdate =  $result1['Response']['Results']['Segments'][0][0]['Origin']['DepTime'];
                $mainarray =array(
                    'airportorgincode'=>$airportorgincode,
                    'airportorignname' =>$airportorignname,
                    'airportorigncityname'=>$airportorigncityname,
                    'airportdeptname'=>$airportdeptname,
                    'airportdeptcityname'=>$airportdeptcityname,
                    'airportdename1'=>$airportdename1,
                    'airportorigndepdate'=>$airportorigndepdate,
                    'journytype' =>$journytype,
                    'flightcabinclass'=>$flightcabinclass,
                    'pricechangereturn'=>$pricechange_return,
                    'pricechangeoneway'=>$pricechangeoneway,
                    'airlinename'=>$airlinename,


                );
               
                echo '<script>
                    $(document).ready(function(){
                      
                      $("#myModal1").modal("hide");
                      });
                </script>';
               
                return view('pages.flight-book')->with(compact('resu'))->with(compact('mainarray'))->with(compact('meal'))->with(compact('resu_return'))->with(compact('meal_return'));
            } //error close


        } //session close
        else
        {
            echo "<script>alert('Session');</script>";
            return view('pages.index');
        }

    } //flightbook close
    public function flight_bookview(Request $request)
    {
        $flightorign=session()->get('flightorignn');
        $flightdep=session()->get('flightdepn');
        $flightname=session()->get('flightnamen');
        echo $modal_show ='  <!doctype html>
                       <html lang="en">
                       <head>
                       
                                    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
                                                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                                    <title>TravoWeb</title>
                                          <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>

        .l-img{
            width: 165px;
            display: block;
            margin: auto;
        }
        .s-note,p.request-p {
            text-align: center;
            color: #381967;
            padding-bottom: 10px;
            font-size: 20px;
            font-weight: 500;
            font-family: calibri;
        }
        .c-in1 {
            background: #381967;
            text-align: center;
            color: white;
            font-size: 18px;
            font-weight: 500;
        }

        .loadernew {
            font-size: 8px;
            margin: 0px auto 30px auto;
            width: 11em;
            height: 11em;
            border-radius: 50%;
            background: #ffffff;
            border-top: 6px solid #fa9e1b;
            border-left: 6px solid #fa9e1b;
            border-right: 6px solid #fa9e1b;
            position: relative;
            animation: load3 1.4s linear infinite;
        }



        @keyframes load3 {
            0% {

                transform: rotate(0deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;

            }
            10%{
                transform: rotate(36deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            20%{
                transform: rotate(72deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            30%{
                transform: rotate(108deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            40% {
                transform: rotate(144deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            50% {

                transform: rotate(180deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;

            }
            60%{
                transform: rotate(216deg);

                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            70%{
                transform: rotate(252deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            80%{
                transform: rotate(288deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            90% {

                transform: rotate(324deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            100% {

                transform: rotate(360deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
        }
        body{
            margin: 0;
            background: transparent;
        }


        div.modal-content.lc{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
       /*p.g-fl:after {
            position: relative;
            content: "\\f072";
            font-family: FontAwesome;
            font-weight: 900;
            color: #FF904B;
            left: 47%;
            transform: rotate(45deg) translateX(-50%);
             top: -18.3%;
            bottom: -3px;
            font-size: 29px;
        }*/
     img.fl-img {
    height: 31.1px;
    position: absolute;
    top: 48% !important;
    left: 48%;
}
        .multi-div{
            overflow: visible !important;
        }
        
        @media screen and (max-width:992px){
.modal-content.lc {
    width: 43% !important;
}
}

@media screen and (max-width:775px){
.modal-content.lc {
    width: 50% !important;
}
} 
@media screen and (max-width: 650px){
            .modal-content.lc {
    padding-top: 50px !important;
    width: 90% !important;
    box-shadow: none;
}
         
        } 
    </style>
                       </head>
                       <body>
      <div class="modal fade myModal" id="myModal1" style="height: 100%;font-family: \'Open Sans\', sans-serif;background: aliceblue; margin: 0;box-sizing: border-box;">

                            <div class="modal-dialog " style="">
                            <div class="modal-content lc" style="background:white;padding-top:60px;width:32%; height:auto;margin:0 auto;border:none;border-radius:7px;box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.2)">
                                <div class="modal-header" style="border: none;padding-bottom: 0">
                                      <img src="'.asset('assets/images/logo.png').'" class="l-img">
                                 </div>
                                  <div class="modal-body">
                                  <p class="request-p">Please Do Not Close Or Refresh The Window While We Are Searching The Best Flights For You</p>
                                    
                                    <h4 class="s-note">Please do not refresh or reload page</h4>
                                    <div class="row" style="width:100%">
                                        <div class="c-in1" style="width:50%;display:block;float:left;">

                                            <p style="margin: 0; padding: 11px;font-size: 16px;font-weight: 500;" class="g-fl">'.$flightorign.'</p>
                                        </div>
                                        <div class="c-in1" style="width:50%;display:block;float:left;margin-left: -1px;">
                                            <p style="margin: 0; padding: 11px;font-size: 16px;font-weight: 500;">'.$flightdep.'</p>
                                        </div>
                                            <img src="'.asset('assets/images/airplane 1.png').'" class="fl-img">
                                    </div>

                                   <p style="font-size: 17px;font-weight: 600;padding-top: 60px;text-align: center; margin-top: 20px;" class="p-wait">Please Wait...</p>

                                </div>
                                <div class="loadernew"></div>
                            </div>
                                  
                            </div>
                          </div>
                          <script>
                $(document).ready(function(){
                  
                  $("#myModal1").modal("show");
                  });
            </script>
             </body>
                       </html>';
        if(session()->has("paymentstatus")!='paymentsuccess')
        {
            echo "<script>alert('Some Technical Problem Please contact on Administrator ');</script>";
        }
        else
        {
            $paymentorder = session()->get('paymentorder');
            $paymentref = session()->get('paymentref');


            $formmyarray= session()->get('myflightbooking');
            parse_str($formmyarray, $formdata);

            $tcknid= $formdata['_token'];
            $TraceId = session()->get('flighttraceid');
            $TokenId=Cookie::get('TokenId');
            $EndUserIp=Cookie::get('localip');
            $journytype= $formdata['journytype'];
            $titleadult= $formdata['titleadult'];
            $firstnameadult=$formdata['firstnameadult'];
            $lastnameadult= $formdata['lastnameadult'];
            $adultdob_date= $formdata['adultdob'];
            $eme_name=$formdata['eme_name'];
            $ema_phone=$formdata['eme_phone'];
            $eme_relation=$formdata['eme_relation'];
            $ff_name1=$formdata['ff_name'];
            $ff_number1=$formdata['ff_number'];
            if($ff_number1=="")
            {
                $ff_name="";
                $ff_number="";
            }
            else
            {
                $ff_name=$formdata['ff_name'];
                $ff_number=$formdata['ff_number'];
            }
            $passportcheck=$formdata['passportcheck'];
        

            $flightcabinclass= $formdata['flightcabinclass'];
            $childfirstname=array();
            $childcount=0;
            if(!empty($formdata['childtitle']))
            {
                $childtitle=$formdata['childtitle'];
                $childfirstname= $formdata['childfirstname'];
                $childlastname=$formdata['childlastname'];
                $childdob_date=$formdata['childdob'];
                $childbasefare=$formdata['childbasefare'];
                $childtaxfare= $formdata['childtaxfare'];
                $childtxnfeepub= $formdata['childtxnfeepub'];
                $childtxnfeeofrd= $formdata['childtxnfeeofrd'];
                $childcount=count($childfirstname);
            }
            $infacount=0;
            if(!empty($formdata['infatitle']))
            {
                $infatitle= $formdata['infatitle'];
                $infafistname= $formdata['infafistname'];
                $infalastname= $formdata['infalastname'];
                $infadob_date= $formdata['infadob'];
                $infantbasefare=$formdata['infantbasefare'];
                $infanttaxfare= $formdata['infanttaxfare'];
                $infanttxnfeepub= $formdata['infanttxnfeepub'];
                $infanttxnfeeofrd= $formdata['infanttxnfeeofrd'];
                $infacount=count($infafistname);
            }

            $AddressLine1=$formdata['address1'];
            $City=  $formdata['city'];
            $mealname= $formdata['totalmealname'];
            $mealprice=$formdata['totalmealprice'];
            $meal_orgprice=$formdata['totalorgmealprice'];

            $flightlastprice =$formdata['flightbaseprice'];
            $flightmarginprice =$formdata['flightbasemarginprice'];
            $flighttaxprice=$formdata['flighttaxprice'];
            $flightotherprice=$formdata['flightotherprice'];
            $convertcurrecny=$formdata['convertcurrecny'];
            $currencyicon=$formdata['flightcurrencyicon'];
            //return
            $mealname_return= $formdata['totalmealname_return'];
            $mealprice_return=$formdata['totalmealprice_return'];
            $meal_orgprice_return=$formdata['totalorgmealprice_return'];


            $flightlastprice_return =$formdata['flightbaseprice_r'];
            $flightmarginprice_return =$formdata['flightbasemarginprice_r'];
            $flighttaxprice_return=$formdata['flighttaxprice_r'];
            $flightotherprice_return=$formdata['flightotherprice_r'];

            $bag_weight=$formdata['bagweight'];
            $bag_price=$formdata['newbagprice'];
            $bag_orgprice=$formdata['orgbagprice'];
            //return
            $bag_weight_return=$formdata['bagweight_return'];
            $bag_price_return=$formdata['newbagprice_return'];
            $bag_orgprice_return=$formdata['orgbagprice_return'];
            $grandprice= $formdata['maingradflightprice'];
            if($formdata['country'] !='')
            {
                $countrycde = explode('%%',$formdata['country']);
                $CountryCode= $countrycde[0];
                $CountryName=$countrycde[1];
            }

            $Email= $formdata['email'];
            $refund=$formdata['refund'];
            $ContactNo=$formdata['phone'];
            $gstaddress= $formdata['gstaddress'];
            $gstemail= $formdata['gstemail'];
            $gstnumber= $formdata['gstnumber'];
            $gstcompname= $formdata['gstcompname'];
            $gstcontactno= $formdata['gstcontactno'];
            // $mealdynamic= $formdata['mealdynamic'];
            // $baggage= $formdata['baggage'];
            // $meal= $formdata['meal'];




            $lcccheck= $formdata['lcccheck'];

            $ResultIndex=$formdata['resultindex'];

            //fareruledata
            $adultbasefare= $formdata['adultbasefare'];
            $adulttaxfare=$formdata['adulttaxfare'];

            $adulttxnfeeofrd=number_format($formdata['adulttxnfeeofrd'],1);
            $adulttxnfeepub=number_format($formdata['adulttxnfeepub'],1);
            $Currency= $formdata['Currency'];
            $YQTax=number_format($formdata['YQTax'],1);
            $OtherCharges=number_format($formdata['OtherCharges'],1);
            $Discount=number_format($formdata['Discount'],1);
            $PublishedFare= $formdata['PublishedFare'];
            $OfferedFare=number_format($formdata['OfferedFare'],1);
            $TdsOnCommission=number_format( $formdata['TdsOnCommission'],1);
            $TdsOnPLB=number_format($formdata['TdsOnPLB'],1);
            $TdsOnIncentive=number_format($formdata['TdsOnIncentive'],1);
            $ServiceFee=number_format($formdata['ServiceFee'],1);
            //bagage

            $baggage=$formdata['baggage'];
            if($baggage !='')
            {

                $baggage1=explode('%',$baggage);
                $WayType = $baggage1[2];
                $bagCode = $baggage1[3];
                $bagDescription = $baggage1[4];
                $Weight = $baggage1[0];
                $Price = $baggage1[1];
                $Origin = $baggage1[5];
                $Destination = $baggage1[6];
                $bagflightnumber=$baggage1[10];
                $bagflightcode=$baggage1[9];

            }
            else
            {
                $WayType = "";
                $bagCode = "";
                $bagDescription = "";
                $Weight = "";
                $Price = "";
                $Origin ="";
                $Destination = "";
                $bagflightcode="";
                $bagflightnumber="";

            }
            //meal
            if(!empty($formdata['mealdynamic']))
            {
                $mealdynamic=$formdata['mealdynamic'];
                if($mealdynamic !='')
                {
                    $mealdyn= explode('%',$mealdynamic);
                    $Code=$mealdyn[0];
                    $Description=$mealdyn[1];
                    $mealway=$formdata['mealway'];
                     $mealflightname=$formdata['mealflightname'];
                     $mealflightcode=$formdata['mealflightcode'];
                     $mealquantaty=$formdata['mealquantaty'];
                }
            }
            else
            {
                $Code="";
                $Description="";
                $mealway="";
                $mealflightname="";
                $mealflightcode="";
                $mealquantaty="";
            }




            //return fare
            $resultindex_return='';
            if(!empty($formdata['resultindex_return']))
            {
                $ff_name_return1=$formdata['ff_name_return'];
                $ff_number_return1=$formdata['ff_number_return'];
                if($ff_number_return1=="")
                {
                    $ff_name_return="";
                    $ff_number_return="";
                }
                else
                {
                    $ff_name_return=$formdata['ff_name_return'];
                    $ff_number_return=$formdata['ff_number_return'];
                }
                $lcccheck_return= $formdata['lcccheck_return'];
                $resultindex_return= $formdata['resultindex_return'];
                $adultbasefarereturn= $formdata['adultbasefarereturn'];
                $adulttaxfarereturn=$formdata['adulttaxfarereturn'];
                $adulttxnfeeofrdreturn= $formdata['adulttxnfeeofrdreturn'];
                $adulttxnfeepubreturn= $formdata['adulttxnfeepubreturn'];
                if(!empty($formdata['childbasefarereturn']))
                {
                    $childbasefarereturn= $formdata['childbasefarereturn'];
                    $childtaxfarereturn= $formdata['childtaxfarereturn'];
                    $childtxnfeeofrdreturn=  $formdata['childtxnfeeofrdreturn'];
                    $childtxnfeepubreturn=$formdata['childtxnfeepubreturn'];
                }
                $infacount=0;
                if(!empty($formdata['infatitle']))
                {

                    $infantbasefarereturn=$formdata['infantbasefarereturn'];
                    $infanttaxfarereturn= $formdata['infanttaxfarereturn'];
                    $infanttxnfeepubreturn= $formdata['infanttxnfeepubreturn'];
                    $infanttxnfeeofrdreturn= $formdata['infanttxnfeeofrdreturn'];
                    $infacount=count($infafistname);
                }

                $Currency_return= $formdata['Currency_return'];
                $YQTax_return=$formdata['YQTax_return'];
                $OtherCharges_return= $formdata['OtherCharges_return'];

                $Discount_return=$formdata['Discount_return'];
                $PublishedFare_return=$formdata['PublishedFare_return'];
                $OfferedFare_return=$formdata['OfferedFare_return'];
                $TdsOnCommission_return=$formdata['TdsOnCommission_return'];
                $TdsOnPLB_return=$formdata['TdsOnPLB_return'];

                $TdsOnIncentive_return= $formdata['TdsOnIncentive_return'];
                $ServiceFee_return=$formdata['ServiceFee_return'];
            }

            //end api hit
            $adultcount=count($firstnameadult);
            $username='';
            $user_id='';
            $userphone='';
            if(session()->has('travo_username'))
            {
                $username=session()->get('travo_username');
            }

            if(session()->has('travo_userid'))
            {
                $user_id=session()->get('travo_userid');
            }
            if(session()->has('travo_userphone'))
            {
                $userphone=session()->get('travo_userphone');
            }
            $useremail=session()->get('travo_useremail');
            // $userphone=session()->get('travo_userphone');
            $usercustname = $firstnameadult[0].' '.$lastnameadult[0];
            if($lcccheck=='1')
            {
                $totaladult=count($firstnameadult);
            $adultbasefare_new=$adultbasefare / $totaladult;
            $adulttaxfare_new=$adulttaxfare/$totaladult;

                for($adult=0;$adult<count($firstnameadult);$adult++)
                {
                    if($titleadult[$adult]=='Mr')
                    {
                        $adultgender="1";
                    }
                    else
                    {
                        $adultgender="2";
                    }
                    if($adult=='0')
                    {
                        $paxval="true";
                       
                    }
                    else
                    {
                        $paxval="false";
                       
                    }
                    if($passportcheck==1)
                    {
                        $PassportNo=$formdata['passportno'][$adult];
                        $passportexp=explode('/',$formdata['passportexp'][$adult]);
                        $PassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $PassportNo="";
                        $PassportExpiry="";
                    }
                    $adultdob1=explode('/',$adultdob_date[$adult]);
                    $adultdob = $adultdob1[2]."-".$adultdob1[1]."-".$adultdob1[0]."T00:00:00";
                    if($journytype=='3') //multicity api not show bagage and meal
                    {
                        $addultpassengerdetail[]=array(
                            'Title' =>$titleadult[$adult],
                            'FirstName'=>$firstnameadult[$adult],
                            'LastName'=>$lastnameadult[$adult],
                            'PaxType' =>'1',
                            'DateOfBirth'=>$adultdob,
                            "PassportNo"=> $PassportNo,
                            "PassportExpiry"=> $PassportExpiry,
                            'Gender'=>$adultgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,

                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> $paxval,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$adultbasefare_new,
                                "Tax"=> $adulttaxfare_new,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $adulttxnfeeofrd,
                                "AdditionalTxnFeePub"=> $adulttxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],
                        );
                    }
                    else
                    {
                        $addultpassengerdetail[]=array(
                            'Title' =>$titleadult[$adult],
                            'FirstName'=>$firstnameadult[$adult],
                            'LastName'=>$lastnameadult[$adult],
                            'PaxType' =>'1',
                            'DateOfBirth'=>$adultdob,
                            'Gender'=>$adultgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=> $PassportNo,
                            "PassportExpiry"=>  $PassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> $paxval,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$adultbasefare_new,
                                "Tax"=> $adulttaxfare_new,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $adulttxnfeeofrd,
                                "AdditionalTxnFeePub"=> $adulttxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],
                            "Baggage"=>[array(
                                                     "AirlineCode"=>$bagflightcode,
                                                    "FlightNumber"=>$bagflightnumber,
                                                    "WayType"=>$WayType,
                                                    "Code"=> $bagCode,
                                                    "Description"=>$bagDescription,
                                                    "Weight"=> $Weight,
                                                    "Currency"=> $Currency,
                                                    "Price"=>$Price,
                                                    "Origin"=> $Origin,
                                                    "Destination"=> $Destination,
                            )],
                            "Meal Dynamic"=>[array(
                               "AirlineCode"=>$mealflightcode,
                                                        "FlightNumber"=>$mealflightname,
                                                        "WayType"=>$mealway,
                                                        "Code"=> $Code,
                                                        "Description"=> $Description,
                                                        "AirlineDescription"=>$mealname,
                                                        "Quantity"=>$mealquantaty,
                                                        "Currency"=> $Currency,
                                                        "Price"=>$meal_orgprice,
                                                        "Origin"=> $Origin,
                                                        "Destination"=> $Destination,
                            )],


                        );
                    } // else condtion journy time close




                }
                if(count($childfirstname)!="0")
                {

               
                    $totalchild=count($childfirstname);
                    $childfare_new=$childbasefare / $totalchild;
                    $childtaxfare_new=$childtaxfare/$totalchild;
                }
                for($child=0;$child<count($childfirstname);$child++)
                {
                    if($childtitle[$child]=='Mr')
                    {
                        $childgender="1";
                    }
                    else
                    {
                        $childgender="2";
                    }
                     if($passportcheck==1)
                    {
                        $childPassportNo=$formdata['childpassportno'][$child];
                        $passportexp=explode('/',$formdata['childpassportexp'][$child]);
                        $childPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $childPassportNo="";
                        $childPassportExpiry="";
                    }
                    $childdob1=explode('/',$childdob_date[$child]);
                    $childdob = $childdob1[2]."-".$childdob1[1]."-".$childdob1[0]."T00:00:00";
                    if($journytype=='3') //multicity api not show bagage and meal
                    {
                        $addultpassengerdetail[]=array(
                            'Title' =>$childtitle[$child],
                            'FirstName'=>$childfirstname[$child],
                            'LastName'=>$childlastname[$child],
                            'PaxType' =>'2',
                            'DateOfBirth'=>$childdob,
                            'Gender'=>$childgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=>$childPassportNo,
                            "PassportExpiry"=>  $childPassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> false,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$childfare_new,
                                "Tax"=> $childtaxfare_new,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $childtxnfeeofrd,
                                "AdditionalTxnFeePub"=> $childtxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],
                        );

                    }
                    else
                    {
                        $addultpassengerdetail[]=array(
                            'Title' =>$childtitle[$child],
                            'FirstName'=>$childfirstname[$child],
                            'LastName'=>$childlastname[$child],
                            'PaxType' =>'2',
                            'DateOfBirth'=>$childdob,
                            'Gender'=>$childgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=> $childPassportNo,
                            "PassportExpiry"=>  $childPassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> false,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$childfare_new,
                                "Tax"=> $childtaxfare_new,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $childtxnfeeofrd,
                                "AdditionalTxnFeePub"=> $childtxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],
                            "Baggage"=>[array(
                                "AirlineCode"=>$bagflightcode,
                                "FlightNumber"=>$bagflightnumber,
                                "WayType"=>$WayType,
                                "Code"=> $bagCode,
                                "Description"=>$bagDescription,
                                "Weight"=> $Weight,
                                "Currency"=> $Currency,
                                "Price"=>$Price,
                                "Origin"=> $Origin,
                                "Destination"=> $Destination,
                            )],
                            "Meal Dynamic"=>[array(
                                "AirlineCode"=>$mealflightcode,
                                "FlightNumber"=>$mealflightname,
                                "WayType"=>$mealway,
                                "Code"=> $Code,
                                "Description"=> $Description,
                                "AirlineDescription"=>$mealname,
                                "Quantity"=>$mealquantaty,
                                "Currency"=> $Currency,
                                "Price"=>$meal_orgprice,
                                "Origin"=> $Origin,
                                "Destination"=> $Destination,
                            )],


                        );
                    } //multicity if close

                }
                if($infacount !="0")
                {
                    $totalinfant=count($infacount);
                    $infantfare_new=$infantbasefare / $totalinfant;
                    $infanttax_new=$infanttaxfare / $totalinfant;
                }
                
                for($infa=0;$infa<$infacount;$infa++)
                {
                    if($infatitle[$infa]=='Mstr')
                    {
                        $infagender="1";
                    }
                    else
                    {
                        $infagender="2";
                    }
                      if($passportcheck==1)
                    {
                        $infatPassportNo=$formdata['infantpassportno'][$infa];
                        $passportexp=explode('/',$formdata['infantpassportexp'][$infa]);
                        $infatPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $infatPassportNo="";
                        $infatPassportExpiry="";
                    }
                    $infadob1=explode('/',$infadob_date[$infa]);
                    $infadob = $infadob1[2]."-".$infadob1[1]."-".$infadob1[0]."T00:00:00";
                    if($journytype=='3') //multicity api not show bagage and meal
                    {
                        $addultpassengerdetail[]=array(
                            'Title' =>$infatitle[$infa],
                            'FirstName'=>$infafistname [$infa],
                            'LastName'=>$infalastname[$infa],
                            'PaxType' =>'3',
                            'DateOfBirth'=>$infadob,
                            'Gender'=>$infagender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=> $infatPassportNo,
                            "PassportExpiry"=>  $infatPassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> false,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$infantfare_new,
                                "Tax"=> $infanttax_new,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $infanttxnfeeofrd,
                                "AdditionalTxnFeePub"=> $infanttxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],
                        );

                    }
                    else
                    {
                        $addultpassengerdetail[]=array(
                            'Title' =>$infatitle[$infa],
                            'FirstName'=>$infafistname [$infa],
                            'LastName'=>$infalastname[$infa],
                            'PaxType' =>'3',
                            'DateOfBirth'=>$infadob,
                            'Gender'=>$infagender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=>$infatPassportNo,
                            "PassportExpiry"=> $infatPassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> false,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$infantfare_new,
                                "Tax"=> $infanttax_new,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $infanttxnfeeofrd,
                                "AdditionalTxnFeePub"=> $infanttxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],

                            "Meal Dynamic"=>[array(
                               "AirlineCode"=>$mealflightcode,
                                "FlightNumber"=>$mealflightname,
                                "WayType"=>$mealway,
                                "Code"=> $Code,
                                "Description"=> $Description,
                                "AirlineDescription"=>$mealname,
                                "Quantity"=>$mealquantaty,
                                "Currency"=> $Currency,
                                "Price"=>$meal_orgprice,
                                "Origin"=> $Origin,
                                "Destination"=> $Destination,
                            )],


                        );
                    } //multicity infa if close

                }

                $form_data = array(
                    'EndUserIp'=>$EndUserIp,
                    'TokenId'=>$TokenId,
                    'TraceId'=>$TraceId,
                    'ResultIndex'=>$ResultIndex,
                    'Passengers'=>$addultpassengerdetail,
                );
                $data_string = json_encode($form_data);
                
                $ch = curl_init('https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Ticket/');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );

                $result = curl_exec($ch);
                $resultticket=json_decode($result,true);
               
                $errorcode = $resultticket['Response']['Error']['ErrorCode'];
                $errormsg = $resultticket['Response']['Error']['ErrorMessage'];
                $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode,'response_msg' => $errormsg]);
                if($resultticket['Response']['Error']['ErrorCode']!='0')
                {

                    $message = $resultticket['Response']['Error']['ErrorMessage'];
                    echo "<script>alert('".$message."');</script>";
                    if($resultticket['Response']['Error']['ErrorCode']=='6' )
                    {
                        return redirect()->intended('/index');
                    }
                    else if($resultticket['Response']['Error']['ErrorCode']=='3' )
                    {
                        return redirect()->intended('/flightbook');
                    }
                    else
                    {
                        return redirect()->intended('/index');
                    }
                    die();
                }
                else
                {
                    $PNR=$resultticket['Response']['Response']['PNR'];
                    $BookingId=$resultticket['Response']['Response']['BookingId'];
                    $status=$resultticket['Response']['Response']['TicketStatus'];
                    $flightstatus='LCC';


                }
            }
            else
            {
                if(!empty($formdata['meal']))
                {
                    $meal=$formdata['meal'];
                    if($meal !='')
                    {
                        $meal1= explode('%',$meal);
                        $Codemeal=$meal1[0];
                        $Descriptionmeal=$meal1[1];
                    }
                }
                else
                {
                    $Codemeal="";
                    $Descriptionmeal="";
                }
                $totaladult=count($firstnameadult);
            $adultfare_new1=$adultbasefare / $totaladult;
            $adulttax_new1=$adulttaxfare / $totaladult;
            $YQTax_new1=$YQTax/$totaladult;
                //book method
                for($adult=0;$adult<count($firstnameadult);$adult++)
                {
                    if($titleadult[$adult]=='Mr')
                    {
                        $adultgender="1";
                    }
                    else
                    {
                        $adultgender="2";
                    }
                    if($adult=='0')
                    {
                        $paxval="true";
                    }
                    else
                    {
                        $paxval="false";
                    }
                    if($passportcheck==1)
                    {
                        $PassportNo=$formdata['passportno'][$adult];
                        $passportexp=explode('/',$formdata['passportexp'][$adult]);
                        $PassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $PassportNo="";
                        $PassportExpiry="";
                    }
                    $adultdob1=explode('/',$adultdob_date[$adult]);
                    $adultdob = $adultdob1[2]."-".$adultdob1[1]."-".$adultdob1[0]."T00:00:00";

                    $addultpassengerdetaillcc[]=array(
                        'Title' =>$titleadult[$adult],
                        'FirstName'=>$firstnameadult[$adult],
                        'LastName'=>$lastnameadult[$adult],
                        'PaxType' =>'1',
                        'DateOfBirth'=>$adultdob,
                        'Gender'=>$adultgender,
                        "GSTCompanyAddress"=> $gstaddress,
                        "GSTCompanyContactNumber"=> $gstcontactno,
                        "GSTCompanyName"=> $gstcompname,
                        "GSTNumber"=> $gstnumber,
                        "GSTCompanyEmail"=> $gstemail,
                        "PassportNo"=> $PassportNo,
                        "PassportExpiry"=> $PassportExpiry,
                        "AddressLine1"=> $AddressLine1,
                        "AddressLine2"=> "",
                        "City"=> $City,
                        "CountryCode"=> $CountryCode,
                        "CountryName"=> $CountryName,
                        "Nationality"=> $CountryCode,
                        "ContactNo"=> $ContactNo,
                        "Email"=> $Email,
                        "IsLeadPax"=> $paxval,
                        "FFAirline"=> $ff_name,
                        "FFNumber"=> $ff_number,
                        "Fare"=>[array(
                            "BaseFare"=>$adultfare_new1,
                            "Tax"=> $adulttax_new1,
                            "YQTax"=> $YQTax_new1,
                            "AdditionalTxnFeeOfrd"=> $adulttxnfeeofrd,
                            "AdditionalTxnFeePub"=> $adulttxnfeepub,
                            "AirTransFee"=> "0.0",
                        )],




                    );
                }
                if(count($childfirstname) !="0")
                {
                    $totalchild=count($childfirstname);
                    $childfare_new1=$childbasefare / $totalchild;
                    $childtax_new1=$childtaxfare / $totalchild;
                    $YQTax_new12=$YQTax/$totalchild;
                }
                
                for($child=0;$child<count($childfirstname);$child++)
                {
                    if($childtitle[$child]=='Mr')
                    {
                        $childgender="1";
                    }
                    else
                    {
                        $childgender="2";
                    }
                     if($passportcheck==1)
                    {
                        $childPassportNo=$formdata['childpassportno'][$child];
                        $passportexp=explode('/',$formdata['childpassportexp'][$child]);
                        $childPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $childPassportNo="";
                        $childPassportExpiry="";
                    }
                    $childdob1=explode('/',$childdob_date[$child]);
                    $childdob = $childdob1[2]."-".$childdob1[1]."-".$childdob1[0]."T00:00:00";
                    $addultpassengerdetaillcc[]=array(
                        'Title' =>$childtitle[$child],
                        'FirstName'=>$childfirstname[$child],
                        'LastName'=>$childlastname[$child],
                        'PaxType' =>'2',
                        'DateOfBirth'=>$childdob,
                        'Gender'=>$childgender,
                        "GSTCompanyAddress"=> $gstaddress,
                        "GSTCompanyContactNumber"=> $gstcontactno,
                        "GSTCompanyName"=> $gstcompname,
                        "GSTNumber"=> $gstnumber,
                        "GSTCompanyEmail"=> $gstemail,
                         "PassportNo"=> $childPassportNo,
                        "PassportExpiry"=> $childPassportExpiry,
                        "AddressLine1"=> $AddressLine1,
                        "AddressLine2"=> "",
                        "City"=> $City,
                        "CountryCode"=> $CountryCode,
                        "CountryName"=> $CountryName,
                        "Nationality"=> $CountryCode,
                        "ContactNo"=> $ContactNo,

                        "Email"=> $Email,
                        "IsLeadPax"=> false,
                        "FFAirline"=> $ff_name,
                        "FFNumber"=> $ff_number,
                        "Fare"=>[array(
                            "BaseFare"=>$childfare_new1,
                            "Tax"=> $childtax_new1,
                            "YQTax"=> $YQTax_new12,
                            "AdditionalTxnFeeOfrd"=> $childtxnfeeofrd,
                            "AdditionalTxnFeePub"=> $childtxnfeepub,
                            "AirTransFee"=> "0.0",
                        )],


                    );
                } //childforloopclose
                if($infacount !="0")
                {
                    $totalinfant=count($infacount);
                    $infantfare_new1=$infantbasefare / $infacount;
                    $infanttax_new1=$infanttaxfare / $infacount;
                    $YQTax_new11=$YQTax/$totalinfant;
                }
                
                for($infa=0;$infa<$infacount;$infa++)
                {
                    if($infatitle[$infa]=='Mstr')
                    {
                        $infagender="1";
                    }
                    else
                    {
                        $infagender="2";
                    }
                      if($passportcheck==1)
                    {
                        $infatPassportNo=$formdata['infantpassportno'][$infa];
                        $passportexp=explode('/',$formdata['infantpassportexp'][$infa]);
                        $infatPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $infatPassportNo="";
                        $infatPassportExpiry="";
                    }
                    $infadob1=explode('/',$infadob_date[$infa]);
                    $infadob = $infadob1[2]."-".$infadob1[1]."-".$infadob1[0]."T00:00:00";
                    $addultpassengerdetaillcc[]=array(
                        'Title' =>$infatitle[$infa],
                        'FirstName'=>$infafistname [$infa],
                        'LastName'=>$infalastname[$infa],
                        'PaxType' =>'3',
                        'DateOfBirth'=>$infadob,
                        'Gender'=>$infagender,
                        "GSTCompanyAddress"=> $gstaddress,
                        "GSTCompanyContactNumber"=> $gstcontactno,
                        "GSTCompanyName"=> $gstcompname,
                        "GSTNumber"=> $gstnumber,
                        "GSTCompanyEmail"=> $gstemail,
                        "PassportNo"=> $infatPassportNo,
                        "PassportExpiry"=> $infatPassportExpiry,
                        "AddressLine1"=> $AddressLine1,
                        "AddressLine2"=> "",
                        "City"=> $City,
                        "CountryCode"=> $CountryCode,
                        "CountryName"=> $CountryName,
                        "Nationality"=> $CountryCode,
                        "ContactNo"=> $ContactNo,
                        "Email"=> $Email,
                        "IsLeadPax"=> false,
                        "FFAirline"=> $ff_name,
                        "FFNumber"=> $ff_number,

                        "Fare"=>[array(
                            "BaseFare"=>$infantfare_new1,
                            "Tax"=> $infanttax_new1,
                            "YQTax"=> $YQTax_new11,
                            "AdditionalTxnFeeOfrd"=> $infanttxnfeeofrd,
                            "AdditionalTxnFeePub"=> $infanttxnfeepub,
                            "AirTransFee"=> "0.0",
                        )],


                    );
                } //infaforloopclose
                $form_data = array(
                    'EndUserIp'=>$EndUserIp,
                    'TokenId'=>$TokenId,
                    'TraceId'=>$TraceId,
                    'ResultIndex'=>$ResultIndex,
                    'Passengers'=>$addultpassengerdetaillcc,
                );
                $data_string_lcc = json_encode($form_data);
                
                $ch = curl_init('https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Book/');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_lcc);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string_lcc))
                );

                $result = curl_exec($ch);
                
                $result1=json_decode($result,true);
                $errorcode1 = $result1['Response']['Error']['ErrorCode'];
                $errormsg1 = $result1['Response']['Error']['ErrorMessage'];
                $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1]);
                if($result1['Response']['Error']['ErrorCode']!='0')
                {

                    $message = $result1['Response']['Error']['ErrorMessage'];
                    echo "<script>alert('".$message."');</script>";
                    if($result1['Response']['Error']['ErrorCode']=='6')
                    {
                        return redirect()->intended('/index');
                    }
                    else if($result1['Response']['Error']['ErrorCode']=='3')
                    {
                        return redirect()->intended('/flightbook');
                    }
                    else
                    {
                        return redirect()->intended('/index');
                    }
                    die();
                }
                else
                {

                    $PNR=$result1['Response']['Response']['PNR'];
                    $BookingId=$result1['Response']['Response']['BookingId'];
                    $status=$result1['Response']['Response']['FlightItinerary']['Status'];
                    $flightstatus='Non Lcc';
                    // echo $paxid = $result1['Response']['FlightItinerary']['Passenger'][0]['PaxId'];
                    //cheackagain
                    //  for($adult=0;$adult<count($firstnameadult);$adult++)
                    //      	{
                    //      		if($titleadult[$adult]=='1')
                    //    	{
                    //    		$adultgender="1";
                    //    	}
                    //    	else
                    //    	{
                    //    		$adultgender="2";
                    //    	}
                    //    	$adultdob1=explode('/',$adultdob_date[$adult]);
                    //     			$adultdob = $adultdob1[2]."-".$adultdob1[1]."-".$adultdob1[0]."T00:00:00";

                    //     			$addultpassengerdetaillcc1[]=array(

                    //     											'PaxType' =>$paxid,
                    //     											'DateOfBirth'=>$adultdob,
                    //     											"PassportNo"=> $PassportNo,
                    // 						            "PassportExpiry"=> $PassportExpiry,
                    // 						          );
                    // }
                    //again ticket call is non lcc flight
                    $form_data_ticket = array(
                        'EndUserIp'=>$EndUserIp,
                        'TokenId'=>$TokenId,
                        'TraceId'=>$TraceId,
                        'PNR'=>$PNR,
                        'BookingId'=>$BookingId,

                    );
                    $data_string_ticket = json_encode($form_data_ticket);

                    $ch = curl_init('https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Ticket/');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_ticket);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($data_string_ticket))
                    );

                    $result_new = curl_exec($ch);
                    $resultticket_new=json_decode($result_new,true);


                    // echo "<hr>";

                    $errorcode1 = $resultticket_new['Response']['Error']['ErrorCode'];
                    $errormsg1 = $resultticket_new['Response']['Error']['ErrorMessage'];
                    $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1]);
                    if($result1['Response']['Error']['ErrorCode']!='0')
                    {
                        echo "<script>alert('".$errormsg1."');</script>";
                        return redirect()->intended('/index');
                    }
                    // end call non lcc flight

                }
            } //if loop close

            if($status=='1')
            {
                $form_data_book = array(
                    'EndUserIp'=>$EndUserIp,
                    'TokenId'=>$TokenId,
                    'TraceId'=>$TraceId,
                    'BookingId'=>$BookingId,
                    'PNR' =>$PNR,
                );
                $data_string_book = json_encode($form_data_book);


                $ch = curl_init('https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/GetBookingDetails/');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_book);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string_book))
                );

                $result_book = curl_exec($ch);

                $result_book1=json_decode($result_book,true);
              
                 $errorcode1 = $result_book1['Response']['Error']['ErrorCode'];
                 $errormsg1 = $result_book1['Response']['Error']['ErrorMessage'];
                $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1,'chkpnr'=>$PNR]);
               
                if($result_book1['Response']['Error']['ErrorCode']!=0)
                {
                    echo "<script>alert(".$errormsg1.");</script>";
                    return redirect()->intended('/index');
                }
                else
                {



                    $flightdata1=$result_book1['Response'];
                    $flightdata = serialize($flightdata1);
                    $domestic=$result_book1['Response']['FlightItinerary']['IsDomestic'];
                    $orign=$result_book1['Response']['FlightItinerary']['Origin'];
                    $destination=$result_book1['Response']['FlightItinerary']['Destination'];
                    $lcc=$result_book1['Response']['FlightItinerary']['IsLCC'];
                    $fareType=$result_book1['Response']['FlightItinerary']['FareType'];
                    $fare=$result_book1['Response']['FlightItinerary']['Fare']; //array
                    $segment=$result_book1['Response']['FlightItinerary']['Segments']; //array
                    $airlinecode = $result_book1['Response']['FlightItinerary']['Segments'][0]['Airline']['AirlineCode'];
                    $flightno = $result_book1['Response']['FlightItinerary']['Segments'][0]['Airline']['FlightNumber'];
                    $passengerdetail=$result_book1['Response']['FlightItinerary']['Passenger']; //array
                    $passengerfname=$result_book1['Response']['FlightItinerary']['Passenger'][0]['FirstName'];
                    $passengerlname=$result_book1['Response']['FlightItinerary']['Passenger'][0]['LastName'];
                    $passengername =$passengerfname.' '.$passengerlname;
                    $passengercity=$result_book1['Response']['FlightItinerary']['Passenger'][0]['City'];
                    $passengerphone=$result_book1['Response']['FlightItinerary']['Passenger'][0]['ContactNo'];
                    $passengeremail=$result_book1['Response']['FlightItinerary']['Passenger'][0]['Email'];
                    $ticketid="";
                    $ticketno="";
                    $totalpassenger =  count($result_book1['Response']['FlightItinerary']['Passenger']);
                    //multiple passenger
                    if(!empty($result_book1['Response']['FlightItinerary']['Passenger'][0]['Ticket']['TicketId']))
                    {
                        for($ps=0;$ps<$totalpassenger;$ps++)
                        {
                            if($ps==0)
                            {
                                $ticketid .=$result_book1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketId'];
                                $ticketno .=$result_book1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketNumber'];
                            }
                            else
                            {
                                $ticketid .=",".$result_book1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketId'];
                                $ticketno .=",".$result_book1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketNumber'];
                            }

                        }
                    }


                    $farebasiccode = $result_book1['Response']['FlightItinerary']['FareRules'][0]['FareBasisCode'];
                    if($lcc!='1')
                    {
                        $farefamilycode = $result_book1['Response']['FlightItinerary']['FareRules'][0]['FareFamilyCode'];
                        $invoice_amount='';
                        $invoice_no='';
                        $invoice_status='';
                        $invoice_date='';
                        $invoice_new_date='';

                    }
                    else
                    {
                        $farefamilycode='';
                        $invoice_amount=$result_book1['Response']['FlightItinerary']['InvoiceAmount'];
                        $invoice_no=$result_book1['Response']['FlightItinerary']['InvoiceNo'];
                        $invoice_status=$result_book1['Response']['FlightItinerary']['InvoiceStatus'];
                        $invoice_date=$result_book1['Response']['FlightItinerary']['InvoiceCreatedOn'];
                        $invoice_date1=explode('T',$invoice_date);
                        $invoice_new_date=$invoice_date1[0];
                    }


                    $flightbook_date = $result_book1['Response']['FlightItinerary']['Segments'][0]['Origin']['DepTime'];
                }//error if close
                $flightbook_date1=explode('T',$flightbook_date);
                $flight_booking_date=$flightbook_date1[0];
                $flight_booking_time=$flightbook_date1[1];
                $create_date=date('Y-m-d');
                $create_time=date('H:i:s');
                $farearray = serialize($fare);
                $segmentarray = serialize($segment);
                $flight = new tbl_flight_book;
                $passengerdetailarray = serialize($passengerdetail);
                $flight->flight_booking_date=$flight_booking_date;
                $flight->pnrno = $PNR;
                $flight->bookingId = $BookingId;
                $flight->domestic = $domestic;
                $flight->orign = $orign;
                $flight->destination = $destination;
                $flight->lcc = $lcc;
                $flight->fareType = $fareType;
                $flight->fare= $farearray;
                $flight->segment= $segmentarray;
                $flight->passenger_detail= $passengerdetailarray;
                $flight->farebasiccode = $farebasiccode;
                $flight->status= $status;
                $flight->invoice_amount= $invoice_amount;
                $flight->invoice_no= $invoice_no;
                $flight->invoice_status= $invoice_status;
                $flight->invoice_date = $invoice_date;
                $flight->invoice_new_date= $invoice_new_date;
                $flight->booking_date= $create_date;
                $flight->booking_time= $create_time;
                $flight->farefamilycode = $farefamilycode;
                $flight->pname= $passengername;
                $flight->pmobile= $passengerphone;
                $flight->pemail= $passengeremail;
                $flight->pcity = $passengercity;
                $flight->adults= $adultcount;
                $flight->child = $childcount;
                $flight->journytype=$journytype;
                $flight->flightcabinclass=$flightcabinclass;
                $flight->grandprice=$grandprice;
                $flight->mealname=$mealname;
                
                $flight->order_id=$paymentorder;
                $flight->order_ref=$paymentref;
                $flight->login_id=$user_id;
                $flight->login_username=$username;
                $flight->login_guest_email=$useremail;
                $flight->refund=$refund;
                $flight->flightdata=$flightdata;
                $flight->guestphone=$userphone;
                $flight->flight_ticekt_id=$ticketid;
                $flight->flight_ticekt_no=$ticketno;
                $flight->flight_check=$flightstatus;
                $flight->mealprice=$mealprice;
                $flight->meal_orgprice=$meal_orgprice;
                $flight->bag_weight=$bag_weight;
                $flight->bag_price=$bag_price;
                $flight->bag_orgprice=$bag_orgprice;
                $flight->flightmainprice=$flightlastprice;
                $flight->flightmarginprice=$flightmarginprice;
                $flight->flighttax=$flighttaxprice;
                $flight->flightothercharges=$flightotherprice;
                $flight->currency_icon=$currencyicon;
                $flight->currency_convert=$convertcurrecny;
                $flight->eme_name=$eme_name;
                $flight->ema_phone=$ema_phone;
                $flight->eme_relation=$eme_relation;
                $flight->ff_number=$ff_number;
                $flight->ff_name=$ff_name;
                $flight->check_type='web';
                if($flight->save())
                {
                    $tbl_id = $flight->id;

                    $paymenttable = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['ticket_id' => $tbl_id,
                        'payment_code'=>'Flight',
                    ]);
                    $mytrip = new tbl_mytrip;
                    $mytrip->triptype='Flight';
                    $mytrip->bookingid=$BookingId;
                    $mytrip->pnr=$PNR;
                    $mytrip->tableid=$tbl_id;
                    $mytrip->booking_date=$flight_booking_date;
                    $mytrip->booking_time=$flight_booking_time;
                    $mytrip->trip_date=$create_date;
                    $mytrip->trip_time=$create_time;
                    $mytrip->login_id=$user_id;
                    $mytrip->login_username=$username;
                    $mytrip->login_guest_email=$useremail;
                    $mytrip->guestphone=$userphone;
                    $mytrip->save();
                    // echo "<script>alert('Successfully Ticket Booked');</script>";
                    // return view('pages.flight_newinvoice');
                    // $this->pnr = $PNR;
                    // return new pnr($PNR);
                    //smsstart
                    $hotelsms=Cookie::get('gensetting');


                    $chdata = session()->get('postData');

                    $customerName = $chdata['customerName'];
                    $orderCurrency=$chdata['orderCurrency'];
                    $customerPhone = $chdata['customerPhone'];
                    $customerEmail =  $chdata['customerEmail'];
                    $hl = explode("-", $hotelsms->sms_active);
                    $sms_user=$hotelsms->sms_user_id;
                    $sms_password=$hotelsms->sms_password;
                    $sms_sender=$hotelsms->sms_sender;
                    $sms_url=$hotelsms->sms_url;
                    $bookingdate= date("d M Y" , strtotime($flight_booking_date));
                    $bookingtime = date("H:s" , strtotime($flight_booking_time));
                    if($hl[1]=='1')
                    {

                        $leadphone1=$customerPhone;
                        $user=$sms_user;
                        $password=$sms_password;
                        $sender=$sms_sender;

                        $text="Dear ".$titleadult[0]." ".$passengername.", ​Your ".$flightname." PNR is ".$PNR." (".$airlinecode."-".$flightno.") booking on ".$bookingdate." ".$bookingtime.". Please check your email for further details. -Travo Web. ";
                        $otp=urlencode("$text");
                        $data = array(
                            'usr' => $user,
                            'pwd' => $password,
                            'ph'  => $leadphone1,
                            'sndr'  => $sender,
                            'text'  => $otp
                        );

                        $ch = curl_init($sms_url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                        curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                        $result = curl_exec($ch);

                    }
                    //smsend
                    $request->session()->put('pnrno', $PNR);
                    $request->session()->put('pnrno_return', '');
                    if($resultindex_return=='')
                    {
                        return redirect()->intended('/flightnewinvoice');
                        echo '<script>
	                    $(document).ready(function(){
	                      
	                      $("#myModal1").modal("hide");
	                      });
	                </script>';
                    }

                }
                else
                {
                    echo "<script>alert('Error ticket');</script>";
                    return redirect()->intended('/index');
                }
            }
            else
            {
                echo "<script>alert('Error');</script>";
            }
            //return flight
            if($resultindex_return !='')
            {


                if($lcccheck_return=='1')
                {
                    $baggage_return=$formdata['baggage_return'];
                    if($baggage_return !='')
                    {
                        $baggage1=explode('%',$baggage_return);
                        $WayType_return = $baggage1[2];
                        $bagCode_return = $baggage1[3];
                        $bagDescription_return = $baggage1[4];
                        $Weight_return = $baggage1[0];
                        $Price_return = $baggage1[1];
                        $Origin_return = $baggage1[5];
                        $Destination_return = $baggage1[6];
                        $bagflightnumber_return=$baggage1[9];
                        $bagflightcode_return=$baggage1[10];
                    }
                    else
                    {
                        $WayType_return = "";
                        $bagCode_return = "";
                        $bagDescription_return = "";
                        $Weight_return = "";
                        $Price_return = "";
                        $Origin_return = "";
                        $Destination_return = "";
                        $bagflightnumber_return="";
                        $bagflightcode_return="";
                    }
                    //meal
                    if(!empty($formdata['mealdynamicreturn']))
                    {

                        $mealdynamicreturn=$formdata['mealdynamicreturn'];
                        if($mealdynamicreturn !='')
                        {
                            $mealdyn= explode('%',$mealdynamicreturn);
                            $Code_return=$mealdyn[0];
                            $Description_return=$mealdyn[1];
                            $mealway_return=$formdata['mealway_return'];
                            $mealflightname_return=$formdata['mealflightname_return'];
                            $mealflightcode_return=$formdata['mealflightcode_return'];
                            $mealquantaty_return=$formdata['mealquantaty_return'];
                        }
                    }
                    else
                    {
                        $Code_return="";
                        $Description_return="";
                        $mealway_return="";
                        $mealflightname_return="";
                        $mealflightcode_return="";
                        $mealquantaty_return="";
                    }

                    $totaladult=count($firstnameadult);
                    $adultbasefare_return=$adultbasefarereturn / $totaladult;
                    $adulttaxfare_return=$adulttaxfarereturn/$totaladult;

                    for($adult=0;$adult<count($firstnameadult);$adult++)
                    {
                        if($titleadult[$adult]=='Mr')
                        {
                            $adultgender="1";
                        }
                        else
                        {
                            $adultgender="2";
                        }
                        if($adult=='0')
                        {
                            $paxval="true";
                        }
                        else
                        {
                            $paxval="false";
                        }
                         if($passportcheck==1)
                        {
                            $PassportNo=$formdata['passportno'][$adult];
                            $passportexp=explode('/',$formdata['passportexp'][$child]);
                            $PassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                            

                        }
                        else
                        {
                            $PassportNo="";
                            $PassportExpiry="";
                        }
                        $adultdob1=explode('/',$adultdob_date[$adult]);
                        $adultdob = $adultdob1[2]."-".$adultdob1[1]."-".$adultdob1[0]."T00:00:00";
                        $addultpassengerdetailr[]=array(
                            'Title' =>$titleadult[$adult],
                            'FirstName'=>$firstnameadult[$adult],
                            'LastName'=>$lastnameadult[$adult],
                            'PaxType' =>'1',
                            'DateOfBirth'=>$adultdob,
                            'Gender'=>$adultgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=> $PassportNo,
                            "PassportExpiry"=> $PassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> $paxval,
                            "FFAirline"=> $ff_name_return,
                            "FFNumber"=> $ff_number_return,
                            "Fare"=>[array(
                                "BaseFare"=>$adultbasefare_return,
                                "Tax"=> $adulttaxfare_return,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $adulttxnfeeofrdreturn,
                                "AdditionalTxnFeePub"=> $adulttxnfeepubreturn,
                                "AirTransFee"=> "0.0",
                            )],
                            "Baggage"=>[array(
                                "AirlineCode"=>$bagflightnumber_return,
                                "FlightNumber"=>$bagflightcode_return,
                                "WayType"=>$WayType_return,
                                "Code"=> $bagCode_return,
                                "Description"=>$bagDescription_return,
                                "Weight"=> $Weight_return,
                                "Currency"=> $Currency,
                                "Price"=>$Price_return,
                                "Origin"=> $Origin_return,
                                "Destination"=> $Destination_return,
                            )],
                            "Meal Dynamic"=>[array(
                                "AirlineCode"=>$mealflightcode_return,
                                "FlightNumber"=>$mealflightname_return,
                                "WayType"=>$mealway_return,
                                "Code"=> $Code_return,
                                "Description"=> $Description_return,
                                "AirlineDescription"=>$mealname_return,
                                "Quantity"=>$mealquantaty_return,
                                "Currency"=> $Currency,
                                "Price"=>$meal_orgprice_return,
                                "Origin"=> $Origin_return,
                                "Destination"=> $Destination_return,
                            )],


                        );

                    }
                    if(count($childfirstname) !="0")
                    {
                         $totalchild=count($childfirstname);
                        $childfare_return=$childbasefarereturn / $totalchild;
                        $childtaxfare_return=$childtaxfarereturn/$totalchild;
                    }
                   
                    for($child=0;$child<count($childfirstname);$child++)
                    {
                        if($childtitle[$child]=='Mr')
                        {
                            $childgender="1";
                        }
                        else
                        {
                            $childgender="2";
                        }
                        if($passportcheck==1)
                        {
                            $childPassportNo=$formdata['childpassportno'][$child];
                            $passportexp=explode('/',$formdata['childpassportexp'][$child]);
                            $childPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                            

                        }
                        else
                        {
                            $childPassportNo="";
                            $childPassportExpiry="";
                        }
                        $childdob1=explode('/',$childdob_date[$child]);
                        $childdob = $childdob1[2]."-".$childdob1[1]."-".$childdob1[0]."T00:00:00";

                        $addultpassengerdetailr[]=array(
                            'Title' =>$childtitle[$child],
                            'FirstName'=>$childfirstname[$child],
                            'LastName'=>$childlastname[$child],
                            'PaxType' =>'2',
                            'DateOfBirth'=>$childdob,
                            'Gender'=>$childgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=> $childPassportNo,
                            "PassportExpiry"=> $childPassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> false,
                            "FFAirline"=> $ff_name_return,
                            "FFNumber"=> $ff_number_return,
                            "Fare"=>[array(
                                "BaseFare"=>$childfare_return,
                                "Tax"=> $childtaxfare_return,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $childtxnfeeofrdreturn,
                                "AdditionalTxnFeePub"=> $childtxnfeepubreturn,
                                "AirTransFee"=> "0.0",
                            )],
                            "Baggage"=>[array(
                                "AirlineCode"=>$bagflightnumber_return,
                                "FlightNumber"=>$bagflightcode_return,
                                "WayType"=>$WayType_return,
                                "Code"=> $bagCode_return,
                                "Description"=>$bagDescription_return,
                                "Weight"=> $Weight_return,
                                "Currency"=> $Currency,
                                "Price"=>$Price_return,
                                "Origin"=> $Origin_return,
                                "Destination"=> $Destination_return,
                            )],
                            "Meal Dynamic"=>[array(
                                "AirlineCode"=>$mealflightcode_return,
                                "FlightNumber"=>$mealflightname_return,
                                "WayType"=>$mealway_return,
                                "Code"=> $Code_return,
                                "Description"=> $Description_return,
                                "AirlineDescription"=>$mealname_return,
                                "Quantity"=>$mealquantaty_return,
                                "Currency"=> $Currency,
                                "Price"=>$meal_orgprice_return,
                                "Origin"=> $Origin_return,
                                "Destination"=> $Destination_return,
                            )],


                        );
                        //multicity if close

                    }
                    if($infacount !=0)
                    {
                        $infantfare_return=$infantbasefarereturn / $infacount;
                        $infanttax_return=$infanttaxfarereturn / $infacount;
                    }
                    
                    for($infa=0;$infa<$infacount;$infa++)
                    {
                        if($infatitle[$infa]=='Mstr')
                        {
                            $infagender="1";
                        }
                        else
                        {
                            $infagender="2";
                        }
                        if($passportcheck==1)
                        {
                            $infatPassportNo=$formdata['infantpassportno'][$infa];
                            $passportexp=explode('/',$formdata['infantpassportexp'][$infa]);
                            $infatPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                            

                        }
                        else
                        {
                            $infatPassportNo="";
                            $infatPassportExpiry="";
                        }
                        $infadob1=explode('/',$infadob_date[$infa]);
                        $infantdob = $infadob1[2]."-".$infadob1[1]."-".$infadob1[0]."T00:00:00";

                        $addultpassengerdetailr[]=array(
                            'Title' =>$infatitle[$infa],
                            'FirstName'=>$infafistname[$infa],
                            'LastName'=>$infalastname[$infa],
                            'PaxType' =>'3',
                            'DateOfBirth'=>$infantdob,
                            'Gender'=>$infagender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=> $infatPassportNo,
                            "PassportExpiry"=>  $infatPassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> false,
                            "FFAirline"=> $ff_name_return,
                            "FFNumber"=> $ff_number_return,
                            "Fare"=>[array(
                                "BaseFare"=>$infantfare_return,
                                "Tax"=> $infanttax_return,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $infanttxnfeeofrdreturn,
                                "AdditionalTxnFeePub"=> $infanttxnfeepubreturn,
                                "AirTransFee"=> "0.0",
                            )],
                            //  "Baggage"=>[array(
                            // 	"WayType"=>$WayType_return,
                            //        "Code"=> $bagCode_return,
                            //        "Description"=>$bagDescription_return,
                            //        "Weight"=> $Weight_return,
                            //        "Currency"=> $Currency,
                            //        "Price"=>$Price_return,
                            //        "Origin"=> $Origin_return,
                            //        "Destination"=> $Destination_return,
                            // )],
                            "Meal Dynamic"=>[array(
                               "AirlineCode"=>$mealflightcode_return,
                                "FlightNumber"=>$mealflightname_return,
                                "WayType"=>$mealway_return,
                                "Code"=> $Code_return,
                                "Description"=> $Description_return,
                                "AirlineDescription"=>$mealname_return,
                                "Quantity"=>$mealquantaty_return,
                                "Currency"=> $Currency,
                                "Price"=>$meal_orgprice_return,
                                "Origin"=> $Origin_return,
                                "Destination"=> $Destination_return,
                            )],


                        );
                        //multicity if close

                    }


                    $form_data_return = array(
                        'EndUserIp'=>$EndUserIp,
                        'TokenId'=>$TokenId,
                        'TraceId'=>$TraceId,
                        'ResultIndex'=>$resultindex_return,
                        'Passengers'=>$addultpassengerdetailr,
                    );
                    $data_string_return = json_encode($form_data_return);
                  
                    $ch = curl_init('https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Ticket/');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_return);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($data_string_return))
                    );

                    $result_ret = curl_exec($ch);
                  
                    $resultticket_return=json_decode($result_ret,true);
                    $errorcode1 = $resultticket_return['Response']['Error']['ErrorCode'];
                    $errormsg1 = $resultticket_return['Response']['Error']['ErrorMessage'];
                    $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1]);
                    if($resultticket_return['Response']['Error']['ErrorCode']!='0')
                    {

                        $message = $resultticket_return['Response']['Error']['ErrorMessage'];

                        if($resultticket_return['Response']['Error']['ErrorCode']=='6' )
                        {
                            return redirect()->intended('/index');
                            echo "<script>alert('".$message."');</script>";
                        }
                        else if($resultticket_return['Response']['Error']['ErrorCode']=='3' )
                        {
                            return redirect()->intended('/flightbook');
                            echo "<script>alert('".$message."');</script>";
                        }
                        else
                        {
                            return redirect()->intended('/index');
                            echo "<script>alert('".$message."');</script>";
                        }
                        die();
                    }
                    else
                    {

                        $PNR_return=$resultticket_return['Response']['Response']['PNR'];
                        $BookingId_return=$resultticket_return['Response']['Response']['BookingId'];
                        $status=$resultticket_return['Response']['Response']['TicketStatus'];
                        $flightstatus_return="Lcc";
                    }
                }
                else
                {		
                    if(!empty($formdata['mealreturn']))
                {
                    $mealreturn= $formdata['mealreturn'];
                    if($mealreturn !='')
                    {
                        $mealreturn1= explode('%',$mealreturn);
                        $Code_return=$mealreturn1[0];
                        $Description_return=$mealreturn1[1];
                        $mealway_return=$formdata['mealway_return'];
                        $mealflightname_return=$formdata['mealflightname_return'];
                        $mealflightcode_return=$formdata['mealflightcode_return'];
                       $mealquantaty_return=$formdata['mealquantaty_return'];
                    }
                }
                else
                {
                    $Code_return="";
                    $Description_return="";
                    $mealway_return="";
                        $mealflightname_return="";
                        $mealflightcode_return="";
                        $mealquantaty_return="";
                }

                    //book method
                $totaladult=count($firstnameadult);
                    $adultfare_retrun1=$adultbasefarereturn / $totaladult;
                    $adulttax_retrun1=$adulttaxfarereturn / $totaladult;
                    $YQTax_retrun1=$YQTax_return/$totaladult;
                    for($adult=0;$adult<count($firstnameadult);$adult++)
                    {
                        if($titleadult[$adult]=='Mr')
                        {
                            $adultgender="1";
                        }
                        else
                        {
                            $adultgender="2";
                        }
                        if($adult=='0')
                        {
                            $paxval="true";
                        }
                        else
                        {
                            $paxval="false";
                        }
                        if($passportcheck==1)
                        {
                            $PassportNo=$formdata['passportno'][$adult];
                            $passportexp=explode('/',$formdata['passportexp'][$adult]);
                            $PassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                            

                        }
                        else
                        {
                            $PassportNo="";
                            $PassportExpiry="";
                        }
                        $adultdob1=explode('/',$adultdob_date[$adult]);
                        $adultdob = $adultdob1[2]."-".$adultdob1[1]."-".$adultdob1[0]."T00:00:00";

                        $addultpassengerdetaillccr[]=array(
                            'Title' =>$titleadult[$adult],
                            'FirstName'=>$firstnameadult[$adult],
                            'LastName'=>$lastnameadult[$adult],
                            'PaxType' =>'1',
                            'DateOfBirth'=>$adultdob,
                            'Gender'=>$adultgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=> $PassportNo,
                            "PassportExpiry"=>  $PassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> $paxval,
                            "FFAirline"=> $ff_name_return,
                            "FFNumber"=> $ff_number_return,
                            "Fare"=>[array(
                                "BaseFare"=>$adultfare_retrun1,
                                "Tax"=> $adulttax_retrun1,
                                "YQTax"=> $YQTax_retrun1,
                                "AdditionalTxnFeeOfrd"=> $adulttxnfeeofrdreturn,
                                "AdditionalTxnFeePub"=> $adulttxnfeepubreturn,
                                "AirTransFee"=> "0.0",
                            )],




                        );
                    }
                    if(count($childfirstname) !=0)
                    {

                         $totalchild=count($childfirstname);
                         $childfare_return1=$childbasefarereturn / $totalchild;
                        $childtax_return1=$childtaxfarereturn / $totalchild;
                        $YQTax_return12=$YQTax_return/$totalchild;
                    }
                    
                    for($child=0;$child<count($childfirstname);$child++)
                    {
                        if($childtitle[$child]=='Mr')
                        {
                            $childgender="1";
                        }
                        else
                        {
                            $childgender="2";
                        }
                         if($passportcheck==1)
                        {
                            $childPassportNo=$formdata['childpassportno'][$child];
                            $passportexp=explode('/',$formdata['childpassportexp'][$child]);
                            $childPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                            

                        }
                        else
                        {
                            $childPassportNo="";
                            $childPassportExpiry="";
                        }
                        $childdob1=explode('/',$childdob_date[$child]);
                        $childdob = $childdob1[2]."-".$childdob1[1]."-".$childdob1[0]."T00:00:00";
                        $addultpassengerdetaillccr[]=array(
                            'Title' =>$childtitle[$child],
                            'FirstName'=>$childfirstname[$child],
                            'LastName'=>$childlastname[$child],
                            'PaxType' =>'2',
                            'DateOfBirth'=>$childdob,
                            'Gender'=>$childgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=>$childPassportNo,
                            "PassportExpiry"=>  $childPassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> false,
                            "FFAirline"=> $ff_name_return,
                            "FFNumber"=> $ff_number_return,
                            "Fare"=>[array(
                                "BaseFare"=>$childfare_return1,
                                "Tax"=> $childtax_return1,
                                "YQTax"=> $YQTax_return12,
                                "AdditionalTxnFeeOfrd"=> $childtxnfeeofrdreturn,
                                "AdditionalTxnFeePub"=> $childtxnfeepubreturn,
                                "AirTransFee"=> "0.0",
                            )],


                        );
                    } //childforloopclose
                    if($infacount !=0)
                    {
                        $infantfare_return1=$infantbasefarereturn / $infacount;
                        $infanttax_return1=$infanttaxfarereturn / $infacount;
                    }
                    
                    for($infa=0;$infa<$infacount;$infa++)
                    {
                        if($infatitle[$infa]=='Mstr')
                        {
                            $infagender="1";
                        }
                        else
                        {
                            $infagender="2";
                        }
                         if($passportcheck==1)
                        {
                            $infatPassportNo=$formdata['infantpassportno'][$infa];
                            $passportexp=explode('/',$formdata['infantpassportexp'][$infa]);
                            $infatPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                            

                        }
                        else
                        {
                            $infatPassportNo="";
                            $infatPassportExpiry="";
                        }
                        $infadob1=explode('/',$infadob_date[$infa]);
                        $infantdob = $infadob1[2]."-".$infadob1[1]."-".$infadob1[0]."T00:00:00";

                        $addultpassengerdetaillccr[]=array(
                            'Title' =>$infatitle[$infa],
                            'FirstName'=>$infafistname[$infa],
                            'LastName'=>$infalastname[$infa],
                            'PaxType' =>'3',
                            'DateOfBirth'=>$infantdob,
                            'Gender'=>$infagender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=> $infatPassportNo,
                            "PassportExpiry"=>  $infatPassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> false,
                            "FFAirline"=> $ff_name_return,
                            "FFNumber"=> $ff_number_return,
                            "Fare"=>[array(
                                "BaseFare"=>$infantfare_return1,
                                "Tax"=> $infanttax_return1,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $infanttxnfeeofrdreturn,
                                "AdditionalTxnFeePub"=> $infanttxnfeepubreturn,
                                "AirTransFee"=> "0.0",
                            )],



                        );
                        //multicity if close

                    }
                    $form_data_returnlcc = array(
                        'EndUserIp'=>$EndUserIp,
                        'TokenId'=>$TokenId,
                        'TraceId'=>$TraceId,
                        'ResultIndex'=>$ResultIndex,
                        'Passengers'=>$addultpassengerdetaillccr,
                    );
                    $data_string_lcc_return = json_encode($form_data_returnlcc);
                   
                    $ch = curl_init('https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Book/');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_lcc_return);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($data_string_lcc_return))
                    );

                    $result_lcc_return = curl_exec($ch);

                    $result_lcc_return1=json_decode($result_lcc_return,true);
                    $errorcode11 = $result_lcc_return1['Response']['Error']['ErrorCode'];
                    $errormsg11 = $result_lcc_return1['Response']['Error']['ErrorMessage'];
                    $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode11,'response_msg' => $errormsg11]);
                    if($result_lcc_return1['Response']['Error']['ErrorCode']!='0')
                    {

                        $message = $result_lcc_return1['Response']['Error']['ErrorMessage'];
                        echo "<script>alert('".$message."');</script>";
                        if($result_lcc_return1['Response']['Error']['ErrorCode']=='6')
                        {
                            return redirect()->intended('/index');
                        }
                        else if($result_lcc_return1['Response']['Error']['ErrorCode']=='3')
                        {
                            return redirect()->intended('/flightbook');
                        }
                        die();
                    }
                    else
                    {
                        $PNR_return=$result_lcc_return1['Response']['Response']['PNR'];
                        $BookingId_return=$result_lcc_return1['Response']['Response']['BookingId'];
                        $status=$result_lcc_return1['Response']['Response']['FlightItinerary']['Status'];
                        //again ticket call is non lcc flight
                        $form_data_ticket = array(
                            'EndUserIp'=>$EndUserIp,
                            'TokenId'=>$TokenId,
                            'TraceId'=>$TraceId,
                            'PNR'=>$PNR_return,
                            'BookingId'=>$BookingId_return,
                        );
                        $data_string_ticket = json_encode($form_data_ticket);
                        $flightstatus_return="Non Lcc";
                        $ch = curl_init('https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Ticket/');
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_ticket);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                'Content-Type: application/json',
                                'Content-Length: ' . strlen($data_string_ticket))
                        );

                        $result_new = curl_exec($ch);
                        $resultticket_new=json_decode($result_new,true);



                        // end call non lcc flight
                    }
                } //if loop close

                if($status=='1')
                {
                    $form_data_book_return = array(
                        'EndUserIp'=>$EndUserIp,
                        'TokenId'=>$TokenId,
                        'TraceId'=>$TraceId,
                        'BookingId'=>$BookingId_return,
                        'PNR' =>$PNR_return,
                    );
                    $data_string_book_return = json_encode($form_data_book_return);

                    $ch = curl_init('https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/GetBookingDetails/');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_book_return);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($data_string_book_return))
                    );

                    $result_book_return = curl_exec($ch);


                    $result_book_return1=json_decode($result_book_return,true);
                    $errorcode11 = $result_book_return1['Response']['Error']['ErrorCode'];
                    $errormsg11 = $result_book_return1['Response']['Error']['ErrorMessage'];
                    $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode11,'response_msg' => $errormsg11]);
                    $flightdata_return1=$result_book_return1['Response'];
                    $flightdata_return = serialize($flightdata_return1);
                    $domestic=$result_book_return1['Response']['FlightItinerary']['IsDomestic'];
                    $orign=$result_book_return1['Response']['FlightItinerary']['Origin'];
                    $destination=$result_book_return1['Response']['FlightItinerary']['Destination'];
                    $lcc=$result_book_return1['Response']['FlightItinerary']['IsLCC'];
                    $fareType=$result_book_return1['Response']['FlightItinerary']['FareType'];
                    $fare=$result_book_return1['Response']['FlightItinerary']['Fare']; //array
                    $segment=$result_book_return1['Response']['FlightItinerary']['Segments']; //array
                    $passengerdetail=$result_book_return1['Response']['FlightItinerary']['Passenger']; //array
                    $passengerfname=$result_book_return1['Response']['FlightItinerary']['Passenger'][0]['FirstName'];
                    $passengerlname=$result_book_return1['Response']['FlightItinerary']['Passenger'][0]['LastName'];
                    $passengername =$passengerfname.' '.$passengerlname;
                    $passengercity=$result_book_return1['Response']['FlightItinerary']['Passenger'][0]['City'];
                    $passengerphone=$result_book_return1['Response']['FlightItinerary']['Passenger'][0]['ContactNo'];
                    $passengeremail=$result_book_return1['Response']['FlightItinerary']['Passenger'][0]['Email'];

                    $totalpassenger =  count($result_book_return1['Response']['FlightItinerary']['Passenger']); //multiple passenger
                    $ticketid_return="";
                    $ticketno_return="";

                    for($ps=0;$ps<$totalpassenger;$ps++)
                    {
                        if($ps==0)
                        {
                            $ticketid_return .=$result_book_return1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketId'];
                            $ticketno_return .=$result_book_return1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketNumber'];
                        }
                        else
                        {
                            $ticketid_return .=",".$result_book_return1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketId'];
                            $ticketno_return .=",".$result_book_return1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketNumber'];
                        }

                    }
                    $farebasiccode = $result_book_return1['Response']['FlightItinerary']['FareRules'][0]['FareBasisCode'];
                    if($lcc!='1')
                    {
                        $farefamilycode = $result_book_return1['Response']['FlightItinerary']['FareRules'][0]['FareFamilyCode'];
                        $invoice_amount='';
                        $invoice_no='';
                        $invoice_status='';
                        $invoice_date='';
                        $invoice_new_date='';

                    }
                    else
                    {
                        $farefamilycode='';
                        $invoice_amount=$result_book_return1['Response']['FlightItinerary']['InvoiceAmount'];
                        $invoice_no=$result_book_return1['Response']['FlightItinerary']['InvoiceNo'];
                        $invoice_status=$result_book_return1['Response']['FlightItinerary']['InvoiceStatus'];
                        $invoice_date=$result_book_return1['Response']['FlightItinerary']['InvoiceCreatedOn'];
                        $invoice_date1=explode('T',$invoice_date);
                        $invoice_new_date=$invoice_date1[0];
                    }

                    $flightbook_date = $result_book_return1['Response']['FlightItinerary']['Segments'][0]['Origin']['DepTime'];
                    $flightbook_date1=explode('T',$flightbook_date);
                    $flight_booking_date=$flightbook_date1[0];
                    $flight_booking_time=$flightbook_date1[1];
                    $create_date=date('Y-m-d');
                    $create_time=date('H:i:s');
                    $farearray = serialize($fare);
                    $segmentarray = serialize($segment);
                    $flight = new tbl_flight_book;
                    $passengerdetailarray = serialize($passengerdetail);
                    $flight->flight_booking_date=$flight_booking_date;
                    $flight->pnrno = $PNR_return;
                    $flight->bookingId = $BookingId_return;
                    $flight->domestic = $domestic;
                    $flight->orign = $orign;
                    $flight->destination = $destination;
                    $flight->lcc = $lcc;
                    $flight->fareType = $fareType;
                    $flight->fare= $farearray;
                    $flight->segment= $segmentarray;
                    $flight->passenger_detail= $passengerdetailarray;
                    $flight->farebasiccode = $farebasiccode;
                    $flight->status= $status;
                    $flight->invoice_amount= $invoice_amount;
                    $flight->invoice_no= $invoice_no;
                    $flight->invoice_status= $invoice_status;
                    $flight->invoice_date = $invoice_date;
                    $flight->invoice_new_date= $invoice_new_date;
                    $flight->booking_date= $create_date;
                    $flight->booking_time= $create_time;
                    $flight->farefamilycode = $farefamilycode;
                    $flight->pname= $passengername;
                    $flight->pmobile= $passengerphone;
                    $flight->pemail= $passengeremail;
                    $flight->pcity = $passengercity;
                    $flight->adults= $adultcount;
                    $flight->child = $childcount;
                    $flight->journytype=$journytype;
                    $flight->flightcabinclass=$flightcabinclass;
                    $flight->grandprice=$grandprice;

                    $flight->order_id=$paymentorder;
                    $flight->order_ref=$paymentref;
                    $flight->login_id=$user_id;
                    $flight->login_username=$username;
                    $flight->login_guest_email=$useremail;
                    $flight->guestphone=$userphone;
                    $flight->refund=$refund;
                    $flight->flightdata=$flightdata_return;
                    $flight->flight_ticekt_id=$ticketid_return;
                    $flight->flight_ticekt_no=$ticketno_return;
                    $flight->flight_check=$flightstatus_return;
                    $flight->mealprice=$mealprice_return;
                    $flight->meal_orgprice=$meal_orgprice_return;
                    $flight->bag_weight=$bag_weight_return;
                    $flight->bag_price=$bag_price_return;
                    $flight->bag_orgprice=$bag_orgprice_return;
                    $flight->mealname=$mealname_return;

                    $flight->flightmainprice=$flightlastprice_return;
                    $flight->flightmarginprice=$flightmarginprice_return;
                    $flight->flighttax=$flighttaxprice_return;
                    $flight->flightothercharges=$flightotherprice_return;
                    $flight->currency_icon=$currencyicon;
                    $flight->currency_convert=$convertcurrecny;
                    $flight->eme_name=$eme_name;
                    $flight->ema_phone=$ema_phone;
                    $flight->eme_relation=$eme_relation;
                    $flight->ff_number=$ff_number_return;
                    $flight->ff_name=$ff_name_return;
                    $flight->check_type='web';
                    if($flight->save())
                    {
                        $tbl_id = $flight->id;
                        $mytrip = new tbl_mytrip;
                        $mytrip->triptype='Flight';
                        $mytrip->bookingid=$BookingId_return;
                        $mytrip->pnr=$PNR_return;
                        $mytrip->tableid=$tbl_id;
                        $mytrip->booking_date=$flight_booking_date;
                        $mytrip->booking_time=$flight_booking_time;
                        $mytrip->trip_date=$create_date;
                        $mytrip->trip_time=$create_time;
                        $mytrip->login_id=$user_id;
                        $mytrip->login_username=$username;
                        $mytrip->login_guest_email=$useremail;
                        $mytrip->guestphone=$userphone;
                        $mytrip->save();
                        $paymenttable = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['ticket_id' => $tbl_id,
                            'payment_code'=>'Flight',
                        ]);


                        $request->session()->put('pnrno_return', $PNR_return);
                        return redirect()->intended('/flightnewinvoice');
                        echo '<script>
			                    $(document).ready(function(){
			                      
			                      $("#myModal1").modal("hide");
			                      });
			                </script>';
                    }
                    else
                    {
                        echo "<script>alert('Error ticket');</script>";
                        return redirect()->intended('/index');
                    }
                }
                else
                {
                    echo "<script>alert('Error');</script>";
                }
            } //payment session close
        }
        // end return flight


    } //function loop close
    //testfile
    public function flight_invoicebooking(Request $request)
    {
        // echo $pnrno =  $this->pnr;
        $data = array(
            'name' => "sarbjit",
            'email' => "sarbjitphp@netwebtechnologies.com",
            'message' => "testpdf",
        );
        $pnrno=session()->get('pnrno');
        $pnrno_return=session()->get('pnrno_return');
        $flightbook= DB::table('tbl_flight_book')->where('pnrno', $pnrno)->first();
        $flightemail = $flightbook->pemail;
        session()->put('flightemailnew',$flightemail);
        if(count($flightbook) > 0)
        {

            // echo $id=$flightbook->pnrno;
            session()->put('flightaraay1',$flightbook);
            $flightbook_return='';
            session()->put('flightaraay2',$flightbook_return);
            if($pnrno_return !='')
            {
                $flightbook_return= DB::table('tbl_flight_book')->where('pnrno', $pnrno_return)->first();
                session()->put('flightaraay2',$flightbook_return);
            }
            $pdf = PDF::loadView('pages.flight_pdf',compact('flightbook','flightbook_return'));

            Mail::send(['html' => 'pages.invoice_flight'], $data, function($message) use($pdf)
            {
                $message->from('sarbjitphp@netwebtechnologies.com', 'sarb');

                $message->to(session()->get('flightemailnew'))->subject('Flight Invoice');

                $message->attachData($pdf->output(), "invoice.pdf");
            });
            return view('pages.flight_newinvoice')->with(compact('flightbook'))->with(compact('flightbook_return'));
        }
        else
        {
            echo "<script>alert('Error');</script>";
        }
    }
    public function flight_pdf($pnr)
    {
        $pnr1=base64_decode($pnr);
        $chekpnr = explode("-", $pnr1);
        $firstpnr = $chekpnr[0];
        $pnrno_return=$chekpnr[1];
        // return view('pages.flight_pdf');
        $flightbook= DB::table('tbl_flight_book')->where('pnrno', $firstpnr)->first();

        if(count($flightbook) > 0)
        {

            // echo $id=$flightbook->pnrno;
            $flightbook_return='';
            if($pnrno_return !='')
            {
                $flightbook_return= DB::table('tbl_flight_book')->where('pnrno', $pnrno_return)->first();

            }


            $pdf = PDF::loadView('pages.flight_pdf',compact('flightbook','flightbook_return'));
            // Mail::send(['html' => 'pages.invoice_flight'], $data, function($message) use($pdf)
            // {
            //     $message->from('sarbjitphp@netwebtechnologies.com', 'sarb');

            //     $message->to('sarbjitphp@netwebtechnologies.com')->subject('Flight Invoice');

            //     $message->attachData($pdf->output(), "invoice.pdf");
            // });
            return $pdf->stream('itsolutionstuff.pdf');
        }
        else
        {
            echo "no";
        }

    }
    //testendfile

    public function request1(Request $request)
    {
        $posts = tbl_payment_order::orderBy('id', 'DESC')->first();
        if(count($posts) == 0)
        {
            $orderid="TRAstt-01";
        }
        else
        {
            $chkid =  $posts->id + 1;
            $orderid="TRAstt-0".$chkid;
        }
        $create_date=date('Y-m-d');
        $create_time=date('H:i:s');
        $customerName=$request->get('customername');
        $customerPhone=$request->get('customerPhone');
        $customerEmail=$request->get('customerEmail');
        $convertcurr=$request->get('convertcurr');
        $customerfname=$request->get('customerfname');

        $lname=$request->get('customerlname');
        $fname=$customerfname;
        $username=strtolower($request->get('customerfname').substr($request->get('customerlname'),0,3).mt_rand(1000,9999));
        if(session()->has('travo_useremail'))
        {
            $useremail=session()->get('travo_useremail');
        }
        else
        {
            $checkdata=DB::table('tbl_user')->where('user_email',$customerEmail)->first();
            if(count($checkdata)>0)
            {
                $user_id=$checkdata->user_id;
                $fname1=$checkdata->user_fname;
                $lname1=$checkdata->user_lname;
                $username1=$checkdata->user_name;
                $email=$checkdata->user_email;
                $fullname=$fname1." ".$lname1;
                session(['travo_userid'=>$user_id,'travo_name'=>$fullname,"travo_username"=>$username1,"travo_useremail"=>$email]);
                $useremail=$customerEmail;
            }
            else
            {
                $insertdata=array(
                    "user_fname"=>$fname,
                    "user_lname"=>$lname,
                    "user_phone"=>$customerPhone,
                    "user_email"=>$customerEmail,
                    "user_name"=>$username,
                    "user_status"=>1,
                    "user_create_date"=>$create_date,
                    "user_create_time"=>$create_time);
                $insert=DB::table('tbl_user')->insert($insertdata);
                if($insert)
                {
                    
                    $fullname=$fname." ".$lname;
                    session(['travo_name'=>$fullname,"travo_useremail"=>$customerEmail]);
                    $useremail=$customerEmail;
                }
            }
        }


        if($convertcurr=='IN')
        {
            $orderCurrency='INR';
            $flightpayment = new tbl_payment_order;
            $flightpayment->orderid=$orderid;
            $flightpayment->orderdate=$create_date;
            $flightpayment->ordertime=$create_time;
            $flightpayment->loginemail=$useremail;
            $flightpayment->save();
        }
        else if($convertcurr=='AU')
        {
            $orderCurrency='US';
        }
        else
        {
            $orderCurrency='US';
        }
        // $useremail=session()->get('travo_useremail');



        $dataString=$request->get('dataString');

        $request->session()->put('myflightbooking',$dataString);
        $appId="2586c1f86d9026c4ea336d946852";
        $orderId=$orderid;
        $orderAmount=$request->get('amount');
        // $orderCurrency=$request->get('currency');
        $orderNote="Flight";

        $returnUrl="https://travoweb.com/response";
        $notifyUrl="https://travoweb.com/response1";
        $csrf_token=$request->get('csrf_token');
        $postData = array(
            "appId" => $appId,
            "orderId" => $orderId,
            "orderAmount" => $orderAmount,
            "orderCurrency" => $orderCurrency,
            "orderNote" => $orderNote,
            "customerName" => $customerName,
            "customerPhone" => $customerPhone,
            "customerEmail" => $customerEmail,
            "returnUrl" => $returnUrl,
            "notifyUrl" => $notifyUrl,
        );
        echo $orderCurrency;
        $request->session()->put('postData', $postData);

        // echo json_encode($postData);
        // return redirect('request',compact('postData'));
        // return redirect()->intended('request',compact('postData'));
        // return view('pages.request')->with(compact('postData'));

        // return view('pages.request');
    }
    public function response(Request $request)
    {

        $backvalue = request()->all();
        $create_date=date('Y-m-d');
        $create_time=date('H:i:s');
        $orderid = $backvalue['orderId'];
        // $flightpayment = new tbl_payment_order;
        // $flightpayment->orderid=$backvalue['orderId'];
        // $flightpayment->amount=$backvalue['orderAmount'];
        // $flightpayment->referenceid=$backvalue['referenceId'];
        // $flightpayment->tx_status=$backvalue['txStatus'];
        // $flightpayment->payment_mode=$backvalue['paymentMode'];
        // $flightpayment->tx_msg=$backvalue['txMsg'];
        // $flightpayment->tx_time=$backvalue['txTime'];
        // $flightpayment->orderdate=$create_date;
        // $flightpayment->ordertime=$create_time;

        $paymentorder = tbl_payment_order::where('orderid', $orderid)->update(
            ['amount' => $backvalue['orderAmount'],
                'referenceid' => $backvalue['referenceId'],
                'tx_status' => $backvalue['txStatus'],
                'payment_mode' => $backvalue['paymentMode'],
                'tx_msg' => $backvalue['txMsg'],
                'tx_time' => $backvalue['txTime'],
                'orderdate' => $create_date,
                'ordertime' => $create_time,

            ]);

        if($paymentorder)
        {

            if($backvalue['txStatus']=='SUCCESS')
            {
                $status = $request->session()->put('paymentstatus','paymentsuccess');
                $request->session()->put('paymentref',$backvalue['referenceId']);
                $request->session()->put('paymentorder',$backvalue['orderId']);
                $hotelsms=Cookie::get('gensetting');


                $chdata = session()->get('postData');

                $customerName = $chdata['customerName'];
                $orderCurrency=$chdata['orderCurrency'];
                $customerPhone = $chdata['customerPhone'];
                $customerEmail =  $chdata['customerEmail'];
                $hl = explode("-", $hotelsms->sms_active);
                $sms_user=$hotelsms->sms_user_id;
                $sms_password=$hotelsms->sms_password;
                $sms_sender=$hotelsms->sms_sender;
                $sms_url=$hotelsms->sms_url;

                if($hl[0]=='1')
                {

                    $leadphone1=$customerPhone;
                    $user=$sms_user;
                    $password=$sms_password;
                    $sender=$sms_sender;

                    $text="Dear ".$customerName.", Your order with order id  ".$orderid." has been successfully placed. Please check your email for further details. -Travo Web. ";
                    $otp=urlencode("$text");
                    $data = array(
                        'usr' => $user,
                        'pwd' => $password,
                        'ph'  => $leadphone1,
                        'sndr'  => $sender,
                        'text'  => $otp
                    );

                    $ch = curl_init($sms_url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                    $result = curl_exec($ch);

                }


                $orderms = '<table class="table table-hover">

			    <tbody>

			      <tr>

			        <td>Order ID</td>

			        <td>'.$orderid.'</td>

			      </tr>

			      <tr>

			        <td>Order Amount</td>

			        <td>'.$backvalue['orderAmount'].'</td>

			      </tr>

			      <tr>

			        <td>Reference ID</td>

			        <td>'.$backvalue['referenceId'].'</td>

			      </tr>
				</tbody>

			</table>';
                $to=$customerEmail;
                $subject="Travo Web Order Placed";
                $message="Dear ".$customerName." <br>Your order with order id ".$orderid." has been successfully placed <br>".$orderms." ";
                $header="From:ticket@travoweb.com \r \n";
                $header = "From: " . strip_tags('ticket@travoweb.com') . "\r\n";
                $header .= "Reply-To: ". strip_tags('ticket@travoweb.com') . "\r\n";
                $header .= "CC: 'ticket@travoweb.com'\r\n";
                $header .= "MIME-Version: 1.0\r\n";
                $header .= "Content-Type: text/html; charset=UTF-8\r\n";
                $mailtest =mail($to,$subject,$message,$header);
                //
                return redirect()->action('FlightBookController@flight_bookview');
            }
            else
            {
                $status =$request->session()->put('paymentstatus','paymentfail');
                echo "<script>alert('Payment Failed ');</script>";
            }


        }
        else
        {

            $status =$request->session()->put('paymentstatus','paymentfail');
            echo "<script>alert('Payment Failed ');</script>";

        }
        // die();

        // return view('pages.response');
    }
    // flightcancel
    public function flightcancel(Request $request)
    {
        $useremail=session()->get('travo_useremail');
        $orign=$request->get('orign');
        $destination=$request->get('destination');
        $requesttype=$request->get('requesttype');
        $remark=$request->get('remark');
        $bookingid=$request->get('bookingid');
        $ticketid=explode(",",$request->get('ticketid'));
        // $ticketid=$request->get('ticketid');
        $canceltype=$request->get('canceltype');
        $TokenId=Cookie::get('TokenId');
        $EndUserIp=Cookie::get('localip');
        $form_cancel = array(
            'EndUserIp'=>$EndUserIp,
            'TokenId'=>$TokenId,
            'BookingId'=>$bookingid,
            'RequestType'=>$requesttype,
            'Remarks'=>$remark,
            'CancellationType'=>$canceltype,
            'Sectors'=>[array(
                'Origin'=>$orign,
                'Destination'=>$destination,
            )],
            'TicketId'=>$ticketid,
        );
        $data_flight_cancel = json_encode($form_cancel);

        $ch = curl_init('https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/SendChangeRequest/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_flight_cancel);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: '.strlen($data_flight_cancel)) );
        $result_flight_cancel = curl_exec($ch);
        $result_cancel=json_decode($result_flight_cancel,true);



        $cancel_data= serialize($result_cancel['Response']);
        date_default_timezone_set("Asia/Kolkata");
        $create_date=date('Y-m-d');
        $create_time=date('H:i:s');
        $changerequestid="";
        $errormsg="";
        $changstatus="";

        if($response=$errormsg = $result_cancel['Response']['ResponseStatus']==1)
        {
            $changerequestid= $result_cancel['Response']['TicketCRInfo'][0]['ChangeRequestId'];
            $changstatus=$result_cancel['Response']['TicketCRInfo'][0]['ChangeRequestStatus'];
            $form_cancel_again = array(
                'EndUserIp'=>$EndUserIp,
                'TokenId'=>$TokenId,
                'ChangeRequestId'=>$changerequestid,
            );
            $data_flightl_cancel_again = json_encode($form_cancel_again);

            $ch = curl_init('https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/GetChangeRequestStatus/');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_flightl_cancel_again);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: '.strlen($data_flightl_cancel_again)) );
            $result_again = curl_exec($ch);
            $result_cancel1=json_decode($result_again,true);


            // echo "<hr>";
            $errormsg="";
            $flight_cancel = tbl_mytrip::where('bookingid', $bookingid)->update(
                [
                    'change_request_id' => $changerequestid,
                    'changerequest_status' => $changstatus,
                    'cancel_error' => $errormsg,
                    'cancel_data' => $cancel_data,
                    'cancel_date' => $create_date,
                    'cancel_time' => $create_time,
                    'cancelstatus'=>'1',

                ]);
            $flightc = tbl_flight_book::where('bookingid', $bookingid)->update(['flight_cancel' => '1']);
            $exist= DB::table('tbl_user')->where('user_email', $useremail)->first();
            $hotelsms=Cookie::get('gensetting');
            $hl = explode("-", $hotelsms->sms_active);
            $sms_user=$hotelsms->sms_user_id;
            $sms_password=$hotelsms->sms_password;
            $sms_sender=$hotelsms->sms_sender;
            $sms_url=$hotelsms->sms_url;
            if($hl[2]=='1')
            {

                $leadphone1=$exist->user_phone;
                $user=$sms_user;
                $password=$sms_password;
                $sender=$sms_sender;

                $text="Dear ".$exist->user_fname." ".$exist->user_lname." Your flight ticket has been cancelled. your change request id is ".$changerequestid." ";
                $otp=urlencode("$text");
                $data = array(
                    'usr' => $user,
                    'pwd' => $password,
                    'ph'  => $leadphone1,
                    'sndr'  => $sender,
                    'text'  => $otp
                );
                $ch = curl_init($sms_url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                $result = curl_exec($ch);
            }
            $to=$useremail;
            $subject="Flight Cancel";
            $message="Dear ".$exist->user_fname." ".$exist->user_lname." <br>Your flight ticket has been cancelled. your change request id is ".$changerequestid." Refund amount will be transferred to your account within 7-15 working days ";
            $header="From:ticket@travoweb.com \r \n";
            $header = "From: " . strip_tags('ticket@travoweb.com') . "\r\n";
            $header .= "Reply-To: ". strip_tags('ticket@travoweb.com') . "\r\n";
            $header .= "CC: 'ticket@travoweb.com'\r\n";
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-Type: text/html; charset=UTF-8\r\n";
            $mailtest =mail($to,$subject,$message,$header);


        }
        else
        {
            $errormsg = $result_cancel['Response']['Error']['ErrorMessage'];
            $flight_cancel = tbl_mytrip::where('bookingid', $bookingid)->update(
                [

                    'cancel_error' => $errormsg,
                    'cancel_data' => $cancel_data,
                    'cancel_date' => $create_date,
                    'cancel_time' => $create_time,
                    'cancelstatus'=>'0',

                ]);
        }

        if($flight_cancel)
        {


            echo "successfull%%".$errormsg."%%".$changerequestid."";
        }
        else
        {
            echo $errormsg;
        }
    }
    //endflightcancel
    //checkpdf
    public function price($amount,$taxs,$othervharges,$mainpri)
    {

        $pricecurrency = 'fa-inr';  //$ fa-usd


        if(Cookie::get('country') == 'IN'){
            $priceamount = $amount;
            $taxamount=$taxs;
            $otherchar =$othervharges;
            $fmainprice=$mainpri;
            $pricecurr = 'IN';
        }else{
            if(Cookie::get('country') == 'AU' || Cookie::get('country') == 'NZ'){
                // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'AUD')->first();
                // if(!empty($dataExsists)){
                //  //$price = Price::where('currency', '=', 'AUD')->first();
                //     $dataExsists->amount = $this->convert('AUD','1');
                //     $dataExsists->save();
                // }
                $price = Price::where('currency', '=', 'AUD')->first();
                $priceamount = ($price->amount*$amount)*100;
                $taxamount=($price->amount*$taxs)*100;
                $otherchar =($price->amount*$othervharges)*100;
                $fmainprice =($price->amount*$mainpri)*100;

                $pricecurr = 'AU';
            }else{
                // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'USD')->first();
                // if(!empty($dataExsists)){
                //  //$price = Price::where('currency', '=', 'USD')->first();
                //     $dataExsists->amount = $this->convert('USD','1');
                //     $dataExsists->save();
                // }
                $price = Price::where('currency', '=', 'USD')->first();
                //echo $price->amount; die;
                $priceamount = ($price->amount*$amount)*100;
                $taxamount=($price->amount*$taxs)*100;
                $otherchar =($price->amount*$othervharges)*100;
                $fmainprice =($price->amount*$mainpri)*100;
                $pricecurr = 'US';
            }
            $pricecurrency = 'fa-usd';
        }
        //echo $priceamount;  die;
        return [$priceamount,$pricecurrency,$taxamount,$otherchar,$pricecurr,$fmainprice];
    }
    //mealprice
    public function mealprice($amount,$mainamount)
    {

        $pricecurrency = 'fa-inr';  //$ fa-usd


        if(Cookie::get('country') == 'IN'){
            $priceamount = $amount;
            $mainpriceamount=$mainamount;

            $pricecurr = 'IN';
        }else{
            if(Cookie::get('country') == 'AU' || Cookie::get('country') == 'NZ'){
                // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'AUD')->first();
                // if(!empty($dataExsists)){
                //  //$price = Price::where('currency', '=', 'AUD')->first();
                //     $dataExsists->amount = $this->convert('AUD','1');
                //     $dataExsists->save();
                // }
                $price = Price::where('currency', '=', 'AUD')->first();
                $priceamount = ($price->amount*$amount)*100;
                $mainpriceamount = ($price->amount*$mainamount)*100;


                $pricecurr = 'AU';
            }else{
                // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'USD')->first();
                // if(!empty($dataExsists)){
                //  //$price = Price::where('currency', '=', 'USD')->first();
                //     $dataExsists->amount = $this->convert('USD','1');
                //     $dataExsists->save();
                // }
                $price = Price::where('currency', '=', 'USD')->first();
                //echo $price->amount; die;
                $priceamount = ($price->amount*$amount)*100;
                $mainpriceamount = ($price->amount*$mainamount)*100;


                $pricecurr = 'US';
            }
            $pricecurrency = 'fa-usd';
        }
        //echo $priceamount;  die;
        return [$priceamount,$pricecurrency,$mainpriceamount];
    }
    public function get_current_location(){
        //return "USD";
        if(Cookie::get('country')!== null){
            return Cookie::get('country');
        }else{

            $url = "http://www.geoplugin.net/php.gp?ip=".$_SERVER['REMOTE_ADDR'];
            $request = curl_init();
            $timeOut = 0;
            curl_setopt ($request, CURLOPT_URL, $url);
            curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
            curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
            $response = curl_exec($request);
            curl_close($request);
            if(!empty($response)){
                $response = unserialize($response);
                if(!empty($response['geoplugin_status']) && $response['geoplugin_status'] == '200'){
                    Cookie::queue('country',$response['geoplugin_countryCode'], 1440);
                    return $response['geoplugin_countryCode'];
                }else{
                    Cookie::queue('country','IN', 1440);
                    return "IN";
                }
            }else{
                Cookie::queue('country','IN', 1440);
                return "IN";
            }
        }


        //$loc = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
        //print_r($response); die;
    }

    //  public function convert($to,$amount){
    //     // if(Cookie::get('country') != '')
    //     // {
    //        $from = 'INR';
    //        $url = "https://www.google.com/search?q=".$from.$to;
    //        $request = curl_init();
    //        $timeOut = 0;
    //        curl_setopt ($request, CURLOPT_URL, $url);
    //        curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
    //        curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
    //        curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
    //        $response = curl_exec($request);
    //        curl_close($request);

    //        preg_match('~<span [^>]* id="knowledge-currency__tgt-amount"[^>]*>(.*?)</span>~si', $response, $finalData);


    //        return floatval((floatval(preg_replace("/[^-0-9\.]/","", $finalData[1]))/100) * $amount);
    //    // }
    // }
    public function flightstripepayment(Request $request)
    {
        $useremail=session()->get('travo_useremail');
        $chdata = session()->get('postData');
        $customerName = $chdata['customerName'];
        $orderAmount=$chdata['orderAmount'];
        $create_date=date('Y-m-d');
        $create_time=date('H:i:s');
        $posts = tbl_payment_order::orderBy('id', 'DESC')->first();
        if(count($posts) == 0)
        {
            $orderid="TRAstt-01";
        }
        else
        {
            $chkid =  $posts->id + 1;
            $orderid="TRAstt-0".$chkid;
        }
        $hotelpayment = new tbl_payment_order;
        $hotelpayment->orderid=$orderid;
        $hotelpayment->orderdate=$create_date;
        $hotelpayment->ordertime=$create_time;
        $hotelpayment->loginemail=$useremail;
        $hotelpayment->save();
        \Stripe\Stripe::setApiKey("pk_test_gdcMtVAnKmNzBAuQpSqVsOtj");
        $params = array(
            "testmode"   => "off",
            "private_live_key" => "sk_live_2QLNRClFjDAFUR56avW3gvun",
            "public_live_key"  => "pk_live_LptMMIsCPx0fqef2pi3tlAXk",
            "private_test_key" => "sk_test_yICHd12qJq7YEret7hAxqTWj",
            "public_test_key"  => "pk_test_gdcMtVAnKmNzBAuQpSqVsOtj"
        );

        if ($params['testmode'] == "on") {
            \Stripe\Stripe::setApiKey($params['private_test_key']);
            $pubkey = $params['public_test_key'];
        } else {
            \Stripe\Stripe::setApiKey($params['private_live_key']);
            $pubkey = $params['public_live_key'];
        }

        if(isset($_POST['stripeToken']))
        {
            $daamount=$orderAmount * 100;
            $amount_cents = str_replace(".","",$daamount);  // Chargeble amount
            $invoiceid = $orderid;                      // Invoice ID
            $description = "Invoice #" . $invoiceid . " - " . $invoiceid;

            try {
                $charge = \Stripe\Charge::create(array(
                        "amount" => $amount_cents,
                        "currency" => "usd",
                        "source" => $_POST['stripeToken'],
                        "description" => $description)
                );

                // if ($charge->card->address_zip_check == "fail") {
                //     throw new Exception("zip_check_invalid");
                // } else if ($charge->card->address_line1_check == "fail") {
                //     throw new Exception("address_check_invalid");
                // } else if ($charge->card->cvc_check == "fail") {
                //     throw new Exception("cvc_check_invalid");
                // }
                // Payment has succeeded, no exceptions were thrown or otherwise caught             

                $result = "success";

            } catch(\Stripe\Stripe_CardError $e) {

                $error = $e->getMessage();
                $result = "declined";

            } catch (\Stripe\Stripe_InvalidRequestError $e) {
                $result = "declined";
            } catch (Stripe_AuthenticationError $e) {
                $result = "declined";
            } catch (\Stripe\Stripe_ApiConnectionError $e) {
                $result = "declined";
            } catch (\Stripe\Stripe_Error $e) {
                $result = "declined";
            } catch (Exception $e) {

                if ($e->getMessage() == "zip_check_invalid") {
                    $result = "declined";
                } else if ($e->getMessage() == "address_check_invalid") {
                    $result = "declined";
                } else if ($e->getMessage() == "cvc_check_invalid") {
                    $result = "declined";
                } else {
                    $result = "declined";
                }
            }
            $orderamount = $charge->amount;
            $tstam=$orderamount/100;
            $id = $charge->id;
            "</br>";
            $tstam;
            "</br>";
            $charge->receipt_url;
            $paymentorder = tbl_payment_order::where('orderid', $orderid)->update(
                ['amount' => $tstam,
                    'referenceid' => $charge->id,
                    'tx_status' => $result,
                    'payment_mode' => "Stripe",
                    'receipt_url'=>$charge->receipt_url,
                    'balance_trans'=>$charge->balance_transaction,
                    'orderdate' => $create_date,
                    'ordertime' => $create_time,

                ]);
            if($paymentorder)
            {
                // echo $hotelpayment->tx_status;
                if($result=='success')
                {
                    $status = $request->session()->put('paymentstatus','paymentsuccess');
                    $request->session()->put('paymentref',$charge->id);
                    $request->session()->put('paymentorder',$orderid);


                    // echo "<script>alert('success ');</script>";

                    return redirect()->action('FlightBookController@flight_bookview');
                }
                else
                {

                    $status =$request->session()->put('paymentstatus','paymentfail');
                    echo "<script>alert('Payment Failed ');</script>";
                }


            }
            else
            {

                $status =$request->session()->put('paymentstatus','paymentfail');
                echo "<script>alert('Payment Failed ');</script>";

            }

        }
    }

    //endpdf
} //main funtion loop close
