<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use session;
use DB;
use Cookie;
use App\countrypackage;
use App\tour_package;
use App\Price;
use App\tbl_package_enquiry;
use App\tbl_payment_order;
use App\advbanner;
use Dompdf\Dompdf;
use App\tbl_offer_package;
use PDF;
use App;
use File;
use Mail;
use App\Mail\SendMailable;

use \Stripe;
class Packagecontroller extends Controller
{
	public function chkpckgtype(Request $request)
	    {
	    	$datanew = $request->get('pckgtype');
	    	$checknewdata=countrypackage::where('package_country_status','1')->orderBy('id', 'DESC')->get();
	    	$mainval='<option value="0">Please Select</option>';
	    	foreach ($checknewdata as $vall) 
	    	{
	    		$mainval .="<option value=".$vall->id.">".$vall->statename."</option>";
	    		
	    	}
	    	echo $mainval;
	    }
    public function packageresult(Request $request)
    {
    	$countryvalue = $request->get('pckgtype');
    	$countryname = $request->get('pckgname');
        $create_date=date('Y-m-d');
	    $checknewdata=tour_package::where('pckg_status','1')->where('countryname',$countryname)->where('package_valid_new','>=',$create_date)->orderBy('id', 'DESC')->get();
	    if(count($checknewdata)>0)
	    {
	    	// return view('pages.package-result');
	    	$data=1;
	    	$pricecheck=array();
	    	$i = 0;
	    	foreach ($checknewdata as $cval) {
	    		 $oldprice = $cval->oldprice;
		    	 $newprice= $cval->newprice;
		    	
		    	$price= $this->price($oldprice,$newprice);
		    	$checknewdata[$i]['oldpricenew']=$price[0];
		    	$checknewdata[$i]['newpricenew']=$price[2];
		    	$checknewdata[$i]['pricecurr']=$price[1];
		    $i++;
	    	}
            $advlist=advbanner::get();
             
	    		
	    	// die();
	    	return view('pages.package-result')->with(compact('checknewdata'))->with('data',$data)->with(compact('advlist'));
	    }
    	else
    	{
    		$data=0;
    		return view('pages.package-result')->with('data',$data);
    	}
    }
    public function search_package(Request $request)
    {
        $price=$request->get('price');
        $idval=array();
        $priceicon=$request->get('priceicon');
        $countryname=$request->get("countrysel");
        if($price!='')
        {
            $price_array=explode(',',$price);
        }
        $check=0;
        $checknewdata=tour_package::where('pckg_status','1')->where('countryname',$countryname)->orderBy('id', 'DESC')->get();

        if(count($checknewdata)>0)
        {
            foreach($checknewdata as $chkval) 
            {
               
                $lastprice =  $chkval->newprice;
                for($pr=0;$pr<count($price_array);$pr++)
                {
                    $price_data=explode('-',$price_array[$pr]);

                    if($lastprice>=$price_data[0] && $lastprice<=$price_data[1])
                    {
                        $check++;
                        $idval[]= $chkval->id;
                    }

                }
            }
            $tbldata="";
            for($ht=0;$ht<count($idval);$ht++)
            {
                $checknewdata=tour_package::where('pckg_status','1')->where('countryname',$countryname)->where('id',$idval[$ht])->orderBy('id', 'DESC')->get();
                foreach ($checknewdata as $key => $chkval)
                {
                   $tbldata .='<div class="flight-list listflight">
                                        <div class="row">
                                            <div class="col-sm-5 col-md-4 col-lg-3 price-div">
                                                <div class="flight-name">';
                                                     $pckgimageval= unserialize($chkval['packimage']) ;
                                                     $imgsou =  'assets/uploads/packageimage/'.$pckgimageval[0];
                                                   $tbldata .= '<img src="'.asset($imgsou).'" class="img-responsive pack-img">
                                                </div>
                                            </div>
                                            <div class="col-sm-7 col-md-4 col-lg-3 price-div detail-div" style="padding: 0">
                                               <div class="pkg-detail">
                                                   <p class="pkg-para">'.ucfirst($chkval['packagename']).'</p>
                                                   <span class="pkg-span">'.$chkval['packagedays'].' Days / '.$chkval['packagenight'].' Nights</span>
                                                   
                                                   <div class="rating" style="margin-top: 6px;">';
                                                                             
                                                        for($i=1;$i<=$chkval['packgstar'];$i++)
                                                        {
                                                            $tbldata .= '<span class="fa fa-star checked"></span>';
                                                        }
                                                        
                                                   $tbldata .='</div>
                                                    <div class="extra-pkg">
                                                       <i class="fa fa-glass iconselect"></i>
                                                       <i class="fa fa-bus iconselect"></i>
                                                       <i class="fa fa-cutlery iconselect"></i>
                                                       <i class="fa fa-eye iconselect"></i>
                                                   </div>

                                               </div>
                                            </div>

                                            <div class="col-md-3 pkg-res-md" style="padding: 0;text-align: left">
                                                <div class="pkg-highlight">
                                                    <p class="pkg-para" style="padding: 10px 0 0">Exclusion</p>
                                                    <ul class="highlight">
                                                    <li>';
                                                       
                                                        $ch = ""; 
                                                $tbldata .=''.$ch.' </li></ul>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-3 price-div pkg-sm-res">
                                                <div class="pkg-price">
                                                    <p class="pkp-price-para pkg-sm-res" style="font-size: 22px;"><strike><i class="fa '.$priceicon.'"></i> '.$chkval['oldprice'].'</strike></p>
                                                    <p  class="pkg-sm-res"><dfn><i class="fa '.$priceicon.'" style="color: #f99d1b;"></i>'.$chkval['newprice'].'</dfn><br>Per Person</p>
                                                    <p  class="resp_dnone pkg-sm-res"><b>Valid Till:</b>

                                                        <span class="datecolor ">'.$chkval['packgvalid'].' </span></p>
                                                 
                                                     <a class="pkg-btn" href="'.url('packagedetails/'.$chkval['id'].'%'.$chkval['pckg_id']).'">View Details</a>

                                                </div>

                                            </div>
                                            <a class="pkg-btn res-block" href="'.url('packagedetails/'.$chkval['id'].'%'.$chkval['pckg_id']).'">View Details</a>
                                        </div>
                                     
                              </div>';
                }
            }
            echo $tbldata;
        }
        else
        {
            echo "no";
        }

    }

    public function packagedetails($mainid)
    {
    	// $mainid=$request->get('mainid');
    	// $pckgid=$request->get('pckgid');
          $mid=explode('%',$mainid);
            $mainid1=$mid[0];
            $pckgid=$mid[1];

    	$pckgdetails=tour_package::where('pckg_status','1')->where('id',$mainid1)->where('pckg_id',$pckgid)->first();
    	$i = 0;
	    	
	    		 $oldprice = $pckgdetails->oldprice;
		    	 $newprice= $pckgdetails->newprice;
		    	
		    	$price= $this->price($oldprice,$newprice);
		    	$pckgdetails['oldpricenew']=$price[0];
		    	$pckgdetails['newpricenew']=$price[2];
		    	$pckgdetails['pricecurr']=$price[1];
		    
	    	
    	return view('pages.package-detail')->with(compact('pckgdetails'));
    }
    //enquiry
    public function package_enquiry(Request $request)
    {
    	$firstname=$request->get('firstname');
    	$lastname=$request->get('lastname');
    	$email=$request->get('email');
    	$mobile=$request->get('mobile');
    	$adult=$request->get('adult');
    	$child=$request->get('child');
    	$pckgdate1=$request->get('pckgdate');
		$travel_msg=$request->get('travel_msg');
    	$travel_type=$request->get('travel_type');
    	$specific_date1=$request->get('specific_date');
    	$specific_time=$request->get('specific_time');
    	$pckg_id=$request->get('pckg_id');
    	$pckg_name=$request->get('pckg_name');
    	$pckg_mainid=$request->get('pckg_mainid');
    	$pckg_price=$request->get('pckg_price');
        $currency= Cookie::get('currencycode');
        $alter_mobile=$request->get("alter_mobile");
        $countryname=$request->get("countryname");
        if($specific_date1 !="")
        {
            $date1 = str_replace('/', '-', $specific_date1 );
            $specific_date = date("Y-m-d", strtotime($date1));
        }
        else
        {
            $specific_date="";
        }
        if($pckgdate1 !="")
        {
            $date = str_replace('/', '-', $pckgdate1 );
            $pckgdate = date("Y-m-d", strtotime($date));
           
        }
        else
        {
            $pckgdate="";
        }
        
    	date_default_timezone_set("Asia/Kolkata");
			$dateime=date('Y-m-d H:i:s');
			$create_date=date('Y-m-d');
			$create_time=date('H:i:s');
			$pckg_order = DB::table('tbl_package_enquiry')->count();
            if($pckg_order == 0)
            {
                $pckg_order_new=1;
                $enqid="ENQ-0".$pckg_order_new;
            }
            else
            {
                
                $pckg_order_new=$pckg_order+1;
                $enqid="ENQ-0".$pckg_order_new;
                
            }
           

			$insertarray=array('firstname'=>$firstname,
	    						'lastname'=>$lastname,
	    						'email'=>$email,
	    						'mobile'=>$mobile,
	    						'adult'=>$adult,
	    						'child'=>$child,
	    						'pckgdate'=>$pckgdate,
	    						'travel_msg'=>$travel_msg,
	    						'travel_type'=>$travel_type,
	    						'specific_date'=>$specific_date,
	    						'specific_time'=>$specific_time,
	    						'pckg_id'=>$pckg_id,
	    						'pckg_name'=>$pckg_name,
	    						'pckg_mainid'=>$pckg_mainid,
	    						'pckg_price'=>$pckg_price,
	    						'create_date'=>$create_date,
	    						'create_time'=>$create_time,
                                'currency'=>$currency,
                                'enq_id'=>$enqid,
                                'countryname'=>$countryname,
                                'alter_mobile'=>$alter_mobile,

	    						);
			$insertquery=tbl_package_enquiry::insert($insertarray);
	    	if($insertquery)
	    	{

                
                $customername=$firstname.' '.$lastname;
               
                 $to=$email;
                $subject="[ENQUIRY ID: ".$enqid."#".$pckg_name."] # Travo Web Packages";
                $message="Dear ".$customername." <br><br> Thanks for submitting Package Enquiry. Your request has been created with id ".$enqid." ";
                $header="From:Travo Web <ticket@travoweb.com> \r \n";
                // $header = "From: " . strip_tags('Travo Web <ticket@travoweb.com>') . "\r\n";
                $header .= "Reply-To: ". strip_tags('ticket@travoweb.com') . "\r\n";
                $header .= "CC: 'ticket@travoweb.com'\r\n";
                $header .= "MIME-Version: 1.0\r\n";
                $header .= "Content-Type: text/html; charset=UTF-8\r\n";
                $mailtest =mail($to,$subject,$message,$header);
	    		echo "success%%".$enqid;
	    	}
	    	else
	    	{
	    		echo "fail";
	    	}

    }
    public function sendEmailReminder(Request $request)
    {
        $tbl_id=5;
        
        $enqdetails=tbl_package_enquiry::where('id', $tbl_id)->first();
        $offer_pack=tbl_offer_package::where('tbl_id',$tbl_id)->first();
        $customername=$enqdetails->firstname.' '.$enqdetails->lastname;
           $chkarry=array('enqdetails'=>$enqdetails,
                            'offer_pack'=>$offer_pack,

                            );
             $to=$enqdetails->email;
            Mail::send('pages.newsletter', ['chkarry' => $chkarry], function ($m) use ($enqdetails) {
                $m->from('rohanphp@netwebtechnologies.com', 'Package Booking');
                $enqid="TEST package";
                 $subject="[ENQUIRY ID: ".$enqid."#".$enqdetails->pckg_name."] # Travo Web Packages";
                $m->to($enqdetails->email, $enqdetails->firstname)->subject($subject);
            });
    }
    public static function enqdatalast($enqid)
    {
        $enqdetails=tbl_package_enquiry::where('enq_id', $enqid)->first();
        return $enqdetails;
    }

     public function request_package(Request $request)
    {
        $posts = tbl_payment_order::orderBy('id', 'DESC')->first();
        if(count($posts) == 0)
        {
            $orderid="TRAstt-01";
        }
        else
        {
            $chkid =  $posts->id + 1;
            $orderid="TRAstt-0".$chkid;
        }
        date_default_timezone_set("Asia/Kolkata");
            $dateime=date('Y-m-d H:i:s');
            $create_date=date('Y-m-d');
            $create_time=date('H:i:s');
            $orderCurrency='INR';
            $packgpayment = new tbl_payment_order;
            $packgpayment->orderid=$orderid;
            $packgpayment->orderdate=$create_date;
            $packgpayment->ordertime=$create_time;
            $packgpayment->payment_code='Packages';
            $packgpayment->save();

       return view('pages.request_package')->with('orderid',$orderid);
       
    }
    public function package_response(Request $request)
    {

        $backvalue = request()->all();
        
        $create_date=date('Y-m-d');
        $create_time=date('H:i:s');
        // $orderid = $backvalue['orderId'];
        $neworderid=explode("|",$backvalue['orderId']);
      
      $orderid=$neworderid[0];
       
        $tblid=$neworderid[1];
       

       $booklistdetails=tbl_package_enquiry::where('id', $tblid)->first();
        $bookbalnew=tbl_offer_package::where('tbl_id', $tblid)->orderBy('id', 'DESC')->first();
         $paymentorder = tbl_payment_order::where('orderid', $orderid)->update(
            ['amount' => $backvalue['orderAmount'],
                'referenceid' => $backvalue['referenceId'],
                'tx_status' => $backvalue['txStatus'],
                'payment_mode' => $backvalue['paymentMode'],
                'tx_msg' => $backvalue['txMsg'],
                'tx_time' => $backvalue['txTime'],
                'orderdate' => $create_date,
                'ordertime' => $create_time,
                'ticket_id'=>$tblid,
            ]);
         if($paymentorder)
        {
            if($backvalue['txStatus']=='SUCCESS')
            {
                // $enqupdate=tbl_package_enquiry::where('id',$tblid)->update(['admit_status' => '4']);
                 $request->session()->put('pckgtblid',$tblid);
                if($bookbalnew->offer_balance !=0)
                {
                    $enqupdate=tbl_package_enquiry::where('id',$tblid)->update(['admit_status' => '3']);
                    return redirect()->action('Packagecontroller@package_print');
                    // return redirect()->action('pages.package_print');
                }
                else
                {
                    $enqupdate=tbl_package_enquiry::where('id',$tblid)->update(['admit_status' => '4']);
                    return redirect()->action('Packagecontroller@package_invoice');
                }
              
                echo "<script>alert('Payment Success');</script>";
            }

        }
        else
        {
            echo "<script>alert('Payment Fail');</script>";
        }
       

    }
     public function packages_pay(Request $request)
    {
        $posts = tbl_payment_order::orderBy('id', 'DESC')->first();
        if(count($posts) == 0)
        {
            $orderid="TRAstt-01";
        }
        else
        {
            $chkid =  $posts->id + 1;
            $orderid="TRAstt-0".$chkid;
        }
        date_default_timezone_set("Asia/Kolkata");
            $dateime=date('Y-m-d H:i:s');
            $create_date=date('Y-m-d');
            $create_time=date('H:i:s');
            
            $packgpayment = new tbl_payment_order;
            $packgpayment->orderid=$orderid;
            $packgpayment->orderdate=$create_date;
            $packgpayment->ordertime=$create_time;
            $packgpayment->payment_code='Packages';
            $packgpayment->save();

       return view('pages.packages_pay')->with('orderid',$orderid);
       
    }
   public function packagestripepayment(Request $request)
    {
        $orderid=$request->get('payorderid');
        $orderAmount=$request->get('payorderamount');
        $tblid=$request->get('packagetblid');
        $booklistdetails=tbl_package_enquiry::where('id', $tblid)->first();
        $bookbalnew=tbl_offer_package::where('tbl_id', $tblid)->orderBy('id', 'DESC')->first();
        $create_date=date('Y-m-d');
        $create_time=date('H:i:s');
        
        \Stripe\Stripe::setApiKey("pk_test_gdcMtVAnKmNzBAuQpSqVsOtj");
        $params = array(
            "testmode"   => "on",
            "private_live_key" => "sk_live_xxxxxxxxxxxxxxxxxxxxx",
            "public_live_key"  => "pk_live_xxxxxxxxxxxxxxxxxxxxx",
            "private_test_key" => "sk_test_yICHd12qJq7YEret7hAxqTWj",
            "public_test_key"  => "pk_test_gdcMtVAnKmNzBAuQpSqVsOtj"
        );

        if ($params['testmode'] == "on") {
            \Stripe\Stripe::setApiKey($params['private_test_key']);
            $pubkey = $params['public_test_key'];
        } else {
            \Stripe\Stripe::setApiKey($params['private_live_key']);
            $pubkey = $params['public_live_key'];
        }
         $request->session()->put('pckgtblid',$tblid);
        if(isset($_POST['stripeToken']))
        {
            $daamount=$orderAmount * 100;
            $amount_cents = str_replace(".","",$daamount);  // Chargeble amount
            $invoiceid = $orderid;                      // Invoice ID
            $description = "Invoice #" . $invoiceid . " - " . $invoiceid;

            try {
                $charge = \Stripe\Charge::create(array(
                        "amount" => $amount_cents,
                        "currency" => "usd",
                        "source" => $_POST['stripeToken'],
                        "description" => $description)
                );

                // if ($charge->card->address_zip_check == "fail") {
                //     throw new Exception("zip_check_invalid");
                // } else if ($charge->card->address_line1_check == "fail") {
                //     throw new Exception("address_check_invalid");
                // } else if ($charge->card->cvc_check == "fail") {
                //     throw new Exception("cvc_check_invalid");
                // }
                // Payment has succeeded, no exceptions were thrown or otherwise caught             

                $result = "success";

            } catch(\Stripe\Stripe_CardError $e) {

                $error = $e->getMessage();
                $result = "declined";

            } catch (\Stripe\Stripe_InvalidRequestError $e) {
                $result = "declined";
            } catch (Stripe_AuthenticationError $e) {
                $result = "declined";
            } catch (\Stripe\Stripe_ApiConnectionError $e) {
                $result = "declined";
            } catch (\Stripe\Stripe_Error $e) {
                $result = "declined";
            } catch (Exception $e) {

                if ($e->getMessage() == "zip_check_invalid") {
                    $result = "declined";
                } else if ($e->getMessage() == "address_check_invalid") {
                    $result = "declined";
                } else if ($e->getMessage() == "cvc_check_invalid") {
                    $result = "declined";
                } else {
                    $result = "declined";
                }
            }
            $orderamount = $charge->amount;
            $tstam=$orderamount/100;
            $id = $charge->id;
            "</br>";
            $tstam;
            "</br>";
            $charge->receipt_url;
            $paymentorder = tbl_payment_order::where('orderid', $orderid)->update(
                ['amount' => $tstam,
                    'referenceid' => $charge->id,
                    'tx_status' => $result,
                    'payment_mode' => "Stripe",
                    'receipt_url'=>$charge->receipt_url,
                    'balance_trans'=>$charge->balance_transaction,
                    'orderdate' => $create_date,
                    'ordertime' => $create_time,
                    'ticket_id' => $tblid,
                ]);
            if($paymentorder)
            {
                // echo $hotelpayment->tx_status;
                if($result=='success')
                {
                    
                     $request->session()->put('pckgtblid',$tblid);
                    if($bookbalnew->offer_balance !=0)
                    {
                        $enqupdate=tbl_package_enquiry::where('id',$tblid)->update(['admit_status' => '3']);
                        return redirect()->action('Packagecontroller@package_print');
                        // return redirect()->action('pages.package_print');
                    }
                    else
                    {
                        $enqupdate=tbl_package_enquiry::where('id',$tblid)->update(['admit_status' => '4']);
                        return redirect()->action('Packagecontroller@package_invoice');
                        // return redirect()->action('pages.package_invoice');
                    }
                  
                     echo "<script>alert('success ');</script>";

                    // return redirect()->action('FlightBookController@flight_bookview');
                }
                else
                {

                    // $status =$request->session()->put('paymentstatus','paymentfail');
                    echo "<script>alert('Payment Failed ');</script>";
                }


            }
            else
            {

                // $status =$request->session()->put('paymentstatus','paymentfail');
                echo "<script>alert('Payment Failed ');</script>";

            }

        }
    }
    //endenquiry
    public function package_print(Request $request)
    {
        $tbl_id = session()->get('pckgtblid');
        $paymenttable=tbl_payment_order::where('ticket_id', $tbl_id)->where('payment_code','Packages')->orderBy('id', 'DESC')->first();
        $booklistdetails=tbl_package_enquiry::where('id', $tbl_id)->first();
        $bookbalnew=tbl_offer_package::where('tbl_id', $tbl_id)->orderBy('id', 'DESC')->first();
        return view('pages.package_print')->with('paymenttable',$paymenttable)->with('booklistdetails',$booklistdetails)->with('bookbalnew',$bookbalnew);

    }
    public function package_invoice(Request $request)
    {
         
        $pckb = tbl_package_enquiry::where('pckg_bookid', '!=', '0')->get();
        $pckbcount = $pckb->count();
        if(count($pckbcount) == 0)
        {
            $pckg_bookid="001";
        }
        else
        {
            
            $pckg_bookid="00".$pckbcount;
        }
        $tbl_id = session()->get('pckgtblid');
        $pckagupdate=tbl_package_enquiry::where('id',$tbl_id)->update(['pckg_bookid' => $pckg_bookid]);
        $paymenttable=tbl_payment_order::where('ticket_id', $tbl_id)->where('payment_code','Packages')->orderBy('id', 'DESC')->first();
        $booklistdetails=tbl_package_enquiry::where('id', $tbl_id)->first();
        $mainid=$booklistdetails->pckg_mainid;
        $packagemm=$booklistdetails->email;
         $request->session()->put('packagemm',$packagemm);
        $tour_package=tour_package::where('id',$mainid)->orderBy('id', 'DESC')->first();
        $bookbalnew=tbl_offer_package::where('tbl_id', $tbl_id)->orderBy('id', 'DESC')->first();
        $totaloffer=0;
        $totalpricepack=tbl_offer_package::where('tbl_id', $tbl_id)->orderBy('id', 'DESC')->get();
        foreach ($totalpricepack as $tourchk)
         {
            $totaloffer +=$tourchk->offer_price;
        }
        $mainval=array('paymenttable'=>$paymenttable,
                        'booklistdetails'=>$booklistdetails,
                        'bookbalnew'=>$bookbalnew,
                        'tour_package'=>$tour_package,
                        'totaloffer'=>$totaloffer,

                        );
         $pdf = PDF::loadView('pages.package_invoice_pdf',compact('mainval'));

        Mail::send(['html' => 'pages.pdf-preview'], $mainval, function($message) use($pdf)
        {

            $message->from('sarbjitphp@netwebtechnologies.com', 'Travo Web');

            $message->to(session()->get('packagemm'))->subject('Package Invoice');

            $message->attachData($pdf->output(), "packageinvoice.pdf");
        });

        return view('pages.package_invoice')->with('paymenttable',$paymenttable)->with('booklistdetails',$booklistdetails)->with('bookbalnew',$bookbalnew)->with('tour_package',$tour_package)->with('totaloffer',$totaloffer);

    }

    //currency check
     public function price($amount,$tax)
    {

        $pricecurrency = 'fa-inr';  //$ fa-usd
        // echo Cookie::get('country');

        if(Cookie::get('country') == 'IN'){
            $priceamount = $amount;
            $taxamount=$tax;
        }else{
            if(Cookie::get('country') == 'AU' ){
                // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'AUD')->first();
                // if(!empty($dataExsists)){
                //  //$price = Price::where('currency', '=', 'AUD')->first();
                //     $dataExsists->amount = $this->convert('AUD','1');
                //     $dataExsists->save();
                // }
                $price = Price::where('currency', '=', 'AUD')->first();
                $priceamount = ($price->amount*$amount)*100;
                $taxamount=($price->amount*$tax)*100;
            }else{
                // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'USD')->first();
                // if(!empty($dataExsists)){
                //  //$price = Price::where('currency', '=', 'USD')->first();
                //     $dataExsists->amount = $this->convert('USD','1');
                //     $dataExsists->save();
                // }
                $price = Price::where('currency', '=', 'USD')->first();
                //echo $price->amount; die;
                $priceamount = ($price->amount*$amount)*100;
                $taxamount=($price->amount*$tax)*100;
            }
            $pricecurrency = 'fa-usd';
        }
        //echo $priceamount;  die;
        return [$priceamount,$pricecurrency,$taxamount];
    }

    public function get_current_location(){
        //return "USD";
        if(Cookie::get('country')!== null){
            return Cookie::get('country');
        }else{

            $url = "http://www.geoplugin.net/php.gp?ip=".$_SERVER['REMOTE_ADDR'];
            $request = curl_init();
            $timeOut = 0;
            curl_setopt ($request, CURLOPT_URL, $url);
            curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
            curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
            $response = curl_exec($request);
            curl_close($request);
            if(!empty($response)){
                $response = unserialize($response);
                if(!empty($response['geoplugin_status']) && $response['geoplugin_status'] == '200'){
                    Cookie::queue('country',$response['geoplugin_countryCode'], 1440);
                     Cookie::queue('currencycode',$response['geoplugin_currencyCode'], 1440);
                    return $response['geoplugin_countryCode'];
                }else{
                    Cookie::queue('country','IN', 1440);
                    Cookie::queue('currencycode','INR', 1440);
                    return "IN";
                }
            }else{
                Cookie::queue('country','IN', 1440);
                Cookie::queue('currencycode','INR', 1440);
                return "IN";
            }
        }


        //$loc = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
        //print_r($response); die;
    }

    public function package_pdf(Request $request,$packid)
    {
       $newid=base64_decode($packid);
       $newdata=explode("%%",$newid);
       $newid=$newdata[0];
       $curr=$newdata[1];
       $newprice=$newdata[2];
        $pckgdetails=tour_package::where('pckg_status','1')->where('id',$newid)->first();

        $pdf = PDF::loadView('pages.package_pdf',compact('pckgdetails','curr','newprice'));
        return $pdf->stream(' ITINERARY.pdf');
    }
}
