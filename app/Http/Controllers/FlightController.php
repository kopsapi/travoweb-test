<?php

namespace App\Http\Controllers;
use session;
use DB;
use Cookie;
use App\flight;
use App\Price;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\tbl_general_setting;
use DateTime;
use App\hotel_city;
use App\tbl_token;
use App\tbl_testimonail;
class FlightController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		

        //Cookie::make('himanshu', '123456', 1440);  
		//echo "<pre>"; print_r(Cookie::get('himanshu')); die;
        $gensetting=tbl_general_setting::orderBy('id', 'DESC')->first();
        Cookie::queue('gensetting',$gensetting, 1440);
        //$this->price('2000');
        $this->get_current_location();

        if(session()->get('country1') == '')
        {
            $loc = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));


            $request->session()->put('country1',$loc['geoplugin_countryCode']);
        }

         $sql_token=tbl_token::where('id','1')->orderBy('id','DESC')->first();
            $todaydate = date("Y-m-d") ;
            $create_time=date('H:i:s');
            $todaynewdate=strtotime($todaydate);
            $tokendate=strtotime($sql_token->create_date);
     		$newtoken=$sql_token->token_id;
        //echo "<pre>"; print_r(Cookie::get()); die;
        if(Cookie::get('TokenId')!== null &&   $todaynewdate ==$tokendate && Cookie::get('TokenId')==$newtoken )
        {
            $reviewtestidata=tbl_testimonail::orderBy('id', 'DESC')->get();
            if(!empty($reviewtestidata))
            {
              $tesdata=1;
            }
            else
            {
              $tesdata=0;
            }
           
            return view('pages.index')->with('reviewtestidata',$reviewtestidata)->with('tesdata',$tesdata);
        }
        else
        {
           
            if($todaynewdate==$tokendate)
            {
              $newtokenid=$sql_token->token_id;
              $memberid=$sql_token->MemberId;
                $agencyid=$sql_token->AgencyId;
                $loginname=$sql_token->LoginName;
                $localip=$sql_token->localip;
            

            }
            else
            {
              // $ClientId ='ApiIntegrationNew';
               $ClientId ='tboprod';
              $UserName = 'ATQS339';
              $Password = 'live/tbo-339@';
               // $UserName = 'Sekap';
            // $Password = 'Sekap@123';
              $EndUserIp = '192.168.11.120';
              $form_data = array(
                  'ClientId'=>$ClientId,
                  'UserName'=>$UserName,
                  'Password'=>$Password,
                  'EndUserIp'=>$EndUserIp,
              );
              $data_string = json_encode($form_data);

              // $ch = curl_init('http://api.tektravels.com/SharedServices/SharedData.svc/rest/Authenticate');
              $ch = curl_init('https://api.travelboutiqueonline.com/SharedAPI/SharedData.svc/rest/Authenticate');
              curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
              curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                      'Content-Type: application/json',
                      'Content-Length: ' . strlen($data_string))
              );

              $result = curl_exec($ch);
              $result1=json_decode($result,true);
            

              // $request->session()->put('TokenId', $result1['TokenId']);
              // $request->session()->put('MemberId', $result1['Member']['MemberId']);
              // $request->session()->put('AgencyId', $result1['Member']['AgencyId']);
              // $request->session()->put('LoginName', $result1['Member']['LoginName']);
              if(!empty($result1))
              {


              $res = explode(':',$result1['Member']['LoginDetails']);
              // $request->session()->put('localip', trim($res[3]));
                $sql_update=tbl_token::where('id','1')->update(['token_id' => $result1['TokenId'],'create_date' => $todaydate,'create_time'=>$create_time,'MemberId'=>$result1['Member']['MemberId'],'AgencyId'=>$result1['Member']['AgencyId'],'LoginName'=>$result1['Member']['LoginName'],'localip'=>$res[3] ]);
             		$newtokenid=$result1['TokenId'];
                $memberid=$result1['Member']['MemberId'];
                $agencyid=$result1['Member']['AgencyId'];
                $loginname=$result1['Member']['LoginName'];
                $localip=$res[3];

              
               
           	}

           
          
            }
             Cookie::queue('TokenId',$newtokenid, 1440);
            Cookie::queue('MemberId',$memberid, 1440);
            Cookie::queue('AgencyId',$agencyid, 1440);
            Cookie::queue('LoginName',$loginname, 1440);
            Cookie::queue('localip',trim($localip), 1440);

            // return view('pages.index');
        }
        $reviewtestidata=tbl_testimonail::orderBy('id', 'DESC')->get();
        if(!empty($reviewtestidata))
        {
          $tesdata=1;
        }
        else
        {
          $tesdata=0;
        }
       
        return view('pages.index')->with('reviewtestidata',$reviewtestidata)->with('tesdata',$tesdata);
    }
    public function showmulticity(Request $request)
    {
        $lastid1 = $request->get('lastid');
        $flight_to=$request->get('flight_to');
        $lastid =$lastid1+1;
        echo '<span style="overflow:visible;" class="more_options_list multicity-another-flight search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start multi-div" id="mul-'.$lastid.'">
                <div class="search_item">
                  
                  <div>Flying From</div>
                  <input type="text" class="search_input autocomplete flight_from-'.$lastid.' " name="flight_from[]" required="required" placeholder="City or Airport" autocomplete="off" id="autocomplete"  value="'.$flight_to.'">
                  <i class="fa fa-map-marker fa-icon"></i>
                </div>
                <a href="javascript:void()" class="exchange-icon exchangeicon" id="'.$lastid.'">
                  <i class="fa fa-exchange"></i>
                </a>
                <div class="search_item">
                  <div>Flying To1</div>
                  <input type="text" class="search_input autocomplete flight_to-'.$lastid.'" name="flight_to[]" required="required" placeholder="City or Airport" autocomplete="off" id="autocomplete1" >
                  <i class="fa fa-map-marker fa-icon"></i>
                  
                </div>
                <div class="search_item">
                  <div>Departures</div>
                  <input type="text" class="search_input flight_dep-'.$lastid.' departure" name="flight_dep[]"  placeholder="YYYY-MM-DD" autocomplete="off" id="departure">
                  <i class="fa fa-calendar fa-icon"></i>
                </div>
                <a href="javascript:void()" class="close-icon remove" id="'.$lastid.'">
                  <i class="fa fa-times"></i>
                </a>

                <div class="search_item">
                  &nbsp;
                </div>
                <div class="more_options-'.$lastid.'">
                  <div class="more_options_trigger">
                    <a href="javascript:void()" class="add_more" id="add_more-'.$lastid.'">Add Another Flight</a>
                  </div>
              </div>
              </span>';
        echo "<script src='https://travoweb.com/assets/js/airport.js'></script>";

    }
      //calanderfare
       public function calendar_fare(Request $request)
       {
        $d = date("Y-m-d") ;
        $dayvar=array();
        $dayvar[] = $d."T00:00:00";
        $firstDayNextMonth = date('Y-m-d', strtotime('first day of next month'));
        $dayvar[]=$firstDayNextMonth."T00:00:00";
        $firstDayAfterTwoMonths = date('Y-m-d', strtotime('first day of +2 month'));
        $dayvar[]=$firstDayAfterTwoMonths."T00:00:00";
        $chk=array();
        $newresultarr=array();
        $TokenId=Cookie::get('TokenId');
        $EndUserIp=Cookie::get('localip');
        $JourneyType=$request->get('JourneyType');
        $Origin=$request->get('Origin');
        $Destination=$request->get('Destination');
        $FlightCabinClass=2;
        $PreferredAirlines="";
        for($i=0;$i<3;$i++)
        {
            $form_data = array(
              'EndUserIp'=>$EndUserIp,
              'TokenId'=>$TokenId,
              
              'JourneyType' => $JourneyType,
              'PreferredAirlines' =>$PreferredAirlines,
              'Segments' => [array(
                'Origin' =>$Origin,
                'Destination' =>$Destination,
                'FlightCabinClass' =>$FlightCabinClass,
                'PreferredDepartureTime' =>$dayvar[$i],
                )],
              
              );
         $data_string = json_encode($form_data); 

         $ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/GetCalendarFare/');                                                                      
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(  
                                                                                  
              'Content-Type: application/json',                                                                                
              'Content-Length: ' . strlen($data_string))                                                                       
          );                                                                                                                   
                                                                                                                     
          $result = curl_exec($ch);
          $result1=json_decode($result,true);
            $newresultarr[]=$result1['Response'];
            
          }

          $newvalue=array();
          
          for($i=0;$i<count($newresultarr);$i++)
          {
            $newvalue[]=$newresultarr[$i]['SearchResults'];
          }
            
            $fare='';
            $newcdate='';
            $airlinename='';
              $onlyday='';
              $countday=0;
              $test = count($newvalue);
              for($i=0;$i< count($newvalue);$i++)
              {
                $countday +=count($newvalue[$i]);

                for($j=0;$j<count($newvalue[$i]);$j++)
                {

                  $cdate = $newvalue[$i][$j]['DepartureDate'];
                  $caldate = explode('T', $cdate);
                  $newcaldate=$caldate[0];
                   $newexpdate = explode('-',$newcaldate);
                  $onlyday.=$newexpdate[2].",";
                   $newcdate .= $newexpdate[1]."/".$newexpdate[2]."/".$newexpdate[0].",";
                 $fare .= round($newvalue[$i][$j]['Fare']).",";
                 $airlinename .= $newvalue[$i][$j]['AirlineName'].",";
                }
              }
              // echo $fare;
              $val['fare']=$fare;
                   $val['airlinename']=$airlinename;
                   $val['newcdate']=$newcdate;
                   $val['onlyday']=$onlyday;
                 echo $mainfile= $fare."%%".$newcdate."%%".$countday ."%%".$test;
       }
       //endcalanderfare
    public function flightsearch(Request $request)
    {
       
       
        $arr=array();
        $orign = $request->get("flight_from");
        $destinew1 = $request->get("flight_to");

        $orginexnew = explode('(',$orign[0]);
        $Originn=$orginexnew[0];
        $destin = explode('(',$destinew1[0]);
        echo $modal_show ='
                       <!doctype html>
                       <html lang="en">
                       <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                                             
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
                                    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
        <style>

        .l-img{
            width: 165px;
            display: block;
            margin: auto;
        }
        .s-note,p.request-p {
          text-align: center;
          color: #381967;
          padding-bottom: 10px;
          font-size: 20px;
          font-weight: 500;
          font-family: calibri;
      }
        .c-in1 {
            background: #381967;
            text-align: center;
            color: white;
            font-size: 18px;
            font-weight: 500;
        }

        .loadernew {
            font-size: 8px;
            margin: 0px auto 30px auto;
            width: 11em;
            height: 11em;
            border-radius: 50%;
            background: #ffffff;
            border-top: 6px solid #fa9e1b;
            border-left: 6px solid #fa9e1b;
            border-right: 6px solid #fa9e1b;
            position: relative;
            animation: load3 1.4s linear infinite;
        }



        @keyframes load3 {
            0% {

                transform: rotate(0deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;

            }
            10%{
                transform: rotate(36deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            20%{
                transform: rotate(72deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            30%{
                transform: rotate(108deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            40% {
                transform: rotate(144deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            50% {

                transform: rotate(180deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;

            }
            60%{
                transform: rotate(216deg);

                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            70%{
                transform: rotate(252deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            80%{
                transform: rotate(288deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            90% {

                transform: rotate(324deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            100% {

                transform: rotate(360deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
        }
       body {
    margin: 0;
    background: transparent;
}


        div.modal-content.lc{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
       /* p.g-fl:after {
            position: relative;
            content: "\\f072";
            font-family: FontAwesome;
            font-weight: 900;
            color: #FF904B;
            left: 47%;
            transform: rotate(45deg) translateX(-50%);
            !* top: -18.3%; *!
            bottom: -3px;
            font-size: 29px;
        }*/
     img.fl-img {
    height: 31.1px;
    position: absolute;
    top: 48% !important;
    left: 48%;
}
        .multi-div{
            overflow: visible !important;
        }
        
        @media screen and (max-width:992px){
.modal-content.lc {
    width: 43% !important;
}
}

@media screen and (max-width:775px){
.modal-content.lc {
    width: 50% !important;
}
} 
@media screen and (max-width: 650px){
            .modal-content.lc {
    padding-top: 50px !important;
    width: 90% !important;
     box-shadow: none !important;
}
         
        } 
    </style>
                       </head>
                       <body>
                             <div class="modal fade myModal" id="myModal1" style="height: 100%;font-family: \'Open Sans\', sans-serif;background: aliceblue; margin: 0;box-sizing: border-box;">

                            <div class="modal-dialog " style="">
                            <div class="modal-content lc" style="background:white;padding-top:60px;width:32%; height:auto;margin:0 auto;border:none;border-radius:7px;box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.2)">
                                <div class="modal-header" style="border: none;padding-bottom: 0">
                                      <img src="'.asset('assets/images/logo.png').'" class="l-img">
                                 </div>
                                  <div class="modal-body">
                                  <p class="request-p">Please Do Not Close Or Refresh The Window While We Are Searching The Best Flights For You</p>
                                    <div class="row" style="width:100%">
                                        <div class="c-in1" style="width:50%;display:block;float:left;">

                                           <p style="margin: 0; padding: 11px;font-size: 16px;font-weight: 500;" class="g-fl">'.$orginexnew[0].'</p>
                                        </div>
                                        <div class="c-in1" style="width:50%;display:block;float:left;margin-left: -1px;">
                                            <p style="margin: 0; padding: 11px;font-size: 16px;font-weight: 500;">'.$destin [0].'</p>
                                        </div>
                                        <img src="'.asset('assets/images/airplane 1.png').'" class="fl-img">
                                    </div>

                                    <p style="font-size: 17px;font-weight: 600;padding-top: 60px;text-align: center; margin-top: 20px;" class="p-wait">Please Wait...</p>

                                </div>
                                <div class="loadernew"></div>
                            </div>
                                  
                            </div>
                          </div>
                          <script>
                $(document).ready(function(){
                  
                  $("#myModal1").modal("show");
                  });
            </script>
                       </body>
                       </html>
      ';


        $NoOfAdults1=0;
        $NoOfChild1=0;
        $NoOfAdultsarray=array();
        $NoOfchildarray=array();
        $childagearray=array();
        $ChildAge=array();
        $dep_date=$request->get('flight_dep');

        $departure_date=explode('/',$dep_date[0]);
        $depdate = $departure_date[2]."-".$departure_date[1]."-".$departure_date[0];
        $new_departure_date=$departure_date[2]."-".$departure_date[1]."-".$departure_date[0]."T00:00:00";
        $country=Cookie::get('country');

        $TokenId=Cookie::get('TokenId');
        $AdultCount=$request->get("adults");
        $ChildCount=$request->get("children");
        $EndUserIp=Cookie::get('localip');
        $InfantCount=$request->get("infant");
        $JourneyType=$request->get("one");
        $originnew=$orign[0];
        $orginex = explode('(',$orign[0]);
        $Origin=$orginex[0];
        // $Origin=$request->get("flight_from");

        $destinew=$destinew1[0];
        $desti = explode('(',$destinew1[0]);
        $Destination=$desti[0];
        // $Destination=$request->get("flight_to");

        $flighcalssv = explode('%', $request->get("flight_class"));
        $FlightCabinClass=$flighcalssv[0];
        $FlightCabinName=$flighcalssv[1];
        $PreferredDepartureTime=$new_departure_date;
        $PreferredArrivalTime=$new_departure_date;
        $PreferredAirlines ="";
        $DirectFlight = false;
        $OneStopFlight = false;
        $flight_return1='';

        //hotel set
        // $destin = explode('-',$destinew1[0]);

        // $hoteldest=explode(')',$destin[1]);

        // $destination=trim($hoteldest[0]);

        $CheckInDate1=$request->get('flight_dep');
        $CheckInDate=$CheckInDate1[0];
        $check_in_format=$depdate;
        $datetime = new DateTime($depdate);
        $datetime->modify('+1 day');
        $check_out_format=$datetime->format('Y-m-d');

        $NoOfRooms='1';
        $check_in_day = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $check_in_format.' 0:00:00');
        $check_out_day = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $check_out_format.' 0:00:00');
        $NoOfNights = $check_in_day->diffInDays($check_out_day);
        $PreferredHotel='';
        $MaxRating ='5';
        $MinRating='0';
        $ReviewScore='0';
        $IsNearBySearchAllowed='false';

        // if($destination=="Delhi")
        // {

        //     $destilist=hotel_city::where('Destination',$destination)->where('country','India')->first();
        // }
        // else
        // {
        //     $destilist=hotel_city::where('Destination',$destination)->first();
        // }
        // $destilist=hotel_city::where('Destination',$destination)->first();
        // $CityId =  $destilist->cityid;
        // $CountryCode =  $destilist->countrycode;
        // $statename= $destilist->stateprovince;
        // $countryname= $destilist->country;
        $ResultCount ='0';
        $PreferredCurrency="INR";
        // $GuestNationality=$destilist->countrycode;
        for($st=1;$st<=$ChildCount;$st++)
        {
            $childage[]="10";
        }
        if(!empty($childage))
        {
            $ChildAge1=implode(',',$childage);
            $ChildAge=explode(',',$ChildAge1);
        }
        else
        {
            $ChildAge="";
        }

        $roomguests_array[]=array('NoOfAdults' =>$AdultCount,
            'NoOfChild' =>$ChildCount,
            'ChildAge' =>$ChildAge);
        $NoOfAdults1+=$AdultCount;
        $NoOfChild1+=$ChildCount;
        $NoOfAdultsarray[]=$AdultCount;
        $NoOfchildarray[]=$ChildCount;
        $childagearray[]=$ChildAge;
        //hotelend
        if($JourneyType=='1')
        {
            $form_data = array(
                'EndUserIp'=>$EndUserIp,
                'TokenId'=>$TokenId,
                'AdultCount'=>$AdultCount,
                'ChildCount'=>$ChildCount,
                'InfantCount' => $InfantCount,
                'DirectFlight' =>$DirectFlight,
                'OneStopFlight'=>$OneStopFlight,
                'JourneyType' => $JourneyType,
                'PreferredAirlines' =>$PreferredAirlines,
                'Segments' => [array(
                    'Origin' =>$Origin,
                    'Destination' =>$Destination,
                    'FlightCabinClass' =>$FlightCabinClass,
                    'PreferredDepartureTime' =>$PreferredDepartureTime,
                    'PreferredArrivalTime'=>$PreferredArrivalTime,

                )],

            );
        }
        else if($JourneyType=='2')
        {
            $flight_return1=$request->get('flight_return');
            $flightretuen=explode('/',$flight_return1);
            $new_flightretuen=$flightretuen[2]."-".$flightretuen[1]."-".$flightretuen[0]."T00:00:00";
            $form_data = array(
                'EndUserIp'=>$EndUserIp,
                'TokenId'=>$TokenId,
                'AdultCount'=>$AdultCount,
                'ChildCount'=>$ChildCount,
                'InfantCount' => $InfantCount,
                'DirectFlight' =>$DirectFlight,
                'OneStopFlight'=>$OneStopFlight,
                'JourneyType' => $JourneyType,
                'PreferredAirlines' =>$PreferredAirlines,
                'Segments' => [array(
                    'Origin' =>$Origin,
                    'Destination' =>$Destination,
                    'FlightCabinClass' =>$FlightCabinClass,
                    'PreferredDepartureTime' =>$PreferredDepartureTime,
                    'PreferredArrivalTime'=>$PreferredArrivalTime,

                ),
                    array(
                        'Origin' =>$Destination,
                        'Destination' =>$Origin,
                        'FlightCabinClass' =>$FlightCabinClass,
                        'PreferredDepartureTime' =>$new_flightretuen,
                        'PreferredArrivalTime'=>$new_flightretuen,
                    )],

            );
        }
        else
        {

            for($multi=0;$multi<count($orign);$multi++)
            {

                $orginex = explode('(',$orign[$multi]);
                $orign1 =  $orginex[0];
                $desti1 = explode('(',$destinew1[$multi]);
                $Destination1=$desti1[0];
                $departure_date=explode('/',$dep_date[$multi]);
                $depdate = $departure_date[2]."-".$departure_date[1]."-".$departure_date[0];
                $new_departure_date1=$departure_date[2]."-".$departure_date[1]."-".$departure_date[0]."T00:00:00";


                $chkmultiarray[]= array(
                    'Origin' =>$orign1,
                    'Destination' =>$Destination1,
                    'FlightCabinClass' =>$FlightCabinClass,
                    'PreferredDepartureTime' =>$new_departure_date1,
                    'PreferredArrivalTime'=>$new_departure_date1,

                );
            }

            $form_data = array(
                'EndUserIp'=>$EndUserIp,
                'TokenId'=>$TokenId,
                'AdultCount'=>$AdultCount,
                'ChildCount'=>$ChildCount,
                'InfantCount' => $InfantCount,
                'DirectFlight' =>$DirectFlight,
                'OneStopFlight'=>$OneStopFlight,
                'JourneyType' => $JourneyType,
                'PreferredAirlines' =>$PreferredAirlines,
                'Segments' => $chkmultiarray,

            );
        }

        $data_string = json_encode($form_data);

        $ch = curl_init('https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Search/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);

        $result1=json_decode($result,true);
        //hotelapi search
        // $form_data_hotel = array(
        //     'CheckInDate'=>$CheckInDate,
        //     'NoOfNights'=>$NoOfNights,
        //     'CountryCode' => $CountryCode,
        //     'CityId' =>$CityId,
        //     'ResultCount'=>$ResultCount,
        //     'PreferredCurrency' => $PreferredCurrency,
        //     'GuestNationality' =>$GuestNationality,
        //     'NoOfRooms' =>$NoOfRooms,
        //     'RoomGuests' =>$roomguests_array,
        //     'PreferredHotel'=>$PreferredHotel,
        //     'MaxRating'=>$MaxRating,
        //     'MinRating'=>$MinRating,
        //     'ReviewScore'=>null,
        //     'IsNearBySearchAllowed'=>$IsNearBySearchAllowed,
        //     'EndUserIp'=>$EndUserIp,
        //     'TokenId'=>$TokenId,
        // );



        // $data_string_hotel = json_encode($form_data_hotel);
        // // print_r($data_string_hotel);
        // $ch = curl_init('http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelResult/');
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_hotel);

        // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        //         'Content-Type: application/json',
        //         'Content-Length: ' . strlen($data_string_hotel))
        // );

        // $hotelresult = curl_exec($ch);

        // $hotelresult1=json_decode($hotelresult,true);

        // $hotelmargin1=Cookie::get('gensetting');

        // $hotelmargin = $hotelmargin1->hotel_margin;

        // $hotelarray = !empty($hotelresult1['HotelSearchResult'])?$hotelresult1['HotelSearchResult']:array();

        // if(!empty($hotelarray))
        // {

        //     if($hotelarray['Error']['ErrorCode']==0)
        //     {

        //         for($ht=0;$ht<count($hotelarray['HotelResults']);$ht++)
        //         {
        //             $mainp = $hotelarray['HotelResults'][$ht]['Price']['PublishedPriceRoundedOff'];
        //             $margin = ($mainp * $hotelmargin)/100;
        //             $mainprice = $mainp + $margin;
        //             $price= $this->price($mainprice,0,0,0,0);
        //             $hotelmainprice = $price[0] ;
        //             $hotelmaincurrecny = $price[1];
        //             $hotelarray['HotelResults'][$ht]['hotelmainprice']=$hotelmainprice;
        //             $hotelarray['HotelResults'][$ht]['hotelmaincurrecny']=$hotelmaincurrecny;
        //             $hotelarray['HotelResults'][$ht]['hotelmargin']=$mainprice;
        //             $hotelarray['HotelResults'][$ht]['hotellastprice']=$mainp;
        //             $hotelarray['HotelResults'][$ht]['hotelconvercurr']=$price[2];




        //         }
        //     }
        //     $TraceId =  $hotelresult1['HotelSearchResult']['TraceId'];
        //     $request->session()->put('hotel_traceid',$TraceId);

        //     $request->session()->put('resultarray',$hotelarray);
        //     $submitted_data=array("City"=>$destination,
        //         "State"=>$statename,
        //         "Country"=>$countryname,
        //         "checkin"=>$check_in_format,
        //         "checkout"=>$check_out_format,
        //         "adult_count"=>$AdultCount,
        //         "child_count"=>$ChildCount,
        //         "no_of_rooms"=>$NoOfRooms,
        //         "guest_nationality"=>$GuestNationality,
        //         "preferred_currency"=>$PreferredCurrency,
        //         "adult_array"=>$NoOfAdultsarray,
        //         "child_array"=>$NoOfchildarray,
        //         "childagearray"=>$childagearray,

        //     );

        //     $request->session()->put('dataarray',$submitted_data);
        // }





        //endhotelapi search

        $message = $result1['Response']['Error']['ErrorMessage'];

        if($result1['Response']['Error']['ErrorCode']!='0')
        {

            echo "<script>alert('".$message."');</script>";

            if($result1['Response']['Error']['ErrorCode']=='6' )
            {
                // return redirect()->intended('/index');
                return view('pages.results');
            }

            else
            {
                // return redirect()->intended('/index');
                return view('pages.results');
            }
            die();
        }
        else
        {
            $res =  $result1['Response']['ResponseStatus'];
            $airportdename1='';
            $airportdeptname='';
            $flight='';
            $airportdeptcityname='';
            $_SESSION['TraceId']=$result1['Response']['TraceId'];
            $request->session()->put('flighttraceid', $_SESSION['TraceId']);
            $flightmargin1=Cookie::get('gensetting');

            $flightmargin = $flightmargin1->flight_margin;
            foreach($result1 as $flightsearch)
            {  //echo "<pre>"; print_r($flightsearch); die;
                $resu = !empty($flightsearch['Results'])?$flightsearch['Results']:array();
                // echo "<pre>";


                if(!empty($resu))
                {
                    for($ma=0;$ma<count($resu);$ma++)
                    { //print_r($flightsearch['Results'][$ma]); die;
                        if(!empty($flightsearch['Results'][$ma])){
                            $i = 0;
                            foreach($flightsearch['Results'][$ma] as $dta )
                            {
                                if(!empty($dta['Fare']['BaseFare']))
                                {
                                    $mainp=$dta['Fare']['BaseFare'];
                                    $margin = ($mainp * $flightmargin)/100;
                                    $mainprice = $mainp + $margin;
                                    $price= $this->price($mainprice,$dta['Fare']['Tax']);

                                    
                                    $resu[$ma][$i]['flight_fare']= $price[0]+$price[2];
                                    $resu[$ma][$i]['flight_base_fare']= $price[0];
                                    $resu[$ma][$i]['flight_base_tax']= $price[2];
                                    $resu[$ma][$i]['flightcurrency']= $price[1];
                                    $resu[$ma][$i]['flightmargin']= $mainprice;
                                    $resu[$ma][$i]['flightmainprice']= $mainp;
                                    // $resu[$ma][$i]['flight_fare'] = $this->convert('USD',round($dta['Fare']['BaseFare'] + $dta['Fare']['Tax']));
                                    // $resu[$ma][$i]['flight_fare']=round($dta['Fare']['BaseFare'] + $dta['Fare']['Tax']);
                                }else{

                                    $resu[$ma][$i]['flight_fare'] = '0';

                                }

                                if(!empty($dta['Segments'][0][0]['Origin']['DepTime'])){
                                    $devdate = explode('T',$dta['Segments'][0][0]['Origin']['DepTime']);
                                    $resu[$ma][$i]['time_filter'] = date("H:i:s" , strtotime($devdate[1]));
                                }else{
                                    $resu[$ma][$i]['time_filter'] = '1';
                                }
                                //print_r($resu); die;
                                // alsi flight name
                                if(!empty($dta['Segments'][0]) && count($dta['Segments'][0]) == 1 ){
                                    $resu[$ma][$i]['multi_stop'] = '0';
                                }else{
                                    $resu[$ma][$i]['multi_stop'] = '1';
                                }
                                if(!empty($dta['Segments'][0][0]['Airline']['AirlineCode'])){
                                    $resu[$ma][$i]['air_code'] = $dta['Segments'][0][0]['Airline']['AirlineCode'];
                                }else{
                                    $resu[$ma][$i]['air_code'] = '';
                                }

                                $array[] = $dta;

                                $i++;

                            }

                        }
                        // $uniflightsearch= $unique->values()->all();
                        //$array[] = $flightsearch['Results'][$ma];
                    }

                    $minprice='';
                    for($il=0;$il<count($array);$il++)
                    {
                        $unique[]=array( 'name' =>$array[$il]['Segments'][0][0]['Airline']['AirlineName'],

                            'code' => $array[$il]['Segments'][0][0]['Airline']['AirlineCode']
                        );
                        // echo "<pre>"; print_r($array[$il]['Fare']['BaseFare']);

                        $minprice1[]= round( $array[$il]['Fare']['BaseFare'] + $array[$il]['Fare']['Tax']);
                        $maxprice1[]= $array[$il]['Fare']['BaseFare'];
                    }
                    $minprice = min($minprice1);
                    $maxprice = max($minprice1);

                    $unique= array_unique($unique,SORT_REGULAR);
                    // print_r($mainflightq);
                    // die;
                    $request->session()->put('sess_arr', $resu);
                    $request->session()->put('sess_arr_new', $resu);
                    for($seg_i=0;$seg_i<count($flightsearch['Results'][0][0]['Segments'][0]);$seg_i++)
                    {
                        if($flightsearch['Results'][0][0]['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode']==$Destination)
                        {
                            $airportdename1= $flightsearch['Results'][0][0]['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'];
                            $airportdeptname =  $flightsearch['Results'][0][0]['Segments'][0][$seg_i]['Destination']['Airport']['AirportName'];
                            $airportdeptcityname =  $flightsearch['Results'][0][0]['Segments'][0][$seg_i]['Destination']['Airport']['CityName'];
                        }

                    }
                }


                // $airportunique= array_unique($flightsearch['Results'][0][0]['Segments'][0][0]['Airline']['AirlineName']);



            }

            if($res=='1')
            {

                $flightorign = $result1['Response']['Origin'];
                $flightdep =  $result1['Response']['Destination'];

                $airportorignname =  $result1['Response']['Results'][0][0]['Segments'][0][0]['Origin']['Airport']['AirportName'];
                $airportorigncityname =  $result1['Response']['Results'][0][0]['Segments'][0][0]['Origin']['Airport']['CityName'];

                $mainarray = array('AdultCount'=>$AdultCount,
                    'ChildCount'=>$ChildCount,
                    'InfantCount' =>$InfantCount,
                    'flightorign'=>$flightorign,
                    'flightdep' =>$flightdep,
                    'FlightCabinName'=>$FlightCabinName,
                    'depdate'=>$depdate,
                    'dep_date'=>$dep_date,
                    'PreferredArrivalTime'=>$PreferredArrivalTime,
                    'airportorignname' =>$airportorignname,
                    'airportorigncityname'=>$airportorigncityname,
                    'airportdeptname'=>$airportdeptname,
                    'airportdeptcityname'=>$airportdeptcityname,
                    'airportdename1'=>$airportdename1,
                    'flight'=>$flight,
                    'JourneyType' => $JourneyType,
                    'destinew1' =>$destinew1,
                    'orign'=>$orign,
                    'flight_return1' =>$flight_return1,
                    'minprice' =>$minprice,
                    'maxprice' => $maxprice,
                );
            }
            echo '<script>
                    $(document).ready(function(){
                      
                      $("#myModal1").modal("hide");
                      });
                </script>';

            // return view('pages.results')->with(compact('array'))->with(compact('mainarray'))->with('data_string',$data_string)->with(compact('resu'))->with(compact('unique'))->with(compact('hotelarray'))->with(compact('submitted_data'));
                return view('pages.results')->with(compact('array'))->with(compact('mainarray'))->with('data_string',$data_string)->with(compact('resu'))->with(compact('unique'))->with(compact('submitted_data'));

        }//eror close
    }
    public function checkrefunable(Request $request)
    {
        //modalstart

        //modalend
        $chkrefunable = explode(',',$request->get('refund'));
        $flightdep=$request->get('flightdep');
        $flightorign=$request->get('flightorign');
        $count_ref =  count($chkrefunable );
        $stops = explode(',',$request->get('stops'));
        $flightcabinclass=$request->get('flightcabinclass');
        $count_stops =  count($stops);

        $count_air = 0;
        if(!empty($request->get('airlines'))){
            $airlines = explode(',',$request->get('airlines'));
            $count_air= count($airlines);
        }

        $journytype=$request->get('journytype');
        if($journytype=='2')
        {
            $cheapclass='chepflight1';
        }
        else
        {
            $cheapclass='';
        }
        $count_price = 0;
        if(!empty($request->get('price'))){
            $prices = explode(',',$request->get('price'));
            $count_price= count($prices );
        }

        $count_time = 0;
        if(!empty($request->get('time'))){
            $times = explode(',',$request->get('time'));
            $count_time= count($times );
        }



        if(session()->has('sess_arr'))
        {
            $arraynew = session()->get('sess_arr');
            $resu=session()->get('sess_arr_new');
            $value='';
            $value_return='';
            $value_intrreturn='';
            foreach($arraynew[0] as $arrays1)
            {
              if($journytype=='1' || $journytype=='2')
              {


                // price filter
                if($count_price == 1)
                {
                    $min = $max = 0 ;
                    $price_d = explode('-',$prices[0]);

                    if($arrays1['flight_fare'] >= $price_d[0] && $arrays1['flight_fare'] <= $price_d[1]){

                    }else{
                        continue;
                    }
                }

                if($count_price == 2)
                {
                    $min = $max = 0 ;
                    $price_d = explode('-',$prices[0]);
                    $price_d1 = explode('-',$prices[1]);
                    if($arrays1['flight_fare'] >= $price_d[0] && $arrays1['flight_fare'] <= $price_d[1] || $arrays1['flight_fare'] >= $price_d1[0] && $arrays1['flight_fare'] <= $price_d1[1]){

                    }else{
                        continue;
                    }
                }

                if($count_price == 3)
                {
                    $min = $max = 0 ;
                    $price_d = explode('-',$prices[0]);
                    $price_d1 = explode('-',$prices[1]);
                    $price_d2 = explode('-',$prices[2]);
                    if($arrays1['flight_fare'] >= $price_d[0] && $arrays1['flight_fare'] <= $price_d[1] || $arrays1['flight_fare'] >= $price_d1[0] && $arrays1['flight_fare'] <= $price_d1[1] || $arrays1['flight_fare'] >= $price_d2[0] && $arrays1['flight_fare'] <= $price_d2[1] ){

                    }else{
                        continue;
                    }
                }

                // time filter
                if($count_time == 1){
                    $time_d = explode('-',$times[0]);
                    if($arrays1['time_filter'] >= $time_d[0] && $arrays1['time_filter'] <= $time_d[1]){

                    }else{
                        continue;
                    }
                }

                if($count_time == 2){
                    $time_d = explode('-',$times[0]);
                    $time_d1 = explode('-',$times[1]);
                    if($arrays1['time_filter'] >= $time_d[0] && $arrays1['time_filter'] <= $time_d[1] || $arrays1['time_filter'] >= $time_d1[0] && $arrays1['time_filter'] <= $time_d1[1] ){

                    }else{
                        continue;
                    }
                }

                if($count_time == 3){
                    $time_d = explode('-',$times[0]);
                    $time_d1 = explode('-',$times[1]);
                    $time_d2 = explode('-',$times[2]);
                    if($arrays1['time_filter'] >= $time_d[0] && $arrays1['time_filter'] <= $time_d[1] || $arrays1['time_filter'] >= $time_d1[0] && $arrays1['time_filter'] <= $time_d1[1] || $arrays1['time_filter'] >= $time_d2[0] && $arrays1['time_filter'] <= $time_d2[1] ){

                    }else{
                        continue;
                    }
                }

                if($count_time == 4){
                    $time_d = explode('-',$times[0]);
                    $time_d1 = explode('-',$times[1]);
                    $time_d2 = explode('-',$times[2]);
                    $time_d3 = explode('-',$times[3]);
                    if($arrays1['time_filter'] >= $time_d[0] && $arrays1['time_filter'] <= $time_d[1] || $arrays1['time_filter'] >= $time_d1[0] && $arrays1['time_filter'] <= $time_d1[1] || $arrays1['time_filter'] >= $time_d2[0] && $arrays1['time_filter'] <= $time_d2[1] || $arrays1['time_filter'] >= $time_d3[0] && $arrays1['time_filter'] <= $time_d3[1] ){

                    }else{
                        continue;
                    }
                }

                if($count_ref == '1'){


                    if(in_array('refundable',$chkrefunable)){
                        if($arrays1['IsRefundable'] == '0'){
                            continue;
                        }
                    }
                    if(in_array('non_refundable',$chkrefunable)){
                        if($arrays1['IsRefundable'] == '1'){
                            continue;
                        }
                    }
                }
                if($count_stops == '1'){
                    if(in_array('1',$stops)){
                        if($arrays1['multi_stop'] == '0'){
                            continue;
                        }
                    }
                    if(in_array('2',$stops)){
                        if($arrays1['multi_stop'] == '1'){
                            continue;
                        }
                    }
                }
                // print_r($airlines);
                // echo $count_air;
                // echo $arrays1['air_code'];

                if($count_air >= 1){


                    if(!in_array($arrays1['air_code'], $airlines)){
                        continue;
                    }
                }

                for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
                {
                    $arvdate1 = explode('T',$arrays1['Segments'][0][$seg_i]['Destination']['ArrTime']);
                    $arvtime2 = date("H:s" , strtotime($arvdate1[1]));
                    $arvtime1 = date("H:i:s" , strtotime($arvdate1[1]));
                    $arvnewdate1= date("d-M-y" , strtotime($arvdate1[0]));
                }


                $arvnewdate1;
                $devdate = explode('T',$arrays1['Segments'][0][0]['Origin']['DepTime']);
                $depnewdate= date("d-M-y" , strtotime($devdate[0]));
                $depnewtime2 = date("H:s" , strtotime($devdate[1]));
                $depnewtime = date("H:i:s" , strtotime($devdate[1]));
                $newdepnewdate= date("Y-m-d" , strtotime($devdate[0]));

                //arrival time
                $arvdate = explode('T',$arrays1['Segments'][0][0]['Destination']['ArrTime']);
                $arvnewdate= date("d-M-y" , strtotime($arvdate[0]));
                $arvtime = date("H:s:i" , strtotime($arvdate[1]));
                $newarvnewdate= date("Y-m-d" , strtotime($arvnewdate1));
                $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newdepnewdate.''. $depnewtime);
                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newarvnewdate.''. $arvtime1);

                $totalDuration = $from->diffInSeconds($to);
                $durationhour =  gmdate('H', $totalDuration);
                $durationmin =  gmdate('s', $totalDuration);

                $newtime=$durationhour.'H : '.$durationmin.'M';

                $imgsou =  'assets/images/flag/'.$arrays1['Segments'][0][0]['Airline']['AirlineCode'].'.gif';
                $stopcount = count($arrays1['Segments'][0]);
                $uniq = $arrays1['Segments'][0][0]['Airline']['AirlineName'];
                $publishvaleu=round($arrays1['flight_fare'] );
                $mainvalue = number_format($publishvaleu);
                if($stopcount=='1')
                {
                    $mainstopflight = 'Non Stop';
                }
                else
                {
                    $stopcount1 = $stopcount-1;
                    $mainstopflight = $stopcount1.' Stop';
                }
                if(!empty($arrays1['Segments'][0][0]['NoOfSeatAvailable']))
                {

                    $value .='<div class="flight-list flightlist '.$cheapclass.'" id= "'.$arrays1['ResultIndex'].'">
                                                                    <div class="row">
                                            <input type="hidden" value="'.$publishvaleu.'" id="publishprice-'.$arrays1['ResultIndex'].'">
                                                                        <div class="col-md-2">
                                                                            <div class="flight-name">
                                                                                <img src="'.$imgsou.'" class="img-responsive flight-logo" id="flightimg-'.$arrays1['ResultIndex'].'">
                                                                                <p class="flight-name-heading" id="flightname-'.$arrays1['ResultIndex'].'" >'.$arrays1['Segments'][0][0]['Airline']['AirlineName'].'</p>
                                                                                <p id="flightcode-'.$arrays1['ResultIndex'].'">'.$arrays1['Segments'][0][0]['Airline']['AirlineCode'].' - '.$arrays1['Segments'][0][0]['Airline']['FlightNumber'].'</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="flight-depart">
                                                                                <p class="flight-depart-heading" id="flightdeptime-'.$arrays1['ResultIndex'].'">'.$depnewtime2.' | ' .$depnewdate.' </p>';
                    for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
                    {
                        $value .= $flightroute = '<p class="flight-depart-text flightroute-'.$arrays1['ResultIndex'].'" id="flightroute-'.$arrays1['ResultIndex'].'">'.$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode'].' <i class="fa fa-plane fa-rotate-45"></i> '.$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'].'</p>';
                    }

                    $value .='</div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="flight-arrival">
                                                                                <p class="flight-arrival-heading" id="flightarvtime-'.$arrays1['ResultIndex'].'">'.$arvtime2.' | ' .$arvnewdate1.'</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <div class="flight-duration">
                                                                                <p class="flight-duration-heading">'.$newtime.'</p>
                                                                                <p style="color:#008cff">'.$mainstopflight.' </p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <div class="flight-price">
                                                                                <p class="flight-price-heading" id="flightprice-'.$arrays1['ResultIndex'].'"><i class="fa '.$arrays1['flightcurrency'].'"></i>'.$mainvalue.'</p>
                                                                                <p>'.$arrays1['Segments'][0][0]['NoOfSeatAvailable'].' Seats Left</p>
                                                                            </div>
                                                                        </div>
                                                                    
                                                                    </div>
                                                                    <div class="flight-fare">
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <h5 class="fare-rules">Fare Rules</h5>
                                                                            </div>
                                                                            <div class="col-md-4">';
                                                                            if($arrays1['IsRefundable'])
                                                                            {
                                                                                $value.='<h5 class="fare-type refundable"><i class="fa fa-money"></i> Refundable</h5>';
                                                                            }
                                                                            else
                                                                            {
                                                                                $value.='<h5 class="fare-type non-refundable"><i class="fa fa-money"></i> Non-Refundable</h5>';
                                                                            }
                                                        
                                                        
                                                                            $value .=' </div>
                                                                                <div class="col-md-4">
                                                                            <div class="flight-book" style="text-align:right;margin-top:-5px">';
                    if($journytype!='2')
                    {
                        $value .='<form action="'.route("flightbook").'"  method="post">
                                                    <input name="_token" type="hidden" value="'.csrf_token().'"/>
                                                    <input type="hidden" name="journytype" value="'.$journytype.'">
                                                    <input type="hidden" name="resultindex" value="'.$arrays1['ResultIndex'].'">
                                                    <input type="hidden" name="flightcabinclass" value="'.$flightcabinclass.'">
                                                    <input type="hidden" name="flightname" value="'.$arrays1['Segments'][0][0]['Airline']['AirlineName'].'">
                                                    <input type="hidden" name="flightorign" id="flightorign" value="'.$flightorign.'">
                                                    <input type="hidden" name="flightdep" id="flightdep" value="'.$flightdep.'">
                                                    <button class="btn flight-book-btn">Book</button>
                                                  </form>';
                    }

                    $value .='<a href="javascript:void()" class="details"  id="'.$arrays1['ResultIndex'].'">Details</a>
                                                                            </div>
                                                                        </div>
                                                                            
                                                                          
                                                </div>
                                                </div>
                                                <div class="flight-details-tabs" id="flight-'.$arrays1['ResultIndex'].'">
                                                    <ul class="nav nav-tabs">
                                                      <li><a data-toggle="tab" href="#home-'.$arrays1['ResultIndex'].'" class="active">Flight information</a></li>
                                                      <li><a data-toggle="tab" href="#menu1-'.$arrays1['ResultIndex'].'">Fare Details</a></li>
                                                      <li><a data-toggle="tab" href="#menu2-'.$arrays1['ResultIndex'].'">Baggage information</a></li>
                                                      <li><a data-toggle="tab" href="#menu3-'.$arrays1['ResultIndex'].'">Cancellation Rules</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                     <div id="home-'.$arrays1['ResultIndex'].'" class="tab-pane fade-in active">
                                                         <div class="flight-list listflight">
                                                            <div class="row">';

                    for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
                    {
                        $arvdate11 = explode('T',$arrays1['Segments'][0][$seg_i]['Destination']['ArrTime']);
                        $arvtime22 = date("H:s" , strtotime($arvdate11[1]));
                        $arvnewdate11= date("d-M" , strtotime($arvdate11[0]));
                        $devdate1 = explode('T',$arrays1['Segments'][0][$seg_i]['Origin']['DepTime']);
                        $depnewdatedetail= date("d-M" , strtotime($devdate1[0]));
                        $depnewtimedetail = date("H:s" , strtotime($devdate1[1]));

                        $value .='<div class="col-md-4">
                                                                <div class="flight-name">
                                                                  <img src="'.$imgsou.'" class="img-responsive flight-logo">
                                                                  <p class="flight-name-heading">'.$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName'].'</p>
                                                                  <p>'.$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode'].' - '.$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber'].'</p>
                                                                </div>
                                                              </div>
                                                              <div class="col-md-4">
                                                                <div class="flight-depart">
                                                                  
                                                                  <p class="flight-depart-text">'.$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['CityName']. '('.$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode'].')</p>
                                                                  <p class="flight-depart-heading">.'.$depnewdatedetail.' |' .$depnewtimedetail.' </p>
                                                                  
                                                                </div>
                                                              </div>
                                                              <div class="col-md-4">
                                                                <div class="flight-depart">
                                                                  
                                                                  <p class="flight-depart-text">.'.$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['CityName']. '('.$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'].')</p>
                                                                  <p class="flight-depart-heading">'.$arvnewdate11.' | '.$arvtime22.'</p>
                                                                  
                                                                </div>
                                                              </div>';
                    }

                    $value .='</div>
                                                          </div>
                                                     </div>
                                                     <div id="menu1-'.$arrays1['ResultIndex'].'" class="tab-pane fade">
                                                        <table class="table table-bordered">
                                                           <tbody>';
                    $totalbasefare=0;
                    $totaltax=0;
                    $child='';
                    $Infant='';
                    for($fare_i=0;$fare_i<count($arrays1['FareBreakdown']);$fare_i++)
                    {
                        $totalbasefare +=$arrays1['FareBreakdown'][$fare_i]['BaseFare'];
                        $totaltax +=$arrays1['FareBreakdown'][$fare_i]['Tax'];
                        if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='1')
                        {
                            $adults=  $arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Adults';
                        }
                        if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='2' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
                        {
                            $child=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Child';
                        }
                        if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='3' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
                        {
                            $Infant=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Infant';
                        }
                    }
                    $totalfare= $arrays1['flight_base_fare'] + $arrays1['flight_base_tax'];

                    $value .='<tr>
                                                                <td>Base Fare ('.$adults.' '.$child.' '.$Infant.')</td>
                                                                <td><i class="fa '.$arrays1['flightcurrency'].'"></i>'.$arrays1['flight_base_fare'].'</td>
                                                              </tr>
                                                              <tr>
                                                                <td>Taxes and Fees ('.$adults.' '.$child.' '.$Infant.')</td>
                                                                <td><i class="fa '.$arrays1['flightcurrency'].'"></i>'.$arrays1['flight_base_tax'].'</td>
                                                                
                                                              </tr>
                                                              <tr>
                                                                <td>Total Fare Roundoff ('.$adults.' '.$child.' '.$Infant.')</td>
                                                                <td><i class="fa '.$arrays1['flightcurrency'].'"></i>'.round($totalfare).'</td>
                                                               
                                                              </tr>
                                                          </tbody>
                                                          </table>
                                                     </div>
                                                      <div id="menu2-'.$arrays1['ResultIndex'].'" class="tab-pane fade">
                                                        <div class="flight-list listflight">
                                                            <div class="row">';

                    for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
                    {
                        $value .='<div class="col-md-4">
                                                                  <div class="flight-name">
                                                                    <img src="'.$imgsou.'" class="img-responsive flight-logo">
                                                                    <p class="flight-name-heading">'.$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName'].'</p>
                                                                    <p>'.$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode'].' - '.$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber'].'</p>
                                                                  </div>
                                                                </div>
                                                                <table class="table table-bordered">
                                                                   <tbody>
                                                                    <tr>
                                                                      <td>Baggage</td>
                                                                      <td>'.$arrays1['Segments'][0][$seg_i]['Baggage'].'</td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td>Cabin Baggage</td>
                                                                      <td>7 Kg</td>
                                                                    </tr>
                                                                   </tbody>
                                                                </table>';
                    }

                    $value .='</div>
                                                          </div>
                                                     </div>
                                                      <div id="menu3-'.$arrays1['ResultIndex'].'" class="tab-pane fade">
                                                        <h4>Terms & Conditions</h4>
                                                            <ul>
                                                                <li>Airlines stop accepting cancellation/rescheduling requests 24 - 72 hours before departure of the flight, depending on the airline. </li>
                                                                <li>The rescheduling/cancellation fee may also vary based on fluctuations in currency conversion rates.</li>
                                                                <li>Rescheduling Charges = Rescheduling/Change  Penalty + Fare Difference (if applicable)</li>
                                                                <li>In case of restricted cases , no amendments /cancellation allowed. </li>
                                                                <li>Airline penalty needs to be reconfirmed prior to any amendments or cancellation.</li>
                                                                <li>Disclaimer: Airline Penalty changes are indicative and can change without prior notice</li>
                                                                <li>NA means Not Available. Please check with airline for penalty information.</li><li>The charges are per passenger per sector.</li>
                                                            </ul>
                                                     </div>
                                                    </div>
                                                </div> </div>';


                }
              }
              else
              {
                // price filter
                if($count_price == 1)
                {
                    $min = $max = 0 ;
                    $price_d = explode('-',$prices[0]);

                    if($arrays1['flight_fare'] >= $price_d[0] && $arrays1['flight_fare'] <= $price_d[1]){

                    }else{
                        continue;
                    }
                }

                if($count_price == 2)
                {
                    $min = $max = 0 ;
                    $price_d = explode('-',$prices[0]);
                    $price_d1 = explode('-',$prices[1]);
                    if($arrays1['flight_fare'] >= $price_d[0] && $arrays1['flight_fare'] <= $price_d[1] || $arrays1['flight_fare'] >= $price_d1[0] && $arrays1['flight_fare'] <= $price_d1[1]){

                    }else{
                        continue;
                    }
                }

                if($count_price == 3)
                {
                    $min = $max = 0 ;
                    $price_d = explode('-',$prices[0]);
                    $price_d1 = explode('-',$prices[1]);
                    $price_d2 = explode('-',$prices[2]);
                    if($arrays1['flight_fare'] >= $price_d[0] && $arrays1['flight_fare'] <= $price_d[1] || $arrays1['flight_fare'] >= $price_d1[0] && $arrays1['flight_fare'] <= $price_d1[1] || $arrays1['flight_fare'] >= $price_d2[0] && $arrays1['flight_fare'] <= $price_d2[1] ){

                    }else{
                        continue;
                    }
                }

                // time filter
                if($count_time == 1){
                    $time_d = explode('-',$times[0]);
                    if($arrays1['time_filter'] >= $time_d[0] && $arrays1['time_filter'] <= $time_d[1]){

                    }else{
                        continue;
                    }
                }

                if($count_time == 2){
                    $time_d = explode('-',$times[0]);
                    $time_d1 = explode('-',$times[1]);
                    if($arrays1['time_filter'] >= $time_d[0] && $arrays1['time_filter'] <= $time_d[1] || $arrays1['time_filter'] >= $time_d1[0] && $arrays1['time_filter'] <= $time_d1[1] ){

                    }else{
                        continue;
                    }
                }

                if($count_time == 3){
                    $time_d = explode('-',$times[0]);
                    $time_d1 = explode('-',$times[1]);
                    $time_d2 = explode('-',$times[2]);
                    if($arrays1['time_filter'] >= $time_d[0] && $arrays1['time_filter'] <= $time_d[1] || $arrays1['time_filter'] >= $time_d1[0] && $arrays1['time_filter'] <= $time_d1[1] || $arrays1['time_filter'] >= $time_d2[0] && $arrays1['time_filter'] <= $time_d2[1] ){

                    }else{
                        continue;
                    }
                }

                if($count_time == 4){
                    $time_d = explode('-',$times[0]);
                    $time_d1 = explode('-',$times[1]);
                    $time_d2 = explode('-',$times[2]);
                    $time_d3 = explode('-',$times[3]);
                    if($arrays1['time_filter'] >= $time_d[0] && $arrays1['time_filter'] <= $time_d[1] || $arrays1['time_filter'] >= $time_d1[0] && $arrays1['time_filter'] <= $time_d1[1] || $arrays1['time_filter'] >= $time_d2[0] && $arrays1['time_filter'] <= $time_d2[1] || $arrays1['time_filter'] >= $time_d3[0] && $arrays1['time_filter'] <= $time_d3[1] ){

                    }else{
                        continue;
                    }
                }

                if($count_ref == '1'){


                    if(in_array('refundable',$chkrefunable)){
                        if($arrays1['IsRefundable'] == '0'){
                            continue;
                        }
                    }
                    if(in_array('non_refundable',$chkrefunable)){
                        if($arrays1['IsRefundable'] == '1'){
                            continue;
                        }
                    }
                }
                if($count_stops == '1'){
                    if(in_array('1',$stops)){
                        if($arrays1['multi_stop'] == '0'){
                            continue;
                        }
                    }
                    if(in_array('2',$stops)){
                        if($arrays1['multi_stop'] == '1'){
                            continue;
                        }
                    }
                }
                // print_r($airlines);
                // echo $count_air;
                // echo $arrays1['air_code'];

                if($count_air >= 1){


                    if(!in_array($arrays1['air_code'], $airlines)){
                        continue;
                    }
                }

                for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
                {
                    $arvdate1 = explode('T',$arrays1['Segments'][0][$seg_i]['Destination']['ArrTime']);
                    $arvtime2 = date("H:s" , strtotime($arvdate1[1]));
                    $arvtime1 = date("H:i:s" , strtotime($arvdate1[1]));
                    $arvnewdate1= date("d-M-y" , strtotime($arvdate1[0]));
                }


                $arvnewdate1;
                $devdate = explode('T',$arrays1['Segments'][0][0]['Origin']['DepTime']);
                $depnewdate= date("d-M-y" , strtotime($devdate[0]));
                $depnewtime2 = date("H:s" , strtotime($devdate[1]));
                $depnewtime = date("H:i:s" , strtotime($devdate[1]));
                $newdepnewdate= date("Y-m-d" , strtotime($devdate[0]));

                //arrival time
                $arvdate = explode('T',$arrays1['Segments'][0][0]['Destination']['ArrTime']);
                $arvnewdate= date("d-M-y" , strtotime($arvdate[0]));
                $arvtime = date("H:s:i" , strtotime($arvdate[1]));
                $newarvnewdate= date("Y-m-d" , strtotime($arvnewdate1));
                $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newdepnewdate.''. $depnewtime);
                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newarvnewdate.''. $arvtime1);

                $totalDuration = $from->diffInSeconds($to);
                $durationhour =  gmdate('H', $totalDuration);
                $durationmin =  gmdate('s', $totalDuration);

                $newtime=$durationhour.'H : '.$durationmin.'M';

      
                        $imgsou =  'assets/images/flag/'.$arrays1['Segments'][0][0]['Airline']['AirlineCode'].'.gif';

                        $uniq = $arrays1['Segments'][0][0]['Airline']['AirlineName'];
                        if(!empty($arrays1['Segments'][0][0]['NoOfSeatAvailable']))
                        {

                        $publishvaleu=round($arrays1['flight_fare'] );
                        $mainvalue = number_format($publishvaleu);

                        $value.='<div class="flight-list listflight">
                          <!-- show new div -->
                          <div class="row">';
                            $trip=1;
                            for($seg_i=0;$seg_i<count($arrays1['Segments']);$seg_i++)
                            {
                            $arvdate11 = explode('T',$arrays1['Segments'][$seg_i][0]['Destination']['ArrTime']);
                            $arvtime22 = date("H:i" , strtotime($arvdate11[1]));
                            $arvnewdate11= date("d-M" , strtotime($arvdate11[0]));
                            $arvtimedis = date("H:i:s" , strtotime($arvdate11[1]));

                            $arvnewdatedis= date("Y-m-d" , strtotime($arvdate11[0]));
                            $devdate1 = explode('T',$arrays1['Segments'][$seg_i][0]['Origin']['DepTime']);
                            $depnewdatedetail= date("d-M" , strtotime($devdate1[0]));
                            $depnewtimedetail = date("H:i" , strtotime($devdate1[1]));
                            $deptimedis = date("H:i:s" , strtotime($devdate1[1]));
                            $depnewdatedis= date("Y-m-d" , strtotime($devdate1[0]));
                            $stopcountnew = count($arrays1['Segments'][$seg_i]);
                            if($stopcountnew=='1')
                            {
                              $mainstopflight = 'Non Stop';
                            }
                            else
                            {
                              $stopcount1 = $stopcountnew-1;
                              $mainstopflight = $stopcount1.' Stop';

                            }
                            for($seg_ii=0;$seg_ii<count($arrays1['Segments'][$seg_i]);$seg_ii++)
                            {

                              $flightroute= $arrays1['Segments'][$seg_i][$seg_ii]['Origin']['Airport']['CityName'].'('. $arrays1['Segments'][$seg_i][$seg_ii]['Origin']['Airport']['AirportCode']. ') -> '.$arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['CityName'] . '('.$arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['AirportCode'].')';
                              $destimain= $arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['CityName'] . '('.$arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['AirportCode'].')';
                            }
                            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $depnewdatedis.''. $deptimedis);
                            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $arvnewdatedis.''. $arvtimedis);

                            $totalDuration = $from->diffInSeconds($to);
                            $durationhour =  gmdate('H', $totalDuration);
                            $durationmin =  gmdate('s', $totalDuration);

                            $newtimenew=$durationhour.'H : '.$durationmin.'M';

                           
                            $value.='<div class="col-md-2">
                              <h5 class="trip-title">Trip '.($trip++).'</h5>
                            </div>
                            <div class="col-md-3">
                              <div class="flight-name">
                                <img src="'.$imgsou.'" class="img-responsive flight-logo">
                                <p class="flight-name-heading">'.$arrays1['Segments'][$seg_i][0]['Airline']['AirlineName'].'</p>
                                <p>'.$arrays1['Segments'][$seg_i][0]['Airline']['AirlineCode'].' - '.$arrays1['Segments'][$seg_i][0]['Airline']['FlightNumber'].'</p>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="flight-depart">

                                <p class="flight-depart-text">'.$arrays1['Segments'][$seg_i][0]['Origin']['Airport']['CityName'].' ('.$arrays1['Segments'][$seg_i][0]['Origin']['Airport']['AirportCode'].')</p>
                                <p class="flight-depart-heading">'.$depnewdatedetail.'  |  '.$depnewtimedetail.'</p>

                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="flight-duration">
                                <p class="flight-duration-heading">'.$newtimenew.'</p>
                                <p  class="tooltip" style="color:#008cff;opacity: 1 !important">'.$mainstopflight.'
                                  <span class="tooltiptext">'.$flightroute.'</span></p>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="flight-depart">

                                <p class="flight-depart-text">'.$destimain.'</p>
                                <p class="flight-depart-heading">'.$arvnewdate11.' | '.$arvtimedis.' </p>

                              </div>
                            </div>';
                         }
                  
                          $value.='</div>
                          <!-- end show new div -->
                          <div class="row">
                            <div class="col-md-8">
                            <!-- <div class="flight-name">
                              <img src="'.$imgsou.'" class="img-responsive flight-logo">
                              <p class="flight-name-heading">'.$arrays1['Segments'][0][0]['Airline']['AirlineName'].'</p>
                            </div> -->
                            </div>

                            <div class="col-md-2">
                              <div class="flight-price">
                                <p class="flight-price-heading"><i class="fa '.$arrays1['flightcurrency'].'"></i> '.$mainvalue.'</p>
                                <p>'.$arrays1['Segments'][0][0]['NoOfSeatAvailable'].' Seats Left</p>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="flight-book">
                                <form action="'.route('flightbook').'" method="post">
                                  <input name="_token" type="hidden" value="'.csrf_token().'"/>
                                  <input type="hidden" name="resultindex" value="'.$arrays1['ResultIndex'].'">
                                  <input type="hidden" name="journytype" value="'.$journytype.'">
                                  <input type="hidden" name="flightcabinclass" value="'.$flightcabinclass.'">
                                  <input type="hidden" name="flightname" value="'.$arrays1['Segments'][0][0]['Airline']['AirlineName'].'">
                                  <input type="hidden" name="flightorign" id="flightorign" value="'.$flightorign.'">
                                  <input type="hidden" name="flightdep" value="'.$flightdep.'" id="flightdep">
                                  <button class="btn flight-book-btn">Book</button>
                                </form>
                                <a href="javascript:void()" class="details" id="'.$arrays1['ResultIndex'].'" >Details</a>
                              </div>
                            </div>
                          </div>

                          <div class="flight-fare">
                            <div class="row">
                              <div class="col-md-2">
                                <h5 class="fare-rules">Fare Rules</h5>
                              </div>
                              <div class="col-md-7">
                              <!-- <h5 class="airline-mark">Airline Mark : <span> '.$arrays1['AirlineRemark'].'</span></h5> -->
                              </div>
                              <div class="col-md-3">';
                                if($arrays1['IsRefundable'])
                                {

                                  $value.='<h5 class="fare-type refundable"><i class="fa fa-money"></i>Refundable</h5>';
                                }
                                else
                                {
                                  $value.='<h5 class="fare-type non-refundable"><i class="fa fa-money"></i> Non-Refundable</h5>';
                                }

                              $value.='</div>
                            </div>

                          </div>
                          <div class="flight-details-tabs" id="flight-'.$arrays1['ResultIndex'].'">
                            <ul class="nav nav-tabs">
                              <li><a data-toggle="tab" href="#home-'.$arrays1['ResultIndex'].'" class="active">Flight information</a></li>
                              <li><a data-toggle="tab" href="#menu1-'.$arrays1['ResultIndex'].'">Fare Details</a></li>
                              <li><a data-toggle="tab" href="#menu2-'.$arrays1['ResultIndex'].'">Baggage information</a></li>
                              <li><a data-toggle="tab" href="#menu3-'.$arrays1['ResultIndex'].'">Cancellation Rules</a></li>
                            </ul>

                            <div class="tab-content">
                              <div id="home-'.$arrays1['ResultIndex'].'" class="tab-pane fade-in active">
                                <div class="flight-list listflight">
                                  <div class="row">';
      
                                    $trip=1;
                                    for($seg_i=0;$seg_i<count($arrays1['Segments']);$seg_i++)
                                    {
                                    $arvdate11 = explode('T',$arrays1['Segments'][$seg_i][0]['Destination']['ArrTime']);
                                    $arvtime22 = date("H:i" , strtotime($arvdate11[1]));
                                    $arvnewdate11= date("d-M" , strtotime($arvdate11[0]));
                                    $arvtimedis = date("H:i:s" , strtotime($arvdate11[1]));

                                    $arvnewdatedis= date("Y-m-d" , strtotime($arvdate11[0]));
                                    $devdate1 = explode('T',$arrays1['Segments'][$seg_i][0]['Origin']['DepTime']);
                                    $depnewdatedetail= date("d-M" , strtotime($devdate1[0]));
                                    $depnewtimedetail = date("H:i" , strtotime($devdate1[1]));
                                    $deptimedis = date("H:i:s" , strtotime($devdate1[1]));
                                    $depnewdatedis= date("Y-m-d" , strtotime($devdate1[0]));
                                    $stopcountnew = count($arrays1['Segments'][$seg_i]);
                                    if($stopcountnew=='1')
                                    {
                                      $mainstopflight = 'Non Stop';
                                    }
                                    else
                                    {
                                      $stopcount1 = $stopcountnew-1;
                                      $mainstopflight = $stopcount1.' Stop';

                                    }
                                    for($seg_ii=0;$seg_ii<count($arrays1['Segments'][$seg_i]);$seg_ii++)
                                    {

                                      $flightroute= $arrays1['Segments'][$seg_i][$seg_ii]['Origin']['Airport']['CityName'].'('. $arrays1['Segments'][$seg_i][$seg_ii]['Origin']['Airport']['AirportCode']. ') -> '.$arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['CityName'] . '('.$arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['AirportCode'].')';
                                      $destimain= $arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['CityName'] . '('.$arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['AirportCode'].')';
                                    }
                                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $depnewdatedis.''. $deptimedis);
                                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $arvnewdatedis.''. $arvtimedis);

                                    $totalDuration = $from->diffInSeconds($to);
                                    $durationhour =  gmdate('H', $totalDuration);
                                    $durationmin =  gmdate('s', $totalDuration);

                                    $newtimenew=$durationhour.'H : '.$durationmin.'M';

                                    $value.='<div class="col-md-2">
                                      <p>Trip '.($trip++).'</p>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="flight-name">
                                        <img src="'.$imgsou.'" class="img-responsive flight-logo">
                                        <p class="flight-name-heading">'.$arrays1['Segments'][$seg_i][0]['Airline']['AirlineName'].'</p>
                                        <p>'.$arrays1['Segments'][$seg_i][0]['Airline']['AirlineCode'].' - '.$arrays1['Segments'][$seg_i][0]['Airline']['FlightNumber'].'</p>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="flight-depart">

                                        <p class="flight-depart-text">'.$arrays1['Segments'][$seg_i][0]['Origin']['Airport']['CityName'].' ('.$arrays1['Segments'][$seg_i][0]['Origin']['Airport']['AirportCode'].')</p>
                                        <p class="flight-depart-heading">'.$depnewdatedetail.'  |  '.$depnewtimedetail.'</p>
                                      </div>
                                    </div>
                                    <div class="col-md-2">
                                      <div class="flight-duration">
                                        <p class="flight-duration-heading">'.$newtimenew.'</p>
                                        <p  class="tooltip" style="color:#008cff;opacity: 1 !important">'.$mainstopflight.'
                                          <span class="tooltiptext">'.$flightroute.'</span></p>
                                      </div>
                                    </div>
                                    <div class="col-md-2">
                                      <div class="flight-depart">

                                        <p class="flight-depart-text">'.$destimain.')</p>
                                        <p class="flight-depart-heading">'.$arvnewdate11.' | '.$arvtimedis.' </p>

                                      </div>
                                    </div>
                                    <?php }
                                    ?>
                                  </div>
                                </div>
                              </div>
                              <div id="menu1-'.$arrays1['ResultIndex'].'" class="tab-pane fade">
                                <table class="table table-bordered">
                                  <tbody>';
                                  $totalbasefare=0;
                                  $totaltax=0;
                                  $child='';
                                  $Infant='';
                                  for($fare_i=0;$fare_i<count($arrays1['FareBreakdown']);$fare_i++)
                                  {
                                    $totalbasefare +=$arrays1['FareBreakdown'][$fare_i]['BaseFare'];
                                    $totaltax +=$arrays1['FareBreakdown'][$fare_i]['Tax'];
                                    if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='1')
                                    {
                                      $adults=  $arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Adults';
                                    }
                                    if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='2' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
                                    {
                                      $child=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Child';
                                    }
                                    if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='3' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
                                    {
                                      $Infant=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Infant';
                                    }
                                  }
                                  $totalfare= $arrays1['flight_base_fare'] + $arrays1['flight_base_tax'];
                                 
                                  $value.='<tr>
                                    <td>Base Fare ('.$adults.' '.$child.' '.$Infant.')</td>
                                    <td><i class="fa '.$arrays1['flightcurrency'].'"></i> '.$arrays1['flight_base_fare'].'</td>
                                  </tr>
                                  <tr>
                                    <td>Taxes and Fees ('.$adults.' '.$child.' '.$Infant.')</td>
                                    <td><i class="fa '.$arrays1['flightcurrency'].'"></i> '.$arrays1['flight_base_tax'].'</td>

                                  </tr>
                                  <tr>
                                    <td>Total Fare (Roundoff) ('.$adults.' '.$child.' '.$Infant.')</td>
                                    <td><i class="fa '.$arrays1['flightcurrency'].'"></i> '.round($totalfare).'</td>

                                  </tr>
                                  </tbody>
                                </table>
                              </div>
                              <div id="menu2-'.$arrays1['ResultIndex'].'" class="tab-pane fade">
                                <div class="flight-list listflight">
                                  <div class="row">';
                                   
                                    for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
                                    { 
                                    $value.='<div class="col-md-4">
                                      <div class="flight-name">
                                        <img src="'.$imgsou.'" class="img-responsive flight-logo">
                                        <p class="flight-name-heading">'.$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName'].'</p>
                                        <p>'.$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode'].' - '.$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber'].'</p>
                                      </div>
                                    </div>
                                    <table class="table table-bordered">
                                      <tbody>
                                      <tr>
                                        <td>Baggage</td>
                                        <td>'.$arrays1['Segments'][0][$seg_i]['Baggage'].'</td>
                                      </tr>
                                      <tr>
                                        <td>Cabin Baggage</td>
                                        <td>7 Kg
                                          <!-- '.$arrays1['Segments'][0][$seg_i]['CabinBaggage'].' -->
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>';
                                    }
                                    
                                  $value.='</div>
                                </div>
                              </div>
                              <div id="menu3-'.$arrays1['ResultIndex'].'" class="tab-pane fade">
                                <h4>Terms & Conditions</h4>
                                <ul>
                                  <li>Airlines stop accepting cancellation/rescheduling requests 24 - 72 hours before departure of the flight, depending on the airline. </li>
                                  <li>The rescheduling/cancellation fee may also vary based on fluctuations in currency conversion rates.</li>
                                  <li>Rescheduling Charges = Rescheduling/Change  Penalty + Fare Difference (if applicable)</li>
                                  <li>In case of restricted cases , no amendments /cancellation allowed. </li>
                                  <li>Airline penalty needs to be reconfirmed prior to any amendments or cancellation.</li>
                                  <li>Disclaimer: Airline Penalty changes are indicative and can change without prior notice</li>
                                  <li>NA means Not Available. Please check with airline for penalty information.</li><li>The charges are per passenger per sector.</li>
                                </ul>
                              </div>
                            </div>
                          </div>

                        </div>';
                        }
                     }
            

              }
            }
            $arr['value']=$value;
            if($journytype=='2')
            {
                if(!empty($arraynew[1]))
                {
                    foreach($arraynew[1] as $arrays1)
                    {

                        if($count_price >= 1){

                            $min = $max = 0 ;
                            $i = 1;
                            foreach($prices as $pri){
                                $price_d = explode('-',$pri);
                                if(!empty($min)){}else{
                                    $min = $price_d[0];
                                }
                                if($i == $count_price){
                                    $max = $price_d[1];
                                }
                                $i++;
                            }
                            if($arrays1['flight_fare'] >= $min && $arrays1['flight_fare'] <= $max){

                            }else{
                                continue;
                            }

                            //echo $min.'---'.$max; die;
                        }

                        // time filter
                        if($count_time >= 1){

                            $min_time = $max_time = 0 ;
                            $i = 1;
                            foreach($times as $pri){
                                $time_d = explode('-',$pri);
                                if(!empty($min_time)){}else{
                                    $min_time = $time_d[0];
                                }
                                if($i == $count_time){
                                    $max_time = $time_d[1];
                                }
                                $i++;
                            }
                            if($arrays1['time_filter'] >= $min_time && $arrays1['time_filter'] <= $max_time){

                            }else{
                                continue;
                            }

                            //echo $min.'---'.$max; die;
                        }

                        if($count_ref == '1'){


                            if(in_array('refundable',$chkrefunable)){
                                if($arrays1['IsRefundable'] == '0'){
                                    continue;
                                }
                            }
                            if(in_array('non_refundable',$chkrefunable)){
                                if($arrays1['IsRefundable'] == '1'){
                                    continue;
                                }
                            }
                        }
                        if($count_stops == '1'){
                            if(in_array('1',$stops)){
                                if($arrays1['multi_stop'] == '0'){
                                    continue;
                                }
                            }
                            if(in_array('2',$stops)){
                                if($arrays1['multi_stop'] == '1'){
                                    continue;
                                }
                            }
                        }

                        if($count_air >= 1){


                            if(!in_array($arrays1['air_code'], $airlines)){
                                continue;
                            }
                        }

                        for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
                        {
                            $arvdate1 = explode('T',$arrays1['Segments'][0][$seg_i]['Destination']['ArrTime']);
                            $arvtime2 = date("H:s" , strtotime($arvdate1[1]));
                            $arvtime1 = date("H:i:s" , strtotime($arvdate1[1]));
                            $arvnewdate1= date("d-M-y" , strtotime($arvdate1[0]));
                        }


                        $arvnewdate1;
                        $devdate = explode('T',$arrays1['Segments'][0][0]['Origin']['DepTime']);
                        $depnewdate= date("d-M-y" , strtotime($devdate[0]));
                        $depnewtime2 = date("H:s" , strtotime($devdate[1]));
                        $depnewtime = date("H:i:s" , strtotime($devdate[1]));
                        $newdepnewdate= date("Y-m-d" , strtotime($devdate[0]));

                        //arrival time
                        $arvdate = explode('T',$arrays1['Segments'][0][0]['Destination']['ArrTime']);
                        $arvnewdate= date("d-M-y" , strtotime($arvdate[0]));
                        $arvtime = date("H:s:i" , strtotime($arvdate[1]));
                        $newarvnewdate= date("Y-m-d" , strtotime($arvnewdate1));
                        $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newdepnewdate.''. $depnewtime);
                        $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newarvnewdate.''. $arvtime1);

                        $totalDuration = $from->diffInSeconds($to);
                        $durationhour =  gmdate('H', $totalDuration);
                        $durationmin =  gmdate('s', $totalDuration);

                        $newtime=$durationhour.'H : '.$durationmin.'M';

                        $imgsou =  'assets/images/flag/'.$arrays1['Segments'][0][0]['Airline']['AirlineCode'].'.gif';
                        $stopcount = count($arrays1['Segments'][0]);
                        $uniq = $arrays1['Segments'][0][0]['Airline']['AirlineName'];
                        $publishvaleu=round($arrays1['flight_fare'] );
                        $mainvalue = number_format($publishvaleu);
                        if($stopcount=='1')
                        {
                            $mainstopflight = 'Non Stop';
                        }
                        else
                        {
                            $stopcount1 = $stopcount-1;
                            $mainstopflight = $stopcount1.' Stop';
                        }
                        if(!empty($arrays1['Segments'][0][0]['NoOfSeatAvailable']))
                        {

                            $value_return .='<div class="flight-list flightlist chepflight2" id= "'.$arrays1['ResultIndex'].'">
                                                <div class="row">
                                                <input type="hidden" value="'.$publishvaleu.'" id="rpublishprice-'.$arrays1['ResultIndex'].'">
                                                  <div class="col-md-2">
                                                    <div class="flight-name">
                                                      <img src="'.$imgsou.'" class="img-responsive flight-logo" id="rflightimg-'.$arrays1['ResultIndex'].'">
                                                      <p class="flight-name-heading" id="rflightname-'.$arrays1['ResultIndex'].'">'.$arrays1['Segments'][0][0]['Airline']['AirlineName'].'</p>
                                                      <p id="rflightcode-'.$arrays1['ResultIndex'].'">'.$arrays1['Segments'][0][0]['Airline']['AirlineCode'].' - '.$arrays1['Segments'][0][0]['Airline']['FlightNumber'].'</p>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-3">
                                                    <div class="flight-depart">
                                                      <p class="flight-depart-heading" id="rflightdeptime-'.$arrays1['ResultIndex'].'">'.$depnewtime2.' | ' .$depnewdate.' </p>';
                            for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
                            {
                                $value_return .= $flightroute = '<p class="flight-depart-text rflightroute-'.$arrays1['ResultIndex'].'" id="rflightroute-'.$arrays1['ResultIndex'].'">'.$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode'].' <i class="fa fa-plane fa-rotate-45"></i> '.$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'].'</p>';
                            }

                            $value_return .='</div>
                                                  </div>
                                                  <div class="col-md-3">
                                                    <div class="flight-arrival">
                                                      <p class="flight-arrival-heading" id="rflightarvtime-'.$arrays1['ResultIndex'].'">'.$arvtime2.' | ' .$arvnewdate1.'</p>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-2">
                                                    <div class="flight-duration">
                                                      <p class="flight-duration-heading">'.$newtime.'</p>
                                                      <p style="color:#008cff">'.$mainstopflight.' </p>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-2">
                                                    <div class="flight-price">
                                                      <p class="flight-price-heading" id="rflightprice-'.$arrays1['ResultIndex'].'"><i class="fa '.$arrays1['flightcurrency'].'"></i>'.$mainvalue.'</p>
                                                      <p>'.$arrays1['Segments'][0][0]['NoOfSeatAvailable'].' Seats Left</p>
                                                    </div>
                                                  </div>
                                                  
                                                </div>
                                                <div class="flight-fare">
                                                  <div class="row">
                                                    <div class="col-md-4">
                                                      <h5 class="fare-rules">Fare Rules</h5>
                                                    </div>
                                                    <div class="col-md-4">';
                            if($arrays1['IsRefundable'])
                            {
                                $value_return.='<h5 class="fare-type refundable"><i class="fa fa-money"></i> Refundable</h5>';
                            }
                            else
                            {
                                $value_return.='<h5 class="fare-type non-refundable"><i class="fa fa-money"></i> Non-Refundable</h5>';
                            }


                            $value_return .=' </div>
                                                    <div class="col-md-4">
                                                    <div class="flight-book" style="margin-top:-5px;text-align:right">
                                                     
                                                      <a href="javascript:void()" class="details"  id="'.$arrays1['ResultIndex'].'">Details</a>
                                                    </div>
                                                  </div>
                                                   
                                                    
                                                     </div>
                                                </div>
                                                    <div class="flight-details-tabs" id="flight-'.$arrays1['ResultIndex'].'">
                                                        <ul class="nav nav-tabs">
                                                          <li><a data-toggle="tab" href="#home-'.$arrays1['ResultIndex'].'" class="active">Flight information</a></li>
                                                          <li><a data-toggle="tab" href="#menu1-'.$arrays1['ResultIndex'].'">Fare Details</a></li>
                                                          <li><a data-toggle="tab" href="#menu2-'.$arrays1['ResultIndex'].'">Baggage information</a></li>
                                                          <li><a data-toggle="tab" href="#menu3-'.$arrays1['ResultIndex'].'">Cancellation Rules</a></li>
                                                        </ul>
                                                        <div class="tab-content">
                                                         <div id="home-'.$arrays1['ResultIndex'].'" class="tab-pane fade-in active">
                                                             <div class="flight-list listflight">
                                                                <div class="row">';

                            for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
                            {
                                $arvdate11 = explode('T',$arrays1['Segments'][0][$seg_i]['Destination']['ArrTime']);
                                $arvtime22 = date("H:s" , strtotime($arvdate11[1]));
                                $arvnewdate11= date("d-M" , strtotime($arvdate11[0]));
                                $devdate1 = explode('T',$arrays1['Segments'][0][$seg_i]['Origin']['DepTime']);
                                $depnewdatedetail= date("d-M" , strtotime($devdate1[0]));
                                $depnewtimedetail = date("H:s" , strtotime($devdate1[1]));

                                $value_return .='<div class="col-md-4">
                                                                    <div class="flight-name">
                                                                      <img src="'.$imgsou.'" class="img-responsive flight-logo">
                                                                      <p class="flight-name-heading">'.$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName'].'</p>
                                                                      <p>'.$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode'].' - '.$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber'].'</p>
                                                                    </div>
                                                                  </div>
                                                                  <div class="col-md-4">
                                                                    <div class="flight-depart">
                                                                      
                                                                      <p class="flight-depart-text">'.$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['CityName']. '('.$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode'].')</p>
                                                                      <p class="flight-depart-heading">.'.$depnewdatedetail.' |' .$depnewtimedetail.' </p>
                                                                      
                                                                    </div>
                                                                  </div>
                                                                  <div class="col-md-4">
                                                                    <div class="flight-depart">
                                                                      
                                                                      <p class="flight-depart-text">.'.$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['CityName']. '('.$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'].')</p>
                                                                      <p class="flight-depart-heading">'.$arvnewdate11.' | '.$arvtime22.'</p>
                                                                      
                                                                    </div>
                                                                  </div>';
                            }

                            $value_return .='</div>
                                                              </div>
                                                         </div>
                                                         <div id="menu1-'.$arrays1['ResultIndex'].'" class="tab-pane fade">
                                                            <table class="table table-bordered">
                                                               <tbody>';
                            $totalbasefare=0;
                            $totaltax=0;
                            $child=0;
                            $Infant=0;
                            for($fare_i=0;$fare_i<count($arrays1['FareBreakdown']);$fare_i++)
                            {
                                $totalbasefare +=$arrays1['FareBreakdown'][$fare_i]['BaseFare'];
                                $totaltax +=$arrays1['FareBreakdown'][$fare_i]['Tax'];
                                if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='1')
                                {
                                    $adults=  $arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Adults';
                                }
                                if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='2' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
                                {
                                    $child=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Child';
                                }
                                if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='3' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
                                {
                                    $Infant=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Infant';
                                }
                            }
                            $totalfare= $arrays1['flight_base_fare'] + $arrays1['flight_base_tax'];

                            $value_return .='<tr>
                                                                    <td>Base Fare ('.$adults.' '.$child.' '.$Infant.')</td>
                                                                    <td><i class="fa '.$arrays1['flightcurrency'].'"></i>'.$arrays1['flight_base_fare'].'</td>
                                                                  </tr>
                                                                  <tr>
                                                                    <td>Taxes and Fees ('.$adults.' '.$child.' '.$Infant.')</td>
                                                                    <td><i class="fa '.$arrays1['flightcurrency'].'"></i>'.$arrays1['flight_base_tax'].'</td>
                                                                    
                                                                  </tr>
                                                                  <tr>
                                                                    <td>Total Fare ('.$adults.' '.$child.' '.$Infant.')</td>
                                                                    <td><i class="fa '.$arrays1['flightcurrency'].'"></i>'.round($totalfare).'</td>
                                                                   
                                                                  </tr>
                                                              </tbody>
                                                              </table>
                                                         </div>
                                                          <div id="menu2-'.$arrays1['ResultIndex'].'" class="tab-pane fade">
                                                            <div class="flight-list listflight">
                                                                <div class="row">';

                            for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
                            {
                                $value_return .='<div class="col-md-4">
                                                                      <div class="flight-name">
                                                                        <img src="'.$imgsou.'" class="img-responsive flight-logo">
                                                                        <p class="flight-name-heading">'.$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName'].'</p>
                                                                        <p>'.$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode'].' - '.$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber'].'</p>
                                                                      </div>
                                                                    </div>
                                                                    <table class="table table-bordered">
                                                                       <tbody>
                                                                        <tr>
                                                                          <td>Baggage</td>
                                                                          <td>'.$arrays1['Segments'][0][$seg_i]['Baggage'].'</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td>Cabin Baggage</td>
                                                                          <td>7 Kg</td>
                                                                        </tr>
                                                                       </tbody>
                                                                    </table>';
                            }

                            $value_return .='</div>
                                                              </div>
                                                         </div>
                                                          <div id="menu3-'.$arrays1['ResultIndex'].'" class="tab-pane fade">
                                                            <h4>Terms & Conditions</h4>
                                                                <ul>
                                                                    <li>Airlines stop accepting cancellation/rescheduling requests 24 - 72 hours before departure of the flight, depending on the airline. </li>
                                                                    <li>The rescheduling/cancellation fee may also vary based on fluctuations in currency conversion rates.</li>
                                                                    <li>Rescheduling Charges = Rescheduling/Change  Penalty + Fare Difference (if applicable)</li>
                                                                    <li>In case of restricted cases , no amendments /cancellation allowed. </li>
                                                                    <li>Airline penalty needs to be reconfirmed prior to any amendments or cancellation.</li>
                                                                    <li>Disclaimer: Airline Penalty changes are indicative and can change without prior notice</li>
                                                                    <li>NA means Not Available. Please check with airline for penalty information.</li><li>The charges are per passenger per sector.</li>
                                                                </ul>
                                                         </div>
                                                        </div>
                                                    </div>


                                                   
                                                 
                                              </div>';


                        }
                    }
                }
                else
                {
                    //internationalvalue
                    foreach($arraynew[0] as $arrays1)
                    {

                        if($count_price >= 1){

                            $min = $max = 0 ;
                            $i = 1;
                            foreach($prices as $pri){
                                $price_d = explode('-',$pri);
                                if(!empty($min)){}else{
                                    $min = $price_d[0];
                                }
                                if($i == $count_price){
                                    $max = $price_d[1];
                                }
                                $i++;
                            }
                            if($arrays1['flight_fare'] >= $min && $arrays1['flight_fare'] <= $max){

                            }else{
                                continue;
                            }

                            //echo $min.'---'.$max; die;
                        }

                        // time filter
                        if($count_time >= 1){

                            $min_time = $max_time = 0 ;
                            $i = 1;
                            foreach($times as $pri){
                                $time_d = explode('-',$pri);
                                if(!empty($min_time)){}else{
                                    $min_time = $time_d[0];
                                }
                                if($i == $count_time){
                                    $max_time = $time_d[1];
                                }
                                $i++;
                            }
                            if($arrays1['time_filter'] >= $min_time && $arrays1['time_filter'] <= $max_time){

                            }else{
                                continue;
                            }

                            //echo $min.'---'.$max; die;
                        }

                        if($count_ref == '1'){


                            if(in_array('refundable',$chkrefunable)){
                                if($arrays1['IsRefundable'] == '0'){
                                    continue;
                                }
                            }
                            if(in_array('non_refundable',$chkrefunable)){
                                if($arrays1['IsRefundable'] == '1'){
                                    continue;
                                }
                            }
                        }
                        if($count_stops == '1'){
                            if(in_array('1',$stops)){
                                if($arrays1['multi_stop'] == '0'){
                                    continue;
                                }
                            }
                            if(in_array('2',$stops)){
                                if($arrays1['multi_stop'] == '1'){
                                    continue;
                                }
                            }
                        }

                        if($count_air >= 1){


                            if(!in_array($arrays1['air_code'], $airlines)){
                                continue;
                            }
                        }


                        if(!empty($arrays1['Segments'][0][0]['NoOfSeatAvailable']))
                        {

                            $value_intrreturn .='<div class="flight-list listflight chepflight1" id="'.$arrays1['ResultIndex'].'">
                                <input type="hidden" value="'.$publishvaleu.'" id="publishprice-'.$arrays1['ResultIndex'].'">';
                                for($seg_new=0;$seg_new<count($arrays1['Segments']);$seg_new++)
                                {
                                for($seg_i=0;$seg_i<count($arrays1['Segments'][$seg_new]);$seg_i++)
                                {
                                  $arvdate1 = explode('T',$arrays1['Segments'][$seg_new][$seg_i]['Destination']['ArrTime']);
                                  $arvtime2 = date("H:s" , strtotime($arvdate1[1]));
                                  $arvtime1 = date("H:i:s" , strtotime($arvdate1[1]));
                                  $arvnewdate1= date("d-M-y" , strtotime($arvdate1[0]));
                                  $arvnewdatec= date("d-M" , strtotime($arvdate1[0]));
                                }
                                $devdate = explode('T',$arrays1['Segments'][$seg_new][0]['Origin']['DepTime']);
                                $depnewdate= date("d-M" , strtotime($devdate[0]));
                                $depnewtime2 = date("H:s" , strtotime($devdate[1]));
                                $depnewtime = date("H:i:s" , strtotime($devdate[1]));
                                $newdepnewdate= date("Y-m-d" , strtotime($devdate[0]));

                                //arrival time
                                $arvdate = explode('T',$arrays1['Segments'][$seg_new][0]['Destination']['ArrTime']);
                                $arvnewdate= date("d-M-y" , strtotime($arvdate[0]));
                                $arvtime = date("H:s:i" , strtotime($arvdate[1]));
                                $newarvnewdate= date("Y-m-d" , strtotime($arvnewdate1));
                                $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newdepnewdate.''. $depnewtime);
                                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newarvnewdate.''. $arvtime1);

                                $totalDuration = $from->diffInSeconds($to);
                                $durationhour =  gmdate('H', $totalDuration);
                                $durationmin =  gmdate('s', $totalDuration);

                                $newtime=$durationhour.'H : '.$durationmin.'M';

                                $imgsou =  'assets/images/flag/'.$arrays1['Segments'][$seg_new][0]['Airline']['AirlineCode'].'.gif';
                                $stopcount = count($arrays1['Segments'][$seg_new]);
                                $uniq = $arrays1['Segments'][$seg_new][0]['Airline']['AirlineName'];
                                if($stopcount=='1')
                                {
                                  $mainstopflight = 'Non Stop';
                                }
                                else
                                {
                                  $stopcount1 = $stopcount-1;
                                  $mainstopflight = $stopcount1.' Stop';
                                }

                                 $value_intrreturn .='<hr style="width: 100%">
                                <div class="row">
                                  <div class="col-md-3">
                                    <div class="flight-name">
                                      <img src="'.$imgsou.'" class="img-responsive flight-logo" id="flightimg-'.$arrays1['ResultIndex'].'">
                                      <p class="flight-name-heading chkflight" id="flightname-'.$arrays1['ResultIndex'].'">'.$arrays1['Segments'][$seg_new][0]['Airline']['AirlineName'].'</p>
                                      <p id="flightcode-'.$arrays1['ResultIndex'].'">'.$arrays1['Segments'][$seg_new][0]['Airline']['AirlineCode'].' - '.$arrays1['Segments'][$seg_new][0]['Airline']['FlightNumber'].'</p>
                                    </div>
                                  </div>
                                  <div class="col-md-3">
                                    <div class="flight-depart">
                                      <p class="flight-depart-heading" id="flightdeptime-'.$arrays1['ResultIndex'].'">'.$depnewtime2.' | '.$depnewdate.' </p>';

                                     
                                      for($seg_i=0;$seg_i<count($arrays1['Segments'][$seg_new]);$seg_i++)
                                      {
                                         $flightroute = '<p class="flight-depart-text flightroute-'.$arrays1['ResultIndex'].'" id="flightroute-'.$arrays1['ResultIndex'].'">'.$arrays1['Segments'][$seg_new][$seg_i]['Origin']['Airport']['AirportCode'].' <i class="fa fa-plane fa-rotate-45"></i>    '.$arrays1['Segments'][$seg_new][$seg_i]['Destination']['Airport']['AirportCode'].'</p>';
                                      }
                                      
                                     $value_intrreturn .='</div>
                                  </div>
                                  <div class="col-md-3">
                                    <div class="flight-arrival">
                                      <p class="flight-arrival-heading" id="flightarvtime-'.$arrays1['ResultIndex'].'">'.$arvtime2.' | '.$arvnewdatec.'</p>
                                    </div>
                                  </div>
                                  <div class="col-md-3">
                                    <div class="flight-duration">
                                      <p class="flight-duration-heading">'.$newtime.'</p>
                                      <p style="color:#008cff">'.$mainstopflight.'</p>
                                    </div>
                                  </div>
                                <!-- <div class="col-md-2">
                                  <div class="flight-price">
                                    <p class="flight-price-heading" id="flightprice-'.$arrays1['ResultIndex'].'"><i class="fa '.$arrays1['flightcurrency'].'"></i> '.$mainvalue.'</p>
                                    <p> </p>
                                  </div>
                                </div> -->

                                </div>';

                                }

                                

                                 $value_intrreturn .='<div class="row">
                                  <div class="col-md-12">
                                    <div class="flight-book">
                                      <form action="'.route('flightbook').'" method="post">
                                        <input name="_token" type="hidden" value="'. csrf_token() .'"/>
                                        <input type="hidden" id="cheapresultindex" name="resultindex" value="'.$arrays1['ResultIndex'].'">
                                        <input type="hidden" name="flightcabinclass" value="'.$flightcabinclass.'">
                                        <input type="hidden" name="journytype" value="'.$journytype.'">
                                        <input type="hidden" name="flightorign" id="flightorign" value="'.$flightorign.'">
                                        <input type="hidden" name="flightdep" id="flightdep" value="'.$flightdep.'">
                                        <input type="hidden" name="flightname" value="'.$resu[0][0]['Segments'][0][0]['Airline']['AirlineName'].'">

                                        <button class="btn flight-book-btn" style="margin-left: auto;display: block; float: right">Book</button>
                                      </form>
                                      <a href="javascript:void()" class="details" id="'.$arrays1['ResultIndex'].'" style="display: block;text-align: right;padding-right: 5px;padding-top: 8px;float:right; margin-right: 15px;font-size: 14px">Details</a>
                                      <p  style="float:right; display: block; margin: 9px 20px" class="flight-price-heading" id="flightprice-'.$arrays1['ResultIndex'].'"><i class="fa '.$arrays1['flightcurrency'].'"></i> '.$mainvalue.'</p>

                                    </div>
                                  </div>
                                </div>

                                <div class="flight-fare">
                                  <div class="row">
                                    <div class="col-md-3">
                                      <h5 class="fare-rules">Fare Rules</h5>
                                    </div>
                                    <div class="col-md-5">
                                    <!-- <h5 class="airline-mark">Airline Mark : <span> '.$arrays1['AirlineRemark'].'</span></h5> -->
                                    </div>
                                    <div class="col-md-4">';
                                      if($arrays1['IsRefundable'])
                                      {
                                         $value_intrreturn .='<h5 class="fare-type refundable"><i class="fa fa-money"></i> Refundable</h5>';
                                      }
                                      else
                                      {
                                         $value_intrreturn .='<h5 class="fare-type non-refundable"><i class="fa fa-money"></i> Non-Refundable</h5>';
                                      }

                                     $value_intrreturn .='</div>
                                  </div>
                                </div>
                                <div class="flight-details-tabs" id="flight-'.$arrays1['ResultIndex'].'">
                                  <ul class="nav nav-tabs">
                                    <li><a data-toggle="tab" href="#home-'.$arrays1['ResultIndex'].'" class="active">Flight information</a></li>
                                    <li><a data-toggle="tab" href="#menu1-'.$arrays1['ResultIndex'].'">Fare Details</a></li>
                                    <li><a data-toggle="tab" href="#menu2-'.$arrays1['ResultIndex'].'">Baggage information</a></li>
                                    <li><a data-toggle="tab" href="#menu3-'.$arrays1['ResultIndex'].'">Cancellation Rules</a></li>
                                  </ul>

                                  <div class="tab-content">
                                    <div id="home-'.$arrays1['ResultIndex'].'" class="tab-pane fade-in active">
                                      <div class="flight-list listflight">
                                        <div class="row">';
                                       
                                          for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
                                          {
                                          $arvdate11 = explode('T',$arrays1['Segments'][0][$seg_i]['Destination']['ArrTime']);
                                          $arvtime22 = date("H:s" , strtotime($arvdate11[1]));
                                          $arvnewdate11= date("d-M" , strtotime($arvdate11[0]));
                                          $devdate1 = explode('T',$arrays1['Segments'][0][$seg_i]['Origin']['DepTime']);
                                          $depnewdatedetail= date("d-M" , strtotime($devdate1[0]));
                                          $depnewtimedetail = date("H:s" , strtotime($devdate1[1]));
                                          $imgsou =  'assets/images/flag/'.$arrays1['Segments'][0][0]['Airline']['AirlineCode'].'.gif';
                                           $value_intrreturn .='<div class="col-md-4">
                                            <div class="flight-name">
                                              <img src="'.$imgsou.'" class="img-responsive flight-logo">
                                              <p class="flight-name-heading">'.$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName'].'</p>
                                              <p>'.$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode'].' - '.$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber'].'</p>
                                            </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="flight-depart">

                                              <p class="flight-depart-text">'.$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['CityName'].' ('.$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode'].')</p>
                                              <p class="flight-depart-heading">'.$depnewdatedetail.' | '.$depnewtimedetail.' </p>

                                            </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="flight-depart">

                                              <p class="flight-depart-text">'.$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['CityName'].' ('.$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'].')</p>
                                              <p class="flight-depart-heading">'.$arvnewdate11.' | '.$arvtime22.' </p>

                                            </div>
                                          </div>';
                                          }
                                         
                                         
                                          for($seg_i=0;$seg_i<count($arrays1['Segments'][1]);$seg_i++)
                                          {
                                          $arvdate11 = explode('T',$arrays1['Segments'][1][$seg_i]['Destination']['ArrTime']);
                                          $arvtime22 = date("H:s" , strtotime($arvdate11[1]));
                                          $arvnewdate11= date("d-M" , strtotime($arvdate11[1]));
                                          $devdate1 = explode('T',$arrays1['Segments'][1][$seg_i]['Origin']['DepTime']);
                                          $depnewdatedetail= date("d-M" , strtotime($devdate1[1]));
                                          $depnewtimedetail = date("H:s" , strtotime($devdate1[1]));
                                          $imgsou =  'assets/images/flag/'.$arrays1['Segments'][1][0]['Airline']['AirlineCode'].'.gif';
        
                                           $value_intrreturn .='<div class="col-md-4">
                                            <div class="flight-name">
                                              <img src="'.$imgsou.'" class="img-responsive flight-logo">
                                              <p class="flight-name-heading">'.$arrays1['Segments'][1][$seg_i]['Airline']['AirlineName'].'</p>
                                              <p>'.$arrays1['Segments'][1][$seg_i]['Airline']['AirlineCode'].' - '.$arrays1['Segments'][1][$seg_i]['Airline']['FlightNumber'].'</p>
                                            </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="flight-depart">

                                              <p class="flight-depart-text">'.$arrays1['Segments'][1][$seg_i]['Origin']['Airport']['CityName'].' ('.$arrays1['Segments'][1][$seg_i]['Origin']['Airport']['AirportCode'].')</p>
                                              <p class="flight-depart-heading">'.$depnewdatedetail.' | '.$depnewtimedetail.' </p>

                                            </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="flight-depart">

                                              <p class="flight-depart-text">'.$arrays1['Segments'][1][$seg_i]['Destination']['Airport']['CityName'].' ('.$arrays1['Segments'][1][$seg_i]['Destination']['Airport']['AirportCode'].')</p>
                                              <p class="flight-depart-heading">'.$arvnewdate11.' | '.$arvtime22.' </p>

                                            </div>
                                          </div>';
                                           }
                                          
                                         $value_intrreturn .='</div>
                                      </div>
                                    </div>
                                    <div id="menu1-'.$arrays1['ResultIndex'].'" class="tab-pane fade">
                                      <table class="table table-bordered">
                                        <tbody>';
                                       
                                        $totalbasefare=0;
                                        $totaltax=0;
                                        $child='';
                                        $Infant='';
                                        for($fare_i=0;$fare_i<count($arrays1['FareBreakdown']);$fare_i++)
                                        {
                                          $totalbasefare +=$arrays1['FareBreakdown'][$fare_i]['BaseFare'];
                                          $totaltax +=$arrays1['FareBreakdown'][$fare_i]['Tax'];
                                          if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='1')
                                          {
                                            $adults=  $arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Adults';
                                          }
                                          if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='2' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
                                          {
                                            $child=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Child';
                                          }
                                          if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='3' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
                                          {
                                            $Infant=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Infant';
                                          }
                                        }
                                        $totalfare= $arrays1['flight_base_fare'] + $arrays1['flight_base_tax'];
                                       
                                         $value_intrreturn .='<tr>
                                          <td>Base Fare ('.$adults.' '.$child.' '.$Infant.')</td>
                                          <td><i class="fa '.$arrays1['flightcurrency'].'"></i> '.$arrays1['flight_base_fare'].'</td>
                                        </tr>
                                        <tr>
                                          <td>Taxes and Fees ('.$adults.' '.$child.' '.$Infant.')</td>
                                          <td><i class="fa '.$arrays1['flightcurrency'].'"></i> '.$arrays1['flight_base_tax'].'</td>

                                        </tr>
                                        <tr>
                                          <td>Total Fare (Roundoff) ('.$adults.' '.$child.' '.$Infant.')</td>
                                          <td><i class="fa '.$arrays1['flightcurrency'].'"></i> '.round($totalfare).'</td>

                                        </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                    <div id="menu2-'.$arrays1['ResultIndex'].'" class="tab-pane fade">
                                      <div class="flight-list listflight">
                                        <div class="row">';
                                         
                                          for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
                                          { 
                                           $value_intrreturn .='<div class="col-md-4">
                                            <div class="flight-name">
                                              <img src="'.$imgsou.'" class="img-responsive flight-logo">
                                              <p class="flight-name-heading">'.$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName'].'</p>
                                              <p>'.$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode'].' - '.$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber'].'</p>
                                            </div>
                                          </div>
                                          <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                              <td>Baggage</td>
                                              <td>'.$arrays1['Segments'][0][$seg_i]['Baggage'].'</td>
                                            </tr>
                                            <tr>
                                              <td>Cabin Baggage</td>
                                              <td>7 Kg</td>
                                            </tr>
                                            </tbody>
                                          </table>';
                                           }
                                          
                                         $value_intrreturn .='</div>
                                      </div>
                                    </div>
                                    <div id="menu3-'.$arrays1['ResultIndex'].'" class="tab-pane fade">
                                      <h4>Terms & Conditions</h4>
                                      <ul>
                                        <li>Airlines stop accepting cancellation/rescheduling requests 24 - 72 hours before departure of the flight, depending on the airline. </li>
                                        <li>The rescheduling/cancellation fee may also vary based on fluctuations in currency conversion rates.</li>
                                        <li>Rescheduling Charges = Rescheduling/Change  Penalty + Fare Difference (if applicable)</li>
                                        <li>In case of restricted cases , no amendments /cancellation allowed. </li>
                                        <li>Airline penalty needs to be reconfirmed prior to any amendments or cancellation.</li>
                                        <li>Disclaimer: Airline Penalty changes are indicative and can change without prior notice</li>
                                        <li>NA means Not Available. Please check with airline for penalty information.</li><li>The charges are per passenger per sector.</li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>

                              </div>';


                        }
                    }
                    //endinternational value
                }
                
                $arr['value1']=$value_return;
                $arr['value_intrreturn']=$value_intrreturn;

                if($arr['value1']=="")
            {
                $arr['value1']=null;
            }
             if($arr['value_intrreturn']=="")
            {
                $arr['value_intrreturn']=null;
            }

            } //journytype2 end
            
            if($arr['value']=="")
            {
                $arr['value']=null;
            }
             
            echo json_encode($arr);

        }
        else
        {
            return view('pages.index');
        }



        // $stopflight = explode(',',$request->get('stopflight'));

    }
    public function price($amount,$tax)
    {

        $pricecurrency = 'fa-inr';  //$ fa-usd
        // echo Cookie::get('country');

        if(Cookie::get('country') == 'IN'){
            $priceamount = $amount;
            $taxamount=$tax;
        }else{
            if(Cookie::get('country') == 'AU' ){
                // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'AUD')->first();
                // if(!empty($dataExsists)){
                //  //$price = Price::where('currency', '=', 'AUD')->first();
                //     $dataExsists->amount = $this->convert('AUD','1');
                //     $dataExsists->save();
                // }
                $price = Price::where('currency', '=', 'AUD')->first();
                $priceamount = ($price->amount*$amount)*100;
                $taxamount=($price->amount*$tax)*100;
            }else{
                // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'USD')->first();
                // if(!empty($dataExsists)){
                //  //$price = Price::where('currency', '=', 'USD')->first();
                //     $dataExsists->amount = $this->convert('USD','1');
                //     $dataExsists->save();
                // }
                $price = Price::where('currency', '=', 'USD')->first();
                //echo $price->amount; die;
                $priceamount = ($price->amount*$amount)*100;
                $taxamount=($price->amount*$tax)*100;
            }
            $pricecurrency = 'fa-usd';
        }
        //echo $priceamount;  die;
        return [$priceamount,$pricecurrency,$taxamount];
    }

    public function get_current_location(){
        //return "USD";
        if(Cookie::get('country')!== null){
            return Cookie::get('country');
        }else{

            $url = "http://www.geoplugin.net/php.gp?ip=".$_SERVER['REMOTE_ADDR'];
            $request = curl_init();
            $timeOut = 0;
            curl_setopt ($request, CURLOPT_URL, $url);
            curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
            curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
            $response = curl_exec($request);
            curl_close($request);
            if(!empty($response)){
                $response = unserialize($response);
                if(!empty($response['geoplugin_status']) && $response['geoplugin_status'] == '200'){
                    Cookie::queue('country',$response['geoplugin_countryCode'], 1440);
                     Cookie::queue('currencycode',$response['geoplugin_currencyCode'], 1440);
                    return $response['geoplugin_countryCode'];
                }else{
                    Cookie::queue('country','IN', 1440);
                    Cookie::queue('currencycode','INR', 1440);
                    return "IN";
                }
            }else{
                Cookie::queue('country','IN', 1440);
                Cookie::queue('currencycode','INR', 1440);
                return "IN";
            }
        }


        //$loc = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
        //print_r($response); die;
    }

    //  public function convert($to,$amount){
    //     // if(Cookie::get('country') != '')
    //     // {

    //        $from = 'INR';
    //        $url = "https://www.google.com/search?q=".$from.$to;
    //        $request = curl_init();
    //        $timeOut = 0;
    //        curl_setopt ($request, CURLOPT_URL, $url);
    //        curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
    //        curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
    //        curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
    //        $response = curl_exec($request);
    //        curl_close($request);
    //       print_r($response);
    //        preg_match('~<span [^>]* id="knowledge-currency__tgt-amount"[^>]*>(.*?)</span>~si', $response, $finalData);


    //        return floatval((floatval(preg_replace("/[^-0-9\.]/","", $finalData[1]))/100) * $amount);
    //    // }
    // }

    //flight update search

} //end mainfunction


