<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use Cookie;
use App\userlist;
use App\tbl_flight_book;
use App\tbl_hotel_book;
use App\tbl_payment_order;
use App\tbl_mytrip;
use App\tbl_general_setting;
use App\advbanner;
use App\countrypackage;
use App\tour_package;
use App\tbl_package_enquiry;
use App\tbl_offer_package;
use App\tbl_token;
use Mail;
use App\Mail\SendMailable;
use PDF;
use Dompdf\Dompdf;
use App\tbl_query;
use App\tbl_testimonail;
class AdminController extends Controller
{
    public function index()
    {
    	 $sql_token=tbl_token::where('id','1')->orderBy('id','DESC')->first();
            $todaydate = date("Y-m-d") ;
            $create_time=date('H:i:s');
            $todaynewdate=strtotime($todaydate);
            $tokendate=strtotime($sql_token->create_date);
    	if(Cookie::get('TokenId')!== null &&   $todaynewdate ==$tokendate )
        {
          return view('admin.index');
        }
        else
            {
           
             if($todaynewdate==$tokendate)
            {
              $newtokenid=$sql_token->token_id;
              $memberid=$sql_token->MemberId;
                $agencyid=$sql_token->AgencyId;
                $loginname=$sql_token->LoginName;
                $localip=$sql_token->localip;
            

            }
            else
            {

           

            	  $ClientId ='tboprod';
	              $UserName = 'ATQS339';
	              $Password = 'live/tbo-339@';
                // $ClientId ='ApiIntegrationNew';
                // $UserName = 'Sekap';
                // $Password = 'Sekap@123';
                $EndUserIp = '192.168.11.120';
                $form_data = array(
                    'ClientId'=>$ClientId,
                    'UserName'=>$UserName,
                    'Password'=>$Password,
                    'EndUserIp'=>$EndUserIp,
                );
                $data_string = json_encode($form_data);  
                $ch = curl_init('https://api.travelboutiqueonline.com/SharedAPI/SharedData.svc/rest/Authenticate');                                                                      
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                    'Content-Type: application/json',                                                                                
                    'Content-Length: ' . strlen($data_string))                                                                       
              );                                                                                                                   

              $result = curl_exec($ch);
              $result1=json_decode($result,true);
              if(!empty($result1))
	          {
				  $res = explode(':',$result1['Member']['LoginDetails']);
              // $request->session()->put('localip', trim($res[3]));
                $sql_update=tbl_token::where('id','1')->update(['token_id' => $result1['TokenId'],'create_date' => $todaydate,'create_time'=>$create_time,'MemberId'=>$result1['Member']['MemberId'],'AgencyId'=>$result1['Member']['AgencyId'],'LoginName'=>$result1['Member']['LoginName'],'localip'=>$res[3] ]);
             		$newtokenid=$result1['TokenId'];
	                $memberid=$result1['Member']['MemberId'];
	                $agencyid=$result1['Member']['AgencyId'];
	                $loginname=$result1['Member']['LoginName'];
	                $localip=$res[3];
	          }
	      }
	      	Cookie::queue('TokenId',$newtokenid, 1440);
            Cookie::queue('MemberId',$memberid, 1440);
            Cookie::queue('AgencyId',$agencyid, 1440);
            Cookie::queue('LoginName',$loginname, 1440);
            Cookie::queue('localip',trim($localip), 1440);
            
          }
       	  return view('admin.index');

    	
    }
    
    public function login_check(Request $request)
    {

    	$username=$request->get('username');
		$password=md5($request->get('password'));
		$remember=$request->get('remember');
		$exist= DB::table('admin')->where('admin_username', $username)->orwhere('admin_email', $username)->where('admin_password', $password)->first();

		if(count($exist) > 0)
		{
			$id=$exist->admin_id;
			$admin_fname=$exist->admin_fname;
			
			if($remember=="on")
			{
				$hour = time() + 3600 * 24 * 30;
	            setcookie('admin_ids', $id, $hour);
			}
			$request->session()->put('admin_id', $id);
			$request->session()->put('admin_fname', $admin_fname);
			 $TokenId=Cookie::get('TokenId');
			$EndUserIp=Cookie::get('localip');
			$TokenAgencyId=Cookie::get('AgencyId');
			$TokenMemberId=Cookie::get('MemberId');
			$form_balance = array(
				'ClientId'=>"ApiIntegrationNew",
				'TokenAgencyId'=>$TokenAgencyId,
				'TokenMemberId'=>$TokenMemberId,
				'EndUserIp'=>$EndUserIp,
				'TokenId'=>$TokenId,
			);

			$data_balance = json_encode($form_balance); 
			
			$ch = curl_init('http://api.tektravels.com/SharedServices/SharedData.svc/rest/GetAgencyBalance');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLINFO_HEADER_OUT, true);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_balance);
			
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: '.strlen($data_balance)) );
			$data_result_balance = curl_exec($ch);
			$data_result_balance1=json_decode($data_result_balance,true);
			 $agencycashbal = $data_result_balance1['CashBalance'];
			$request->session()->put('agencycashbal', $agencycashbal);
			echo "success";
		
		}
		else
		{
			echo "fail";
			
		}
    }

    public function dashboard()
    {
    	
    	if(session()->has('admin_id'))
		{
			$tot_data=array();
			array_push($tot_data,DB::table('tbl_user')->count());
			array_push($tot_data,DB::table('tbl_flight_book')->count());
			array_push($tot_data,DB::table('tbl_flight_book')->where('flight_cancel','!=',0)->count());
			array_push($tot_data,DB::table('tbl_hotel_book')->count());
			array_push($tot_data,DB::table('tbl_hotel_book')->where('hotel_cancel','!=',0)->count());
			array_push($tot_data,DB::table('tbl_mytrip')->where('cancelstatus','!=',0)->count());

			// array_push($tot_data,DB::table('releases')->where('release_code','!=',0)->count());
			return view('admin.dashboard',['tot_data'=>$tot_data]);
		}
		else
		{
			return redirect()->intended('/admin');
		}
    }
    //user

    public function user()
    {
    	
    	if(session()->has('admin_id'))
		{
			$userlist=userlist::get();
			
			 return view('admin.user', compact('userlist'));
			
		}
		else
		{

			return redirect()->intended('/admin');
		}
    }
    public function adminuserlist(Request $request)
	{
		if($request->get('search')!='')
		{
		$search=$request->get('search');
		$userlist = userlist::where( 'user_id', 'LIKE', '%' . $search . '%' )->orWhere( 'user_fname', 'LIKE', '%' . $search . '%' )->orWhere( 'user_lname', 'LIKE', '%' . $search . '%' )->orWhere( 'user_phone', 'LIKE', '%' . $search . '%' )->orWhere( 'user_email', 'LIKE', '%' . $search . '%' )->orWhere( 'user_name', 'LIKE', '%' . $search . '%' )->paginate (10);
		}
		else
		{
		
		$userlist = userlist::paginate(10);
		}
	       if ($request->ajax()) 
	       {
	           return view('admin.user', compact('userlist'));
	       }
	       return view('admin.user',compact('userlist'));
	}
	//flightbooking
	 public function flightbooking()
    {
    	
    	if(session()->has('admin_id'))
		{
			$flightbooklist=tbl_flight_book::orderBy('id', 'DESC')->get();
			
			 return view('admin.flightbooking', compact('flightbooklist'));
			
		}
		else
		{

			return redirect()->intended('/admin');
		}
    }
    public function flightbooklist(Request $request)
	{
		if($request->get('search')!='')
		{
		$search=$request->get('search');
		$flightbooklist = tbl_flight_book::where( 'user_id', 'LIKE', '%' . $search . '%' )->orWhere( 'pnrno', 'LIKE', '%' . $search . '%' )->orWhere( 'bookingId', 'LIKE', '%' . $search . '%' )->orWhere( 'orign', 'LIKE', '%' . $search . '%' )->orWhere( 'destination', 'LIKE', '%' . $search . '%' )->orWhere( 'pname', 'LIKE', '%' . $search . '%' )->orWhere( 'pmobile', 'LIKE', '%' . $search . '%' )->orWhere( 'pemail', 'LIKE', '%' . $search . '%' )->orWhere( 'pcity', 'LIKE', '%' . $search . '%' )->orWhere( 'flight_booking_date', 'LIKE', '%' . $search . '%' )->paginate (10);
		}
		else
		{
		
		$flightbooklist = tbl_flight_book::orderBy('id', 'DESC')->paginate(10);
		}
	       if ($request->ajax()) 
	       {
	           return view('admin.flightbooking', compact('flightbooklist'));
	       }
	       return view('admin.flightbooking',compact('flightbooklist'));
	}



	//hotelbooking
	 public function hotelbooking()
    {
    	
    	if(session()->has('admin_id'))
		{
			$hotelbookinglist=tbl_hotel_book::where('hotel_cancel','0')->orderBy('id', 'DESC')->get();
			
			 return view('admin.hotelbooking', compact('hotelbookinglist'));
			
		}
		else
		{

			return redirect()->intended('/admin');
		}
    }
    public function hotelbooklist(Request $request)
	{
		if($request->get('search')!='')
		{
		$search=$request->get('search');
		$hotelbookinglist = tbl_hotel_book::where( 'bookingid', 'LIKE', '%' . $search . '%' )->orWhere( 'hotel_name', 'LIKE', '%' . $search . '%' )->orWhere( 'hotel_city', 'LIKE', '%' . $search . '%' )->orWhere( 'lead_name', 'LIKE', '%' . $search . '%' )->orWhere( 'lead_mobile', 'LIKE', '%' . $search . '%' )->orWhere( 'lead_email', 'LIKE', '%' . $search . '%' )->orWhere( 'booking_date')->paginate (10);
		}
		else
		{
		
		$hotelbookinglist = tbl_hotel_book::where('hotel_cancel','0')->orderBy('id', 'DESC')->paginate(10);
		}
	       if ($request->ajax()) 
	       {
	           return view('admin.hotelbooking', compact('hotelbookinglist'));
	       }
	       return view('admin.hotelbooking',compact('hotelbookinglist'));
	}
	//end of hotelbooking
	//payment details
	 public function paymentdetail()
    {
    	
    	if(session()->has('admin_id'))
		{
			$paymentlist=tbl_payment_order::orderBy('id', 'DESC')->get();
			
			 return view('admin.paymentdetail', compact('paymentlist'));
			
		}
		else
		{

			return redirect()->intended('/admin');
		}
    }
    public function paymentdetaillist(Request $request)
	{
		if($request->get('search')!='')
		{
		$search=$request->get('search');
		$paymentlist = tbl_payment_order::where( 'user_id', 'LIKE', '%' . $search . '%' )->orWhere( 'pnrno', 'LIKE', '%' . $search . '%' )->orWhere( 'bookingId', 'LIKE', '%' . $search . '%' )->orWhere( 'orign', 'LIKE', '%' . $search . '%' )->orWhere( 'destination', 'LIKE', '%' . $search . '%' )->orWhere( 'pname', 'LIKE', '%' . $search . '%' )->orWhere( 'pmobile', 'LIKE', '%' . $search . '%' )->orWhere( 'pemail', 'LIKE', '%' . $search . '%' )->orWhere( 'pcity', 'LIKE', '%' . $search . '%' )->orWhere( 'flight_booking_date', 'LIKE', '%' . $search . '%' )->paginate (10);
		}
		else
		{
		
		$paymentlist = tbl_payment_order::orderBy('id', 'DESC')->paginate(10);
		}
	       if ($request->ajax()) 
	       {
	           return view('admin.paymentdetail', compact('paymentlist'));
	       }
	       return view('admin.paymentdetail',compact('paymentlist'));
	}
	//end paymentdetails
	//booking details
	public function bookingdetail($bookid)
	{
		if(session()->has('admin_id'))
		{
			$booklistdetails=tbl_flight_book::where('id', $bookid)->first();
			$orderrefid= $booklistdetails->order_ref;
			$orderdetails=tbl_payment_order::where('referenceid', $orderrefid)->first();
			 return view('admin.booking-details', compact('booklistdetails'))->with(compact('orderdetails'));
			
		}
		else
		{

			return redirect()->intended('/admin');
		}
	}
	//end booking details
	//Hotel details
	public function hotelbookingdetail($bookid)
	{

		if(session()->has('admin_id'))
		{
			$booklistdetails=tbl_hotel_book::where('id', $bookid)->first();
			 $orderrefid= $booklistdetails->booking_ref_no;

			$orderdetails=tbl_payment_order::where('ticket_id', $bookid)->where('payment_code', 'Hotel')->first();
			
		
			 return view('admin.hotel-booking-details', compact('booklistdetails'))->with(compact('orderdetails'));
			
		}
		else
		{

			return redirect()->intended('/admin');
		}
	}
	//end hotel details
	//canceldetail
	//hotelbooking
	 	public function canceldetail()
	    {
	    	
	    	if(session()->has('admin_id'))
			{
				$cancellist=tbl_mytrip::where('cancelstatus','1')->where('triptype','Flight')->orderBy('id', 'DESC')->get();
				
				 return view('admin.canceldetail', compact('cancellist'));
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
	    }
	     public function canceldetaillist(Request $request)
		{
			$chkcancel= tbl_mytrip::where('cancelstatus','1')->where('triptype','Hotel')->orderBy('id', 'DESC')->get();
			foreach ($chkcancel as $chkval ) 
			{
				$ChangeRequestId= $chkval->change_request_id;
				$TokenId=Cookie::get('TokenId');
				$EndUserIp=Cookie::get('localip');
				$form_cancel_again = array(
				'EndUserIp'=>$EndUserIp,
				'TokenId'=>$TokenId,
				'ChangeRequestId'=>$ChangeRequestId,
					);
				$data_flightl_cancel_again = json_encode($form_cancel_again);  
				
				$ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/GetChangeRequestStatus/');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLINFO_HEADER_OUT, true);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_flightl_cancel_again);
				
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: '.strlen($data_flightl_cancel_again)) );
				$result_again = curl_exec($ch);
				$result_cancel1=json_decode($result_again,true);
				

			}
			if($request->get('search')!='')
			{
			$search=$request->get('search');
			$cancellist = tbl_mytrip::where( 'triptype', 'LIKE', '%' . $search . '%' )->orWhere( 'bookingid', 'LIKE', '%' . $search . '%' )->orWhere( 'change_request_id', 'LIKE', '%' . $search . '%' )->orWhere( 'booking_date', 'LIKE', '%' . $search . '%' )->orWhere( 'cancel_data', 'LIKE', '%' . $search . '%' )->orWhere( 'trip_date', 'LIKE', '%' . $search . '%' )->paginate (10);
			}
			else
			{
			
			$cancellist = tbl_mytrip::where('cancelstatus','1')->where('triptype','Flight')->orderBy('id', 'DESC')->paginate(10);
			}

		       if ($request->ajax()) 
		       {
		           return view('admin.canceldetail', compact('cancellist'));
		       }
		       return view('admin.canceldetail',compact('cancellist'));
		}
	//endcanceldetail
		//hotelcanceldetail
		public function adminhotelcancel()
	    {
	    	
	    	if(session()->has('admin_id'))
			{
				$cancellist=tbl_mytrip::where('cancelstatus','1')->where('triptype','Hotel')->orderBy('id', 'DESC')->get();
				
				 return view('admin.adminhotelcancel', compact('cancellist'));
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
	    }
	     public function adminhotelcancellist(Request $request)
		{
			
			$chkcancel= tbl_mytrip::where('cancelstatus','1')->where('triptype','Hotel')->orderBy('id', 'DESC')->get();
			foreach ($chkcancel as $chkval ) 
			{
				$ChangeRequestId= $chkval->change_request_id;

				$TokenId=Cookie::get('TokenId');
				$EndUserIp=Cookie::get('localip');
				$form_cancel_request = array(
					'EndUserIp'=>$EndUserIp,
					'TokenId'=>$TokenId,
					'ChangeRequestId'=>$ChangeRequestId,
					

				);

				$data_hotel_cancel_request = json_encode($form_cancel_request);  
				
				$ch = curl_init('http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetChangeRequestStatus/');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLINFO_HEADER_OUT, true);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_hotel_cancel_request);
				
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: '.strlen($data_hotel_cancel_request)) );
				$result_hotel_cancel_request = curl_exec($ch);
				$result_cancel_request=json_decode($result_hotel_cancel_request,true);
				$checkstatus =  $result_cancel_request['HotelChangeRequestStatusResult']['ChangeRequestStatus'];
				$cancel_wholedata=serialize($result_cancel_request['HotelChangeRequestStatusResult']);
				$cancelstatus = tbl_mytrip::where('change_request_id', $ChangeRequestId)->where('cancelstatus','1')->where('triptype','Hotel')->update(['changerequest_status' => $checkstatus,'cancel_allrequestdata'=>$cancel_wholedata]);

			}
			if($request->get('search')!='')
			{
			$search=$request->get('search');
			$cancellist = tbl_mytrip::where( 'triptype', 'LIKE', '%' . $search . '%' )->orWhere( 'bookingid', 'LIKE', '%' . $search . '%' )->orWhere( 'change_request_id', 'LIKE', '%' . $search . '%' )->orWhere( 'booking_date', 'LIKE', '%' . $search . '%' )->orWhere( 'cancel_data', 'LIKE', '%' . $search . '%' )->orWhere( 'trip_date', 'LIKE', '%' . $search . '%' )->paginate (10);
			}
			else
			{
			
			$cancellist = tbl_mytrip::where('cancelstatus','1')->where('triptype','Hotel')->orderBy('id', 'DESC')->paginate(10);
			}

		       if ($request->ajax()) 
		       {
		           return view('admin.adminhotelcancel', compact('cancellist'));
		       }
		       return view('admin.adminhotelcancel',compact('cancellist'));
		}
		//endhotelcanceldetail
		//hotelcancelview
		public function hotelbookingcanceldetail($bookid)
		{

			if(session()->has('admin_id'))
			{
				$booklistdetails=tbl_hotel_book::where('bookingid', $bookid)->first();
				 $booktableid= $booklistdetails->id;
				 $hotelcancdetailway = tbl_mytrip::where('cancelstatus','1')->where('triptype','Hotel')->where('bookingid', $bookid)->orderBy('id', 'DESC')->first();
				$orderdetails=tbl_payment_order::where('ticket_id', $booktableid)->where('payment_code', 'Hotel')->first();
				
			
				 return view('admin.hotel-booking-cancel-details', compact('booklistdetails'))->with(compact('orderdetails'))->with(compact('hotelcancdetailway'));
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
		}
		//endhotelcancelview
		//hotelrefund
		public function hotelrefund(Request $request)
		{
			$bookingid = $request->get('bookingid');
			$changerequestid = $request->get('changereq');
		    $refundamount = $request->get('refundamount');
		    $paxname= $request->get('paxname');
		    $leadphone=$request->get('leadphone');
		    $leademail=$request->get('leademail');
		    date_default_timezone_set("Asia/Kolkata");
			$dateime=date('Y-m-d H:i:s');
			$create_date=date('Y-m-d');
			$create_time=date('H:i:s');
		    $cancelstatus = tbl_mytrip::where('change_request_id', $changerequestid)->where('cancelstatus','1')->where('bookingid',$bookingid)->update(
		    	['c_adminstatus' => 1,
		    	'c_refund_amount'=>$refundamount,
		    	'c_create_date'=>$create_date,
		    	'c_create_time'=>$create_time,
		    	]);
		    if($cancelstatus)
		    {
		    	$setting=tbl_general_setting::orderBy('id', 'DESC')->first();
				$to=$leademail;
				$subject="Refund Amount";
	            $message="Dear ".$paxname." <br>Your refund ".$refundamount." has been generated Please check  portal ";
	            $header="From:ticket@travoweb.com \r \n";
	            $header = "From: " . strip_tags('ticket@travoweb.com') . "\r\n";
	            $header .= "Reply-To: ". strip_tags('ticket@travoweb.com') . "\r\n";
	            $header .= "CC: 'ticket@travoweb.com'\r\n";
	            $header .= "MIME-Version: 1.0\r\n";
	            $header .= "Content-Type: text/html; charset=UTF-8\r\n";
	            $mailtest =mail($to,$subject,$message,$header);
	            $hl = explode("-", $setting->hotel_sms_active); 
	            if($hl[3]=='1')
	            {


		            $leadphone1=$leadphone;

		            $user=$setting->sms_user_id;
		            $password=$setting->sms_password;
		            $sender=$setting->sms_sender;
		            $sms_url=$setting->sms_url;
		            $text="Dear ".$paxname.", Your refund ".$refundamount." has been generated Please check  portal";
					$otp=urlencode("$text");
					$data = array(
					        'usr' => $user,
					        'pwd' => $password,
					        'ph'  => $leadphone1,
					        'sndr'  => $sender,
					        'text'  => $otp
					);
					$ch = curl_init($sms_url);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
					curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
					$result = curl_exec($ch);
				}
		    	echo "success";

		    }
		    else
		    {
		    	echo "fail";
		    }


		}
		//endhotelrefund
		//flightcancel
		//hotelcancelview
		public function flightbookingcanceldetail($bookid)
		{

			if(session()->has('admin_id'))
			{
				$booklistdetails=tbl_flight_book::where('bookingId', $bookid)->first();
				 $booktableid= $booklistdetails->id;
				 $flightcancdetailway = tbl_mytrip::where('cancelstatus','1')->where('triptype','Flight')->where('bookingid', $bookid)->orderBy('id', 'DESC')->first();
				$orderdetails=tbl_payment_order::where('ticket_id', $booktableid)->where('payment_code', 'Flight')->first();
				
			
				 return view('admin.flight-booking-cancel-details', compact('booklistdetails'))->with(compact('orderdetails'))->with(compact('flightcancdetailway'));
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
		}
		public function generalsetting(Request $request)
		{

			if(session()->has('admin_id'))
			{
				$setting=tbl_general_setting::orderBy('id', 'DESC')->first();
				
				
			
				 return view('admin.general-setting', compact('setting'));
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
		}
		//endhotelcancelview
		public function smsrequest(Request $request)
		{
			if(session()->has('admin_id'))
			{
				$sms_userid = $request->get('sms_userid');
				$sms_password = $request->get('sms_password');
				$sms_sender = $request->get('sms_sender');
				$sms_url = $request->get('sms_url');
				$smslist = $request->get('smslist');
				$hotellist=$request->get('hotellist');
				$smsstatus = tbl_general_setting::where('id',1)->update(
					['sms_user_id' => $sms_userid,
					'sms_password'=>$sms_password,
					'sms_sender' => $sms_sender,
					'sms_url'=>$sms_url,
					'sms_active' => $smslist,
					'hotel_sms_active'=>$hotellist,
					
				]);
				if($smsstatus)
				{
					echo "success";
				}
				else
				{
					echo "fail";
				}

			}
			else
			{

				return redirect()->intended('/admin');
			}

		}
		public function hotelmargin(Request $request)
		{
			if(session()->has('admin_id'))
			{
				$hotelmargin = $request->get('hotelmargin');
				
				$hotelstatus = tbl_general_setting::where('id',1)->update(
					['hotel_margin' => $hotelmargin,
					
					
				]);
				if($hotelstatus)
				{
					echo "success";
				}
				else
				{
					echo "fail";
				}

			}
			else
			{

				return redirect()->intended('/admin');
			}

		}
		//flightmargin
		public function flightmargin(Request $request)
		{
			if(session()->has('admin_id'))
			{
				$mealmargin = $request->get('mealmargin');
				$bagmargin = $request->get('bagmargin');
				$flightmargin = $request->get('flightmargin');
				
				$flightstatus = tbl_general_setting::where('id',1)->update(
					['flight_margin' => $flightmargin,
					'flight_meal_margin' => $mealmargin,
					'flight_bag_margin' => $bagmargin,
					
					
				]);
				if($flightstatus)
				{
					echo "success";
				}
				else
				{
					echo "fail";
				}

			}
			else
			{

				return redirect()->intended('/admin');
			}

		}
		//endflightcancel
		// advertisment
		public function advertisment()
	    {
	    	
	    	if(session()->has('admin_id'))
			{
				
				$data=0;
				$checknewdata=tour_package::where('pckg_status','1')->orderBy('id', 'DESC')->get();
		    	$mainval='<option value="0">Select package</option>';
		    	foreach ($checknewdata as $vall) 
		    	{
		    		$mainval .="<option value=".$vall->id.'%'.$vall->pckg_id. ">".$vall->packagename."</option>";
		    		
		    	}
				 return view('admin.advertisement-banner')->with('data',$data)->with('mainval',$mainval) ;
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
	    }
	    public function advertisment_image_upload(Request $request)
	    {
	        $extension = $request->file('file')->getClientOriginalExtension();
	        $extensions=strtolower($extension);
	        if($extensions=="jpeg"||$extensions=="jpg"||$extensions=="png"||$extensions=="bmp"||$extensions=="pdf")
	        {
	        	$dir = 'assets/uploads/';
		        $filename = uniqid() . '_' . time() . '.' . $extension;
		        $request->file('file')->move($dir, $filename);
		        echo $filename;
	        }
	        else
	        {
	        	echo "no";
	        }
	    }
	    public function insert_banner(Request $request)
	    {
	    	$advimage=$request->get('adv_atchmnt_image');
	    	$advurl=$request->get('banner_link');
	    	date_default_timezone_set("Asia/Kolkata");
			$dateime=date('Y-m-d H:i:s');
			$create_date=date('Y-m-d');
			$create_time=date('H:i:s');
	    	$insertarray=array('adv_image'=>$advimage,
	    						'adv_url'=>$advurl,
	    						'adv_date'=>$create_date,
	    						'adv_time'=>$create_time,
	    						);
	    	$insertquery=advbanner::insert($insertarray);
	    	if($insertquery)
	    	{
	    		echo "success";
	    	}
	    	else
	    	{
	    		echo "fail";
	    	}

	    }
		//end advertisment
		 public function advertisement_banner_list()
	    {
	    	
	    	if(session()->has('admin_id'))
			{
				$advlist=advbanner::get();
				
				 return view('admin.advertisement-banner-list', compact('advlist'));
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
	    }
	    public function advlist(Request $request)
		{
			if($request->get('search')!='')
			{
			$search=$request->get('search');
			$advlist = advbanner::where( 'id', 'LIKE', '%' . $search . '%' )->orWhere( 'adv_url', 'LIKE', '%' . $search . '%' )->orWhere( 'adv_date', 'LIKE', '%' . $search . '%' )->orWhere( 'adv_time', 'LIKE', '%' . $search . '%' )->paginate (10);
			}
			else
			{
			
			$advlist = advbanner::paginate(10);
			}
		       if ($request->ajax()) 
		       {
		           return view('admin.advertisement-banner-list', compact('advlist'));
		       }
		       return view('admin.advertisement-banner-list',compact('advlist'));
		}
		public function advertismentedit($advid)
		{

			if(session()->has('admin_id'))
			{
				$advdetail=advbanner::where('id', $advid)->first();
				 
				$data=1;
				$checknewdata=tour_package::where('pckg_status','1')->orderBy('id', 'DESC')->get();
		    	$mainval1='<option value="0">Select Country/State</option>';
		    	foreach ($checknewdata as $vall) 
		    	{
		    		if($vall->id==$advdetail->adv_url)
		    		{
		    			$sel="selected";
		    		}
		    		else
		    		{
		    			$sel="";
		    		}
		    		$mainval1 .="<option value=".$vall->id.'%'.$vall->pckg_id. " ".$sel.">".$vall->packagename."</option>";
		    		
		    		
		    	}
			
				 return view('admin.advertisement-banner', compact('advdetail'))->with('data',$data)->with('mainval1',$mainval1);
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
		}
		public function adv_edit_banner(Request $request)
		{
			$advid=$request->get('adv_id');
			$advimage=$request->get('adv_atchmnt_image');
			$advurl=$request->get('banner_link');
			date_default_timezone_set("Asia/Kolkata");
			$dateime=date('Y-m-d H:i:s');
			$create_date=date('Y-m-d');
			$create_time=date('H:i:s');
			$updatead=array('adv_image'=>$advimage,
	    						'adv_url'=>$advurl,
	    						'adv_date'=>$create_date,
	    						'adv_time'=>$create_time,
	    						);
			
			$updatequery=advbanner::where('id',$advid)->update($updatead);
			if($updatequery)
			{
				echo "success";
			}
			else
			{
				echo "fail";
			}

		}
		//package country
		public function packagecountry()
	    {
	    	if(session()->has('admin_id'))
			{
				$data=0;
				 return view('admin.package-country')->with('data',$data);
			}
			else
			{
				return redirect()->intended('/admin');
			}
	    }
	    public function insert_package_country(Request $request)
	    {
	    	$countryname=$request->get('country_name');
	    	$country_type=$request->get('country_type');
	    	date_default_timezone_set("Asia/Kolkata");
			$dateime=date('Y-m-d H:i:s');
			$create_date=date('Y-m-d');
			$create_time=date('H:i:s');
	    	$countryinsert=array('statename'=>$countryname,
	    						'packge_type'=>$country_type,
	    						'package_country_status'=>1,
	    						'create_date'=>$create_date,
	    						'create_time'=>$create_time,
	    						);
	    	$countryinsertquery=countrypackage::insert($countryinsert);
	    	if($countryinsertquery)
	    	{
	    		echo "success";
	    	}
	    	else
	    	{
	    		echo "fail";
	    	}
	    }
	     public function packagecountrylist()
	    {
	    	
	    	if(session()->has('admin_id'))
			{
				$pckgcounttylist=countrypackage::orderBy('order_id','ASC')->get();
				
				 return view('admin.package-country-list', compact('pckgcounttylist'));
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
	    }
	    public static function chkcityname($cityname)

	    {

	        $cityname=countrypackage::where('id', $cityname)->first();

	        $cityname=$cityname->statename;
	        return $cityname;

	    }
	    public function pckgcounttylist(Request $request)
		{
			if($request->get('search')!='')
			{
			$search=$request->get('search');
			$pckgcounttylist = countrypackage::where( 'id', 'LIKE', '%' . $search . '%' )->orWhere( 'statename', 'LIKE', '%' . $search . '%' )->orWhere( 'packge_type', 'LIKE', '%' . $search . '%' )->orWhere( 'create_date', 'LIKE', '%' . $search . '%' )->paginate (10);
			}
			else
			{
			
			$pckgcounttylist= countrypackage::paginate(10);
			}
		       if ($request->ajax()) 
		       {
		           return view('admin.package-country-list', compact('pckgcounttylist'));
		       }
		       return view('admin.package-country-list',compact('pckgcounttylist'));
		}
		public function pckgcountrylistedit($pckgcounty)
		{
			if(session()->has('admin_id'))
			{
				$pckgcountrydetail=countrypackage::where('id', $pckgcounty)->first();
				 
				$data=1;
			
				 return view('admin.package-country', compact('pckgcountrydetail'))->with('data',$data);
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
		}
		public function update_package_country(Request $request)
		{
			$country_name=$request->get('country_name');
			$country_type=$request->get('country_type');
			$pckgid=$request->get('pckgid');
			date_default_timezone_set("Asia/Kolkata");
			$dateime=date('Y-m-d H:i:s');
			$create_date=date('Y-m-d');
			$create_time=date('H:i:s');
	    	$updatepack=array('statename'=>$country_name,
	    						'packge_type'=>$country_type,
	    						'package_country_status'=>1,
	    						'create_date'=>$create_date,
	    						'create_time'=>$create_time,
	    						);
	    	$updatequery=countrypackage::where('id',$pckgid)->update($updatepack);
	    	if($updatequery)
	    	{
	    		echo "success";
	    	}
	    	else
	    	{
	    		echo "fail";
	    	}

		}
		//package country
		public function packageadd()
	    {
	    	if(session()->has('admin_id'))
			{
				$data=0;
				$checknewdata=countrypackage::where('package_country_status','1')->orderBy('id', 'DESC')->get();
		    	$mainval='<option value="0">Select Country/State</option>';
		    	foreach ($checknewdata as $vall) 
		    	{
		    		$mainval .="<option value=".$vall->id.">".$vall->statename."</option>";
		    		
		    	}
				 return view('admin.package-add')->with('data',$data)->with('mainval',$mainval);
			}
			else
			{
				return redirect()->intended('/admin');
			}
	    }
	    public function checkpackagetype(Request $request)
	    {
	    	$datanew = $request->get('datanew');
	    	$checknewdata=countrypackage::where('package_country_status','1')->where('packge_type',$datanew)->orderBy('id', 'DESC')->get();
	    	$mainval='<option value="0">Select Country/State</option>';
	    	foreach ($checknewdata as $vall) 
	    	{
	    		$mainval .="<option value=".$vall->id.">".$vall->statename."</option>";
	    		
	    	}
	    	echo $mainval;
	    }

	    //package add
	    public function package_image_upload(Request $request)
	    {
	    	$file = $_FILES["filename"];
	    	$newimage=$request->file('filename');
	    	$chklen =  sizeof($newimage);
	    	$mainimg = $chklen - 1;
	    	$newimage=$request->file('filename')[$mainimg];

	         $extension = $newimage->getClientOriginalExtension();
	       $extensions=strtolower($extension);
	        if($extensions=="jpeg"||$extensions=="jpg"||$extensions=="png"||$extensions=="bmp"||$extensions=="pdf")
	        {
	        	$dir = 'assets/uploads/packageimage/';
		        $filename = uniqid() . '_' . time() . '.' . $extensions;

		        $newimage->move($dir, $filename);
		        echo $filename;
	        }
	        else
	        {
	        	echo "no";
	        }
	    }
	    //sightseen Upload
	     public function sightseen_image_upload(Request $request)
	    {
	    
	    	$newimage=$request->file('imgsight');
	    	$chklen =  sizeof($newimage);
	    	$mainimg = $chklen - 1;
	    	$newimage=$request->file('imgsight')[$mainimg];

	         $extension = $newimage->getClientOriginalExtension();
	       $extensions=strtolower($extension);
	        if($extensions=="jpeg"||$extensions=="jpg"||$extensions=="png"||$extensions=="bmp"||$extensions=="pdf")
	        {
	        	$dir = 'assets/uploads/packageimage/';
		        $filename = uniqid() . '_' . time() . '.' . $extensions;

		        $newimage->move($dir, $filename);
		        echo $filename;
	        }
	        else
	        {
	        	echo "no";
	        }
	    }
	    public function packgae_pdf_upload(Request $request)
	    {
	    
	    	
	    	$newimage=$request->file('pdffileupload');

	         $extension = $newimage->getClientOriginalExtension();
	       $extensions=strtolower($extension);
	        if($extensions=="jpeg"||$extensions=="jpg"||$extensions=="png"||$extensions=="bmp"||$extensions=="pdf")
	        {
	        	$dir = 'assets/uploads/packageimage/';
		        $filename = uniqid() . '_' . time() . '.' . $extensions;

		        $newimage->move($dir, $filename);
		        echo $filename;
	        }
	        else
	        {
	        	echo "no";
	        }
	    }
	    
	    //end package add
	    // add package
	    public function package_add(Request $request)
	  
	    {
	    	$countryvalue=$request->get('countryvalue');
	    	$countryname=$request->get('countryname');
			$packagename=$request->get('packagename');
			$packagedays=$request->get('packagedays');
			$packagenight=$request->get('packagenight');
	    	$oldprice=$request->get('oldprice');
			$newprice=$request->get('newprice');
	    	$packgstar=$request->get('packgstar');
	    	$packgvalid=$request->get('packgvalid');
			$packimage=serialize($request->get('packimage'));
	    	$packgtitle=$request->get('packgtitle');
			$maindescription=$request->get('maindescription');
	    	$inttitle=serialize($request->get('inttitle'));
			$intdescription=serialize($request->get('intdescription'));
			$meallist=serialize($request->get('meallist'));
	    	$Inclusionsdescription=$request->get('Inclusionsdescription');
			$hotelname=$request->get('hotelname');
	    	$hotelstar=$request->get('hotelstar');
			$twinshare=$request->get('twinshare');
	    	$childwithbed=$request->get('childwithbed');
			$childwithoutbed=$request->get('childwithoutbed');
	    	$highlitedescription=$request->get('highlitedescription');
			$sighttitle=serialize($request->get('sighttitle'));
			$sightimage=serialize($request->get('sightimage'));
	    	$sightdescription=serialize($request->get('sightdescription'));
			$maploc=$request->get('maploc');
	    	$termcondition=$request->get('termcondition');
			$packagepdf=serialize($request->get('packagepdf'));
			$cancelpolicy=$request->get('cancelpolicy');
			// $token=$request->get('token');
	    	// $pckg_order=tour_package::count();
	    	 $pckg_order = DB::table('tbl_tour_package')->count();
	    	if($pckg_order == 0)
	    	{
	    		$pckg_order_new=1;
	    		$pckg_id="PCKG-0".$pckg_order_new;
	    	}
	    	else
	    	{
	    		
	    		$pckg_order_new=$pckg_order+1;
	    		$pckg_id="PCKG-0".$pckg_order_new;
	    		
	    	}
	    
	    	date_default_timezone_set("Asia/Kolkata");
			$dateime=date('Y-m-d H:i:s');
			$create_date=date('Y-m-d');
			$create_time=date('H:i:s');
			
			 $date1 = str_replace('/', '-', $packgvalid );

            $package_valid_new = date("Y-m-d", strtotime($date1));
	    	$pckginsert=array('pckg_id'=>$pckg_id,
	    						'countryvalue'=>$countryvalue,
	    						'countryname'=>$countryname,
	    						'packagename'=>$packagename,
	    						'oldprice'=>$oldprice,
	    						'newprice'=>$newprice,
	    						'packagedays'=>$packagedays,
	    						'packagenight'=>$packagenight,
								'packgstar'=>$packgstar,
								'packgvalid'=>$packgvalid,
	    						'packimage'=>$packimage,
	    						'packgtitle'=>$packgtitle,
	    						'maindescription'=>$maindescription,
	    						'inttitle'=>$inttitle,
	    						'intdescription'=>$intdescription,
								'meallist'=>$meallist,
	    						'Inclusionsdescription'=>$Inclusionsdescription,
	    						'hotelname'=>$hotelname,
	    						'hotelstar'=>$hotelstar,
	    						'twinshare'=>$twinshare,
	    						'childwithbed'=>$childwithbed,
								'childwithoutbed'=>$childwithoutbed,
	    						'highlitedescription'=>$highlitedescription,
	    						'sighttitle'=>$sighttitle,
	    						'sightimage'=>$sightimage,
	    						'sightdescription'=>$sightdescription,
	    						'maploc'=>$maploc,
								'termcondition'=>$termcondition,
	    						'packagepdf'=>$packagepdf,
	    						'pckg_order'=>$pckg_order_new,
	    						
	    						'cancelpolicy'=>$cancelpolicy,
	    						'create_date'=>$create_date,
	    						'create_time'=>$create_time,
	    						'package_valid_new'=>$package_valid_new,
	    						);
	    	$pckginsertquery=tour_package::insert($pckginsert);
	    	if($pckginsertquery)
	    	{
	    		echo "success";
	    	}
	    	else
	    	{
	    		echo "fail";
	    	}
	    }
	    //endpackage
	     public function package_list()
	    {
	    	
	    	if(session()->has('admin_id'))
			{
				$pckglist=tour_package::orderBy('pckg_order','ASC')->get();
				if(count($pckglist) >0)
				{
					$data='yes';
					foreach ($pckglist as $show)
					{
						$countryname = $show->countryname;
						$pckgcountrydetail=countrypackage::where('id', $countryname)->first();
						$countryn = $pckgcountrydetail->statename;

					}
				}
				else
				{
					$data='no';
				}
				
				 return view('admin.package_list', compact('pckglist'))->with('countryn',$countryn)->with('data',$data);
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
	    }
	    public function sortpackagelist(Request $request)
	    {
	    	$tasks = tour_package::all();
	       
	        foreach ($tasks as $task) {
	            $task->timestamps = false; // To disable update_at field updation
	            $id = $task->id;

	            foreach ($request->order as $order) {
	               
	                if ($order['id'] == $id) {
	                    $task->update(['pckg_order' => $order['position']]);
	                }
	            }
	        }
	        
	        return response('Update Successfully.', 200);
	    }
	    public function package_edit($pckgid)
		{
			if(session()->has('admin_id'))
			{
				$pckgdetails=tour_package::where('id', $pckgid)->first();
				 
				$data=1;
				$datanew=$pckgdetails->countryvalue;
				$checknewdata=countrypackage::where('package_country_status','1')->orderBy('id', 'DESC')->get();
		    	$mainval='<option value="0">Select Country/State</option>';
		    	foreach ($checknewdata as $vall) 
		    	{
		    		if($vall->id==$pckgdetails->countryname)
		    		{
		    			$sel="selected";
		    		}
		    		else
		    		{
		    			$sel="";
		    		}
		    		$mainval .="<option value=".$vall->id." ".$sel.">".$vall->statename."</option>";
		    		
		    	}
		    	
			
				 return view('admin.package_edit', compact('pckgdetails'))->with('data',$data)->with('mainval',$mainval);
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
		}
		public function package_update(Request $request)
	  
	    {
	    	$countryvalue=$request->get('countryvalue');
	    	$countryname=$request->get('countryname');
			$packagename=$request->get('packagename');
			$packagedays=$request->get('packagedays');
			$packagenight=$request->get('packagenight');
	    	$oldprice=$request->get('oldprice');
			$newprice=$request->get('newprice');
	    	$packgstar=$request->get('packgstar');
	    	$packgvalid=$request->get('packgvalid');
			$packimage=serialize($request->get('packimage'));
	    	$packgtitle=$request->get('packgtitle');
			$maindescription=$request->get('maindescription');
	    	$inttitle=serialize($request->get('inttitle'));
			$intdescription=serialize($request->get('intdescription'));
			$meallist=serialize($request->get('meallist'));
	    	$Inclusionsdescription=$request->get('Inclusionsdescription');
			$hotelname=$request->get('hotelname');
	    	$hotelstar=$request->get('hotelstar');
			$twinshare=$request->get('twinshare');
	    	$childwithbed=$request->get('childwithbed');
			$childwithoutbed=$request->get('childwithoutbed');
	    	$highlitedescription=$request->get('highlitedescription');
			$sighttitle=serialize($request->get('sighttitle'));
			$sightimage=serialize($request->get('sightimage'));
	    	$sightdescription=serialize($request->get('sightdescription'));
			$maploc=$request->get('maploc');
	    	$termcondition=$request->get('termcondition');
			$packagepdf=serialize($request->get('packagepdf'));
			$cancelpolicy=$request->get('cancelpolicy');
			$pckg_id=$request->get('pckg_id');
			$pckg_order_new=$request->get('pckg_order_new');
			$tbl_id=$request->get('tbl_id');
			// $token=$request->get('token');
	    	// $pckg_order=tour_package::count();
	    	
	    
	    	date_default_timezone_set("Asia/Kolkata");
			$dateime=date('Y-m-d H:i:s');
			$create_date=date('Y-m-d');
			$create_time=date('H:i:s');
			 $date1 = str_replace('/', '-', $packgvalid );

            $package_valid_new = date("Y-m-d", strtotime($date1));
	    	$pckgupdate=array('pckg_id'=>$pckg_id,
	    						'countryvalue'=>$countryvalue,
	    						'countryname'=>$countryname,
	    						'packagename'=>$packagename,
	    						'oldprice'=>$oldprice,
	    						'newprice'=>$newprice,
								'packgstar'=>$packgstar,
								'packgvalid'=>$packgvalid,
								'packagedays'=>$packagedays,
	    						'packagenight'=>$packagenight,
	    						'packimage'=>$packimage,
	    						'packgtitle'=>$packgtitle,
	    						'maindescription'=>$maindescription,
	    						'inttitle'=>$inttitle,
	    						'intdescription'=>$intdescription,
								'meallist'=>$meallist,
	    						'Inclusionsdescription'=>$Inclusionsdescription,
	    						'hotelname'=>$hotelname,
	    						'hotelstar'=>$hotelstar,
	    						'twinshare'=>$twinshare,
	    						'childwithbed'=>$childwithbed,
								'childwithoutbed'=>$childwithoutbed,
	    						'highlitedescription'=>$highlitedescription,
	    						'sighttitle'=>$sighttitle,
	    						'sightimage'=>$sightimage,
	    						'sightdescription'=>$sightdescription,
	    						'maploc'=>$maploc,
								'termcondition'=>$termcondition,
	    						'packagepdf'=>$packagepdf,
	    						'pckg_order'=>$pckg_order_new,
	    						
	    						'cancelpolicy'=>$cancelpolicy,
	    						'create_date'=>$create_date,
	    						'create_time'=>$create_time,
	    						'package_valid_new'=>$package_valid_new,
	    						);
	    	$pckgupdatequery=tour_package::where('id',$tbl_id)->update($pckgupdate);
	    	if($pckgupdatequery)
	    	{
	    		echo "success";
	    	}
	    	else
	    	{
	    		echo "fail";
	    	}
	    }

	     public function package_enquiry_list()
	    {
	    	
	    	if(session()->has('admin_id'))
			{
				$pckglist=tbl_package_enquiry::where('enq_status','1')->orderBy('id', 'DESC')->get();
				
				 return view('admin.package_enquiry_list', compact('pckglist'));
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
	    }
	    public function package_enquiry_list1(Request $request)
		{
			if($request->get('search')!='')
			{
			$search=$request->get('search');
			$pckglist = tbl_package_enquiry::where( 'id', 'LIKE', '%' . $search . '%' )->orWhere( 'enq_id', 'LIKE', '%' . $search . '%' )->orWhere( 'firstname', 'LIKE', '%' . $search . '%' )->orWhere( 'lastname', 'LIKE', '%' . $search . '%' )->orWhere( 'email', 'LIKE', '%' . $search . '%' )->orWhere( 'mobile', 'LIKE', '%' . $search . '%' )->orWhere( 'pckg_id', 'LIKE', '%' . $search . '%' )->orWhere( 'pckg_name', 'LIKE', '%' . $search . '%' )->orWhere( 'pckgdate', 'LIKE', '%' . $search . '%' )->orWhere( 'create_date', 'LIKE', '%' . $search . '%' )->paginate (10);
			}
			else
			{
			$pckglist=tbl_package_enquiry::where('enq_status','1')->orderBy('id', 'DESC')->paginate(10);
			
			}
		       if ($request->ajax()) 
		       {
		           return view('admin.package_enquiry_list', compact('pckglist'));
		       }
		       return view('admin.package_enquiry_list',compact('pckglist'));
		}

		public function pckgenqdetails($pckid)
		{
			if(session()->has('admin_id'))
			{
				$booklistdetails=tbl_package_enquiry::where('id', $pckid)->first();

				$bookbal=tbl_offer_package::where('tbl_id', $pckid)->orderBy('id', 'ASC')->get();
				$bookbalnew=tbl_offer_package::where('tbl_id', $pckid)->orderBy('id', 'DESC')->first();
				$paymenttable=tbl_payment_order::where('ticket_id', $pckid)->where('payment_code','Packages')->orderBy('id', 'DESC')->get();
				//  $orderrefid= $booklistdetails->booking_ref_no;

				// $orderdetails=tbl_payment_order::where('ticket_id', $bookid)->where('payment_code', 'Hotel')->first();
				
			
				 return view('admin.package-enq-details')->with(compact('booklistdetails'))->with(compact('bookbal'))->with(compact('bookbalnew'))->with(compact('paymenttable'));
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
		}
		//offerinsert
		public function offerprice_insert(Request $request)
		{
			$offer_price=$request->get('offer_price');
			$offer_msg=$request->get('offer_msg');
			$pckgid=$request->get('pckgid');
			$enqid=$request->get('enqid');
			$tbl_id=$request->get('tblid');
			$currency=$request->get('currency');
			$dateime=date('Y-m-d H:i:s');
			$create_date=date('Y-m-d');
			$create_time=date('H:i:s');
			$instda=$request->get('instda');
			if($request->get('instda')=="")
			{
				$intallmendate="";
			}
			else
			{
				$instda=explode('/',$request->get('instda'));
            	$intallmendate = $instda[2]."-".$instda[1]."-".$instda[0];
			}
			
			$offermainprice=$request->get('offermainprice');
			$offer_bal_price=$request->get('offer_bal_price');
			$enquiryinsert=array( 'pckg_id'=>$pckgid,
									'enq_id'=>$enqid,
									'tbl_id'=>$tbl_id,
									'currency'=>$currency,
									'offer_price'=>$offer_price,
									'offer_msg'=>$offer_msg,
									'create_date'=>$create_date,
									'create_time'=>$create_time,
									'offer_main_price'=>$offermainprice,
									'offer_balance'=>$offer_bal_price,
									'intallmendate'=>$intallmendate,
								);
			$insertenquiry=tbl_offer_package::insert($enquiryinsert);
			if($insertenquiry)
			{
				
				$newpckid=base64_encode($pckgid);
				$newenqid=base64_encode($enqid);
				$newprice=base64_encode($offer_price);
				$enqupdate=tbl_package_enquiry::where('id',$tbl_id)->update(['admit_status' => '2']);
				$enqdetails=tbl_package_enquiry::where('id', $tbl_id)->first();
				$customername=$enqdetails->firstname.' '.$enqdetails->lastname;
				if($currency=='INR')
				{
					$pagedirect='request_package';
				}
				else
				{
					$pagedirect='packages_pay';
				}
				//first test

				// $enqdetails=tbl_package_enquiry::where('id', $tbl_id)->first();
		        $offer_pack=tbl_offer_package::where('tbl_id',$tbl_id)->orderBy('id','DESC')->first();
		        $customername=$enqdetails->firstname.' '.$enqdetails->lastname;
		           $chkarry=array('enqdetails'=>$enqdetails,
		                            'offer_pack'=>$offer_pack,
		                            
		                            );
		             $to=$enqdetails->email;
		            Mail::send('pages.newsletter', ['chkarry' => $chkarry], function ($m) use ($enqdetails) {
		                $m->from('sarbjitphp@netwebtechnologies.com', 'Package Booking');
		                $enqid="TEST package";
		                 $subject="[ENQUIRY ID: ".$enqid."#".$enqdetails->pckg_name."] # Travo Web Packages";
		                $m->to($enqdetails->email, $enqdetails->firstname)->subject($subject);
		            });
		            echo "success";
				// test check
		        
				
                
                // $message="Dear ".$customername." <br><br> ".$offer_msg." <br> Your Price :".$offer_price." <br>Book Your Packages <a href='https://travoweb.com/".$pagedirect."?pck=".$newpckid."%%".$newprice."%%".$newenqid."'>click here</a> <br><br>Do let us know if any further information required from our end. ";
                // $header="From:Travo Web <ticket@travoweb.com> \r \n";
                // // $header = "From: " . strip_tags('Travo Web <ticket@travoweb.com>') . "\r\n";
                // $header .= "Reply-To: ". strip_tags('ticket@travoweb.com') . "\r\n";
                // $header .= "CC: 'ticket@travoweb.com'\r\n";
                // $header .= "MIME-Version: 1.0\r\n";
                // $header .= "Content-Type: text/html; charset=UTF-8\r\n";
                // $mailtest =mail($to,$subject,$message,$header);
                // if($mailtest)
                // {
                // 	echo "success";
                // }
                
                
			}
			else
			{
				echo "fail";
			}
		}
    //logout
    public function adminlogout()
	{
		Session::flush();
		auth()->logout();
		setcookie("admin_ids", "", time() - 3600);
		return redirect()->intended('/admin');
	}

	public function tour_delete_pack(Request $request)
	{
		$id=$request->get('id');
		$sql=DB::table('tbl_tour_package')->where('id', $id)->delete();
        if($sql)
        {
            echo "success";
        }
        else
        {
            echo "fail";
        }
	}

	    public function query_list()
	    {
	    	
	    	if(session()->has('admin_id'))
			{
				$pckglist=tbl_query::orderBy('id', 'DESC')->get();
				
				 return view('admin.query-list', compact('pckglist'));
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
	    }
	    public function query_list1(Request $request)
		{
			
			if($request->get('search')!='')
			{
			$search=$request->get('search');
			$pckglist = tbl_query::where( 'id', 'LIKE', '%' . $search . '%' )->orWhere( 'contact_name', 'LIKE', '%' . $search . '%' )->orWhere( 'contact_email', 'LIKE', '%' . $search . '%' )->orWhere( 'contact_phone', 'LIKE', '%' . $search . '%' )->orWhere( 'contact_subject', 'LIKE', '%' . $search . '%' )->orWhere( 'contact_message', 'LIKE', '%' . $search . '%' )->orWhere( 'page', 'LIKE', '%' . $search . '%' )->paginate (10);
			}
			else
			{
			$pckglist=tbl_query::orderBy('id', 'DESC')->paginate(10);
			
			}
		       if ($request->ajax()) 
		       {
		           return view('admin.query-list', compact('pckglist'));
		       }
		       return view('admin.query-list',compact('pckglist'));
		}

		//testiminol
		public function add_testimonial()
	    {
	    	
	    	if(session()->has('admin_id'))
			{
				
				$data=0;
				
				 return view('admin.add-testimonial')->with('data',$data) ;
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
	    }

	    public function insert_testi(Request $request)
	    {
	    	$test_name=$request->get('test_name');
	    	$city_name=$request->get('city_name');
	    	$test_address=$request->get('test_address');
	    	$image=$request->get('image');
	    	$test_content=$request->get('test_content');
	    	$statusdata=$request->get('statusdata');
	    	$create_date=date('Y-m-d');
			$create_time=date('H:i:s');
	    	$querytest=array("name"=>$test_name,
	    					"address"=>$test_address,
	    					"city"=>$city_name,
	    					"comment"=>$test_content,
	    					"image"=>$image,
	    					"status"=>$statusdata,
	    					"create_date"=>$create_date,
							"create_time"=>$create_time,
	    					);
	    	$insertquery=tbl_testimonail::insert($querytest);
	    	if($insertquery)
	    	{
	    		echo "success";
	    	}
	    	else
	    	{
	    		echo "fail";
	    	}

	    }
	    public function testimonial_list()
	    {
	    	
	    	if(session()->has('admin_id'))
			{
				$pckglist=tbl_testimonail::orderBy('id', 'DESC')->get();
				
				 return view('admin.list-testimonial', compact('pckglist'));
				
			}
			else
			{

				return redirect()->intended('/admin');
			}
	    }
	    public function testimonial_list1(Request $request)
		{
			
			if($request->get('search')!='')
			{
			$search=$request->get('search');
			$pckglist = tbl_testimonail::where( 'id', 'LIKE', '%' . $search . '%' )->orWhere( 'name', 'LIKE', '%' . $search . '%' )->orWhere( 'city', 'LIKE', '%' . $search . '%' )->orWhere( 'comment', 'LIKE', '%' . $search . '%' )->paginate (10);
			}
			else
			{
			$pckglist=tbl_testimonail::orderBy('id', 'DESC')->paginate(10);
			
			}
		       if ($request->ajax()) 
		       {
		           return view('admin.list-testimonial', compact('pckglist'));
		       }
		       return view('admin.list-testimonial',compact('pckglist'));
		}

		public function testi_active(Request $request)
		{
			$id=$request->get('id');
			$idval=$request->get('idval');
			$statusupddate=tbl_testimonail::where('id',$id)->update(['status' => $idval]);
			if($statusupddate)
			{
				echo "success";
			}
			else
			{
				echo "fail";
			}
		}
		public function edit_testimonial(Request $request)
		{
			$testiid=base64_decode($request->get('testiid'));
			if(session()->has('admin_id'))
			{
				$testidata=tbl_testimonail::where('id', $testiid)->first();
				 
				$data=1;
				
			
				 return view('admin.add-testimonial', compact('testidata'))->with('data',$data);
				
			}
			else
			{

				return redirect()->intended('/admin');
			}

		}
		public function update_testi(Request $request)
	    {
	    	$test_name=$request->get('test_name');
	    	$city_name=$request->get('city_name');
	    	$test_address=$request->get('test_address');
	    	$image=$request->get('image');
	    	$testid=$request->get('testid');
	    	$test_content=$request->get('test_content');
	    	$statusdata=$request->get('statusdata');
	    	$create_date=date('Y-m-d');
			$create_time=date('H:i:s');
	    	$querytest=array("name"=>$test_name,
	    					"address"=>$test_address,
	    					"city"=>$city_name,
	    					"comment"=>$test_content,
	    					"image"=>$image,
	    					"status"=>$statusdata,
	    					"create_date"=>$create_date,
							"create_time"=>$create_time,

	    					);
	    	$insertquery=tbl_testimonail::where('id',$testid)->update($querytest);
	    	if($insertquery)
	    	{
	    		echo "success";
	    	}
	    	else
	    	{
	    		echo "fail";
	    	}

	    }
} //mainfile

