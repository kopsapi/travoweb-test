<?php

namespace App\Http\Controllers;

use session;
use DB;
use Cookie;
use App\flight;
use App\Price;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\tbl_general_setting;
use DateTime;
use App\hotel_city;
use App\tbl_token;
use App\tbl_flight_book;
use App\tbl_mytrip;
use App\tbl_payment_order;
use PDF;
use Dompdf\Dompdf;
class ApiFlightController extends Controller
{
    public function apiflightsearch(Request $request)
    {
       
       
        $arr=array();
        $orign = explode(",",$request->get("flight_from"));
        $destinew1 = explode(",",$request->get("flight_to"));

        $orginexnew = explode('(',$orign[0]);
        $Originn=$orginexnew[0];
        $destin = explode('(',$destinew1[0]);
       


        $NoOfAdults1=0;
        $NoOfChild1=0;
        $NoOfAdultsarray=array();
        $NoOfchildarray=array();
        $childagearray=array();
        $ChildAge=array();
        $dep_date=explode(",",$request->get('flight_dep'));

        $departure_date=explode('/',$dep_date[0]);  //array check wehn multiple select
        $depdate = $departure_date[2]."-".$departure_date[1]."-".$departure_date[0];
        $new_departure_date=$departure_date[2]."-".$departure_date[1]."-".$departure_date[0]."T00:00:00";

        $TokenId=$request->get("TokenId");
        $AdultCount=$request->get("adults");
        $ChildCount=$request->get("children");
        $EndUserIp=$request->get("localip");
        $InfantCount=$request->get("infant");
        $JourneyType=$request->get("journytype");
        $originnew=$orign[0];
        $orginex = explode('(',$orign[0]);
        $Origin=$orginex[0];
        // $Origin=$request->get("flight_from");
       

        $destinew=$destinew1[0];
        $desti = explode('(',$destinew1[0]);
        $Destination=$desti[0];
        // $Destination=$request->get("flight_to");

        $flighcalssv = explode('%', $request->get("flight_class"));
        $FlightCabinClass=$flighcalssv[0];
        $FlightCabinName=$flighcalssv[1];
        $PreferredDepartureTime=$new_departure_date;
        $PreferredArrivalTime=$new_departure_date;
        $PreferredAirlines ="";
        $DirectFlight = false;
        $OneStopFlight = false;
        $flight_return1='';

        //hotel set
       

        $ResultCount ='0';
        $PreferredCurrency="INR";
       
       
        //hotelend
        if($JourneyType=='1')
        {
            $form_data = array(
                'EndUserIp'=>$EndUserIp,
                'TokenId'=>$TokenId,
                'AdultCount'=>$AdultCount,
                'ChildCount'=>$ChildCount,
                'InfantCount' => $InfantCount,
                'DirectFlight' =>$DirectFlight,
                'OneStopFlight'=>$OneStopFlight,
                'JourneyType' => $JourneyType,
                'PreferredAirlines' =>$PreferredAirlines,
                'Segments' => [array(
                    'Origin' =>$Origin,
                    'Destination' =>$Destination,
                    'FlightCabinClass' =>$FlightCabinClass,
                    'PreferredDepartureTime' =>$PreferredDepartureTime,
                    'PreferredArrivalTime'=>$PreferredArrivalTime,

                )],

            );
        }
        else if($JourneyType=='2')
        {
            $flight_return1=$request->get('flight_return');
            $flightretuen=explode('/',$flight_return1);
            $new_flightretuen=$flightretuen[2]."-".$flightretuen[1]."-".$flightretuen[0]."T00:00:00";
            $form_data = array(
                'EndUserIp'=>$EndUserIp,
                'TokenId'=>$TokenId,
                'AdultCount'=>$AdultCount,
                'ChildCount'=>$ChildCount,
                'InfantCount' => $InfantCount,
                'DirectFlight' =>$DirectFlight,
                'OneStopFlight'=>$OneStopFlight,
                'JourneyType' => $JourneyType,
                'PreferredAirlines' =>$PreferredAirlines,
                'Segments' => [array(
                    'Origin' =>$Origin,
                    'Destination' =>$Destination,
                    'FlightCabinClass' =>$FlightCabinClass,
                    'PreferredDepartureTime' =>$PreferredDepartureTime,
                    'PreferredArrivalTime'=>$PreferredArrivalTime,

                ),
                    array(
                        'Origin' =>$Destination,
                        'Destination' =>$Origin,
                        'FlightCabinClass' =>$FlightCabinClass,
                        'PreferredDepartureTime' =>$new_flightretuen,
                        'PreferredArrivalTime'=>$new_flightretuen,
                    )],

            );
        }
        else
        {

            for($multi=0;$multi<count($orign);$multi++)
            {

                $orginex = explode('(',$orign[$multi]);
                $orign1 =  $orginex[0];
                $desti1 = explode('(',$destinew1[$multi]);
                $Destination1=$desti1[0];
                $departure_date=explode('/',$dep_date[$multi]);
                $depdate = $departure_date[2]."-".$departure_date[1]."-".$departure_date[0];
                $new_departure_date1=$departure_date[2]."-".$departure_date[1]."-".$departure_date[0]."T00:00:00";


                $chkmultiarray[]= array(
                    'Origin' =>$orign1,
                    'Destination' =>$Destination1,
                    'FlightCabinClass' =>$FlightCabinClass,
                    'PreferredDepartureTime' =>$new_departure_date1,
                    'PreferredArrivalTime'=>$new_departure_date1,

                );
            }

            $form_data = array(
                'EndUserIp'=>$EndUserIp,
                'TokenId'=>$TokenId,
                'AdultCount'=>$AdultCount,
                'ChildCount'=>$ChildCount,
                'InfantCount' => $InfantCount,
                'DirectFlight' =>$DirectFlight,
                'OneStopFlight'=>$OneStopFlight,
                'JourneyType' => $JourneyType,
                'PreferredAirlines' =>$PreferredAirlines,
                'Segments' => $chkmultiarray,

            );
        }

        $data_string = json_encode($form_data);
       
        $ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Search/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);

        $result1=json_decode($result,true);
      




        //endhotelapi search

        $message = $result1['Response']['Error']['ErrorMessage'];

        if($result1['Response']['Error']['ErrorCode']!='0')
        {

          

            if($result1['Response']['Error']['ErrorCode']=='6' )
            {
                // return redirect()->intended('/index');
               echo $message;
            }

            else
            {
                // return redirect()->intended('/index');
                 echo $message;
            }
            die();
        }
        else
        {
            $res =  $result1['Response']['ResponseStatus'];
            $airportdename1='';
            $airportdeptname='';
            $flight='';
            $airportdeptcityname='';
            $_SESSION['TraceId']=$result1['Response']['TraceId'];
           
            

            $flightmargin = $request->get('flight_margin');
            $curr=$request->get('maincurrency');
            foreach($result1 as $flightsearch)
            {  //echo "<pre>"; print_r($flightsearch); die;
                $resu = !empty($flightsearch['Results'])?$flightsearch['Results']:array();
                // echo "<pre>";


                if(!empty($resu))
                {
                    for($ma=0;$ma<count($resu);$ma++)
                    { //print_r($flightsearch['Results'][$ma]); die;
                        if(!empty($flightsearch['Results'][$ma])){
                            $i = 0;
                            foreach($flightsearch['Results'][$ma] as $dta )
                            {
                                if(!empty($dta['Fare']['BaseFare']))
                                {
                                    $mainp=$dta['Fare']['BaseFare'];
                                    $margin = ($mainp * $flightmargin)/100;
                                    $mainprice = $mainp + $margin;
                                    $price= $this->pricenew($mainprice,$dta['Fare']['Tax'],$curr);


                                    $resu[$ma][$i]['flight_fare']= $price[0]+$price[2];
                                    $resu[$ma][$i]['flight_base_fare']= $price[0];
                                    $resu[$ma][$i]['flight_base_tax']= $price[2];
                                    $resu[$ma][$i]['flightcurrency']= $price[1];
                                    $resu[$ma][$i]['flightmargin']= $mainprice;
                                    $resu[$ma][$i]['flightmainprice']= $mainp;
                                    // $resu[$ma][$i]['flight_fare'] = $this->convert('USD',round($dta['Fare']['BaseFare'] + $dta['Fare']['Tax']));
                                    // $resu[$ma][$i]['flight_fare']=round($dta['Fare']['BaseFare'] + $dta['Fare']['Tax']);
                                }
                                else
                                {

                                    $resu[$ma][$i]['flight_fare'] = '0';

                                }

                                if(!empty($dta['Segments'][0][0]['Origin']['DepTime'])){
                                    $devdate = explode('T',$dta['Segments'][0][0]['Origin']['DepTime']);
                                    $resu[$ma][$i]['time_filter'] = date("H:i:s" , strtotime($devdate[1]));
                                }
                                else
                                {
                                    $resu[$ma][$i]['time_filter'] = '1';
                                }
                                //print_r($resu); die;
                                // alsi flight name
                                if(!empty($dta['Segments'][0]) && count($dta['Segments'][0]) == 1 ){
                                    $resu[$ma][$i]['multi_stop'] = '0';
                                }else{
                                    $resu[$ma][$i]['multi_stop'] = '1';
                                }
                                if(!empty($dta['Segments'][0][0]['Airline']['AirlineCode'])){
                                    $resu[$ma][$i]['air_code'] = $dta['Segments'][0][0]['Airline']['AirlineCode'];
                                }else{
                                    $resu[$ma][$i]['air_code'] = '';
                                }

                                $array[] = $dta;

                                $i++;

                            }

                        }
                        // $uniflightsearch= $unique->values()->all();
                        //$array[] = $flightsearch['Results'][$ma];
                    }

                    $minprice='';
                    for($il=0;$il<count($array);$il++)
                    {
                        $unique[]=array( 'name' =>$array[$il]['Segments'][0][0]['Airline']['AirlineName'],

                            'code' => $array[$il]['Segments'][0][0]['Airline']['AirlineCode']
                        );
                        // echo "<pre>"; print_r($array[$il]['Fare']['BaseFare']);

                        $minprice1[]= round( $array[$il]['Fare']['BaseFare'] + $array[$il]['Fare']['Tax']);
                        $maxprice1[]= $array[$il]['Fare']['BaseFare'];
                    }
                    $minprice = min($minprice1);
                    $maxprice = max($minprice1);

                    $unique= array_unique($unique,SORT_REGULAR);
                    // print_r($mainflightq);
                    // die;
                    // $request->session()->put('sess_arr', $resu);
                    // $request->session()->put('sess_arr_new', $resu);
                    for($seg_i=0;$seg_i<count($flightsearch['Results'][0][0]['Segments'][0]);$seg_i++)
                    {
                        if($flightsearch['Results'][0][0]['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode']==$Destination)
                        {
                            $airportdename1= $flightsearch['Results'][0][0]['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'];
                            $airportdeptname =  $flightsearch['Results'][0][0]['Segments'][0][$seg_i]['Destination']['Airport']['AirportName'];
                            $airportdeptcityname =  $flightsearch['Results'][0][0]['Segments'][0][$seg_i]['Destination']['Airport']['CityName'];
                        }

                    }
                }


                // $airportunique= array_unique($flightsearch['Results'][0][0]['Segments'][0][0]['Airline']['AirlineName']);



            }

            if($res=='1')
            {

                $flightorign = $result1['Response']['Origin'];
                $flightdep =  $result1['Response']['Destination'];

                $airportorignname =  $result1['Response']['Results'][0][0]['Segments'][0][0]['Origin']['Airport']['AirportName'];
                $airportorigncityname =  $result1['Response']['Results'][0][0]['Segments'][0][0]['Origin']['Airport']['CityName'];

                $mainarray = array('AdultCount'=>$AdultCount,
                    'ChildCount'=>$ChildCount,
                    'InfantCount' =>$InfantCount,
                    'flightorign'=>$flightorign,
                    'flightdep' =>$flightdep,
                    'FlightCabinName'=>$FlightCabinName,
                    'depdate'=>$depdate,
                    'dep_date'=>$dep_date,
                    'PreferredArrivalTime'=>$PreferredArrivalTime,
                    'airportorignname' =>$airportorignname,
                    'airportorigncityname'=>$airportorigncityname,
                    'airportdeptname'=>$airportdeptname,
                    'airportdeptcityname'=>$airportdeptcityname,
                    'airportdename1'=>$airportdename1,
                    'flight'=>$flight,
                    'JourneyType' => $JourneyType,
                    'destinew1' =>$destinew1,
                    'orign'=>$orign,
                    'flight_return1' =>$flight_return1,
                    'minprice' =>$minprice,
                    'maxprice' => $maxprice,
                );
            }
            $chkvalue['traceid']=$result1['Response']['TraceId'];
            $chkvalue['resu']=$resu;
            // $chkvalue['array']=$array;
           
            $chkvalue['data_string']=$data_string;
          
            
           echo json_encode($chkvalue);
            

        }//eror close
    }
    public function apiflightbook(Request $request)
    {
            $ResultIndex=$request->get('resultindex');
            $ResultIndex1=$request->get('resultindex1');
            $journytype=$request->get('journytype');
            $TraceId = $request->get('flighttraceid');
            $flightcabinclass=$request->get('flightcabinclass');
            $TokenId=$request->get('TokenId');
            $EndUserIp=$request->get('localip');
            $curr=$request->get('maincurrency');
            
            $form_data_return = array(
                'EndUserIp'=>$EndUserIp,
                'TokenId'=>$TokenId,
                'TraceId'=>$TraceId,
                'ResultIndex'=>$ResultIndex1,
            );
            $data_string_return = json_encode($form_data_return);

            $ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/FareQuote/');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_return);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string_return))
            );
            $result_return = curl_exec($ch);
            if(!empty($result_return))
            {
                $result1_return=json_decode($result_return,true);
                $pricechange_return=$result1_return['Response']['IsPriceChanged'];
                $ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/SSR/');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_return);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string_return))
                );

                $mealresult_return = curl_exec($ch);
                $mealresult1_return=json_decode($mealresult_return,true);
                $meal_return = !empty($mealresult1_return['Response'])?$mealresult1_return['Response']:array();
                if(!empty($meal_return['MealDynamic']))
                {
                    
                    $mealpmargin = $request->get('margin');
                    for($mealc=0;$mealc<count($meal_return['MealDynamic'][0]);$mealc++)
                    {
                        $dmealprice = $meal_return['MealDynamic'][0][$mealc]['Price'];
                        $totaldmealprice = ($dmealprice * $mealpmargin) /100;
                        $cmealprice=$dmealprice + $totaldmealprice;
                        $mprice= $this->mealprice($cmealprice,$dmealprice,$curr);
                        $meal_return['MealDynamic'][0][$mealc]['mealmainprice']=number_format($mprice[2], 1, '.', '');
                        $meal_return['MealDynamic'][0][$mealc]['mealmarginprice']=number_format($cmealprice, 1, '.', '');
                        $meal_return['MealDynamic'][0][$mealc]['mealfprice']=number_format($mprice[0], 1, '.', '');
                        $meal_return['MealDynamic'][0][$mealc]['mealcur']=$mprice[1];


                    }
                }
                if(!empty($meal_return['Baggage']))
                {
                    for($bag=0;$bag<count($meal_return['Baggage'][0]);$bag++)
                    {
                        
                        $bagmargin = $request->get('margin');

                        $bagmainprice = $meal_return['Baggage'][0][$bag]['Price'];
                        $totalbagprice = ($bagmainprice * $bagmargin) /100;
                        $bagmarginprice=$bagmainprice + $totalbagprice;
                        $mprice= $this->mealprice($bagmarginprice,$bagmainprice,$curr);
                        $meal_return['Baggage'][0][$bag]['bagmainprice']= number_format($mprice[2], 1, '.', '');
                        $meal_return['Baggage'][0][$bag]['bagmarginprice']= number_format($bagmarginprice, 1, '.', '');
                        $meal_return['Baggage'][0][$bag]['bagfprice']= number_format($mprice[0], 1, '.', '');
                        $meal_return['Baggage'][0][$bag]['bagcur']=$mprice[1];


                    }
                }
                foreach($result1_return as $flightretrun)
                {
                    $resu_return = !empty($flightretrun['Results'])?$flightretrun['Results']:array();
                    if(!empty($resu_return))
                    {
                       

                        $flightmargin = $request->get('margin');
                        $othervharges = $resu_return['Fare']['OtherCharges'] + $resu_return['Fare']['TdsOnCommission'] + $resu_return['Fare']['TdsOnPLB'] + $resu_return['Fare']['TdsOnIncentive'] + $resu_return['Fare']['ServiceFee'];
                        $taxs=$resu_return['Fare']['Tax'];

                        $mainp=$resu_return['Fare']['BaseFare'];

                        $margin = ($mainp * $flightmargin)/100;
                        $mainprice = $mainp + $margin;

                        $price= $this->price($mainprice,$taxs,$othervharges,$mainp,$curr);
                        $resu_return['Fare']['rflightmainprice']= number_format($price[5], 1, '.', '');
                        $resu_return['Fare']['rflightmmargin']=number_format($mainprice, 1, '.', '');
                        $resu_return['Fare']['rflightfare']=number_format($price[0], 1, '.', '');
                        $resu_return['Fare']['rflighttax']= number_format($price[2], 1, '.', '');
                        $resu_return['Fare']['rflightcurrency']= $price[1];
                        $resu_return['Fare']['rflightothercharges']= number_format($price[3], 1, '.', '');
                        $resu_return['Fare']['rflightconvr']=$price[1];
                    }


                }



            }

            //endinbound index
            $form_data = array(
                'EndUserIp'=>$EndUserIp,
                'TokenId'=>$TokenId,
                'TraceId'=>$TraceId,
                'ResultIndex'=>$ResultIndex,
            );
            $data_string = json_encode($form_data);
            $ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/FareQuote/');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );
            $result = curl_exec($ch);
            $result1=json_decode($result,true);
            if($result1['Response']['Error']['ErrorCode']!='0')
            {
                $message = $result1['Response']['Error']['ErrorMessage'];
                echo $message;
                $chknew['error']=$message;
                
                
            }
            else
            {


                $pricechangeoneway=$result1['Response']['IsPriceChanged'];

                //mealcode and seat code
                $ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/SSR/');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );

                $mealresult = curl_exec($ch);
                $mealresult1=json_decode($mealresult,true);
                $meal = !empty($mealresult1['Response'])?$mealresult1['Response']:array();
                if(!empty($meal['MealDynamic']))
                {
                   
                    $mealpmargin =$request->get('margin');
                    for($mealc=0;$mealc<count($meal['MealDynamic'][0]);$mealc++)
                    {
                        $dmealprice = $meal['MealDynamic'][0][$mealc]['Price'];
                        $totaldmealprice = ($dmealprice * $mealpmargin) /100;
                        $cmealprice=$dmealprice + $totaldmealprice;
                        $mprice= $this->mealprice($cmealprice,$dmealprice,$curr);
                        $meal['MealDynamic'][0][$mealc]['mealmainprice']=number_format($mprice[2], 1, '.', '');
                        $meal['MealDynamic'][0][$mealc]['mealmarginprice']= number_format($cmealprice, 1, '.', '');
                        $meal['MealDynamic'][0][$mealc]['mealfprice']=number_format($mprice[0], 1, '.', '');
                        $meal['MealDynamic'][0][$mealc]['mealcur']=$mprice[1];


                    }
                }
                if(!empty($meal['Baggage']))
                {
                    for($bag=0;$bag<count($meal['Baggage'][0]);$bag++)
                    {
                       
                        $bagmargin = $request->get('margin');

                        $bagmainprice = $meal['Baggage'][0][$bag]['Price'];
                        $totalbagprice = ($bagmainprice * $bagmargin) /100;
                        $bagmarginprice=$bagmainprice + $totalbagprice;
                        $mprice= $this->mealprice($bagmarginprice,$bagmainprice,$curr);
                        $meal['Baggage'][0][$bag]['bagmainprice']= number_format($mprice[2], 1, '.', '');
                        $meal['Baggage'][0][$bag]['bagmarginprice']= number_format($bagmarginprice, 1, '.', '');
                        $meal['Baggage'][0][$bag]['bagfprice']= number_format($mprice[0], 1, '.', '');
                        $meal['Baggage'][0][$bag]['bagcur']=$mprice[1];


                    }
                }
                //end mealcode
                foreach($result1 as $flightbook)
                {
                    $resu = !empty($flightbook['Results'])?$flightbook['Results']:array();
                    $totalbasefare=0;
                    if(!empty($resu))
                    {
                        $flightmargin1=$request->get('margin');

                        $flightmargin = $request->get('margin');

                        $othervharges = $resu['Fare']['OtherCharges'] + $resu['Fare']['TdsOnCommission'] + $resu['Fare']['TdsOnPLB'] + $resu['Fare']['TdsOnIncentive'] + $resu['Fare']['ServiceFee'];
                        $taxs=$resu['Fare']['Tax'];

                        $mainp=$resu['Fare']['BaseFare'];

                        $margin = ($mainp * $flightmargin)/100;
                        $mainprice = $mainp + $margin;

                        $price= $this->price($mainprice,$taxs,$othervharges,$mainp,$curr);
                        $resu['Fare']['flightmainprice']= number_format($price[5], 1, '.', '');
                        $resu['Fare']['flightmmargin']=number_format($mainprice, 1, '.', '');
                        $resu['Fare']['flightfare']= number_format($price[0], 1, '.', '');
                        $resu['Fare']['flighttax']=number_format($price[2], 1, '.', '');
                        $resu['Fare']['flightcurrency']=$price[1];
                        $resu['Fare']['flightothercharges']= number_format($price[3], 1, '.', '');
                        $resu['Fare']['flightconvr']=$price[4];
                        $resu['Fare']['curencycode']=Cookie::get('currencycode');


                    }

                    for($seg_i=0;$seg_i<count($flightbook['Results']['Segments'][0]);$seg_i++)
                    {


                        $airportdename1= $flightbook['Results']['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'];
                        $airportdeptname =  $flightbook['Results']['Segments'][0][$seg_i]['Destination']['Airport']['AirportName'];
                        $airportdeptcityname =  $flightbook['Results']['Segments'][0][$seg_i]['Destination']['Airport']['CityName'];


                    }
                }
                $airlinename =  $flightbook['Results']['Segments'][0][0]['Airline']['AirlineName'];
                $airportorgincode=  $result1['Response']['Results']['Segments'][0][0]['Origin']['Airport']['AirportCode'];
                $airportorignname =  $result1['Response']['Results']['Segments'][0][0]['Origin']['Airport']['AirportName'];
                $airportorigncityname =  $result1['Response']['Results']['Segments'][0][0]['Origin']['Airport']['CityName'];
                $airportorigndepdate =  $result1['Response']['Results']['Segments'][0][0]['Origin']['DepTime'];
                $mainarray =array(
                    'airportorgincode'=>$airportorgincode,
                    'airportorignname' =>$airportorignname,
                    'airportorigncityname'=>$airportorigncityname,
                    'airportdeptname'=>$airportdeptname,
                    'airportdeptcityname'=>$airportdeptcityname,
                    'airportdename1'=>$airportdename1,
                    'airportorigndepdate'=>$airportorigndepdate,
                    'journytype' =>$journytype,
                    'flightcabinclass'=>$flightcabinclass,
                    'pricechangereturn'=>$pricechange_return,
                    'pricechangeoneway'=>$pricechangeoneway,
                    'airlinename'=>$airlinename,


                );
               $chknew['resu']=$resu;
              
                $chknew['meal']=$meal;
                $chknew['meal_return']=$meal_return;
                $chknew['mainarray']=$mainarray;
                
              // $result = array('response'=>1,'status'=>'success','message'=>$chknew);
                
                
                echo json_encode($chknew);
               
                // return view('pages.flight-book')->with(compact('resu'))->with(compact('mainarray'))->with(compact('meal'))->with(compact('resu_return'))->with(compact('meal_return'));
            }
            
    }
     public function apiflight_bookview(Request $request)
    {
        
    	$TraceId = $request->get('flighttraceid');
    	 $TokenId=$request->get('TokenId');
    	 $EndUserIp=$request->get('localip');
    	 $journytype= $request->get('journytype');
       
    	$titleadult=  explode(",",$request->get('titleadult'));
      
    	$firstnameadult=  explode(",",$request->get('firstnameadult'));
    	$lastnameadult=  explode(",",$request->get('lastnameadult'));
    	$adultdob_date=  explode(",",$request->get('adultdob'));
        //emergency
        $eme_name=$request->get('eme_name');
        $ema_phone=$request->get('ema_phone');
        $eme_relation=$request->get('eme_relation');

        $ff_name=$request->get('ff_name'); //airlineff number
        $ff_number1=$request->get('ff_number');
        if($ff_number1=="")
        {
            $ff_name="";
            $ff_number="";
        }
        else
        {
            $ff_name=$formdata['ff_name'];
            $ff_number=$formdata['ff_number'];
        }
    	$passportcheck=$request->get('passportcheck');
        $flightcabinclass=$request->get('flightcabinclass');
        $childfirstname=array();
        $childcount=0;
       
         
        if(!empty($request->get('childtitle')))
        {
           
            $childtitle=explode(",",$request->get('childtitle'));
            $childfirstname=  explode(",",$request->get('childfirstname'));
            $childlastname=explode(",",$request->get('childlastname'));
            $childdob_date= explode(",",$request->get('childdob'));
            $childbasefare=explode(",",$request->get('childbasefare'));
            $childtaxfare= explode(",",$request->get('childtaxfare'));
            $childtxnfeepub=  explode(",",$request->get('childtxnfeepub'));
            $childtxnfeeofrd= explode(",",$request->get('childtxnfeeofrd'));
            $childcount=count($childfirstname);
        }
        
        $infacount=0;
      
            if(!empty($request->get('infatitle')))
            {
                $infatitle= $infatitle;
                $infafistname= explode(",",$request->get('infafistname'));
                $infalastname= explode(",",$request->get('infalastname'));
                $infadob_date= explode(",",$request->get('infadob'));
                $infantbasefare=explode(",",$request->get('infantbasefare'));
                $infanttaxfare= explode(",",$request->get('infanttaxfare'));
                $infanttxnfeepub= explode(",",$request->get('infanttxnfeepub'));
                $infanttxnfeeofrd= explode(",",$request->get('infanttxnfeeofrd'));
                $infacount=count($infafistname);
            }
            $AddressLine1=$request->get('address1');
            $City=  $request->get('city');
            $mealname=  $request->get('totalmealname');
            $mealprice=$request->get('totalmealprice');
            $meal_orgprice=$request->get('totalorgmealprice');

            $flightlastprice = $request->get('flightbaseprice');
            $flightmarginprice =$request->get('flightbasemarginprice');
            $flighttaxprice=$request->get('flighttaxprice');
            $flightotherprice=$request->get('flightotherprice');
            $convertcurrecny= $request->get('convertcurrecny');
            $currencyicon=$request->get('flightcurrencyicon');
            //return
            $mealname_return= $request->get('totalmealname_return');
            $mealprice_return= $request->get('totalmealprice_return');
            $meal_orgprice_return= $request->get('totalorgmealprice_return');

            $flightlastprice_return = $request->get('flightbaseprice_r');
            $flightmarginprice_return =$request->get('flightbasemarginprice_r');
            $flighttaxprice_return=$request->get('flighttaxprice_r');
            $flightotherprice_return= $request->get('flightotherprice_r');

            $bag_weight=$request->get('bagweight');
            $bag_price=$request->get('newbagprice');
            $bag_orgprice=$request->get('orgbagprice');
            //return
            $bag_weight_return=$request->get('bagweight_return');
            $bag_price_return=$request->get('newbagprice_return');
            $bag_orgprice_return=$request->get('orgbagprice_return');
            $grandprice= $request->get('maingradflightprice');
            if($request->get('country') !='')
            {
                $countrycde = explode('%%',$request->get('country'));
                $CountryCode= $countrycde[0];
                $CountryName=$countrycde[1];
            }
            $Email= $request->get('email');
            $refund= $request->get('refund');
            $ContactNo= $request->get('phone');
            $gstaddress=  $request->get('gstaddress');
            $gstemail=  $request->get('gstemail');
            $gstnumber=  $request->get('gstnumber');
            $gstcompname=  $request->get('gstcompname');
            $gstcontactno=  $request->get('gstcontactno');

            $lcccheck= $request->get('lcccheck');
 
            $ResultIndex=$request->get('resultindex');

            //fareruledata
            $adultbasefare= $request->get('adultbasefare');
            $adulttaxfare= $request->get('adulttaxfare');

            $adulttxnfeeofrd=number_format($request->get('adulttxnfeeofrd'),1); 
            $adulttxnfeepub=number_format($request->get('adulttxnfeepub'),1); 
            $Currency=  $request->get('Currency');
            $YQTax=number_format($request->get('YQTax'),1);
            $OtherCharges=number_format($request->get('OtherCharges'),1); 
            $Discount=number_format($request->get('Discount'),1); 
            $PublishedFare=  $request->get('PublishedFare');
            $OfferedFare=number_format($request->get('OfferedFare'),1);
            $TdsOnCommission=number_format( $request->get('TdsOnCommission'),1);
            $TdsOnPLB=number_format($request->get('TdsOnPLB'),1);
            $TdsOnIncentive=number_format($request->get('TdsOnIncentive'),1);
            $ServiceFee=number_format($request->get('ServiceFee'),1); 

            $baggage=$request->get('baggage');
            if($baggage !='')
            {

                $baggage1=explode('%',$baggage);
                $WayType = $baggage1[2];
                $bagCode = $baggage1[3];
                $bagDescription = $baggage1[4];
                $Weight = $baggage1[0];
                $Price = $baggage1[1];
                $Origin = $baggage1[5];
                $Destination = $baggage1[6];
                $bagflightnumber=$baggage1[10];
                $bagflightcode=$baggage1[9];

            }
            else
            {
                $WayType = "";
                $bagCode = "";
                $bagDescription = "";
                $Weight = "";
                $Price = "";
                $Origin ="";
                $Destination = "";
                $bagflightcode="";
                $bagflightnumber="";

            }
             if(!empty($request->get('mealdynamic')))
            {
                $mealdynamic=$request->get('mealdynamic');
                if($mealdynamic !='')
                {
                    $mealdyn= explode('%',$mealdynamic);
                    $Code=$mealdyn[0];
                    $Description=$mealdyn[1];
                    $mealway=$formdata['mealway'];
                     $mealflightname=$formdata['mealflightname'];
                     $mealflightcode=$formdata['mealflightcode'];
                     $mealquantaty=$formdata['mealquantaty'];
                }
            }
            else
            {
                $Code="";
                $Description="";
                $mealway="";
                $mealflightname="";
                $mealflightcode="";
                $mealquantaty="";
            }
            $resultindex_return='';
            if(!empty($request->get('resultindex_return')))
            {
                $ff_name_return1=$request->get('ff_name_return');
                $ff_number_return1=$request->get('ff_number_return');
                if($ff_number_return1=="")
                {
                    $ff_name_return="";
                    $ff_number_return="";
                }
                else
                {
                    $ff_name_return=$request->get('ff_name_return');
                    $ff_number_return=$request->get('ff_number_return');
                }
                $lcccheck_return= $request->get('lcccheck_return');
                $resultindex_return= $request->get('resultindex_return');
                $adultbasefarereturn= $request->get('adultbasefarereturn');
                $adulttaxfarereturn=$request->get('adulttaxfarereturn');
                $adulttxnfeeofrdreturn=$request->get('adulttxnfeeofrdreturn');
                $adulttxnfeepubreturn= $request->get('adulttxnfeepubreturn');
                
                $childbasefarereturn=explode(",",$request->get('childbasefarereturn'));
                if(!empty($childbasefarereturn))
                {
                    $childbasefarereturn= explode(",",$request->get('childbasefarereturn'));
                    $childtaxfarereturn= explode(",",$request->get('childtaxfarereturn'));
                    $childtxnfeeofrdreturn= explode(",",$request->get('childtxnfeeofrdreturn'));
                    $childtxnfeepubreturn= explode(",",$request->get('childtxnfeepubreturn'));
                }
                $infacount=0;
                if(!empty($request->get('infatitle')))
                {

                    $infantbasefarereturn=  explode(",",$request->get('infantbasefarereturn'));
                    $infanttaxfarereturn=   explode(",",$request->get('infanttaxfarereturn'));
                    $infanttxnfeepubreturn=  explode(",",$request->get('infanttxnfeepubreturn'));
                    $infanttxnfeeofrdreturn=  explode(",",$request->get('infanttxnfeeofrdreturn'));
                    $infacount=count($infafistname);
                }

                $Currency_return=$request->get('Currency_return');
                $YQTax_return= $request->get('YQTax_return');
                $OtherCharges_return=  $request->get('OtherCharges_return');

                $Discount_return= $request->get('Discount_return');
                $PublishedFare_return= $request->get('PublishedFare_return');
                $OfferedFare_return= $request->get('OfferedFare_return');
                $TdsOnCommission_return= $request->get('TdsOnCommission_return');
                $TdsOnPLB_return= $request->get('TdsOnPLB_return');

                $TdsOnIncentive_return= $request->get('TdsOnIncentive_return');
                $ServiceFee_return= $request->get('ServiceFee_return');
            }
            $adultcount=count($firstnameadult);
            $username='';
            $user_id='';
            $userphone='';
            $usercustname = $firstnameadult[0].' '.$lastnameadult[0];
            if($lcccheck=='1')
            {
                $totaladult=count($firstnameadult);
                $adultbasefare_new=$adultbasefare / $totaladult;
                $adulttaxfare_new=$adulttaxfare/$totaladult;

                for($adult=0;$adult<count($firstnameadult);$adult++)
                {
                    if($titleadult[$adult]=='Mr')
                    {
                        $adultgender="1";
                    }
                    else
                    {
                        $adultgender="2";
                    }
                    if($adult=='0')
                    {
                        $paxval="true";
                       
                    }
                    else
                    {
                        $paxval="false";
                       
                    }
                    if($passportcheck==1)
                    {
                        $PassportNo=$request->get(['passportno'][$adult]);
                        $passportexp=explode('/',$$request->get(['passportexp'][$adult]));
                        $PassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $PassportNo="";
                        $PassportExpiry="";
                    }
                    $adultdob1=explode('/',$adultdob_date[$adult]);
                    $adultdob = $adultdob1[2]."-".$adultdob1[1]."-".$adultdob1[0]."T00:00:00";
                    if($journytype=='3') //multicity api not show bagage and meal
                    {
                        $addultpassengerdetail[]=array(
                            'Title' =>$titleadult[$adult],
                            'FirstName'=>$firstnameadult[$adult],
                            'LastName'=>$lastnameadult[$adult],
                            'PaxType' =>'1',
                            'DateOfBirth'=>$adultdob,
                            "PassportNo"=> $PassportNo,
                            "PassportExpiry"=> $PassportExpiry,
                            'Gender'=>$adultgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,

                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> $paxval,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$adultbasefare_new,
                                "Tax"=> $adulttaxfare_new,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $adulttxnfeeofrd,
                                "AdditionalTxnFeePub"=> $adulttxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],
                        );
                    }
                    else
                    {
                        $addultpassengerdetail[]=array(
                            'Title' =>$titleadult[$adult],
                            'FirstName'=>$firstnameadult[$adult],
                            'LastName'=>$lastnameadult[$adult],
                            'PaxType' =>'1',
                            'DateOfBirth'=>$adultdob,
                            'Gender'=>$adultgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=> $PassportNo,
                            "PassportExpiry"=>  $PassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> $paxval,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$adultbasefare_new,
                                "Tax"=> $adulttaxfare_new,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $adulttxnfeeofrd,
                                "AdditionalTxnFeePub"=> $adulttxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],
                            "Baggage"=>[array(
                                                     "AirlineCode"=>$bagflightcode,
                                                    "FlightNumber"=>$bagflightnumber,
                                                    "WayType"=>$WayType,
                                                    "Code"=> $bagCode,
                                                    "Description"=>$bagDescription,
                                                    "Weight"=> $Weight,
                                                    "Currency"=> $Currency,
                                                    "Price"=>$Price,
                                                    "Origin"=> $Origin,
                                                    "Destination"=> $Destination,
                            )],
                            "Meal Dynamic"=>[array(
                               "AirlineCode"=>$mealflightcode,
                                                        "FlightNumber"=>$mealflightname,
                                                        "WayType"=>$mealway,
                                                        "Code"=> $Code,
                                                        "Description"=> $Description,
                                                        "AirlineDescription"=>$mealname,
                                                        "Quantity"=>$mealquantaty,
                                                        "Currency"=> $Currency,
                                                        "Price"=>$meal_orgprice,
                                                        "Origin"=> $Origin,
                                                        "Destination"=> $Destination,
                            )],


                        );
                    } // else condtion journy time close




                }
               
                if(count($childfirstname)!="0")
                {

               
                    $totalchild=count($childfirstname);
                    $childfare_new=$childbasefare / $totalchild;
                    $childtaxfare_new=$childtaxfare/$totalchild;
                }
                for($child=0;$child<count($childfirstname);$child++)
                {
                    if($childtitle[$child]=='Mr')
                    {
                        $childgender="1";
                    }
                    else
                    {
                        $childgender="2";
                    }
                     if($passportcheck==1)
                    {
                        $childPassportNo=$$request->get(['childpassportno'][$child]);
                        $passportexp=explode('/',$request->get(['childpassportexp'][$child]));
                        $childPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $childPassportNo="";
                        $childPassportExpiry="";
                    }
                    $childdob1=explode('/',$childdob_date[$child]);
                    $childdob = $childdob1[2]."-".$childdob1[1]."-".$childdob1[0]."T00:00:00";
                    if($journytype=='3') //multicity api not show bagage and meal
                    {
                        $addultpassengerdetail[]=array(
                            'Title' =>$childtitle[$child],
                            'FirstName'=>$childfirstname[$child],
                            'LastName'=>$childlastname[$child],
                            'PaxType' =>'2',
                            'DateOfBirth'=>$childdob,
                            'Gender'=>$childgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=>$childPassportNo,
                            "PassportExpiry"=>  $childPassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> false,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$childfare_new,
                                "Tax"=> $childtaxfare_new,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $childtxnfeeofrd,
                                "AdditionalTxnFeePub"=> $childtxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],
                        );

                    }
                    else
                    {
                        $addultpassengerdetail[]=array(
                            'Title' =>$childtitle[$child],
                            'FirstName'=>$childfirstname[$child],
                            'LastName'=>$childlastname[$child],
                            'PaxType' =>'2',
                            'DateOfBirth'=>$childdob,
                            'Gender'=>$childgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=> $childPassportNo,
                            "PassportExpiry"=>  $childPassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> false,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$childfare_new,
                                "Tax"=> $childtaxfare_new,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $childtxnfeeofrd,
                                "AdditionalTxnFeePub"=> $childtxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],
                            "Baggage"=>[array(
                                "AirlineCode"=>$bagflightcode,
                                "FlightNumber"=>$bagflightnumber,
                                "WayType"=>$WayType,
                                "Code"=> $bagCode,
                                "Description"=>$bagDescription,
                                "Weight"=> $Weight,
                                "Currency"=> $Currency,
                                "Price"=>$Price,
                                "Origin"=> $Origin,
                                "Destination"=> $Destination,
                            )],
                            "Meal Dynamic"=>[array(
                                "AirlineCode"=>$mealflightcode,
                                "FlightNumber"=>$mealflightname,
                                "WayType"=>$mealway,
                                "Code"=> $Code,
                                "Description"=> $Description,
                                "AirlineDescription"=>$mealname,
                                "Quantity"=>$mealquantaty,
                                "Currency"=> $Currency,
                                "Price"=>$meal_orgprice,
                                "Origin"=> $Origin,
                                "Destination"=> $Destination,
                            )],


                        );
                    } //multicity if close

                }
                if($infacount !="0")
                {
                    $totalinfant=count($infacount);
                    $infantfare_new=$infantbasefare / $totalinfant;
                    $infanttax_new=$infanttaxfare / $totalinfant;
                }
                
                    for($infa=0;$infa<$infacount;$infa++)
                    {
                        if($infatitle[$infa]=='Mstr')
                        {
                            $infagender="1";
                        }
                        else
                        {
                            $infagender="2";
                        }
                          if($passportcheck==1)
                        {
                            $infatPassportNo=$$request->get(['infantpassportno'][$infa]);
                            $passportexp=explode('/',$$request->get(['infantpassportexp'][$infa]));
                            $infatPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                            

                        }
                        else
                        {
                            $infatPassportNo="";
                            $infatPassportExpiry="";
                        }
                        $infadob1=explode('/',$infadob_date[$infa]);
                        $infadob = $infadob1[2]."-".$infadob1[1]."-".$infadob1[0]."T00:00:00";
                        if($journytype=='3') //multicity api not show bagage and meal
                        {
                            $addultpassengerdetail[]=array(
                                'Title' =>$infatitle[$infa],
                                'FirstName'=>$infafistname [$infa],
                                'LastName'=>$infalastname[$infa],
                                'PaxType' =>'3',
                                'DateOfBirth'=>$infadob,
                                'Gender'=>$infagender,
                                "GSTCompanyAddress"=> $gstaddress,
                                "GSTCompanyContactNumber"=> $gstcontactno,
                                "GSTCompanyName"=> $gstcompname,
                                "GSTNumber"=> $gstnumber,
                                "GSTCompanyEmail"=> $gstemail,
                                "PassportNo"=> $infatPassportNo,
                                "PassportExpiry"=>  $infatPassportExpiry,
                                "AddressLine1"=> $AddressLine1,
                                "AddressLine2"=> "",
                                "City"=> $City,
                                "CountryCode"=> $CountryCode,
                                "CountryName"=> $CountryName,
                                "Nationality"=> $CountryCode,
                                "ContactNo"=> $ContactNo,
                                "Email"=> $Email,
                                "IsLeadPax"=> false,
                                "FFAirline"=> $ff_name,
                                "FFNumber"=> $ff_number,
                                "Fare"=>[array(
                                    "BaseFare"=>$infantfare_new,
                                    "Tax"=> $infanttax_new,
                                    "TransactionFee"=> "",
                                    "AdditionalTxnFeeOfrd"=> $infanttxnfeeofrd,
                                    "AdditionalTxnFeePub"=> $infanttxnfeepub,
                                    "AirTransFee"=> "0.0",
                                )],
                            );

                        }
                        else
                        {
                            $addultpassengerdetail[]=array(
                                'Title' =>$infatitle[$infa],
                                'FirstName'=>$infafistname [$infa],
                                'LastName'=>$infalastname[$infa],
                                'PaxType' =>'3',
                                'DateOfBirth'=>$infadob,
                                'Gender'=>$infagender,
                                "GSTCompanyAddress"=> $gstaddress,
                                "GSTCompanyContactNumber"=> $gstcontactno,
                                "GSTCompanyName"=> $gstcompname,
                                "GSTNumber"=> $gstnumber,
                                "GSTCompanyEmail"=> $gstemail,
                                "PassportNo"=>$infatPassportNo,
                                "PassportExpiry"=> $infatPassportExpiry,
                                "AddressLine1"=> $AddressLine1,
                                "AddressLine2"=> "",
                                "City"=> $City,
                                "CountryCode"=> $CountryCode,
                                "CountryName"=> $CountryName,
                                "Nationality"=> $CountryCode,
                                "ContactNo"=> $ContactNo,
                                "Email"=> $Email,
                                "IsLeadPax"=> false,
                                "FFAirline"=> $ff_name,
                                "FFNumber"=> $ff_number,
                                "Fare"=>[array(
                                    "BaseFare"=>$infantfare_new,
                                    "Tax"=> $infanttax_new,
                                    "TransactionFee"=> "",
                                    "AdditionalTxnFeeOfrd"=> $infanttxnfeeofrd,
                                    "AdditionalTxnFeePub"=> $infanttxnfeepub,
                                    "AirTransFee"=> "0.0",
                                )],

                                "Meal Dynamic"=>[array(
                                   "AirlineCode"=>$mealflightcode,
                                    "FlightNumber"=>$mealflightname,
                                    "WayType"=>$mealway,
                                    "Code"=> $Code,
                                    "Description"=> $Description,
                                    "AirlineDescription"=>$mealname,
                                    "Quantity"=>$mealquantaty,
                                    "Currency"=> $Currency,
                                    "Price"=>$meal_orgprice,
                                    "Origin"=> $Origin,
                                    "Destination"=> $Destination,
                                )],


                            );
                        } //multicity infa if close

                    }

                $form_data = array(
                    'EndUserIp'=>$EndUserIp,
                    'TokenId'=>$TokenId,
                    'TraceId'=>$TraceId,
                    'ResultIndex'=>$ResultIndex,
                    'Passengers'=>$addultpassengerdetail,
                );
                $data_string = json_encode($form_data);

                
                $ch = curl_init('https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Ticket/');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );

                $result = curl_exec($ch);
                $resultticket=json_decode($result,true);
              
                $errorcode = $resultticket['Response']['Error']['ErrorCode'];
                 $errormsg = $resultticket['Response']['Error']['ErrorMessage'];
                // $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode,'response_msg' => $errormsg]);
                 $paymentdb = tbl_payment_order::where('orderid', $orderid)->where('referenceid', $paymentid)->update(['reponse' => $errorcode,'response_msg' => $errormsg,'payment_mode'=>$paymenttype]);
                if($resultticket['Response']['Error']['ErrorCode']!='0')
                {

                    $message = $resultticket['Response']['Error']['ErrorMessage'];
                    
                    if($resultticket['Response']['Error']['ErrorCode']=='6' )
                    {
                        $newresult['error']=$errormsg;
                    }
                    else if($resultticket['Response']['Error']['ErrorCode']=='3' )
                    {
                        $newresult['error']=$errormsg;
                    }
                    else
                    {
                        $newresult['error']=$errormsg;
                    }
                    
                }
                else
                {
                   
                    $PNR=$resultticket['Response']['Response']['PNR'];
                    $BookingId=$resultticket['Response']['Response']['BookingId'];
                    $status=$resultticket['Response']['Response']['TicketStatus'];
                    $flightstatus='LCC';
                    $newresult['PNR']=$PNR;
                    $newresult['BookingId']=$BookingId;
                    $newresult['status']=$status;


                }
               
               
            }
             else
            {
                if(!empty($request->get(['meal'])))
                {
                    $meal=$formdata['meal'];
                    if($meal !='')
                    {
                        $meal1= explode('%',$meal);
                        $Codemeal=$meal1[0];
                        $Descriptionmeal=$meal1[1];
                    }
                }
                else
                {
                    $Codemeal="";
                    $Descriptionmeal="";
                }
                $totaladult=count($firstnameadult);
                $adultfare_new1=$adultbasefare / $totaladult;
                $adulttax_new1=$adulttaxfare / $totaladult;
                $YQTax_new1=$YQTax/$totaladult;
                //book method
                for($adult=0;$adult<count($firstnameadult);$adult++)
                {
                    if($titleadult[$adult]=='Mr')
                    {
                        $adultgender="1";
                    }
                    else
                    {
                        $adultgender="2";
                    }
                    if($adult=='0')
                    {
                        $paxval="true";
                    }
                    else
                    {
                        $paxval="false";
                    }
                    if($passportcheck==1)
                    {
                        $PassportNo=$request->get(['passportno'][$adult]);
                        $passportexp=explode('/',$request->get(['passportexp'][$adult]));
                        $PassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $PassportNo="";
                        $PassportExpiry="";
                    }
                    $adultdob1=explode('/',$adultdob_date[$adult]);
                    $adultdob = $adultdob1[2]."-".$adultdob1[1]."-".$adultdob1[0]."T00:00:00";

                    $addultpassengerdetaillcc[]=array(
                        'Title' =>$titleadult[$adult],
                        'FirstName'=>$firstnameadult[$adult],
                        'LastName'=>$lastnameadult[$adult],
                        'PaxType' =>'1',
                        'DateOfBirth'=>$adultdob,
                        'Gender'=>$adultgender,
                        "GSTCompanyAddress"=> $gstaddress,
                        "GSTCompanyContactNumber"=> $gstcontactno,
                        "GSTCompanyName"=> $gstcompname,
                        "GSTNumber"=> $gstnumber,
                        "GSTCompanyEmail"=> $gstemail,
                        "PassportNo"=> $PassportNo,
                        "PassportExpiry"=> $PassportExpiry,
                        "AddressLine1"=> $AddressLine1,
                        "AddressLine2"=> "",
                        "City"=> $City,
                        "CountryCode"=> $CountryCode,
                        "CountryName"=> $CountryName,
                        "Nationality"=> $CountryCode,
                        "ContactNo"=> $ContactNo,
                        "Email"=> $Email,
                        "IsLeadPax"=> $paxval,
                        "FFAirline"=> $ff_name,
                        "FFNumber"=> $ff_number,
                        "Fare"=>[array(
                            "BaseFare"=>$adultfare_new1,
                            "Tax"=> $adulttax_new1,
                            "YQTax"=> $YQTax_new1,
                            "AdditionalTxnFeeOfrd"=> $adulttxnfeeofrd,
                            "AdditionalTxnFeePub"=> $adulttxnfeepub,
                            "AirTransFee"=> "0.0",
                        )],




                    );
                }
                if(count($childfirstname) !="0")
                {
                    $totalchild=count($childfirstname);
                    $childfare_new1=$childbasefare / $totalchild;
                    $childtax_new1=$childtaxfare / $totalchild;
                    $YQTax_new12=$YQTax/$totalchild;
                }
                
                for($child=0;$child<count($childfirstname);$child++)
                {
                    if($childtitle[$child]=='Mr')
                    {
                        $childgender="1";
                    }
                    else
                    {
                        $childgender="2";
                    }
                     if($passportcheck==1)
                    {
                        $childPassportNo=$$request->get(['childpassportno'][$child]);
                        $passportexp=explode('/',$$request->get(['childpassportexp'][$child]));
                        $childPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $childPassportNo="";
                        $childPassportExpiry="";
                    }
                    $childdob1=explode('/',$childdob_date[$child]);
                    $childdob = $childdob1[2]."-".$childdob1[1]."-".$childdob1[0]."T00:00:00";
                    $addultpassengerdetaillcc[]=array(
                        'Title' =>$childtitle[$child],
                        'FirstName'=>$childfirstname[$child],
                        'LastName'=>$childlastname[$child],
                        'PaxType' =>'2',
                        'DateOfBirth'=>$childdob,
                        'Gender'=>$childgender,
                        "GSTCompanyAddress"=> $gstaddress,
                        "GSTCompanyContactNumber"=> $gstcontactno,
                        "GSTCompanyName"=> $gstcompname,
                        "GSTNumber"=> $gstnumber,
                        "GSTCompanyEmail"=> $gstemail,
                         "PassportNo"=> $childPassportNo,
                        "PassportExpiry"=> $childPassportExpiry,
                        "AddressLine1"=> $AddressLine1,
                        "AddressLine2"=> "",
                        "City"=> $City,
                        "CountryCode"=> $CountryCode,
                        "CountryName"=> $CountryName,
                        "Nationality"=> $CountryCode,
                        "ContactNo"=> $ContactNo,

                        "Email"=> $Email,
                        "IsLeadPax"=> false,
                        "FFAirline"=> $ff_name,
                        "FFNumber"=> $ff_number,
                        "Fare"=>[array(
                            "BaseFare"=>$childfare_new1,
                            "Tax"=> $childtax_new1,
                            "YQTax"=> $YQTax_new12,
                            "AdditionalTxnFeeOfrd"=> $childtxnfeeofrd,
                            "AdditionalTxnFeePub"=> $childtxnfeepub,
                            "AirTransFee"=> "0.0",
                        )],


                    );
                } //childforloopclose
                if($infacount !="0")
                {
                    $totalinfant=count($infacount);
                    $infantfare_new1=$infantbasefare / $infacount;
                    $infanttax_new1=$infanttaxfare / $infacount;
                    $YQTax_new11=$YQTax/$totalinfant;
                }
                
                for($infa=0;$infa<$infacount;$infa++)
                {
                    if($infatitle[$infa]=='Mstr')
                    {
                        $infagender="1";
                    }
                    else
                    {
                        $infagender="2";
                    }
                      if($passportcheck==1)
                    {
                        $infatPassportNo=$$request->get(['infantpassportno'][$infa]);
                        $passportexp=explode('/',$$request->get(['infantpassportexp'][$infa]));
                        $infatPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $infatPassportNo="";
                        $infatPassportExpiry="";
                    }
                    $infadob1=explode('/',$infadob_date[$infa]);
                    $infadob = $infadob1[2]."-".$infadob1[1]."-".$infadob1[0]."T00:00:00";
                    $addultpassengerdetaillcc[]=array(
                        'Title' =>$infatitle[$infa],
                        'FirstName'=>$infafistname [$infa],
                        'LastName'=>$infalastname[$infa],
                        'PaxType' =>'3',
                        'DateOfBirth'=>$infadob,
                        'Gender'=>$infagender,
                        "GSTCompanyAddress"=> $gstaddress,
                        "GSTCompanyContactNumber"=> $gstcontactno,
                        "GSTCompanyName"=> $gstcompname,
                        "GSTNumber"=> $gstnumber,
                        "GSTCompanyEmail"=> $gstemail,
                        "PassportNo"=> $infatPassportNo,
                        "PassportExpiry"=> $infatPassportExpiry,
                        "AddressLine1"=> $AddressLine1,
                        "AddressLine2"=> "",
                        "City"=> $City,
                        "CountryCode"=> $CountryCode,
                        "CountryName"=> $CountryName,
                        "Nationality"=> $CountryCode,
                        "ContactNo"=> $ContactNo,
                        "Email"=> $Email,
                        "IsLeadPax"=> false,
                        "FFAirline"=> $ff_name,
                        "FFNumber"=> $ff_number,

                        "Fare"=>[array(
                            "BaseFare"=>$infantfare_new1,
                            "Tax"=> $infanttax_new1,
                            "YQTax"=> $YQTax_new11,
                            "AdditionalTxnFeeOfrd"=> $infanttxnfeeofrd,
                            "AdditionalTxnFeePub"=> $infanttxnfeepub,
                            "AirTransFee"=> "0.0",
                        )],


                    );
                } //infaforloopclose
                $form_data = array(
                    'EndUserIp'=>$EndUserIp,
                    'TokenId'=>$TokenId,
                    'TraceId'=>$TraceId,
                    'ResultIndex'=>$ResultIndex,
                    'Passengers'=>$addultpassengerdetaillcc,
                );
                $data_string_lcc = json_encode($form_data);
                
                $ch = curl_init('https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Book/');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_lcc);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string_lcc))
                );

                $result = curl_exec($ch);
                
                $result1=json_decode($result,true);
                $errorcode1 = $result1['Response']['Error']['ErrorCode'];
                $errormsg1 = $result1['Response']['Error']['ErrorMessage'];
                // $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1]);
                $paymentdb = tbl_payment_order::where('orderid', $orderid)->where('referenceid', $paymentid)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1,'payment_mode'=>$paymenttype]);
                if($result1['Response']['Error']['ErrorCode']!='0')
                {

                    $message = $result1['Response']['Error']['ErrorMessage'];
                    
                    if($result1['Response']['Error']['ErrorCode']=='6')
                    {
                        $result['error']=$message;
                        
                    }
                    else if($result1['Response']['Error']['ErrorCode']=='3')
                    {
                         $result['error']=$message;
                    }
                    else
                    {
                         $result['error']=$message;
                    }
                    die();
                }
                else
                {

                    $PNR=$result1['Response']['Response']['PNR'];
                    $BookingId=$result1['Response']['Response']['BookingId'];
                    $status=$result1['Response']['Response']['FlightItinerary']['Status'];
                    $flightstatus='Non Lcc';
                     $newresult['PNR']=$PNR;
                    $newresult['BookingId']=$BookingId;
                    $newresult['status']=$status;
                    // echo $paxid = $result1['Response']['FlightItinerary']['Passenger'][0]['PaxId'];
                    //cheackagain
                    //  for($adult=0;$adult<count($firstnameadult);$adult++)
                    //          {
                    //              if($titleadult[$adult]=='1')
                    //      {
                    //          $adultgender="1";
                    //      }
                    //      else
                    //      {
                    //          $adultgender="2";
                    //      }
                    //      $adultdob1=explode('/',$adultdob_date[$adult]);
                    //              $adultdob = $adultdob1[2]."-".$adultdob1[1]."-".$adultdob1[0]."T00:00:00";

                    //              $addultpassengerdetaillcc1[]=array(

                    //                                              'PaxType' =>$paxid,
                    //                                              'DateOfBirth'=>$adultdob,
                    //                                              "PassportNo"=> $PassportNo,
                    //                                  "PassportExpiry"=> $PassportExpiry,
                    //                                );
                    // }
                    //again ticket call is non lcc flight
                    $form_data_ticket = array(
                        'EndUserIp'=>$EndUserIp,
                        'TokenId'=>$TokenId,
                        'TraceId'=>$TraceId,
                        'PNR'=>$PNR,
                        'BookingId'=>$BookingId,

                    );
                    $data_string_ticket = json_encode($form_data_ticket);

                    $ch = curl_init('https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Ticket/');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_ticket);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($data_string_ticket))
                    );

                    $result_new = curl_exec($ch);
                    $resultnew['ticketarray']=$result_new;
                    $resultticket_new=json_decode($result_new,true);


                    // echo "<hr>";

                    $errorcode1 = $resultticket_new['Response']['Error']['ErrorCode'];
                    $errormsg1 = $resultticket_new['Response']['Error']['ErrorMessage'];
                    // $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1]);
                    $paymentdb = tbl_payment_order::where('orderid', $orderid)->where('referenceid', $paymentid)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1,'payment_mode'=>$paymenttype]);
                    if($result1['Response']['Error']['ErrorCode']!='0')
                    {
                         $result['error']=$errormsg1;
                    }
                    // end call non lcc flight

                }
            } //if loop close

             echo json_encode($newresult);

    }
     public function newapiflight_bookview(Request $request)
    {
        $newdata=array();
        $status="";
        $paymentorder="";
        $paymentref="";
        $user_id="";
        $username="";
        $userphone="";
        $useremail="";
       $TraceId = $request->get('flighttraceid');
       
       $TokenId=$request->get('TokenId');
      
        $EndUserIp=$request->get('localip');
       
       $newdata=json_decode($request->get('journytype'));

      $segmentdata= $newdata->Segments[0];
   
      
    $airlinecode=$request->get('allflights')[0]['Segments'][0][0]['Airline']['AirlineCode'];
    $airlinename=$request->get('allflights')[0]['Segments'][0][0]['Airline']['AirlineName'];
    $flightno=$request->get('allflights')[0]['Segments'][0][0]['Airline']['FlightNumber'];
        $journytype= $newdata->JourneyType;
    
      $listdata=$request->get('listdata');
     
        //  $titleadult=  explode(",",$request->get('titleadult'));
      
        // $firstnameadult=  explode(",",$request->get('firstnameadult'));
        // $lastnameadult=  explode(",",$request->get('lastnameadult'));
        // $adultdob_date=  explode(",",$request->get('adultdob'));
        //emergency
        $eme_name=$request->get('eme_name');
        $ema_phone=$request->get('ema_phone');
        $eme_relation=$request->get('eme_relation');
        $posts = tbl_payment_order::orderBy('id', 'DESC')->first();
        if(count($posts) == 0)
        {
            $orderid="TRAstt-01";
        }
        else
        {
            $chkid =  $posts->id + 1;
            $orderid="TRAstt-0".$chkid;
        }
        $paymentid=$request->get('paymentid');
        $paymenttype=$request->get('paymenttype');
        $ff_name=""; 
        $ff_number1="";
        if($ff_number1=="")
        {
            $ff_name="";
            $ff_number="";
        }
        else
        {
            $ff_name="";
            $ff_number="";
        }
        $ff_number_return="";
        $ff_name_return="";
        $passportcheck=$request->get('passportcheck');
        $flightcabinclass=$segmentdata->FlightCabinClass;
        // $childfirstname=array();
        // $childcount=0;
       
         
        // if(!empty($request->get('childtitle')))
        // {
           
        //     $childtitle=explode(",",$request->get('childtitle'));
        //     $childfirstname=  explode(",",$request->get('childfirstname'));
        //     $childlastname=explode(",",$request->get('childlastname'));
        //     $childdob_date= explode(",",$request->get('childdob'));
        //     $childbasefare=explode(",",$request->get('childbasefare'));
        //     $childtaxfare= explode(",",$request->get('childtaxfare'));
        //     $childtxnfeepub=  explode(",",$request->get('childtxnfeepub'));
        //     $childtxnfeeofrd= explode(",",$request->get('childtxnfeeofrd'));
        //     $childcount=count($childfirstname);
        // }
        
        // $infacount=0;
      
        //     if(!empty($request->get('infatitle')))
        //     {
        //         $infatitle= $infatitle;
        //         $infafistname= explode(",",$request->get('infafistname'));
        //         $infalastname= explode(",",$request->get('infalastname'));
        //         $infadob_date= explode(",",$request->get('infadob'));
        //         $infantbasefare=explode(",",$request->get('infantbasefare'));
        //         $infanttaxfare= explode(",",$request->get('infanttaxfare'));
        //         $infanttxnfeepub= explode(",",$request->get('infanttxnfeepub'));
        //         $infanttxnfeeofrd= explode(",",$request->get('infanttxnfeeofrd'));
        //         $infacount=count($infafistname);
        //     }
            
            $City="Amritsar";
            $grandprice=$request->get('totalcharge');
            $refund="";
      
                $AddressLine1="Ranjit Avenue Amritsar";
                $mealflightcode=$airlinecode;
                    $mealflightname=$flightno;
               $Origin=$segmentdata->Origin;
                $Destination=$segmentdata->Destination;
                if(!empty($request->get('meals')[0]['WayType']))
                {
                    $mealway=$request->get('meals')[0]['WayType'];
                    $Code=$request->get('meals')[0]['Code'];
                    $mealquantaty=$request->get('meals')[0]['Quantity'];
                    $Currency=$request->get('meals')[0]['Currency'];
                    $meal_orgprice=$request->get('meals')[0]['mealmainprice'];
                    
                     $mealname=  $request->get('meals')[0]['AirlineDescription'];
                    $Description=$request->get('meals')[0]['Description'];
                    
                }
                else
                {
                    $mealway="2";
                    $Code="No Meal";
                    $mealquantaty="0";
                    $meal_orgprice="0";
                  
                    $mealname="";
                    $Description="2";
                    $mealname="";
                    $Description="2";
                   
                }
              $bagflightcode=$airlinecode;
                $bagflightnumber=$flightno;  
              
            if(!empty($request->get('bags')[0]['WayType']))
            {
                
                $WayType=$request->get('bags')[0]['WayType'];
                $bagCode=$request->get('bags')[0]['Code'];
                $bagDescription=$request->get('bags')[0]['Description'];
                $Weight=$request->get('bags')[0]['Weight'];
                $Currency=$request->get('bags')[0]['Currency'];
                $Price=$request->get('bags')[0]['Price'];
               
            }
            else
            {
               
                $WayType="2";
                $bagCode="No Baggage";
                $bagDescription="2";
                $Weight="0";
                $Price="0";
                

            }

            
           
            $Code_return="No Meal";
            $Description_return="2";
            $mealname_return="";
            $mealquantaty_return="0";
         
            $meal_orgprice_return="0";
         
            $WayType_return="2";
            $bagCode_return="No Baggage";
            $bagDescription_return="2";
            $Weight_return="0";
           
            $Weight_return="0";
            $price_return="0";
           
            $mealway_return="2";
            if(!empty($request->get('allflights')[1]['ResultIndex']))
            {
                $segmentdatareturn1= $newdata->Segments[1];
                $Origin_return=$segmentdatareturn1->Origin;
                $Destination_return=$segmentdatareturn1->Destination;

                    $mealflightcode_return=$request->get('allflights')[1]['Segments'][0][0]['Airline']['AirlineCode'];
                    $mealflightname_return=$request->get('allflights')[1]['Segments'][0][0]['Airline']['FlightNumber'];
                    $bagflightnumber_return=$request->get('allflights')[1]['Segments'][0][0]['Airline']['AirlineCode'];
                    $bagflightcode_return=$request->get('allflights')[1]['Segments'][0][0]['Airline']['FlightNumber'];
                    if(!empty($request->get('meals')[1]['WayType']))
                {
                    
                    $mealway_return=$request->get('meals')[1]['WayType'];
                    $Code_return=$request->get('meals')[1]['Code'];
                    $Description_return=$request->get('meals')[1]['Description'];
                    $mealname_return=$request->get('meals')[1]['AirlineDescription'];
                    $mealquantaty_return=$request->get('meals')[1]['Quantity'];
                    $Currency=$request->get('meals')[1]['Currency'];
                    $meal_orgprice_return=$request->get('meals')[1]['mealmainprice'];
                    

                }
                if(!empty($request->get('bags')[1]['WayType']))
                {
                    
                    $WayType_return=$request->get('bags')[1]['WayType'];
                    $bagCode_return=$request->get('bags')[1]['Code'];
                    $bagDescription_return=$request->get('bags')[1]['Description'];
                    $Weight_return=$request->get('bags')[1]['Weight'];
                  
                    $price_return=$request->get('bags')[1]['Price'];
                    $Origin_return=$request->get('bags')[1]['Origin'];
                    $Destination_return=$request->get('bags')[1]['Destination'];
                    
                }
            }

            

                if($request->get('allflights')[0]['FareBreakdown'][0]['PassengerType']=='1')
                {
                    $passcount=$request->get('allflights')[0]['FareBreakdown'][0]['PassengerCount'];
                    $adbaseprice=$request->get('allflights')[0]['FareBreakdown'][0]['BaseFare'];
                    $adultbasefare_new=$adbaseprice / $passcount;
                    $adulttaxfare_new=$request->get('allflights')[0]['FareBreakdown'][0]['Tax'] / $passcount;
                    $adulttxnfeeofrd1=$request->get('allflights')[0]['FareBreakdown'][0]['AdditionalTxnFeeOfrd'] / $passcount;
                    $adulttxnfeeofrd=  number_format($adulttxnfeeofrd1,1);
                    $adulttxnfeepub1=$request->get('allflights')[0]['FareBreakdown'][0]['AdditionalTxnFeePub'] / $passcount;
                    $adulttxnfeepub=  number_format($adulttxnfeepub1,1);
                }
               
               
                $childfare_new="";
                $childtaxfare_new="";
                $childtxnfeeofrd="";
                $childtxnfeepub="";
                $infantfare_new="";
                $infanttax_new="";
                $infanttxnfeeofrd="";
                $infanttxnfeepub="";
                $resultindex_return="";
                if(!empty($request->get('allflights')[0]['FareBreakdown'][1]))
                {
                  

                    if($request->get('allflights')[0]['FareBreakdown'][1]['PassengerType']=='2')
                    {
                        
                        $childcount=$request->get('allflights')[0]['FareBreakdown'][1]['PassengerCount'];
                        $childbas=$request->get('allflights')[0]['FareBreakdown'][1]['BaseFare'];
                        $childfare_new=$childbas / $childcount;
                        $childtaxfare_new=$request->get('allflights')[0]['FareBreakdown'][1]['Tax'] / $childcount;
                        $childtxnfeeofrd=$request->get('allflights')[0]['FareBreakdown'][1]['AdditionalTxnFeeOfrd'] / $childcount;
                        $childtxnfeepub1=$request->get('allflights')[0]['FareBreakdown'][1]['AdditionalTxnFeePub'] / $childcount;
                        $childtxnfeepub= number_format($childtxnfeepub1,1);
                    }
                    
                     if($request->get('allflights')[0]['FareBreakdown'][2]['PassengerType']=='3')
                    {
                        $infantcount=$request->get('allflights')[0]['FareBreakdown'][2]['PassengerCount'];
                        $childbas=$request->get('allflights')[0]['FareBreakdown'][2]['BaseFare'];
                        $infantfare_new=$childbas / $infantcount;
                        $infanttax_new=$request->get('allflights')[0]['FareBreakdown'][2]['Tax'] / $infantcount;
                        $infanttxnfeeofrd=$request->get('allflights')[0]['FareBreakdown'][2]['AdditionalTxnFeeOfrd'] / $infantcount;
                        $infanttxnfeepub1=$request->get('allflights')[0]['FareBreakdown'][2]['AdditionalTxnFeePub'] / $infantcount;
                        $childtxnfeepub= number_format($infanttxnfeepub1,1);
                    }
                }
       
                

        //     $flightlastprice = $request->get('flightbaseprice');
        //     $flightmarginprice =$request->get('flightbasemarginprice');
        //     $flighttaxprice=$request->get('flighttaxprice');
        //     $flightotherprice=$request->get('flightotherprice');
        //     $convertcurrecny= $request->get('convertcurrecny');
        //     $currencyicon=$request->get('flightcurrencyicon');
        //     //return
        //     $mealname_return= $request->get('totalmealname_return');
        //     $mealprice_return= $request->get('totalmealprice_return');
        //     $meal_orgprice_return= $request->get('totalorgmealprice_return');

        //     $flightlastprice_return = $request->get('flightbaseprice_r');
        //     $flightmarginprice_return =$request->get('flightbasemarginprice_r');
        //     $flighttaxprice_return=$request->get('flighttaxprice_r');
        //     $flightotherprice_return= $request->get('flightotherprice_r');

        //     $bag_weight=$request->get('bagweight');
        //     $bag_price=$request->get('newbagprice');
        //     $bag_orgprice=$request->get('orgbagprice');
        //     //return
        //     $bag_weight_return=$request->get('bagweight_return');
        //     $bag_price_return=$request->get('newbagprice_return');
        //     $bag_orgprice_return=$request->get('orgbagprice_return');
        //     $grandprice= $request->get('maingradflightprice');
       
            $Email= $request->get('email');
            $CountryCode=$request->get('maincurrency');
            $CountryName=$request->get('country');
            $ContactNo= $request->get('phone');
            if(!empty($request->get('gstaddress')))
            {
                 $gstaddress=  $request->get('gstaddress');
                $gstemail=  $request->get('gstemail');
                $gstnumber=  $request->get('gstnumber');
                $gstcompname=  $request->get('gstcompname');
                $gstcontactno=  $request->get('gstcontactno');
            }
            else
            {
                $gstaddress="177 E VIJAY NAGAR, AMRITSAR BATALA ROAD,Amritsar 1 - Ward No.6 Punjab";
                $gstemail="info@travoweb.com";
                $gstnumber="03BDHPK8214M1ZG";
                $gstcompname="SEKAP TRAVEL";
                $gstcontactno="61490149833";
            }
           

            $lcccheck=$request->get('allflights')[0]['IsLCC'];
 
            $ResultIndex=$request->get('allflights')[0]['ResultIndex'];
            
            $Currency=$request->get('allflights')[0]['Fare']['Currency'];
           
            $YQTax=number_format( $request->get('allflights')[0]['Fare']['YQTax'],1);
            $OtherCharges=number_format($request->get('allflights')[0]['Fare']['OtherCharges'],1); 
            $Discount=number_format($request->get('allflights')[0]['Fare']['Discount'],1); 
            $PublishedFare=number_format($request->get('allflights')[0]['Fare']['PublishedFare'],1); 
           
            $OfferedFare=number_format($request->get('allflights')[0]['Fare']['OfferedFare'],1); 
            $TdsOnCommission=number_format($request->get('allflights')[0]['Fare']['TdsOnCommission'] ,1); 
            $TdsOnPLB=number_format($request->get('allflights')[0]['Fare']['TdsOnPLB'],1); 
            $TdsOnIncentive=number_format($request->get('allflights')[0]['Fare']['TdsOnIncentive'] ,1);
       
            $ServiceFee=number_format($request->get('allflights')[0]['Fare']['ServiceFee'],1);
         
             if(!empty($request->get('allflights')[1]))
            {
                $segmentdatareturn= $newdata->Segments[1];
                 $lcccheck_return= $request->get('allflights')[1]['IsLCC'];
                 $resultindex_return=$request->get('allflights')[1]['ResultIndex'];
                 $Currency_return= $request->get('allflights')[1]['Fare']['Currency'];
                 $YQTax_return=  number_format($request->get('allflights')[1]['Fare']['YQTax'],1);
                $OtherCharges_return=  number_format($request->get('allflights')[1]['Fare']['OtherCharges'],1);
                $Discount_return= number_format($request->get('allflights')[1]['Fare']['Discount'],1);
                $PublishedFare_return= number_format($request->get('allflights')[1]['Fare']['PublishedFare'],1);
                $OfferedFare_return= number_format($request->get('allflights')[1]['Fare']['OfferedFare'],1);
                $TdsOnCommission_return=   number_format($request->get('allflights')[1]['Fare']['TdsOnCommission'],1);
                $TdsOnPLB_return= number_format($request->get('allflights')[1]['Fare']['TdsOnPLB'],1);
                $TdsOnIncentive_return= number_format($request->get('allflights')[1]['Fare']['TdsOnIncentive'],1);
                $ServiceFee_return=  number_format($request->get('allflights')[1]['Fare']['ServiceFee'],1);
 

                 if($request->get('allflights')[1]['FareBreakdown'][0]['PassengerType']=='1')
                {
                    $passcount=$request->get('allflights')[1]['FareBreakdown'][0]['PassengerCount'];
                    $adbaseprice=$request->get('allflights')[1]['FareBreakdown'][0]['BaseFare'];
                    $adultbasefarereturn=$adbaseprice / $passcount;
                    $adulttaxfarereturn=$request->get('allflights')[1]['FareBreakdown'][0]['Tax'] / $passcount;
                    $adulttxnfeeofrdreturn=$request->get('allflights')[1]['FareBreakdown'][0]['AdditionalTxnFeeOfrd'] / $passcount;
                    $adulttxnfeepubreturn1=$request->get('allflights')[1]['FareBreakdown'][0]['AdditionalTxnFeePub'] / $passcount;
                    $adulttxnfeepubreturn= number_format($adulttxnfeepubreturn1,1);
                }
                $childbasefarereturn="";
                $childtaxfarereturn="";
                $childtxnfeeofrdreturn="";
                $childtxnfeepubreturn="";
                $infantbasefarereturn="";
                $infanttaxfarereturn="";
                $infanttxnfeeofrdreturn="";
                $infanttxnfeepubreturn="";
                if(!empty($request->get('allflights')[1]['FareBreakdown'][1]))
                {

                    if($request->get('allflights')[1]['FareBreakdown'][1]['PassengerType']=='2')
                    {
                        $childcount=$request->get('allflights')[1]['FareBreakdown'][1]['PassengerCount'];
                        $childbas=$request->get('allflights')[1]['FareBreakdown'][1]['BaseFare'];
                        $childbasefarereturn=$childbas / $childcount;
                        $childtaxfarereturn=$request->get('allflights')[1]['FareBreakdown'][1]['Tax'] / $childcount;
                        $childtxnfeeofrdreturn=$request->get('allflights')[1]['FareBreakdown'][1]['AdditionalTxnFeeOfrd'] / $childcount;
                        $childtxnfeepubreturn1=$request->get('allflights')[1]['FareBreakdown'][1]['AdditionalTxnFeePub'] / $childcount;
                        $childtxnfeepubreturn= number_format($childtxnfeepubreturn1,1);
                    }
                     if($request->get('allflights')[1]['FareBreakdown'][2]['PassengerType']=='3')
                    {
                        $infantcount=$request->get('allflights')[1]['FareBreakdown'][2]['PassengerCount'];
                        $infantbs=$request->get('allflights')[1]['FareBreakdown'][2]['BaseFare'];
                        $infantbasefarereturn=$infantbs / $infantcount;
                        $infanttaxfarereturn=$request->get('allflights')[1]['FareBreakdown'][2]['Tax'] / $infantcount;
                        $infanttxnfeeofrdreturn=$request->get('allflights')[1]['FareBreakdown'][2]['AdditionalTxnFeeOfrd'] / $infantcount;
                        $infanttxnfeepubreturn1=$request->get('allflights')[1]['FareBreakdown'][2]['AdditionalTxnFeePub'] / $infantcount;
                        $infanttxnfeepubreturn= number_format($infanttxnfeepubreturn1,1);
                    }
                }

            }
        
            $usercustname =$listdata[0]['fname'].' '.$listdata[0]['lname'];
        $adultcount=0;
        $childcount=0;  
        if($lcccheck=='1')
        {
            for($mk=0;$mk<count($listdata);$mk++)
              {
               
               if($listdata[$mk]['types']=='Adult')
               {
                    
                    $newgender=explode(".",$listdata[$mk]['gender']);

                    $adultcount++;
                    if($newgender[0]=='Mr')
                    {
                        $adultgender="1";
                    }
                    else
                    {
                        $adultgender="2";
                    }
                    if(count($listdata[$mk]['types'])=='0')
                    {
                        $paxval="true";
                       
                    }
                    else
                    {
                        $paxval="false";
                       
                    }
                     if($passportcheck==1)
                    {
                        $PassportNo=$request->get(['passportno'][$mk]);
                        $passportexp=explode('/',$$request->get(['passportexp'][$mk]));
                        $PassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $PassportNo="";
                        $PassportExpiry="";
                    }
                    $adultdob1=explode('/',$listdata[$mk]['age']);
                    $adultdob = $adultdob1[2]."-".$adultdob1[1]."-".$adultdob1[0]."T00:00:00";
                    if($journytype=='3') //multicity api not show bagage and meal
                    {
                        $addultpassengerdetail[]=array(
                            'Title' =>$newgender[0],
                            'FirstName'=> $listdata[$mk]['fname'],
                            'LastName'=>$listdata[$mk]['lname'],
                            'PaxType' =>'1',
                            'DateOfBirth'=>$adultdob,
                            "PassportNo"=> $PassportNo,
                            "PassportExpiry"=> $PassportExpiry,
                            'Gender'=>$adultgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,

                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> $paxval,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$adultbasefare_new,
                                "Tax"=> $adulttaxfare_new,
                                "TransactionFee"=> "0.0",
                                "AdditionalTxnFeeOfrd"=> $adulttxnfeeofrd,
                                "AdditionalTxnFeePub"=> $adulttxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],
                        );
                    }
                    else
                    {
                        $addultpassengerdetail[]=array(
                               'Title' =>$newgender[0],
                            'FirstName'=> $listdata[$mk]['fname'],
                            'LastName'=>$listdata[$mk]['lname'],
                            'PaxType' =>'1',
                            'DateOfBirth'=>$adultdob,
                            "PassportNo"=> $PassportNo,
                            "PassportExpiry"=> $PassportExpiry,
                            'Gender'=>$adultgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,

                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> $paxval,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$adultbasefare_new,
                                "Tax"=> $adulttaxfare_new,
                                "TransactionFee"=> "0.0",
                                "AdditionalTxnFeeOfrd"=> $adulttxnfeeofrd,
                                "AdditionalTxnFeePub"=> $adulttxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],
                            "Baggage"=>[array(
                                                     "AirlineCode"=>$bagflightcode,
                                                    "FlightNumber"=>$bagflightnumber,
                                                    "WayType"=>$WayType,
                                                    "Code"=> $bagCode,
                                                    "Description"=>$bagDescription,
                                                    "Weight"=> $Weight,
                                                    "Currency"=> $Currency,
                                                    "Price"=>$Price,
                                                    "Origin"=> $Origin,
                                                    "Destination"=> $Destination,
                            )],
                              "Meal Dynamic"=>[array(
                                "AirlineCode"=>$mealflightcode,
                                "FlightNumber"=>$mealflightname,
                                "WayType"=>$mealway,
                                "Code"=> $Code,
                                "Description"=> $Description,
                                "AirlineDescription"=>$mealname,
                                "Quantity"=>$mealquantaty,
                                "Currency"=> $Currency,
                                "Price"=>$meal_orgprice,
                                "Origin"=> $Origin,
                                "Destination"=> $Destination,
                            )],
                          


                        );
                    } // else condtion journy time close

               } //adult if
               
                //child if start
               if($listdata[$mk]['types']=='Children')
               {
                    $childcount++;
                 $newgenderc=explode(".",$listdata[$mk]['gender']);
                     if($newgenderc[0]=='Mr')
                    {
                        $childgender="1";
                    }
                    else
                    {
                        $childgender="2";
                    }
                    if(count($listdata[$mk]['types'])=='0')
                    {
                        $paxval="true";
                       
                    }
                    else
                    {
                        $paxval="false";
                       
                    }
                    if($passportcheck==1)
                    {
                        $childPassportNo=$request->get(['passportno'][$mk]);
                        $passportexp=explode('/',$$request->get(['passportexp'][$mk]));
                        $childPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $childPassportNo="";
                        $childPassportExpiry="";
                    }
                    $childdob1=explode('/',$listdata[$mk]['age']);
                    $childdob = $childdob1[2]."-".$childdob1[1]."-".$childdob1[0]."T00:00:00";
                    if($journytype=='3') //multicity api not show bagage and meal
                    {
                        $addultpassengerdetail[]=array(
                             'Title' =>$newgenderc[0],
                            'FirstName'=> $listdata[$mk]['fname'],
                            'LastName'=>$listdata[$mk]['lname'],
                            'PaxType' =>'2',
                            'DateOfBirth'=>$childdob,
                            'Gender'=>$childgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=>$childPassportNo,
                            "PassportExpiry"=>  $childPassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> false,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$childfare_new,
                                "Tax"=> $childtaxfare_new,
                                "TransactionFee"=> "0.0",
                                "AdditionalTxnFeeOfrd"=> $childtxnfeeofrd,
                                "AdditionalTxnFeePub"=> $childtxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],
                        );

                    }
                    else
                    {
                        $addultpassengerdetail[]=array(
                            'Title' =>$newgenderc[0],
                            'FirstName'=> $listdata[$mk]['fname'],
                            'LastName'=>$listdata[$mk]['lname'],
                            'PaxType' =>'2',
                            'DateOfBirth'=>$childdob,
                            'Gender'=>$childgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=> $childPassportNo,
                            "PassportExpiry"=>  $childPassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> false,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$childfare_new,
                                "Tax"=> $childtaxfare_new,
                                "TransactionFee"=> "0.0",
                                "AdditionalTxnFeeOfrd"=> $childtxnfeeofrd,
                                "AdditionalTxnFeePub"=> $childtxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],
                            "Baggage"=>[array(
                                "AirlineCode"=>$bagflightcode,
                                "FlightNumber"=>$bagflightnumber,
                                "WayType"=>$WayType,
                                "Code"=> $bagCode,
                                "Description"=>$bagDescription,
                                "Weight"=> $Weight,
                                "Currency"=> $Currency,
                                "Price"=>$Price,
                                "Origin"=> $Origin,
                                "Destination"=> $Destination,
                            )],
                            "Meal Dynamic"=>[array(
                                "AirlineCode"=>$mealflightcode,
                                "FlightNumber"=>$mealflightname,
                                "WayType"=>$mealway,
                                "Code"=> $Code,
                                "Description"=> $Description,
                                "AirlineDescription"=>$mealname,
                                "Quantity"=>$mealquantaty,
                                "Currency"=> $Currency,
                                "Price"=>$meal_orgprice,
                                "Origin"=> $Origin,
                                "Destination"=> $Destination,
                            )],


                        );
                    } //multicity if close
               } //child if end
               // child if end
               if($listdata[$mk]['types']=='Infants')
               {
                    $newgenderi=explode(".",$listdata[$mk]['gender']);
                      if($newgenderi[0]=='Mr')
                    {
                        $infagender="1";
                    }
                    else
                    {
                        $infagender="2";
                    }
                    if(count($listdata[$mk]['types'])=='0')
                    {
                        $paxval="true";
                       
                    }
                    else
                    {
                        $paxval="false";
                       
                    }
                     if($passportcheck==1)
                    {
                        $infatPassportNo=$request->get(['passportno'][$mk]);
                        $passportexp=explode('/',$$request->get(['passportexp'][$mk]));
                        $infatPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $infatPassportNo="";
                        $infatPassportExpiry="";
                    }
                    $infadob1=explode('/',$listdata[$mk]['age']);
                    $infadob = $infadob1[2]."-".$infadob1[1]."-".$infadob1[0]."T00:00:00";
                     if($journytype=='3') //multicity api not show bagage and meal
                        {
                            $addultpassengerdetail[]=array(
                                'Title' =>$newgenderi[0],
                                'FirstName'=> $listdata[$mk]['fname'],
                                'LastName'=>$listdata[$mk]['lname'],
                                'PaxType' =>'3',
                                'DateOfBirth'=>$infadob,
                                'Gender'=>$infagender,
                                "GSTCompanyAddress"=> $gstaddress,
                                "GSTCompanyContactNumber"=> $gstcontactno,
                                "GSTCompanyName"=> $gstcompname,
                                "GSTNumber"=> $gstnumber,
                                "GSTCompanyEmail"=> $gstemail,
                                "PassportNo"=> $infatPassportNo,
                                "PassportExpiry"=>  $infatPassportExpiry,
                                "AddressLine1"=> $AddressLine1,
                                "AddressLine2"=> "",
                                "City"=> $City,
                                "CountryCode"=> $CountryCode,
                                "CountryName"=> $CountryName,
                                "Nationality"=> $CountryCode,
                                "ContactNo"=> $ContactNo,
                                "Email"=> $Email,
                                "IsLeadPax"=> false,
                                "FFAirline"=> $ff_name,
                                "FFNumber"=> $ff_number,
                                "Fare"=>[array(
                                    "BaseFare"=>$infantfare_new,
                                    "Tax"=> $infanttax_new,
                                    "TransactionFee"=> "",
                                    "AdditionalTxnFeeOfrd"=> $infanttxnfeeofrd,
                                    "AdditionalTxnFeePub"=> $infanttxnfeepub,
                                    "AirTransFee"=> "0.0",
                                )],
                            );

                        }
                        else
                        {
                            $addultpassengerdetail[]=array(
                               'Title' =>$newgenderi[0],
                                'FirstName'=> $listdata[$mk]['fname'],
                                'LastName'=>$listdata[$mk]['lname'],
                                'PaxType' =>'3',
                                'DateOfBirth'=>$infadob,
                                'Gender'=>$infagender,
                                "GSTCompanyAddress"=> $gstaddress,
                                "GSTCompanyContactNumber"=> $gstcontactno,
                                "GSTCompanyName"=> $gstcompname,
                                "GSTNumber"=> $gstnumber,
                                "GSTCompanyEmail"=> $gstemail,
                                "PassportNo"=>$infatPassportNo,
                                "PassportExpiry"=> $infatPassportExpiry,
                                "AddressLine1"=> $AddressLine1,
                                "AddressLine2"=> "",
                                "City"=> $City,
                                "CountryCode"=> $CountryCode,
                                "CountryName"=> $CountryName,
                                "Nationality"=> $CountryCode,
                                "ContactNo"=> $ContactNo,
                                "Email"=> $Email,
                                "IsLeadPax"=> false,
                                "FFAirline"=> $ff_name,
                                "FFNumber"=> $ff_number,
                                "Fare"=>[array(
                                    "BaseFare"=>$infantfare_new,
                                    "Tax"=> $infanttax_new,
                                    "TransactionFee"=> "",
                                    "AdditionalTxnFeeOfrd"=> $infanttxnfeeofrd,
                                    "AdditionalTxnFeePub"=> $infanttxnfeepub,
                                    "AirTransFee"=> "0.0",
                                )],

                                "Meal Dynamic"=>[array(
                                   "AirlineCode"=>$mealflightcode,
                                    "FlightNumber"=>$mealflightname,
                                    "WayType"=>$mealway,
                                    "Code"=> $Code,
                                    "Description"=> $Description,
                                    "AirlineDescription"=>$mealname,
                                    "Quantity"=>$mealquantaty,
                                    "Currency"=> $Currency,
                                    "Price"=>$meal_orgprice,
                                    "Origin"=> $Origin,
                                    "Destination"=> $Destination,
                                )],


                            );
                        } //multicity infa if close
                }
                //close infant

              }
               $form_data = array(
                    'EndUserIp'=>$EndUserIp,
                    'TokenId'=>$TokenId,
                    'TraceId'=>$TraceId,
                    'ResultIndex'=>$ResultIndex,
                    'Passengers'=>$addultpassengerdetail,
                );
                $data_string = json_encode($form_data);
               
              
                $ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Ticket/');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );

                $result = curl_exec($ch);
                
           
               
                $resultticket=json_decode($result,true);
               
                $errorcode = $resultticket['Response']['Error']['ErrorCode'];
                 $errormsg = $resultticket['Response']['Error']['ErrorMessage'];
                $paymentdb = tbl_payment_order::where('orderid', $orderid)->where('referenceid', $paymentid)->update(['reponse' => $errorcode,'response_msg' => $errormsg,'payment_mode'=>$paymenttype]);
                if($resultticket['Response']['Error']['ErrorCode']!='0')
                {

                    $message = $resultticket['Response']['Error']['ErrorMessage'];
                    $newreturn['error']=$message;
                    
                    
                }
                else
                {
                   
                    $PNR=$resultticket['Response']['Response']['PNR'];
                    $BookingId=$resultticket['Response']['Response']['BookingId'];
                    $status=$resultticket['Response']['Response']['TicketStatus'];
                    $flightstatus='LCC';
                    $newreturn['PNR']=$PNR;
                    $newreturn['BookingId']=$BookingId;
                    $newreturn['status']=$status;


                }
            
        }
        else
        {
                 for($mk=0;$mk<count($listdata);$mk++)
              {
                
               if($listdata[$mk]['types']=='Adult')
               {
                   $adultcount++;
                  $newgendera=explode(".",$listdata[$mk]['gender']);
                    if($$newgendera[0]=='Mr')
                    {
                        $adultgender="1";
                    }
                    else
                    {
                        $adultgender="2";
                    }
                    if(count($listdata[$mk]['types'])=='0')
                    {
                        $paxval="true";
                       
                    }
                    else
                    {
                        $paxval="false";
                       
                    }
                     if($passportcheck==1)
                    {
                        $PassportNo=$request->get(['passportno'][$mk]);
                        $passportexp=explode('/',$$request->get(['passportexp'][$mk]));
                        $PassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $PassportNo="";
                        $PassportExpiry="";
                    }
                    $adultdob1=explode('/',$listdata[$mk]['age']);
                    $adultdob = $adultdob1[2]."-".$adultdob1[1]."-".$adultdob1[0]."T00:00:00";
                  
                        $addultpassengerdetaillcc[]=array(
                               'Title' =>$newgendera[0],
                            'FirstName'=> $listdata[$mk]['fname'],
                            'LastName'=>$listdata[$mk]['lname'],
                            'PaxType' =>'1',
                            'DateOfBirth'=>$adultdob,
                            "PassportNo"=> $PassportNo,
                            "PassportExpiry"=> $PassportExpiry,
                            'Gender'=>$adultgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,

                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> $paxval,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$adultbasefare_new,
                                "Tax"=> $adulttaxfare_new,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $adulttxnfeeofrd,
                                "AdditionalTxnFeePub"=> $adulttxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],

                           


                        );
                    // else condtion journy time close

               } //adult if
               
                //child if start
               if($listdata[$mk]['types']=='Children')
               {
                $childcount++;
                $newgenderc=explode(".",$listdata[$mk]['gender']);
                     if($listdata[$mk]['gender']==$newgenderc[0])
                    {
                        $childgender="1";
                    }
                    else
                    {
                        $childgender="2";
                    }
                    if(count($listdata[$mk]['types'])=='0')
                    {
                        $paxval="true";
                       
                    }
                    else
                    {
                        $paxval="false";
                       
                    }
                    if($passportcheck==1)
                    {
                        $childPassportNo=$request->get(['passportno'][$mk]);
                        $passportexp=explode('/',$$request->get(['passportexp'][$mk]));
                        $childPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $childPassportNo="";
                        $childPassportExpiry="";
                    }
                    $childdob1=explode('/',$listdata[$mk]['age']);
                    $childdob = $childdob1[2]."-".$childdob1[1]."-".$childdob1[0]."T00:00:00";
                   
                        $addultpassengerdetaillcc[]=array(
                             'Title' =>$newgenderc[0],
                            'FirstName'=> $listdata[$mk]['fname'],
                            'LastName'=>$listdata[$mk]['lname'],
                            'PaxType' =>'2',
                            'DateOfBirth'=>$childdob,
                            'Gender'=>$childgender,
                            "GSTCompanyAddress"=> $gstaddress,
                            "GSTCompanyContactNumber"=> $gstcontactno,
                            "GSTCompanyName"=> $gstcompname,
                            "GSTNumber"=> $gstnumber,
                            "GSTCompanyEmail"=> $gstemail,
                            "PassportNo"=> $childPassportNo,
                            "PassportExpiry"=>  $childPassportExpiry,
                            "AddressLine1"=> $AddressLine1,
                            "AddressLine2"=> "",
                            "City"=> $City,
                            "CountryCode"=> $CountryCode,
                            "CountryName"=> $CountryName,
                            "Nationality"=> $CountryCode,
                            "ContactNo"=> $ContactNo,
                            "Email"=> $Email,
                            "IsLeadPax"=> false,
                            "FFAirline"=> $ff_name,
                            "FFNumber"=> $ff_number,
                            "Fare"=>[array(
                                "BaseFare"=>$childfare_new,
                                "Tax"=> $childtaxfare_new,
                                "TransactionFee"=> "",
                                "AdditionalTxnFeeOfrd"=> $childtxnfeeofrd,
                                "AdditionalTxnFeePub"=> $childtxnfeepub,
                                "AirTransFee"=> "0.0",
                            )],
                          


                        );
                   //multicity if close
               } //child if end
               // child if end
               if($listdata[$mk]['types']=='Infants')
               {
                $newgenderi=explode(".",$listdata[$mk]['gender']);
                     if($listdata[$mk]['gender']==$newgenderi[0])
                    {
                        $infagender="1";
                    }
                    else
                    {
                        $infagender="2";
                    }
                    if(count($listdata[$mk]['types'])=='0')
                    {
                        $paxval="true";
                       
                    }
                    else
                    {
                        $paxval="false";
                       
                    }
                     if($passportcheck==1)
                    {
                        $infatPassportNo=$request->get(['passportno'][$mk]);
                        $passportexp=explode('/',$$request->get(['passportexp'][$mk]));
                        $infatPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                        

                    }
                    else
                    {
                        $infatPassportNo="";
                        $infatPassportExpiry="";
                    }
                    $infadob1=explode('/',$listdata[$mk]['age']);
                    $infadob = $infadob1[2]."-".$infadob1[1]."-".$infadob1[0]."T00:00:00";
                    
                            $addultpassengerdetaillcc[]=array(
                               'Title' =>$newgenderi[0],
                                'FirstName'=> $listdata[$mk]['fname'],
                                'LastName'=>$listdata[$mk]['lname'],
                                'PaxType' =>'3',
                                'DateOfBirth'=>$infadob,
                                'Gender'=>$infagender,
                                "GSTCompanyAddress"=> $gstaddress,
                                "GSTCompanyContactNumber"=> $gstcontactno,
                                "GSTCompanyName"=> $gstcompname,
                                "GSTNumber"=> $gstnumber,
                                "GSTCompanyEmail"=> $gstemail,
                                "PassportNo"=>$infatPassportNo,
                                "PassportExpiry"=> $infatPassportExpiry,
                                "AddressLine1"=> $AddressLine1,
                                "AddressLine2"=> "",
                                "City"=> $City,
                                "CountryCode"=> $CountryCode,
                                "CountryName"=> $CountryName,
                                "Nationality"=> $CountryCode,
                                "ContactNo"=> $ContactNo,
                                "Email"=> $Email,
                                "IsLeadPax"=> false,
                                "FFAirline"=> $ff_name,
                                "FFNumber"=> $ff_number,
                                "Fare"=>[array(
                                    "BaseFare"=>$infantfare_new,
                                    "Tax"=> $infanttax_new,
                                    "TransactionFee"=> "",
                                    "AdditionalTxnFeeOfrd"=> $infanttxnfeeofrd,
                                    "AdditionalTxnFeePub"=> $infanttxnfeepub,
                                    "AirTransFee"=> "0.0",
                                )],

                               

                            );
                       //multicity infa if close
                }
                //close infant

              }
              $form_data = array(
                    'EndUserIp'=>$EndUserIp,
                    'TokenId'=>$TokenId,
                    'TraceId'=>$TraceId,
                    'ResultIndex'=>$ResultIndex,
                    'Passengers'=>$addultpassengerdetaillcc,
                );
                $data_string_lcc = json_encode($form_data);
                
                  $ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Book/');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_lcc);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string_lcc))
                );

                $result = curl_exec($ch);
                $result1=json_decode($result,true);
                
                $errorcode1 = $result1['Response']['Error']['ErrorCode'];
                $errormsg1 = $result1['Response']['Error']['ErrorMessage'];
                // $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1]);
                $paymentdb = tbl_payment_order::where('orderid', $orderid)->where('referenceid', $paymentid)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1,'payment_mode'=>$paymenttype]);
                if($result1['Response']['Error']['ErrorCode']!='0')
                {

                    $message = $result1['Response']['Error']['ErrorMessage'];
                     $newreturn['error']=$message;
                    
                  
                }
                else
                {

                    $PNR=$result1['Response']['Response']['PNR'];
                    $BookingId=$result1['Response']['Response']['BookingId'];
                    $status=$result1['Response']['Response']['FlightItinerary']['Status'];
                    $flightstatus='Non Lcc';
                     $newresult['PNR']=$PNR;
                    $newresult['BookingId']=$BookingId;
                    $newresult['status']=$status;
                   
                    //again ticket call is non lcc flight
                    $form_data_ticket = array(
                        'EndUserIp'=>$EndUserIp,
                        'TokenId'=>$TokenId,
                        'TraceId'=>$TraceId,
                        'PNR'=>$PNR,
                        'BookingId'=>$BookingId,

                    );
                    $data_string_ticket = json_encode($form_data_ticket);
                    
                    $ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Ticket/');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_ticket);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($data_string_ticket))
                    );

                    $result_new = curl_exec($ch);
                    $resultnew['ticketarray']=$result_new;

                    $resultticket_new=json_decode($result_new,true);


                    // echo "<hr>";

                    $errorcode1 = $resultticket_new['Response']['Error']['ErrorCode'];
                    $errormsg1 = $resultticket_new['Response']['Error']['ErrorMessage'];
                    // $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1]);
                    $paymentdb = tbl_payment_order::where('orderid', $orderid)->where('referenceid', $paymentid)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1,'payment_mode'=>$paymenttype]);
                    if($result1['Response']['Error']['ErrorCode']!='0')
                    {
                        
                         $newresult['error']=$errormsg1;
                    }
                    // end call non lcc flight

                }

        }
          

        if($status=='1')
        {
             $form_data_book = array(
                    'EndUserIp'=>$EndUserIp,
                    'TokenId'=>$TokenId,
                    'TraceId'=>$TraceId,
                    'BookingId'=>$BookingId,
                    'PNR' =>$PNR,
                );
             $data_string_book = json_encode($form_data_book);
             $ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/GetBookingDetails/');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_book);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string_book))
                );
                $result_book = curl_exec($ch);

                $result_book1=json_decode($result_book,true);
              
                 $errorcode1 = $result_book1['Response']['Error']['ErrorCode'];
                 $errormsg1 = $result_book1['Response']['Error']['ErrorMessage'];
                // $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1,'chkpnr'=>$PNR]);
                 $paymentdb = tbl_payment_order::where('orderid', $orderid)->where('referenceid', $paymentid)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1,'payment_mode'=>$paymenttype,'chkpnr'=>$PNR]);
               
                if($result_book1['Response']['Error']['ErrorCode']!=0)
                {
                    $newresult["error"]=$errormsg1;
                    
                }
                else
                {
                     $flightdata1=$result_book1['Response'];
                    $flightdata = serialize($flightdata1);
                    $domestic=$result_book1['Response']['FlightItinerary']['IsDomestic'];
                    $orign=$result_book1['Response']['FlightItinerary']['Origin'];
                    $destination=$result_book1['Response']['FlightItinerary']['Destination'];
                    $lcc=$result_book1['Response']['FlightItinerary']['IsLCC'];
                    $fareType=$result_book1['Response']['FlightItinerary']['FareType'];
                    $fare=$result_book1['Response']['FlightItinerary']['Fare']; //array
                    $segment=$result_book1['Response']['FlightItinerary']['Segments']; //array
                    $airlinecode = $result_book1['Response']['FlightItinerary']['Segments'][0]['Airline']['AirlineCode'];
                    $flightno = $result_book1['Response']['FlightItinerary']['Segments'][0]['Airline']['FlightNumber'];
                    $passengerdetail=$result_book1['Response']['FlightItinerary']['Passenger']; //array
                    $passengerfname=$result_book1['Response']['FlightItinerary']['Passenger'][0]['FirstName'];
                    $passengerlname=$result_book1['Response']['FlightItinerary']['Passenger'][0]['LastName'];
                    $passengername =$passengerfname.' '.$passengerlname;
                    $passengercity=$result_book1['Response']['FlightItinerary']['Passenger'][0]['City'];
                    $passengerphone=$result_book1['Response']['FlightItinerary']['Passenger'][0]['ContactNo'];
                    $passengeremail=$result_book1['Response']['FlightItinerary']['Passenger'][0]['Email'];
                    $ticketid="";
                    $ticketno="";
                    $totalpassenger =  count($result_book1['Response']['FlightItinerary']['Passenger']);
                        if(!empty($result_book1['Response']['FlightItinerary']['Passenger'][0]['Ticket']['TicketId']))
                        {
                            for($ps=0;$ps<$totalpassenger;$ps++)
                            {
                                if($ps==0)
                                {
                                    $ticketid .=$result_book1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketId'];
                                    $ticketno .=$result_book1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketNumber'];
                                }
                                else
                                {
                                    $ticketid .=",".$result_book1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketId'];
                                    $ticketno .=",".$result_book1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketNumber'];
                                }

                            }
                        }
                         $farebasiccode = $result_book1['Response']['FlightItinerary']['FareRules'][0]['FareBasisCode'];
                        if($lcc!='1')
                        {
                            $farefamilycode = $result_book1['Response']['FlightItinerary']['FareRules'][0]['FareFamilyCode'];
                            $invoice_amount='';
                            $invoice_no='';
                            $invoice_status='';
                            $invoice_date='';
                            $invoice_new_date='';

                        }
                        else
                        {
                            $farefamilycode='';
                            $invoice_amount=$result_book1['Response']['FlightItinerary']['InvoiceAmount'];
                            $invoice_no=$result_book1['Response']['FlightItinerary']['InvoiceNo'];
                            $invoice_status=$result_book1['Response']['FlightItinerary']['InvoiceStatus'];
                            $invoice_date=$result_book1['Response']['FlightItinerary']['InvoiceCreatedOn'];
                            $invoice_date1=explode('T',$invoice_date);
                            $invoice_new_date=$invoice_date1[0];
                        }


                        $flightbook_date = $result_book1['Response']['FlightItinerary']['Segments'][0]['Origin']['DepTime'];

                } //bookif
                //strart db data
                 $flightbook_date1=explode('T',$flightbook_date);
                $flight_booking_date=$flightbook_date1[0];
                $flight_booking_time=$flightbook_date1[1];
                $create_date=date('Y-m-d');
                $create_time=date('H:i:s');
                $farearray = serialize($fare);
                $segmentarray = serialize($segment);
                $flight = new tbl_flight_book;
                $passengerdetailarray = serialize($passengerdetail);
                $flight->flight_booking_date=$flight_booking_date;
                $flight->pnrno = $PNR;
                $flight->bookingId = $BookingId;
                $flight->domestic = $domestic;
                $flight->orign = $orign;
                $flight->destination = $destination;
                $flight->lcc = $lcc;
                $flight->fareType = $fareType;
                $flight->fare= $farearray;
                $flight->segment= $segmentarray;
                $flight->passenger_detail= $passengerdetailarray;
                $flight->farebasiccode = $farebasiccode;
                $flight->status= $status;
                $flight->invoice_amount= $invoice_amount;
                $flight->invoice_no= $invoice_no;
                $flight->invoice_status= $invoice_status;
                $flight->invoice_date = $invoice_date;
                $flight->invoice_new_date= $invoice_new_date;
                $flight->booking_date= $create_date;
                $flight->booking_time= $create_time;
                $flight->farefamilycode = $farefamilycode;
                $flight->pname= $passengername;
                $flight->pmobile= $passengerphone;
                $flight->pemail= $passengeremail;
                $flight->pcity = $passengercity;
                $flight->adults= $adultcount;
                $flight->child = $childcount;
                $flight->journytype=$journytype;
                $flight->flightcabinclass=$flightcabinclass;
                $flight->grandprice=$grandprice;
                $flight->mealname=$mealname;
                
                $flight->order_id=$paymentorder;
                $flight->order_ref=$paymentref;
                $flight->login_id=$user_id;
                $flight->login_username=$username;
                $flight->login_guest_email=$useremail;
                $flight->refund=$refund;
                $flight->flightdata=$flightdata;
                $flight->guestphone=$userphone;
                $flight->flight_ticekt_id=$ticketid;
                $flight->flight_ticekt_no=$ticketno;
                $flight->flight_check=$flightstatus;
                $flight->mealprice=$meal_orgprice;
                $flight->meal_orgprice=$meal_orgprice;
                $flight->bag_weight=$Weight;
                $flight->bag_price=$Price;
                $flight->bag_orgprice=$Price;
               
                // $flight->currency_icon=$currencyicon;
                // $flight->currency_convert=$convertcurrecny;
                $flight->eme_name=$eme_name;
                $flight->ema_phone=$ema_phone;
                $flight->eme_relation=$eme_relation;
                $flight->ff_number=$ff_number;
                $flight->ff_name=$ff_name;
                $flight->check_type='android';
                if($flight->save())
                {
                    $tbl_id = $flight->id;

                    // $paymenttable = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['ticket_id' => $tbl_id,
                    //     'payment_code'=>'Flight',
                    // ]);
                    $mytrip = new tbl_mytrip;
                    $mytrip->triptype='Flight';
                    $mytrip->bookingid=$BookingId;
                    $mytrip->pnr=$PNR;
                    $mytrip->tableid=$tbl_id;
                    $mytrip->booking_date=$flight_booking_date;
                    $mytrip->booking_time=$flight_booking_time;
                    $mytrip->trip_date=$create_date;
                    $mytrip->trip_time=$create_time;
                    $mytrip->login_id=$user_id;
                    $mytrip->login_username=$username;
                    $mytrip->login_guest_email=$useremail;
                    $mytrip->guestphone=$userphone;
                    $mytrip->save();
                    // echo "<script>alert('Successfully Ticket Booked');</script>";
                    // return view('pages.flight_newinvoice');
                    // $this->pnr = $PNR;
                    // return new pnr($PNR);
                    //smsstart
                    $hotelsms=Cookie::get('gensetting');


                    $chdata = session()->get('postData');

                    $customerName = $chdata['customerName'];
                    $orderCurrency=$chdata['orderCurrency'];
                    $customerPhone = $chdata['customerPhone'];
                    $customerEmail =  $chdata['customerEmail'];
                    // $hl = explode("-", $hotelsms->sms_active);
                    // $sms_user=$hotelsms->sms_user_id;
                    // $sms_password=$hotelsms->sms_password;
                    // $sms_sender=$hotelsms->sms_sender;
                    // $sms_url=$hotelsms->sms_url;
                    // $bookingdate= date("d M Y" , strtotime($flight_booking_date));
                    // $bookingtime = date("H:s" , strtotime($flight_booking_time));
                    // if($hl[1]=='1')
                    // {

                    //     $leadphone1=$customerPhone;
                    //     $user=$sms_user;
                    //     $password=$sms_password;
                    //     $sender=$sms_sender;

                    //     $text="Dear ".$titleadult[0]." ".$passengername.", ​Your ".$flightname." PNR is ".$PNR." (".$airlinecode."-".$flightno.") booking on ".$bookingdate." ".$bookingtime.". Please check your email for further details. -Travo Web. ";
                    //     $otp=urlencode("$text");
                    //     $data = array(
                    //         'usr' => $user,
                    //         'pwd' => $password,
                    //         'ph'  => $leadphone1,
                    //         'sndr'  => $sender,
                    //         'text'  => $otp
                    //     );

                    //     $ch = curl_init($sms_url);
                    //     curl_setopt($ch, CURLOPT_POST, true);
                    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    //     curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
                    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                    //     $result = curl_exec($ch);

                    // }
                    //smsend
                  
                    if($resultindex_return=='')
                    {
                        $newreturn['pnr']=$PNR;
                        $newreturn['pnrno_return']="";
                       
                    }

                }
                else
                {
                   $newreturn['error']="Error Ticket";
                }
                //end status

        }

         //status 1

    //return flight
  

        if($resultindex_return !='')
        {
            $adultcount=0;
            $childcount=0;
            if($lcccheck_return=='1')
            {  for($mk=0;$mk<count($listdata);$mk++)
                {
                    if($listdata[$mk]['types']=='Adult')
                   {
                        
                        $newgender=explode(".",$listdata[$mk]['gender']);

                        $adultcount++;
                        if($listdata[$mk]['gender']==$newgender[0])
                        {
                            $adultgender="1";
                        }
                        else
                        {
                            $adultgender="2";
                        }
                        if(count($listdata[$mk]['types'])=='0')
                        {
                            $paxval="true";
                           
                        }
                        else
                        {
                            $paxval="false";
                           
                        }
                         if($passportcheck==1)
                        {
                            $PassportNo=$request->get(['passportno'][$mk]);
                            $passportexp=explode('/',$$request->get(['passportexp'][$mk]));
                            $PassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                            

                        }
                        else
                        {
                            $PassportNo="";
                            $PassportExpiry="";
                        }
                        $adultdob1=explode('/',$listdata[$mk]['age']);
                        $adultdob = $adultdob1[2]."-".$adultdob1[1]."-".$adultdob1[0]."T00:00:00";
                      
                            $addultpassengerdetailr[]=array(
                                   'Title' =>$newgender[0],
                                'FirstName'=> $listdata[$mk]['fname'],
                                'LastName'=>$listdata[$mk]['lname'],
                                'PaxType' =>'1',
                                'DateOfBirth'=>$adultdob,
                                "PassportNo"=> $PassportNo,
                                "PassportExpiry"=> $PassportExpiry,
                                'Gender'=>$adultgender,
                                "GSTCompanyAddress"=> $gstaddress,
                                "GSTCompanyContactNumber"=> $gstcontactno,
                                "GSTCompanyName"=> $gstcompname,
                                "GSTNumber"=> $gstnumber,
                                "GSTCompanyEmail"=> $gstemail,

                                "AddressLine1"=> $AddressLine1,
                                "AddressLine2"=> "",
                                "City"=> $City,
                                "CountryCode"=> $CountryCode,
                                "CountryName"=> $CountryName,
                                "Nationality"=> $CountryCode,
                                "ContactNo"=> $ContactNo,
                                "Email"=> $Email,
                                "IsLeadPax"=> $paxval,
                                "FFAirline"=> $ff_name_return,
                                "FFNumber"=> $ff_number_return,
                                "Fare"=>[array(
                                    "BaseFare"=>$adultbasefarereturn,
                                    "Tax"=> $adulttaxfarereturn,
                                    "TransactionFee"=> "",
                                    "AdditionalTxnFeeOfrd"=> $adulttxnfeeofrdreturn,
                                    "AdditionalTxnFeePub"=> $adulttxnfeepubreturn,
                                    "AirTransFee"=> "0.0",
                                )],
                                "Baggage"=>[array(
                                                         "AirlineCode"=>$bagflightnumber_return,
                                                        "FlightNumber"=>$bagflightcode_return,
                                                        "WayType"=>$WayType_return,
                                                        "Code"=> $bagCode_return,
                                                        "Description"=>$bagDescription_return,
                                                        "Weight"=> $Weight_return,
                                                        "Currency"=> $Currency,
                                                        "Price"=>$price_return,
                                                        "Origin"=> $Origin_return,
                                                        "Destination"=> $Destination_return,
                                )],
                                "Meal Dynamic"=>[array(
                                   "AirlineCode"=>$mealflightcode,
                                                            "FlightNumber"=>$mealflightcode_return,
                                                            "WayType"=>$mealflightname_return,
                                                            "Code"=> $mealway_return,
                                                            "Description"=> $Code_return,
                                                            "AirlineDescription"=>$Description_return,
                                                            "Quantity"=>$mealquantaty_return,
                                                            "Currency"=> $Currency,
                                                            "Price"=>$meal_orgprice_return,
                                                            "Origin"=> $Origin_return,
                                                            "Destination"=> $Destination_return,
                                )],


                            );
                        // else condtion journy time close

                   } 
                   if($listdata[$mk]['types']=='Children')
                   {
                        $childcount++;
                        $newgenderc=explode(".",$listdata[$mk]['gender']);
                         if($listdata[$mk]['gender']==$newgenderc[0])
                        {
                            $childgender="1";
                        }
                        else
                        {
                            $childgender="2";
                        }
                        if(count($listdata[$mk]['types'])=='0')
                        {
                            $paxval="true";
                           
                        }
                        else
                        {
                            $paxval="false";
                           
                        }
                        if($passportcheck==1)
                        {
                            $childPassportNo=$request->get(['passportno'][$mk]);
                            $passportexp=explode('/',$$request->get(['passportexp'][$mk]));
                            $childPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                            

                        }
                        else
                        {
                            $childPassportNo="";
                            $childPassportExpiry="";
                        }
                        $childdob1=explode('/',$listdata[$mk]['age']);
                        $childdob = $childdob1[2]."-".$childdob1[1]."-".$childdob1[0]."T00:00:00";
                       
                            $addultpassengerdetailr[]=array(
                                'Title' =>$newgenderc[0],
                                'FirstName'=> $listdata[$mk]['fname'],
                                'LastName'=>$listdata[$mk]['lname'],
                                'PaxType' =>'2',
                                'DateOfBirth'=>$childdob,
                                'Gender'=>$childgender,
                                "GSTCompanyAddress"=> $gstaddress,
                                "GSTCompanyContactNumber"=> $gstcontactno,
                                "GSTCompanyName"=> $gstcompname,
                                "GSTNumber"=> $gstnumber,
                                "GSTCompanyEmail"=> $gstemail,
                                "PassportNo"=> $childPassportNo,
                                "PassportExpiry"=>  $childPassportExpiry,
                                "AddressLine1"=> $AddressLine1,
                                "AddressLine2"=> "",
                                "City"=> $City,
                                "CountryCode"=> $CountryCode,
                                "CountryName"=> $CountryName,
                                "Nationality"=> $CountryCode,
                                "ContactNo"=> $ContactNo,
                                "Email"=> $Email,
                                "IsLeadPax"=> false,
                                "FFAirline"=> $ff_name,
                                "FFNumber"=> $ff_number,
                                "Fare"=>[array(
                                    "BaseFare"=>$childbasefarereturn,
                                    "Tax"=> $childtaxfarereturn,
                                    "TransactionFee"=> "",
                                    "AdditionalTxnFeeOfrd"=> $childtxnfeeofrdreturn,
                                    "AdditionalTxnFeePub"=> $childtxnfeepubreturn,
                                    "AirTransFee"=> "0.0",
                                )],
                                "Baggage"=>[array(
                                                         "AirlineCode"=>$bagflightnumber_return,
                                                        "FlightNumber"=>$bagflightcode_return,
                                                        "WayType"=>$WayType_return,
                                                        "Code"=> $bagCode_return,
                                                        "Description"=>$bagDescription_return,
                                                        "Weight"=> $Weight_return,
                                                        "Currency"=> $Currency,
                                                        "Price"=>$price_return,
                                                        "Origin"=> $Origin_return,
                                                        "Destination"=> $Destination_return,
                                )],
                                "Meal Dynamic"=>[array(
                                   "AirlineCode"=>$mealflightcode,
                                                            "FlightNumber"=>$mealflightcode_return,
                                                            "WayType"=>$mealflightname_return,
                                                            "Code"=> $mealway_return,
                                                            "Description"=> $Code_return,
                                                            "AirlineDescription"=>$Description_return,
                                                            "Quantity"=>$mealquantaty_return,
                                                            "Currency"=> $Currency,
                                                            "Price"=>$meal_orgprice_return,
                                                            "Origin"=> $Origin_return,
                                                            "Destination"=> $Destination_return,
                                )],


                            );
                        //multicity if close
                   }
                   //infant changes
                    if($listdata[$mk]['types']=='Infants')
                   {
                        $newgenderi=explode(".",$listdata[$mk]['gender']);
                         if($listdata[$mk]['gender']==$newgenderi[0])
                        {
                            $infagender="1";
                        }
                        else
                        {
                            $infagender="2";
                        }
                        if(count($listdata[$mk]['types'])=='0')
                        {
                            $paxval="true";
                           
                        }
                        else
                        {
                            $paxval="false";
                           
                        }
                         if($passportcheck==1)
                        {
                            $infatPassportNo=$request->get(['passportno'][$mk]);
                            $passportexp=explode('/',$$request->get(['passportexp'][$mk]));
                            $infatPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                            

                        }
                        else
                        {
                            $infatPassportNo="";
                            $infatPassportExpiry="";
                        }
                        $infadob1=explode('/',$listdata[$mk]['age']);
                        $infadob = $infadob1[2]."-".$infadob1[1]."-".$infadob1[0]."T00:00:00";
                         
                                $addultpassengerdetailr[]=array(
                                   'Title' =>$newgenderi[0],
                                    'FirstName'=> $listdata[$mk]['fname'],
                                    'LastName'=>$listdata[$mk]['lname'],
                                    'PaxType' =>'3',
                                    'DateOfBirth'=>$infadob,
                                    'Gender'=>$infagender,
                                    "GSTCompanyAddress"=> $gstaddress,
                                    "GSTCompanyContactNumber"=> $gstcontactno,
                                    "GSTCompanyName"=> $gstcompname,
                                    "GSTNumber"=> $gstnumber,
                                    "GSTCompanyEmail"=> $gstemail,
                                    "PassportNo"=>$infatPassportNo,
                                    "PassportExpiry"=> $infatPassportExpiry,
                                    "AddressLine1"=> $AddressLine1,
                                    "AddressLine2"=> "",
                                    "City"=> $City,
                                    "CountryCode"=> $CountryCode,
                                    "CountryName"=> $CountryName,
                                    "Nationality"=> $CountryCode,
                                    "ContactNo"=> $ContactNo,
                                    "Email"=> $Email,
                                    "IsLeadPax"=> false,
                                    "FFAirline"=> $ff_name,
                                    "FFNumber"=> $ff_number,
                                    "Fare"=>[array(
                                        "BaseFare"=>$infantbasefarereturn,
                                        "Tax"=> $infanttaxfarereturn,
                                        "TransactionFee"=> "",
                                        "AdditionalTxnFeeOfrd"=> $infanttxnfeeofrdreturn,
                                        "AdditionalTxnFeePub"=> $infanttxnfeepubreturn,
                                        "AirTransFee"=> "0.0",
                                    )],

                                    "Meal Dynamic"=>[array(
                                   "AirlineCode"=>$mealflightcode,
                                                "FlightNumber"=>$mealflightcode_return,
                                                "WayType"=>$mealflightname_return,
                                                "Code"=> $mealway_return,
                                                "Description"=> $Code_return,
                                                "AirlineDescription"=>$Description_return,
                                                "Quantity"=>$mealquantaty_return,
                                                "Currency"=> $Currency,
                                                "Price"=>$meal_orgprice_return,
                                                "Origin"=> $Origin_return,
                                                "Destination"=> $Destination_return,
                                         )],


                                );
                            //multicity infa if close
                   }
                }
                     $form_data_return = array(
                        'EndUserIp'=>$EndUserIp,
                        'TokenId'=>$TokenId,
                        'TraceId'=>$TraceId,
                        'ResultIndex'=>$resultindex_return,
                        'Passengers'=>$addultpassengerdetailr,
                    );
                    $data_string_return = json_encode($form_data_return);
                   
                     $ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Ticket/');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_return);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($data_string_return))
                    );

                    $result_ret = curl_exec($ch);
                     $resultticket_return=json_decode($result_ret,true);
                    

                    $errorcode1 = $resultticket_return['Response']['Error']['ErrorCode'];
                    $errormsg1 = $resultticket_return['Response']['Error']['ErrorMessage'];
                    // $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1]);
                     $paymentdb = tbl_payment_order::where('orderid', $orderid)->where('referenceid', $paymentid)->update(['reponse' => $errorcode1,'response_msg' => $errormsg1,'payment_mode'=>$paymenttype]);
                      if($resultticket_return['Response']['Error']['ErrorCode']!='0')
                    {

                        $message = $resultticket_return['Response']['Error']['ErrorMessage'];
                        $newreturn["error3"]=$message;
                       
                       
                    }
                     else
                    {

                        $PNR_return=$resultticket_return['Response']['Response']['PNR'];
                        $BookingId_return=$resultticket_return['Response']['Response']['BookingId'];
                        $status=$resultticket_return['Response']['Response']['TicketStatus'];
                        $flightstatus_return="Lcc";
                    }


            } 
            else
            {
                 for($mk=0;$mk<count($listdata);$mk++)
                {
                    if($listdata[$mk]['types']=='Adult')
                   {
                        
                        $newgender=explode(".",$listdata[$mk]['gender']);

                        $adultcount++;
                        if($listdata[$mk]['gender']==$newgender[0])
                        {
                            $adultgender="1";
                        }
                        else
                        {
                            $adultgender="2";
                        }
                        if(count($listdata[$mk]['types'])=='0')
                        {
                            $paxval="true";
                           
                        }
                        else
                        {
                            $paxval="false";
                           
                        }
                         if($passportcheck==1)
                        {
                            $PassportNo=$request->get(['passportno'][$mk]);
                            $passportexp=explode('/',$$request->get(['passportexp'][$mk]));
                            $PassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                            

                        }
                        else
                        {
                            $PassportNo="";
                            $PassportExpiry="";
                        }
                        $adultdob1=explode('/',$listdata[$mk]['age']);
                        $adultdob = $adultdob1[2]."-".$adultdob1[1]."-".$adultdob1[0]."T00:00:00";
                      
                            $addultpassengerdetaillccr[]=array(
                                   'Title' =>$newgender[0],
                                'FirstName'=> $listdata[$mk]['fname'],
                                'LastName'=>$listdata[$mk]['lname'],
                                'PaxType' =>'1',
                                'DateOfBirth'=>$adultdob,
                                "PassportNo"=> $PassportNo,
                                "PassportExpiry"=> $PassportExpiry,
                                'Gender'=>$adultgender,
                                "GSTCompanyAddress"=> $gstaddress,
                                "GSTCompanyContactNumber"=> $gstcontactno,
                                "GSTCompanyName"=> $gstcompname,
                                "GSTNumber"=> $gstnumber,
                                "GSTCompanyEmail"=> $gstemail,

                                "AddressLine1"=> $AddressLine1,
                                "AddressLine2"=> "",
                                "City"=> $City,
                                "CountryCode"=> $CountryCode,
                                "CountryName"=> $CountryName,
                                "Nationality"=> $CountryCode,
                                "ContactNo"=> $ContactNo,
                                "Email"=> $Email,
                                "IsLeadPax"=> $paxval,
                                "FFAirline"=> $ff_name_return,
                                "FFNumber"=> $ff_number_return,
                                "Fare"=>[array(
                                    "BaseFare"=>$adultbasefarereturn,
                                    "Tax"=> $adulttaxfarereturn,
                                    "TransactionFee"=> "",
                                    "AdditionalTxnFeeOfrd"=> $adulttxnfeeofrdreturn,
                                    "AdditionalTxnFeePub"=> $adulttxnfeepubreturn,
                                    "AirTransFee"=> "0.0",
                                )],
                             


                            );
                        // else condtion journy time close

                   } 
                   if($listdata[$mk]['types']=='Children')
                   {
                        $childcount++;
                        $newgenderc=explode(".",$listdata[$mk]['gender']);
                         if($listdata[$mk]['gender']==$newgenderc[0])
                        {
                            $childgender="1";
                        }
                        else
                        {
                            $childgender="2";
                        }
                        if(count($listdata[$mk]['types'])=='0')
                        {
                            $paxval="true";
                           
                        }
                        else
                        {
                            $paxval="false";
                           
                        }
                        if($passportcheck==1)
                        {
                            $childPassportNo=$request->get(['passportno'][$mk]);
                            $passportexp=explode('/',$$request->get(['passportexp'][$mk]));
                            $childPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                            

                        }
                        else
                        {
                            $childPassportNo="";
                            $childPassportExpiry="";
                        }
                        $childdob1=explode('/',$listdata[$mk]['age']);
                        $childdob = $childdob1[2]."-".$childdob1[1]."-".$childdob1[0]."T00:00:00";
                       
                            $addultpassengerdetaillccr[]=array(
                                'Title' =>$newgenderc[0],
                                'FirstName'=> $listdata[$mk]['fname'],
                                'LastName'=>$listdata[$mk]['lname'],
                                'PaxType' =>'2',
                                'DateOfBirth'=>$childdob,
                                'Gender'=>$childgender,
                                "GSTCompanyAddress"=> $gstaddress,
                                "GSTCompanyContactNumber"=> $gstcontactno,
                                "GSTCompanyName"=> $gstcompname,
                                "GSTNumber"=> $gstnumber,
                                "GSTCompanyEmail"=> $gstemail,
                                "PassportNo"=> $childPassportNo,
                                "PassportExpiry"=>  $childPassportExpiry,
                                "AddressLine1"=> $AddressLine1,
                                "AddressLine2"=> "",
                                "City"=> $City,
                                "CountryCode"=> $CountryCode,
                                "CountryName"=> $CountryName,
                                "Nationality"=> $CountryCode,
                                "ContactNo"=> $ContactNo,
                                "Email"=> $Email,
                                "IsLeadPax"=> false,
                                "FFAirline"=> $ff_name,
                                "FFNumber"=> $ff_number,
                                "Fare"=>[array(
                                    "BaseFare"=>$childbasefarereturn,
                                    "Tax"=> $childtaxfarereturn,
                                    "TransactionFee"=> "",
                                    "AdditionalTxnFeeOfrd"=> $childtxnfeeofrdreturn,
                                    "AdditionalTxnFeePub"=> $childtxnfeepubreturn,
                                    "AirTransFee"=> "0.0",
                                )],
                              


                            );
                        //multicity if close
                   }
                   //infant changes
                    if($listdata[$mk]['types']=='Infants')
                   {
                        $newgenderi=explode(".",$listdata[$mk]['gender']);
                         if($listdata[$mk]['gender']==$newgenderi[0])
                        {
                            $infagender="1";
                        }
                        else
                        {
                            $infagender="2";
                        }
                        if(count($listdata[$mk]['types'])=='0')
                        {
                            $paxval="true";
                           
                        }
                        else
                        {
                            $paxval="false";
                           
                        }
                         if($passportcheck==1)
                        {
                            $infatPassportNo=$request->get(['passportno'][$mk]);
                            $passportexp=explode('/',$$request->get(['passportexp'][$mk]));
                            $infatPassportExpiry=$passportexp[2]."-".$passportexp[1]."-".$passportexp[0]."T00:00:00";
                            

                        }
                        else
                        {
                            $infatPassportNo="";
                            $infatPassportExpiry="";
                        }
                        $infadob1=explode('/',$listdata[$mk]['age']);
                        $infadob = $infadob1[2]."-".$infadob1[1]."-".$infadob1[0]."T00:00:00";
                         
                                $addultpassengerdetaillccr[]=array(
                                   'Title' =>$newgenderi[0],
                                    'FirstName'=> $listdata[$mk]['fname'],
                                    'LastName'=>$listdata[$mk]['lname'],
                                    'PaxType' =>'3',
                                    'DateOfBirth'=>$infadob,
                                    'Gender'=>$infagender,
                                    "GSTCompanyAddress"=> $gstaddress,
                                    "GSTCompanyContactNumber"=> $gstcontactno,
                                    "GSTCompanyName"=> $gstcompname,
                                    "GSTNumber"=> $gstnumber,
                                    "GSTCompanyEmail"=> $gstemail,
                                    "PassportNo"=>$infatPassportNo,
                                    "PassportExpiry"=> $infatPassportExpiry,
                                    "AddressLine1"=> $AddressLine1,
                                    "AddressLine2"=> "",
                                    "City"=> $City,
                                    "CountryCode"=> $CountryCode,
                                    "CountryName"=> $CountryName,
                                    "Nationality"=> $CountryCode,
                                    "ContactNo"=> $ContactNo,
                                    "Email"=> $Email,
                                    "IsLeadPax"=> false,
                                    "FFAirline"=> $ff_name,
                                    "FFNumber"=> $ff_number,
                                    "Fare"=>[array(
                                        "BaseFare"=>$infantbasefarereturn,
                                        "Tax"=> $infanttaxfarereturn,
                                        "TransactionFee"=> "",
                                        "AdditionalTxnFeeOfrd"=> $infanttxnfeeofrdreturn,
                                        "AdditionalTxnFeePub"=> $infanttxnfeepubreturn,
                                        "AirTransFee"=> "0.0",
                                    )],

                                


                                );
                            //multicity infa if close
                   }
                }
                  $form_data_returnlcc = array(
                        'EndUserIp'=>$EndUserIp,
                        'TokenId'=>$TokenId,
                        'TraceId'=>$TraceId,
                        'ResultIndex'=>$ResultIndex,
                        'Passengers'=>$addultpassengerdetaillccr,
                    );
                    $data_string_lcc_return = json_encode($form_data_returnlcc);
                    $ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Book/');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_lcc_return);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($data_string_lcc_return))
                    );

                    $result_lcc_return = curl_exec($ch);

                    $result_lcc_return1=json_decode($result_lcc_return,true);
                    $errorcode11 = $result_lcc_return1['Response']['Error']['ErrorCode'];
                    $errormsg11 = $result_lcc_return1['Response']['Error']['ErrorMessage'];
                    // $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode11,'response_msg' => $errormsg11]);
                     $paymentdb = tbl_payment_order::where('orderid', $orderid)->where('referenceid', $paymentid)->update(['reponse' => $errorcode11,'response_msg' => $errormsg11,'payment_mode'=>$paymenttype]);
                    if($result_lcc_return1['Response']['Error']['ErrorCode']!='0')
                    {

                        $message = $result_lcc_return1['Response']['Error']['ErrorMessage'];
                       $newreturn['error']=$message;
                       
                    }
                    else
                    {
                        $PNR_return=$result_lcc_return1['Response']['Response']['PNR'];
                        $BookingId_return=$result_lcc_return1['Response']['Response']['BookingId'];
                        $status=$result_lcc_return1['Response']['Response']['FlightItinerary']['Status'];
                        //again ticket call is non lcc flight
                        $form_data_ticket = array(
                            'EndUserIp'=>$EndUserIp,
                            'TokenId'=>$TokenId,
                            'TraceId'=>$TraceId,
                            'PNR'=>$PNR_return,
                            'BookingId'=>$BookingId_return,
                        );
                        $data_string_ticket = json_encode($form_data_ticket);
                        $flightstatus_return="Non Lcc";
                        $ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Ticket/');
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_ticket);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                'Content-Type: application/json',
                                'Content-Length: ' . strlen($data_string_ticket))
                        );

                        $result_new = curl_exec($ch);
                        $resultticket_new=json_decode($result_new,true);



                        // end call non lcc flight
                    }

            }
            if($status=='1')
            {
                 $form_data_book_return = array(
                        'EndUserIp'=>$EndUserIp,
                        'TokenId'=>$TokenId,
                        'TraceId'=>$TraceId,
                        'BookingId'=>$BookingId_return,
                        'PNR' =>$PNR_return,
                    );
                    $data_string_book_return = json_encode($form_data_book_return);
                    $ch = curl_init('http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/GetBookingDetails/');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_book_return);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($data_string_book_return))
                    );

                    $result_book_return = curl_exec($ch);
                    $result_book_return1=json_decode($result_book_return,true);
                    $errorcode11 = $result_book_return1['Response']['Error']['ErrorCode'];
                    $errormsg11 = $result_book_return1['Response']['Error']['ErrorMessage'];
                    // $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode11,'response_msg' => $errormsg11]);
                    $paymentdb = tbl_payment_order::where('orderid', $orderid)->where('referenceid', $paymentid)->update(['reponse' => $errorcode11,'response_msg' => $errormsg11,'payment_mode'=>$paymenttype]);
                    $flightdata_return1=$result_book_return1['Response'];
                    $flightdata_return = serialize($flightdata_return1);
                    $domestic=$result_book_return1['Response']['FlightItinerary']['IsDomestic'];
                    $orign=$result_book_return1['Response']['FlightItinerary']['Origin'];
                    $destination=$result_book_return1['Response']['FlightItinerary']['Destination'];
                    $lcc=$result_book_return1['Response']['FlightItinerary']['IsLCC'];
                    $fareType=$result_book_return1['Response']['FlightItinerary']['FareType'];
                    $fare=$result_book_return1['Response']['FlightItinerary']['Fare']; //array
                    $segment=$result_book_return1['Response']['FlightItinerary']['Segments']; //array
                    $passengerdetail=$result_book_return1['Response']['FlightItinerary']['Passenger']; //array
                    $passengerfname=$result_book_return1['Response']['FlightItinerary']['Passenger'][0]['FirstName'];
                    $passengerlname=$result_book_return1['Response']['FlightItinerary']['Passenger'][0]['LastName'];
                    $passengername =$passengerfname.' '.$passengerlname;
                    $passengercity=$result_book_return1['Response']['FlightItinerary']['Passenger'][0]['City'];
                    $passengerphone=$result_book_return1['Response']['FlightItinerary']['Passenger'][0]['ContactNo'];
                    $passengeremail=$result_book_return1['Response']['FlightItinerary']['Passenger'][0]['Email'];

                    $totalpassenger =  count($result_book_return1['Response']['FlightItinerary']['Passenger']); //multiple passenger
                    $ticketid_return="";
                    $ticketno_return="";
                     for($ps=0;$ps<$totalpassenger;$ps++)
                    {
                        if($ps==0)
                        {
                            $ticketid_return .=$result_book_return1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketId'];
                            $ticketno_return .=$result_book_return1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketNumber'];
                        }
                        else
                        {
                            $ticketid_return .=",".$result_book_return1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketId'];
                            $ticketno_return .=",".$result_book_return1['Response']['FlightItinerary']['Passenger'][$ps]['Ticket']['TicketNumber'];
                        }

                    }
                    $farebasiccode = $result_book_return1['Response']['FlightItinerary']['FareRules'][0]['FareBasisCode'];
                    if($lcc!='1')
                    {
                        $farefamilycode = $result_book_return1['Response']['FlightItinerary']['FareRules'][0]['FareFamilyCode'];
                        $invoice_amount='';
                        $invoice_no='';
                        $invoice_status='';
                        $invoice_date='';
                        $invoice_new_date='';

                    }
                    else
                    {
                        $farefamilycode='';
                        $invoice_amount=$result_book_return1['Response']['FlightItinerary']['InvoiceAmount'];
                        $invoice_no=$result_book_return1['Response']['FlightItinerary']['InvoiceNo'];
                        $invoice_status=$result_book_return1['Response']['FlightItinerary']['InvoiceStatus'];
                        $invoice_date=$result_book_return1['Response']['FlightItinerary']['InvoiceCreatedOn'];
                        $invoice_date1=explode('T',$invoice_date);
                        $invoice_new_date=$invoice_date1[0];
                    }
                     $flightbook_date = $result_book_return1['Response']['FlightItinerary']['Segments'][0]['Origin']['DepTime'];
                    $flightbook_date1=explode('T',$flightbook_date);
                    $flight_booking_date=$flightbook_date1[0];
                    $flight_booking_time=$flightbook_date1[1];
                    $create_date=date('Y-m-d');
                    $create_time=date('H:i:s');
                    $farearray = serialize($fare);
                    $segmentarray = serialize($segment);
                    $flight = new tbl_flight_book;
                    $passengerdetailarray = serialize($passengerdetail);
                    $flight->flight_booking_date=$flight_booking_date;
                    $flight->pnrno = $PNR_return;
                    $flight->bookingId = $BookingId_return;
                    $flight->domestic = $domestic;
                    $flight->orign = $orign;
                    $flight->destination = $destination;
                    $flight->lcc = $lcc;
                    $flight->fareType = $fareType;
                    $flight->fare= $farearray;
                    $flight->segment= $segmentarray;
                    $flight->passenger_detail= $passengerdetailarray;
                    $flight->farebasiccode = $farebasiccode;
                    $flight->status= $status;
                    $flight->invoice_amount= $invoice_amount;
                    $flight->invoice_no= $invoice_no;
                    $flight->invoice_status= $invoice_status;
                    $flight->invoice_date = $invoice_date;
                    $flight->invoice_new_date= $invoice_new_date;
                    $flight->booking_date= $create_date;
                    $flight->booking_time= $create_time;
                    $flight->farefamilycode = $farefamilycode;
                    $flight->pname= $passengername;
                    $flight->pmobile= $passengerphone;
                    $flight->pemail= $passengeremail;
                    $flight->pcity = $passengercity;
                    $flight->adults= $adultcount;
                    $flight->child = $childcount;
                    $flight->journytype=$journytype;
                    $flight->flightcabinclass=$flightcabinclass;
                    $flight->grandprice=$grandprice;

                    $flight->order_id=$paymentorder;
                    $flight->order_ref=$paymentref;
                    $flight->login_id=$user_id;
                    $flight->login_username=$username;
                    $flight->login_guest_email=$useremail;
                    $flight->guestphone=$userphone;
                    $flight->refund=$refund;
                    $flight->flightdata=$flightdata_return;
                    $flight->flight_ticekt_id=$ticketid_return;
                    $flight->flight_ticekt_no=$ticketno_return;
                    $flight->flight_check=$flightstatus_return;
                    $flight->mealprice=$meal_orgprice_return;
                    $flight->meal_orgprice=$meal_orgprice_return;
                    $flight->bag_weight=$Weight_return;
                    $flight->bag_price=$price_return;
                    $flight->bag_orgprice=$price_return;
                    $flight->mealname=$mealname_return;
                    $flight->check_type='android';

                    // $flight->flightmainprice=$flightlastprice_return;
                    // $flight->flightmarginprice=$flightmarginprice_return;
                    // $flight->flighttax=$flighttaxprice_return;
                    // $flight->flightothercharges=$flightotherprice_return;
                    // $flight->currency_icon=$currencyicon;
                    // $flight->currency_convert=$convertcurrecny;
                    $flight->eme_name=$eme_name;
                    $flight->ema_phone=$ema_phone;
                    $flight->eme_relation=$eme_relation;
                    $flight->ff_number=$ff_number_return;
                    $flight->ff_name=$ff_name_return;
                    if($flight->save())
                    {
                        $tbl_id = $flight->id;
                        $mytrip = new tbl_mytrip;
                        $mytrip->triptype='Flight';
                        $mytrip->bookingid=$BookingId_return;
                        $mytrip->pnr=$PNR_return;
                        $mytrip->tableid=$tbl_id;
                        $mytrip->booking_date=$flight_booking_date;
                        $mytrip->booking_time=$flight_booking_time;
                        $mytrip->trip_date=$create_date;
                        $mytrip->trip_time=$create_time;
                        $mytrip->login_id=$user_id;
                        $mytrip->login_username=$username;
                        $mytrip->login_guest_email=$useremail;
                        $mytrip->guestphone=$userphone;
                        $mytrip->save();
                        // $paymenttable = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['ticket_id' => $tbl_id,
                        //     'payment_code'=>'Flight',
                        // ]);

                         $newreturn['pnrno_return']=$PNR_return;
             
                        
                    }
                    else
                    {
                         $newreturn['error']="Error Ticket";
                      
                    }
            }

        } // return flight close
        echo json_encode($newreturn);
      
    }

     public function apiflight_invoice(Request $request)
    {
        
      
        $pnrno=$request->get('pnrno');
        $pnrno_return=$request->get('pnrno_return');
        $flightbook= DB::table('tbl_flight_book')->where('pnrno', $pnrno)->first();
        $flightemail = $flightbook->pemail;
        $flightbookdata=unserialize($flightbook->segment);
        if(count($flightbook) > 0)
        {

            if($pnrno_return !='')
            {
                $flightbook_return= DB::table('tbl_flight_book')->where('pnrno', $pnrno_return)->first();
                $twoway_segment=unserialize($flightbook_return->segment);
                 $twoarrray=array("flightcabinclass"=>$flightbook_return->flightcabinclass,
                            "journytype"=>$flightbook_return->journytype,
                            "flight_booking_date"=>$flightbook_return->flight_booking_date,
                            "pnrno"=>$flightbook_return->pnrno,
                            "bookingId"=>$flightbook_return->bookingId,
                            "orign"=>$flightbook_return->orign,
                            "destination"=>$flightbook_return->destination,
                            "pname"=>$flightbook_return->pname,
                            "pmobile"=>$flightbook_return->pmobile,
                            "pemail"=>$flightbook_return->pemail,
                            "pcity"=>$flightbook_return->pcity,
                            "adults"=>$flightbook_return->adults,
                            "child"=>$flightbook_return->child,
                            "infant"=>$flightbook_return->infant,
                            "invoice_amount"=>$flightbook_return->invoice_amount,
                            "invoice_no"=>$flightbook_return->invoice_no,
                            "mealname"=>$flightbook_return->mealname,
                            "mealprice"=>$flightbook_return->mealprice,
                            "bag_weight"=>$flightbook_return->bag_weight,
                            "bag_price"=>$flightbook_return->bag_price,
                            "booking_date"=>$flightbook_return->booking_date,
                            "booking_time"=>$flightbook_return->booking_time,
                            "flight_ticekt_id"=>$flightbook_return->flight_ticekt_id,
                            "flight_ticekt_no"=>$flightbook_return->flight_ticekt_no,
                          
                            

                            );
               $result['twoway_segment']=$flightbookdata;
            }
            else
            {
                $twoarrray=null;

                $twoway_segment=array();
            }
            $onearrray=array("flightcabinclass"=>$flightbook->flightcabinclass,
                            "journytype"=>$flightbook->journytype,
                            "flight_booking_date"=>$flightbook->flight_booking_date,
                            "pnrno"=>$flightbook->pnrno,
                            "bookingId"=>$flightbook->bookingId,
                            "orign"=>$flightbook->orign,
                            "destination"=>$flightbook->destination,
                            "pname"=>$flightbook->pname,
                            "pmobile"=>$flightbook->pmobile,
                            "pemail"=>$flightbook->pemail,
                            "pcity"=>$flightbook->pcity,
                            "adults"=>$flightbook->adults,
                            "child"=>$flightbook->child,
                            "infant"=>$flightbook->infant,
                            "invoice_amount"=>$flightbook->invoice_amount,
                            "invoice_no"=>$flightbook->invoice_no,
                            "mealname"=>$flightbook->mealname,
                            "mealprice"=>$flightbook->mealprice,
                            "bag_weight"=>$flightbook->bag_weight,
                            "bag_price"=>$flightbook->bag_price,
                            "booking_date"=>$flightbook->booking_date,
                            "booking_time"=>$flightbook->booking_time,
                            "flight_ticekt_id"=>$flightbook->flight_ticekt_id,
                            "flight_ticekt_no"=>$flightbook->flight_ticekt_no,
                           "grandprice"=>$flightbook->grandprice,
                           


                            );
            $result['oneway']=$onearrray;
            $result['twoway']=$twoarrray;

            $result['oneway_segment']=$flightbookdata;
            $result['twoway_segment']=$twoway_segment;
            // $result['flightbook_return']=$flightbook_return;
            // $result['flightbook']=$flightbook;
            
            
        }
        else
        {
            $result['error']="error";
        }
       echo json_encode($result);
    }
    public function flightdownloadpdf(Request $request)
    {
        $pnrno=$request->get('pnrno');
        $pnrno_return=$request->get('pnrno_return');
        $flightbook= DB::table('tbl_flight_book')->where('pnrno', $pnrno)->first();
        $flightemail = $flightbook->pemail;
      
        if(count($flightbook) > 0)
        {
            if($pnrno_return !='')
            {
                $flightbook_return= DB::table('tbl_flight_book')->where('pnrno', $pnrno_return)->first();
               
            }
            else
            {
                $flightbook_return=array();
            }
         
            
            
        }
        $pdf = PDF::loadView('pages.flight_pdf',compact('flightbook','flightbook_return'));
             return $pdf->stream('flightbooking.pdf');
       
    }
 public function price($amount,$taxs,$othervharges,$mainpri,$curr)
    {

        $pricecurrency = 'fa-inr';  //$ fa-usd
    $maincurrency=$curr;

        if($maincurrency == 'IN'){
            $priceamount = $amount;
            $taxamount=$taxs;
            $otherchar =$othervharges;
            $fmainprice=$mainpri;
            $pricecurr = 'IN';
        }else{
            if($maincurrency == 'AU' || $maincurrency == 'NZ'){
                // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'AUD')->first();
                // if(!empty($dataExsists)){
                //  //$price = Price::where('currency', '=', 'AUD')->first();
                //     $dataExsists->amount = $this->convert('AUD','1');
                //     $dataExsists->save();
                // }
                $price = Price::where('currency', '=', 'AUD')->first();
                $priceamount = ($price->amount*$amount)*100;
                $taxamount=($price->amount*$taxs)*100;
                $otherchar =($price->amount*$othervharges)*100;
                $fmainprice =($price->amount*$mainpri)*100;

                $pricecurr = 'AU';
            }else{
                // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'USD')->first();
                // if(!empty($dataExsists)){
                //  //$price = Price::where('currency', '=', 'USD')->first();
                //     $dataExsists->amount = $this->convert('USD','1');
                //     $dataExsists->save();
                // }
                $price = Price::where('currency', '=', 'USD')->first();
                //echo $price->amount; die;
                $priceamount = ($price->amount*$amount)*100;
                $taxamount=($price->amount*$taxs)*100;
                $otherchar =($price->amount*$othervharges)*100;
                $fmainprice =($price->amount*$mainpri)*100;
                $pricecurr = 'US';
            }
            $pricecurrency = 'fa-usd';
        }
        //echo $priceamount;  die;
        return [$priceamount,$pricecurrency,$taxamount,$otherchar,$pricecurr,$fmainprice];
    }
    //price controller
    public function pricenew($amount,$tax,$curr)
    {

        $pricecurrency = 'fa-inr';  //$ fa-usd
        // echo Cookie::get('country');
            $maincurrency=$curr;
        if($maincurrency == 'IN'){
            $priceamount = $amount;
            $taxamount=$tax;
        }else{
            if($maincurrency == 'AU' ){
             
                $price = Price::where('currency', '=', 'AUD')->first();
                $priceamount = ($price->amount*$amount)*100;
                $taxamount=($price->amount*$tax)*100;
            }else{
               
                $price = Price::where('currency', '=', 'USD')->first();
                //echo $price->amount; die;
                $priceamount = ($price->amount*$amount)*100;
                $taxamount=($price->amount*$tax)*100;
            }
            $pricecurrency = 'fa-usd';
        }
        //echo $priceamount;  die;
        return [$priceamount,$pricecurrency,$taxamount];
    }
     public function mealprice($amount,$mainamount,$curr)
    {

        $pricecurrency = 'fa-inr';  //$ fa-usd
            $maincurrency=$curr;

        if($maincurrency == 'IN'){
            $priceamount = $amount;
            $mainpriceamount=$mainamount;

            $pricecurr = 'IN';
        }else{
            if($maincurrency == 'AU' || $maincurrency == 'NZ'){
                // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'AUD')->first();
                // if(!empty($dataExsists)){
                //  //$price = Price::where('currency', '=', 'AUD')->first();
                //     $dataExsists->amount = $this->convert('AUD','1');
                //     $dataExsists->save();
                // }
                $price = Price::where('currency', '=', 'AUD')->first();
                $priceamount = ($price->amount*$amount)*100;
                $mainpriceamount = ($price->amount*$mainamount)*100;


                $pricecurr = 'AU';
            }else{
                // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'USD')->first();
                // if(!empty($dataExsists)){
                //  //$price = Price::where('currency', '=', 'USD')->first();
                //     $dataExsists->amount = $this->convert('USD','1');
                //     $dataExsists->save();
                // }
                $price = Price::where('currency', '=', 'USD')->first();
                //echo $price->amount; die;
                $priceamount = ($price->amount*$amount)*100;
                $mainpriceamount = ($price->amount*$mainamount)*100;


                $pricecurr = 'US';
            }
            $pricecurrency = 'fa-usd';
        }
        //echo $priceamount;  die;
        return [$priceamount,$pricecurrency,$mainpriceamount];
    }
   public function get_current_location(Request $request){
        //return "USD";
        if(Cookie::get('country')!== null){
            return Cookie::get('country');
         }else
         {
             $ip=$request->get('ip');
            $url = "http://www.geoplugin.net/php.gp?ip=".$ip;
             $request = curl_init();
             $timeOut = 0;
             curl_setopt ($request, CURLOPT_URL, $url);
             curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
             curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
             curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
             $response = curl_exec($request);

             curl_close($request);
             if(!empty($response)){
                 $response = unserialize($response); 
                 if(!empty($response['geoplugin_status']) && $response['geoplugin_status'] == '200')
                 {
                     Cookie::queue('country',$response['geoplugin_countryCode'], 1440);

                 return $response['geoplugin_countryCode'];

                  echo json_encode($response);
                 }
                 else{
                     Cookie::queue('country','IN', 1440);
                     return "  ";
                 }
             }
             else{
                 Cookie::queue('country','IN', 1440);
                 return "IN";
             }
         }
         
         
        //$loc = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
        //print_r($response); die;
    }

    public function api_register(Request $request)
	{
	    $fullname = trim($request->get('fullname'));
	    if (strpos($fullname, ' ') !== false) {
		   $chkname = explode(" ",$fullname,2);
		   $fname=$chkname[0];
		   $lname=$chkname[1];
		}
		else
		{
			$fname=$fullname;
		   $lname="";
		}
		$email=$request->get('email_address');
		$phone=$request->get('phone');
		$password=$request->get('pass');
		$address="";
		$username=strtolower($fname.substr($lname,0,3).mt_rand(1000,9999));
		$password_secure=md5($password);

		$create_date=date('Y-m-d');
		$create_time=date('H:i:s');
		$checklogin=DB::table('tbl_user')->where('user_email',$email)->first();
		if(count($checklogin)>0)
		{
				echo "exist";
		}
		else
		{
			$insertdata=array(
			"user_fname"=>$fname,
			"user_lname"=>$lname,
			"user_phone"=>$phone,
			"user_email"=>$email,
			"user_address"=>$address,
			"user_name"=>$username,
			"user_password"=>$password_secure,
			"user_hint_password"=>$password,
			"user_status"=>1,
			"user_create_date"=>$create_date,
			"user_create_time"=>$create_time);
			$insert=DB::table('tbl_user')->insert($insertdata);
			if($insert)
			{
				$fullname=$fname." ".$lname;
				// session(['name'=>$fullname,"username"=>$username]);
				// session(['travo_name'=>$fullname,"travo_useremail"=>$email,"travo_phone"=>$phone,"travo_address"=>$address]);
				
				echo "Done -".$fullname;
			}
			else
			{
				echo "Fail";
			}
		}
	}
	
	public function api_mytrip(Request $request)
	{
		
		if(session()->has('travo_userid') || session()->has('travo_username'))
		{
			$useremail=session()->get('travo_useremail');
			$showdata=tbl_mytrip::where('login_guest_email',$useremail)->orderBy('id', 'DESC')->get();
			
			$whole_trip_data=array();
			if(count($showdata)>=1)
			{
				$hotelname='';
				$hotel_star='';
				$check_in='';
				$check_out='';
				$hotel_star='';
				$hotel_booking_status='';
				$pnrno='';
				$orign='';
				$destination='';
				$ticketid='';
				$ticketrfund='';
				foreach($showdata as $data)
				{
					 $bookingidnew= $data->bookingid;
					  // $results = DB::select('select * from tbl_hotel_book where bookingid = "'.$bookingidnew.'" ');
					  if($data->triptype=='Hotel')
					  {
						  	$showdata_hotel=tbl_hotel_book::where('login_guest_email',$useremail)->where('bookingid',$bookingidnew)->first();
						  	$hotelname = $showdata_hotel->hotel_name;
						  	$hotel_star=$showdata_hotel->hotel_star;
						  	$check_in = $showdata_hotel->check_in_date;
						  	$check_out=$showdata_hotel->check_out_date;
						  	
						  	$hotel_booking_status = $showdata_hotel->hotel_booking_status;
					  	
					 	}
					 	if($data->triptype=='Flight')
						  {
							  	$showdata_flight=tbl_flight_book::where('login_guest_email',$useremail)->where('bookingId',$bookingidnew)->first();
							  	$pnrno = $showdata_flight->pnrno;
							  	$orign=$showdata_flight->orign;
							  	$destination = $showdata_flight->destination;
							  	$ticketid=$showdata_flight->flight_ticekt_id;
							  	$ticketrfund=$showdata_flight->refund;
							}
					
										$whole_trip_data[]=array('bookingid'=>$data->bookingid,
										'hotelname'=>$hotelname,
										'hotel_star'=>$hotel_star,
										"check_in"=>$check_in,
										"check_out"=>$check_out,
										"hotel_booking_status"=>$hotel_booking_status,
										
										"pnrno"=>$pnrno,
										"orign"=>$orign,
										"destination"=>$destination,
										"bookingdate"=>$data->booking_date,
										"bookingtime"=>$data->booking_time,
										"triptype"=>$data->triptype,
										"canceltrip"=>$data->cancelstatus,
										"change_request_id"=>$data->change_request_id,
										"ticketid"=>$ticketid,
										"ticketrfund"=>$ticketrfund,
										
									);
					
					  // return view('pages.mytrip')->with(compact('whole_trip_data'));
								

				}
				$result['whole_trip_data']=$whole_trip_data;
				
				// return view('pages.mytrip')->with(compact('whole_trip_data'));
			}
			       $result['whole_trip_data']=$whole_trip_data;
// 			return view('pages.mytrip')->with(compact('whole_trip_data'));

		}
		else
		{
		    $result['error']="No Data Found";
			return redirect()->intended('/index');
		}
		echo json_encode($whole_trip_data);
	}

} //main file
