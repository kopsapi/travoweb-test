<?php

namespace App\Http\Controllers;
use DB;
use Session;
use App\tbl_hotel_book;
use Illuminate\Http\Request;
use App\tbl_flight_book;
use App\tbl_mytrip;
use App\userlist;
use Socialite;
use Exception;
use App\tbl_query;
class LoginController extends Controller
{

	public function __construct()
	{
		date_default_timezone_set('Asia/Kolkata');
	}

	 //facebooklogin
     public function socialLogin($social)
   {
   	
       return Socialite::driver($social)->redirect();
   }
   public function handleProviderCallback($social)
   {
 
       $userSocial = Socialite::driver($social)->user();
      
       $existUser = userlist::where('user_email',$userSocial->email)->first();
       
       if($existUser)
       {
         $fullname=$existUser->user_fname." ".$existUser->user_lname;
           session(['travo_userid'=>$existUser->user_id,'travo_name'=>$fullname,"travo_useremail"=>$existUser->user_email,"travo_phone"=>$existUser->user_phone,"travo_address"=>$existUser->user_address,"travo_title"=>$existUser->user_title]);
           return redirect()->to('/index');
		}
       else
       {
         
                $user = new userlist;
                $user->user_fname = $userSocial->name;
                $user->user_email = $userSocial->email;
                $user->facebookid = $userSocial->id;
                $user->logintype='Facebook';
                $user->user_create_date=date('Y-m-d');
				$user->user_create_time=date('H:i:s');

                $user->save();
                session(['travo_name'=>$userSocial->name,"travo_useremail"=>$userSocial->email]);
                return redirect()->to('/index');
       }
   }
   //endfacebook
   //google login
   public function redirect()
    {
    	 
        return Socialite::driver('google')->stateless()->redirect();
    }


    public function callback()
    {
    	
    	
        try {
            $googleUser = Socialite::driver('google')->stateless()->user();
           
            $existUser = userlist::where('user_email',$googleUser->email)->first();
            

            if($existUser) {
            	$fullname=$existUser->user_fname." ".$existUser->user_lname;
            	session(['travo_userid'=>$existUser->user_id,'travo_name'=>$fullname,"travo_useremail"=>$existUser->user_email,"travo_phone"=>$existUser->user_phone,"travo_address"=>$existUser->user_address,"travo_title"=>$existUser->user_title]);

                // Auth::loginUsingId($existUser->id);
            }
            else {
            	echo "test";
                $user = new userlist;
                $user->user_fname = $googleUser->name;
                $user->user_email = $googleUser->email;
                $user->googleid = $googleUser->id;
                $user->logintype='Gmail';
                $user->user_create_date=date('Y-m-d');
				$user->user_create_time=date('H:i:s');
                $user->save();
                session(['travo_name'=>$googleUser->name,"travo_useremail"=>$googleUser->email]);
                // Auth::loginUsingId($user->id);
            }
            return redirect()->to('/index');
        } 
        catch (Exception $e) {
            return 'error';
        }
    }
   //endgoogle login
	public function register(Request $request)
	{
		$fullname = trim($request->get('fullname'));
		
		
		if (strpos($fullname, ' ') !== false) {
		   $chkname = explode(" ",$fullname,2);
		   $fname=$chkname[0];
		   $lname=$chkname[1];
		}
		else
		{
			$fname=$fullname;
		   $lname="";
		}
		// $fname=strtoupper($request->get('firstname'));
		// $lname=strtoupper($request->get('lastname'));
		$email=$request->get('email_address');
		$phone=$request->get('phone');
		$password=$request->get('pass');
		$address="";
		$username=strtolower($fname.substr($lname,0,3).mt_rand(1000,9999));
		$password_secure=md5($password);

		$create_date=date('Y-m-d');
		$create_time=date('H:i:s');
		$checklogin=DB::table('tbl_user')->where('user_email',$email)->first();
		if(count($checklogin)>0)
		{
				echo "exist";
		}
		else
		{
			$insertdata=array(
			"user_fname"=>$fname,
			"user_lname"=>$lname,
			"user_phone"=>$phone,
			"user_email"=>$email,
			"user_address"=>$address,
			"user_name"=>$username,
			"user_password"=>$password_secure,
			"user_hint_password"=>$password,
			"user_status"=>1,
			"user_create_date"=>$create_date,
			"user_create_time"=>$create_time);
			$insert=DB::table('tbl_user')->insert($insertdata);
			if($insert)
			{
				$fullname=$fname." ".$lname;
				// session(['name'=>$fullname,"username"=>$username]);
				session(['travo_name'=>$fullname,"travo_useremail"=>$email,"travo_phone"=>$phone,"travo_address"=>$address]);
				
				echo "Done -".$fullname;
			}
			else
			{
				echo "Fail";
			}
		}
		
	}
	public function checkuser(Request $request)
	{
		
		$email=$request->get('login_email');
		$password=$request->get('login_password');

		$checkdata=DB::table('tbl_user')->where('user_email',$email)->where('user_password',md5($password))->first();
		if(count($checkdata)>0)
		{
			$user_id=$checkdata->user_id;
			$fname1=$checkdata->user_fname;
			$lname1=$checkdata->user_lname;
			$username1=$checkdata->user_name;
			$email=$checkdata->user_email;
			$phone=$checkdata->user_phone;
			$address=$checkdata->user_address;
			$fullname=$fname1." ".$lname1;
			session(['travo_userid'=>$user_id,'travo_name'=>$fullname,"travo_username"=>$username1,"travo_useremail"=>$email,"travo_phone"=>$phone,"travo_address"=>$address,"travo_title"=>$checkdata->user_title]);
			
			echo "Done -".$fullname;
		}
		else
		{
			echo "Fail";
		}

	}
	//guestuser
	public function guestuser(Request $request)
	{
		
		$email=$request->get('guest_email');
		$guest_phone=$request->get('guest_phone');
		$checklogin=DB::table('tbl_user')->where('user_email',$email)->first();
		if(count($checklogin)>0)
		{
				echo "exist";
		}
		else
		{
			$fullname='Guest';
			session(['travo_userphone'=>$guest_phone,"travo_useremail"=>$email]);
			echo "Done -".$email;
		}
		
	
		

	}

	public function mytrip(Request $request)
	{
		
		if(session()->has('travo_userid') || session()->has('travo_username'))
		{
			$useremail=session()->get('travo_useremail');
			$showdata=tbl_mytrip::where('login_guest_email',$useremail)->orderBy('id', 'DESC')->get();
			
			$whole_trip_data=array();
			if(count($showdata)>=1)
			{
				$hotelname='';
				$hotel_star='';
				$check_in='';
				$check_out='';
				$hotel_star='';
				$hotel_booking_status='';
				$pnrno='';
				$orign='';
				$destination='';
				$ticketid='';
				$ticketrfund='';
				foreach($showdata as $data)
				{
					 $bookingidnew= $data->bookingid;
					  // $results = DB::select('select * from tbl_hotel_book where bookingid = "'.$bookingidnew.'" ');
					  if($data->triptype=='Hotel')
					  {
						  	$showdata_hotel=tbl_hotel_book::where('login_guest_email',$useremail)->where('bookingid',$bookingidnew)->first();
						  	$hotelname = $showdata_hotel->hotel_name;
						  	$hotel_star=$showdata_hotel->hotel_star;
						  	$check_in = $showdata_hotel->check_in_date;
						  	$check_out=$showdata_hotel->check_out_date;
						  	
						  	$hotel_booking_status = $showdata_hotel->hotel_booking_status;
					  	
					 	}
					 	if($data->triptype=='Flight')
						  {
							  	$showdata_flight=tbl_flight_book::where('login_guest_email',$useremail)->where('bookingId',$bookingidnew)->first();
							  	$pnrno = $showdata_flight->pnrno;
							  	$orign=$showdata_flight->orign;
							  	$destination = $showdata_flight->destination;
							  	$ticketid=$showdata_flight->flight_ticekt_id;
							  	$ticketrfund=$showdata_flight->refund;
							}
					
										$whole_trip_data[]=array('bookingid'=>$data->bookingid,
										'hotelname'=>$hotelname,
										'hotel_star'=>$hotel_star,
										"check_in"=>$check_in,
										"check_out"=>$check_out,
										"hotel_booking_status"=>$hotel_booking_status,
										
										"pnrno"=>$pnrno,
										"orign"=>$orign,
										"destination"=>$destination,
										"bookingdate"=>$data->booking_date,
										"bookingtime"=>$data->booking_time,
										"triptype"=>$data->triptype,
										"canceltrip"=>$data->cancelstatus,
										"change_request_id"=>$data->change_request_id,
										"ticketid"=>$ticketid,
										"ticketrfund"=>$ticketrfund,
										
									);
					
					  // return view('pages.mytrip')->with(compact('whole_trip_data'));
								

				}
				
				return view('pages.mytrip')->with(compact('whole_trip_data'));
			}
			return view('pages.mytrip')->with(compact('whole_trip_data'));

		}
		else
		{
			return redirect()->intended('/index');
		}
	}
	public function adminforgotpassword_check(Request $request)
	{
		 $email = $request->get('forgotemail');

       	$exist= DB::table('tbl_user')->where('user_email', $email)->first();
       	if(count($exist) > 0)
   		{
   			$user_id = $exist->user_id;
   			$user_ids = base64_encode(base64_encode($user_id));
   			$to=$email;
            $subject="Reset Password";
            $message="Dear ".$exist->user_fname." ".$exist->user_lname." <br>Your Password Reset Link Please <a href='".url('/')."/forgotpassword?id=".$user_ids."' target='_blank'>click here</a>";
            $header="From:ticket@travoweb.com \r \n";
            $header = "From: " . strip_tags('ticket@travoweb.com') . "\r\n";
            $header .= "Reply-To: ". strip_tags('ticket@travoweb.com') . "\r\n";
            $header .= "CC: 'ticket@travoweb.com'\r\n";
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-Type: text/html; charset=UTF-8\r\n";
            $mailtest =mail($to,$subject,$message,$header);
            if($mailtest== true)
            {
               echo "success";
            }
            else
            {
                echo "fail";
            }
   		}
   		else
   		{
   			echo "invalid Email";
   		}

	}
	//adminresetpassword
	public function adminresetpassword_check(Request $request)
	{
		echo "test";
		echo $user_id = base64_decode(base64_decode($request->get('id')));

    	  $password=md5($request->get('password'));
    	$exist= DB::table('tbl_user')->where('user_id', $user_id)->first();
        if(count($exist) > 0)
        {
        	$updateDetails=array(
            'user_password'=>$password,
            'user_hint_password'=>$request->get('password'),
            );
            $sql= DB::table('tbl_user')->where('user_id', $user_id)->update($updateDetails);
           echo "success";
        }
        else
        {
        	echo "fail";
        }
	}

	//endadminresetpassword 
	//profile
	public function profile(Request $request)
	{
		if(session()->has('travo_userid') || session()->has('travo_username'))
		{
			$useremail=session()->get('travo_useremail');
			$exist= DB::table('tbl_user')->where('user_email', $useremail)->first();
	       	if(count($exist) > 0)
	   		{
	   			return view('pages.profile')->with(compact('exist'));
	   		}
		}
		else
		{
			return redirect()->intended('/index');
		}
		
		// return redirect('pages.profile');
	}
	//endprofile
	//updateprofile
	public function userdetailupdate(Request $request)
	{
		$useremail=session()->get('travo_useremail');
		$user_title = $request->get('user_title');
		$user_firstname = $request->get('user_firstname');
		$user_lastname = $request->get('user_lastname');
		$user_gender = $request->get('user_gender');
		$user_code = $request->get('user_code');
		$user_phone = $request->get('user_phone');
		$user_dobday = $request->get('user_dobday');
		$user_dobmonth = $request->get('user_dobmonth');
		$user_dobyear = $request->get('user_dobyear');

		$user_address = $request->get('user_address');
		$user_city = $request->get('user_city');
		$user_pincode = $request->get('user_pincode');
		$user_country = $request->get('user_country');
		$org_img=$request->get('org_img');
		$user_dob=$user_dobyear.'-'.$user_dobmonth.'-'.$user_dobday;
		$updatedetail = userlist::where('user_email', $useremail)->update(
			['user_title' => $user_title,
			'user_fname' => $user_firstname,
			'user_lname' => $user_lastname,
			'user_gender' => $user_gender,
			'user_phone_code' => $user_code,
			'user_phone' => $user_phone,
			'user_dob' => $user_dob,
			'user_address' => $user_address,

			'user_city' => $user_city,
			'user_pincode' => $user_pincode,
			'user_county' => $user_country,
			'user_image' =>$org_img,
			]);
		if($updatedetail)
		{
			echo "sucess";
		}
		else
		{
			echo "fail";
		}
	}
	//endupdateprofile
	//imageupload
	public function adminimage_upload(Request $request)
    {
    	if($request->hasFile('imageupoload'))
		{
			$extension = $request->file('imageupoload')->getClientOriginalExtension();
 			  $extensions=strtolower($extension);
			if($extensions=="png" || $extensions=="jpg" || $extensions=="jpeg")
			{
			$imageName = time().'.'.$request->file('imageupoload')->getClientOriginalExtension();

			// request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
			$dir = 'assets/profile_image/';

			$request->file('imageupoload')->move($dir, $imageName);
			echo $imageName;
			}
			else
			{
			$imageName = "no";
			}
		}
		else
		{
		$imageName = "no";
		}
    }
	//endimage upload
	//oldlgincheck
	public function oldpasswordcheck(Request $request)
	{
		if(session()->has('travo_userid') || session()->has('travo_username'))
		{
			$useremail=session()->get('travo_useremail');
			$oldpassword = $request->get('oldpassword');
			$exist= DB::table('tbl_user')->where('user_email', $useremail)->where('user_hint_password', $oldpassword)->first();
	       	if(count($exist) > 0)
	   		{
	   			echo "success";
	   		}
	   		else
	   		{
	   			echo "incorrect";
	   		}

		}
		else
		{
			echo "fail";
		}
	}
	public function usernewpassword(Request $request)
	{
		$useremail=session()->get('travo_useremail');
		$password=md5($request->get('newpassword'));
    	$exist= DB::table('tbl_user')->where('user_email', $useremail)->first();
        if(count($exist) > 0)
        {
        	$updateDetails=array(
            'user_password'=>$password,
            'user_hint_password'=>$request->get('newpassword'),
            );
            $sql= DB::table('tbl_user')->where('user_email', $useremail)->update($updateDetails);
           echo "success";
        }
        else
        {
        	echo "fail";
        }
	}
	//endoldlogin

	public function logout()
	{
		session()->forget('travo_userid');
		session()->forget('travo_name');
		session()->forget('travo_username');
		session()->forget('travo_useremail');
		return redirect('index');
	}

	public function query_insert(Request $request)
	{
		$contact_name=$request->get('contact_name');
		$contact_email=$request->get('contact_email');
		$contact_phone=$request->get('contact_phone');
		$contact_subject=$request->get('contact_subject');
		$contact_message=$request->get('contact_message');
		$pagename=$request->get('pagename');
		$create_date=date('Y-m-d');
		$create_time=date('H:i:s');
		$data_query=array("contact_name"=>$contact_name,
						"contact_email"=>$contact_email,
						"contact_phone"=>$contact_phone,
						"contact_subject"=>$contact_subject,
						"contact_message"=>$contact_message,
						"create_date"=>$create_date,
						"create_time"=>$create_time,
						"page"=>$pagename,

						);
		$newd=tbl_query::insert($data_query);
		if($newd)
		{
			$to=$contact_email;
            $subject="Travo Web Query";
            $message="Dear ".$contact_name." <br>We have received your message and would like to thank you for writing to us. If your inquiry is urgent, please use the telephone number listed below to talk to one of our staff members. Otherwise, we will reply by email as soon as possible.<br><br>

Talk to you soon,
<br>

Travo Web <br>
+61 25924 0804";
            $header="From:admin@travoweb.com \r \n";
            $header = "From: " . strip_tags('admin@travoweb.com') . "\r\n";
            $header .= "Reply-To: ". strip_tags('admin@travoweb.com') . "\r\n";
            $header .= "CC: 'admin@travoweb.com'\r\n";
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-Type: text/html; charset=UTF-8\r\n";
            $mailtest =mail($to,$subject,$message,$header);
            if($mailtest== true)
            {
               echo "success";
            }
            else
            {
                echo "fail";
            }
		}


	}
}
