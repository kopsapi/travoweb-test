<?php
namespace App\Http\Controllers;

use session;
use DB;
use Cookie;
use App\hotel_city;
use App\tbl_hotel_book;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\tbl_mytrip;
use PDF;
use App;
use Dompdf\Dompdf;
use App\tbl_payment_order;
use File;
use Mail;
use App\Mail\SendMailable;
use App\Price;
use \Stripe;
// use Stripe;
// use Stripe\Stripe;
class HotelController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Kolkata');

        // $this->get_current_location();

        // if(session()->get('country1')!== null)
        // {
        //     return session()->get('country1');
        //  }
        //  else
        //  {
        //  $loc = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));

        //      Cookie::queue('country1',$loc['geoplugin_countryCode'], 1440);


        //  }

        // echo unserialize($chk);
        // echo "<pre>"; print_r(Cookie::get());

    }
    public function getcities(Request $request)
    {
        $this->get_current_location();
        $citykeyword=$request->get('citykeyword');
        $citieslist=hotel_city::where('Destination','LIKE',$citykeyword."%")->limit(7)->get();
        $data='';
        foreach($citieslist as $list)
        {
            $data.='<option data-value="'.$list->cityid.'" value="'.$list->Destination.' ('.$list->country.') ">';

        }

        if($data=='')
        {
            $data="No cities found";
        }

        echo $data;

    }

    public function gethotels(Request $request)
    {
        $CheckInDate1=$request->get('check_in');
        $check_out1=$request->get('check_out');
        $detinnn = explode(' (',$request->get('destination'));

        echo $modal_show ='
                     <!doctype html>
                       <html lang="en">
                       <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                      
                            
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">  
        <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">      
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <style>
                  p.g-fl:after {
    position: relative;
    content: "\\f594";
    font-family: "Font Awesome 5 Free";
    font-weight: 900;
    color: #FF904B;
    left: 16%;
    transform: translateX(-50%);
    bottom: 0px;
    font-size: 16px;
}
        .l-img{
            width: 165px;
            display: block;
            margin: auto;
        }
        .s-note,p.request-p {
    text-align: center;
    color: #381967;
    padding-bottom: 10px;
    font-size: 20px;
    font-weight: 500;
    font-family: calibri;
}
        .c-in1 {
            background: #381967;
            text-align: center;
            color: white;
            font-size: 18px;
            font-weight: 500;
        }

        .loadernew {
            font-size: 8px;
            margin: 0px auto 30px auto;
            width: 11em;
            height: 11em;
            border-radius: 50%;
            background: #ffffff;
            border-top: 6px solid #fa9e1b;
            border-left: 6px solid #fa9e1b;
            border-right: 6px solid #fa9e1b;
            position: relative;
            animation: load3 1.4s linear infinite;
        }



        @keyframes load3 {
            0% {

                transform: rotate(0deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;

            }
            10%{
                transform: rotate(36deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            20%{
                transform: rotate(72deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            30%{
                transform: rotate(108deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            40% {
                transform: rotate(144deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            50% {

                transform: rotate(180deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;

            }
            60%{
                transform: rotate(216deg);

                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            70%{
                transform: rotate(252deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            80%{
                transform: rotate(288deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            90% {

                transform: rotate(324deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            100% {

                transform: rotate(360deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
        }
        body{
            margin: 0;
            background: transparent;
        }


        div.modal-content.lc{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
       /* p.g-fl:after {
            position: relative;
            content: "\\f072";
            font-family: FontAwesome;
            font-weight: 900;
            color: #FF904B;
            left: 47%;
            transform: rotate(45deg) translateX(-50%);
            !* top: -18.3%; *!
            bottom: -3px;
            font-size: 29px;
        }*/
     img.fl-img {
    height: 31.1px;
    position: absolute;
    top: 44% !important;
    left: 48%;
}
        .multi-div{
            overflow: visible !important;
        }
        
        @media screen and (max-width:992px){
.modal-content.lc {
    width: 43% !important;
}
}

@media screen and (max-width:775px){
.modal-content.lc {
    width: 50% !important;
}
} 
@media screen and (max-width: 650px){
            .modal-content.lc {
    padding-top: 50px !important;
    width: 90% !important;
     box-shadow: none !important;
}
         
        } 
    </style>
                       </head>
                       <body>
     <div class="modal fade myModal" id="myModal1" style="height: 100%;font-family: \'Open Sans\', sans-serif;background: aliceblue; margin: 0;box-sizing: border-box;">

                            <div class="modal-dialog " style="">
                            <div class="modal-content lc" style="background:white;padding-top:60px;width:32%; height:auto;margin:0 auto;border:none;border-radius:7px;box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.2)">
                                <div class="modal-header" style="border: none;padding-bottom: 0">
                                      <img src="'.asset('assets/images/logo.png').'" class="l-img">
                                 </div>
                                  <div class="modal-body">
								    <p class="request-p">Please Do Not Close Or Refresh The Window While We Are Searching The Best Hotels For You</p>
                                    <!--<h3 class="s-note">We Are Searching Hotels in '.$detinnn[0].'</h3>-->
                                    <div class="row" style="width:100%">
                                        <div class="c-in1" style="width:50%;display:block;float:left;">

                                            <p style="margin: 0; padding: 11px;font-size: 16px;font-weight: 500;" class="g-fl">Check In '.$CheckInDate1.'</p>
                                        </div>
                                        <div class="c-in1" style="width:50%;display:block;float:left;margin-left: -1px;">
                                            <p style="margin: 0; padding: 11px;font-size: 16px;font-weight: 500;">Check Out '.$check_out1.'</p>
                                        </div>
                                    </div>

                                     <p style="font-size: 17px;font-weight: 600;padding-top: 60px;text-align: center; margin-top: 20px;" class="p-wait">Please Wait...</p>


                                </div>
                                <div class="loadernew"></div>
                            </div>
                                  
                            </div>
                          </div>
                                 <script>
                $(document).ready(function(){
                  
                  $("#myModal1").modal("show");
                  });
            </script>
                         </body>
                       </html>';

        $NoOfAdults1=0;
        $NoOfChild1=0;
        $NoOfAdultsarray=array();
        $NoOfchildarray=array();
        $childagearray=array();

        $TokenId=Cookie::get('TokenId');
        $EndUserIp=Cookie::get('localip');
        $destination= $request->get('destination');
        $CheckInDate=$request->get('check_in');
        $newchkdate = explode('/',$request->get('check_in'));
        $check_in_format=$newchkdate[2].'-'.$newchkdate[1].'-'.$newchkdate[0];
        $check_out=$request->get('check_out');
        $newchkdateout = explode('/',$request->get('check_out'));
        $check_out_format=$newchkdateout[2].'-'.$newchkdateout[1].'-'.$newchkdateout[0];
        $check_in_day = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $check_in_format.' 0:00:00');
        $check_out_day = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $check_out_format.' 0:00:00');
        $NoOfNights = $check_in_day->diffInDays($check_out_day);
        // $NoOfRooms= $request->get('rooms');
        // $NoOfAdults=$request->get('adults');
        // $NoOfChild=$request->get('children');
        $ChildAge='';
        $PreferredHotel='';
        $MaxRating ='5';
        $MinRating='0';
        $ReviewScore='0';
        $IsNearBySearchAllowed='false';

        $deti = explode(' (',$request->get('destination'));
        if(!empty($deti))
        {
        	$destination=$deti[0];
        }
        else
        {
        	$request->get('destination');
        }
       	if(!empty($deti[1]))
       	{
       		$destination_country=str_replace(')','',$deti[1]);
       	}
       	else
       	{
       		$destination_country='India';
       	}

        
        $destilist=hotel_city::where('Destination',$destination)->where('country',$destination_country)->first();
        if(!empty($destilist))
        {
        	$CityId =  $destilist->cityid;
	        $CountryCode =  $destilist->countrycode;
	        $statename= $destilist->stateprovince;
	        $countryname= $destilist->country;
	        $ResultCount ='0';
	        $PreferredCurrency="INR";
	        $GuestNationality=$destilist->countrycode;
        }
        else
        {
        	echo "<script>alert('Please Check Destination')</script>";
        	 return redirect()->intended('/index');
        }
        
        $NoOfRooms= $request->get('newroom');
        $roomguests_array=array();
        for($st=1;$st<=$NoOfRooms;$st++)
        {

            $roomadult = $request->get('roomadult-'.$st);
            $children = $request->get('children-'.$st);
            $childage = $request->get('childage-'.$st);
            if(!empty($childage))
            {
                $ChildAge1=implode(',',$childage);
                $ChildAge=explode(',',$ChildAge1);
            }
            else
            {
                $ChildAge="";
            }


            // print_r($ChildAge1);
            $roomguests_array[]=array('NoOfAdults' =>$roomadult,
                'NoOfChild' =>$children,
                'ChildAge' =>$ChildAge);

            $NoOfAdults1+=$roomadult;
            $NoOfChild1+=$children;
            $NoOfAdultsarray[]=$roomadult;
            $NoOfchildarray[]=$children;
            $childagearray[]=$childage;
            // echo count($roomadult);
        }

        $form_data = array(
            'CheckInDate'=>$CheckInDate,
            'NoOfNights'=>$NoOfNights,
            'CountryCode' => $CountryCode,
            'CityId' =>$CityId,
            'ResultCount'=>$ResultCount,
            'PreferredCurrency' => $PreferredCurrency,
            'GuestNationality' =>$GuestNationality,
            'NoOfRooms' =>$NoOfRooms,
            'RoomGuests' =>$roomguests_array,
            'PreferredHotel'=>$PreferredHotel,
            'MaxRating'=>$MaxRating,
            'MinRating'=>$MinRating,
            'ReviewScore'=>null,
            'IsNearBySearchAllowed'=>$IsNearBySearchAllowed,
            'EndUserIp'=>$EndUserIp,
            'TokenId'=>$TokenId,
        );


        $data_string = json_encode($form_data);
      
        // print_r($data_string);

        // for($i=0;$i<$NoOfRooms;$i++)
        // {

        // 	$roomguests_array[]=array('NoOfAdults' =>$NoOfAdults,
        // 		'NoOfChild' =>$NoOfChild,
        // 		'ChildAge' =>$ChildAge);
        // 	$NoOfAdults1+=$NoOfAdults;
        // 	$NoOfChild1+=$NoOfChild;
        // 	$NoOfAdultsarray[]=$NoOfAdults;
        // 	$NoOfchildarray[]=$NoOfChild;
        // }
        // $form_data = array(
        // 	'CheckInDate'=>$CheckInDate,
        // 	'NoOfNights'=>$NoOfNights,
        // 	'CountryCode' => $CountryCode,
        // 	'CityId' =>$CityId,
        // 	'ResultCount'=>$ResultCount,
        // 	'PreferredCurrency' => $PreferredCurrency,
        // 	'GuestNationality' =>$GuestNationality,
        // 	'NoOfRooms' =>$NoOfRooms,
        // 	'RoomGuests' =>$roomguests_array,
        // 	'PreferredHotel'=>$PreferredHotel,
        // 	'MaxRating'=>$MaxRating,
        // 	'MinRating'=>$MinRating,
        // 	'ReviewScore'=>null,
        // 	'IsNearBySearchAllowed'=>$IsNearBySearchAllowed,
        // 	'EndUserIp'=>$EndUserIp,
        // 	'TokenId'=>$TokenId,
        // );


        // $data_string = json_encode($form_data);

        $ch = curl_init('https://api.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/GetHotelResult/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);
       
        $result1=json_decode($result,true);
        // print_r($result1);
        // die();

        $hotelmargin1=Cookie::get('gensetting');

        $hotelmargin = $hotelmargin1->hotel_margin;

        $hotelarray = !empty($result1['HotelSearchResult'])?$result1['HotelSearchResult']:array();

        if(!empty($hotelarray))
        {
            if($hotelarray['Error']['ErrorCode']==0)
            {
                for($ht=0;$ht<count($hotelarray['HotelResults']);$ht++)
                {
                    $mainp = $hotelarray['HotelResults'][$ht]['Price']['PublishedPriceRoundedOff'];
                    $margin = ($mainp * $hotelmargin)/100;
                    $mainprice = $mainp + $margin;
                    $price= $this->price($mainprice,0,0,0,0);
                    $hotelmainprice = $price[0] ;
                    $hotelmaincurrecny = $price[1];
                    $hotelarray['HotelResults'][$ht]['hotelmainprice']=$hotelmainprice;
                    $hotelarray['HotelResults'][$ht]['hotelmaincurrecny']=$hotelmaincurrecny;
                    $hotelarray['HotelResults'][$ht]['hotelmargin']=$mainprice;
                    $hotelarray['HotelResults'][$ht]['hotellastprice']=$mainp;
                    $hotelarray['HotelResults'][$ht]['hotelconvercurr']=$price[2];




                }
            }
        }

        $TraceId =  $result1['HotelSearchResult']['TraceId'];
        $request->session()->put('hotel_traceid',$TraceId);

        $request->session()->put('resultarray',$hotelarray);

        $submitted_data=array("City"=>$destination,
            "State"=>$statename,
            "Country"=>$countryname,
            "checkin"=>$check_in_format,
            "checkout"=>$check_out_format,
            "adult_count"=>$NoOfAdults1,
            "child_count"=>$NoOfChild1,
            "no_of_rooms"=>$NoOfRooms,
            "guest_nationality"=>$GuestNationality,
            "preferred_currency"=>$PreferredCurrency,
            "adult_array"=>$NoOfAdultsarray,
            "child_array"=>$NoOfchildarray,
            "childagearray"=>$childagearray,

        );

        $request->session()->put('resultarray',$hotelarray);
        $request->session()->put('dataarray',$submitted_data);
        echo '<script>
                    $(document).ready(function(){
                      
                      $("#myModal1").modal("hide");
                      });
                </script>';

        // print_r($hotelarray);
        // die();
        // return view('pages.hotel-results')->with(compact('hotelarray'))->with(compact('hotelinfoarray'));

        return view('pages.hotel-results')->with(compact('hotelarray'))->with(compact('submitted_data'));


    }
    public function hotel_info(Request $request)
    {
        $hotelname=$request->get('hotelname');
        $hotelchkin=session()->get('hotelchkin');
        $hotelchkout=session()->get('hotelchkout');
        echo $modal_show ='
     <!doctype html>
                       <html lang="en">
                       <head>
                      
                              
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">   
                          <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">           
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
       
        <style>

p.g-fl:after {
    position: relative;
    content: "\\f594";
    font-family: "Font Awesome 5 Free";
    font-weight: 900;
    color: #FF904B;
    left: 16%;
    transform: translateX(-50%);
    bottom: 0px;
    font-size: 16px;
}
        .l-img{
            width: 165px;
            display: block;
            margin: auto;
        }
       .s-note,p.request-p {
    text-align: center;
    color: #381967;
    padding-bottom: 10px;
    font-size: 20px;
    font-weight: 500;
    font-family: calibri;
}
        .c-in1 {
            background: #381967;
            text-align: center;
            color: white;
            font-size: 18px;
            font-weight: 500;
        }

        .loadernew {
            font-size: 8px;
            margin: 0px auto 30px auto;
            width: 11em;
            height: 11em;
            border-radius: 50%;
            background: #ffffff;
            border-top: 6px solid #fa9e1b;
            border-left: 6px solid #fa9e1b;
            border-right: 6px solid #fa9e1b;
            position: relative;
            animation: load3 1.4s linear infinite;
        }



        @keyframes load3 {
            0% {

                transform: rotate(0deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;

            }
            10%{
                transform: rotate(36deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            20%{
                transform: rotate(72deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            30%{
                transform: rotate(108deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            40% {
                transform: rotate(144deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            50% {

                transform: rotate(180deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;

            }
            60%{
                transform: rotate(216deg);

                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            70%{
                transform: rotate(252deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            80%{
                transform: rotate(288deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            90% {

                transform: rotate(324deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            100% {

                transform: rotate(360deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
        }
        body{
            margin: 0;
            background: transparent;
        }


        div.modal-content.lc{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
       /* p.g-fl:after {
            position: relative;
            content: "\\f072";
            font-family: FontAwesome;
            font-weight: 900;
            color: #FF904B;
            left: 47%;
            transform: rotate(45deg) translateX(-50%);
            !* top: -18.3%; *!
            bottom: -3px;
            font-size: 29px;
        }*/
     img.fl-img {
    height: 31.1px;
    position: absolute;
    top: 44% !important;
    left: 48%;
}
        .multi-div{
            overflow: visible !important;
        }
        
        @media screen and (max-width:992px){
.modal-content.lc {
    width: 43% !important;
}
}

@media screen and (max-width:775px){
.modal-content.lc {
    width: 50% !important;
}
} 
@media screen and (max-width: 650px){
            .modal-content.lc {
    padding-top: 50px !important;
    width: 90% !important;
    box-shadow: none !important;
}

         
        } 
    </style>
                       </head>
                       <body>
      <div class="modal fade myModal" id="myModal1" style="font-family: \'Open Sans\', sans-serif; margin: 0;box-sizing: border-box;">

                           <div class="modal fade myModal" id="myModal1" style="height: 100%;font-family: \'Open Sans\', sans-serif;background: aliceblue; margin: 0;box-sizing: border-box;">

                            <div class="modal-dialog " style="">
                            <div class="modal-content lc" style="background:white;padding-top:60px;width:32%; height:auto;margin:0 auto;border:none;border-radius:7px;box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.2)">
                                <div class="modal-header" style="border: none;padding-bottom: 0">
                                      <img src="'.asset('assets/images/logo.png').'" class="l-img">
                                 </div>
                                  <div class="modal-body">
                                     <p class="request-p">Please Do Not Close Or Refresh The Window While We Are Searching The Best Hotels For You</p>
                                    <div class="row" style="width:100%">
                                        <div class="c-in1" style="width:50%;display:block;float:left;">

                                            <p style="margin: 0; padding: 11px;font-size: 16px;font-weight: 500;" class="g-fl">Check In '.$hotelchkin.'</p>
                                        </div>
                                        <div class="c-in1" style="width:50%;display:block;float:left;">
                                            <p style="margin: 0; padding: 11px;font-size: 16px;font-weight: 500;">Check Out '.$hotelchkout.'</p>
                                        </div>
                                    </div>

                                    <p style="font-size: 17px;font-weight: 600;padding-top: 60px;text-align: center; margin-top: 20px;" class="p-wait">Please Wait...</p>

                                </div>
                                <div class="loadernew"></div>
                            </div>
                                  
                            </div>
                          </div>
                      
            </div>
                <script>
                $(document).ready(function(){
                  
                  $("#myModal1").modal("show");
                  });
            </script>
            </body>
            </html>
            ';

        $hotelindex=$request->get('hotelindex');
        $hotelcode=$request->get('hotelcode');
        $hotelname=$request->get('hotelname');
        $traceid=session()->get('hotel_traceid');
        $TokenId=Cookie::get('TokenId');
        $EndUserIp=Cookie::get('localip');
        $form_data = array(
            'EndUserIp'=>$EndUserIp,
            'TokenId'=>$TokenId,
            'TraceId'=>$traceid,
            'ResultIndex'=>$hotelindex,
            'HotelCode'=>$hotelcode
        );

// Code for hotel info
        $data_string = json_encode($form_data);

        $ch = curl_init('https://api.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/GetHotelInfo/');


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result_info = curl_exec($ch);
        $result_info1=json_decode($result_info,true);

        $hotelinfoarray= !empty($result_info1['HotelInfoResult'])?$result_info1['HotelInfoResult']:array();

//End of Code for hotel info


// Code for hotel rooms
        $data_string1 = json_encode($form_data);
       
        $ch = curl_init('https://api.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/GetHotelRoom/');


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result_rooms = curl_exec($ch);
      
        $result_rooms1=json_decode($result_rooms,true);
        $hotelmargin1=Cookie::get('gensetting');

        $hotelmargin = $hotelmargin1->hotel_margin;
        $hotelroomsarray= !empty($result_rooms1['GetHotelRoomResult'])?$result_rooms1['GetHotelRoomResult']:array();

        if(!empty($hotelroomsarray))
        {
            if($hotelinfoarray['Error']['ErrorCode']==0)
            {
                for($hr=0;$hr<count($hotelroomsarray['HotelRoomsDetails']);$hr++)
                {
                    $mainp = $hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['RoomPrice'];

                    $ServiceTax = $hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['Tax']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['AgentCommission']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['AgentMarkUp']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['ServiceTax']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['TDS']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['TotalGSTAmount'];

                    $tdsa =$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['ExtraGuestCharge']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['ChildCharge']+$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['OtherCharges'];

                    $gsta = $hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['TotalGSTAmount'];


                    $margin = ($mainp * $hotelmargin)/100;
                    $mainprice = $mainp + $margin;
                    $price= $this->price($mainprice,$ServiceTax,$tdsa,$gsta,0);


                    $hotelmainprice = $price[0];
                    $hotelmaincurrecny = $price[1];


                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmainprice']=$hotelmainprice;
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmaincurrecny']=$hotelmaincurrecny;
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmargin']=$mainprice;
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotellastprice']=$mainp;
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelconvercurr']=$price[2];
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelservicetax']=$price[3];
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelothercharges']=$price[4];
                    $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelgst']=$price[5];

                }
            }
        }

        //put hotel rooms data into session to use in booking
        $request->session()->put('hotelroomsdata',$hotelroomsarray);


//End of Code for hotel rooms

//details for next api blockroom
        $bookingarray=array('hotelresultindex'=>$hotelindex,
            'hotelcode'=>$hotelcode,
            'hotelname'=>$hotelname);
        $request->session()->put('hotelbookname',$bookingarray['hotelname']);
//end of details for next api blockroom
        echo '<script>
                    $(document).ready(function(){
                      
                      $("#myModal1").modal("hide");
                      });
                </script>';
             
        return view('pages.hotel-info')->with(compact('hotelinfoarray'))->with(compact('hotelroomsarray'))->with(compact('bookingarray'));
    }




    //search hotels
    public function searchhotels(Request $request)
    {
        $stars=$request->get('stars');
        $price=$request->get('price');
        $hotelname=$request->get('hotelname');
        // $hotelname="Avtar";
        $star_arr=array();
        $price_arr=array();
        $hotel_arr=array();
        if($stars!='')
        {
            $stars_array=explode(',',$stars);
        }
        if($price!='')
        {
            $price_array=explode(',',$price);
        }
        if($hotelname !='')
        {
            $hotel_arr=explode(',',$hotelname);
        }

        $newresults=array();
        $data_frontend="";
        if(session()->has('resultarray'))
        {
            $results=session()->get('resultarray');

            for($res=0;$res<count($results['HotelResults']);$res++)
            {
                if($stars!='' && $price=='' && $hotelname=='')
                {
                    for($st=0;$st<count($stars_array);$st++)
                    {
                        if($results['HotelResults'][$res]['StarRating']==$stars_array[$st])
                        {
                            $newresults[]=$results['HotelResults'][$res];
                        }
                    }
                }

                if($stars=='' && $price!='' && $hotelname=='')
                {
                    for($pr=0;$pr<count($price_array);$pr++)
                    {
                        $price_data=explode('-',$price_array[$pr]);

                        if($results['HotelResults'][$res]['hotelmainprice']>=$price_data[0] && $results['HotelResults'][$res]['hotelmainprice']<=$price_data[1])
                        {
                            $newresults[]=$results['HotelResults'][$res];
                        }

                    }
                }
                if($hotelname!='' && $stars=='' && $price=='')
                {
                    for($st=0;$st<count($hotel_arr);$st++)
                    {
                        if($results['HotelResults'][$res]['HotelName']==$hotel_arr[$st])
                        {
                            $newresults[]=$results['HotelResults'][$res];
                        }
                    }
                }
                if($stars!='' &&  $price!='' &&  $hotelname !='')
                {
                    $check=0;
                    for($st=0;$st<count($stars_array);$st++)
                    {
                        if($results['HotelResults'][$res]['StarRating']==$stars_array[$st])
                        {
                            $check++;

                        }
                    }

                    for($pr=0;$pr<count($price_array);$pr++)
                    {
                        $price_data=explode('-',$price_array[$pr]);

                        if($results['HotelResults'][$res]['hotelmainprice']>=$price_data[0] && $results['HotelResults'][$res]['hotelmainprice']<=$price_data[1])
                        {
                            $check++;

                        }

                    }
                    for($st=0;$st<count($hotel_arr);$st++)
                    {
                        if($results['HotelResults'][$res]['HotelName']==$hotel_arr[$st])
                        {
                            $check++;

                        }
                    }

                    if($check==2)
                    {
                        $newresults[]=$results['HotelResults'][$res];
                    }

                }

                if($stars=='' && $price=='' && $hotelname=='' )
                {
                    $newresults[]=$results['HotelResults'][$res];
                }

            }


            for($ht=0;$ht<count($newresults);$ht++)
            {
                $data_frontend.='<form action="'.route('hotelinfo').'" method="post">
                <div class="hotel-result-list">
                <div class="row">
                <div class="col-md-3">
                <div class="hotel-img"> <img src="'.$newresults[$ht]['HotelPicture'].'" class="img-responsive"> </div>

                </div>
                <div class="col-md-6">
                <div class="hotel-result-content">
                <h4 class="hotel-title">'.$newresults[$ht]['HotelName'].'';
                for($star=1;$star<=$newresults[$ht]['StarRating'];$star++)
                {
                    $data_frontend.='<i class="fa fa-star"></i>&nbsp';
                }

                $data_frontend.='</h4> 
                <small class="loc">'.$newresults[$ht]['HotelAddress'].'</small>
                <br><br>
                
                <input type="hidden" name="_token" value="'.csrf_token().'">
                <input type="hidden" name="hotelindex" value="'.$newresults[$ht]['ResultIndex'].'">
                <input type="hidden" name="hotelcode" value="'.$newresults[$ht]['HotelCode'].'">
                <input type="hidden" name="hotelname" value="'.$newresults[$ht]['HotelName'].'">

                <button type="submit" class="btn btn-success">Get Details</button>
                </div>
                </div>
                <div class="col-md-3">
                <div class="hotel-result-price">
                
                <h4 class="price"><i class="fa '.$newresults[$ht]['hotelmaincurrecny'].'"></i> '.number_format($newresults[$ht]['hotelmainprice']).'</h4> <small>per night</small><br>
                <button type="submit" class="btn btn-success" style="margin-top: 8px;">Book</button>
                </div>
                </div>
                </div>
                </div>
                </form>';
            }
            if(count($newresults)<=0)
            {
                $data_frontend.='<br>
                <div class="container">
                <div class="row">
                <div class="col-lg-12">
                <div class="intro_content nano-content">
                <div class="no-flight">
                <h3>No Results Found</h3>

                </div>
                </div>
                </div>
                </div>
                </div>';
            }
            echo $data_frontend;
        }
        else
        {
            echo "no results";
        }
    }
    public function searchhotelname(Request $request)
    {
        $search=strtoupper($request->get('search'));

        $newresultsname=array();
        $htname="";
        if(session()->has('resultarray'))
        {
            $results=session()->get('resultarray');

            for($res=0;$res<count($results['HotelResults']);$res++)
            {
                $input = preg_quote($search, '~');
                $gtname= strtoupper($results['HotelResults'][$res]['HotelName']);
                $data = array($gtname);
                $result = preg_grep('~' . $input . '~', $data);
                // echo "<pre>";
                // print_r($search);
                // echo "</pre>";
                for($st=0;$st<count($result);$st++)
                {
                    if($gtname==$result[$st])
                    {
                        $newresultsname[]=$results['HotelResults'][$res];
                    }
                }
            }
            for($ht=0;$ht<count($newresultsname);$ht++)
            {
                $htname.='<label class="checkcontainer">'.$newresultsname[$ht]['HotelName'].'
                        <input type="checkbox" name="hotelname" value="'.$newresultsname[$ht]['HotelName'].'"> <span class="checkmark"></span> </label>';
            }
            echo $htname;
            // echo "<pre>";
            // print_r($newresultsname);
            // echo "</pre>";
        }

    }
    //end of search hotels

    public function hotelbook(Request $request)
    {
        $hotelname=$request->get('hotelname');
        $hotelchkin=session()->get('hotelchkin');
        $hotelchkout=session()->get('hotelchkout');
        $hotelroomtype=$request->get('hotelroomname');
        echo $modal_show ='
       <!doctype html>
                       <html lang="en">
                       <head>
                            
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                             <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">            
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
               
                <style>

        p.g-fl:after {
            position: relative;
            content: "\\f594";
            font-family: "Font Awesome 5 Free";
            font-weight: 900;
            color: #FF904B;
            left: 16%;
            transform: translateX(-50%);
            bottom: 0px;
            font-size: 16px;
        }
        .l-img{
            width: 165px;
            display: block;
            margin: auto;
        }
       .s-note,p.request-p {
    text-align: center;
    color: #381967;
    padding-bottom: 10px;
    font-size: 20px;
    font-weight: 500;
    font-family: calibri;
}
        .c-in1 {
            background: #381967;
            text-align: center;
            color: white;
            font-size: 18px;
            font-weight: 500;
        }

        .loadernew {
            font-size: 8px;
            margin: 0px auto 30px auto;
            width: 11em;
            height: 11em;
            border-radius: 50%;
            background: #ffffff;
            border-top: 6px solid #fa9e1b;
            border-left: 6px solid #fa9e1b;
            border-right: 6px solid #fa9e1b;
            position: relative;
            animation: load3 1.4s linear infinite;
        }



        @keyframes load3 {
            0% {

                transform: rotate(0deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;

            }
            10%{
                transform: rotate(36deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            20%{
                transform: rotate(72deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            30%{
                transform: rotate(108deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            40% {
                transform: rotate(144deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            50% {

                transform: rotate(180deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;

            }
            60%{
                transform: rotate(216deg);

                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            70%{
                transform: rotate(252deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            80%{
                transform: rotate(288deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            90% {

                transform: rotate(324deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            100% {

                transform: rotate(360deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
        }
        body{
            margin: 0;
            background: transparent;
        }


        div.modal-content.lc{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
       /* p.g-fl:after {
            position: relative;
            content: "\\f072";
            font-family: FontAwesome;
            font-weight: 900;
            color: #FF904B;
            left: 47%;
            transform: rotate(45deg) translateX(-50%);
            !* top: -18.3%; *!
            bottom: -3px;
            font-size: 29px;
        }*/
     img.fl-img {
        height: 31.1px;
        position: absolute;
        top: 44% !important;
        left: 48%;
        }
        p.request-p {
            padding: 11px;
            text-align: center;
            color:#381967;
            font-size: 17px;
            line-height: 1.5;
        }
                .multi-div{
                    overflow: visible !important;
                }
                
                @media screen and (max-width:992px){
        .modal-content.lc {
            width: 43% !important;
        }
        }

        @media screen and (max-width:775px){
        .modal-content.lc {
            width: 50% !important;
        }
        } 
        @media screen and (max-width: 650px){
                    .modal-content.lc {
            padding-top: 50px !important;
            width: 90% !important;
             box-shadow: none !important;
        }
                 
                } 
        </style>
                       </head>
                       <body>
      <div class="modal fade myModal" id="myModal1" style="height: 100%;font-family: \'Open Sans\', sans-serif;background: aliceblue; margin: 0;box-sizing: border-box;">

                            <div class="modal-dialog " style="">
                            <div class="modal-content lc" style="background:white;padding-top:60px;width:32%; height:auto;margin:0 auto;border:none;border-radius:7px;box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.2)">
                                <div class="modal-header" style="border: none;padding-bottom: 0">
                                      <img src="'.asset('assets/images/logo.png').'" class="l-img">
                                 </div>
                                  <div class="modal-body">
                                  <p class="request-p">Please Do Not Close Or Refresh The Window While We Are Searching The Best Hotels For You</p>
                                    <!-- <h3 class="s-note">We  Are Searching Hotel  '.$hotelname.' <br> Room Type : '.$hotelroomtype.'</h3> -->
                                    <div class="row" style="width:100%">
                                        <div class="c-in1" style="width:50%;display:block;float:left;">

                                            <p style="margin: 0; padding: 11px;font-size: 16px;font-weight: 500;" class="g-fl">Check In '.$hotelchkin.'</p>
                                        </div>
                                        <div class="c-in1" style="width:50%;display:block;float:left;">
                                            <p style="margin: 0; padding: 11px;font-size: 16px;font-weight: 500;">Check Out '.$hotelchkout.'</p>
                                        </div>
                                    </div>

                                    <p style="font-size: 17px;font-weight: 600;padding-top: 60px;text-align: center; margin-top: 20px;" class="p-wait">Please Wait...</p>

                                </div>
                                <div class="loadernew"></div>
                            </div>
                                  
                            </div>
                          </div>
                          <script>
                $(document).ready(function(){
                  
                  $("#myModal1").modal("show");
                  });
            </script>
            </body>
            </html>';

        $TokenId=Cookie::get('TokenId');
        $EndUserIp=Cookie::get('localip');
        $TraceId=session()->get('hotel_traceid');

        $resultindex=$request->get('resultindex');
        $hotelcode=$request->get('hotelcode');
        $roomindex=$request->get('roomindex');
        $roomindexarray=explode('-',$roomindex);
        $hotelname=$request->get('hotelname');
        $selectedhotel=array();
        $selectedroom=array();
        $roomdetails=array();

        //hotel details code
        if(session()->has('resultarray'))
        {
            $resultarray=session()->get('resultarray');
            for($i=0;$i<count($resultarray['HotelResults']);$i++)
            {
                if($resultarray['HotelResults'][$i]['HotelName']==$hotelname)
                {
                    $selectedhotel=$resultarray['HotelResults'][$i];
                }
            }
        }
        //end of hotel details code
        if(session()->has('hotelroomsdata'))
        {
            $resultarray=session()->get('hotelroomsdata');
        }
        for($i=0;$i<count($resultarray['HotelRoomsDetails']);$i++)
        {
            for($room=0;$room<count($roomindexarray);$room++)
            {
                if($roomindexarray[$room]!='')
                {
                    if($resultarray['HotelRoomsDetails'][$i]['RoomIndex']== $roomindexarray[$room])
                    {
                        $selectedroom[]=$resultarray['HotelRoomsDetails'][$i];
                    }
                }
            }
        }
        $GuestNationality=session()->get('dataarray')['guest_nationality'];
        $NoOfRooms=session()->get('dataarray')['no_of_rooms'];
        $ClientReferenceNo=0;
        $IsVoucherBooking=true;

        for($roomdetailcount=0;$roomdetailcount<count($selectedroom);$roomdetailcount++)
        {

            $RoomIndex=$selectedroom[$roomdetailcount]['RoomIndex'];
            $RoomTypeCode=$selectedroom[$roomdetailcount]['RoomTypeCode'];
            $RoomTypeName=$selectedroom[$roomdetailcount]['RoomTypeName'];
            $RatePlanCode=$selectedroom[$roomdetailcount]['RatePlanCode'];
            $BedTypeCode=null;

            $SmokingPreference=0;
            $Supplements=null;
            $CurrencyCode=$selectedroom[$roomdetailcount]['Price']['CurrencyCode'];
            $RoomPrice=$selectedroom[$roomdetailcount]['Price']['RoomPrice'];

            $Tax=$selectedroom[$roomdetailcount]['Price']['Tax'];
            $ExtraGuestCharge=$selectedroom[$roomdetailcount]['Price']['ExtraGuestCharge'];
            $ChildCharge=$selectedroom[$roomdetailcount]['Price']['ChildCharge'];
            $OtherCharges=$selectedroom[$roomdetailcount]['Price']['OtherCharges'];

            $Discount=$selectedroom[$roomdetailcount]['Price']['Discount'];
            $PublishedPrice=$selectedroom[$roomdetailcount]['Price']['PublishedPrice'];
            $PublishedPriceRoundedOff=$selectedroom[$roomdetailcount]['Price']['PublishedPriceRoundedOff'];
            $OfferedPrice=$selectedroom[$roomdetailcount]['Price']['OfferedPrice'];

            $OfferedPriceRoundedOff=$selectedroom[$roomdetailcount]['Price']['OfferedPriceRoundedOff'];
            $AgentCommission=$selectedroom[$roomdetailcount]['Price']['AgentCommission'];
            $AgentMarkUp=$selectedroom[$roomdetailcount]['Price']['AgentMarkUp'];
            $ServiceTax=$selectedroom[$roomdetailcount]['Price']['ServiceTax'];
            $TDS=$selectedroom[$roomdetailcount]['Price']['TDS'];

            $roomdetails[]=array(
                'RoomIndex' =>$RoomIndex,
                'RoomTypeCode' =>$RoomTypeCode,
                'RoomTypeName' =>$RoomTypeName,
                'RatePlanCode' =>$RatePlanCode,
                'BedTypeCode' =>$BedTypeCode,
                'SmokingPreference' =>$SmokingPreference,
                'RoomTypeName' =>$RoomTypeName,
                'Price' =>array(
                    'CurrencyCode' =>$CurrencyCode,
                    'RoomPrice' =>number_format($RoomPrice, 2, '.', ''),
                    'RoomTypeName' =>$RoomTypeName,
                    'Tax' =>number_format($Tax, 2, '.', ''),
                    'ExtraGuestCharge' =>$ExtraGuestCharge,
                    'ChildCharge' => number_format($ChildCharge, 2, '.', ''),
                    'OtherCharges' =>number_format($OtherCharges, 2, '.', ''),
                    'Discount' =>number_format($Discount, 2, '.', ''),
                    'PublishedPrice' =>number_format($PublishedPrice, 2, '.', ''),
                    'PublishedPriceRoundedOff' =>$PublishedPriceRoundedOff,
                    'OfferedPrice' =>number_format($OfferedPrice, 2, '.', ''),
                    'OfferedPriceRoundedOff' =>$OfferedPriceRoundedOff,
                    'AgentCommission' =>number_format($AgentCommission, 2, '.', ''),
                    'AgentMarkUp' =>number_format($AgentMarkUp, 2, '.', ''),
                    'ServiceTax' =>number_format($ServiceTax, 2, '.', ''),
                    'TDS' =>number_format($TDS, 2, '.', ''),
                )
            );

        }
        $form_data = array(
            'ResultIndex'=>$resultindex,
            'HotelCode'=>$hotelcode,
            'HotelName' => $hotelname,
            'GuestNationality' =>$GuestNationality,
            'NoOfRooms'=>$NoOfRooms,
            'ClientReferenceNo' => $ClientReferenceNo,
            'IsVoucherBooking' =>$IsVoucherBooking,
            'HotelRoomsDetails' => $roomdetails,
            'EndUserIp'=>$EndUserIp,
            'TokenId'=>$TokenId,
            'TraceId'=>$TraceId
        );

        $data_string = json_encode($form_data);
       
        $ch = curl_init('https://api.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/BlockRoom/');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);
        
        $result1=json_decode($result,true);


        $hotelblockroomarray = !empty($result1['BlockRoomResult'])?$result1['BlockRoomResult']:array();
        $hotelmargin1=Cookie::get('gensetting');
        $hotelmargin = $hotelmargin1->hotel_margin;
        $mainp=0;
        $ServiceTax=0;


        if(!empty($hotelblockroomarray))
        {
            if($hotelblockroomarray['Error']['ErrorCode']==0)
            {
                for($i=0;$i<count($hotelblockroomarray['HotelRoomsDetails']);$i++)
                {
                    $mainp+= $hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['RoomPrice'];
                    $maintax = $hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['Tax']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['ExtraGuestCharge']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['ChildCharge']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['OtherCharges']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['AgentCommission']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['AgentMarkUp']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['ServiceTax']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['TDS']+$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['TotalGSTAmount'];
                    $ServiceTax+=$maintax;



                    $margin = ($mainp * $hotelmargin)/100;
                    $mainprice = $mainp + $margin;
                    $price= $this->price($mainprice,$ServiceTax,0,0,$mainp);


                    $hotelmainprice = $price[0];
                    $hotelmaincurrecny = $price[1];



                    $hotelblockroomarray['hotelmainprice']=$hotelmainprice;
                    $hotelblockroomarray['hotelmaincurrecny']=$hotelmaincurrecny;
                    $hotelblockroomarray['hotelmargin']=$hotelmainprice;
                    $hotelblockroomarray['hotellastprice']=$price[7];
                    $hotelblockroomarray['hotelconvercurr']=$price[2];
                    $hotelblockroomarray['hotelservicetax']=$price[3];
                    $hotelblockroomarray['hotelmaingst']=$price[6];
                    // $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelothercharges']=$price[4];
                    // $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelgst']=$price[5];
                }
            }
        }


        echo '<script>
                    $(document).ready(function(){
                      
                      $("#myModal1").modal("hide");
                      });
                </script>';

        return view('pages.hotel-book')->with(compact('selectedhotel'))->with(compact('hotelblockroomarray'))->with('roomindexes',$roomindex);
    }


    public function hotelbooking(Request $request)
    {
        $hotelname=session()->get('hotelroomnn');
        $hotelchkin=session()->get('hotelchkin');
        $hotelchkout=session()->get('hotelchkout');
        $hotelroomtype=session()->get('hotelroomntype');
        echo $modal_show ='
    <!doctype html>
                       <html lang="en">
                       <head>
                       
                                    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
                                                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                                    <title>TravoWeb</title>
                        <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">             
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <style>
                  p.g-fl:after {
    position: relative;
    content: "\\f594";
    font-family: "Font Awesome 5 Free";
    font-weight: 900;
    color: #FF904B;
    left: 16%;
    transform: translateX(-50%);
    bottom: 0px;
    font-size: 16px;
}
        .l-img{
            width: 165px;
            display: block;
            margin: auto;
        }
        .s-note {
            text-align: center;
            color: #ff000a;
            padding-bottom: 10px;
            font-size: 19px;
            font-weight: lighter;
            font-family: inherit;
        }
        .c-in1 {
            background: #381967;
            text-align: center;
            color: white;
            font-size: 18px;
            font-weight: 500;
        }

        .loadernew {
            font-size: 8px;
            margin: 0px auto 30px auto;
            width: 11em;
            height: 11em;
            border-radius: 50%;
            background: #ffffff;
            border-top: 6px solid #fa9e1b;
            border-left: 6px solid #fa9e1b;
            border-right: 6px solid #fa9e1b;
            position: relative;
            animation: load3 1.4s linear infinite;
        }



        @keyframes load3 {
            0% {

                transform: rotate(0deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;

            }
            10%{
                transform: rotate(36deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            20%{
                transform: rotate(72deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            30%{
                transform: rotate(108deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            40% {
                transform: rotate(144deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            50% {

                transform: rotate(180deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;

            }
            60%{
                transform: rotate(216deg);

                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            70%{
                transform: rotate(252deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            80%{
                transform: rotate(288deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            90% {

                transform: rotate(324deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
            100% {

                transform: rotate(360deg);
                border-top: 6px solid #FF4060;
                border-left: 6px solid #00206A;
                border-right: 6px solid #fa9e1b;
                border-bottom: 6px solid #00206A;
            }
        }
        body{
            margin: 0;
            background: transparent;
        }


        div.modal-content.lc{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
       /* p.g-fl:after {
            position: relative;
            content: "\\f072";
            font-family: FontAwesome;
            font-weight: 900;
            color: #FF904B;
            left: 47%;
            transform: rotate(45deg) translateX(-50%);
            !* top: -18.3%; *!
            bottom: -3px;
            font-size: 29px;
        }*/
     img.fl-img {
    height: 31.1px;
    position: absolute;
    top: 44% !important;
    left: 48%;
}
        .multi-div{
            overflow: visible !important;
        }
        
        @media screen and (max-width:992px){
.modal-content.lc {
    width: 43% !important;
}
}

@media screen and (max-width:775px){
.modal-content.lc {
    width: 50% !important;
}
} 
@media screen and (max-width: 650px){
            .modal-content.lc {
    padding-top: 50px !important;
    width: 90% !important;
     box-shadow: none !important;
}
         
        } 
    </style>
                       </head>
                       <body>
                               <div class="modal fade myModal" id="myModal1" style="height: 100%;font-family: \'Open Sans\', sans-serif;background: aliceblue; margin: 0;box-sizing: border-box;">

                            <div class="modal-dialog " style="">
                            <div class="modal-content lc" style="background:white;padding-top:60px;width:32%; height:auto;margin:0 auto;border:none;border-radius:7px;box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.2)">
                                <div class="modal-header" style="border: none;padding-bottom: 0">
                                      <img src="'.asset('assets/images/logo.png').'" class="l-img">
                                 </div>
                                  <div class="modal-body">
                                    <h3 class="s-note">We Are Searching Hotel  '.$hotelname.' <br> Room Type : '.$hotelroomtype.'</h3>
                                    <div class="row" style="width:100%">
                                        <div class="c-in1" style="width:50%;display:block;float:left;">

                                            <p style="margin: 0; padding: 11px;font-size: 16px;font-weight: 500;" class="g-fl">Check In '.$hotelchkin.'</p>
                                        </div>
                                        <div class="c-in1" style="width:50%;display:block;float:left;margin-left: -1px;">
                                            <p style="margin: 0; padding: 11px;font-size: 16px;font-weight: 500;">Check Out '.$hotelchkout.'</p>
                                        </div>
                                    </div>

                                    <p style="font-size: 17px;font-weight: 600;padding-top: 60px;text-align: center; margin-top: 20px;" class="p-wait">Please Wait...</p>

                                </div>
                                <div class="loadernew"></div>
                            </div>
                                  
                            </div>
                          </div>
                          <script>
                $(document).ready(function(){
                  
                  $("#myModal1").modal("show");
                  });
            </script> 
            </body>
            </html>
            ';

        $paymentorder = session()->get('paymentorder');
        $paymentref = session()->get('paymentref');
        $formmyarray= session()->get('myhotelbooking');
        parse_str($formmyarray, $formdata);



        $TokenId=Cookie::get('TokenId');
        $EndUserIp=Cookie::get('localip');
        $TraceId=session()->get('hotel_traceid');

        $resultindex= $formdata['resultindex'];
        $hotelcode=$formdata['hotelcode'];
        $roomindex=$formdata['roomindexes'];
        $roomindexarray=explode('-',$roomindex);
        $hotelname=$formdata['roomindexes'];
        $NoOfRooms=session()->get('dataarray')['no_of_rooms'];
        $selectedroom=array();
        $roomdetails=array();
        $passengerdetails=array();
        //newmargin
        $hotelmarginprice=$formdata['hotelmarginprice'];
        $hotelmainprice=$formdata['hotellastprice'];
        $hotelcurrecny=$formdata['hotelconvcurr'];
        $hotelmargingst=$formdata['hotelmaingst'];
        $hotelfaicon=$formdata['currencyicon'];
        $hotelservice_tax=$formdata['hotelservicetax'];
        $hotelamount =$formdata['amount'];
        //endnewmargin
        $lead_passenger_title=$formdata['lead_passenger_title'];
        $lead_passenger_first=$formdata['lead_passenger_first'];
        $lead_passenger_middle=$formdata['lead_passenger_middle'];
        $lead_passenger_last=$formdata['lead_passenger_last'];
        $lead_passenger_age=$formdata['lead_passenger_age'];
        $lead_passenger_email= $formdata['lead_passenger_email'];
        $lead_passenger_contact=$formdata['lead_passenger_contact'];
        $passportno=null;
    	$PassportExpDate="0001-01-01T00:00:00";
    	$pan=null;
    	 $passvalid=$formdata['lead_passvalid'];
		if($passvalid=='1')
		{
		    $pan1=$formdata['lead_passenger_pancard'];
		    if($pan1=="")
    		{
    		    $pan=null;
    		}
    		else
    		{
    		    $pan=$pan1;
    		}
		}
		else
		{
		    $passportno1=$formdata['lead_passenger_passport'];
		    $passportdata=$formdata['lead_passenger_passport_date'];
		    if($passportno=="")
    		{
    		    $passportno=null;
    		   
    		    $PassportExpDate="0001-01-01T00:00:00";
    		   
    		}
    		else
    		{
    		    $passportno=$passportno1;
    		    
    		    $PassportExpDate=$passportdata[2]."-".$passportdata[1]."-".$passportdata[0]."T00:00:00";
    		}
		}

        $gst_company_address=$formdata['gst_company_address'];
        $gst_companyemail=$formdata['gst_companyemail'];
        $gst_number=$formdata['gst_number'];
        $gst_company_name= $formdata['gst_company_name'];
        $gst_company_contact=$formdata['gst_company_contact'];
        //emergency
        $eme_name=$formdata['eme_name'];
        $eme_phone=$formdata['eme_phone'];
        $eme_relation=$formdata['eme_relation'];

        //endemergency

        $adultcount_per_room=$formdata['adultcount_per_room'];
        $childcount_per_room= $formdata['childcount_per_room'];
        if(!empty($formdata['room_adult_title']))
        {
            $room_adult_title=$formdata['room_adult_title'];
            $room_adult_first=$formdata['room_adult_first'];
            $room_adult_last= $formdata['room_adult_last'];
            $room_adult_age=$formdata['room_adult_age'];
        }
        if(!empty($formdata['room_child_title']))
        {
            $room_child_title=$formdata['room_child_title'];
            $room_child_first=$formdata['room_child_first'];
            $room_child_last=$formdata['room_child_last'];
            $room_child_age=$formdata['room_child_age'];
        }



        $adultbackindex=0;
        $childbackindex=0;

        for($rooms=0;$rooms< $NoOfRooms;$rooms++)
        {

            $adultarray=array();
            $childarray=array();
            $current_room_adult_count=$adultcount_per_room[$rooms];
            $current_room_child_count=$childcount_per_room[$rooms];
            for($adults=0;$adults<$current_room_adult_count;$adults++)
            {
                if($rooms==1 && $adults==0)
                {
                    $leadpassenger=true;
                }
                else
                {
                    $leadpassenger=false;
                }
                $adultarray[]=array("Title"=>$room_adult_title[$adultbackindex],
                    "FirstName"=>$room_adult_first[$adultbackindex],
                    "MiddleName"=>null,
                    "LastName"=>$room_adult_last[$adultbackindex],
                    "Phoneno"=> null,
                    "Email"=> null,
                    "PaxType"=>1,
                    "LeadPassenger"=>$leadpassenger,
                    "GSTCompanyAddress"=>null,
                    "GSTCompanyContactNumber"=>null,
                    "GSTCompanyEmail"=>null,
                    "GSTCompanyName"=>null,
                    "GSTNumber"=>null,
                    "Age"=>$room_adult_age[$adultbackindex],
                    "PassportNo"=> $passportno,
				    "PassportExpDate"=> $PassportExpDate,
					"PAN"=>$pan);
                $adultbackindex++;

            }

            for($child=0;$child<$current_room_child_count;$child++)
            {

                $childarray[]=array("Title"=>$room_child_title[$childbackindex],
                    "FirstName"=>$room_child_first[$childbackindex],
                    "MiddleName"=>null,
                    "LastName"=>$room_child_last[$childbackindex],
                    "Phoneno"=> null,
                    "Email"=> null,
                    "PaxType"=>2,
                    "LeadPassenger"=>false,
                    "GSTCompanyAddress"=>null,
                    "GSTCompanyContactNumber"=>null,
                    "GSTCompanyEmail"=>null,
                    "GSTCompanyName"=>null,
                    "GSTNumber"=>null,
                    "Age"=>$room_child_age[$childbackindex],
                    "PassportNo"=> $passportno,
				    "PassportExpDate"=> $PassportExpDate,
					"PAN"=>$pan);

                $childbackindex++;

            }
            // echo "<pre>";
            // print_r($adultarray);
            // echo "</pre>";

            if($rooms==0)
            {
                if(count($childarray)>0 && count($adultarray)>0)
                {
                    $passengerdetails[$rooms]=[array("Title"=> $lead_passenger_title,
                        "FirstName"=>$lead_passenger_first,
                        "MiddleName"=>$lead_passenger_middle,
                        "LastName"=>$lead_passenger_last,
                        "Phoneno"=>$lead_passenger_contact,
                        "Email"=>$lead_passenger_email,
                        "PaxType"=>1,
                        "LeadPassenger"=>true,
                        "GSTCompanyAddress"=>"$gst_company_address",
                        "GSTCompanyContactNumber"=>"$gst_company_contact",
                        "GSTCompanyEmail"=>"$gst_companyemail",
                        "GSTCompanyName"=>"$gst_company_name",
                        "GSTNumber"=>"$gst_number",
                        "Age"=> $lead_passenger_age,
                        "PassportNo"=> $passportno,
    				    "PassportExpDate"=> $PassportExpDate,
    					"PAN"=>$pan
                    )];
                    $adult_with_child=array_merge($adultarray,$childarray);
                    $passengerdetails[$rooms]=array_merge($passengerdetails[$rooms],$adult_with_child);
                }
                else if(count($adultarray)>0)
                {
                    $passengerdetails[$rooms]=[array("Title"=> $lead_passenger_title,
                        "FirstName"=>$lead_passenger_first,
                        "MiddleName"=>$lead_passenger_middle,
                        "LastName"=>$lead_passenger_last,
                        "Phoneno"=>$lead_passenger_contact,
                        "Email"=>$lead_passenger_email,
                        "PaxType"=>1,
                        "LeadPassenger"=>true,
                        "GSTCompanyAddress"=>"$gst_company_address",
                        "GSTCompanyContactNumber"=>"$gst_company_contact",
                        "GSTCompanyEmail"=>"$gst_companyemail",
                        "GSTCompanyName"=>"$gst_company_name",
                        "GSTNumber"=>"$gst_number",
                        "Age"=> $lead_passenger_age,
                        "PassportNo"=> $passportno,
    				    "PassportExpDate"=> $PassportExpDate,
    					"PAN"=>$pan
                    )];
                    $passengerdetails[$rooms]=array_merge($passengerdetails[$rooms],$adultarray);
                }
                else if(count($childarray)>0)
                {
                    $passengerdetails[$rooms]=[array("Title"=> $lead_passenger_title,
                        "FirstName"=>$lead_passenger_first,
                        "MiddleName"=>$lead_passenger_middle,
                        "LastName"=>$lead_passenger_last,
                        "Phoneno"=>$lead_passenger_contact,
                        "Email"=>$lead_passenger_email,
                        "PaxType"=>1,
                        "LeadPassenger"=>true,
                        "GSTCompanyAddress"=>"$gst_company_address",
                        "GSTCompanyContactNumber"=>"$gst_company_contact",
                        "GSTCompanyEmail"=>"$gst_companyemail",
                        "GSTCompanyName"=>"$gst_company_name",
                        "GSTNumber"=>"$gst_number",
                        "Age"=> $lead_passenger_age,
                        "PassportNo"=> $passportno,
    				    "PassportExpDate"=> $PassportExpDate,
    					"PAN"=>$pan
                    )];
                    $passengerdetails[$rooms]=array_merge($passengerdetails[$rooms],$childarray);
                }
                else
                {
                    $passengerdetails[$rooms]=[array("Title"=> $lead_passenger_title,
                        "FirstName"=>$lead_passenger_first,
                        "MiddleName"=>$lead_passenger_middle,
                        "LastName"=>$lead_passenger_last,
                        "Phoneno"=>$lead_passenger_contact,
                        "Email"=>$lead_passenger_email,
                        "PaxType"=>1,
                        "LeadPassenger"=>true,
                        "Age"=> $lead_passenger_age,
                        "GSTCompanyAddress"=>"$gst_company_address",
                        "GSTCompanyContactNumber"=>"$gst_company_contact",
                        "GSTCompanyEmail"=>"$gst_companyemail",
                        "GSTCompanyName"=>"$gst_company_name",
                        "GSTNumber"=>"$gst_number",
                        "PassportNo"=> $passportno,
    				    "PassportExpDate"=> $PassportExpDate,
    					"PAN"=>$pan
                    )];
                }
            }
            else
            {
                if(count($childarray)>0)
                {
                    $adult_with_child=array_merge($adultarray,$childarray);
                    $passengerdetails[$rooms]=[];
                    $passengerdetails[$rooms]=array_merge($passengerdetails[$rooms],$adult_with_child);

                }
                else
                {
                    $passengerdetails[$rooms]=[];
                    $passengerdetails[$rooms]=array_merge($passengerdetails[$rooms],$adultarray);
                }

            }

        }
        // echo "<pre>";
        // print_r($passengerdetails[1]);
        // echo "</pre>";

        if(session()->has('hotelroomsdata'))
        {
            $resultarray=session()->get('hotelroomsdata');
        }
        for($i=0;$i<count($resultarray['HotelRoomsDetails']);$i++)
        {
            for($room=0;$room<count($roomindexarray);$room++)
            {
                if($roomindexarray[$room]!='')
                {
                    if($resultarray['HotelRoomsDetails'][$i]['RoomIndex']== $roomindexarray[$room])
                    {
                        $selectedroom[]=$resultarray['HotelRoomsDetails'][$i];
                    }
                }
            }
        }
        $GuestNationality=session()->get('dataarray')['guest_nationality'];
        $NoOfRooms=session()->get('dataarray')['no_of_rooms'];
        $ClientReferenceNo="0";
        $IsVoucherBooking="true";
        if($request->has('smoking_preference'))
        {
            $SmokingPreference=2;
        }
        else
        {
            $SmokingPreference=0;
        }
        for($roomdetailcount=0;$roomdetailcount<count($selectedroom);$roomdetailcount++)
        {

            $RoomIndex=$selectedroom[$roomdetailcount]['RoomIndex'];
            $RoomTypeCode=$selectedroom[$roomdetailcount]['RoomTypeCode'];
            $RoomTypeName=$selectedroom[$roomdetailcount]['RoomTypeName'];
            $RatePlanCode=$selectedroom[$roomdetailcount]['RatePlanCode'];
            $BedTypeCode=null;


            $Supplements=null;
            $CurrencyCode=$selectedroom[$roomdetailcount]['Price']['CurrencyCode'];
            $RoomPrice=$selectedroom[$roomdetailcount]['Price']['RoomPrice'];

            $Tax=$selectedroom[$roomdetailcount]['Price']['Tax'];
            $ExtraGuestCharge=$selectedroom[$roomdetailcount]['Price']['ExtraGuestCharge'];
            $ChildCharge=$selectedroom[$roomdetailcount]['Price']['ChildCharge'];
            $OtherCharges=$selectedroom[$roomdetailcount]['Price']['OtherCharges'];

            $Discount=$selectedroom[$roomdetailcount]['Price']['Discount'];
            $PublishedPrice=$selectedroom[$roomdetailcount]['Price']['PublishedPrice'];
            $PublishedPriceRoundedOff=$selectedroom[$roomdetailcount]['Price']['PublishedPriceRoundedOff'];
            $OfferedPrice=$selectedroom[$roomdetailcount]['Price']['OfferedPrice'];

            $OfferedPriceRoundedOff=$selectedroom[$roomdetailcount]['Price']['OfferedPriceRoundedOff'];
            $AgentCommission=$selectedroom[$roomdetailcount]['Price']['AgentCommission'];
            $AgentMarkUp=$selectedroom[$roomdetailcount]['Price']['AgentMarkUp'];
            $ServiceTax=$selectedroom[$roomdetailcount]['Price']['ServiceTax'];
            $TDS=$selectedroom[$roomdetailcount]['Price']['TDS'];

            $roomdetails[]=array(
                'RoomIndex' =>$RoomIndex,
                'RoomTypeCode' =>$RoomTypeCode,
                'RoomTypeName' =>$RoomTypeName,
                'RatePlanCode' =>$RatePlanCode,
                'BedTypeCode' =>$BedTypeCode,
                'SmokingPreference' =>$SmokingPreference,
                'RoomTypeName' =>$RoomTypeName,
                'Price' =>array(
                    'CurrencyCode' =>$CurrencyCode,
                    'RoomPrice' =>"$RoomPrice",
                    'RoomTypeName' =>$RoomTypeName,
                    'Tax' =>"$Tax",
                    'ExtraGuestCharge' =>number_format($ExtraGuestCharge,1),
                    'ChildCharge' =>number_format($ChildCharge,1),
                    'OtherCharges' =>$OtherCharges,
                    'Discount' =>number_format($Discount,1),
                    'PublishedPrice' =>"$PublishedPrice",
                    'PublishedPriceRoundedOff' =>"$PublishedPriceRoundedOff",
                    'OfferedPrice' =>"$OfferedPrice",
                    'OfferedPriceRoundedOff' =>"$OfferedPriceRoundedOff",
                    'AgentCommission' =>"$AgentCommission",
                    'AgentMarkUp' =>number_format($AgentMarkUp,1),
                    'ServiceTax' =>"$ServiceTax",
                    'TDS' =>number_format($TDS),

                ),
                'HotelPassenger'=>$passengerdetails[$roomdetailcount]
            );

        }
        $form_data = array(
            'ResultIndex'=>$resultindex,
            'HotelCode'=>$hotelcode,
            'HotelName' => $hotelname,
            'GuestNationality' =>$GuestNationality,
            'NoOfRooms'=>$NoOfRooms,
            'ClientReferenceNo' => $ClientReferenceNo,
            'IsVoucherBooking' =>$IsVoucherBooking,
            'HotelRoomsDetails' => $roomdetails,
            'EndUserIp'=>$EndUserIp,
            'TokenId'=>$TokenId,
            'TraceId'=>$TraceId
        );

        $data_string = json_encode($form_data);



        $ch = curl_init('https://booking.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/Book/');


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        $result1=json_decode($result,true);


        //erroshow pending
        $errorcode = $result1['BookResult']['Error']['ErrorCode'];
        $errormsg = $result1['BookResult']['Error']['ErrorMessage'];
        $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode,'response_msg' => $errormsg]);
        if($result1['BookResult']['Error']['ErrorCode']!=0)
        {
            echo "<script>alert('".$errormsg."');</script>";
            return redirect()->intended('/index');
        }
        $hotelbookingarray = !empty($result1['BookResult'])?$result1['BookResult']:array();

        // die();
        $bookingid=$hotelbookingarray['BookingId'];
        // 	echo "<pre>";
        // 	echo "<br>";
        // 	print_r($result1);
        // $bookingid=1478154;
        $form_data1 = array(
            'EndUserIp'=>$EndUserIp,
            'TokenId'=>$TokenId,
            'BookingId'=>"$bookingid"
        );

        $data_string = json_encode($form_data1);

        $ch = curl_init('https://booking.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/GetBookingDetail/');


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        $result1=json_decode($result,true);


        $errorcode = $result1['GetBookingDetailResult']['Error']['ErrorCode'];
        $errormsg = $result1['GetBookingDetailResult']['Error']['ErrorMessage'];
        $paymentdb = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['reponse' => $errorcode,'response_msg' => $errormsg]);
        if($result1['GetBookingDetailResult']['Error']['ErrorCode']!=0)
        {
            echo "<script>alert('".$errormsg."');</script>";
            return redirect()->intended('/index');
        }
        $hotelbookdetailsarray = !empty($result1['GetBookingDetailResult'])?$result1['GetBookingDetailResult']:array();
        if($hotelbookdetailsarray['Error']['ErrorCode']==0)
        {
            $bookingid=$hotelbookdetailsarray['BookingId'];
            $hotel_booking_status=$hotelbookdetailsarray['HotelBookingStatus'];
            $confirmation_no=$hotelbookdetailsarray['ConfirmationNo'];
            $booking_ref_no=$hotelbookdetailsarray['BookingRefNo'];
            $booking_date=$hotelbookdetailsarray['BookingDate'];
            $total_adult_count=$request->session()->get('dataarray')['adult_count'];
            $total_child_count=$request->session()->get('dataarray')['child_count'];
            $lead_name=$hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['FirstName']." ".$hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['LastName'];
            $lead_email=$hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['Email'];
            $lead_mobile=$hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['Phoneno'];
            $lead_pax=$hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['PaxType'];
            $lead_title=$hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['Title'];
            $hotel_rooms_details=serialize($hotelbookdetailsarray['HotelRoomsDetails']);
            $invoice_no=$hotelbookdetailsarray['InvoiceNo'];
            $invoice_date_time=explode('T',$hotelbookdetailsarray['InvoiceCreatedOn']);
            $invoice_date=$invoice_date_time[0];
            $invoice_time=$invoice_date_time[1];
            $invoice_amount=$hotelbookdetailsarray['InvoiceAmount'];
            $hotel_name=$hotelbookdetailsarray['HotelName'];
            $hotelbookname=$request->session()->get('hotelbookname');
            if(session()->has('resultarray'))
            {
                $resultarray=session()->get('resultarray');
                for($i=0;$i<count($resultarray['HotelResults']);$i++)
                {
                    if($resultarray['HotelResults'][$i]['HotelName']==$hotelbookname)
                    {
                        $currenthotel=$resultarray['HotelResults'][$i];
                    }
                }
            }
            $hotel_picture=$currenthotel['HotelPicture'];
            $hotel_star=$hotelbookdetailsarray['StarRating'];
            $hotel_address1=$hotelbookdetailsarray['AddressLine1'];
            $hotel_address2=$hotelbookdetailsarray['AddressLine2'];
            $hotel_countrycode=$hotelbookdetailsarray['CountryCode'];
            $hotel_latitude=$hotelbookdetailsarray['Latitude'];
            $hotel_longitude=$hotelbookdetailsarray['Longitude'];
            $hotel_city=$hotelbookdetailsarray['City'];
            $checkin_datetime=explode('T',$hotelbookdetailsarray['CheckInDate']);
            $check_in_date=$checkin_datetime[0];
            $checkout_datetime=explode('T',$hotelbookdetailsarray['CheckOutDate']);
            $check_out_date=$checkout_datetime[0];
            $no_of_rooms=$hotelbookdetailsarray['NoOfRooms'];
            $special_request=$hotelbookdetailsarray['SpecialRequest'];
            $is_domestic=$hotelbookdetailsarray['IsDomestic'];
            $hotel_policy_detail=$hotelbookdetailsarray['HotelPolicyDetail'];
            $hotel_confirmation_no=$hotelbookdetailsarray['HotelConfirmationNo'];
            $whole_response=serialize($hotelbookdetailsarray);
            date_default_timezone_set("Asia/Kolkata");
            $dateime=date('Y-m-d H:i:s');
            $create_date=date('Y-m-d');
            $create_time=date('H:i:s');
            $username='';
            $user_id='';
            $userphone='';
            if(session()->has('travo_username'))
            {
                $username=session()->get('travo_username');
            }

            if(session()->has('travo_userid'))
            {
                $user_id=session()->get('travo_userid');
            }
            if(session()->has('travo_userphone'))
            {
                $userphone=session()->get('travo_userphone');
            }

            $useremail=session()->get('travo_useremail');

            $bookingdate1 = explode('T',$booking_date);
            $bookindate_new=$bookingdate1[0];
            $bookindate_time=$bookingdate1[1];

            $insertarray=array("bookingid"=>$bookingid,
                "hotel_booking_status"=>$hotel_booking_status,
                "confirmation_no"=>$confirmation_no,
                "booking_ref_no"=>$booking_ref_no,
                "booking_date"=>$bookindate_new,
                "booking_time"=>$bookindate_time,
                "total_adult_count"=>$total_adult_count,
                "total_child_count"=>$total_child_count,

                "lead_name"=>$lead_name,
                "lead_email"=>$lead_email,
                "lead_mobile"=>$lead_mobile,
                "lead_pax"=>$lead_pax,
                "lead_title"=>$lead_title,

                "hotel_rooms_details"=>$hotel_rooms_details,

                "invoice_no"=>$invoice_no,
                "invoice_date"=>$invoice_date,
                "invoice_time"=>$invoice_time,
                "invoice_amount"=>$invoice_amount,

                "hotel_name"=>$hotel_name,
                "hotel_picture"=>$hotel_picture,
                "hotel_star"=>$hotel_star,
                "hotel_address1"=>$hotel_address1,
                "hotel_address2"=>$hotel_address2,
                "hotel_countrycode"=>$hotel_countrycode,
                "hotel_latitude"=>$hotel_latitude,
                "hotel_longitude"=>$hotel_longitude,
                "hotel_city"=>$hotel_city,

                "check_in_date"=>$check_in_date,
                "check_out_date"=>$check_out_date,

                "no_of_rooms"=>$no_of_rooms,
                "special_request"=>$special_request,
                "is_domestic"=>$is_domestic,
                "hotel_policy_detail"=>$hotel_policy_detail,
                "hotel_confirmation_no"=>$hotel_confirmation_no,

                "whole_response"=>$whole_response,
                "login_id"=>$user_id,
                "login_username"=>$username,
                "login_guest_email"=>$useremail,
                "create_date"=>$create_date,
                "create_time"=>$create_time,
                "guestphone"=>$userphone,
                "order_id"=>$paymentorder,
                "order_ref"=>$paymentref,
                "hotelmarginprice"=>$hotelmarginprice,

                "hotelmainprice"=>$hotelmainprice,
                "hotelcurrecny"=>$hotelcurrecny,
                "hotelmargingst"=>$hotelmargingst,
                "hotelservice_tax"=>$hotelservice_tax,
                "hotelfaicon"=>$hotelfaicon,
                "hotelamount"=>$hotelamount,
                "eme_name"=>$eme_name,
                "eme_phone"=>$eme_phone,
                "eme_relation"=>$eme_relation,


            );

        }

        $insertquery=tbl_hotel_book::insert($insertarray);



        if($insertquery)
        {
            $id = DB::getPdo()->lastInsertId();
            $request->session()->put('bookingid',$bookingid);
            $mytrip = new tbl_mytrip;
            $mytrip->triptype='Hotel';
            $mytrip->bookingid=$bookingid;

            $mytrip->tableid=$id;
            $mytrip->booking_date=$check_in_date;
            $mytrip->booking_time="00:00:00";
            $mytrip->trip_date=$create_date;
            $mytrip->trip_time=$create_time;
            $mytrip->login_id=$user_id;
            $mytrip->login_username=$username;
            $mytrip->login_guest_email=$useremail;
            $mytrip->guestphone=$userphone;
            $mytrip->save();
            $paymenttable = tbl_payment_order::where('orderid', $paymentorder)->where('referenceid', $paymentref)->update(['ticket_id' => $id,
                'payment_code'=>'Hotel',
            ]);
            //smsunit
            $chdata = session()->get('postData');

            $customerName = $chdata['customerName'];
            $orderCurrency=$chdata['orderCurrency'];
            $customerPhone = $chdata['customerPhone'];
            $customerEmail =  $chdata['customerEmail'];
            $hotelsms=Cookie::get('gensetting');
            $hl = explode("-", $hotelsms->hotel_sms_active);
            $sms_user=$hotelsms->sms_user_id;
            $sms_password=$hotelsms->sms_password;
            $sms_sender=$hotelsms->sms_sender;
            $sms_url=$hotelsms->sms_url;
            if($hl[1]=='1')
            {

                $leadphone1=$customerPhone;
                $user=$sms_user;
                $password=$sms_password;
                $sender=$sms_sender;

                $text="Dear ".$customerName.",Your hotel ".$hotel_name." booking has been confirmed. booking Id: ".$bookingid.", No of room ".$no_of_rooms.", Total Member (".$total_adult_count.",".$total_child_count."). Check your email for more details. -Travo Web.";
                $otp=urlencode("$text");
                $data = array(
                    'usr' => $user,
                    'pwd' => $password,
                    'ph'  => $leadphone1,
                    'sndr'  => $sender,
                    'text'  => $otp
                );
                $ch = curl_init($sms_url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                $result = curl_exec($ch);
            }

            echo '<script>
                    $(document).ready(function(){
                      
                      $("#myModal1").modal("hide");
                      });
                </script>';
            // echo "<script>alert('Successfully booked')</script>";
            return redirect()->intended('/hotelinvoice');
        }
        else
        {
            // echo "<script>Error while booking</script>";
            return redirect()->intended('/index');
        }



    }

    public function hotelinvoice(Request $request)
    {

        $booking_id=session()->get('bookingid');

        $getdata=tbl_hotel_book::where("bookingid",$booking_id)->first();

        $hotelbookdetailsarray=unserialize($getdata->whole_response);

        $booking_creation_date_time=$getdata->created_at;
        $hotelpicture=$getdata->hotel_picture;
        $hotelamount=$getdata->hotelamount;
        $hotelfaicon=$getdata->hotelfaicon;
        $hotelmargingst=$getdata->hotelmargingst;
        $hotelservice_tax=$getdata->hotelservice_tax;
        $hotelmarginprice=$getdata->hotelmarginprice;

        $hotelbookdetailsarray['hotelmarginprice']=$hotelmarginprice;
        $hotelbookdetailsarray['created_on']=$booking_creation_date_time;
        $hotelbookdetailsarray['hotelpicture']=$hotelpicture;
        $hotelbookdetailsarray['hotelamount']=$hotelamount;
        $hotelbookdetailsarray['hotelfaicon']=$hotelfaicon;
        $hotelbookdetailsarray['hotelmargingst']=$hotelmargingst;
        $hotelbookdetailsarray['hotelservice_tax']=$hotelservice_tax;
        session()->put('hotelbookdetailsarray1',$hotelbookdetailsarray);
        $pdf = PDF::loadView('pages.hotel_pdf',compact('hotelbookdetailsarray'));
        $pdfsave="hotelpdf/".$booking_id.".pdf";
        $pdf->save($pdfsave);
        $hotelemailnew=$getdata->lead_email;
        session()->put('hotelemailnew',$hotelemailnew);

        $pdf = PDF::loadView('pages.hotel_pdf',compact('hotelbookdetailsarray'));

        Mail::send(['html' => 'pages.invoice_hotel'], $hotelbookdetailsarray, function($message) use($pdf)
        {

            $message->from('sarbjitphp@netwebtechnologies.com', 'Travo Web');

            $message->to(session()->get('hotelemailnew'))->subject('Hotel Invoice');

            $message->attachData($pdf->output(), "invoice.pdf");
        });

        // Mail::send('pages.hotel-invoice', $hotelbookdetailsarray, function($message) use($pdf)
        // {
        //     $message->from('sarbjitphp@netwebtechnologies.com', 'sarb');

        //     $message->to('sarbjitphp@netwebtechnologies.com')->subject('Invoice');

        //     $message->attachData($pdf->output(), "invoice.pdf");
        // });
        return view('pages.hotel-invoice')->with(compact('hotelbookdetailsarray'));


    }

    public function hotelviewinvoice(Request $request,$bookingid)
    {
        $booking_id=$bookingid;

        $getdata=tbl_hotel_book::where("bookingid",$booking_id)->first();

        $hotelbookdetailsarray=unserialize($getdata->whole_response);

        $booking_creation_date_time=$getdata->created_at;
        $hotelpicture=$getdata->hotel_picture;
        $hotelamount=$getdata->hotelamount;
        $hotelfaicon=$getdata->hotelfaicon;
        $hotelmargingst=$getdata->hotelmargingst;
        $hotelservice_tax=$getdata->hotelservice_tax;
        $hotelmarginprice=$getdata->hotelmarginprice;

        $hotelbookdetailsarray['hotelmarginprice']=$hotelmarginprice;
        $hotelbookdetailsarray['created_on']=$booking_creation_date_time;
        $hotelbookdetailsarray['hotelpicture']=$hotelpicture;
        $hotelbookdetailsarray['hotelamount']=$hotelamount;
        $hotelbookdetailsarray['hotelfaicon']=$hotelfaicon;
        $hotelbookdetailsarray['hotelmargingst']=$hotelmargingst;
        $hotelbookdetailsarray['hotelservice_tax']=$hotelservice_tax;



        return view('pages.hotel-invoice')->with(compact('hotelbookdetailsarray'));


    }
    public function hotel_pdf($hotelbook)
    {
        $booking_id=$hotelbook;

        $getdata=tbl_hotel_book::where("bookingid",$booking_id)->first();

        $hotelbookdetailsarray=unserialize($getdata->whole_response);

        $booking_creation_date_time=$getdata->created_at;
        $hotelpicture=$getdata->hotel_picture;

        $hotelamount=$getdata->hotelamount;
        $hotelfaicon=$getdata->hotelfaicon;
        $hotelmargingst=$getdata->hotelmargingst;
        $hotelservice_tax=$getdata->hotelservice_tax;
        $hotelmarginprice=$getdata->hotelmarginprice;

        $hotelbookdetailsarray['hotelmarginprice']=$hotelmarginprice;
        $hotelbookdetailsarray['created_on']=$booking_creation_date_time;
        $hotelbookdetailsarray['hotelpicture']=$hotelpicture;
        $hotelbookdetailsarray['hotelamount']=$hotelamount;

        $hotelbookdetailsarray['hotelfaicon']=$hotelfaicon;
        $hotelbookdetailsarray['hotelmargingst']=$hotelmargingst;
        $hotelbookdetailsarray['hotelservice_tax']=$hotelservice_tax;

        $pdf = PDF::loadView('pages.hotel_pdf',compact('hotelbookdetailsarray'));

        return $pdf->stream('itsolutionstuf4f.pdf');
    }
    public function pdftest(Request $request)
    {

        $data = array(
            'name' => "sarbjit",
            'email' => "sarbjitphp@netwebtechnologies.com",
            'message' => "testpdf",
        );
        $booking_id='1482106';

        $getdata=tbl_hotel_book::where("bookingid",$booking_id)->first();

        $hotelbookdetailsarray=unserialize($getdata->whole_response);
        session()->put('hotelbookdetailsarray1',$hotelbookdetailsarray);
        $booking_creation_date_time=$getdata->created_at;
        $hotelpicture=$getdata->hotel_picture;
        $hotelemail=$getdata->lead_email;
        $hotelbookdetailsarray['created_on']=$booking_creation_date_time;
        $hotelbookdetailsarray['hotelpicture']=$hotelpicture;
        // return view('pages.invoice_hotel')->with(compact('hotelbookdetailsarray'));
        // $pdf = PDF::loadView('pages.invoice_hotel', $data);
        $pdf = PDF::loadView('pages.hotel_pdf',compact('hotelbookdetailsarray'));

        Mail::send(['html' => 'pages.invoice_hotel'], $hotelbookdetailsarray, function($message) use($pdf)
        {
            $message->from('sarbjitphp@netwebtechnologies.com', 'sarb');

            $message->to('damankumar862@gmail.com')->subject('Invoice');

            $message->attachData($pdf->output(), "invoice.pdf");
        });
    }



    //hotelpayment
    public function request_hotel(Request $request)
    {
        $posts = tbl_payment_order::orderBy('id', 'DESC')->first();
        if(count($posts) == 0)
        {
            $orderid="TRAstt-01";
        }
        else
        {
            $chkid =  $posts->id + 1;
            $orderid="TRAstt-0".$chkid;
        }

        $hoteldata=$request->get('hoteldata');
        $create_date=date('Y-m-d');
        $create_time=date('H:i:s');
        $customerName=$request->get('customername');
        $customerPhone=$request->get('customerPhone');
        $customerEmail=$request->get('customerEmail');

        $customerfname=$request->get('customerfname');
        $customermname=$request->get('customermname');
        $lname=$request->get('customerlname');
        $fname=$customerfname." ".$customermname;

        $username=strtolower($request->get('customerfname').substr($request->get('customerlname'),0,3).mt_rand(1000,9999));
        if(session()->has('travo_useremail'))
        {
            $useremail=session()->get('travo_useremail');
        }
        else
        {
            $checkdata=DB::table('tbl_user')->where('user_email',$customerEmail)->first();
            if(count($checkdata)>0)
            {
                $user_id=$checkdata->user_id;
                $fname1=$checkdata->user_fname;
                $lname1=$checkdata->user_lname;
                $username1=$checkdata->user_name;
                $email=$checkdata->user_email;
                $fullname=$fname1." ".$lname1;
                session(['travo_userid'=>$user_id,'travo_name'=>$fullname,"travo_username"=>$username1,"travo_useremail"=>$email]);
                $useremail=$customerEmail;
            }
            else
            {
                $insertdata=array(
                    "user_fname"=>$fname,
                    "user_lname"=>$lname,
                    "user_phone"=>$customerPhone,
                    "user_email"=>$customerEmail,
                    "user_name"=>$username,
                    "user_status"=>1,
                    "user_create_date"=>$create_date,
                    "user_create_time"=>$create_time);
                $insert=DB::table('tbl_user')->insert($insertdata);
                if($insert)
                {
                    $fullname=$fname." ".$lname;
                    session(['travo_name'=>$fullname,"travo_useremail"=>$email]);
                    $useremail=$customerEmail;
                }
            }
        }

        $request->session()->put('myhotelbooking',$hoteldata);
        $appId="2586c1f86d9026c4ea336d946852";
        $orderId=$orderid;
        $orderAmount=$request->get('amount');
        // $orderCurrency=$request->get('currency');
        $orderNote="Hotel";

        $hotelconvcurr=$request->get('hotelconvcurr');
        if($hotelconvcurr=='IN')
        {
            $orderCurrency='INR';
            $hotelpayment = new tbl_payment_order;
            $hotelpayment->orderid=$orderid;
            $hotelpayment->orderdate=$create_date;
            $hotelpayment->ordertime=$create_time;
            $hotelpayment->loginemail=$useremail;
            $hotelpayment->save();
        }
        else if($hotelconvcurr=='AU')
        {
            $orderCurrency='US';
        }
        else
        {
            $orderCurrency='US';
        }
        $returnUrl="https://travoweb.com/hotel_response";
        $notifyUrl="https://travoweb.com/response1";
        $csrf_token=$request->get('csrf_token');
        $postData = array(
            "appId" => $appId,
            "orderId" => $orderId,
            "orderAmount" => $orderAmount,
            "orderCurrency" => $orderCurrency,
            "orderNote" => $orderNote,
            "customerName" => $customerName,
            "customerPhone" => $customerPhone,
            "customerEmail" => $customerEmail,
            "returnUrl" => $returnUrl,
            "notifyUrl" => $notifyUrl,
        );
        echo $orderCurrency;
        $request->session()->put('postData', $postData);
    }
    public function hotel_response(Request $request)
    {

        $backvalue = request()->all();

        $create_date=date('Y-m-d');
        $create_time=date('H:i:s');
        $orderid = $backvalue['orderId'];
        // $hotelpayment = new tbl_payment_order;
        // $hotelpayment->orderid=$backvalue['orderId'];
        // $hotelpayment->amount=$backvalue['orderAmount'];
        // $hotelpayment->referenceid=$backvalue['referenceId'];
        // $hotelpayment->tx_status=$backvalue['txStatus'];
        // $hotelpayment->payment_mode=$backvalue['paymentMode'];
        // $hotelpayment->tx_msg=$backvalue['txMsg'];
        // $hotelpayment->tx_time=$backvalue['txTime'];
        // $hotelpayment->orderdate=$create_date;
        // $hotelpayment->ordertime=$create_time;
        $paymentorder = tbl_payment_order::where('orderid', $orderid)->update(
            ['amount' => $backvalue['orderAmount'],
                'referenceid' => $backvalue['referenceId'],
                'tx_status' => $backvalue['txStatus'],
                'payment_mode' => $backvalue['paymentMode'],
                'tx_msg' => $backvalue['txMsg'],
                'tx_time' => $backvalue['txTime'],
                'orderdate' => $create_date,
                'ordertime' => $create_time,

            ]);
        if($paymentorder)
        {
            // echo $hotelpayment->tx_status;
            if($backvalue['txStatus']=='SUCCESS')
            {
                $status = $request->session()->put('paymentstatus','paymentsuccess');
                $request->session()->put('paymentref',$backvalue['referenceId']);
                $request->session()->put('paymentorder',$backvalue['orderId']);
                $hotelsms=Cookie::get('gensetting');


                $chdata = session()->get('postData');

                $customerName = $chdata['customerName'];
                $orderCurrency=$chdata['orderCurrency'];
                $customerPhone = $chdata['customerPhone'];
                $customerEmail =  $chdata['customerEmail'];
                $hl = explode("-", $hotelsms->hotel_sms_active);
                $sms_user=$hotelsms->sms_user_id;
                $sms_password=$hotelsms->sms_password;
                $sms_sender=$hotelsms->sms_sender;
                $sms_url=$hotelsms->sms_url;

                if($hl[0]=='1')
                {

                    $leadphone1=$customerPhone;
                    $user=$sms_user;
                    $password=$sms_password;
                    $sender=$sms_sender;

                    $text="Dear ".$customerName.", Your order with order id  ".$orderid." has been successfully placed. Please check your email for details. -Travo Web. ";
                    $otp=urlencode("$text");
                    $data = array(
                        'usr' => $user,
                        'pwd' => $password,
                        'ph'  => $leadphone1,
                        'sndr'  => $sender,
                        'text'  => $otp
                    );

                    $ch = curl_init($sms_url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                    $result = curl_exec($ch);

                }


                $orderms = '<table class="table table-hover">

			    <tbody>

			      <tr>

			        <td>Order ID</td>

			        <td>'.$orderid.'</td>

			      </tr>

			      <tr>

			        <td>Order Amount</td>

			        <td>'.$backvalue['orderAmount'].'</td>

			      </tr>

			      <tr>

			        <td>Reference ID</td>

			        <td>'.$backvalue['referenceId'].'</td>

			      </tr>
				</tbody>

			</table>';
                $to=$customerEmail;
                $subject="Travo Web Order Placed";
                $message="Dear ".$customerName." <br>Your order with order id ".$orderid." has been successfully placed <br>".$orderms." ";
                $header="From:ticket@travoweb.com \r \n";
                $header = "From: " . strip_tags('ticket@travoweb.com') . "\r\n";
                $header .= "Reply-To: ". strip_tags('ticket@travoweb.com') . "\r\n";
                $header .= "CC: 'ticket@travoweb.com'\r\n";
                $header .= "MIME-Version: 1.0\r\n";
                $header .= "Content-Type: text/html; charset=UTF-8\r\n";
                $mailtest =mail($to,$subject,$message,$header);

                // echo "<script>alert('success ');</script>";

                return redirect()->action('HotelController@hotelbooking');
            }
            else
            {

                $status =$request->session()->put('paymentstatus','paymentfail');
                echo "<script>alert('Payment Failed ');</script>";
            }


        }
        else
        {

            $status =$request->session()->put('paymentstatus','paymentfail');
            echo "<script>alert('Payment Failed ');</script>";

        }

    }
    public function hotelcancel(Request $request)
    {
        if(session()->has('travo_useremail'))
        {
            $useremail=session()->get('travo_useremail');
        }
        else
        {
            $useremail=$request->get('useremail');
        }

        $bookingid = $request->get('bookingid');
        $remark = $request->get('remark');
        $requesttype = $request->get('requesttype');
        $TokenId=Cookie::get('TokenId');
        $EndUserIp=Cookie::get('localip');
        $form_cancel = array(
            'EndUserIp'=>$EndUserIp,
            'TokenId'=>$TokenId,
            'BookingId'=>$bookingid,
            'RequestType'=>$requesttype,
            'Remarks'=>$remark,

        );

        $data_hotel_cancel = json_encode($form_cancel);
        $ch = curl_init('https://booking.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/SendChangeRequest/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_hotel_cancel);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: '.strlen($data_hotel_cancel)) );
        $result_hotel_cancel = curl_exec($ch);
        $result_cancel=json_decode($result_hotel_cancel,true);
        $cancel_data= serialize($result_cancel['HotelChangeRequestResult']);
        $errormsg = $result_cancel['HotelChangeRequestResult']['Error']['ErrorMessage'];
        if($errormsg=="")
        {
            $errormsg1="";
        }
        else
        {
            $errormsg1=$result_cancel['HotelChangeRequestResult']['Error']['ErrorMessage'];
        }
        $changerequestid=$result_cancel['HotelChangeRequestResult']['ChangeRequestId'];
        $changstatus = $result_cancel['HotelChangeRequestResult']['ChangeRequestStatus'];
        date_default_timezone_set("Asia/Kolkata");
        $create_date=date('Y-m-d');
        $create_time=date('H:i:s');
        $hotel_cancel = tbl_mytrip::where('bookingid', $bookingid)->update(
            ['cancel_charges' => $result_cancel['HotelChangeRequestResult']['CancellationChargeBreakUp'],
                'cancel_service_charges' => $result_cancel['HotelChangeRequestResult']['TotalServiceCharge'],
                'change_request_id' => $changerequestid,
                'changerequest_status' => $changstatus,
                'cancel_error' => $errormsg1,

                'cancel_date' => $create_date,
                'cancel_time' => $create_time,
                'cancelstatus'=>'1',

            ]);

        // print_r($hotel_cancel);
        if($hotel_cancel)
        {

            $hotelc = tbl_hotel_book::where('bookingid', $bookingid)->update(['hotel_cancel' => '1']);
            $exist= DB::table('tbl_user')->where('user_email', $useremail)->first();
            //sms
            $hotelsms=Cookie::get('gensetting');
            $hl = explode("-", $hotelsms->hotel_sms_active);
            $sms_user=$hotelsms->sms_user_id;
            $sms_password=$hotelsms->sms_password;
            $sms_sender=$hotelsms->sms_sender;
            $sms_url=$hotelsms->sms_url;
            if($hl[2]=='1')
            {

                $leadphone1=$exist->user_phone;
                $user=$sms_user;
                $password=$sms_password;
                $sender=$sms_sender;

                $text="Dear ".$exist->user_fname." ".$exist->user_lname." Your hotel booking has been cancelled. your change request id is ".$changerequestid." ";
                $otp=urlencode("$text");
                $data = array(
                    'usr' => $user,
                    'pwd' => $password,
                    'ph'  => $leadphone1,
                    'sndr'  => $sender,
                    'text'  => $otp
                );
                $ch = curl_init($sms_url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                $result = curl_exec($ch);
            }
            //endsms
            $to=$useremail;
            $subject="Hotel Cancel";
            $message="Dear ".$exist->user_fname." ".$exist->user_lname." <br>Your hotel booking has been cancelled. your change request id is ".$changerequestid." Refund amount will be transferred to your account within 7-15 working days  ";
            $header="From:ticket@travoweb.com \r \n";
            $header = "From: " . strip_tags('ticket@travoweb.com') . "\r\n";
            $header .= "Reply-To: ". strip_tags('ticket@travoweb.com') . "\r\n";
            $header .= "CC: 'ticket@travoweb.com'\r\n";
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-Type: text/html; charset=UTF-8\r\n";
            $mailtest =mail($to,$subject,$message,$header);
            echo "successfull%%".$errormsg."%%".$changerequestid."";
        }
        else
        {
            echo "fail";
        }
        // if($result_cancel['HotelChangeRequestResult']['Error']['ErrorCode']!=0)
        // {
        // 	echo $errormsg = $result_cancel['HotelChangeRequestResult']['Error']['ErrorMessage'];
        // }

    }
    //hotel canceldetail
    public function hotelcanceldetail(Request $request,$chnagerequestid)
    {
        $ChangeRequestId=$chnagerequestid;
        $hotelcanceldetail = tbl_mytrip::where('change_request_id', $ChangeRequestId)->where('cancelstatus','1')->first();
        return view('pages.cancel_details')->with(compact('hotelcanceldetail'));



    }
    //end hotel cancel
    public function price($amount,$servicetax,$tdsa,$gsta,$mainp)
    {

        $pricecurrency = 'fa-inr';  //$ fa-usd
        // echo Cookie::get('country');

        if(Cookie::get('country') == 'IN'){
            $priceamount = $amount;
            $pservicetax = $servicetax;
            $ptds=$tdsa;
            $pgsta=$gsta;
            $pricecurr = 'IN';
            $gsthotel=($amount*5)/100;
            $mprice=$mainp;

        }
        else
        {
            if(Cookie::get('country') == 'AU' || Cookie::get('country') == 'NZ'){

                // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'AUD')->first();
                // if(!empty($dataExsists)){

                //     $dataExsists->amount = $this->convert('AUD','1');
                //     $dataExsists->save();
                // }
                $price = Price::where('currency', '=', 'AUD')->first();
                $priceamount = ($price->amount*$amount)*100;
                $pservicetax = ($price->amount*$servicetax)*100;
                $ptds=($price->amount*$tdsa)*100;
                $pgsta=($price->amount*$gsta)*100;
                $gsthotel1=($amount*10)/100;
                $gsthotel = ($price->amount*$gsthotel1)*100;

                $mprice = ($price->amount*$mainp)*100;
                $pricecurr = 'AU';
            }else{
                // $dataExsists =  Price::where('updated_at', '<=', date('Y-m-d H:i:s',strtotime("-1 hours.") ))->where('currency', '=', 'USD')->first();
                // if(!empty($dataExsists)){
                //     //$price = Price::where('currency', '=', 'USD')->first();
                //     $dataExsists->amount = $this->convert('USD','1');
                //     $dataExsists->save();
                // }
                $price = Price::where('currency', '=', 'USD')->first();
                //echo $price->amount; die;
                $priceamount = ($price->amount*$amount)*100;
                $pservicetax = ($price->amount*$servicetax)*100;
                $ptds=($price->amount*$tdsa)*100;
                $pgsta=($price->amount*$gsta)*100;
                $gsthotel1=($amount*10)/100;
                $gsthotel = ($price->amount*$gsthotel1)*100;
                $mprice = ($price->amount*$mainp)*100;
                $pricecurr = 'US';
            }
            $pricecurrency = 'fa-usd';
        }
        //echo $priceamount;  die;
        return [$priceamount,$pricecurrency,$pricecurr,$pservicetax,$ptds,$pgsta,$gsthotel,$mprice];
    }

    public function get_current_location(){
        //return "USD";
        if(Cookie::get('country')!== null){
            return Cookie::get('country');
        }else{

            $url = "http://www.geoplugin.net/php.gp?ip=".$_SERVER['REMOTE_ADDR'];
            $request = curl_init();
            $timeOut = 0;
            curl_setopt ($request, CURLOPT_URL, $url);
            curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
            curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
            $response = curl_exec($request);
            curl_close($request);
            if(!empty($response)){
                $response = unserialize($response);
                if(!empty($response['geoplugin_status']) && $response['geoplugin_status'] == '200'){
                    Cookie::queue('country',$response['geoplugin_countryCode'], 1440);
                    return $response['geoplugin_countryCode'];
                }else{
                    Cookie::queue('country','IN', 1440);
                    return "IN";
                }
            }else{
                Cookie::queue('country','IN', 1440);
                return "IN";
            }
        }


        //$loc = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
        //print_r($response); die;
    }

    //  public function convert($to,$amount){
    //     // Cookie::get('country');
    //   	// if(Cookie::get('country') != '')
    //   	// {
    //       $from = 'INR';
    //       $url = "https://www.google.com/search?q=".$from.$to;
    //       $request = curl_init();
    //       $timeOut = 0;
    //       curl_setopt ($request, CURLOPT_URL, $url);
    //       curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
    //       curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
    //       curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
    //       $response = curl_exec($request);
    //       curl_close($request);

    //       preg_match('~<span [^>]* id="knowledge-currency__tgt-amount"[^>]*>(.*?)</span>~si', $response, $finalData);


    //       return floatval((floatval(preg_replace("/[^-0-9\.]/","", $finalData[1]))/100) * $amount);
    //   // }
    // }
    public function stripepayment(Request $request)
    {
        $useremail=session()->get('travo_useremail');
        $chdata = session()->get('postData');
        $customerName = $chdata['customerName'];
        $orderAmount=$chdata['orderAmount'];
        $create_date=date('Y-m-d');
        $create_time=date('H:i:s');
        $posts = tbl_payment_order::orderBy('id', 'DESC')->first();
        if(count($posts) == 0)
        {
            $orderid="TRAstt-01";
        }
        else
        {
            $chkid =  $posts->id + 1;
            $orderid="TRAstt-0".$chkid;
        }
        $hotelpayment = new tbl_payment_order;
        $hotelpayment->orderid=$orderid;
        $hotelpayment->orderdate=$create_date;
        $hotelpayment->ordertime=$create_time;
        $hotelpayment->loginemail=$useremail;
        $hotelpayment->save();
        \Stripe\Stripe::setApiKey("pk_live_LptMMIsCPx0fqef2pi3tlAXk");
        $params = array(
            "testmode"   => "off",
            "private_live_key" => "sk_live_2QLNRClFjDAFUR56avW3gvun",
            "public_live_key"  => "pk_live_LptMMIsCPx0fqef2pi3tlAXk",
            "private_test_key" => "sk_test_yICHd12qJq7YEret7hAxqTWj",
            "public_test_key"  => "pk_test_gdcMtVAnKmNzBAuQpSqVsOtj"
        );

        if ($params['testmode'] == "on") {
            \Stripe\Stripe::setApiKey($params['private_test_key']);
            $pubkey = $params['public_test_key'];
        } else {
            \Stripe\Stripe::setApiKey($params['private_live_key']);
            $pubkey = $params['public_live_key'];
        }

        if(isset($_POST['stripeToken']))
        {
            $daamount=$orderAmount * 100;
            $amount_cents = str_replace(".","",$daamount);  // Chargeble amount
            $invoiceid = $orderid;                      // Invoice ID
            $description = "Invoice #" . $invoiceid . " - " . $invoiceid;

            try {
                $charge = \Stripe\Charge::create(array(
                        "amount" => $amount_cents,
                        "currency" => "usd",
                        "source" => $_POST['stripeToken'],
                        "description" => $description)
                );

                // if ($charge->card->address_zip_check == "fail") {
                //     throw new Exception("zip_check_invalid");
                // } else if ($charge->card->address_line1_check == "fail") {
                //     throw new Exception("address_check_invalid");
                // } else if ($charge->card->cvc_check == "fail") {
                //     throw new Exception("cvc_check_invalid");
                // }
                // Payment has succeeded, no exceptions were thrown or otherwise caught

                $result = "success";

            } catch(\Stripe\Stripe_CardError $e) {

                $error = $e->getMessage();
                $result = "declined";

            } catch (\Stripe\Stripe_InvalidRequestError $e) {
                $result = "declined";
            } catch (Stripe_AuthenticationError $e) {
                $result = "declined";
            } catch (\Stripe\Stripe_ApiConnectionError $e) {
                $result = "declined";
            } catch (\Stripe\Stripe_Error $e) {
                $result = "declined";
            } catch (Exception $e) {

                if ($e->getMessage() == "zip_check_invalid") {
                    $result = "declined";
                } else if ($e->getMessage() == "address_check_invalid") {
                    $result = "declined";
                } else if ($e->getMessage() == "cvc_check_invalid") {
                    $result = "declined";
                } else {
                    $result = "declined";
                }
            }
            $orderamount = $charge->amount;
            $tstam=$orderamount/100;
            $id = $charge->id;
            "</br>";
            $tstam;
            "</br>";
            $charge->receipt_url;
            $paymentorder = tbl_payment_order::where('orderid', $orderid)->update(
                ['amount' => $tstam,
                    'referenceid' => $charge->id,
                    'tx_status' => $result,
                    'payment_mode' => "Stripe",
                    'receipt_url'=>$charge->receipt_url,
                    'balance_trans'=>$charge->balance_transaction,
                    'orderdate' => $create_date,
                    'ordertime' => $create_time,

                ]);
            if($paymentorder)
            {
                // echo $hotelpayment->tx_status;
                if($result=='success')
                {
                    $status = $request->session()->put('paymentstatus','paymentsuccess');
                    $request->session()->put('paymentref',$charge->id);
                    $request->session()->put('paymentorder',$orderid);


                    // echo "<script>alert('success ');</script>";

                    return redirect()->action('HotelController@hotelbooking');
                }
                else
                {

                    $status =$request->session()->put('paymentstatus','paymentfail');
                    echo "<script>alert('Payment Failed ');</script>";
                }


            }
            else
            {

                $status =$request->session()->put('paymentstatus','paymentfail');
                echo "<script>alert('Payment Failed ');</script>";

            }

        }
    }
    //endcurencyconver
} //mainfunction close
