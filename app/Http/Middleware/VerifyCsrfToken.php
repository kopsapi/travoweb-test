<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
    	'response',
    	'hotel_response',
        'stripepayment',
        'flightstripepayment',
        'results',
        'package_response',
        'packagestripepayment',
    
        // "http://travoweb.com/apiflight/response",
    ];
}
