<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tour_package extends Model
{
    protected $table = 'tbl_tour_package';
    protected $fillable = [
        'id', 'pckg_id','countryvalue', 'countryname','packagename','pckg_order',
    ];
}
