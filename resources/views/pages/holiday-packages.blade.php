@include('pages.include.header')

<body>
    <style>
        .intro {
            width: 100%;
            padding-top: 100px;
            padding-bottom: 0px;

        }

        .add_content {
            z-index: 9;
        }

        .about-header {
            background: url("{{asset('assets/images/about-h.jpg')}}");
            height: 450px;
            margin-top: 126px;
            position: relative;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center center;
        }

        h1.about-title {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
            background: rgba(49, 18, 75, 0.8);
            padding: 10px 35px;
        }

        .intro_title {
            text-align: center;
        }

        ul.n-ul {
            margin-top: 30px;
            text-align: left;
        }

        ul.n-ul li {
            padding: 10px 20px;
            color: #9555ef;
            font-size: 15px;
            font-weight: 500;
        }

        ul.n-ul li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
            font-size: 20px;
            font-weight: 900;
        }

        .intro {
            background: white;
        }

        h1.h1-title {
            color: white;
            font-size: 20px;
        }

        .milestones {
            background: white;
        }

        .content-overlay {
            background: rgba(49, 18, 75, 0.8);
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 1;
        }

        p.p-para {
            font-size: 21px;
            color: #ffffff;
            background: linear-gradient(to right, #fa9e1b, #8d4fff, #fa9e1b);
            padding: 10px;
            border-radius: 5px;
        }

        ul.points {

            text-align: initial;
        }

        .column {
            column-count: 2;
        }

        ul.points li {
            padding: 10px;
            color: #8f50fb;
            font-size: 16px;
            font-weight: 600;
        }

        ul.points li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
        }

        .milestone_icon img {
            width: auto;
            height: 50PX;
        }

        @media screen and (max-width:768px) {
            .column {
                column-count: 1;
            }

            ul.points {
                text-align: left !important;
            }
        }

        div.cstm-accordion .card-header {
            /* background: black; */
            font-size: 21px;
            color: #ffffff;
            background: white;
            padding: 10px 30px;
            border-radius: 5px;
            border-radius: 2px;
            border: none;
        }

        div.cstm-accordion .card-header a {
            color: #fa9e1b;
            font-size: 15px;
            display: block;
            text-align: center;
            font-weight: 500;
            text-transform: uppercase;
        }

        div.cstm-accordion .card {
            /* border: none !IMPORTANT; */
            margin-bottom: 10px;
            border: 1px solid #dadada;
        }

        .heading-p {
            font-size: 17px;
            color: #fa9e1b;
            font-weight: 700;
            text-transform: uppercase;
        }
        .search {
    width: 100%;
    height: auto;
    /* background: linear-gradient(to right, #fa9e1b, #8d4fff); */
    z-index: 9;
    position: relative;
    top: 0;
    margin-bottom: 80px;
}
.search_tabs_container {
    position: relative;
    bottom: 100%;
    left: 0;
    width: 100%;
}
table.d-table tr td {
    padding: 10px;
    border: 1px solid #e0e0e0;
    color: black;
}
table.d-table {
    width: 100%;
    background: white;
}
table.d-table tr th {
    background: #4a3a7c;
    color: white;
    padding: 15px;
    border: 1px solid #4a3a7c;
}
.search_panel.active {
    background: #ffffff !IMPORTANT;
    border: 1px solid #cacaca;
    height: 500px;
    overflow: auto;
}
.search_tab:first-child {
    border-top-left-radius: 0;
    padding-left: 7px;
}
.search_tab:last-child {
    border-top-right-radius: 0;
    padding-right: 7px;
}
/* width */
.search_panel.active::-webkit-scrollbar {
    width: 7px;
}
.search_panel.active::-webkit-scrollbar-thumb {
    background: #7961c4;
    border-radius: 30px;
}
.search_panel.active::-webkit-scrollbar-track {
    background: #e4dbff;
}

/* Handle on hover */
.search_panel.active::-webkit-scrollbar-thumb:hover {
  background: #6652a5;
}
.search_tab.d-flex.flex-row.align-items-center.justify-content-lg-center.justify-content-start.active {
    background: #fa9e1b;
}
    </style>
    <div class="super_container">
        <!-- Header -->
        @include ('pages.include.topheader')
        <div class="about-header">
            <h1 class="about-title">Holiday Packages</h1>
        </div>
        <!-- Intro -->
        <div class="intro">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12">
                        <div class="intro_content">
                            <div class="intro_title">Holiday Packages</div>

                           
                        </div>
                      
                    </div>
                </div>
            </div>

            <!-- Footer -->

            @include ('pages.include.footer')
            @include ('pages.include.copyright')
        </div>
    </div>
</body>

</html>