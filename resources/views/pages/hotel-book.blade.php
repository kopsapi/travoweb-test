@include('pages.include.header')

<body>
	<style>
		.flight-book-list .flight-depart .fa-plane
		{
			color: #fa9e1b;
			font-size: 16px;
		}
		.flight-book-list .flight-arrival .fa-plane
		{
			color: #fa9e1b;
			font-size: 16px;
		}
		.hotel-room-result
		{
			padding:30px 0px;
		}
		.hotel-room-result .caption .hotel-address small
		{
			font-size:14px;
			margin-bottom:10px;
			font-weight: 600;
		}
		.hotel-room-result .caption .check-in
		{

		}
		.hotel-room-result .caption .hotel-address
		{
			margin-bottom:5px;
			color:#333;
		}
		.hotel-room-result .caption .check-in strong, .hotel-room-result .caption .check-out strong
		{
			color:#fa9e1b;
		}
		.hotel-room-result .caption .check-in, .hotel-room-result .caption .check-out
		{
			color: #333;
			font-weight: 600;
			margin-bottom: 5px;
			line-height: 26px;
		}
		.hotel-room-result .caption .search-filter
		{
			line-height: 30px;
			margin-bottom: 5px;
		}
		.hotel-room-result .caption .search-filter span
		{
			padding: 6px 10px;
			border-radius: 4px;
			font-size: 12px;
		}
		.hotel-room-result .caption h3
		{
			color: #00206A;
			font-size: 20px;
			font-family: calibri;
			margin-bottom: 5px;
			line-height: 30px;
			font-weight: 600;
		}
		.hotel-room-result .caption .fa-star
		{
			padding: 2px;
			color: rgba(255, 179, 2, 1);
		}
		.alert
		{
			text-align: center;
			font-weight: 600;
		}
		.alert-danger a
		{
			text-decoration: none;
			color: #00206A;
		}
		.hotel-room-result .room-type-details
		{
			border: 1px solid #00206A;
			padding: 20px;
			border-radius: 4px;
			margin-bottom: 20px;
		}
		.hotel-room-result .room-type-details h4
		{
			color:#00206A;
			margin-bottom:5px;
			font-weight:600;
		}
		.hotel-room-result .room-type-details small
		{
			color: #fa9e1b;
			font-size: 14px;
			font-weight: 600;
		}
		.hotel-room-result .room-type-details label
		{
			text-transform: uppercase;
			color: #fa9e1b;
			font-weight: bold;
			font-size: 12px;
		}
		.hotel-room-result .room-type-details .tariff-type li:nth-child(1)
		{
			margin-left:0px;
		}
		.hotel-room-result .room-type-details .tariff-type li.ref
		{
			color:green;
		}
		.hotel-room-result .room-type-details .tariff-type li.non-ref
		{
			color:red;
		}
		.hotel-room-result .room-type-details .tariff-type li
		{
			display:inline;
			margin-left:50px;
			position:relative;
			color:#333;
			padding-left:20px;
			font-weight:600;
		}
		.hotel-room-result .room-type-details hr
		{
			margin-top: 10px;
			margin-bottom: 10px;
		}
		.hotel-room-result .room-type-details .tariff-type li:before
		{
			content: "\f00c";
			font-family: FontAwesome;
			font-style: normal;
			font-weight: normal;
			text-decoration: inherit;
			color: #333;
			font-size: 14px;
			position: absolute;
			top: 0px;
			left: 0;
		}
		.booking-traveller-details .form-control::-webkit-input-placeholder,
		.booking-traveller-details .form-control::-webkit-input-placeholder
		{
			color:#CCC !important;
			font-size:14px;
		}
		.booking-traveller-details
		{
			margin-bottom:20px;
		}
		.booking-traveller-details label
		{
			color:#00206A;
			font-weight:600;
			font-size: 13px;
		}
		.booking-traveller-details .traveler
		{
			color: #FFF;
			font-weight: 600;
			margin-bottom: 20px;
			background: #fa9e1b;
			font-family: calibri;
			padding: 8px 20px;
			border-radius: 5px;
		}
		.booking-traveller-details .gst-checkbox h4
		{
			color:#fa9e1b;
			font-size: 16px;
			font-weight: 600;
		}
		.special-request
		{
			margin-bottom:10px;
		}
		.special-request .request
		{
			color: #FFF;
			font-weight: 600;
			font-family: calibri;
			margin-bottom: 20px;
			background: #fa9e1b;
			padding: 8px 20px;
			border-radius: 5px;
		}
		.special-request .commonly
		{
			margin-bottom: 15px;
		}
		.special-request .commonly h5
		{
			text-transform:uppercase;
			color: #00206A;
			font-weight: 600;
		}
		.special-request .request
		{
			color: #FFF;
			font-weight: 600;
			margin-bottom: 20px;
			background: #fa9e1b;
			padding: 8px 20px;
			border-radius: 5px;
		}
		.special-request textarea
		{
			height:100px;
		}
		.special-request label
		{
			color: #00206A;
			font-weight: 600;
			font-size: 13px;
		}
		.hotel-policies h4
		{
			color: #FFF;
			font-weight: 600;
			margin-bottom: 20px;
			background: #fa9e1b;
			padding: 8px 20px;
			border-radius: 5px;
		}
		.hotel-policies .read-more
		{
			color:#fa9e1b;
			text-decoration:none !important;
		}
		.hotel-policies .read-more:hover
		{
			background:transparent;
			text-decoration:underline;
		}
		.form_submit_button
		{
			margin-top:0px;
		}

		@media screen and (max-width: 992px){
			button#form_submit_button {
				margin-bottom: 45px;
			}

		}
	</style>
	<style>

		.payment-card {
			background: white;
			padding: 20px 45px 30px;
			border: 1px solid #cccccc;
			margin-bottom: 25px;
			margin-top: 45px;
		}

		img.pay-img {

			width: 100%;

			height: auto;

			min-height: 100%;

			object-fit: cover;

			border-top-left-radius: 10px;

			border-bottom-left-radius: 10px;

		}

		.payment-section {

			margin-top: 4%;

			border: 1px solid #bebebe;

			margin-left: 130px;

			margin-right: 130px;

			border-top-left-radius: 10px;

			border-bottom-left-radius: 10px;

			box-shadow: 0 10px 25px 0 rgba(0, 0, 0, 0.4);

		}

		div.bt_title {

			color: black;

			text-align: center;

		}

		.pay-input{

			border-radius: 0;

			margin-bottom: 15px;

		}



		.pay-input:focus{

			border: none;

			border: 1px solid #01729e;

			border-radius: 0;

			box-shadow: none;

		}

		label.pay-label {

			color: #01729e;

			font-weight: 500;

		}

		input.submit.pay-btn {

			border: none;

			border: 1px solid;

			background: #01729e;

			color: white;

			padding: 10px 20px;

			display: block;

			margin: 20px auto 0;

		}



		span.span-1 {

			text-align: left;

			display: block;

			padding: 10px 20px;

			color: #ffc107;

			font-weight: 500;

		}

		span.span-2 {

			text-align: right;

			display: block;

			padding: 10px;

			color: #ffc107;

			font-weight: 500;

		}



		.icon-container {

			margin-bottom: 20px;

			padding: 7px 0;

			font-size: 24px;

		}

		.icon-container i{

			font-size: 30px;

		}

		h3.bt_title {
			text-align: center;
			margin-bottom: 30px;
			color: #fff;
			background: #1a86b0;
			padding: 10px 14px;
			margin-left: auto;
			margin-right: auto;
			border-radius: 5px;
			box-shadow: 0 4px 10px 0 rgba(19, 19, 19, 0.2), 0 4px 20px 0 rgba(0, 0, 0, 0.16);
		}
		.home{
	 height: 145px;
}
@media screen and (max-width:600px){
	.home{
	 height: 40px;
}
img.img.img-responsive {
    width: 100%;
    height: 150px;
}

}

	</style>
	<div class="super_container">
		<!-- Header -->@include('pages.include.topheader')
		<div class="home">
			<!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->
			<div class="home_content">
				<div class="home_title">Results </div>
			</div>
		</div>
	
		<!-- Intro -->
		<?php
			 $params = array("testmode" => "off",
	    "private_live_key" => "sk_live_2QLNRClFjDAFUR56avW3gvun",
	    "public_live_key" => "pk_live_LptMMIsCPx0fqef2pi3tlAXk",
	    "private_test_key" => "sk_test_yICHd12qJq7YEret7hAxqTWj",
	    "public_test_key" => "pk_test_gdcMtVAnKmNzBAuQpSqVsOtj");

			?>
		<div class="flight-book">
			<div class="intro">
			<!-- <section class="flight-book-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="intro_content">
								<div class="row">
									<div class="col-lg-2 res-mb-20">
										<h4><i class="fa fa-map-marker"></i> Amritsar</h4> </div>
									<div class="col-lg-1 res-mb-20"> <i class="fa fa-plane flight-i fa-rotate-45"></i> </div>
									<div class="col-lg-2 res-mb-20">
										<h4><i class="fa fa-map-marker"></i> Delhi</h4> </div>
									<div class="col-lg-2 res-mb-20">
										<h4><i class="fa fa-calendar"></i> 30 Feb 2019 </h4> </div>
									<div class="col-lg-2 res-mb-20">											<h4><i class="fa fa-calendar"></i> 31 Feb 2019</h4>										</div>										<div class="col-lg-2 res-mb-20">											<h4 class="flight-class"><small>ADULTS</small> 2 <small>CHILD</small> 0 </h4>										</div>
									<div class="col-lg-3 res-mb-20 res-none"> &nbsp; </div>
									<div class="col-lg-2">
										<button type="button" class="btn btn-refundable">Refundable</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section> -->
		

			@if($hotelblockroomarray['Error']['ErrorCode']==0)
			<section class="hotel-room-result">
				<div class="container">
					<div class="row">
						<div class="col-lg-8">
							<div class="row">
								<div class="col-lg-3">
									<img src="{{$selectedhotel['HotelPicture']}}" class="img img-responsive">
								</div>
								<div class="col-lg-9">
									<div class="caption">
										
										<h3>{{$selectedhotel['HotelName']}} 
											<div class="rating-div">
												@for($star=1;$star<=$selectedhotel['StarRating'];$star++)
												<span class="fa fa-star"></span>
											@endfor
											</div>
											</h3>
										<p class="hotel-address"><small>{{$selectedhotel['HotelAddress']}}{{Session::get('dataarray')['City']}}</small></p>
										<div class="search-filter">
											<span class="badge-success">Travoweb</span>
											<!-- <span class="badge-info">Couple Friendly</span> -->
										</div>
										<p class="check-in"><strong>Check-in : </strong><?php echo date('d F, l Y',strtotime(Session::get('dataarray')['checkin'])); ?></p>
										<p class="check-out"><strong>Check-Out : </strong><?php echo date('d F, l Y',strtotime(Session::get('dataarray')['checkout'])); ?></p>
									</div>
								</div>
							</div>
							<!-- 	<div class="alert alert-danger"> This is a Non Refundable tariff. Here's the <a href="javascript:void()">Full Policy</a> </div> -->
							<?php
							session(['hotelroomnn'=>$selectedhotel['HotelName'],'hotelroomntype'=>$hotelblockroomarray['HotelRoomsDetails'][0]['RoomTypeName']]);
							?>
							@for($i=0;$i< count($hotelblockroomarray['HotelRoomsDetails']);$i++)
							<h5>Room {{$i+1}}</h5>
							<div class="room-type-details">
								<div class="row">
									<div class="col-lg-12">
										<div class="row">
											<div class="col-lg-4">
												<h4>{{$hotelblockroomarray['HotelRoomsDetails'][$i]['RoomTypeName']}} </h4>
												<small>{{Session::get('dataarray')['adult_array'][$i]}} Adults {{Session::get('dataarray')['child_array'][$i]}} Children</small>
											</div>
											<div class="col-lg-8">
												<label>Tariff Type</label>
												<ul class="tariff-type">
													<!-- <li class="non-ref">Non Refundable</li> -->
													<li>Room Only</li>
												</ul>
											</div>
										</div>
										<hr>
										<div class="row">
											<div class="col-lg-12">
												<label>Tariff Inclusion</label>
												<ul class="tariff-type">
													@foreach($hotelblockroomarray['HotelRoomsDetails'][$i]['Inclusion'] as $inclusion)
													<li>{{$inclusion}}</li>
													@endforeach
												</ul>
											</div>
										</div>
									</div>
									<!-- <div class="col-lg-3"> -->
										<!-- 	<img src="{{$selectedhotel['HotelPicture']}}"  class="img img-responsive"> -->
										<!-- </div> -->
									</div>
								</div>
								@endfor
								<form id="hotelbooking_submit">
									<!-- {{csrf_field()}} -->
									 <input name="_token" type="hidden" id="csrf_token" value="{{ csrf_token() }}"/>
									<input type="hidden" name="resultindex" value="{{$selectedhotel['ResultIndex']}}">
									<input type="hidden" name="hotelname" value="{{$selectedhotel['HotelName']}}">
									<input type="hidden" name="hotelcode" value="{{$selectedhotel['HotelCode']}}">
									<input type="hidden" name="roomindexes" value="{{$roomindexes}}">
									<input type="hidden" name="amount" id="grandtotal" >
									<input type="hidden" name="hotelmarginprice" id="hotelmarginprice" value="<?php echo round($hotelblockroomarray['hotelmargin']) ?>">
									<input type="hidden" name="hotellastprice" id="hotellastprice" value="<?php echo round($hotelblockroomarray['hotellastprice']) ?>">
									<input type="hidden" name="hotelconvcurr" id="hotelconvcurr" value="<?php echo $hotelblockroomarray['hotelconvercurr'] ?>">
									<input type="hidden" name="hotelmaingst" id="hotelmaingst" value="<?php echo round($hotelblockroomarray['hotelmaingst']) ?>">	
									<input type="hidden" name="hotelservicetax" id="hotelservicetax" value="<?php echo round($hotelblockroomarray['hotelservicetax']) ?>">	
									<input type="hidden" name="currencyicon" id="currencyicon" value="<?php echo $hotelblockroomarray['hotelmaincurrecny'] ?>">
									<input type="hidden" name="currency" id="currency" value="{{$selectedhotel['Price']['CurrencyCode']}} ">
									<div class="booking-traveller-details">
										<h4 class="traveler"> Lead Traveller Information</h4>
										<h5>Room 1 (Adult 1)</h5>
										<?php
										$trav_name = session()->get('travo_name');
										$splitName = explode(' ', $trav_name); 
										$first_name = $splitName[0];
										$last_name = !empty($splitName[1]) ? $splitName[1] : ''; 
										
										$ltitle = session()->get('travo_title');
										?>
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label><span style="color: red">*</span> Title</label>
													<select name="lead_passenger_title" id="title" class="form-control leadtitle">
														<option value="Mr" <?php if($ltitle=='Mr'){echo "Selected";} ?>>Mr</option>
														<option value="Miss" <?php if($ltitle=='Ms'){echo "Selected";} ?>>Miss</option>
														<option value="Mrs" <?php if($ltitle=='Mrs'){echo "Selected";} ?>>Mrs</option>
													</select>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label><span style="color: red">*</span> First Name</label>
													<input type="text" name="lead_passenger_first" id="firstname" maxlength="50" class="form-control fillable leadfirstname" placeholder="Enter First Name" value="{{$first_name}}">
													<span class="textfield_error"></span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label> Middle Name</label>
													<input type="text" name="lead_passenger_middle"  id="middlename" maxlength="50" class="form-control " placeholder="Enter Middle Name">
													<span class="textfield_error"></span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label><span style="color: red">*</span> Last Name</label>
													<input type="text" name="lead_passenger_last" id="lastname"  maxlength="50" class="form-control fillable leadlastname" placeholder="Enter Last Name" value="{{$last_name}}">
													<span class="textfield_error"></span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label><span style="color: red">*</span> Age</label>
													<input type="text" name="lead_passenger_age" maxlength="2" class="form-control fillable leadage" placeholder="Enter Age">
													<span class="textfield_error"></span>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label><span style="color: red">*</span> Email ID (Booking confirmation sent to this email ID)</label>
													<input id="email-filter" type="text" name="lead_passenger_email" maxlength="50" class="form-control fillable email leademail" placeholder="Enter Email ID" value="{{session()->get('travo_useremail')}}">
													<span class="textfield_error"></span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label><span style="color: red">*</span> Contact Info</label>
													<input type="text" name="lead_passenger_contact" id="phone" maxlength="15"  class="form-control fillable" placeholder="Enter Mobile No." value="{{session()->get('travo_phone')}}">
													<span class="textfield_error" ></span>
												</div>
											</div>
											<input type="hidden" id="passvalid" name="lead_passvalid" value="{{$hotelblockroomarray['HotelRoomsDetails'][0]['IsPANMandatory']}}">
											
											@if($hotelblockroomarray['HotelRoomsDetails'][0]['IsPANMandatory'] =='1')
											<div class="col-md-3">
												<div class="form-group">
													<label><span style="color: red">*</span> Pan Card</label>
													<input type="text" name="lead_passenger_pancard" id="pancardno" maxlength="11"  class="form-control fillable" placeholder="Enter Pan Card No" >
													<span class="textfield_error" ></span>
												</div>
											</div>
											@endif
											@if($hotelblockroomarray['HotelRoomsDetails'][0]['IsPANMandatory'] =='1')
											<div class="col-md-3">
												<div class="form-group">
													<label><span style="color: red">*</span>Passport No</label>
													<input type="text" name="lead_passenger_passport" id="passportno"   class="form-control fillable" placeholder="Enter Passport No" >
													<span class="textfield_error" ></span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label><span style="color: red">*</span>Passport Date</label>
													<input type="text" name="lead_passenger_passport_date" id="passportdate"   class="form-control fillable passportdate" placeholder="Enter Passport No" >
													<span class="textfield_error" ></span>
												</div>
											</div>
											@endif

										</div>
										<hr>
										<h4 class="traveler"> Emergency Contact</h4>
										<div class="row">
											
											<div class="col-md-3">
												<div class="form-group">
													<label><span style="color: red">*</span> Name</label>
													<input type="text" name="eme_name" id="eme_name" maxlength="150" class="form-control fillable eme_name" placeholder="Enter First Name">
													<span class="textfield_error"></span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label><span style="color: red">*</span> Contact Info</label>
													<input type="text" name="eme_phone" id="eme_phone" maxlength="15"  class="form-control fillable" placeholder="Enter Mobile No." >
													<span class="textfield_error" ></span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label><span style="color: red">*</span>  Relation</label>
													<input type="text" name="eme_relation"  id="eme_relation" maxlength="50" class="form-control fillable" placeholder="Enter Relation">
													<span class="textfield_error"></span>
												</div>
											</div>
											
											
										</div>
										<hr>
										<div class="row">
											<div class="col-md-12">
												<div class="gst-checkbox mb-20">
													<h4>I have a GST number (optional) &nbsp;
														<label class="checkcontainer" style="display:inline-block; margin-bottom:17px;">
															<input type="checkbox" value="1" id="chkgst" name="chkgst" class="chkgst"> <span class="checkmark"></span>
														</label></h4>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="gst-details" id="showgst" style="display:none;">
														<div class="row mb-20">
															<div class="col-md-4">
																<div class="form-group">
																	<label>Company Address</label>
																	<input type="text" class="form-control fillable_gst" name="gst_company_address" placeholder="Enter Company Address">
																	<span class="textfield_error"></span>
																</div>
															</div>
															<div class="col-md-4"> 
																<div class="form-group">
																	<label>Company's GST Email</label>
																	<input id="gst-email-filter" type="text" class="form-control fillable_gst" name="gst_companyemail" placeholder="Enter Company's GST Email">
																	<span class="textfield_error"></span>
																</div>
															</div>
															<div class="col-md-4">
																<div class="form-group">
																	<label>GST Number</label>
																	<input id="gst-number" type="text" class="form-control fillable_gst"  name="gst_number" placeholder="Enter GST Number">
																	<span class="textfield_error"></span>
																</div>
															</div>
															<div class="col-md-4">
																<div class="form-group">
																	<label>Company Name</label>
																	<input type="text" class="form-control  fillable_gst"  name="gst_company_name" placeholder="Enter Company Name">
																	<span class="textfield_error"></span>
																</div>
															</div>
															<div class="col-md-4">
																<div class="form-group">
																	<label>Company Contact Number</label>
																	<input type="text" class="form-control fillable_gst" name="gst_company_contact"  placeholder="Enter Company Contact Number">
																	<span class="textfield_error"></span>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>


										<div class="booking-traveller-details">
											<h4 class="traveler"> Other Traveller Information</h4>
											<?php
											$adultarray=Session::get('dataarray')['adult_array'];
											$childarray=Session::get('dataarray')['child_array'];
											$childagearrya=Session::get('dataarray')['childagearray'];
											
											?>
											@for($data=0;$data< count($adultarray);$data++)

											<?php
											$adultcount=$adultarray[$data];

											?>
											@if($data==0)
											<input type="hidden" name="adultcount_per_room[]" value="{{$adultcount-1}}">
											@else
											<input type="hidden" name="adultcount_per_room[]" value="{{$adultcount}}">
											@endif
											@for($adults=0;$adults<$adultcount;$adults++)
											@if($data==0 && $adults==0)
											@continue
											@endif
											<h5>Room {{$data+1}} (Adult {{$adults+1}})</h5>
											<input type="hidden" class="roomcount" id="roomcount" value="{{$adults+1}}-{{$data+1}}">
											<div class="row">
												<div class="col-md-3">
													<div class="form-group">
														<label><span style="color: red">*</span> Title</label>
														<select name="room_adult_title[]"  id="room_adult_title-{{$adults+1}}-{{$data+1}}" class="form-control room_adult_title-{{$adults+1}}-{{$data+1}}">
															<option value="Mr">Mr</option>
															<option value="Miss">Miss</option>
															<option value="Mrs">Mrs</option>
														</select>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label><span style="color: red">*</span> First Name</label>
														<input type="text" name="room_adult_first[]" maxlength="50" class="form-control fillable  room_adult_first room_adult_first-{{$adults+1}}-{{$data+1}}" id="room_adult_first-{{$adults+1}}-{{$data+1}}" placeholder="Enter First Name">
														<span class="textfield_error"></span>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label><span style="color: red">*</span> Last Name</label>
														<input type="text" name="room_adult_last[]" maxlength="50" class="form-control fillable ladult room_adult_last-{{$adults+1}}-{{$data+1}}" id="room_adult_last-{{$adults+1}}-{{$data+1}}" placeholder="Enter Last Name">
														<span class="textfield_error"></span>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label><span style="color: red">*</span> Age</label>
														<input type="text" name="room_adult_age[]" class="form-control fillable leadage" placeholder="Enter Age" maxlength="2">
														<span class="textfield_error"></span>
													</div>
												</div>

											</div>
											@endfor

											<?php
											$childcount=$childarray[$data];
											?>
											
											<input type="hidden" name="childcount_per_room[]" value="{{$childcount}}">
											@for($child=0;$child<$childcount;$child++)
											
											<h5>Room {{$data+1}} (Child {{$child+1}})</h5>
											<input type="hidden" class="childcount" id="childcount" value="{{$child+1}}-{{$data+1}}">
											<div class="row">
												<div class="col-md-3">
													<div class="form-group">
														<label>Title</label>
														<select name="room_child_title[]" id="room_child_title-{{$child+1}}-{{$data+1}}" class="form-control room_child_title-{{$child+1}}-{{$data+1}} ">
															<option value="Mr">Mr</option>
															<option value="Miss">Miss</option>
															
														</select>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label><span style="color: red">*</span>First Name</label>
														<input type="text"  name="room_child_first[]" maxlength="50" class="form-control fillable room_child_first-{{$child+1}}-{{$data+1}}" placeholder="Enter First Name" id="room_child_first-{{$child+1}}-{{$data+1}}">
														<span class="textfield_error"></span>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label><span style="color: red">*</span>Last Name</label>
														<input type="text"  name="room_child_last[]" maxlength="50" class="form-control fillable room_child_last-{{$child+1}}-{{$data+1}}" placeholder="Enter Last Name" id="room_child_last-{{$child+1}}-{{$data+1}}">
														<span class="textfield_error"></span>
													</div>
												</div>
												<div class="col-md-3" style="display: none">
													<div class="form-group">
														<label>Age</label>
														<input type="hidden"  name="room_child_age[]" class="form-control " placeholder="" value="{{$childagearrya[$data][$child]}}">
														<span class="textfield_error"></span>
													</div>
												</div>

											</div>
											@endfor
											@endfor
										</div>



										<div class="special-request">
											<h4 class="request">Special Request (optional)</h4>
											<div class="commonly">
												<h5>Commonly Requested</h5>
											</div>
											<div class="row">
												<div class="col-md-4">
													<label class="checkcontainer">Non-Smoking Room
														<input type="checkbox" class="chkgst" name="smoking_preference"> <span class="checkmark"></span>
													</label>
												</div>
									<!-- <div class="col-md-4">
										<label class="checkcontainer">Late check-in
											<input type="checkbox" class="chkgst"> <span class="checkmark"></span>
										</label>
									</div>
									<div class="col-md-4">
										<label class="checkcontainer">Early check-in
											<input type="checkbox" class="chkgst"> <span class="checkmark"></span>
										</label>
									</div>
									<div class="col-md-4">
										<label class="checkcontainer">Room on a high floor
											<input type="checkbox" class="chkgst"> <span class="checkmark"></span>
										</label>
									</div>
									<div class="col-md-4">
										<label class="checkcontainer">Large Bed
											<input type="checkbox" class="chkgst"> <span class="checkmark"></span>
										</label>
									</div>
									<div class="col-md-4">
										<label class="checkcontainer">Twin Beds
											<input type="checkbox" class="chkgst"> <span class="checkmark"></span>
										</label>
									</div>
									<div class="col-md-4">
										<label class="checkcontainer">Airport Transfer
											<input type="checkbox" class="chkgst"> <span class="checkmark"></span>
										</label>
									</div> -->
								</div>
								<hr>
								<!-- <div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>ANY OTHER REQUEST?</label>
											<textarea type="text" class="form-control" placeholder="in case of any issues, Please get in touch with the hotelier directly."></textarea>
										</div>
									</div>
								</div> -->
							</div>



							<div class="hotel-policies">
								<div class="container">
									<div class="row hotel-impo-details">
										<h4>Hotel Policies you should know</h4>
										<?php
										$cancellationpolicy=explode('|',$hotelblockroomarray['HotelPolicyDetail']);
										echo "<ul>";
										for($policy=0;$policy<count($cancellationpolicy);$policy++)
										{
											if($policy<=1)
											{
												continue;
											}
											if($cancellationpolicy[$policy]!='')
												echo "<li>".$cancellationpolicy[$policy]."</li>";

										}
										echo "</ul>";
										?>
										<!-- <p>Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm <a href="javascript:void()" class="read-more">Read More</a></p> -->
									</div>
								</div>
							</div>

							<hr>
							<div class="hotel-policies">
								<div class="container">
									<div class="row">
										<h4>Hotel Cancellation Policy</h4>

										<?php
										for($i=0;$i< count($hotelblockroomarray['HotelRoomsDetails']);$i++)
										{
											$cancellationpolicy=$hotelblockroomarray['HotelRoomsDetails'][$i]['CancellationPolicies'];
											echo "<table class='table table-bordered'>
											<thead style='background-color:#00206A;color:white;'>
											<tr>
											<th>Room</th>
											<th>From Date</th>
											<th>To Date</th>
											<th>Cancellation Charge</th>
											</tr>
											</thead>
											<tbody style='color:black'>";
											for($cancel=0;$cancel<count($cancellationpolicy);$cancel++)
											{
												$fromdatetime=explode('T',$cancellationpolicy[$cancel]['FromDate']);
												$fromdate=date('d F Y',strtotime($fromdatetime[0]));
												$fromtime=date('h:i a',strtotime($fromdatetime[1]));

												$todatetime=explode('T',$cancellationpolicy[$cancel]['ToDate']);
												$todate=date('d F Y',strtotime($todatetime[0]));
												$totime=date('h:i a',strtotime($todatetime[1]));
												echo "<tr>
												<td>Room ".($i+1),"</td>
												<td>".$fromdate." ".$fromtime."</td>
												<td>".$todate." ".$totime."</td>
												<td>".$cancellationpolicy[$cancel]['Charge']."</td>
												</tr>";
											}
											echo "</tbody></table>";
										}
										?>
										<!-- <p>Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm Lorem Ipusm <a href="javascript:void()" class="read-more">Read More</a></p> -->
									</div>
								</div>
							</div>
							<hr>
							<label class="checkcontainer"><input type="checkbox" name="terms-condition" class="chkgst" required="required">I Agree to the <a href="javascript:void()">Hotel Booking Policy</a>, <a href="javascript:void()">Hotel Cancellation Policy</a> and <a href="javascript:void()">Terms & Conditions</a> of Travoweb.com
								<span class="checkmark"></span>
							</label>
							<hr>
							<div class="text-right" id="payment_btn">
								<button type="button" id="form_submit_button" class="form_submit_button button trans_200 makepyament" >Continue<span></span><span></span><span></span></button>

								<!-- @if(session()->has("travo_useremail"))
								<button type="button" id="form_submit_button" class="form_submit_button button trans_200 makepyament" >Make Payment<span></span><span></span><span></span></button>
								@else
								<button type="button"  id="form_submit_button" class="form_submit_button button trans_200 user_box_login">Login<span></span><span></span><span></span></button>
								<button type="button"  id="form_submit_button2" class="form_submit_button button trans_200  guest_box_login" >Guest<span></span><span></span><span></span></button>
								@endif -->
							</div>
						</div>
					</form>

					<?php
					$RoomPrice=0;
					$Tax=0;
					$ExtraGuestCharge=0;
					$ChildCharge=0;
					$OtherCharges=0;
					$AgentCommission=0;
					$AgentMarkUp=0;
					$PublishedPrice=0;
					$ServiceTax=0;
					$TDS=0;
					$TotalGSTAmount=0;
					$total_fare=0;
					$total_gst=0;
					$grand_total=0;
					// for($i=0;$i<count($hotelblockroomarray['HotelRoomsDetails']);$i++)
					// {
					// 	$RoomPrice+=$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['RoomPrice'];
					// 	$Tax+=$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['Tax'];
					// 	$ExtraGuestCharge+=$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['ExtraGuestCharge'];
					// 	$ChildCharge+=$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['ChildCharge'];

					// 	$OtherCharges+=$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['OtherCharges'];
					// 	$AgentCommission+=$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['AgentCommission'];
					// 	$AgentMarkUp+=$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['AgentMarkUp'];
					// 	$PublishedPrice+=$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['PublishedPrice'];
					// 	$ServiceTax+=$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['ServiceTax'];
					// 	$TDS+=$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['TDS'];
					// 	$TotalGSTAmount+=$hotelblockroomarray['HotelRoomsDetails'][$i]['Price']['TotalGSTAmount'];

					// 	$total_fare=$RoomPrice+$Tax+$ExtraGuestCharge+$ChildCharge+$OtherCharges+$AgentCommission
					// 	+ $AgentMarkUp+$ServiceTax+$TDS+$TotalGSTAmount;
					// 	$total_gst=round(($total_fare*5)/100,2);
					
					// 							$grand_total=$total_fare+$total_gst;

					// }
					$totpap = $hotelblockroomarray['hotelmainprice']+ $hotelblockroomarray['hotelservicetax'];
					$grand_total=$hotelblockroomarray['hotelmainprice']+ $hotelblockroomarray['hotelservicetax']+$hotelblockroomarray['hotelmaingst'];
					?>
					<input type="hidden" id="totalvalue" value="<?php echo round($grand_total); ?>">


					<div class="col-lg-4">
						<div class="flight-book-price">
							<h3>Fare Breakup</h3>
							<div class="booking-price">
								<h4 class="traveller">Adult {{Session::get('dataarray')['adult_count']}} Child {{Session::get('dataarray')['child_count']}}</h4>
								<div class="inner-box">
									<div class="flight-total-price">
										<div class="inner-div">
											<div class="total-base-price left-price">Total Base Price <span class="base-price right-price"><i class="fa {{$hotelblockroomarray['hotelmaincurrecny']}}"></i> {{number_format($hotelblockroomarray['hotelmainprice'],2)}}</span></div>
										</div>
										<div class="inner-div">
											<div class="total-taxes left-price">Total Taxes & Fees <span class="tax right-price"> <i class="fa {{$hotelblockroomarray['hotelmaincurrecny']}}"></i> {{number_format($hotelblockroomarray['hotelservicetax'],2)}}</span></div>
										</div>
										<div class="inner-div air-fare-border">
											<div class="total-airfare left-price">Total Fare <span class="airfare right-price"><i class="fa {{$hotelblockroomarray['hotelmaincurrecny']}}"></i> {{number_format($totpap,2)}}</span></div>
										</div>
										<div class="inner-div">
											<div class="total-convenience-fee left-price">Total GST <span class="convenience right-price"><i class="fa {{$hotelblockroomarray['hotelmaincurrecny']}}"></i>{{number_format($hotelblockroomarray['hotelmaingst'],2)}}</span></div>
										</div>
										<div class="inner-div">
											<div class="grand-total left-price">Grand Total <span class="grand right-price"> <i class="fa {{$hotelblockroomarray['hotelmaincurrecny']}}" style="font-size:23px;"></i> {{number_format($grand_total),2}}</span></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
						<div class="col-lg-8">
						<div class="payment-card" id="paymentcard" style="display: none">

							<div class="dropin-page">

								<form action="{{route('stripepayment')}}" method="POST" id="payment-form">



									<h3 class="bt_title">Payment Method </h3>

									<span class="payment-errors" style="color:red"></span>

									<div class="form-row row">

										<div class="col-12">
											<div class="icon-container">
												<i class="fa fa-cc-visa" style="color:navy;"></i>
												<i class="fa fa-cc-amex" style="color:blue;"></i>
												<i class="fa fa-cc-mastercard" style="color:red;"></i>
												<i class="fa fa-cc-discover" style="color:orange;"></i>
											</div>
										</div>
										<div class="col-12">

											<label class="pay-label"><span><i class="fa fa-credit-card-alt" style="color: #000000;"></i> Card Holder Name</span></label>

											<span class="text-danger err-msg password textfield_error textfield"  id="name_error" style="display: none;font-size: 11px"> </span>

											<input type="text" size="20" id="name" name="name" class="form-control pay-input textfield" placeholder="Card Holder Name">





										</div>

									</div>

									<div class="form-row row">

										<div class="col-12">

											<label class="pay-label"><span><i class="fa fa-credit-card-alt" style="color: #000000;"></i> Card Number</span></label>

											<span class="text-danger err-msg password textfield_error textfield"  id="card_error" style="display: none;font-size: 11px"> </span>

											<input type="text" size="20" id="card" data-stripe="number" class="form-control pay-input textfield leadage" placeholder="Enter Your Card No">



										</div>

									</div>

									<div class="form-row row">

										<div class="col-sm-6" >

											<div class="row">

												<div class="col-sm-6">

													<label class="pay-label"><span><i class="fa fa-ban" style="color: #000000;"></i> Expiration</span></label>

													<span class="text-danger err-msg password textfield_error textfield"  id="expmonth_error" style="display: none;font-size: 11px"> </span>

													<input type="text" size="2" id="expmonth" data-stripe="exp_month" class="form-control pay-input textfield leadage" placeholder="Month" maxlength="2">

												</div>

												<div class="col-sm-6">

													<label class="pay-label"><span>&nbsp;</span> </label>

													<span class="text-danger err-msg password textfield_error textfield"  id="expyear_error" style="display: none;font-size: 11px"> </span>

													<input type="text" size="2" id="expyear" data-stripe="exp_year" class="form-control pay-input textfield leadage" placeholder="Year" maxlength="2">

												</div>

											</div>

										</div>

										<div class="col-sm-6">

											<label class="pay-label"><span><i class="fa fa-credit-card" style="color: #000000;"></i> CVC</span></label>

											<span class="text-danger err-msg password textfield_error textfield"  id="cvc_error" style="display: none;font-size: 11px"> </span>

											<input type="text" size="4" id="cvc" data-stripe="cvc" class="form-control pay-input textfield leadage" placeholder="Enter Your CVC" maxlength="4">



										</div>



									</div>





									<input type="submit" class="submit pay-btn" value="Make Payment">

								</form>

							</div>

						</div>
						</div>
				</div>
			</div>
		</section>
		@elseif($hotelblockroomarray['Error']['ErrorCode']==6)
		<br>  
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="intro_content nano-content">
						<div class="no-flight">
							<h3>Your session has been expired</h3>
							<a href="{{url('/index')}}" class="btn btn-back">Go Back</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		@else
		<br>  
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="intro_content nano-content">
						<div class="no-flight">
							<h3>{{$hotelblockroomarray['Error']['ErrorMessage']}}</h3>
							<a href="{{url('/index')}}" class="btn btn-back">Go Back</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		@endif
		
	</div>
</div>
<div class="modal fade myModal" id="myModaln1" role="dialog" data-backdrop="static" data-keyboard="false" style="background-color: #f0f8ffd1;">
	<div class="modal-dialog">
	<center><img src="{{ asset('assets/images/newf.gif')   }}" style="height: auto;width: auto; display: block;margin:60% auto;position: relative;top:50%;transform: translateY(-50%)"></center>
	</div>
</div>
		<div class='session-counter'>
			<p class='timer' data-minutes-left=15>Your booking session will expire in </p>
			<section class='actions'></section>
		</div>
		<style>
			.session-counter {
				background: #000000d9;
				color: white;
				padding: 5px;
				position: fixed;
				bottom: 0;
				display: block;
				width: 100%;
				z-index: 1;
			}

			.session-counter p{
				margin: 0;
				color: white;
				font-size: 17px;
				font-weight: 500;
				text-align: center;
				z-index: 1;
			}
			.jst-hours,.jst-minutes,.jst-seconds {
				display: inline;
				color: orange;
			}

		</style>
<!-- Footer -->@include('pages.include.footer')
<!-- Copyright -->@include('pages.include.copyright')
<script>
	$(document).on('click', 'input[name="chkgst"]', function() {
		if ($(this).is(":checked")) {
			$('#showgst').show();
		} else if ($(this).is(":not(:checked)")) {
			$('#showgst').hide();
		}
	});
</script>

<script>
	$(document).on('click','.makepyament',function(e)
	{
		
		var error=0;
		var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var gstinformat = new RegExp('^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$');
		var currency=$('#currency').val();
		var passvalid=$('#passvalid').val();
		var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
		var regsaid = /[a-zA-Z]{2}[0-9]{7}/;
		// e.preventDefault();
		$('.roomcount').each(function(){
			
			var roomcount = $(this).val();
			
			var adultlname= $('#room_adult_last-'+roomcount).val();
			var room_adult_title=$('#room_adult_title-'+roomcount).val();
			
			if(adultlname=='')
			{
				var adultfname = $("#room_adult_first-"+roomcount).val();
				
				$('#room_adult_last-'+roomcount).val(adultfname);
				$("#room_adult_first-"+roomcount).val(room_adult_title);

			}
			
		});
		$('.childcount').each(function(){
			var childcount = $(this).val();
			var childlname = $("#room_child_last-"+childcount).val();
			var childtitle = $('#room_child_title-'+childcount).val();
			
			if(childlname=="")
			{
				childfirstname = $("#room_child_first-"+childcount).val();
				$("#room_child_last-"+childcount).val(childfirstname);
				$("#room_child_first-"+childcount).val(childtitle);
			}
			

		})
		

		
		
		$(".fillable").each(function()
		{
			var childcount=$('#childcount').val();
			
			
			if($(".leadlastname").val()=='')
			{
				if($(".leadfirstname").val() !='')
				{
					var leadfirstname = $(".leadfirstname").val();
					$(".leadlastname").val(leadfirstname);
					var leadtitle = $(".leadtitle").val();
					$(".leadfirstname").val(leadtitle);

				}
			}

			
			
			
			
			if($(this).val().trim()=='')
			{
				error++;
				$(this).focus();
				$(this).siblings('.textfield_error').text("Cannot be empty");
				$(this).siblings('.textfield_error').css({
					"color":"red",
				});

			}
			if($(this).val().trim()!='' && $(this).val().length<2)
			{
				error++;
				$(this).focus();
				$(this).siblings('.textfield_error').text("Characters should be more than 2");
				$(this).siblings('.textfield_error').css({
					"color":"red",
				});
			}

			if($(this).val().trim()!='' && $(this).val().length>=2)
			{
				$(this).siblings('.textfield_error').text("");
				$(this).siblings('.textfield_error').css({
					"color":"red",
				});
			}
			if($('#phone').val().length < 10 || $('#phone').val().length >15 )
			 {
			 	error++;
				$('.phone').siblings('.textfield_error').text("Mobile number should be between of 10 to 15-digits");
				$('.phone').siblings('.textfield_error').css({
					"color":"red",
					"font-size": "11px",
				});
			 }
			 if($('#eme_phone').val().length < 10 || $('#eme_phone').val().length >15 )
			 {
			 	error++;
				$('.eme_phone').siblings('.textfield_error').text("Mobile number should be between of 10 to 15-digits");
				$('.eme_phone').siblings('.textfield_error').css({
					"color":"red",
					"font-size": "11px",
				});
			 }



		});

		if($('input[name="chkgst"]').is(":checked"))
		{

			$(".fillable_gst").each(function()
			{

				

				if($(this).val().trim()=='')
				{
					error++;
					$(this).focus();
					$(this).siblings('.textfield_error').text("Cannot be empty");
					$(this).siblings('.textfield_error').css({
						"color":"red",
					});


				}

				if($(this).val().trim()!='' && $(this).val().length<2)
				{
					error++;
					$(this).focus();
					$(this).siblings('.textfield_error').text("Characters should be more than 2");
					$(this).siblings('.textfield_error').css({
						"color":"red",
					});
				}


				if($(this).val().trim()!='')
				{

					$(this).siblings('.textfield_error').text("");
					$(this).siblings('.textfield_error').css({
						"color":"red",
					});
				}
				if($(this).val().trim()!='' && $(this).attr('id')=='gst-email-filter')
				{
					
					if(emailfilter.test($(this).val().trim()))
					{

						$(this).siblings('.textfield_error').text("");
						$(this).siblings('.textfield_error').css({
							"color":"red",
						});

					}
					else
					{
						error++;
						$(this).focus();
						$(this).siblings('.textfield_error').text("Please Enter Valid Email");
						$(this).siblings('.textfield_error').css({
							"color":"red",
						});
					}
				}

				if($(this).val().trim()!='' && $(this).attr('id')=='gst-number')
				{
					
					if(gstinformat.test($(this).val().trim()))
					{

						$(this).siblings('.textfield_error').text("");
						$(this).siblings('.textfield_error').css({
							"color":"red",
						});

					}
					else
					{
						error++;
						$(this).focus();
						$(this).siblings('.textfield_error').text("Please Enter Valid GST Number");
						$(this).siblings('.textfield_error').css({
							"color":"red",
						});
					}
				}


			})
		}
		else
		{
			$(".fillable_gst").each(function()
			{
				$(this).val('');
				$(this).siblings('.textfield_error').text("");
			})
		}

		if(emailfilter.test($("#email-filter").val().trim()))
		{
			$("#email-filter").siblings('.textfield_error').text("");
			$("#email-filter").siblings('.textfield_error').css({
				"color":"red",
			});

		}
		else
		{
			error++;
			$("#email-filter").focus();
			$("#email-filter").siblings('.textfield_error').text("Please Enter Valid Email");
			$("#email-filter").siblings('.textfield_error').css({
				"color":"red",
			});
		}
		
		if(passvalid.trim()=='1')
		 {
		 
		 	if($('#pancardno').val().length != 10)
		 	{
		 		
		 		error++;
				$('#pancardno').siblings('.textfield_error').text("Pan card  number should be 10 digit ");
				$('#pancardno').siblings('.textfield_error').css({
					"color":"red",
					"font-size": "11px",
				});
		 	}

		 	if(regpan.test($("#pancardno").val().trim()))
			{
				
				$("#pancardno").siblings('.textfield_error').text("");
				$("#pancardno").siblings('.textfield_error').css({
					"color":"red",
				});

			}
			else
			{
				error++;
				
				$("#pancardno").focus();
				$("#pancardno").siblings('.textfield_error').text("Please Enter Valid Pan Card No");
				$("#pancardno").siblings('.textfield_error').css({
					"color":"red",
				});
			}
		 	

		 }
		 else
		 {
		 	
		 		//PASSPORT CHCEK VALIDATION

		 }

		if(error==0)
		{
		
			$('#myModaln1').modal('show');
			var amount = $('#grandtotal').val();
			var currency=$('#currency').val();
			var customerfname=$('#firstname').val();
			var customermname=$('#middlename').val();
			var customerlname=$('#lastname').val();
			var customername =customerfname + '' + customermname + ' '+customerlname ;
			var customerEmail = $('.email').val();
			var customerPhone = $('#phone').val();
			var csrf_token = $('#csrf_token').val();
			var hotelconvcurr=$('#hotelconvcurr').val();
			var currencyicon=$('#currencyicon').val();
			var hoteldata = $("#hotelbooking_submit").serialize();
			
			$.ajax({
					url : "{{route('request_hotel')}}",
					data : {
							'amount' : amount,
							'currency' : currency,
							'customername' : customername,
							'customerEmail' : customerEmail,
							'customerPhone' : customerPhone,
							'csrf_token' : csrf_token,
							'hoteldata' : hoteldata,
							
							'customerfname' : customerfname,
							'customermname' : customermname,
							'customerlname' :customerlname,
							'hotelconvcurr' : hotelconvcurr,
							'currencyicon' : currencyicon,

							},
					type:'get',
					// dataType:'Json',
					success : function(data)
					{
						if(data=='INR')
						{
							window.location="{{url('request')}}";
						}
						else
						{
							$('#paymentcard').fadeIn(1500);
							$('.makepyament').hide();
							// window.location="{{url('stripepayment')}}";
						}
						$('#myModaln1').modal('hide');
											
						
						
						
					},
			});
			
		}
		
		
	});
</script>
<script>
	$(document).ready(function(){
		var gtvalue = $('#totalvalue').val();
		$('#grandtotal').val(gtvalue);
	})
</script>
<script>
	$(document).ready(function () {
  
  $(".leadage").keypress(function (e) {
  	
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       return false;
    }
   });
});

</script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<!-- TO DO : Place below JS code in js file and include that JS file -->
<script>
    $(document).on('blur', '.textfield', function()
    {
        var id=this.id;
        var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if($('#'+id).val().trim()!="")
        {
            $('#'+id+'_error').text('');
            $('#'+id+'_error').hide();
        }
        if(id=="name" && $('#'+id).val().trim()=="")
        {
            
            $('#'+id+'_error').text('Please Enter Card Holder Name');
            $('#'+id+'_error').show();
        }
        if(id=="card" && $('#'+id).val().trim()=="")
        {
            $('#'+id+'_error').text('Please Enter Card Number');
            $('#'+id+'_error').show();
        }
        if(id=="expmonth" && $('#'+id).val().trim()=="")
        {
            $('#'+id+'_error').text('Enter Month');
            $('#'+id+'_error').show();
        }
        if(id=="expyear" && $('#'+id).val().trim()=="")
        {
            $('#'+id+'_error').text('Enter Year');
            $('#'+id+'_error').show();
        }
        if(id=="cvc" && $('#'+id).val().trim()=="")
        {
            $('#'+id+'_error').text('Enter CVC');
            $('#'+id+'_error').show();
        }
    });
</script>

<script>

    Stripe.setPublishableKey('<?php echo $params['public_live_key']; ?>');
    $(function () {
        var $form = $('#payment-form');
        $form.submit(function (event) {
        $('.textfield_error').text('');
        $('.textfield_error').hide();
        var name=$('#name').val();
        var card=$('#card').val();
        var expmonth=$('#expmonth').val();
        var expyear=$('#expyear').val();
        var cvv=$('#cvv').val();
        if(name=="")
        {
            $('#name_error').text("Please Enter Name");
            $('#name_error').show();
        }
        else if(card=="")
        {
            $('#card_error').text("Please Enter Card Number");
            $('#card_error').show();
        }
        else if(expmonth=="")
        {
            $('#expmonth_error').text("Enter exp month ");
            $('#expmonth_error').show();
        }
        else if(expyear=="")
        {
            $('#expyear_error').text("Enter exp year ");
            $('#expyear_error').show();
        }
        else if(cvv=="")
        {
            $('#cvv_error').text("Enter CVV Number ");
            $('#cvv_error').show();
        }
        else
        {
            $form.find('.submit').prop('disabled', true);
            // Request a token from Stripe:
            Stripe.card.createToken($form, stripeResponseHandler);
            // Prevent the form from being submitted:
            
        }
        return false;
    
        });
    });
    function stripeResponseHandler(status, response) {
        // Grab the form:

    var $form = $('#payment-form');
    
    if (response.error) {
        // Problem!
    // Show the errors on the form:
    $form.find('.payment-errors').text(response.error.message);
    $form.find('.submit').prop('disabled', false);
    // Re-enable submission
    } else {
    // Token was created!
    // Get the token ID:
    var token = response.id;
    // Insert the token ID into the form so it gets submitted to the server:
    $form.append($('<input type="hidden" name="stripeToken">').val(token));
    // Submit the form:
    $form.get(0).submit();
    }
    };

</script>
</body>

</html>