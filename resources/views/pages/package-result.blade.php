@include('pages.include.header')

<style>
    @import url("https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/blitzer/jquery-ui.min.css");

    .ui-datepicker td span,
    .ui-datepicker td a {
        padding-bottom: 1em;
    }
    .res-block{
        display: none !important;
    }
    span.m-close {
        position: absolute;
        font-size: 27px;
        font-weight: 700;
        top: 0;
        color: white;
        right: 19px;
        cursor: pointer;
    }
    .ui-datepicker td[title]::after {
        content: attr(title);
        display: block;
        position: relative;
        font-size: .8em;
        height: 1.25em;
        margin-top: -1.25em;
        text-align: right;
        padding-right: .25em;
    }
    #ui-datepicker-div
    {
        z-index:12 !important;
    }
    .table-condensed>thead>tr>th,.table-condensed>thead>tr>td
    {
        padding:5px;
    }
    .dropdown-menu {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 160px;
        margin: 2px 0 0;
        font-size: 14px;
        text-align: left;
        list-style: none;
        background-color: #fff;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        border: 1px solid #ccc;
        border: 1px solid rgba(0,0,0,.15);
        border-radius: 4px;
        -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        box-shadow: 0 6px 12px rgba(0,0,0,.175);
    }
    .dropdown-menu {
        box-shadow: none;
        border-color: #eee;
    }
    .search_panel
    {
        padding-top:50px;
        padding-bottom:50px;
    }
    .search_panel_content {
        flex-wrap: wrap;
    }
    .extras {
        width: 100%;
        margin-bottom:10px;
    }
    .search_extras_item {
        width: 50%;
        float: left;
        margin-bottom: 10px;
    }
    .search_extras_item div {
        display: inline-block;
        cursor: pointer;
    }
    .search_extras_cb {
        display: block;
        position: relative;
        width: 15px;
        height: 15px;
        -webkit-appearance: none;
        -moz-appearance: none;
        -ms-appearance: none;
        -o-appearance: none;
        appearance: none;
        background-color: #FFFFFF;
        border: 1px solid #FFFFFF;
        padding: 9px;
        margin-top: 4px;
        border-radius: 50%;
        display: inline-block;
        position: relative;
        cursor: pointer;
        float: left;
    }
    .search_extras_cb:checked::after {
        display: block;
        position: absolute;
        top: 2px;
        left: 2px;
        border-radius:50%;
        width: calc(100% - 4px);
        height: calc(100% - 4px);
        content: '';
        background: #fa9e1b;
    }
    .flight-details-tabs .nav>li>a {
        position: relative;
        display: block;
        padding: 10px 15px;
        border: 1px solid #FF9800 !important;
        border-top-left-radius: 6px !important;
        border-top-right-radius: 25px !important;
    }
    .flight-details-tabs .nav-tabs>li>a.active, .nav-tabs>li>a.active:focus, .nav-tabs>li>a.active:hover {
        color: #FFF;
        cursor: default;
        background-color: #fa9e1b;
        border: 1px solid #FF9800 !important;
        border-radius: 0!important;
        padding: 10px 15px !important;
        border-top-left-radius: 6px !important;
        border-top-right-radius: 25px !important;
    }
    .search_extras label {
        display: block;
        position: relative;
        font-size: 15px;
        font-weight: 400;
        padding-left: 25px;
        margin-bottom: 0px;
        cursor: pointer;
        color: #FFFFFF;
    }
    @media only screen and (max-width: 1730px)
    {
        .search_extras_item {
            width: 20%;
        }
    }
    @media only screen and (max-width: 1730px)
    {
        .search_panel {
            display: none !important;
            width: 100%;
            height: 100%;
            -webkit-animation: fadeEffect 1s;
            animation: fadeEffect 1s;
            margin-top: 0px;
        }
    }

    @media only screen and (max-width: 350px)
    {
        .d-div{
            display: none;
        }
    }
</style>
<style>


    .cheap-flight .flight-list {
        margin-bottom: 0px;
        border-left: 0;
        padding-bottom: 52px !important;
        border-right: 0;
        background: #FEE1BE;
        border-bottom: 2px solid #fa9e1b;
    }
    @media screen and (max-width: 450px) {
        .cheap-flight .flight-list {

            padding-bottom: 83px !important;

        }

    }
    @media screen and (max-width: 500px) {
        .cheap-btn {
            padding: 11px 10px !important;
            font-size: 11px !important;
        }
        .res-block {
            display: block!important;
            position: absolute !important;
            right: 10px !important;
            bottom: 0px !important;
        }
        .bar-heading h5 {
            font-size: 12px !important;
        }
    }
    a.active {
        border-radius: 20px !important;
        padding: 9px 14px !important;
    }
    a.a-done-btn {
        text-decoration: none;
        padding: 10px;
        background: #fa9e1b;
        color: white;
        margin: 20px auto auto auto;
        width: 120px;
        display: block;
        text-align: center;
        border-radius: 20px;
    }
    .a-done-btn {
        text-decoration: none;
        padding: 10px;
        background: #fa9e1b;
        color: white;
        margin: 20px auto auto auto;
        width: 120px;
        display: block;
        text-align: center;
        border-radius: 20px;
        cursor: pointer;
    }
    .dropdown a:hover {background-color: #ddd;}

    .show {display: block;}
    @media screen and (max-width: 500px) {

        .flight-list .flight-book .flight-book-btn {
            padding: 4px 6px ;
            font-size: 11px ;
        }
        .flight-name p {
            font-size: 10px !important;
        }
        .flight-list .flight-book .details {

            font-size: 11px;
        }
        .flight-list .flight-price .flight-price-heading {
            font-size: 11px;
        }
        .flight-list p {

            font-size: 11px !important;
        }
        .flight-list .flight-fare .fare-type {
            font-size: 11px;
            font-weight: 600;
            text-align: right;
        }
        .flight-list .flight-fare .fare-rules {
            color: #00206A;
            font-size: 11px;
            font-weight: 600;
            text-align: left !important;
        }
        .flight-list .flight-fare .fare-type {
            font-size: 11px;
            font-weight: 600;
            text-align: right;
        }
        h5.fare-rules {
            text-align: left !important;
        }
    }
    h5.fare-type.refundable {
        text-align: right !important;
        padding-right: 10px;
    }
    h5.fare-rules {
        text-align: left !important;
        padding-left: 10px;
    }
    .airlines-list .title-bar {
        background: #fa9e1b;
        padding: 8px 15px;
        border-radius: 0 !important;
        border-top-left-radius: 4px !important;
        border-top-right-radius: 4px !important;
       margin-bottom: 0px !important;
    }

</style>
<style>
    button.view-h-btn {
        display: block;
        width: auto;
        margin: 50px auto 0;
        padding: 15px 31px;
        background: #00206a;
        border: none;
        color: white;
    }
    .slider-div {
        margin-left: 30px;
        margin-right: 30px;
    }
    span.hs-price {
        position: absolute;
        top: 15px;
        left: 33px;
        color: white;
        font-size: 16px;
        font-weight: 700;
    }
    a.ind-btn {
        position: absolute;
        top: 50%;

        transform: translateY(-50%);
        color: black;
        background: whitesmoke;
        padding: 17px 16px;
        font-size: 20px;
        font-weight: 900 !important;
    }
    a.ind-btn:hover {
        background: #FF9800;
        color: white;
    }

    .right-btn {
        right: -99px;
        position: absolute;
    }
    .flight-details {
        padding: 30px 0px 0 !important;
    }
    h3.h-head {
        color: #00206a;
        font-weight: 600;
        text-align: center;
        font-size: 35px;
        margin: -20px 0 66px;
    }
    .hotel-div {
        height: 210px;
        min-height: 210px;
        border-radius: 5px;
        border: 1px solid #FF9800;
    }
    button.b-btn {
        border: none;
        background: #FF9800;
        color: white;
        padding: 7px 10px;
        border-radius: 5px;
        display: block;
        margin-left: auto;
    }
    .r-star {
        position: relative;
        top: 30px;
        left: 11px;
    }
    h2.h2-head {
        font-size: 19px;
        margin-bottom: 0;
        color: white;
        position: relative;
        top: 29px;
        left: 11px;
    }
    span.fa.fa-star.checked {
        color: #FFC107;
    }
    span.fa.fa-star {
        color: #ffffff;
    }
    button.b-btn {
        border: none;
        background: #FF9800;
        color: white;
        padding: 7px 10px;
        border-radius: 5px;
        display: block;
        margin-left: auto;
    }
    button.b-btn:hover {

        background: #00206A;

    }
    .book-btn-div {
        position: relative;
        top: 115px;
        right: 10px;
        border: none;
    }
    img.h-img {
        width: 100%;
        position: relative;
        height: 100%;
        border-radius: 5px;
    }
    .over-content {
        position: absolute;
        top: 0px;
        width: 100%;
        left: 0px;
        background: #00000085;
        height: 100%;
        border-radius: 5px;
    }
    span.hotel-name-tag {
        color: white;
        background: #E91E63;
        padding: 6px 10px;
        /* top: 10px; */
        position: relative;
        top: 17px;
        left: 10px;
        border-radius: 5px;
    }
    @media screen and (max-width:1280px){
        .col-sm-6.col-md-3.slides-res {
            flex: 0 0 45%;
            max-width: 40%;
            margin: 0 auto 45px auto;
        }
    }
    @media screen and (max-width:760px) {
        .col-sm-6.col-md-3.slides-res {
            flex: 0 0 50%;
            max-width: 50%;
            margin: 0 auto 45px auto;
        }
        button.view-h-btn {

            margin: 0 auto 0;

        }

    }
    @media screen and (max-width:650px) {
        .col-sm-6.col-md-3.slides-res {
            flex: 0 0 100%;
            max-width: 100%;
            margin: 0 auto 45px auto;
        }

    }
</style>
   <style>
       .carousel-inner img {
           width: 100%;
           height: 100%;
       }
       img.img-responsive.pack-img {
           max-width: 100%;
           width: 180px;
           padding: 4px;
           border: 1px solid;
           margin: 0px;
       }
       .checked {
           color: orange;
       }
       .pkg-detail {
           padding: 10px;
           text-align: left;
       }
       p.pkg-para {
           color: #FF5722;
           font-size: 18px;
           font-weight: 600;
           margin-bottom: 10px;
       }
       span.pkg-span {
           color: white;
           background: #3b499a;
           padding: 2px 5px;
           border-radius: 3px;
       }
       span.theme-badge {
           color: white;
           padding: 2px 10px;
           background: #009688;
          
           font-size: 13px;
           border-radius: 22px;
           display: inline-block;
           margin-bottom: 10px !important;
       }
       .extra-pkg i {
           color: #607D8B;
           padding: 0 7px 0 0;
       }
       p.theme-para {
           margin-top: 10px;
       }
       ul.highlight li {
           color: black;
           font-size: 13px;
           padding-bottom: 10px;
       }
       ul.highlight li i {
           color: #3F51B5;
       }
       .extra-pkg {
           margin-top: 4px;
       }
       button.pkg-btn {
           border: none;
           background: #f99d1b;
           color: #FFF;
           padding: 9px 20px;
           border-radius: 5px;
           font-weight: 600;
       }
       a.pkg-btn {
           width: 100px;
           border: none;
           background: #f99d1b;
           color: #FFF;
           padding: 9px;
           border-radius: 5px;
           font-weight: 600;
           font-size: 12px;
           margin-left: auto;
           display: block;
       }
       span.datecolor {
           color: red;
       }
       .pkp-price-para s {
           color: white;
           background: #4c5458;
           padding: 5px;
       }
       dfn {
           color: #00206a;
           font-size: 18px;
           font-weight: 600;
       }
       .carousel-item img {
           height: 250px;
       }
       .price-div{
           text-align: right !important;
       }
       .pkg-sm-active{
           display: none;
       }
       @media screen and (max-width:992px){
           .pkg-res-md{
               display:none !important;
           }
       }
       @media screen and (max-width:500px){
           .pkg-price{
               margin-top: 0px
           }
           .pkg-sm-active{
               display: block;
           }
           button.pkg-btn {
               border: none;
               background: #f99d1b;
               color: #FFF;
               padding: 9px;
               border-radius: 5px;
               font-weight: 600;
               font-size: 12px;
               margin-left: auto;


               display: block;
           }
            a.pkg-btn {
               border: none;
               background: #f99d1b;
               color: #FFF;
               padding: 9px;
               border-radius: 5px;
               font-weight: 600;
               font-size: 12px;
               margin-left: auto;
               display: block;
           }
           .pkg-sm-res{
               display: none;
           }
           .detail-div{
               flex: 0 0 58.33% !important;
               max-width: 58.33% !important;
           }

           .pkg-detail {
               padding: 0px;
               text-align: left;
           }
           span.pkg-span {
               color: white;
               background: #3b499a;
               padding: 2px 5px;
               border-radius: 3px;
               font-size: 12px;
           }
           p.pkg-para {
               color: #FF5722;
               font-size: 15px !important;
               font-weight: 600;
               margin-bottom: 5px;
           }
           dfn {
               color: #00206a;
               font-size: 14px;
               font-weight: 600;
           }



       }
       .pkg-price{
           margin-top: 12px
       }

   </style>
<body>
<style>
    .box-hover
    {
        background-color: black;
        position: absolute;
        width: 290px;
        height: 185px;
        margin: 5px 5px 0 5px;
        display: none;
        opacity: 0.7;
    }
    button#read-more {
    border: none;
    background: none;
    color: #f99e1b;
    font-size: 15px;
}
button#read-more span {
    color: #a5a5a5;
}
    @media screen and (max-width: 350px) {


        .flight-list .flight-book  {
            padding: 3px !important;
            font-size: 11px !important;

        }
    }
    @media screen and (max-width: 650px){
        .showtable_returninter div .flight-list .row .col-md-3 {
            padding: 0;
        }
        .showtable_returninter div .flight-list .flight-fare .row .col-md-3 {
            padding: 10px;
        }

    }
    .showtable_returninter p.flight-price-heading {
        font-size: 15px !important;
        font-weight: 700;
        color: #fa9e1b;
    }
    .collapse.show {
    display: inline;
}
</style>
<div class="super_container">
    <!-- Modal -->
    <div class="modal fade modify-flight" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Modify Search</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@include('pages.include.topheader')
    <div class="home" style="height:20vh;">
        <!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->
        <div class="home_content">
            <div class="home_title">Results  </div>
        </div>
    </div>
    <!-- Intro -->
    <div class="result">
        <div class="intro">
                <section class="search-breadcrumb">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="intro_content">
                                <div class="row">
                                    <div class="col-lg-3 res-mb-20">
                                        <input type="hidden" value="1" id="journytype">
                                        <input type="hidden" value="Economy" id="FlightCabinName">

                                        <!-- 
                                        <h4>Holiday Packages  <i class="fa fa-plane plane fa-rotate-45"></i></h4>
                                        <h5><small>Amritsar  (Amritsar)</small></h5> -->
                                    </div>
                                    <div class="col-lg-3 res-mb-20">
                                        <!-- <h4>Holiday Packages</h4>
                                        <h5><small>Delhi</small></h5> -->
                                    </div>
                                    <div class="col-lg-4 res-mb-20">
                                        <!-- <h4 class="flight-class"><i class="fa fa-calendar"></i>&nbsp;  27 Aug 2019 Tuesday</h4> -->
                                        
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </section>
                
            <section class="flight-details">
            <!-- CHEPEAT FLIGHT SHOW DIV -->
           <!--  <div class="col-lg-12 cheap-flight">
                <div class="airlines-list">
                    <div class="showtable1">
                        <input type="hidden" value="fa-inr" id="recur">
                    </div>
                </div>
            </div> -->
            <!-- CHEPEAT FLIGHT END DIV -->
            <?php
            if($data=='1')
            {
            ?>
            <div class="container">
                <div class="row">
                    <div class="col-lg-9" id="res-two">
                        <div class="airlines-list">
                            <div class="title-bar">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="bar-heading">
                                        	<?php
                                        	if($checknewdata[0]['countryvalue']=='1')
                                        	{
                                        		$type='Domestic';
                                        	}
                                        	else
                                        	{
                                        		$type="International";
                                        	}
                                        	// foreach ($checknewdata as $pckgval) {
                                        	// 	echo $pckgval['countryvalue'];
                                        	?>

                                            <h5>{{$type}} Holiday Packages</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="showtable">
                                <div class="newtable packagedata" id="listflight">
                                	@foreach($checknewdata as $chkval)
                                 
                                    <div class="flight-list listflight">
                                        <div class="row">
                                            <div class="col-sm-5 col-md-4 col-lg-3 price-div">
                                                <div class="flight-name">
                                                	<?php $pckgimageval= unserialize($chkval['packimage']) ;

                                                	?>
                                                    <img src="{{asset('assets/uploads/packageimage')}}/{{$pckgimageval[0]}}" class="img-responsive pack-img">
                                                </div>
                                            </div>
                                            <div class="col-sm-7 col-md-4 col-lg-3 price-div detail-div" style="padding: 0">
                                               <div class="pkg-detail">
                                                   <p class="pkg-para">{{ucfirst($chkval['packagename'])}}</p>
                                                   <span class="pkg-span">{{$chkval['packagedays']}} Days / {{$chkval['packagenight']}} Nights</span>
                                                   <div class="rating" style="margin-top: 6px;">
                              													<?php
                              													for($i=1;$i<=$chkval['packgstar'];$i++)
                              													{
                              														echo '<span class="fa fa-star checked"></span>';
                              													}
                              													?>
                                                       <!-- <span class="fa fa-star checked"></span>
                                                       <span class="fa fa-star checked"></span>
                                                       <span class="fa fa-star checked"></span>
                                                       <span class="fa fa-star" style="color: #232842;"></span>
                                                       <span class="fa fa-star" style="color: #232842;"></span> -->
                                                   </div>
                                                

                                                   <div class="extra-pkg">
                                                       <i class="fa fa-glass iconselect"></i>
                                                       <i class="fa fa-bus iconselect"></i>
                                                       <i class="fa fa-cutlery iconselect"></i>
                                                       <i class="fa fa-eye iconselect"></i>
                                                   </div>

                                               </div>
                                            </div>

                                            <div class="col-md-3 pkg-res-md" style="padding: 0;text-align: left">
                                                <div class="pkg-highlight">
                                                    <p class="pkg-para" style="padding: 10px 0 0">Description </p>
                                                    
                                                      <?php  
                                                         echo $str = trim(substr(strip_tags($chkval['maindescription']), 0, 150)," ");
                                                        ?>
                                                       
                                                       
                                                       <div id="demo" class="collapse">
                                                     <?php
                                                        echo $str = trim(substr(strip_tags($chkval['maindescription']), 150, -1)," ");
                                                         
                                                     ?>
                                                       </div>
                                                       <button data-toggle="collapse" id="read-more" data-target="#demo">Read More</button>


                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-3 price-div pkg-sm-res">
                                                <div class="pkg-price">
                                                    <p class="pkp-price-para pkg-sm-res" style="font-size: 22px;"><strike><i class="fa {{$chkval['pricecurr']}}"></i>  {{$chkval['oldpricenew']}}</strike></p>
                                                    <p  class="pkg-sm-res"><dfn><i class="fa {{$chkval['pricecurr']}}" style="color: #f99d1b;"></i> {{$chkval['newpricenew']}}</dfn><br>Per Person</p>
                                                    <p  class="resp_dnone pkg-sm-res"><b>Valid Till:</b>

                                                        <span class="datecolor "><?php 
															                           echo $chkval['packgvalid']; ?></span>

                                                    </p>
                                                 
                                                     <a class="pkg-btn" href="{{url('packagedetails/'.$chkval['id'].'%'.$chkval['pckg_id'])}}">View Details</a>

                                                </div>

                                            </div>
                                            <a class="pkg-btn res-block" href="{{url('packagedetails/'.$chkval['id'].'%'.$chkval['pckg_id'])}}">View Details</a>
                                        </div>
                                       <!--  <div class="flight-fare " style="margin-top: 10px;">
                                             <div class="row">
                                                 <div class="col-6 pkg-sm-active"><p  class=""><dfn><i class="fa fa-inr" style="color: #f99d1b;"></i> 15120</dfn><br>Per Person</p></div>
                                                 <div class="col-6 pkg-sm-active">
                                                     <button class="pkg-btn" type="button">View Details</button>
                                                 </div>
                                             </div>
                                        </div> -->
									</div>
									@endforeach
                                   


                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-3" id="res-one">
                        <div class="flight-filter">
                            <h3>Sort &amp; Filter</h3>
                            <div class="filter">
                                <h4><i class="fa {{$checknewdata[0]['pricecurr']}}"></i>&nbsp; Price</h4>
                                <div class="inner-filter">  
                                  <?php
                                 
                                  $price=array();
                                    $currency="";
                                     for($ht=0;$ht<count($checknewdata);$ht++)
                                    {
                                      
                                      $price[]=$checknewdata[$ht]['newpricenew'];
                                      $currency = $checknewdata[$ht]['pricecurr'];
                                    }   
                                    ?>
                                     <input type="hidden" id="countrysel" value="{{$checknewdata[0]['countryname']}}">
                                     <input type="hidden" id="priceicon" value="{{$checknewdata[0]['pricecurr']}}">
                                    <?php
                                    $minprice=min($price);
                                   $maxprice=max($price);
                                   $commonvalue=round(($maxprice-$minprice)/3);
                                    $first=$minprice;
                                    $final=0;
                                    for($price_count=0;$price_count<3;$price_count++)
                                    {
                                      $final=$first+$commonvalue;
                                  ?>
                                   @if($price_count==0)
                                    <label class="checkcontainer"><i class="fa {{$currency}}"></i> {{round($first)}} - <i class="fa {{$currency}}"></i>  {{round($final)}}
                                    @else
                                      <label class="checkcontainer"><i class="fa {{$currency}}"></i> {{round($first+1)}} - <i class="fa {{$currency}}"></i> {{round($final)}}
                                    @endif
                                     @if($price_count==0)
                                    <input type="checkbox" name="priceable" id="priceable{{$price_count}}" value="{{($first)}}-{{round($final)}}">
                                    @else
                                    <input type="checkbox" name="priceable" id="priceable{{$price_count}}" value="{{($first+1)}}-{{round($final)}}">
                                    @endif
                                <span class="checkmark"></span>
                                </label>
                                   <?php $first=$final; } ?>
      
                                </div>
                            </div>

                        </div>


                        <div class="flight-filter">
                           
                            <div class="filter" style="border-bottom: 2px solid #00206a">
                                <h4 style="margin-bottom: 0"><i class="fa fa-audio-description"></i> Advertisement</h4>

                                <div id="demo" class="carousel slide" data-ride="carousel" data-interval="2000">


                                    <!-- The slideshow -->
                                    <div class="carousel-inner">
                                      <?php
                                      for($a=0;$a<count($advlist);$a++)
                                      {
                                        if($a=='0')
                                        {
                                          $chl='active';
                                        }
                                        else
                                        {
                                          $chl='';
                                        }
                                      ?>
                                        <div class="carousel-item {{$chl}}">
                                           <a href="{{url('packagedetails/'.$advlist[$a]['adv_url'])}}"> </a>
                                            <img src="assets/uploads/{{$advlist[$a]['adv_image']}}"
                                                 alt="Los Angeles" width="1100" height="500">
                                                
                                        </div>
                                         
                                      <?php } ?>
                                      
                                    </div>


                                </div>


                            </div>
                           

                        </div>
                    </div>
                </div>
            </div>
                <?php }
                	else
                	{
                	?>
                	<br>
		        	<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="intro_content nano-content">
							<div class="no-flight">
								<h3>No Packages Found</h3>
								<a href="{{url('/index')}}" class="btn btn-back">Go Back</a>
							</div>
						</div>
					</div>
				</div>
		        	</div>
                <?php		
                	}
                 ?>

          

          

            </section>
        
        </div>
    </div>
    
</div>

</body>






<!-- Footer -->
@include('pages.include.footer')
<!-- Copyright -->
@include('pages.include.copyright')
<script>
      $(document).on('change',"input[name='priceable']",function()
      {

          var data_price=[];
          var countrysel=$('#countrysel').val();
          var priceicon=$('#priceicon').val();
          $("input:checkbox[name=priceable]:checked").each(function()
          {
            data_price.push($(this).val());
          });
          var price=data_price.join(",");
         
          $.ajax({
          url:"{{route('search_package')}}",
          type:"GET",
          data:{
                  'price':price,
                  'countrysel':countrysel,
                  'priceicon':priceicon,
                  
              },
          success:function(response)
          {
            $('.packagedata').html(response);
          }
         })
      });

</script>