@include('pages.include.header')

<body>
    <style>
        .intro {
            width: 100%;
            padding-top: 100px;
            padding-bottom: 0px;

        }

        .add_content {
            z-index: 9;
        }

        .about-header {
            background: url("{{asset('assets/images/about-h.jpg')}}");
            height: 450px;
            margin-top: 126px;
            position: relative;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center center;
        }

        h1.about-title {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
            background: rgba(49, 18, 75, 0.8);
            padding: 10px 35px;
        }

        .intro_title {
            text-align: center;
        }

        ul.n-ul {
            margin-top: 30px;
            text-align: left;
        }

        ul.n-ul li {
            padding: 10px 20px;
            color: #9555ef;
            font-size: 15px;
            font-weight: 500;
        }

        ul.n-ul li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
            font-size: 20px;
            font-weight: 900;
        }

        .intro {
            background: white;
        }

        h1.h1-title {
            color: white;
            font-size: 20px;
        }

        .milestones {
            background: white;
        }

        .content-overlay {
            background: rgba(49, 18, 75, 0.8);
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 1;
        }

        p.p-para {
            font-size: 21px;
            color: #ffffff;
            background: linear-gradient(to right, #fa9e1b, #8d4fff, #fa9e1b);
            padding: 10px;
            border-radius: 5px;
        }

        ul.points {

            text-align: initial;
        }

        .column {
            column-count: 2;
        }

        ul.points li {
            padding: 10px;
            color: #8f50fb;
            font-size: 16px;
            font-weight: 600;
        }

        ul.points li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
        }

        .milestone_icon img {
            width: auto;
            height: 50PX;
        }

        @media screen and (max-width:768px) {
            .column {
                column-count: 1;
            }

            ul.points {
                text-align: left !important;
            }
        }

        div.cstm-accordion .card-header {
            /* background: black; */
            font-size: 21px;
            color: #ffffff;
            background: white;
            padding: 10px 30px;
            border-radius: 5px;
            border-radius: 2px;
            border: none;
        }

        div.cstm-accordion .card-header a {
            color: #fa9e1b;
            font-size: 15px;
            display: block;
            text-align: center;
            font-weight: 500;
            text-transform: uppercase;
        }

        div.cstm-accordion .card {
            /* border: none !IMPORTANT; */
            margin-bottom: 10px;
            border: 1px solid #dadada;
        }

        .heading-p {
            font-size: 17px;
            color: #fa9e1b;
            font-weight: 700;
            text-transform: uppercase;
        }

        .contact_form_message {
            height: 126px;
            width: 100%;
            border: none;
            outline: none;
            margin-top: 0px;
            background: transparent;
            font-size: 12px;
            font-weight: 400;
            color: #FFFFFF;
            border-bottom: solid 2px #e1e1e1;
            padding-top: 11px;
        }

        .contact_form_container {
            padding-top: 74px;
            padding-left: 48px;
            padding-right: 48px;
            padding-bottom: 30px;
            margin-bottom: 80px;
            background: linear-gradient(to bottom, #002c, #8d4fff) !important;
            background: rgba(30, 10, 78, 0.84);
        }

        input.input_field {
            margin-bottom: 20px;
        }

        .f-box {
            display: flex;
            position: relative;
            padding: 23px 15px 17px;
            justify-content: space-between !important;
            align-items: flex-start;
            /* background: white; */
            background: url(https://travoweb.com/assets/images/about-h.jpg);
            /* border: 1px solid #fa9e1b; */
            margin-bottom: 30px;
            border-radius: 5px;
            color: white !important;
            background-size: cover;
            background-repeat: no-repeat;
            min-height: 150px;
        }

        p.ph-no {
            line-height: 1;
            margin-top: 10px;

        }

        .f-content p {
            color: #ffffff;
            margin-bottom: 0;
            text-align: left;
            font-size: 16px;
            /* line-height: 1.5; */
        }

        .f-icon {
            flex: 0 0 20%;
            background: #fa9e1b;
            color: #ffffff;
            text-align: center;
            padding: 20px;
            border-radius: 30px;
            max-width: 60px !important;
            height: 60px;

        }

        .f-icon i {
            font-size: 22px;
        }

        p.f-heading {
            color: #834ceb;
            background: #ffffff;
            padding: 5px 15px;
            border-radius: 5px;
            text-align: left;
            font-weight: 600;
            /* display: inline-block; */
        }

        .f-over {
            background: rgba(49, 18, 75, 0.8);
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            border-radius: 5px;
        }

        .f-content {
            flex: 0 0 65%;
        }
        
    </style>
    <div class="super_container">
        <!-- Header -->
        @include ('pages.include.topheader')
        <div class="about-header">
            <h1 class="about-title">Feedback</h1>
        </div>
        <!-- Intro -->
        <div class="intro">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row" style="margin-bottom: 70px;">
                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="f-box">
                                    <div class="f-over"></div>
                                    <div class="f-icon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <div class="f-content">
                                        <p class="f-heading"> Contact Us</p>
                                        <p class="ph-no" style="margin-top: 15px;">Austraila: +61 2 5924 0804 </p>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="f-box">
                                    <div class="f-over"></div>
                                    <div class="f-icon">
                                        <i class="fa fa-envelope"></i>
                                    </div>
                                    <div class="f-content">
                                        <p class="f-heading"> Email Us</p>
                                        <p class="ph-no" style="margin-top: 15px;">info@travoweb.com</p>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="f-box">
                                    <div class="f-over"></div>
                                    <div class="f-icon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <div class="f-content">
                                        <p class="f-heading"> Our Address</p>
                                        <p class="ph-no" style="margin-top: 15px;line-height: 1.5;">7/25 Sheridan Lane
                                            Gundagai NSW 2722
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="contact_form_section">
                            <div class="container">
                                <div class="row">
                                    <div class="col" style="padding:0">

                                        <div class="contact_form_container">
                                            <div class="contact_title text-center">Feedback & Complaints
                                            </div>
                                           
                                            <form action="#" id="contact_form" class="contact_form text-center">
                                                <input type="text" id="contact_form_name"
                                                    class="contact_form_name input_field" placeholder="Name"
                                                    required="required" data-error="Name is required.">
                                                <input type="text" id="contact_form_email"
                                                    class="contact_form_email input_field" placeholder="E-mail"
                                                    required="required" data-error="Email is required.">
                                                <input type="text" id="contact_form_name"
                                                    class="contact_form_name input_field" placeholder="Phone No"
                                                    required="required" data-error="Name is required.">
                                                <input type="text" id="contact_form_email"
                                                    class="contact_form_email input_field" placeholder="Subject"
                                                    required="required" data-error="Email is required.">

                                                <textarea id="contact_form_message"
                                                    class="text_field contact_form_message" name="message" rows="4"
                                                    placeholder="Message" required="required"
                                                    data-error="Please, write us a message."></textarea>
                                                <button type="submit" id="form_submit_button"
                                                    class="form_submit_button button trans_200">send
                                                    message<span></span><span></span><span></span></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->

            @include ('pages.include.footer')
            @include ('pages.include.copyright')
        </div>
    </div>
</body>

</html>