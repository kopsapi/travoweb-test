@include('pages.include.header')

<body>
    <style>
        .intro {
            width: 100%;
            padding-top: 100px;
            padding-bottom: 0px;

        }

        .add_content {
            z-index: 9;
        }

        .about-header {
            background: url("{{asset('assets/images/about-h.jpg')}}");
            height: 450px;
            margin-top: 126px;
            position: relative;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center center;
        }

        h1.about-title {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
            background: rgba(49, 18, 75, 0.8);
            padding: 10px 35px;
        }

        .intro_title {
            text-align: center;
        }

        ul.n-ul {
            margin-top: 30px;
            text-align: left;
        }

        ul.n-ul li {
            padding: 10px 20px;
            color: #9555ef;
            font-size: 15px;
            font-weight: 500;
        }

        ul.n-ul li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
            font-size: 20px;
            font-weight: 900;
        }

        .intro {
            background: white;
        }

        h1.h1-title {
            color: white;
            font-size: 20px;
        }

        .milestones {
            background: white;
        }

        .content-overlay {
            background: rgba(49, 18, 75, 0.8);
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 1;
        }

        p.p-para {
            font-size: 21px;
            color: #ffffff;
            background: linear-gradient(to right, #fa9e1b, #8d4fff, #fa9e1b);
            padding: 10px;
            border-radius: 5px;
        }

        ul.points {

            text-align: initial;
        }

        .column {
            column-count: 2;
        }

        ul.points li {
            padding: 10px;
            color: #8f50fb;
            font-size: 16px;
            font-weight: 600;
        }

        ul.points li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
        }

        .milestone_icon img {
            width: auto;
            height: 50PX;
        }

        @media screen and (max-width:768px) {
            .column {
                column-count: 1;
            }

            ul.points {
                text-align: left !important;
            }
        }

        div.cstm-accordion .card-header {
            /* background: black; */
            font-size: 21px;
            color: #ffffff;
            background: white;
            padding: 10px 30px;
            border-radius: 5px;
            border-radius: 2px;
            border: none;
        }

        div.cstm-accordion .card-header a {
            color: #fa9e1b;
            font-size: 15px;
            display: block;
            text-align: center;
            font-weight: 500;
            text-transform: uppercase;
        }

        div.cstm-accordion .card {
            /* border: none !IMPORTANT; */
            margin-bottom: 10px;
            border: 1px solid #dadada;
        }

        .heading-p {
            font-size: 17px;
            color: #fa9e1b;
            font-weight: 700;
            text-transform: uppercase;
        }
    </style>
    <div class="super_container">
        <!-- Header -->
        @include ('pages.include.topheader')
        <div class="about-header">
            <h1 class="about-title">Travoweb Business</h1>
        </div>
        <!-- Intro -->
        <div class="intro">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12">
                        <div class="intro_content" style="margin-bottom: 80px;">
                            <div class="intro_title">Travoweb Business</div>
                            <p class="intro_text">
                                <p class="heading-p">Product Offering</p>

                                Technology for searching online Flights, Flight+Hotel Deals, International Flights,
                                Hotels, International Hotels, Holidays in India, International Holidays, Cabs, Cheap
                                Tickets to India, Bus Tickets, Rail, Route Planner, Flight Status, Mobile Apps

                                <p class="heading-p">Email us If:</p>

                                Complaints, Contact Us, Payment Security, Privacy Policy, User Agreement, Visa
                                Information, More Offices, File Rail TDR, Make A Payment, Submit Account Details, Report
                                a defect/MMT Bug Bounty

                                <p class="heading-p">Partner Programs </p>

                                Our Retail Stores, Franchise Program Details, Foreign Exchange, Apollo Munich – Travel
                                Insurance, List your hotel

                                <p class="heading-p">Tags </p>

                                Cheap Flights, Flight Status, Chennai Mumbai flights, Mumbai Hyderabad flights,
                                Hyderabad Bangalore flights, Pune Bangalore flights, Mumbai Jaipur flights, Mumbai
                                Chandigarh flights, Bangalore Kolkata flights, Hyderabad Visakhapatnam flights,
                                Chandigarh Bangalore flights, Jaipur Mumbai flights, Hotels in Goa, Hotels in Jaipur,
                                Hotels in Ooty, Kerala Packages, Kashmir Packages, Ladakh Packages.International 700
                                plus sectors.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->

            @include ('pages.include.footer')
            @include ('pages.include.copyright')
        </div>
    </div>
</body>

</html>