@include('pages.include.header')
<div class="super_container">
	<!-- Header -->
	@include('pages.include.topheader')
	<div class="home">
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div>
		<div class="home_content">
			<div class="home_title">Disclaimer Policy</div>
		</div>
	</div>
	<!-- Intro -->
	<div class="intro">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="intro_content nano-content">
						<div class="intro_title">Disclaimer Policy</div>
						<p>Welcome to our website. If you continue to browse and use this website you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our Terms and Conditions policy govern relationship with you in relation to this website. Travoweb makes no representations or warranties, either expressed or implied, with respect to Travoweb Websites or the contents and, to the fullest extent permissible under the law, disclaims all such representations and warranties. Your access to and use of software and other materials on, or through, Travoweb Websites is solely at your own risk. Travoweb makes no warranty whatsoever about the reliability, stability or virus-free nature of such software. Subject to applicable law, under no circumstance is Travoweb responsible for any direct or indirect, incidental, special, punitive, exemplary or consequential damages of any kind (including but not limited to lost profits, lost savings or revenue, or loss or corruption of data or information) which arises out of or is in any way connected with your use of or inability to use Travoweb Websites whether based on breach of contract, tort, negligence, product liability or otherwise, even if advised of the possibility of such damages. This includes any information, products or services obtained through or any contract entered into via Travoweb Websites, but excludes carriage by air performed by airlines in respect of any ticket obtained using the booking engine provided in Travoweb Websites, which is subject to the conditions of carriage and applicable international conventions.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer -->
	@include('pages.include.footer')
	<!-- Copyright -->
		@include('pages.include.copyright')
</div>

</body>
</html>