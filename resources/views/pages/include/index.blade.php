@include('pages.include.header')
<style>
@import url("https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/blitzer/jquery-ui.min.css");

.ui-datepicker td span,
.ui-datepicker td a {
  padding-bottom: 1em;
}

.ui-datepicker td[title]::after {
  content: attr(title);
  display: block;
  position: relative;
  font-size: .8em;
  height: 1.25em;
  margin-top: -1.25em;
  text-align: right;
  padding-right: .25em;
}
#ui-datepicker-div
{
	z-index:12 !important;
}
.table-condensed>thead>tr>th,.table-condensed>thead>tr>td
{
	padding:5px;
}
.dropdown-menu {
	position: absolute;
	top: 100%;
	left: 0;
	z-index: 1000;
	display: none;
	float: left;
	min-width: 160px;
	margin: 2px 0 0;
	font-size: 14px;
	text-align: left;
	list-style: none;
	background-color: #fff;
	-webkit-background-clip: padding-box;
	background-clip: padding-box;
	border: 1px solid #ccc;
	border: 1px solid rgba(0,0,0,.15);
	border-radius: 4px;
	-webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
	box-shadow: 0 6px 12px rgba(0,0,0,.175);
}
.dropdown-menu {
	box-shadow: none;
	border-color: #eee;
}
.search_panel
{
	padding-top:50px;
	padding-bottom:50px;
}
.search_panel_content {
	flex-wrap: wrap;
}
.extras {
	width: 100%;
	margin-bottom:10px;
}
.search_extras_item {
	width: 50%;
	float: left;
	margin-bottom: 10px;
}
.search_extras_item div {
	display: inline-block;
	cursor: pointer;
}
.search_extras_cb {
	display: block;
	position: relative;
	width: 15px;
	height: 15px;
	-webkit-appearance: none;
	-moz-appearance: none;
	-ms-appearance: none;
	-o-appearance: none;
	appearance: none;
	background-color: #FFFFFF;
	border: 1px solid #FFFFFF;
	padding: 9px;
	margin-top: 4px;
	border-radius: 50%;
	display: inline-block;
	position: relative;
	cursor: pointer;
	float: left;
}
.search_extras_cb:checked::after {
	display: block;
	position: absolute;
	top: 2px;
	left: 2px;
	border-radius:50%;
	width: calc(100% - 4px);
	height: calc(100% - 4px);
	content: '';
	background: #fa9e1b;
}
.search_extras label {
	display: block;
	position: relative;
	font-size: 15px;
	font-weight: 400;
	padding-left: 25px;
	margin-bottom: 0px;
	cursor: pointer;
	color: #FFFFFF;
}
@media only screen and (max-width: 1730px)
{
	.search_extras_item {
		width: 20%;
	}
}
@media only screen and (max-width: 1730px)
{
	.search_panel {
		display: none !important;
		width: 100%;
		height: 100%;
		-webkit-animation: fadeEffect 1s;
		animation: fadeEffect 1s;
		margin-top: 0px;
	}
}
</style>
</head>
<body>
	<div class="super_container">
		<!-- Header -->
		@include('pages.include.topheader')
		<!-- Home -->
		<div class="home">
			<!-- Home Slider -->
		<!-- <div class="home_slider_container">
			<div class="owl-carousel owl-theme home_slider">
				<div class="owl-item home_slider_item">
					<div class="home_slider_background" style="background-image:url(images/slider1.jpg)"></div>
				</div>
				<div class="owl-item home_slider_item">
					<div class="home_slider_background" style="background-image:url(images/slider2.jpg)"></div>
				</div>
				<div class="owl-item home_slider_item">
					<div class="home_slider_background" style="background-image:url(images/slider3.jpg)"></div>
				</div>

			</div>
			<div class="home_slider_dots">
				<ul id="home_slider_custom_dots" class="home_slider_custom_dots">
					<li class="home_slider_custom_dot active"><div></div>01.</li>
					<li class="home_slider_custom_dot"><div></div>02.</li>
					<li class="home_slider_custom_dot"><div></div>03.</li>
				</ul>
			</div>
		</div> -->
		<video autoplay muted loop>
			<source src="{{ asset('assets/images/slider-vdo.mp4') }}" type="video/mp4"></source>
		</video>
	</div>
	<!-- Search -->
	@include('pages.include.home-booking')
	<!-- Testimonials -->
	<div class="testimonials">
		<div class="test_border"></div>
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<h2 class="section_title">what our clients say about us </h2>
				</div>
			</div>
			<div class="row">
				<div class="col">
					
					<!-- Testimonials Slider -->

					<div class="test_slider_container">
						<div class="owl-carousel owl-theme test_slider">

							<!-- Testimonial Item -->
							<div class="owl-item">
								<div class="test_item">
									<div class="test_image"><img src="{{ asset('assets/images/test_1.jpg') }}" alt=""></div>
									<div class="test_icon"><img src="{{ asset('assets/images/backpack.png') }}" alt=""></div>
									<div class="test_content_container">
										<div class="test_content">
											<div class="test_item_info">
												<div class="test_name">carla smith</div>
												<div class="test_date">Apr 24, 2019</div>
											</div>
											<div class="test_quote_title">" Best holliday ever "</div>
											<p class="test_quote_text">Nullam eu convallis tortor. Suspendisse potenti. In faucibus massa arcu, vitae cursus mi hendrerit nec.</p>
										</div>
									</div>
								</div>
							</div>

							<!-- Testimonial Item -->
							<div class="owl-item">
								<div class="test_item">
									<div class="test_image"><img src=" {{ asset('assets/images/test_2.jpg') }}" alt=""></div>
									<div class="test_icon"><img src="{{ asset('assets/images/island_t.png') }}"  alt=""></div>
									<div class="test_content_container">
										<div class="test_content">
											<div class="test_item_info">
												<div class="test_name">carla smith</div>
												<div class="test_date">Mar 07, 2019</div>
											</div>
											<div class="test_quote_title">" Best holliday ever "</div>
											<p class="test_quote_text">Nullam eu convallis tortor. Suspendisse potenti. In faucibus massa arcu, vitae cursus mi hendrerit nec.</p>
										</div>
									</div>
								</div>
							</div>

							<!-- Testimonial Item -->
							<div class="owl-item">
								<div class="test_item">
									<div class="test_image"><img src="{{ asset('assets/images/test_3.jpg') }}" alt=""></div>
									<div class="test_icon"><img src="{{ asset('assets/images/kayak.png') }}" alt=""></div>
									<div class="test_content_container">
										<div class="test_content">
											<div class="test_item_info">
												<div class="test_name">carla smith</div>
												<div class="test_date">May 01, 2019</div>
											</div>
											<div class="test_quote_title">" Best holliday ever "</div>
											<p class="test_quote_text">Nullam eu convallis tortor. Suspendisse potenti. In faucibus massa arcu, vitae cursus mi hendrerit nec.</p>
										</div>
									</div>
								</div>
							</div>

							<!-- Testimonial Item -->
							<div class="owl-item">
								<div class="test_item">
									<div class="test_image"><img src="{{ asset('assets/images/test_2.jpg') }}" alt=""></div>
									<div class="test_icon"><img src="{{ asset('assets/images/island_t.png') }}" alt=""></div>
									<div class="test_content_container">
										<div class="test_content">
											<div class="test_item_info">
												<div class="test_name">carla smith</div>
												<div class="test_date">Mar 19, 2019</div>
											</div>
											<div class="test_quote_title">" Best holliday ever "</div>
											<p class="test_quote_text">Nullam eu convallis tortor. Suspendisse potenti. In faucibus massa arcu, vitae cursus mi hendrerit nec.</p>
										</div>
									</div>
								</div>
							</div>

							<!-- Testimonial Item -->
							<div class="owl-item">
								<div class="test_item">
									<div class="test_image"><img src=" {{ asset('assets/images/test_1.jpg') }}" alt=""></div>
									<div class="test_icon"><img src="{{ asset('assets/images/backpack.png') }}" alt=""></div>
									<div class="test_content_container">
										<div class="test_content">
											<div class="test_item_info">
												<div class="test_name">carla smith</div>
												<div class="test_date">Apr 24, 2019</div>
											</div>
											<div class="test_quote_title">" Best holliday ever "</div>
											<p class="test_quote_text">Nullam eu convallis tortor. Suspendisse potenti. In faucibus massa arcu, vitae cursus mi hendrerit nec.</p>
										</div>
									</div>
								</div>
							</div>

							<!-- Testimonial Item -->
							<div class="owl-item">
								<div class="test_item">
									<div class="test_image"><img src="{{ asset('assets/images/test_3.jpg') }}" alt=""></div>
									<div class="test_icon"><img src="{{ asset('assets/images/kayak.png') }}" alt=""></div>
									<div class="test_content_container">
										<div class="test_content">
											<div class="test_item_info">
												<div class="test_name">carla smith</div>
												<div class="test_date">Apr 18, 2019</div>
											</div>
											<div class="test_quote_title">" Best holliday ever "</div>
											<p class="test_quote_text">Nullam eu convallis tortor. Suspendisse potenti. In faucibus massa arcu, vitae cursus mi hendrerit nec.</p>
										</div>
									</div>
								</div>
							</div>

						</div>

						<!-- Testimonials Slider Nav - Prev -->
						<div class="test_slider_nav test_slider_prev">
							<svg version="1.1" id="Layer_6" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							width="28px" height="33px" viewBox="0 0 28 33" enable-background="new 0 0 28 33" xml:space="preserve">
							<defs>
								<linearGradient id='test_grad_prev'>
									<stop offset='0%' stop-color='#fa9e1b'/>
									<stop offset='100%' stop-color='#8d4fff'/>
								</linearGradient>
							</defs>
							<path class="nav_path" fill="#F3F6F9" d="M19,0H9C4.029,0,0,4.029,0,9v15c0,4.971,4.029,9,9,9h10c4.97,0,9-4.029,9-9V9C28,4.029,23.97,0,19,0z
							M26,23.091C26,27.459,22.545,31,18.285,31H9.714C5.454,31,2,27.459,2,23.091V9.909C2,5.541,5.454,2,9.714,2h8.571
							C22.545,2,26,5.541,26,9.909V23.091z"/>
							<polygon class="nav_arrow" fill="#F3F6F9" points="15.044,22.222 16.377,20.888 12.374,16.885 16.377,12.882 15.044,11.55 9.708,16.885 11.04,18.219 
							11.042,18.219 "/>
						</svg>
					</div>

					<!-- Testimonials Slider Nav - Next -->
					<div class="test_slider_nav test_slider_next">
						<svg version="1.1" id="Layer_7" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						width="28px" height="33px" viewBox="0 0 28 33" enable-background="new 0 0 28 33" xml:space="preserve">
						<defs>
							<linearGradient id='test_grad_next'>
								<stop offset='0%' stop-color='#fa9e1b'/>
								<stop offset='100%' stop-color='#8d4fff'/>
							</linearGradient>
						</defs>
						<path class="nav_path" fill="#F3F6F9" d="M19,0H9C4.029,0,0,4.029,0,9v15c0,4.971,4.029,9,9,9h10c4.97,0,9-4.029,9-9V9C28,4.029,23.97,0,19,0z
						M26,23.091C26,27.459,22.545,31,18.285,31H9.714C5.454,31,2,27.459,2,23.091V9.909C2,5.541,5.454,2,9.714,2h8.571
						C22.545,2,26,5.541,26,9.909V23.091z"/>
						<polygon class="nav_arrow" fill="#F3F6F9" points="13.044,11.551 11.71,12.885 15.714,16.888 11.71,20.891 13.044,22.224 18.379,16.888 17.048,15.554 
						17.046,15.554 "/>
					</svg>
				</div>

			</div>

		</div>
	</div>

</div>
</div>

<div class="contact">
	<div class="contact_background" style="background-image:url({{ asset('assets/images/contact.png') }})"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
				<div class="contact_image">
				</div>
			</div>
			<div class="col-lg-7">
				<div class="contact_form_container">
					<div class="contact_title">get in touch</div>
					<form action="#" id="contact_form" class="contact_form">
						<input type="text" id="contact_form_name" class="contact_form_name input_field" placeholder="Name" required="required" autocomplete="off" data-error="Name is required.">
						<input type="text" id="contact_form_email" class="contact_form_email input_field" placeholder="E-mail" required="required" autocomplete="off" data-error="Email is required.">
						<input type="text" id="contact_form_subject" class="contact_form_subject input_field" placeholder="Subject" required="required" autocomplete="off" data-error="Subject is required.">
						<textarea id="contact_form_message" class="text_field contact_form_message" name="message" rows="4" placeholder="Message" required="required" autocomplete="off" data-error="Please, write us a message."></textarea>
						<button type="submit" id="form_submit_button" class="form_submit_button button">send message<span></span><span></span><span></span></button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Payment -->
<div class="payment">
	<div class="container">
		<div class="row">
			<div class="col-lg-2 col-sm-6">
				<div class="payment-card">
					<img src="{{ asset('assets/images/visa-express.png') }}" class="payment-img img-responsive">
				</div>
			</div>
			<div class="col-lg-2 col-sm-6">
				<div class="payment-card">
					<img src="{{ asset('assets/images/mastercard.png') }}" class="payment-img img-responsive">
				</div>
			</div>
			<div class="col-lg-2 col-sm-6">
				<div class="payment-card">
					<img src="{{ asset('assets/images/maestro.png') }}" class="payment-img img-responsive">
				</div>
			</div>
			<div class="col-lg-2 col-sm-6">
				<div class="payment-card">
					<img src="{{ asset('assets/images/paypal.png') }}" class="payment-img img-responsive">
				</div>
			</div>
			<div class="col-lg-2 col-sm-6">
				<div class="payment-card">
					<img src="{{ asset('assets/images/american-express.png') }}" class="payment-img img-responsive">
				</div>
			</div>
			<div class="col-lg-2 col-sm-6">
				<div class="payment-card">
					<img src="{{ asset('assets/images/visa-elctron.png') }}" class="payment-img img-responsive">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Footer -->
@include('pages.include.footer')
<!-- Copyright -->
@include('pages.include.copyright')
</div>
<!-- jQuery 3 -->
<!-- Price Datepicker -->
<!-- <script>
	$(document).on('click','.ui-datepicker-prev',function(){
		// alert('previos');
		var date = new Date();
		var mon = $('.ui-datepicker-month').text();
		var year = $('.ui-datepicker-year').text();
		day     = date.getDate()  < 10 ? '0' + date.getDate()  : date.getDate();
		// alert(day);
		// alert(mon);
		// alert(year)
	})
	$(document).on('click','.ui-datepicker-next',function(){
		// alert('nxt');
		var date = new Date();
		var mon = $('.ui-datepicker-month').text();
		var year = $('.ui-datepicker-year').text();
		day     = 1;
		// alert(day);
		// alert(mon);
		// alert(year)
	})
	$(function() {
    var date = new Date();
	date.setDate(date.getDate());
	// alert(date.setDate(date.getDate()));
	day     = date.getDate()  < 10 ? '0' + date.getDate()  : date.getDate();
	// alert(day);
  var dayrates = [100, 150, 150, 150, 150, 250, 250];

  $("#departure").datepicker({
  	minDate: 0,
  	dateFormat: 'dd/mm/yyyy',

    beforeShowDay: function(date) {
      var selectable = true;
      var classname = "";
      var title = "\u20B9" + dayrates[date.getDay()];
      return [selectable, classname, title];
    }
  });
});
</script> -->
<!-- Price Datepicker -->
<script>
	$(function () {
    //Departure
    var date = new Date();
    date.setDate(date.getDate());
    $('.departure').datepicker({
    	autoclose: true,
    	todayHighlight: true,
    	format: 'dd/mm/yyyy',
    	startDate: date

    })
    //Date picker
    $('#return').datepicker({
    	autoclose: true,
    	todayHighlight: true,
    	format: 'dd/mm/yyyy',
    	startDate: date
    })
    //Check In
    $('#check-in').datepicker({
    	autoclose: true,
    	todayHighlight: true,
    	format: 'dd/mm/yyyy',
    	startDate: date
    })
    //Date picker
    $('#check-out').datepicker({
    	autoclose: true,
    	todayHighlight: true,
    	format: 'dd/mm/yyyy',
    	startDate: date
    })

})
</script>
<script>
	// $("input[name='one']").change(function()
	// {
	// 	if($("input[name='one']:checked").prop('id')=='search_extras_1')
	// 	{
	// 		$('#restuntype').hide();
	// 		$("#return").attr("disabled", "disabled"); 
	// 	}
	// 	else
	// 	{
	// 	    $('#restuntype').hide();	
	// 		$("#return").removeAttr("disabled");  
	// 	}
	// });
	$("input[name='one']").change(function()
	{

		if($("input[name='one']:checked").val()=='1')
		{
			$('#restuntype').hide();
			$('.multicity-main').hide();
		}
		else if($("input[name='one']:checked").val()=='2')
		{
			$('#restuntype').show();
			$('.multicity-main').hide();
		}
		else if($("input[name='one']:checked").val()=='3')
		{
			$('.multicity-main').show();
			$('#restuntype').hide();
		}
		else
		{
			$('.multicity-main').hide();
			$('#restuntype').hide();
		}
		// if($("input[name='one']:checked").prop('id')=='search_extras_2')
		// {
		// 	$('#restuntype').show();
		// 	$("#return").attr("disabled", "disabled"); 
		// }
		// else
		// {
		//     $('#restuntype').hide();	
		// 	$("#return").removeAttr("disabled");  
		// }
		// if($("input[name='one']:checked").prop('id')=='search_extras_3')
		// {
		// 	 $('#restuntype').hide();

		// 	$('.multicity-main').show();
		// }
		// else
		// {
		// 	$('#restuntype').show();

		//     $('.multicity-main').hide();
		// }
		// if($("input[name='one']:checked").prop('id')=='search_extras_1')
		// {
		// 	 $('#restuntype').hide();

		// }
		// else
		// {
		// 	$('#restuntype').show();

		// }
	});

</script>
<script>
	$("input[name='one']").change(function()
	{
		if($("input[name='one']:checked").val()=='2')
		{
				$("#departure").on("change",function (){ 
			   // console.log($(this).val());
			   var date = $('#departure').datepicker('getDate', '+1d');
			   date.setDate(date.getDate() + 1);
			   $('#return').datepicker('setDate',date);
			});
			
		}
	
	});
</script>
<script>
	$(document).on('click','.add_more',function(){
		var id=this.id;
		var id1=id.split("-");
		var lastid=id1[1];
		var flight_to = $('.flight_to-'+lastid).val();
		var flight_from = $('.flight_from-'+lastid).val();
		var flight_dep = $('.flight_dep-'+lastid).val();
		if(flight_from=='')
		{
			alert("Please Select Orign");
		}
		else if(flight_to=='')
		{
			alert("Please Select Destination");
		}
		else if(flight_dep=='')
		{
			alert("Please Select Departure Date");
		}
		else
		{

			$.ajax({
				url : "{{route('showmulticity')}}",
				data : {'lastid' :  lastid ,
				'flight_to' : flight_to,
			},
			type : 'GET',
			success : function(data)
			{

				$('.showmultinew').append(data);
				$('.more_options-'+lastid).hide();
				var date = new Date();
				date.setDate(date.getDate());
				$('.departure').datepicker({
					autoclose: true,
					todayHighlight: true,
					format: 'dd/mm/yyyy',
					startDate: date

				})


			}
		});
		}


	})
</script>
<script>
	$(document).on('click','.remove', function(){
		var id=this.id;
		var lastid = parseInt(id) - 1;
		$("span#mul-"+id).remove();
		if(id == '1')
		{
			$('.more_options-1').show();
		}
		else
		{
			$('.more_options-'+lastid).show();
		}
		
		
	})
</script>
<!-- <script>
$(document).ready(function(){
  $(".more_options").click(function(){
    $(".multicity-another-flight").css("display", "flex");
  });
});
</script>
<script>
$(document).ready(function(){
  $(".close-icon").click(function(){
    $(".multicity-another-flight").css("display", "none");
  });
});
</script> -->
<script>
	//SCRIPTS FOR BOOKING AREA
	$("#autocomplete1").on('keyup',function()
	{
		if($("#autocomplete").val().toLowerCase()==$(this).val().toLowerCase())
		{
			alert("From & To airports cannot be the same");
		}
	});
	$("#search_form_1").submit(function(e)
	{

		
		var depdate = $("#departure").val();
		var rettype = $('.returnchk').val();
		
		var dateOne = new Date(depdate); //Year, Month, Date  
       var dateTwo = new Date(rettype); //Year, Month, Date  
       
       var error=0;
       if($("#autocomplete").val().trim()=="")
       {
       	error++;
       	alert("Please Enter From Airport");
       }
       if($("#autocomplete1").val().trim()=="")
       {
       	error++;
       	alert("Please Enter Departure Date");
       }
       if($("#departure").val().trim()=="")
       {
       	error++;
       	alert("Please Enter Departure Date");
       }
       if($("input[name='one']:checked").val()=='2' && $('.returnchk').val().trim()=='')
       {
       	error++;
       	alert("Please Enter Return Date");
       }
       if($("input[name='one']:checked").val()=='2' && $('.returnchk').val().trim()!='' && dateOne >= dateTwo)
       {
       	error++;
       	alert("Please Enter Return Date Equal or Greater Than Departure Date");
       }
       if($("#autocomplete").val().toLowerCase()==$("#autocomplete1").val().toLowerCase())
       {
       	error++;
       	alert("From & To airports cannot be the same");
       }

       if(error==1)
       {
       	e.preventDefault();
			// var formdata=new FormData($("#search_form_1")[0]);

			// $.ajax({
			// 	url:"{{route('flightsearch')}}",
			// 	type:"POST",
			// 	data: formdata,
			// 	processData:false,
			// 	contentType:false,
			// 	success:function(response)
			// 	{
			// 	 console.log(response);
			// 	}
			// })
		}
	});
</script>
<script>
	$(document).on('click','#opendiv',function(){
		$('#showdiv').toggle();
	})
</script>
<script>
	$(document).on('keyup','#search-keyword',function()
	{
		var keyword=$(this).val().trim();
		if(keyword!='')
		{
			$.ajax({
				url : '{{route("getcities")}}',
				type: 'GET',
				data: {'citykeyword' :keyword},
				success: function(response)
				{
					$('#cities1').html('');
    		// console.log(response);
    		$('#cities1').html(response);
    	}
    })
		}
	});

</script>
<script>
	$("#check-in").on("change",function (){ 
   // console.log($(this).val());
   var date = $('#check-in').datepicker('getDate', '+1d');
   date.setDate(date.getDate() + 1);
   $('#check-out').datepicker('setDate',date);
});
</script>

<script>
	$(document).on('submit','#search_form_2',function(e)
	{
		var errors='';
		var city=$("#search-keyword").val();
		var indate=$("#check-in").val();
		var outdate=$("#check-out").val();
		var rooms=$("#rooms").val();
		// var adults=$("#adults_2").val();
		// var children=$("#children_2").val();
		var roomval=$(".roomval").val();
		

		if(city.trim()=='')
		{
			errors+="Going to field cannot be empty ," ;
		}
		if(indate.trim()=='')
		{
			errors+="Check-in date field cannot be empty ," ;
		}
		if(outdate.trim()=='')
		{
			errors+="Check-out date field cannot be empty ," ;
		}
		// if(rooms.trim()=='')
		// {
		// 	errors+="Please select no. of rooms ," ;
		// }
		// if(adults.trim()=='')
		// {
		// 	errors+="Please select no. of adults ," ;
		// }
		// if(errors=='')
		// {
		// 	console.log(city+'-'+indate+'-'+outdate+'-'+rooms+'-'+adults+'-'+children);
		// }
		if(errors!='')
		{
			e.preventDefault();
			alert(errors);
		}
	})
</script>
<script>
	$(document).on('click','.searchflight',function(){
		$('.dropshowflight').toggle();
	})

	$(document).on('click','.adultplus',function(){
		var chkval = $('.adultval').val();
		var chkplus = $('.childval').val();
		var infantval = $('.infantval').val();
		if(chkval < 9)
		{
			var chkval1=parseInt(chkval)+1;
			$('.adultval').val(chkval1);
			$('.adultcount').text(chkval1);
			var totaltravelcount=parseInt(chkval1)+parseInt(chkplus)+parseInt(infantval);
			$('.totaltravel').val(totaltravelcount + ' Travelers');
			
		}
		else
		{
			$('.adultval').val(chkval);
			$('.adultcount').text(chkval);
			$('.totaltravel').val(chkval + ' Travelers');
			var totaltravelcount=parseInt(chkval)+parseInt(chkplus)+parseInt(infantval);
			$('.totaltravel').val(totaltravelcount + ' Travelers');
		}
	});
	$(document).on('click','.adultminus',function(){
		var chkval = $('.adultval').val();
		var chkplus = $('.childval').val();
		var infantval = $('.infantval').val();
		if(chkval!=1)
		{
			var chkval1=parseInt(chkval)-1;
			$('.adultval').val(chkval1);
			$('.adultcount').text(chkval1);
			var totaltravelcount=parseInt(chkval1)+parseInt(chkplus)+parseInt(infantval);
			$('.totaltravel').val(totaltravelcount + ' Travelers');
		}
		else
		{
			$('.adultval').val(1);
			$('.adultcount').text(1);
			var totaltravelcount=parseInt(chkval)+parseInt(chkplus)+parseInt(infantval);
			$('.totaltravel').val(totaltravelcount + ' Travelers');
		}
	});
	$(document).on('click','.childplus',function(){
		var chkval = $('.adultval').val();
		var chkplus = $('.childval').val();
		var infantval = $('.infantval').val();
		if(chkplus < 9)
		{

			var chkplus1=parseInt(1)+parseInt(chkplus);
			$('#childval').val(chkplus1);
			$('.childcount').text(chkplus1);
			var totaltravelcount=parseInt(chkval)+parseInt(chkplus1)+parseInt(infantval);
			$('.totaltravel').val(totaltravelcount + ' Travelers');

		}
		else
		{
			$('#childval').val(chkplus);
			$('.childcount').text(chkplus);
			var totaltravelcount=parseInt(chkval)+parseInt(chkplus)+parseInt(infantval);
			$('.totaltravel').val(totaltravelcount + ' Travelers');
		}
	});
	$(document).on('click','.childminus',function(){
		var chkval = $('.adultval').val();
		var chkplus = $('.childval').val();
		var infantval = $('.infantval').val();		
		if(chkplus!=0)
		{
			var chkplus1=parseInt(chkplus)-parseInt(1);
			$('#childval').val(chkplus1);
			$('.childcount').text(chkplus1);
			var totaltravelcount=parseInt(chkval)+parseInt(chkplus1)+parseInt(infantval);
			$('.totaltravel').val(totaltravelcount + ' Travelers');
		}
		else
		{
			$('#childval').val(0);
			$('.childcount').text(0);
			var totaltravelcount=parseInt(chkval)+parseInt(0)+parseInt(infantval);
			$('.totaltravel').val(totaltravelcount + ' Travelers');
		}
	});
	$(document).on('click','.infantplus',function(){
		var chkval = $('.adultval').val();
		var chkplus = $('.childval').val();
		var infantval = $('.infantval').val();

		if(infantval < 9)
		{

			var infantval1=parseInt(1)+parseInt(infantval);
			$('#infantval').val(infantval1);
			$('.infantcount').text(infantval1);
			var totaltravelcount=parseInt(chkval)+parseInt(chkplus)+parseInt(infantval1);
			$('.totaltravel').val(totaltravelcount + ' Travelers');
		}
		else
		{
			$('#infantval').val(infantval);
			$('.infantcount').text(infantval);
			var totaltravelcount=parseInt(chkval)+parseInt(chkplus)+parseInt(0);
			$('.totaltravel').val(totaltravelcount + ' Travelers');
		}
	});
	$(document).on('click','.infantminus',function(){
		var chkval = $('.adultval').val();
		var chkplus = $('.childval').val();
		var infantval = $('.infantval').val();
		if(infantval!=0)
		{
			var infantval1=parseInt(infantval)-parseInt(1);
			$('#infantval').val(infantval1);
			$('.infantcount').text(infantval1);
			var totaltravelcount=parseInt(chkval)+parseInt(chkplus)+parseInt(infantval1);
			$('.totaltravel').val(totaltravelcount + ' Travelers');
		}
		else
		{
			$('#infantval').val(0);
			$('.infantcount').text(0);
			var totaltravelcount=parseInt(chkval)+parseInt(chkplus)+parseInt(0);
			$('.totaltravel').val(totaltravelcount + ' Travelers');
		}
	});
// 	$(function() {
    

//     $(".minus").click(function() {
//         var text = $(this).prev(":text");
//         text.val(parseInt(text.val(), 10) - 1);
//     });
// });
</script>
<script>
	$(document).on('click','#flightdone',function(){
		$('.dropshowflight').hide();
	});
</script>
<!-- hotel dropsearch -->
<script>
	$(document).on('click','.searchhotel',function(){

		$('.dropshowhotel').toggle();
	});
	$(document).on('click','.roomplus',function(){
		var rooms = $('.roomval').val();
		if(rooms < 9)
		{
			var rooms1=parseInt(rooms)+1;
			$('.roomval').val(rooms1);
			$('.roomcount').text(rooms1);
			$('.newroom').append('<div class="roomcheckcount-'+rooms1+'"><div class="row"><div class="col-md-12" ><div class="trave-drop"><div class="travel-left">Room '+rooms1+'</div><div class="travel-left"><small>Adults (12 + years)</small></div><div class="plus-minus "><div class="minus roomadultminus" id="'+rooms1+'"><i class="fa fa-minus-circle"></i></div><input type="hidden" name="roomadult-'+rooms1+'" id="adultval" class="roomadultval-'+rooms1+'" value="1"  /><div class="text roomadultcount-'+rooms1+'">1</div><div class="plus roomadultplus" id="'+rooms1+'"><i class="fa fa-plus-circle"></i></div></div></div><div class="trave-drop"><div class="travel-left"><small>Child (<12 years)</small></div><div class="plus-minus "><div class="minus roomchildminus"  id="'+rooms1+'" ><i class="fa fa-minus-circle"></i></div><input type="hidden" name="children-'+rooms1+'" id="roomchildval" class="roomchildval-'+rooms1+' chkchild" value="0"  /><div class="text roomchildcount-'+rooms1+'">0</div><div class="plus roomchildplus" id="'+rooms1+'"><i class="fa fa-plus-circle"></i></div></div></div><div class="trave-drop showchilddiv-'+rooms1+'" style="display: none"></div></div></div>');
			$('.totalrooms').val(rooms1 + " Rooms");
		}
		else
		{
			$('.roomval').val(rooms);
			$('.roomcount').text(rooms);
			$('.totalrooms').val(rooms+ " Rooms");
			
		}
	});
//roome minus
$(document).on('click','.roomminus',function(){
		var rooms = $('.roomval').val();
		
		if(rooms!=1)
		{
			var rooms1=parseInt(rooms)-1;
			$('.roomval').val(rooms1);
			$('.roomcount').text(rooms1);
			$('.roomcheckcount-'+rooms).remove();
			$('.totalrooms').val(rooms1 + " Rooms");
			
		}
		else
		{
			$('.roomval').val(1);
			$('.roomcount').text(1);
			$('.totalrooms').val( "1 Rooms");
			
		}
	});
//roomadult
$(document).on('click','.roomadultplus',function(){
	var newid = this.id;
		var roomadult = $('.roomadultval-'+newid).val();
		
		if(roomadult < 3)
		{
			var roomadult1=parseInt(roomadult)+1;
			$('.roomadultval-'+newid).val(roomadult1);
			$('.roomadultcount-'+newid).text(roomadult1);
			
			
		}
		else
		{
			$('.roomadultval-'+newid).val(roomadult);
			$('.roomadultcount-'+newid).text(roomadult);
			
		}
	});
	$(document).on('click','.roomadultminus',function(){
		var newid = this.id;
		var roomadult = $('.roomadultval-'+newid).val();
		
		if(roomadult!=1)
		{
			var roomadult1=parseInt(roomadult)-1;
			$('.roomadultval-'+newid).val(roomadult1);
			$('.roomadultcount-'+newid).text(roomadult1);
			
		}
		else
		{
			$('.roomadultval-'+newid).val(1);
			$('.roomadultcount-'+newid).text(1);
			
		}
	});
	//childadult
$(document).on('click','.roomchildplus',function(){
	var newid = this.id;
	
		var roomchild = $('.roomchildval-'+newid).val();
		
		if(roomchild < 2)
		{
			var roomchild1=parseInt(roomchild)+1;
			$('.roomchildval-'+newid).val(roomchild1);
			$('.roomchildcount-'+newid).text(roomchild1);
			$('.showchilddiv-'+newid).show();
				$('.showchilddiv-'+newid).append('<div class="child-age childage-'+roomchild1+'"><label>Child '+roomchild1+' age</label><select class="dropdown_item_select search_input agenew" name="childage-'+newid+'[]"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><select></div>');
		}
		else
		{
			$('.roomchildval-'+newid).val(roomchild);
			$('.roomchildcount-'+newid).text(roomchild);
			
		}
	});
	$(document).on('click','.roomchildminus',function(){
		var newid = this.id;
		
		var roomchild = $('.roomchildval-'+newid).val();
		
		if(roomchild!=0)
		{
			var roomchild1=parseInt(roomchild)-1;
			$('.roomchildval-'+newid).val(roomchild1);
			$('.roomchildcount-'+newid).text(roomchild1);
			$('.childage-'+roomchild).remove();
		}
		else
		{
			$('.roomchildval-'+newid).val(0);
			$('.roomchildcount-'+newid).text(0);
			$('.childage-'+roomchild).remove();
			$('.showchilddiv-'+newid).hide();
			
		}
	});
</script>
<script>
	$(document).on('click','#hoteldone',function(){
		$('.dropshowhotel').hide();
	});
</script>
<script>
	$(document).on('click','.exchangeicon',function(){
		var chk = this.id;
		var flightfrom=$('.flight_from-'+chk).val();
		var flightto=$('.flight_to-'+chk).val();
		
		$('.flight_from-'+chk).val(flightto);
		$('.flight_to-'+chk).val(flightfrom);
	})
</script>

</body>
</html>
