<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Travoweb</title>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Travelix Project">
   <meta name="google-site-verification" content="uUjF_lbUGwxdQW-78_IQdUJqhvkUgazeFDOsJQfxP7E" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('assets/styles/bootstrap4/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/scripts/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/scripts/OwlCarousel2-2.2.1/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/scripts/OwlCarousel2-2.2.1/owl.theme.default.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/scripts/OwlCarousel2-2.2.1/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/scripts/OwlCarousel2-2.2.1/animate.css') }}" rel="stylesheet">

   
    <link href="{{ asset('assets/styles/about_styles.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/styles/main_styles.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/styles/main.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/styles/hotel-web.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/styles/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/styles/about_responsive.css') }}" rel="stylesheet">
    
    <link href="{{ asset('assets/styles/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link href="{{ asset('assets/sweetalert/sweetalert.css') }}" rel="stylesheet">

    <style>
    .l-img{
        width: 165px;
        display: block;
        margin: auto;
    }
    .s-note{
        text-align: center;
        color: #ffc107;
        padding-bottom: 10px;
    }
    .c-in1{
        background: blueviolet;
        text-align: center;
        color: white;
    }

    .loadernew {
        font-size: 8px;
        margin: 0px auto 30px auto;
        width: 11em;
        height: 11em;
        border-radius: 50%;
        background: #ffffff;
        border-top: 6px solid #fa9e1b;
        border-left: 6px solid #fa9e1b;
        border-right: 6px solid #fa9e1b;
        position: relative;
        animation: load3 1.4s linear infinite;
    }


    @keyframes load3 {
        0% {

            transform: rotate(0deg);
            border-top: 6px solid #fa9e1b;
            border-left: 6px solid #fa9e1b;
            border-right: 6px solid #fa9e1b;

        }
        100% {

            transform: rotate(360deg);
            border-top: 6px solid #9271dc;
            border-left: 6px solid #9271dc;
            border-right: 6px solid #9271dc;
        }
    }
</style>