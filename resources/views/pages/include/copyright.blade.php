<div class="copyright">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="copyright_content d-flex flex-row align-items-center" style="text-align:center">
					<div style="text-align: center;
    display: block;
    width: 100%;">&copy; 2019.Travoweb.com.  All rights reserved</div>
				</div>
			</div>
		<!--	<div class="col-lg-9 order-lg-2 order-1">
				<div class="footer_nav_container d-flex flex-row align-items-center justify-content-lg-end">
					<div class="footer_nav">
						<ul class="footer_nav_list">
							<li class="footer_nav_item"><a href="javascript:void()">Flights</a></li>
							<li class="footer_nav_item"><a href="javascript:void()">Hotels</a></li>
						
						</ul>
					</div>
				</div>
			</div>-->
		</div>
	</div>
</div>

<div class="modal fade" id="loginModal" style=" padding-left: 17px; padding-right:0px !important;">
	<div class="modal-dialog form-dark" role="document">
		<!--Content-->
		<div class="modal-content card card-image" style="margin-top: 13%;background-image: url('{{asset('assets/images/modal-img.jpg')}}'); background-repeat: no-repeat; background-size: cover">
			<div class="text-white rgba-stylish-strong py-4 px-5 z-depth-4">
				<!--Header-->
				<div class="modal-header text-center pb-3">
					<h3 class="modal-title w-100 white-text font-weight-bold" id="myModalLabel"><strong>LOG</strong> <a
								class="green-text font-weight-bold"><strong style="color: #fa9e1b;">IN</strong></a></h3>

					<span class="close2 close" data-dismiss="modal">&times</span>
				</div>
				<!--Body-->
				<div class="modal-body">
					<form id="loginForm">
						{{ csrf_field() }}
						<h5 id="successMessage1" class="alert alert-success" style="display: none"></h5>
						<h5 id="failMessage1" class="alert alert-danger" style="display: none"></h5>
						<div class="form-group md-form mb-4">
							<input type="email" name="login_email" class="form-control t-email" placeholder="Your Email" autofocus>

							<span id="login_email_err" class="text-danger" style="display:none" ></span>
						</div>

						<div class="form-group md-form  mb-5">
							<input type="password"  name="login_password" id="Form-pass5" class="form-control t-email" placeholder=" Your Password">

							<span id="login_password_err" class="text-danger" style="display:none" ></span>

						</div>

						<div class="form-group">
							<button type="submit" name="submit" class="form-control t-btn"> LOGIN</button>
						</div>


						<div class="container login-or" style="">
						<div class="vl">
							<span class="vl-innertext">or</span>
						</div>
						</div>

						<div class="row" style="margin-top: 65px;">
							<div class="col-sm-6 s-icon mb-5">
						<a href="{{ url('login/facebook') }}" class="fb btn">
							<div class="fb-icon-l">
							<i class="fa fa-facebook fa-fw"></i>
							</div>
							<span>Login with</span>
						</a>
							</div>
							<div class="col-sm-6 s-icon">
						<a href="{{ url('/redirect') }}" class="google btn">
							<div class="fb-icon-l">
								<i class="fa fa-google fa-fw">
								</i>
							</div>
							<span>Login with</span>
						</a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">

								<p class="forgot-p">Forgot password ? <a href="#" data-toggle="modal" data-target="#forget">Click Here!!</a> </p>

							</div>
						</div>
					</form>


				</div>
			</div>
		</div>
		<!--/.Content-->
	</div>
</div>
<!-- modal-forget-->

<div class="modal fade text-center py-5" style="top:30px; background: none" id="forget">
    <div class="modal-dialog modal-login" role="document">
        <div class="modal-content" style="position: relative;left: 0%;margin:auto; !important;background: rgb(53, 55, 64); padding: 2em;">
            <div class="modal-header h-bg">
				<h3>Change Password</h3>
                <span class="close close-btn" data-dismiss="modal">&times;</span>

            </div>
           		
	            	<input name="_token" type="hidden" class="csrf_token" id="csrf_token" value="{{ csrf_token() }}"/>
		            <div class="modal-body">
		            	<!-- login form  -->
				            	<form id="forgotForm">
								{{ csrf_field() }}
								<h5 id="successMessage1" class="alert alert-success" style="display: none"></h5>
								<h5 id="failMessage1" class="alert alert-danger" style="display: none"></h5>
								<div class="form-group md-form mb-4">
									<input type="email" name="forgotemail" id="forgotemail" class="form-control t-email forgotemail" placeholder="Your Email" autofocus>

									<span id="login_email_err" class="text-danger eerror" style="display:none" ></span>
								</div>
								<div class="form-group">
									<button type="submit" name="submit" class="form-control t-btn"> Submit</button>
								</div>


							</form>
		            	<!-- end loginform -->
						<!-- <div class="form-group md-form mt-3">
							<input type="email" name="forgotemail" id="forgotemail" class="form-control t-email" placeholder="Your Email" autofocus>

							<span id="login_email_err" class="text-danger eerror" style="display:none" ></span>
						</div> -->



		            </div>
		            <!-- <div class="modal-footer" style="border: none">
		            	<button type="button" class="btn1 text-white" id="forgot-button" >Submit</button>
		                >
		            </div> -->
		        
       		
        </div>
    </div>
</div>
<!-- guest login -->
<div id="guestModal" class="modal fade">
	<div class="modal-dialog form-dark" role="document">
		<div class="modal-content card card-image" style="background-image: url('{{asset('assets/images/modal-img.jpg')}}'); background-repeat: no-repeat; background-size: cover">
		<div class="text-white rgba-stylish-strong py-5 px-5 z-depth-4">
		<div class="modal-header text-center pb-3">
			<h3 class="modal-title w-100 white-text font-weight-bold" id="myModalLabel"><strong>GUEST LOG</strong> <a
						class="green-text font-weight-bold"><strong style="color: #fa9e1b;">IN</strong></a></h3>

			<span class="close1 close" data-dismiss="modal">&times</span>
			</div>
			<div class="modal-body">
				<form id="guestForm">
					{{ csrf_field() }}
					<h5 id="successMessage2" class="alert alert-success" style="display: none"></h5>
					<h5 id="failMessage2" class="alert alert-danger" style="display: none"></h5>
					<div class="form-group md-form mb-5">

						<input type="text" name="guest_email" class="form-control t-email" placeholder="Email Address">
						<span id="guest_email_err" class="text-danger err-msg" style="display:none" ></span>
					</div>
					<div class="form-group md-form pb-3">

						<input type="text" name="guest_phone" class="form-control t-email"  placeholder="Phone No">
						<span id="guest_phone_err" class="text-danger err-msg" style="display:none" ></span>
					</div>
					<div class="form-group">
						<button type="submit" name="submit" class="form-control t-btn"> Guest Login</button>
					</div>
				</form>
			</div>
		</div>
		</div>
	</div>
</div>
<!-- end guest login -->
<div class="modal fade" id="registerModal">
	<div class="modal-dialog form-dark" role="document">
		<!--Content-->
		<div class="modal-content card card-image" style="background-image: url('{{asset("assets/images/modal-img.jpg")}}'); background-repeat: no-repeat; background-size: cover">
			<div class="text-white rgba-stylish-strong py-5 px-5 z-depth-4">
				<!--Header-->
				<div class="modal-header text-center pb-4">
					<h3 class="modal-title w-100 white-text font-weight-bold" id="myModalLabel"><strong>REGISTER</strong> 
						<a
								class="green-text font-weight-bold"></a></h3>
					<span class="close1 close" data-dismiss="modal">&times</span>
				</div>
				<!--Body-->
				<div class="modal-body">

					<form id="registerForm" action="#" method="post">
						{{ csrf_field() }}
						<h5 id="successMessage" class="alert alert-success" style="display: none"></h5>
						<h5 id="failMessage" class="alert alert-danger" style="display: none"></h5>
						<div class="row md-form">
							<div class="col-md-12">
								<div class="form-group">

									<input type="text" name="fullname" placeholder="Full Name" class="form-control t-email1">
									<span id="firstname_err" class="text-danger err-msg" style="display:none"></span>
								</div>
							</div>
							<!-- <div class="col-md-6">
								<div class="form-group">

									<input type="text" name="lastname" placeholder="Last Name" class="form-control t-email1">
									<span id="lastname_err" class="text-danger err-msg" style="display:none"></span>
								</div>
							</div> -->
						</div>
						<div class="row md-form">
							<div class="col-md-6">

								<div class="form-group">

									<input type="text" name="email_address" placeholder="Email Address"  class="form-control t-email1">
									<span id="email_address_err" class="text-danger err-msg" style="display:none"></span>
									<span id="incorrect_email_address_err" class="text-danger err-msg" style="display:none"></span>
								</div>

							</div>
							<div class="col-md-6">
								<div class="form-group">

									<input type="text" name="phone"  placeholder="Mobile Number" class="form-control t-email1">
									<span id="phone_err" class="text-danger err-msg" style="display:none"></span>
								</div>

							</div>
						</div>


						<div class="row md-form">
							<div class="col-md-6">

								<div class="form-group">

									<input type="password" name="pass" placeholder="Password" class="form-control t-email1">
									<span id="pass_err" class="text-danger err-msg" style="display:none"></span>
								</div>

							</div>
							<div class="col-md-6">
								<div class="form-group">

									<input type="password" name="conf_pass" placeholder="Confirm Password" class="form-control t-email1">
									<span id="conf_pass_err" class="text-danger err-msg" style="display:none"></span>
									<span id="conf_pass_match_err" class="text-danger err-msg" style="display:none"></span>
								</div>

							</div>
						</div>
						<!-- <div class="form-group">

							<textarea name="address" class="form-control t-email1" placeholder="Address"></textarea>
							<span id="address_err" class="text-danger err-msg" style="display:none"></span>
						</div> -->




						<div class="form-group">
							<button type="submit" name="submit" class="form-control t-btn"> REGSITER</button>
						</div>




					</form>


				</div>
			</div>
		</div>
		<!--/.Content-->
	</div>
</div>
	<div class="modal fade text-center py-5" style="top:30px;background-color: rgba(0,0,0,0); " id="myModal_chargesnew">
            <div class="modal-dialog modal-md" role="document">

                <div class="modal-content modal-bg" style="margin-top: 12%; width: 350px">
					<img src="{{asset('assets/images/green-tic-icon.png')}}" class="rp-done">
					<div class="over-modal">


					<div class="modal-body">

						<span class="close close-btn1" data-dismiss="modal">&times;</span>
						

						<img src="{{asset('assets/images/icon-recover-password-512.png')}}" class="recover-pass-img">
						
						<h3 class="pt-3 mb-0 h-cancel success_modal alert alert-success" style="font-size: 16px;    margin: 15px 0 20px !important;"></h3>
						<!-- <p class="t-id-info">Your Change Request Id:1234</p> -->
						<button class="btn2 text-white mb-5 success_ok"
								style="margin-top:45px; margin-bottom: 0px !important">Submit
						</button>
						<!--   <a role="button" class="btn1 text-white mb-5 success_ok" href="https://www.sunlimetech.com" target="_blank">Submit</a> -->
					</div>
					<div class="modal-footer f-bg mt-3" style="background: rgba(49, 18, 75, 0.8);"
					>
						<span class="d-block mr-auto f-help " style="color: white;"><i class="fa fa-phone"></i> Helpline:<a href="#" style="color:#FF9800"> +61 2 5924 0804</a></span>
						<span class="f-help"><img src="{{asset('assets/images/logo.png')}}" class="f-logo"></span>
					</div>
                </div>
				</div>

            </div>
        </div>
<style>
	.rp-done {
		height: 80px;
		position: absolute;
		top: -13px;
		left: -16px;
		z-index: 99;
	}
	.over-modal {
		background: #00000061;
		overflow: hidden;
		border-bottom-left-radius: 5px;
		border-bottom-right-radius: 5px;
	}
	img.recover-pass-img {
		height: 125px;
		display: block;
		margin: 0 auto 0 32%;
	}

	.modal-bg {
		border-radius: 7px;
		margin: 18% auto;
		background-image: url("{{asset('assets/images/pr-img.png')}}") !important;
		background-repeat: no-repeat;
		background-size: cover
	}
	.btn2 {
		font-size: 17px;
		font-weight: 500;
		color: #fff;
		border: 1px solid black;
		text-transform: uppercase;
		background: rgba(0, 0, 0, 0.8);
		outline: none;
		padding: 8px 17px;
		border-radius: 5px;
		display: block;
		margin: auto;
		width: 50%;
		cursor: pointer;
	}

	.btn2:hover {

		color: white !important;
		    font-weight: 600 !important;
		background: transparent;
		border: 1px solid black;

	}
	img.f-logo {
		height: 22px;
	}
	input.t-email:-webkit-autofill,
	input.t-email:-webkit-autofill:hover,
	input.t-email:-webkit-autofill:focus,
	input.t-email:-webkit-autofill:active {
		transition: background-color 5000s ease-in-out 0s;

		-webkit-text-fill-color:#fff;

	}
	textarea.t-email1:-webkit-autofill,
	textarea.t-email1:-webkit-autofill:hover,
	textarea.t-email1:-webkit-autofill:focus,
	textarea.t-email1:-webkit-autofill:active ,
    textarea.t-email1:-webkit-autofill:placeholder-shown,
    textarea.t-email1:-webkit-autofill::placeholder{
		transition: background-color 5000s ease-in-out 0s !important;

		-webkit-text-fill-color:#fff !important;

	}
	span#login_email_err,span#login_password_err {
		width: 100%;
		display: block;
		text-align: center !important;
	}
	span.err-msg{
		width: 100%;
		display: block;
		text-align: center !important;
	}
	.rgba-stylish-strong {
		background-color: rgba(62,69,81,.7);
	}
	input.t-email:focus,input.t-email1:focus{
		box-shadow: none;
	}

	button.t-btn {
		padding: 8px 20px;
		 width: 70%;
		margin-left: auto;
		color: white;
		margin-right: auto;
		cursor: pointer;
		/* border-radius: 20px; */
		background-color: #fa9e1b;
		transition: all .5s ease;
	}
	.modal-login .form-control {
		padding-left: 40px;
	}
	button.t-btn:hover {

		width: 70%;

	}
	input.t-email::placeholder,input.t-email1::placeholder,textarea.t-email1::placeholder{
		color: white!important;
	}



	input.t-email,textarea.t-email{
		border: none;
		border-bottom: 1px solid #ffffff;
        width: 200px;
		color: white!important;
		text-align: center;
		margin-left: auto;
		margin-right: auto;
		border-radius: 0;
		background-color: transparent;
		outline: none !important;
		transition: all .5s ease;
	}
	.container.login-or {
		margin-top: 30px;
		/* margin-bottom: 47px; */
	}
	input.t-email1,textarea.t-email1{
		border: none;
		color: white!important;
		border-bottom: 1px solid #ffffff;
		width: 100%;
		text-align: center;
		margin-left: auto;
		margin-right: auto;
		border-radius: 0;
		background-color: transparent;
		outline: none !important;
		transition: all .5s ease;
	}
	input.t-email:focus,input.t-email1:focus,textarea.t-email1:focus{
		border-bottom: 1px solid #fff;
		outline: none!important;
		background-color: transparent;
		width: 100%;
	}
	textarea.t-email1{
		padding-bottom: 5px;
	}
	.modal-header {
		display: -ms-flexbox;
		display: flex;
		-ms-flex-align: start;
		align-items: flex-start;
		-ms-flex-pack: justify;
		justify-content: space-between;
		padding: 15px;
		border-bottom: 1px solid #fa9e1b;
		border-top-left-radius: .3rem;
		border-top-right-radius: .3rem;
	}
.modal-header h3{
	color: white;
}
	.modal-login i {
		position: absolute;
		left: 13px;
		top: 11px;
		font-size: 18px;
	}
.modal{
	background-color: rgba(0,0,0,0.5);
}
	.modal-header .close1 {
		color: #fff;
		border: none;
		cursor: pointer;
		outline: none;
		font-size: 30px;
		position: absolute;
		top: -30px;
		right: -19px;
		font-weight: bolder;
	}
	.modal-header .close2 {
		color: white;
		border: none;
		cursor: pointer;
		outline: none;
		font-size: 30px;
		position: absolute;
		top: -11px;
		right: 15px;
		font-weight: bolder;
	}
    .form-group {
        padding-bottom: 10px;
    }
	#loginForm,#registerForm{
		color: white;
		font-size: 17px;
	}
	p.forgot-p {
		color: white;
		font-size: 17px;
		margin-top: -10px;
		margin-bottom: 0;
		text-align: right;
	}
	p.forgot-p>a {
		color: #fa9e1b;
		border: none!important;
		text-decoration: underline;
		transition: all .5s ease;
	}
	p.forgot-p a:hover {
		color: #fa9e1b ;
		background: none;
		border: none!important;
	}


    .modal-content{
		margin-top: 20%;
		box-shadow: 0 4px 8px 0 rgb(0, 0, 0);
	}
	.fb {
		background-color: #3B5998;
		color: white;
		display: block;
		position: relative;
		width: 100%;
		text-align: left;
		transition: all .5s ease;
		padding-left: 17px;

	}
	.google {
		background: #dd4b39;
		color: white;
		display: block;
		position: relative;
		width: 100%;
		transition: all .5s ease;
		text-align: left;
		padding-left: 19px;

	}
	.s-icon:hover .fb,.s-icon:hover .google{
		padding-left: 75px;
	}
	.s-icon:hover div.fb-icon-l{
		right: 93px;
		transform: translateY(-50%) rotate(360deg);

	}
	@media screen and (max-width: 576px){

		.modal-header h3 {
			color: white;
			font-size: 18px;
			margin: 0 !important;
		}
		.modal-header .close {
			padding: 15px;
			margin: -20px -15px -15px auto;
		}
		.modal-login .form-control {
		padding-left: 10px; !important;
		}
		.s-icon:hover div.fb-icon-l{
			right: 78%;
		}
		.s-icon:hover .fb,.s-icon:hover .google{
			padding-left: 80px;
		}
		p.forgot-p {
			color: white;
			font-size: 16px;
			margin-top: 25px;
			margin-bottom: 0;
			text-align: right;
		}
		.modal-content {
			padding: 0;
			margin: 0;
		}

	}
	.s-icon a:hover{
		color: white;
		text-decoration: none;
	}
	.s-icon a span{
		transition: all .5s ease;
	}

	i.fa.fa-facebook.fa-fw, i.fa.fa-google.fa-fw {
		font-size: 25px;
		vertical-align: center;
		margin-top: 20px;
		margin-bottom: auto;
	}
	.vl {
		position: absolute;
		left: 50%;
		transform: translate(-50%);
		border: 0.5px solid #f8f9fa;
		width: 100%;
	}
	div.fb-icon-l {
		position: absolute;
		width: 65px;
		height: 65px;
		border-radius: 34px;
		transition: all .4s ease;
		text-align: center;
		background-color: #3B5998;
		color:  white;
		/* border: 1px solid; */
		top: 50%;
		transform: translateY(-50%) rotate(0deg);
		right: 10px;
	}
	.google div.fb-icon-l {
		position: absolute;
		width: 65px;
		height: 65px;
        transition: all .4s ease;
		border-radius: 34px;
		text-align: center;
		color:  white;
		background-color: #dd4b39;
		top: 50%;
		transform: translateY(-50%);
		right: 10px;
	}

	.vl-innertext {
		position: absolute;
		top: 50%;
		left: 50%;
		width: 40px;
		transform: translate(-50%, -50%);
		background-color: #f8f9fa;
		border: 1px solid #fa9e1b;
		border-radius: 50%;
		text-align: center;
		color: #fa9e1b;
		font-weight: 600;
		padding: 5px 5px 5px 5px;
		height: 40px;
	}



    span.close.close-btn {
        color: white;
        font-size: 26px;
        font-weight: 600;
        cursor: pointer;
    }

	span.close.close-btn1 {
		color: #000000;
		font-size: 26px;
		font-weight: 600;
		cursor: pointer;
		text-shadow: 0 1px 0 #000000;
	}


	@media screen and (max-width: 650px) {
		.text-white.rgba-stylish-strong.py-4.px-5.z-depth-4{
			padding-left: 0 !important;
			padding-right: 0 !important;
		}
		  .modal-bg{
			  width: auto !important;
		  }
		

	}
</style>
<!-- Price Datepicker -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script> -->
<!-- Price Datepicker -->
<script src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/styles/bootstrap4/popper.js') }}"></script>

<script src="{{ asset('assets/styles/bootstrap4/bootstrap.min.js') }}"></script>

<script src="{{ asset('assets/scripts/greensock/TweenMax.min.js') }}"></script>
<script src="{{ asset('assets/scripts/scrollmagic/ScrollMagic.min.js') }}"></script>
<script src="{{ asset('assets/scripts/greensock/animation.gsap.min.js') }}"></script>
<script src="{{ asset('assets/scripts/greensock/ScrollToPlugin.min.js') }}"></script>

<script src="{{ asset('assets/scripts/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
<script src="{{ asset('assets/scripts/easing/easing.js') }}"></script>
<script src="{{ asset('assets/scripts/parallax-js-master/parallax.min.js') }}"></script>
<script src="{{ asset('assets/js/custom.js') }}"></script>
<script src="{{ asset('assets/js/about_custom.js') }}"></script>
<script src="{{ asset('assets/ajax.cloudflare.com/cdn-cgi/scripts/a2bd7673/cloudflare-static/rocket-loader.min.js') }}" data-cf-settings="27af61e031c2aad01bb9c3d0-|49" defer=""></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>


<script src='https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.16.1/lodash.min.js'></script>
<script src='https://unpkg.com/fuse.js@2.5.0/src/fuse.min.js'></script>

<script src="{{ asset('assets/js/airportlist.js') }}"></script>
<script src="{{ asset('assets/js/airport.js') }}"></script>
<script src="{{ asset('assets/js/dojo.js') }}"></script>
<script src="{{ asset('assets/js/jquery.simple.timer.js') }}"></script>
<script src="{{ asset('assets/sweetalert/sweetalert.min.js') }}"></script>
      <script src="{{ asset('assets/sweetalert/jquery.sweet-alert.custom.js') }}"></script>
<script>

$(document).ready(function() {
    
    $(".loader").fadeOut("slow");
});
</script>
<script>
	$(".user_box_login").click(function()
	{
		$("#loginModal").modal('show');
	})
	$(".user_box_register").click(function()
	{
		$("#registerModal").modal('show');
	});
	$(".guest_box_login").click(function()
	{
		$("#guestModal").modal('show');
	})
</script>
<script>
	$("#registerForm").submit(function(e)
	{
		e.preventDefault();
		var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
		var error=0;
		if($("input[name='fullname']").val().trim()=='')
		{
			error++;
			$("#firstname_err").text('Enter First Name').show();
			$("input[name='fullname']").css("border-color","red");
		}
		else
		{
			$("#firstname_err").hide();
			$("input[name='fullname']").removeAttr("style");
		}

		

		if($("input[name='email_address']").val().trim()=='')
		{
			error++;
			$("#email_address_err").text('Enter Email Address').show();
			$("input[name='email_address']").css("border-color","red");
		}
		else
		{
			$("#email_address_err").hide();
			$("input[name='email_address']").removeAttr("style");
		}
		if($("input[name='email_address']").val().trim()!='' && !pattern.test($("input[name='email_address']").val().trim()))
		{
			error++;
			$("#incorrect_email_address_err").text('Enter Correct Email Address').show();
			$("input[name='email_address']").css("border-color","red");
		}
		else
		{
			$("#incorrect_email_address_err").hide();
		}

		if($("input[name='pass']").val().trim()=='')
		{
			error++;
			$("#pass_err").text('Enter Password').show();
			$("input[name='pass']").css("border-color","red");
		}
		else
		{
			$("#pass_err").hide();
			$("input[name='pass']").removeAttr("style");
		}

		if($("input[name='conf_pass']").val().trim()=='')
		{
			error++;
			$("#conf_pass_err").text('Enter Confirm Password').show();
			$("input[name='conf_pass']").css("border-color","red");
		}
		else
		{
			$("#conf_pass_err").hide();
			$("input[name='conf_pass']").removeAttr("style");
		}

		if($("input[name='pass']").val().trim()!='' && $("input[name='conf_pass']").val().trim()!='' && $("input[name='pass']").val().trim()!=$("input[name='conf_pass']").val().trim())
		{	
			error++;
			$("#conf_pass_match_err").text('Password and Confirm Password should be same').show();
			$("input[name='conf_pass']").css("border-color","red");
		}
		else
		{
			$("#conf_pass_match_err").hide();
		}

		if($("input[name='phone']").val().trim()=='')
		{
			error++;
			$("#phone_err").text('Enter Mobile Number').show();
			$("input[name='phone']").css("border-color","red");
		}
		else
		{
			$("#phone_err").hide();
			$("input[name='phone']").removeAttr("style");
		}

		// if($("textarea[name='address']").val().trim()=='')
		// {
		// 	error++;
		// 	$("#address_err").text('Enter Address').show();
		// 	$("textarea[name='address']").css("border-color","red");
		// }
		// else
		// {
		// 	$("#address_err").hide();
		// 	$("textarea[name='address']").removeAttr("style");
		// }

		if(error==0)
		{
			var formdata=new FormData($("#registerForm")[0]);
			$.ajax(
			{
				url:"{{ route('register') }}",
				type:"POST",
				data: formdata,
				processData: false,
				contentType: false,
				success:function(result)
				{
					if(result.indexOf("Done")!=-1)
					{
						$("#failMessage").hide();
						$("#successMessage").text('You are registered Successfully').show();
						var data=result.split('-');
						$(".user_box").empty();
						

						setTimeout(function(){
							$(".user_box").html('<div class="user_box_link"><a href="javascript:void(0)">Welcome, '+data[1]+'</a></div> | <div class="user_box_link"><a href="{{ route('logout') }}">Logout</a></div>');
							$('#registerModal').modal('hide')},500);
					}
					else if(result=='exist')
					{
						$("#successMessage").hide();
						$("#failMessage").text("User Already Register").show();   
					}
					else
					{
						$("#successMessage").hide();
						$("#failMessage").text("Unable to handle this request").show();   
					}
				}
			})

		}
	});
</script>


<script>
	$("#loginForm").submit(function(e)
	{
		e.preventDefault();
		var error=0;
		if($("input[name='login_email']").val().trim()=='')
		{
			error++;
			$("#login_email_err").text('Enter Email Address').show();
		}
		else
			$("#login_email_err").hide();

		if($("input[name='login_password']").val().trim()=='')
		{
			error++;
			$("#login_password_err").text('Enter Password').show();
		}
		else
			$("#login_password_err").hide();



		if(error==0)
		{
			var formdata=new FormData($("#loginForm")[0]);
			$.ajax(
			{
				url:"{{ route('checkuser') }}",
				type:"POST",
				data: formdata,
				processData: false,
				contentType: false,
				success:function(result)
				{
					if(result.indexOf("Done")!=-1)
					{
						$("#failMessage1").hide();
						$("#successMessage1").text('Login Success').show();
						var data=result.split('-');
						$(".user_box").empty();
						 
						

						setTimeout(function(){
							$(".user_box").html('<div class="user_box_link dropdown-account"><a href="javascript:void(0)">Welcome, '+data[1]+'</a><div class="dropdown-content-account"><a href="{{ route("profile") }}">Profile</a><a href="{{route("mytrip")}}">My Trips</a></div></div> | <div class="user_box_link"><a href="{{ route("logout") }}">Logout</a></div>');
							$('#loginModal').modal('hide')},500);

 							$("#form_submit_button").remove();
 							$("#form_submit_button2").remove();
						 $("#payment_btn").html('<button type="button" id="form_submit_button" class="form_submit_button button trans_200 makepyament">Make Payment<span></span><span></span><span></span></button>');

					}
					else
					{
						$("#successMessage1").hide();
						$("#failMessage1").text("Incorrect Username and/or Password").show();   
					}
				}
			})

		}
	});
</script>

<!-- guest login -->
<script>
	$("#guestForm").submit(function(e)
	{
		e.preventDefault();
		var error=0;
		var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if($("input[name='guest_email']").val().trim()=='')
		{
			error++;
			$("#guest_email_err").text('Enter Email Address').show();
		}
		else
			$("#guest_email_err").hide();
		if($("input[name='guest_email']").val().trim()!='' && !emailfilter.test($("input[name='guest_email']").val()) )
		{
			error++;
			$("#guest_email_err").text('Enter Email Not Valid').show();
		}
		else
			$("#guest_email_err").hide();
		if($("input[name='guest_phone']").val().trim()=='')
		{
			error++;
			$("#guest_password_err").text('Enter Password').show();
		}
		else
			$("#guest_password_err").hide();



		if(error==0)
		{
			var formdata=new FormData($("#guestForm")[0]);
			$.ajax(
			{
				url:"{{ route('guestuser') }}",
				type:"POST",
				data: formdata,
				processData: false,
				contentType: false,
				success:function(result)
				{
					if(result.indexOf("Done")!=-1)
					{
						$("#failMessage2").hide();
						$("#successMessage2").text('Login Success').show();
						var data=result.split('-');
						$(".user_box").empty();
						 
						

						setTimeout(function(){
							$(".user_box").html('<div class="user_box_link"><a href="javascript:void(0)">Welcome, '+data[1]+'</a></div> | <div class="user_box_link"><a href="{{ route("logout") }}">Logout</a></div>');
							$('#guestModal').modal('hide')},500);

 							$("#form_submit_button").remove();
 							$("#form_submit_button2").remove();
						 $("#payment_btn").html('<button type="button" id="form_submit_button" class="form_submit_button button trans_200 makepyament">Make Payment<span></span><span></span><span></span></button>');

					}
					else if(result=='exist')
					{
						$("#successMessage2").hide();
						$("#failMessage2").text("User Already Register").show();   
					}
					else
					{
						$("#successMessage2").hide();
						$("#failMessage2").text("Incorrect Username and/or Password").show();   
					}
				}
			})

		}
	});
</script>
<script>
	$("#forgotForm").submit(function(e)
	{
		e.preventDefault();
	
       $('.eerror').hide();
       $('.eerror').text("");
       var email=$('#forgotemail').val();
       
       var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
       if(email=='')
       {
       		$('.eerror').text("Please Enter Email Id");
       		$('.eerror').show();
       }
       else if(email!='' && !emailfilter.test(email))
       {
       		$('.eerror').text("Please Enter Valid Email Id");
       		$('.eerror').show();
       }
       else
       {
       	var formdata=new FormData($("#forgotForm")[0]);
       	$('.eerror').hide();
       	$('.eerror').text("");

       	 $.ajax({
	       	 		url: '{{route("adminforgotpassword_check")}}',
		            type: 'POST',
		            data: formdata,
					processData: false,
					contentType: false,
		            success: function (result) 
		            {
		            	if(result=='success')
		            	{
		            		$('#successMessage1').text('Reset Password Sent to this email.');
	                        $('#successMessage1').show();
	                        $('#email').val('');
	                        $('#email').focus();
	                        $('#forget').modal('hide');
	                        $('#loginModal').modal('hide');
	                        $('#myModal_chargesnew').modal('show');
                            $('.success_modal').text("Reset Password Sent to this email ");
		            	}
		            	else if(result=='fail')
		            	{
		            		$('#failMessage1').text('Unable to send link for password reset');
	                        $('#failMessage1').show();
	                        $('#email').val('');
	                        $('#email').focus();
		            	}
		            	else
		            	{
		            		$('#failMessage1').text('Invalid Email');
                        	$('#failMessage1').show();
		            	}
		            }

       	 		});
       }
    });
</script>

<script type="text/javascript">
$(document).on('click', ".success_ok", function ()
{
    $('#myModal_chargesnew').modal('hide');
    location.reload();
});
</script>