<div class="search">
	<!-- Search Contents -->
	<div class="container fill_height">
		<div class="row fill_height">
			<div class="col fill_height">
				<!-- Search Tabs -->
				<div class="search_tabs_container">
					<div class="search_tabs d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start">
						<div class="search_tab active d-flex flex-row align-items-center justify-content-lg-center justify-content-start"><img src=" {{ asset('assets/images/departure.png') }}" alt="">flights</div>
						<div class="search_tab d-flex flex-row align-items-center justify-content-lg-center justify-content-start"><img src="{{ asset('assets/images/suitcase.png') }}" alt=""><span>hotels</span></div>
					</div>		
				</div>
				<!-- Search Panel -->
				<div class="search_panel active">
					<form action="{{route('flightsearch')}}" method="post" id="search_form_1" class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start flights">
						<div class="extras">
							<ul class="search_extras clearfix">
								<li class="search_extras_item">
									<div class="clearfix">
										<input type="radio" id="search_extras_1" name="one" value="1" class="search_extras_cb" checked >
										<label for="search_extras_1">One Way</label>
									</div>	
								</li>
								<li class="search_extras_item">
									<div class="clearfix">
										<input type="radio" id="search_extras_2" name="one" value="2" class="search_extras_cb" >
										<label for="search_extras_2">Round Trip</label>
									</div>
								</li>
							</ul>
						</div>
						<div class="search_item">
							{{ csrf_field() }}
							<div>From</div>
							<input type="text" class="search_input autocomplete" name="flight_from" required="required" id="autocomplete" autocomplete="off">
							<i class="fa fa-map-marker fa-icon"></i>
						</div>
						<a href="javascript:void()" class="exchange-icon">
							<i class="fa fa-exchange"></i>
						</a>
						<div class="search_item">
							<div>To</div>
							<input type="text" class="search_input autocomplete" name="flight_to" required="required" id="autocomplete1" autocomplete="off">
							<i class="fa fa-map-marker fa-icon"></i>
							
						</div>
						<div class="search_item">
							<div>Departures</div>
							<input type="text" class="search_input"  name="flight_dep" placeholder="YYYY-MM-DD" id="departure" autocomplete="off">
							<i class="fa fa-calendar fa-icon"></i>
						</div>
						<div class="search_item" id="restuntype" style="display: none">
							<div>Return Date</div>
							<input type="text" class="search_input" name="flight_return" placeholder="YYYY-MM-DD" id="return" autocomplete="off" disabled>
							<i class="fa fa-calendar fa-icon"></i>
						</div>
						<div class="search_item">
							<div>Adults</div>
							<select name="adults" id="adults_1" class="dropdown_item_select search_input">
								<option value="1">01</option>
								<option value="2">02</option>
								<option value="3">03</option>
								<option value="4">04</option>
								<option value="5">05</option>
								<option value="6">06</option>
								<option value="7">07</option>
								<option value="8">08</option>
								<option value="9">09</option>
							</select>
							<i class="fa fa-user fa-icon"></i>
						</div>
						<div class="search_item">
							<div>children</div>
							<select name="children" id="children_1" class="dropdown_item_select search_input children-icon">
								<option value="0">0</option>
								<option value="1">01</option>
								<option value="2">02</option>
								<option value="3">03</option>
							</select>
							<i class="fa fa-child fa-icon"></i>
						</div>
						<div class="search_item">
							<div>Class</div>
							<select class="dropdown_item_select search_input class-icon" name="flight_class">
								<option value="1%All">All</option>
								<option value="2%Economy">Economy</option>
								<option value="3%Premium Economy">Premium Economy</option>
								<option value="4%Business">Business</option>
								<option value="5%Premium Business">Premium Business</option>
								<option value="6%First">First</option>
							</select>
							<i class="fa fa-plane fa-icon"></i>
						</div>
						<div class="text-center">
							<button class="button search_button">search<span></span><span></span><span></span></button>
						</div>
					</form>
				</div>

				<!-- Search Panel -->

				<div class="search_panel">
					<form action="#" id="search_form_2" class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start hotels">
						<div class="search_item">
							<div>City / Hotel</div>
							<input type="text" class="destination search_input autocomplete" required="required" id="autocomplete2">
							<i class="fa fa-map-marker fa-icon"></i>
						</div>
						<div class="search_item">
							<div>check in</div>
							<input type="text" class="check_in search_input" placeholder="YYYY-MM-DD" id="check-in">
							<i class="fa fa-calendar fa-icon"></i>
						</div>
						<div class="search_item">
							<div>check out</div>
							<input type="text" class="check_out search_input" placeholder="YYYY-MM-DD" id="check-out">
							<i class="fa fa-calendar fa-icon"></i>
						</div>
						<div class="search_item">
							<div>Rooms</div>
							<select name="adults" id="adults_2" class="dropdown_item_select search_input">
								<option>01</option>
								<option>02</option>
								<option>03</option>
							</select>
							<i class="fa fa-building fa-icon"></i>
						</div>
						<div class="search_item">
							<div>adults</div>
							<select name="adults" id="adults_2" class="dropdown_item_select search_input">
								<option>01</option>
								<option>02</option>
								<option>03</option>
							</select>
							<i class="fa fa-user fa-icon"></i>
						</div>
						<div class="search_item">
							<div>children</div>
							<select name="children" id="children_2" class="dropdown_item_select search_input">
								<option>0</option>
								<option>02</option>
								<option>03</option>
							</select>
							<i class="fa fa-child fa-icon"></i>
						</div>
						<div class="text-center">
							<button class="button search_button">search<span></span><span></span><span></span></button>
						</div>
					</form>
				</div>
				<!-- Search Panel -->
			</div>
		</div>
	</div>		
</div>
