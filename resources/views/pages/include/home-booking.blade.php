<!doctype html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>TravoWeb</title>
	<style>
		.dropbtn {
			background-color: #3498DB;
			color: white;
			padding: 16px;
			font-size: 16px;
			border: none;
			cursor: pointer;
		}
		.search {
    width: 100%;
    height: auto;
    /* background: linear-gradient(to right, #fa9e1b, #8d4fff); */
    z-index: unset;
	position: absolute;
	top:10%; 
   
}
		.dropbtn:hover,
		.dropbtn:focus {
			background-color: #2980B9;
		}

		.dropdown {
			position: relative;
			display: inline-block;
		}

		.dropdown-content {
			display: none;
			position: absolute;
			background-color: #f1f1f1;
			min-width: 160px;
			overflow: auto;
			box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
			z-index: 1;
		}

		.dropdown-content a {
			color: black;
			padding: 12px 16px;
			text-decoration: none;
			display: block;
		}

		.exchange-icon {
			font-size: 17px;
			color: #fa9e1b;
			padding-top: 0px !important;
			margin-top: -13px !important;
		}

		a.a-done-btn {
			text-decoration: none;
			padding: 10px;
			background: #fa9e1b;
			color: white;
			margin: 20px auto auto auto;
			width: 120px;
			display: block;
			text-align: center;
			border-radius: 20px;
		}

		.a-done-btn {
			text-decoration: none;
			padding: 10px;
			background: #fa9e1b;
			color: white;
			margin: 20px auto auto auto;
			width: 120px;
			display: block;
			text-align: center;
			border-radius: 20px;
			cursor: pointer;
		}

		.dropdown a:hover {
			background-color: #ddd;
		}

		.show {
			display: block;
		}

		.container3 {
			display: block;
			position: relative;
			padding-left: 35px;
			margin-bottom: 12px;
			cursor: pointer;
			font-size: 22px;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}

		/* Hide the browser's default radio button */
		.container3 .cstm-rdio {
			position: absolute;
			opacity: 0;
			cursor: pointer;
		}

		/* Create a custom radio button */
		.checkmark {
			position: absolute;
			top: 4px;
			left: 0;
			height: 16px;
			width: 16px;
			background-color: #eee;
			border-radius: 50%;
		}

		/* On mouse-over, add a grey background color */
		.container3:hover .cstm-rdio~.checkmark {
			background-color: #ccc;
		}

		/* When the radio button is checked, add a blue background */
		.container3 .cstm-rdio:checked~.checkmark {
			background-color: #fff;
		}

		/* Create the indicator (the dot/circle - hidden when not checked) */
		.checkmark:after {
			content: "";
			position: absolute;
			display: none;
		}

		/* Show the indicator (dot/circle) when checked */
		.container3 .cstm-rdio:checked~.checkmark:after {
			display: block;
		}

		/* Style the indicator (dot/circle) */
		.container3 .checkmark:after {
			top: 3px;
			left: 3px;
			width: 8px;
			height: 8px;
			border-radius: 50%;
			background: #fa9e1b;
		}
		div#testiviewdata {
    color: #000;
    background: white;
    min-height: 200px;
    padding: 50px 20px 20px !important;
}
h5#exampleModalLabel {
    position: absolute;
    top: 43px;
    color: #FF5722;
    z-index: 99;
    font-size: 18px;
    left: 20px;
}
div#testmodal .modal-header .close{
	background: #000000;
    opacity: 1;
    padding: 3px 8px;
    color: #ffffff;
    font-size: 23px;
    border-radius: 50px;
    top: 13px;
    z-index: 999;
    position: relative;
    left: 13px;
}
div#testmodal .modal-header{
	background: none;
    border: none;
}
div#testmodal .modal-content {
    background: transparent;
    box-shadow: none;
    border: none;
}
	</style>

	<style>
		.mbsc-mobiscroll .mbsc-stepper-cont {
			padding: 1.75em 12.875em 1.75em 1em;
		}

		.mbsc-control-w {
			max-width: none;
			margin: 0;
			font-size: 1em;
			font-weight: normal;
		}

		.mbsc-checkbox,
		.mbsc-switch,
		.mbsc-btn,
		.mbsc-radio,
		.mbsc-segmented,
		.mbsc-stepper-cont {
			position: relative;
			display: block;
			margin: 0;
			z-index: 0;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}

		.mbsc-desc {
			display: block;
			font-size: .75em;
			opacity: .6;
		}

		.mbsc-stepper {
			position: absolute;
			display: block;
			width: auto;
			right: 1em;
			top: 50%;
		}

		.mbsc-form .mbsc-stepper-val-left .mbsc-stepper input {
			left: 0;
		}

		.mbsc-mobiscroll .mbsc-stepper input {
			color: #454545;
		}

		.mbsc-stepper input {
			position: absolute;
			left: 4.142857em;
			width: 4.142857em;
			height: 100%;
			padding: 0;
			margin: 0;
			border: 0;
			outline: 0;
			box-shadow: none;
			font-size: .875em;
			text-align: center;
			opacity: 1;
			z-index: 4;
			background: transparent;
			-webkit-appearance: none;
			-moz-appearance: textfield;
			appearance: none;
		}

		.mbsc-stepper .mbsc-segmented-item {
			width: 3.625em;
		}

		.mbsc-segmented .mbsc-segmented-item {
			margin: 0;
			display: table-cell;
			position: relative;
			vertical-align: top;
			text-align: center;
			font-size: 1em;
		}

		.mbsc-stepper-cont.mbsc-stepper-val-left .mbsc-stepper .mbsc-segmented-item:nth-child(2) .mbsc-segmented-content,
		.mbsc-stepper-cont.mbsc-stepper-val-right .mbsc-stepper .mbsc-segmented-item:last-child .mbsc-segmented-content {
			border: 0;
			background: transparent;
		}

		.mbsc-mobiscroll .mbsc-segmented-content {
			border: 0.142857em solid #4eccc4;
			color: #4eccc4;
			height: 2.28571428em;
			margin: 0 -.071428em;
			line-height: 2.28575em;
			padding: 0 .285714em;
			text-transform: uppercase;
		}

		.mbsc-segmented-content {
			position: relative;
			display: block;
			white-space: nowrap;
			text-overflow: ellipsis;
			overflow: hidden;
			font-size: .875em;
			font-weight: normal;
			z-index: 2;
		}

		.mbsc-segmented .mbsc-segmented-item {
			margin: 0;
			display: table-cell;
			position: relative;
			vertical-align: top;
			text-align: center;
			font-size: 1em;
		}

		.mbsc-mobiscroll .mbsc-segmented input:disabled~.mbsc-segmented-item .mbsc-segmented-content,
		.mbsc-mobiscroll .mbsc-segmented .mbsc-segmented-item.mbsc-stepper-control.mbsc-disabled .mbsc-segmented-content,
		.mbsc-mobiscroll .mbsc-segmented .mbsc-segmented-item input:disabled+.mbsc-segmented-content {
			color: #d6d6d6;
			border-color: #d6d6d6;
		}

		.mbsc-mobiscroll .mbsc-segmented input:disabled~.mbsc-segmented-item .mbsc-segmented-content,
		.mbsc-mobiscroll .mbsc-segmented .mbsc-segmented-item.mbsc-stepper-control.mbsc-disabled .mbsc-segmented-content,
		.mbsc-mobiscroll .mbsc-segmented .mbsc-segmented-item input:disabled+.mbsc-segmented-content {
			background: transparent;
		}

		.mbsc-mobiscroll .mbsc-segmented-content {
			border: 0.142857em solid #4eccc4;
			color: #4eccc4;
			height: 2.28571428em;
			margin: 0 -.071428em;
			line-height: 2.28575em;
			padding: 0 .285714em;
			text-transform: uppercase;
		}

		.mbsc-mobiscroll .mbsc-segmented-content {
			border: 0.142857em solid #4eccc4;
			color: #4eccc4;
		}

		.mbsc-mobiscroll .mbsc-segmented-content {
			height: 2.28571428em;
			margin: 0 -.071428em;
			line-height: 2.28575em;
			padding: 0 .285714em;
			text-transform: uppercase;
		}

		.mbsc-disabled .mbsc-segmented-content,
		.mbsc-segmented input:disabled,
		.mbsc-segmented input:disabled~.mbsc-segmented-item .mbsc-segmented-content {
			cursor: not-allowed;
		}

		.mbsc-segmented-item .mbsc-control,
		.mbsc-stepper .mbsc-segmented-content {
			cursor: pointer;
		}

		.mbsc-segmented input:disabled~.mbsc-segmented-item .mbsc-segmented-content,
		.mbsc-disabled .mbsc-segmented-content,
		.mbsc-segmented input:disabled+.mbsc-segmented-content {
			z-index: 0;
		}

		.traveller-dropdown .dropdown-menu hr {
			margin-top: 7px !important;
			margin-bottom: 7px !important;
		}

		.traveller-dropdown .dropdown-menu {
			min-width: 300px !important;
			margin-bottom: 0px;
			background: #FFF4e7;
			border: 1px solid #CCC;
			margin-top: 10px;
		}

		.traveller-dropdown .dropdown-menu .row {
			margin-bottom: 0px;
		}

		.traveller-dropdown .dropdown-menu .col-md-12 {
			margin-bottom: 0px;
		}

		.traveller-dropdown .dropdown-menu .trave-drop {
			padding-left: 15px;
			padding-right: 15px;
			margin-bottom: 0px;
		}

		.traveller-dropdown .dropdown-menu .trave-drop .travel-left {
			width: 60%;
			float: left;
			color: #00206A;
			font-weight: 600;
			margin-bottom: 0px;
			font-size: 16px;
			line-height: 36px;
		}

		.traveller-dropdown .dropdown-menu .trave-drop .plus-minus .minus {
			color: #fa9e1b;
			width: 35%;
			float: left;
			font-size: 24px;
			margin-bottom: 0px;
			text-align: center;
		}

		.traveller-dropdown .dropdown-menu .trave-drop .plus-minus .text {
			color: #00206A;
			width: 30%;
			float: left;
			font-size: 17px;
			line-height: 37px;
			font-weight: 600;
			margin-bottom: 0px;
			text-align: center;
		}

		.traveller-dropdown .dropdown-menu .trave-drop .plus-minus .plus {
			color: #fa9e1b;
			width: 35%;
			float: left;
			font-size: 24px;
			margin-bottom: 0px;
			text-align: center;
		}

		.traveller-dropdown .dropdown-menu .trave-drop .child-age label {
			color: #333;
			margin-bottom: 3px;
			font-weight: 600;
		}

		.traveller-dropdown .dropdown-menu .trave-drop .child-age select {
			border: 1px solid #CCC;
			padding-left: 10px;
		}

		.traveller-dropdown .dropdown-menu .trave-drop .child-age:nth-child(1),
		.traveller-dropdown .dropdown-menu .trave-drop .child-age:nth-child(4),
		.traveller-dropdown .dropdown-menu .trave-drop .child-age:nth-child(7),
		.traveller-dropdown .dropdown-menu .trave-drop .child-age:nth-child(10) {
			margin-left: 0px;
		}

		.traveller-dropdown .dropdown-menu .trave-drop .child-age {
			width: 30%;
			float: left;
			margin-top: 10px;
			margin-left: 13px;
		}

		.traveller-dropdown .dropdown-menu .trave-drop .plus-minus {
			width: 40%;
			float: left;
			margin-bottom: 0px;
		}

		.t-err {
			color: red;
			visibility: hidden;
		}
		@media screen and (max-width:600){
			.search {
    padding: 70px 0px 0px !important;
}
.search_tabs div {
    width: 33%;
    display: flex;
    flex-direction: column !important;
    justify-content: center!important;
    padding: 10px !important;
}
.search_tabs {
    display: flex;
    flex-direction: row!important;
}
		}
	</style>
</head>

<body>
	<div class="search">
		<!-- Search Contents -->
		<div class="container fill_height">
			<div class="row fill_height">
				<div class="col fill_height">
					<!-- Search Tabs -->
					<div class="search_tabs_container">
						<div
							class="search_tabs d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start">
							<div id="flight-tab"
								class="search_tab active d-flex flex-row align-items-center justify-content-lg-center justify-content-start">
								<img src=" {{ asset('assets/images/departure.png') }}" alt="">flights</div>
							<div id="hotel-tab"
								class="search_tab d-flex flex-row align-items-center justify-content-lg-center justify-content-start">
								<img src="{{ asset('assets/images/suitcase.png') }}" alt=""><span>hotels</span></div>
							<div 
								class="search_tab d-flex flex-row align-items-center justify-content-lg-center justify-content-start"
								id="pckg"><img src="{{ asset('assets/images/packages-img.png') }}"
									alt=""><span>Packages</span></div>
						</div>
					</div>
					<!-- Search Panel -->
					<div class="search_panel active">
						<form action="{{route('flightsearch')}}" method="post" id="search_form_1"
							class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start flights">
							<div class="extras">
								<ul class="search_extras clearfix">
									<li class="search_extras_item">
										<div class="clearfix">
											<input type="radio" id="search_extras_1" name="one" value="1"
												class="search_extras_cb" checked>
											<label for="search_extras_1">One Way</label>
										</div>
									</li>
									<li class="search_extras_item">
										<div class="clearfix">
											<input type="radio" id="search_extras_2" name="one" value="2"
												class="search_extras_cb">
											<label for="search_extras_2">Round Trip</label>
										</div>
									</li>
									<li class="search_extras_item">
										<div class="clearfix">
											<input type="radio" id="search_extras_3" name="one" value="3"
												class="search_extras_cb">
											<label for="search_extras_3">Multi City</label>
										</div>
									</li>
								</ul>
							</div>
							<div class="search_item">
								{{ csrf_field() }}
								<div>
									<div>Flying From</div>
									<input type="text" class="search_input autocomplete flightfrom flight_from-1"
										name="flight_from[]" required="required" placeholder="City or Airport"
										autocomplete="off" id="autocomplete">

									<i class="fa fa-map-marker fa-icon"></i>
								</div>
								<div>
									<span class="t-err">Enter Your City</span>
								</div>
							</div>
							<a href="javascript:void()" class="exchange-icon exchangeicon" id="1">
								<i class="fa fa-exchange"></i>
							</a>
							<div class="search_item">
								<div>
									<div>Flying To</div>
									<input type="text" class="search_input autocomplete flightto flight_to-1 "
										name="flight_to[]" required="required" placeholder="City or Airport"
										autocomplete="off" id="autocomplete1">
									<i class="fa fa-map-marker fa-icon"></i>
								</div>
								<div>
									<span class="t-err">Enter Your Destination</span>
								</div>
							</div>
							<div class="search_item">
								<div>
									<div>Departures</div>
									<input type="text" class="search_input flight_dep-1 departure" name="flight_dep[]"
										placeholder="DD-MM-YYYY" autocomplete="off" id="departure">
									<i class="fa fa-calendar fa-icon"></i>
								</div>
								<div>
									<span class="t-err">Enter Your Departures</span>
								</div>
							</div>

							<div class="search_item" id="restuntype" style="display: none">
								<div>
									<div>Return Date</div>
									<input type="text" class="search_input returnchk" name="flight_return"
										placeholder="DD-MM-YYYY" autocomplete="off" id="return">
									<i class="fa fa-calendar fa-icon"></i>
								</div>
								<div>
									<span class="t-err">Enter Your Return Date</span>
								</div>
							</div>
							<!-- start code -->
							<div class="search_item">
								<div>
									<div>Travelers / Cabin Class</div>
									<div class=" traveller-dropdown" style="margin-bottom:0px;">
										<div class="searchflight" style="margin-bottom:0px;">
											<input type="text" class="search_input totaltravel " name="flight_nocheck"
												value=" 1 Travelers" autocomplete="off">
											<i class="fa fa-user fa-icon"></i>
										</div>
										<div class="dropdown-menu dropshowflight filter-res-div">
											<div class="row">
												<div class="col-md-12">
													<div class="trave-drop">
														<div class="travel-left">Adults</div>
														<div class="plus-minus">
															<div class="minus adultminus">
																<i class="fa fa-minus-circle"></i>
															</div>
															<input type="hidden" name="adults" class="adultval"
																value="1" maxlength="9" />
															<div class="text adultcount">1</div>
															<div class="plus adultplus">
																<i class="fa fa-plus-circle"></i>
															</div>
														</div>
													</div>
												</div>
											</div>
											<hr>
											<div class="row">
												<div class="col-md-12">
													<div class="trave-drop">
														<div class="travel-left">Children
															<small>Ages (2-17)</small>
														</div>
														<div class="plus-minus ">
															<div class="minus childminus">
																<i class="fa fa-minus-circle"></i>
															</div>
															<input type="hidden" name="children" id="childval"
																class="childval" value="0" />

															<div class="text childcount">0</div>
															<div class="plus childplus">
																<i class="fa fa-plus-circle"></i>
															</div>
														</div>
													</div>
												</div>

											</div>

											<div class="row">
												<div class="col-md-12">
													<div class="trave-drop">
														<div class="travel-left">Infants <small>Age Under 0-2</small>
														</div>
														<div class="plus-minus">
															<div class="minus infantminus">
																<i class="fa fa-minus-circle"></i>
															</div>
															<input type="hidden" name="infant" id="infantval"
																class="infantval" value="0" />
															<div class="text infantcount">0</div>
															<div class="plus infantplus">
																<i class="fa fa-plus-circle"></i>
															</div>
														</div>
													</div>
												</div>
											</div>
											<hr>
											<div class="row">
												<div class="col-md-12">
													<div class="trave-drop">
														<div class="travel-left">Class </div>
														<select class="dropdown_item_select search_input class-icon"
															name="flight_class">

															<option value="2%Economy">Economy</option>
															<option value="3%Premium Economy">Premium Economy</option>
															<option value="4%Business">Business</option>

															<option value="6%First">First Class</option>
														</select>
													</div>
												</div>
											</div>

											<div class="container">
												<div class="row">
													<div class="col-md-12">
														<span class="a-done-btn" id="flightdone">Done</span>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div>
									<span class="t-err">Enter Your City</span>
								</div>
							</div>
							<!-- newcode -->

							<!-- <div class="search_item">
							<div>Adults</div>
							<select name="adults" id="adults_1" class="dropdown_item_select search_input">
								<option value="1">01</option>
								<option value="2">02</option>
								<option value="3">03</option>
								<option value="4">04</option>
								<option value="5">05</option>
								<option value="6">06</option>
								<option value="7">07</option>
								<option value="8">08</option>
								<option value="9">09</option>
							</select>
							<i class="fa fa-user fa-icon"></i>
						</div>
						<div class="search_item">

							<div>children</div>
							<select name="children" id="children_1" class="dropdown_item_select search_input children-icon">
								<option value="0">0</option>
								<option value="1">01</option>
								<option value="2">02</option>
								<option value="3">03</option>
							</select>
							<i class="fa fa-child fa-icon"></i>
						</div>
						<div class="search_item">

							<div>Class</div>
							<select class="dropdown_item_select search_input class-icon" name="flight_class">

								<option value="2%Economy">Economy</option>
								<option value="3%Premium Economy">Premium Economy</option>
								<option value="4%Business">Business</option>

								<option value="6%First">First Class</option>
							</select>
							<i class="fa fa-plane fa-icon"></i>
						</div> -->
							<div class="multicity-main" style="display:none !important;">
								<div class="showmultinew" id="showmulti">

								</div>
								<div class="more_options-1">
									<div class="more_options_trigger">
										<a href="javascript:void()" class="add_more" id="add_more-1">Add Another
											Flight</a>
									</div>
								</div>

							</div>
							<div class="" style="width:100%;">
								<div class="float-right">
									<button
										class="button search_button">search<span></span><span></span><span></span></button>
								</div>
							</div>
							<!-- <div class="error">Error</div> -->
						</form>
					</div>

					<!-- Search Panel -->

					<div class="search_panel">
						<form action="{{ route('gethotels') }}" method="post" id="search_form_2"
							class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start hotels">
							<div class="search_item">
								{{ csrf_field() }}
								<div>
									<div>Going to</div>
									<input list="cities1" type="text" name="destination"
										class="destination search_input" placeholder="City / Hotel" autocomplete="off"
										id="search-keyword">
									<i class="fa fa-map-marker fa-icon"></i>
									<datalist id="cities1">
									</datalist>
								</div>
								<div>
									<span class="t-err">Enter Your Return Date</span>
								</div>
							</div>
							<div class="search_item">
								<div>
									<div>check in</div>
									<input type="text" name="check_in" class="check_in search_input"
										placeholder="DD/MM/YYYY" autocomplete="off" id="check-in">
									<i class="fa fa-calendar fa-icon"></i>
								</div>
								<div>
									<span class="t-err">Enter Your Return Date</span>
								</div>
							</div>
							<div class="search_item">
								<div>
									<div>check out</div>
									<input type="text" name="check_out" class="check_out search_input"
										placeholder="DD/MM/YYYY" autocomplete="off" id="check-out">
									<i class="fa fa-calendar fa-icon"></i>
								</div>
								<div>
									<span class="t-err">Enter Your Return Date</span>
								</div>
							</div>

							<!-- start code -->
							<div class="search_item">
								<div>
									<div>Rooms</div>
									<div class=" traveller-dropdown" style="margin-bottom:0px;">
										<div class="searchhotel" style="margin-bottom:0px;">
											<input type="text" class="search_input totalrooms " name="flight_nocheck"
												value="1  Rooms" autocomplete="off">
											<i class="fa fa-user fa-icon"></i>
										</div>
										<div class="dropdown-menu dropshowhotel filter-hotel-div">
											<div class="row">
												<div class="col-md-12">
													<div class="trave-drop">
														<div class="travel-left">Rooms</div>
														<div class="plus-minus">
															<div class="minus roomminus">
																<i class="fa fa-minus-circle"></i>
															</div>
															<input type="hidden" name="newroom" class="roomval"
																value="1" maxlength="9" />
															<div class="text roomcount">1</div>
															<div class="plus roomplus">
																<i class="fa fa-plus-circle"></i>
															</div>
														</div>
													</div>
												</div>
											</div>
											<hr>
											<div class="row">
												<div class="col-md-12 newroom">
													<div class="roomcheckcount-1">
														<div class="row">
															<div class="col-md-12">

																<div class="trave-drop">
																	<div class="travel-left">Room 1</div>
																	<div class="travel-left">
																		<small>Adults (12 + years)</small>
																	</div>
																	<div class="plus-minus ">
																		<div class="minus roomadultminus" id="1">
																			<i class="fa fa-minus-circle"></i>
																		</div>
																		<input type="hidden" name="roomadult-1"
																			id="adultval" class="roomadultval-1"
																			value="2" />

																		<div class="text roomadultcount-1">2</div>
																		<div class="plus roomadultplus" id="1">
																			<i class="fa fa-plus-circle"></i>
																		</div>
																	</div>
																</div>
																<div class="trave-drop">
																	<div class="travel-left">
																		<small>Child (<12 years)</small> </div> <div
																				class="plus-minus ">
																				<div class="minus roomchildminus"
																					id="1">
																					<i class="fa fa-minus-circle"></i>
																				</div>
																				<input type="hidden" name="children-1"
																					id="roomchildval"
																					class="roomchildval-1 chkchild"
																					value="0" />

																				<div class="text roomchildcount-1">0
																				</div>
																				<div class="plus roomchildplus" id="1">
																					<i class="fa fa-plus-circle"></i>
																				</div>
																	</div>
																</div>
																<div class="trave-drop showchilddiv-1"
																	style="display: none">

																</div>
															</div>
														</div>

													</div>
												</div>

												<hr>

											</div>
											<!-- close row div -->

											<div class="row">

											</div>
											<hr>


											<div class="container">
												<div class="row">
													<div class="col-md-12">
														<!-- <a href="#" class="a-done-btn">Done</a> -->
														<span class="a-done-btn" id="hoteldone">Done</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div>
									<span class="t-err">Enter Your City</span>
								</div>
							</div>
							<!-- newcode -->
							<div class="text-center">
								<button class="button search_button"
									id="search_hotel_btn">search<span></span><span></span><span></span></button>
							</div>
						</form>
					</div>

					<!-- Search Panel -->

					<div class="search_panel" style="display: none">

						<!-- Packages-->
						<form action="{{ route('packageresult') }}" method="post" id="search_form_4"
							class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start flights">
							<!-- 	<div class="extras">
                                <ul class="search_extras clearfix">
                                    <li class="search_extras_item">
                                        <div class="clearfix">
                                            <label class="container3">Domestic
                                                <input type="radio" value="1" checked="checked" name="pckgtype" class="cstm-rdio checkradio">
                                                <span class="checkmark"></span>
                                            </label>

                                        </div>
                                    </li>
                                    <li class="search_extras_item">
                                        <div class="clearfix">
                                            <label class="container3">International
                                                <input type="radio" value="2" name="pckgtype" class="cstm-rdio checkradio">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </li>

                                </ul>
                            </div> -->
							<div class="search_item" style="width: 70%">
								{{ csrf_field() }}
								<div>
									<div>Packages</div>
									<select name="pckgname" class="custom-select search_input " id="pckgname">
										<option value="0">Select Packages</option>

									</select>
									<i class="fa fa-map-marker fa-icon"></i>
								</div>
								<div>
									<span class="t-err" id="pckgcity"></span>
								</div>
							</div>
							<div class="" style="">
								<div class="">
									<button class="button search_button"
										style="transform: translateY(4px);;">search<span></span><span></span><span></span></button>
								</div>
							</div>
							<!-- <div class="error">Error</div> -->
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		/* When the user clicks on the button,
    toggle between hiding and showing the dropdown content */
		function myFunction() {
			document.getElementById("myDropdown").classList.toggle("show");
		}

		// Close the dropdown if the user clicks outside of it
		window.onclick = function (event) {
			if (!event.target.matches('.dropbtn')) {
				var dropdowns = document.getElementsByClassName("dropdown-content");
				var i;
				for (i = 0; i < dropdowns.length; i++) {
					var openDropdown = dropdowns[i];
					if (openDropdown.classList.contains('show')) {
						openDropdown.classList.remove('show');
					}
				}
			}
		}
		var flight = document.getElementById("flight-tab")
			var hotel = document.getElementById("hotel-tab")
			var pack = document.getElementById("pckg")
			pack.onclick=function(){
			
			}
		function f() {
			var page=localStorage.getItem("tab")
		
		
			
			
			if (page == "flight") {
				flight.click();
				
			} else if (page == "hotel") {
			
				hotel.click()
				
			} else if (page == "package") {
			
			
				pack.click()
			
			}


		}
		document.body.onload=function(){
			f()
			localStorage.clear()
		}
		
	</script>
	<script>
		var input = document.querySelector(".searchflight input");
		var hInput = document.querySelector(".searchhotel input");
		var fil_div = document.querySelector(".filter-res-div");
		var fil_h_div = document.querySelector(".filter-hotel-div");
		var searchDiv = document.getElementsByClassName("search")[0];

		var myVar;
		var reason = 1;

		input.addEventListener("click", function () {
			this.addEventListener("click", rem)
		})
		input.addEventListener("click", function () {
			this.addEventListener("click", rem)
		})
		input.addEventListener("focus", function () {
			myVar = window.setInterval(timecheck, 100)
		})
		input.addEventListener("blur", function (event) {
			event.preventDefault();
			reason = 0;

		})

		hInput.addEventListener("click", function () {
			this.addEventListener("click", rem1)
		})
		hInput.addEventListener("click", function () {
			this.addEventListener("click", rem1)
		})
		hInput.addEventListener("focus", function () {

			myVar = window.setInterval(timecheck1, 100)

		})

		function timecheck(e) {
			var rect = input.getBoundingClientRect()
			x = rect.left;
			y = rect.top;
			w = rect.width;
			h = rect.height;
			if (fil_div.clientHeight > y) {
				fil_div.style.top = "100%"

			} else {
				fil_div.style.top = "-313px"

				fil_div.style.zIndex = "99999"
			}

		}

		function rem() {
			clearInterval(myVar);
			if (reason == 1) {
				input.blur()
			} else {
				input.focus()
			}
			input.removeEventListener("click", rem)
		}

		function timecheck1(e) {

			var rect = hInput.getBoundingClientRect();
			x = rect.left;
			y = rect.top;
			w = rect.width;
			h = rect.height;
			if (fil_h_div.clientHeight > y) {
				fil_h_div.style.top = "100%"

			} else {
				fil_h_div.style.top = "-313px"

				fil_h_div.style.zIndex = "99999"
			}

		}

		function rem1() {

			clearInterval(myVar);

			if (reason == 1) {
				hInput.blur()
			} else {
				hInput.focus()
			}
			hInput.removeEventListener("click", rem1)
		}
	</script>
</body>

</html>