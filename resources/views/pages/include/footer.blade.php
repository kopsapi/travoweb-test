<!--  <footer class="footer">
		<div class="container">
			<div class="row"> -->
<!-- Footer Column -->
<!-- <div class="col-lg-3 footer_column">
					<div class="footer_col">
						<div class="footer_content footer_about">
							<div class="logo_container footer_logo">
								<div class="logo"><a href="#"><img src="{{ asset('assets/images/logo.png') }}" alt="">travoweb</a></div>
							</div>
							<p class="footer_about_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis vu lputate eros, iaculis consequat nisl. Nunc et suscipit urna. Integer eleme ntum orci eu vehicula pretium.</p>
							<ul class="footer_social_list">
								<li class="footer_social_item"><a href="#"><i class="fa fa-pinterest"></i></a></li>
								<li class="footer_social_item"><a href="#"><i class="fa fa-facebook-f"></i></a></li>
								<li class="footer_social_item"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="footer_social_item"><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li class="footer_social_item"><a href="#"><i class="fa fa-behance"></i></a></li>
							</ul>
						</div>
					</div>
				</div> -->
<!-- Footer Column -->
<!-- 	<div class="col-lg-3 footer_column">
					<div class="footer_col">
						<div class="footer_title">blog posts</div>
						<div class="footer_content footer_blog"> -->

<!-- Footer blog item -->
<!-- 	<div class="footer_blog_item clearfix">
								<div class="footer_blog_image"><img src=" {{ asset('assets/images/footer_blog_1.jpg') }}" alt=""></div>
								<div class="footer_blog_content">
									<div class="footer_blog_title"><a href="javascript:void()">Travel with us this year</a></div>
									<div class="footer_blog_date">May 29, 2019</div>
								</div>
							</div> -->

<!-- Footer blog item -->
<!-- 	<div class="footer_blog_item clearfix">
								<div class="footer_blog_image"><img src="images/footer_blog_2.jpg" alt=""></div>
								<div class="footer_blog_content">
									<div class="footer_blog_title"><a href="javascript:void()">New destinations for you</a></div>
									<div class="footer_blog_date">Jun 19, 2019</div>
								</div>
							</div> -->

<!-- Footer blog item -->
<!-- 	<div class="footer_blog_item clearfix">
								<div class="footer_blog_image"><img src="images/footer_blog_3.jpg" alt=""></div>
								<div class="footer_blog_content">
									<div class="footer_blog_title"><a href="javascript:void()">Travel with us this year</a></div>
									<div class="footer_blog_date">May 24, 2019</div>
								</div>
							</div>

						</div>
					</div>
				</div> -->

<!-- Footer Column -->
<!-- <div class="col-lg-3 footer_column">
					<div class="footer_col">
						<div class="footer_title">Quick Links</div>
						<div class="footer_content footer_menu">
							<ul class="clearfix">
								<li class="footer_nav"><a href="#">Home</a></li>
								<li class="footer_nav"><a href="#">About Us</a></li>
								<li class="footer_nav"><a href="#">Privacy Policy</a></li>
								<li class="footer_nav"><a href="#">Refund Policy</a></li>
								<li class="footer_nav"><a href="#">Terms & Condition</a></li>
								<li class="footer_nav"><a href="#">Contact Us</a></li>
							</ul>
						</div>
					</div>
				</div> -->

<!-- Footer Column -->
<!-- <div class="col-lg-3 footer_column">
					<div class="footer_col">
						<div class="footer_title">contact info</div>
						<div class="footer_content footer_contact">
							<ul class="contact_info_list">
								<li class="contact_info_item d-flex flex-row">
									<div><div class="contact_info_icon"><img src="images/placeholder.svg" alt=""></div></div>
									<div class="contact_info_text"> 7/25 Sheridan Lane Gundagai NSW 27221</div>
								</li>
								<li class="contact_info_item d-flex flex-row">
									<div><div class="contact_info_icon"><img src="images/phone-call.svg" alt=""></div></div>
									<div class="contact_info_text">+61 2 5924 0804, 1800 123 2135</div>
								</li>
								<li class="contact_info_item d-flex flex-row">
									<div><div class="contact_info_icon"><img src="images/message.svg" alt=""></div></div>
									<div class="contact_info_text"><a href="mailto:info@travoweb.com"><span class="__cf_email__">info@travoweb.com</span></a></div>
								</li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
	</footer> -->

<style>


</style>
<footer class="footer">
	<div class="container">
		<div class="row">
			<!-- Footer Column -->
			<div class="col-lg-3 footer_column">
				<div class="footer_col">
					<div class="footer_content footer_about">
						<div class="logo_container footer_logo">
							<div class="logo"><a href="{{url('/index')}}"><img
										src="{{ asset('assets/images/footer-logo.png') }}" alt=""
										style="height:60px;"></a>
							</div>
						</div>

						<div class="footer_content footer_menu">
							<ul class="clearfix">
								<li class="footer_nav"><a href="">1000+ Destinations</a></li>
								<li class="footer_nav"><a href="">Super Fast Booking</a></li>
								<li class="footer_nav"><a href="">Inclusive Packages</a></li>
								<li class="footer_nav"><a href="">Latest Model Vehicles</a></li>
								<li class="footer_nav"><a href="">Best Price In The Industry</a></li>
								<li class="footer_nav"><a href="">Accesibility managment</a></li>
							</ul>
						</div>

						<!-- <p class="footer_about_text">Travo Web is a Pty Limited licensed travel agent. Travo Web is not
							a direct supplier of products and services, or controlling the costs appropriate to any of
							the travel alternatives (Flights, accommodation services, car rentals, cruise packages. </p> -->
						<!--<ul class="footer_social_list">
								<li class="footer_social_item"><a href="#"><i class="fa fa-pinterest"></i></a></li>
								<li class="footer_social_item"><a href="#"><i class="fa fa-facebook-f"></i></a></li>
								<li class="footer_social_item"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="footer_social_item"><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li class="footer_social_item"><a href="#"><i class="fa fa-behance"></i></a></li>
							</ul>-->
					</div>
				</div>
			</div>
			<!-- Footer Column -->


			<!-- Footer Column -->
			<div class="col-lg-3 footer_column">
				<div class="footer_col">
					<div class="footer_title">Customer Care</div>
					<div class="footer_content footer_menu">
						<ul class="clearfix">
							<li class="footer_nav"><a href="{{url('/aboutus')}}">About Us</a></li>
							<li class="footer_nav"><a href="{{url('/privacy-and-policy')}}">Privacy Policy</a></li>
							<li class="footer_nav"><a href="{{url('/refund-policy')}}">Refund Policy</a></li>
							<li class="footer_nav"><a href="{{url('/our-profile')}}">Profile</a></li>
							<li class="footer_nav"><a href="{{url('/disclaimer')}}">Disclaimer Policy</a></li>
							<li class="footer_nav"><a href="{{url('/terms-and-conditions')}}">Terms & Condition</a></li>
							<li class="footer_nav"><a href="{{url('/faqs')}}">FAQs</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-3 footer_column">
				<div class="footer_col">
					<div class="footer_title">Overview</div>
					<div class="footer_content footer_menu">
						<ul class="clearfix">
							<li class="footer_nav"><a href="{{url('/partner-with-travoweb')}}">Partner with Travoweb</a>
							</li>
							<li class="footer_nav"><a href="{{url('/travoweb-business')}}">Travoweb-Business</a></li>


							<!-- <li class="footer_nav"><a href="{{url('/holiday-packages')}}">Holiday Packages</a></li> -->
							<li class="footer_nav"><a href="{{url('/airport-codes')}}">Airport Codes</a></li>

							<li class="footer_nav"><a href="{{url('/domestic-airlines')}}">Domestic Airlines</a></li>
							<li class="footer_nav"><a href="{{url('/international-airlines')}}">International Airlines</a></li>
							<li class="footer_nav"><a href="{{url('/feedback')}}">Feedback & Complaints</a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- Footer Column -->
			<div class="col-lg-3 footer_column">
				<div class="footer_col">
					<div class="footer_title">contact info</div>
					<div class="footer_content footer_contact">
						<ul class="contact_info_list">
							<li class="contact_info_item d-flex flex-row">
								<div>
									<div class="contact_info_icon"><img
											src="{{ asset('assets/images/placeholder.svg') }}" alt=""></div>
								</div>
								<div class="contact_info_text"> 7/25 Sheridan Lane Gundagai NSW 2722</div>
							</li>
							<li class="contact_info_item d-flex flex-row">
								<div>
									<div class="contact_info_icon"><img
											src="{{ asset('assets/images/phone-call.svg') }}" alt=""></div>
								</div>
								<div class="contact_info_text">+61 2 5924 0804</div>
							</li>
							<li class="contact_info_item d-flex flex-row">
								<div>
									<div class="contact_info_icon"><img src="{{ asset('assets/images/message.svg') }}"
											alt=""></div>
								</div>
								<div class="contact_info_text"><a href="mailto:info@travoweb.com"><span
											class="__cf_email__">info@travoweb.com</span></a></div>
							</li>
						</ul>
					</div>
				</div>
			</div>

		</div>
	</div>
</footer>
<style>
	.logo a img {
		width: 100% !important;
		height: auto !important;
	}
	h1.about-title {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    color: white;
    background: rgba(49, 18, 75, 0.8);
    padding: 10px 18px;
    width: auto !IMPORTANT;
    min-width: 200px;
    text-align: center;
}
.search_panel.active::-webkit-scrollbar {
    width: 7px;
    height: 7px;
}
	@media screen and (max-width:1200px){
	.logo a img {
    width: auto !important;
    height: 45px !important;
}
.main_nav_item a {
    font-family: 'Open Sans', sans-serif;
    font-size: 12px !important;
    font-weight: 600;
    color: #00206A;
    text-transform: uppercase;
    padding-bottom: 10px;
    padding-top: 10px;
}
	}

</style>