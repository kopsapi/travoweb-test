<!DOCTYPE html>
<html>
<head>
<title>Page not found - 404</title><style>*{	padding:0;	margin:0;}.page-not-found{	height:100%;	width:100%;}.page-not-found img{	display:block;	vertical-align:middle;	max-width:100%;	width:auto;	height:auto;	object-fit:cover;	min-height: 657px;}.go-back{	font-size: 35px;    font-weight: 700;    color: #fff;    text-transform: uppercase;    line-height: 60px;	border-radius: 8px;    padding-left: 46px;    padding-right: 46px;    background: #31124b;    -webkit-transform: translateY(15px);    -moz-transform: translateY(15px);    -ms-transform: translateY(15px);    -o-transform: translateY(15px);    transform: translateY(15px);    border: none;    outline: none;    cursor: pointer;	position:absolute;	z-index:1;	text-decoration:none;	top:50%;	left:12%;}.go-back:hover{    background: #1f0733;}</style>
</head>
<body>	<div class="page-not-found">		<img src="assets/images/404.jpg" class="img-responsive">		<a href="{{url('/index')}}" class="go-back">GO BACK</a>	</div></body>
</html>