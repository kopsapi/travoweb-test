@include('pages.include.header')
<style>
.intro
{
	padding-top:0px;
	padding-bottom:50px;
}
.search-breadcrumb
{
	background:linear-gradient(to right, #002c, #8d4fff);
	padding:15px 5px;
}
.search-breadcrumb h4
{
	color:#FFF;
	font-size:18px;
}
.search-breadcrumb h4.flight-class
{
	font-size:16px;
}
.search-breadcrumb h4.flight-class:nth-child(2)
{
	padding-left:18px;
}
.search-breadcrumb h4 small
{
	padding-left: 5px;
	padding-right: 5px;
    text-transform: uppercase;
    color: #cbb;
}
.search-breadcrumb h5
{
	margin-bottom:0px;
	font-size: 17px;
}
.search-breadcrumb h5 small
{
    color: #cbb;
}
.search-breadcrumb .btn-modify
{
	background:#fa9e1b;
	border-radius:27px;
	color:#FFF;
	padding: 10px 46px;
	line-height:26px;
	cursor:pointer;
}
.search-breadcrumb .btn-modify:hover
{
	opacity: 0.9;
}
.search-breadcrumb .plane
{
	float:right;
	color:#fa9e1b;
	font-size:45px;
}
.fa-rotate-45
{
	filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=1);
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
.flight-details
{
    padding: 30px 5px;
}
.flight-details .flight-filter h3
{
	background: #fa9e1b;
    color: #FFF;
    padding: 6px 5px;
    text-align: center;
    border-radius: 4px;
    font-size: 21px;
    margin-bottom: 15px;
}
.flight-details .flight-filter .filter h4
{
	background:#00206A;
	color:#FFF;
	padding:8px 15px;
}
.filter
{
	border: 1px solid #CCC;
	margin-bottom:20px;
	box-shadow: 0px 0px 12px 3px rgba(0,0,0,0.1);
	border-bottom:2px solid #fa9e1b;
}
.filter:hover
{
	box-shadow: 0px 0px 12px 3px rgba(0,0,0,0.2);
}
.filter .slider-time, .filter .slider-time2
{
	color: #fff;
    font-size: 14px;
    line-height: 1.333;
    text-shadow: none;
    padding: 1px 5px;
    background: #fa9e1b;
    border-radius: 3px;
	margin-bottom:10px;
}
.filter .inner-filter
{
	padding:10px 20px;
}
.checkcontainer
{
	display: block;
    position: relative;
    padding-left: 30px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 15px;
	font-weight:600;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
	color:#00206A;
}
.checkcontainer input
{
	position: absolute;
    opacity: 0;
    cursor: pointer;
}
.checkcontainer input:checked ~ .checkmark
{
	background-color: #fa9e1b;
	border: 1px solid #f29410;
}
.checkmark
{
	position: absolute;
    top: 3px;
    left: 0;
    height: 18px;
    width: 18px;
	background-color: #eee;
    border: 1px solid #CCC;
    border-radius: 3px;
}
.checkcontainer input:checked ~ .checkmark:after
{
	display:block;
}
.checkcontainer .checkmark:after
{
	left: 5px;
    top: 1px;
    width: 6px;
    height: 11px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
.checkmark:after
{
    content: "";
    position: absolute;
    display: none;
}
.airlines-list .title-bar
{
	background:#fa9e1b;
	padding:8px 15px;
	border-radius: 4px;
	margin-bottom:20px;
}
.airlines-list .bar-heading h5
{
	font-size:18px;
	color:#FFF;
	font-weight:600;
	text-align:center;
	margin-bottom:0px;
}
.flight-list
{
	background:#F5F5F5;
	text-align:center;
	border:1px solid #CCC;
	padding:10px 15px;
	border-bottom: 3px solid #00206A;
    box-shadow: 0px 0px 17px 1px rgba(0,0,0,0.1);
	margin-bottom:20px;
}
.flight-list .col-md-2, .flight-list .col-md-8, .title-bar .col-md-2
{
	padding-left:3px;
	padding-right:3px;
}
.flight-list p
{
	margin-bottom:5px;
	color:#000;
	line-height:20px;
}
.flight-list .flight-name .flight-logo
{
	height:25px;
	margin:0 auto;
	margin-bottom:5px;
}
.flight-list .flight-depart .fa-plane
{
	color:#fa9e1b;
	font-size:16px;
}
.flight-list .flight-price .fa-inr
{
	color:#fa9e1b;
	font-size:22px;
}
.flight-list .flight-price .flight-price-heading
{
	color:#00206A;
	font-size:20px;
	font-weight:600;
}
.flight-list .flight-depart .flight-depart-heading, .flight-list .flight-arrival .flight-arrival-heading, .flight-list .flight-duration .flight-duration-heading
{
	font-weight:600;
}
.flight-list .flight-book .details
{
	font-weight:600;
	color:#FF0000;
	font-size:13px;
}
.flight-list .flight-book .flight-book-btn
{
	background:#00206A;
	border-color:#00206A;
	color:#FFF;
	padding:5px 20px;
}
.flight-list .flight-book .flight-book-btn:hover
{
	background:transparent;
	border-color:#00206A;
	color:#00206A;
}
.flight-list .flight-depart .flight-depart-text
{
	color:#00206A;
	font-weight:600;
}
.flight-list .flight-name .flight-name-heading
{
	color:#00206A;
	font-weight:600;
}
.flight-list .flight-fare
{
	text-align:left;
	border-top:1px solid #CCC;
	padding-top:8px;
}
.flight-list .flight-fare .fare-rules
{
	color:#00206A;
	font-size:14px;
	font-weight:600;
	text-align:center;
}
.flight-list .flight-fare .airline-mark span
{
	color:#000;
	font-weight:400;
}
.flight-list .flight-fare .airline-mark
{
	color:#00206A;
	font-size:14px;
	font-weight:600;
}
.flight-list .flight-fare .fare-type.non-refundable
{
	color:red;
}
.flight-list .flight-fare .fare-type.refundable
{
	color:green;
}
.flight-list .flight-fare .fare-type
{
	font-size:13px;
	font-weight:600;
	text-align:right;
}
@media (max-width: 992px)
{
	.flight-list .col-lg-3, .flight-list .col-md-3
	{
		-ms-flex: 0 0 25%;
		flex: 0 0 25%;
		max-width: 25%;
	}
	.flight-list .col-lg-4, .flight-list .col-md-4
	{
		-ms-flex: 0 0 33.333333%;
		flex: 0 0 33.333333%;
		max-width: 33.333333%;
	}
	.flight-list .col-lg-2, .flight-list .col-md-2
	{
		-ms-flex: 0 0 16.666667%;
		flex: 0 0 16.666667%;
		max-width: 16.666667%;
	}
	.flight-list .col-md-7
	{
		-ms-flex: 0 0 58.333333%;
		flex: 0 0 58.333333%;
		max-width: 58.333333%;
	}
	.flight-list .col-md-8
	{
		-ms-flex: 0 0 66.666667%;
		flex: 0 0 66.666667%;
		max-width: 66.666667%;
	}
	.flight-list .flight-book .flight-book-btn
	{
		padding:5px;
	}
	#res-two
	{
		order:2;
	}
	#res-one
	{
		order:1;
	}
	.res-none
	{
		display:none;
	}
	.title-bar .col-md-2
	{
		-ms-flex: 0 0 20%;
		flex: 0 0 20%;
		max-width: 20%;
	}
	.res-mb-20
	{
		margin-bottom:20px;
	}
}
.no-flight
{
	min-height:400px;
	background:#00206A;
	padding:140px 0px;
	text-align:center;
}
.no-flight h3
{
	font-size:40px;
}
.btn-back
{
	font-size: 25px;
    font-weight: 700;
    color: #fff;
    text-transform: uppercase;
    background: #fa9e1b;
	border:1px solid #fa9e1b;
    border: none;
    outline: none;
    cursor: pointer;
	padding:10px 50px;
    border-radius: 27px;
	line-height: 37px;
}
.btn-back:hover
{
	background:#FFF;
	color:#fa9e1b;
}
.modify-flight
{
	background:linear-gradient(to right, #002c, #8d4fff);
}
.result-modify
{
	margin:30px 0px 0px;
	display:none;
}
.result-modify .search_panel
{
	padding:30px 30px 45px;
	border-radius: 10px;
}
.result-modify .search
{
	position:relative;
	top:0;
}
</style>
<body>
<div class="super_container">
	<!-- Modal -->
	<div class="modal fade modify-flight" id="myModal" role="dialog">
		<div class="modal-dialog">
		  <!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Modify Search</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Header -->
		@include('pages.include.topheader')
	<div class="home" style="height:20vh;">
		<!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->
		<div class="home_content">
			<div class="home_title">Results  </div>
		</div>
	</div>
	<!-- Intro -->
	<div class="intro">
	
		
		<?php
		print_r($array);
		$data_string;
		// echo date("d M Y l" ,$mainarray['depdate']);

		if(!empty($array)){
		}else
		 {			
		  "No data found";
		 }        
		?>
		
		@if(!empty($resu))
		<section class="search-breadcrumb">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="intro_content">
							<div class="row">
								<div class="col-lg-3 res-mb-20">
									<h4>{{$mainarray['flightorign']}}  <i class="fa fa-plane plane fa-rotate-45"></i></h4>
									<h5><small>{{$mainarray['airportorigncityname']}}  ({{$mainarray['airportorignname']}})</small></h5>
								</div>
								<div class="col-lg-3 res-mb-20">
									<h4>{{$mainarray['flightdep']}}</h4>
									<h5><small>{{$mainarray['airportdeptcityname']}} ({{$mainarray['airportdeptname']}})</small></h5>
								</div>
								<div class="col-lg-4 res-mb-20">
									<h4 class="flight-class"><i class="fa fa-calendar"></i>&nbsp;  {{date("d M Y l", strtotime($mainarray['depdate']))}}</h4>
									<h4 style="margin-bottom:0px;" class="flight-class"><small>ADULTS</small> {{$mainarray['AdultCount']}} <small>CHILD</small> {{$mainarray['ChildCount']}} <small>CLASS</small> {{$mainarray['FlightCabinName']}}</h4>
								</div>
								<div class="col-lg-2">
									<button type="button" class="btn btn-modify">Modify</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="result-modify">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="search">
							<!-- Search Contents -->
							<div class="fill_height">
								<!-- Search Panel -->
								<div class="search_panel active">
									<form action="{{route('flightsearch')}}" method="post" id="search_form_1" class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start flights">
										<div class="extras">
											<ul class="search_extras clearfix">
												<li class="search_extras_item">
													<div class="clearfix">
														<input type="radio" id="search_extras_1" name="one" value="1" class="search_extras_cb" checked>
														<label for="search_extras_1">One Way</label>
													</div>	
												</li>
												<li class="search_extras_item">
													<div class="clearfix">
														<input type="radio" id="search_extras_2" name="one" value="2" class="search_extras_cb">
														<label for="search_extras_2">Round Trip</label>
													</div>
												</li>
											</ul>
										</div>
										<div class="search_item">
											{{ csrf_field() }}
											<div>Flying From</div>
											<input type="text" class="search_input autocomplete" name="flight_from" required="required" placeholder="City or Airport" autocomplete="off" id="autocomplete">
											<i class="fa fa-map-marker fa-icon"></i>
										</div>
										<a href="javascript:void()" class="exchange-icon">
											<i class="fa fa-exchange"></i>
										</a>
										<div class="search_item">
											<div>Flying To</div>
											<input type="text" class="search_input autocomplete" name="flight_to" required="required" placeholder="City or Airport" autocomplete="off" id="autocomplete1">
											<i class="fa fa-map-marker fa-icon"></i>
											
										</div>
										<div class="search_item">
											<div>Departures</div>
											<input type="text" class="search_input"  name="flight_dep" placeholder="YYYY-MM-DD" autocomplete="off" id="departure">
											<i class="fa fa-calendar fa-icon"></i>
										</div>
										<div class="search_item">
											<div>Return Date</div>
											<input type="text" class="search_input" name="flight_return" placeholder="YYYY-MM-DD" autocomplete="off" id="return" disabled>
											<i class="fa fa-calendar fa-icon"></i>
										</div>
										<div class="search_item">
											<div>Adults</div>
											<select name="adults" id="adults_1" class="dropdown_item_select search_input">
												<option value="1">01</option>
												<option value="2">02</option>
												<option value="3">03</option>
												<option value="4">04</option>
												<option value="5">05</option>
												<option value="6">06</option>
												<option value="7">07</option>
												<option value="8">08</option>
												<option value="9">09</option>
											</select>
											<i class="fa fa-user fa-icon"></i>
										</div>
										<div class="search_item">
											<div>children</div>
											<select name="children" id="children_1" class="dropdown_item_select search_input children-icon">
												<option value="0">0</option>
												<option value="1">01</option>
												<option value="2">02</option>
												<option value="3">03</option>
											</select>
											<i class="fa fa-child fa-icon"></i>
										</div>
										<div class="search_item">
											<div>Class</div>
											<select class="dropdown_item_select search_input class-icon" name="flight_class">
												<option value="1%All">All</option>
												<option value="2%Economy">Economy</option>
												<option value="3%Premium Economy">Premium Economy</option>
												<option value="4%Business">Business</option>
												<option value="5%Premium Business">Premium Business</option>
												<option value="6%First">First</option>
											</select>
											<i class="fa fa-plane fa-icon"></i>
										</div>
										<div class="float-right">
											<button class="button search_button">search<span></span><span></span><span></span></button>
										</div>
									</form>
								</div>

								<!-- Search Panel -->

								<div class="search_panel">
									<form action="#" id="search_form_2" class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start hotels">
										<div class="search_item">
											<div>Going to</div>
											<input type="text" class="destination search_input autocomplete" required="required" placeholder="City / Hotel" autocomplete="off" id="autocomplete2">
											<i class="fa fa-map-marker fa-icon"></i>
										</div>
										<div class="search_item">
											<div>check in</div>
											<input type="text" class="check_in search_input" placeholder="YYYY-MM-DD" autocomplete="off" id="check-in">
											<i class="fa fa-calendar fa-icon"></i>
										</div>
										<div class="search_item">
											<div>check out</div>
											<input type="text" class="check_out search_input" placeholder="YYYY-MM-DD" autocomplete="off" id="check-out">
											<i class="fa fa-calendar fa-icon"></i>
										</div>
										<div class="search_item">
											<div>Rooms</div>
											<select name="adults" id="adults_2" class="dropdown_item_select search_input">
												<option>01</option>
												<option>02</option>
												<option>03</option>
												<option>04</option>
												<option>05</option>
												<option>06</option>
											</select>
											<i class="fa fa-building fa-icon"></i>
										</div>
										<div class="search_item">
											<div>adults</div>
											<select name="adults" id="adults_2" class="dropdown_item_select search_input">
												<option>01</option>
												<option>02</option>
												<option>03</option>
												<option>04</option>
												<option>05</option>
												<option>06</option>
											</select>
											<i class="fa fa-user fa-icon"></i>
										</div>
										<div class="search_item">
											<div>children</div>
											<select name="children" id="children_2" class="dropdown_item_select search_input">
												<option>00</option>
												<option>01</option>
												<option>02</option>
												<option>03</option>
											</select>
											<i class="fa fa-child fa-icon"></i>
										</div>
										<div class="text-center">
											<button class="button search_button">search<span></span><span></span><span></span></button>
										</div>
									</form>
								</div>
								<!-- Search Panel -->
							</div>		
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="flight-details">
			<div class="container">
				<div class="row">
					<div class="col-lg-9" id="res-two">
						<div class="airlines-list">
							<div class="title-bar">
								<div class="row">
									<div class="col-md-2">
										<div class="bar-heading">
											<h5>Airline</h5>
										</div>
									</div>
									<div class="col-md-2">
										<div class="bar-heading">
											<h5>Depart</h5>
										</div>
									</div>
									<div class="col-md-2">
										<div class="bar-heading">
											<h5>Arrive</h5>
										</div>
									</div>
									<div class="col-md-2">
										<div class="bar-heading">
											<h5>Duration</h5>
										</div>
									</div>
									<div class="col-md-2">
										<div class="bar-heading">
											<h5>Price</h5>
										</div>
									</div>
									<div class="col-md-2 res-none">
										<div class="bar-heading">
											<h5>&nbsp;</h5>
										</div>
									</div>
								</div>
							</div>
							<div class="showtable">
								<div class="newtable"  id="listflight" >
									@foreach($array as $arrays1)
									<?php
									$timetotal=0;
									//depature time
									for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
									{
										$arvdate1 = explode('T',$arrays1['Segments'][0][$seg_i]['Destination']['ArrTime']);
										$arvtime2 = date("H:s" , strtotime($arvdate1[1]));
										$arvtime1 = date("H:i:s" , strtotime($arvdate1[1]));
										$arvnewdate1= date("d-M-y" , strtotime($arvdate1[0]));
									}
									 
									
									 $arvnewdate1;
									$devdate = explode('T',$arrays1['Segments'][0][0]['Origin']['DepTime']);
					 				$depnewdate= date("d-M-y" , strtotime($devdate[0]));
					 				$depnewtime2 = date("H:s" , strtotime($devdate[1]));
					 				$depnewtime = date("H:i:s" , strtotime($devdate[1]));
					 				$newdepnewdate= date("Y-m-d" , strtotime($devdate[0]));
					 				
					 				//arrival time
					 				$arvdate = explode('T',$arrays1['Segments'][0][0]['Destination']['ArrTime']);
					 				$arvnewdate= date("d-M-y" , strtotime($arvdate[0]));
					 				$arvtime = date("H:s:i" , strtotime($arvdate[1]));
					 				$newarvnewdate= date("Y-m-d" , strtotime($arvnewdate1));
					 				$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newdepnewdate.''. $depnewtime);
									$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newarvnewdate.''. $arvtime1);
									
									$totalDuration = $from->diffInSeconds($to);
									 $durationhour =  gmdate('H', $totalDuration);
									 $durationmin =  gmdate('s', $totalDuration);
									
									$newtime=$durationhour.'H : '.$durationmin.'M';

					 				 $imgsou =  'assets/images/flag/'.$arrays1['Segments'][0][0]['Airline']['AirlineCode'].'.gif';
					 				$stopcount = count($arrays1['Segments'][0]);
					 				$uniq = $arrays1['Segments'][0][0]['Airline']['AirlineName'];
					 				if(!empty($arrays1['Segments'][0][0]['NoOfSeatAvailable']))
					 				{
					 					$publishvaleu=round($arrays1['Fare']['PublishedFare']);
					 					 $mainvalue = number_format($publishvaleu);
					 					 if($stopcount=='1')
					 					 {
					 					 	$mainstopflight = 'Non Stop';
					 					 }
					 					 else
					 					 {
					 					 	$stopcount1 = $stopcount-1;
					 					 	$mainstopflight = $stopcount1.' Stop';
					 					 }
					 				?>
									
										<div class="flight-list listflight">
											<div class="row">
												<div class="col-md-2">
													<div class="flight-name">
														<img src="{{$imgsou}}" class="img-responsive flight-logo">
														<p class="flight-name-heading">{{$arrays1['Segments'][0][0]['Airline']['AirlineName']}}</p>
														<p>{{$arrays1['Segments'][0][0]['Airline']['AirlineCode']}} - {{$arrays1['Segments'][0][0]['Airline']['FlightNumber']}}</p>
													</div>
												</div>
												<div class="col-md-2">
													<div class="flight-depart">
														<p class="flight-depart-heading">{{$depnewtime2}} | {{$depnewdate}} </p>
														<?php
														for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
										 				{
										 					echo $flightroute = '<p class="flight-depart-text">'.$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode'].' <i class="fa fa-plane fa-rotate-45"></i>    '.$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'].'</p>';
										 				}
														?>
													</div>
												</div>
												<div class="col-md-2">
													<div class="flight-arrival">
														<p class="flight-arrival-heading">{{$arvtime2}} | {{$arvnewdate1}}</p>
													</div>
												</div>
												<div class="col-md-2">
													<div class="flight-duration">
														<p class="flight-duration-heading">{{$newtime}}</p>
														<p style="color:#008cff">{{$mainstopflight}}</p>
													</div>
												</div>
												<div class="col-md-2">
													<div class="flight-price">
														<p class="flight-price-heading"><i class="fa fa-inr"></i> {{$mainvalue}}</p>
														<p>{{$arrays1['Segments'][0][0]['NoOfSeatAvailable']}} Seats Left</p>
													</div>
												</div>
												<div class="col-md-2">
													<div class="flight-book">
														<p><a href="#" class="btn flight-book-btn">Book</a></p>
														<a href="javascript:void()" class="details">Details</a>
													</div>
												</div>
											</div>
											<div class="flight-fare">
												<div class="row">
													<div class="col-md-2">
														<h5 class="fare-rules">Fare Rules</h5>
													</div>
													<div class="col-md-7">
														<h5 class="airline-mark">Airline Mark : <span> {{$arrays1['AirlineRemark']}}</span></h5>
													</div>
													<div class="col-md-3">
														@if($arrays1['IsRefundable'])
														<h5 class="fare-type refundable"><i class="fa fa-money"></i> Refundable</h5>
														@else
														<h5 class="fare-type non-refundable"><i class="fa fa-money"></i> Non-Refundable</h5>
														@endif

													</div>
												</div>
											</div>
											<div class="flight-details-tabs">
												<ul class="nav nav-tabs">
													<li><a data-toggle="tab" href="#home" class="active">Home</a></li>
													<li><a data-toggle="tab" href="#menu1">Menu 1</a></li>
													<li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
													<li><a data-toggle="tab" href="#menu3">Menu 3</a></li>
												</ul>

												<div class="tab-content">
													<div id="home" class="tab-pane fade in active">
													  1
													</div>
													<div id="menu1" class="tab-pane fade">
													  2
													</div>
													<div id="menu2" class="tab-pane fade">
													  3
													</div>
													<div id="menu3" class="tab-pane fade">
													  4
													</div>
												</div>
											</div>
											
											
											<!-- <div class="flight-details-tabs">
												<div class="row">
													<ul class="nav nav-tabs">
														<li class="active"><a data-toggle="tab" href="#home">Home</a></li>
														<li><a data-toggle="tab" href="#menu1">Menu 1</a></li>
														<li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
														<li><a data-toggle="tab" href="#menu3">Menu 3</a></li>
													</ul>

													<div class="tab-content">
														<div id="home" class="tab-pane fade in active">
														  <h3>HOME</h3>
														  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
														</div>
														<div id="menu1" class="tab-pane fade">
														  <h3>Menu 1</h3>
														  <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
														</div>
														<div id="menu2" class="tab-pane fade">
														  <h3>Menu 2</h3>
														  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
														</div>
														<div id="menu3" class="tab-pane fade">
														  <h3>Menu 3</h3>
														  <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
														</div>
													</div>
												</div>
											</div> -->
										</div>
									<?php } ?>
									@endforeach
								</div>
							</div>	
							
						</div>
						<div class="round-partition">
							<div class="row">
								<div class="col-lg-6 pd-r-5" id="res-two">
									<div class="airlines-list">
										<div class="title-bar">
											<div class="row">
												<div class="col-md-2">
													<div class="bar-heading">
														<h5>Airline</h5>
													</div>
												</div>
												<div class="col-md-2">
													<div class="bar-heading">
														<h5>Depart</h5>
													</div>
												</div>
												<div class="col-md-2">
													<div class="bar-heading">
														<h5>Arrive</h5>
													</div>
												</div>
												<div class="col-md-2">
													<div class="bar-heading">
														<h5>Duration</h5>
													</div>
												</div>
												<div class="col-md-2">
													<div class="bar-heading">
														<h5>Price</h5>
													</div>
												</div>
												<div class="col-md-2 res-none">
													<div class="bar-heading">
														<h5>&nbsp;</h5>
													</div>
												</div>
											</div>
										</div>
										<div class="showtable">
											<div class="newtable"  id="listflight" >
												@foreach($array as $arrays1)
												<?php
												//echo "<pre>"; print_r($arrays1); die;
												//depature time
												$devdate = explode('T',$arrays1['Segments'][0][0]['Origin']['DepTime']);
												$depnewdate= date("d-M-y" , strtotime($devdate[0]));
												$depnewtime = date("h:i" , strtotime($devdate[1]));
												$newdepnewdate= date("Y-m-d" , strtotime($devdate[0]));
												
												//arrival time
												$arvdate = explode('T',$arrays1['Segments'][0][0]['Destination']['ArrTime']);
												$arvnewdate= date("d-M-y" , strtotime($arvdate[0]));
												$arvtime = date("h:i" , strtotime($arvdate[1]));
												$newarvnewdate= date("Y-m-d" , strtotime($arvdate[0]));
												
												$ts1 = strtotime($depnewtime.''.$newdepnewdate);
												$ts2 = strtotime($newarvnewdate.''.$arvtime);
												$diff = abs($ts1 - $ts2) / 3600;
												$lastvc =  number_format((float)$diff, 2, '.', '');
												$new=explode('.',$lastvc);
												$newtime=$new[0].'H :'.$new[1].'M';
												

												 $imgsou =  'assets/images/flag/'.$arrays1['Segments'][0][0]['Airline']['AirlineCode'].'.gif';
												$stopcount = count($arrays1['Segments'][0]);
												$uniq = $arrays1['Segments'][0][0]['Airline']['AirlineName'];
												
												?>
												
													<div class="flight-list listflight">
														<div class="row">
															<div class="col-md-2">
																<div class="flight-name">
																	<img src="{{$imgsou}}" class="img-responsive flight-logo">
																	<p class="flight-name-heading">{{$arrays1['Segments'][0][0]['Airline']['AirlineName']}}</p>
																	<p>{{$arrays1['Segments'][0][0]['Airline']['AirlineCode']}} - {{$arrays1['Segments'][0][0]['Airline']['FlightNumber']}}</p>
																</div>
															</div>
															<div class="col-md-2">
																<div class="flight-depart">
																	<p class="flight-depart-heading">{{$depnewtime}} | {{$depnewdate}} </p>
																	<?php
																	for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
																	{
																		echo $flightroute = '<p class="flight-depart-text">'.$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode'].' <i class="fa fa-plane fa-rotate-45"></i>    '.$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'].'</p>';
																	}
																	?>
																</div>
															</div>
															<div class="col-md-2">
																<div class="flight-arrival">
																	<p class="flight-arrival-heading">{{$arvtime}} | {{$arvnewdate}}</p>
																</div>
															</div>
															<div class="col-md-2">
																<div class="flight-duration">
																	<p class="flight-duration-heading">{{$newtime}}</p>
																	<p>{{$stopcount}} Stop</p>
																</div>
															</div>
															<div class="col-md-2">
																<div class="flight-price">
																	<p class="flight-price-heading"><i class="fa fa-inr"></i> {{$arrays1['Fare']['PublishedFare']}}</p>
																	<p>2 Seats Left</p>
																</div>
															</div>
															<div class="col-md-2">
																<div class="flight-book">
																	<p><a href="#" class="btn flight-book-btn">Book</a></p>
																	<a href="javascript:void()" class="details">Details</a>
																</div>
															</div>
														</div>
														<div class="flight-fare">
															<div class="row">
																<div class="col-md-4">
																	<h5 class="fare-rules">Fare Rules</h5>
																</div>
																<div class="col-md-4">
																	<!-- <h5 class="airline-mark">Airline Mark : <span> {{$arrays1['AirlineRemark']}}</span></h5> -->
																</div>
																<div class="col-md-4">
																	@if($arrays1['IsRefundable'])
																	<h5 class="fare-type refundable"><i class="fa fa-money"></i> Refundable</h5>
																	@else
																	<h5 class="fare-type non-refundable"><i class="fa fa-money"></i> Non-Refundable</h5>
																	@endif

																</div>
															</div>
														</div>
														
														
														
														<div class="flight-details-tabs">
															<ul class="nav nav-tabs">
																<li><a data-toggle="tab" href="#home" class="active">Home</a></li>
																<li><a data-toggle="tab" href="#menu1">Menu 1</a></li>
																<li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
																<li><a data-toggle="tab" href="#menu3">Menu 3</a></li>
															</ul>

															<div class="tab-content">
																<div id="home" class="tab-pane fade in active">
																  1
																</div>
																<div id="menu1" class="tab-pane fade">
																  2
																</div>
																<div id="menu2" class="tab-pane fade">
																  3
																</div>
																<div id="menu3" class="tab-pane fade">
																  4
																</div>
															</div>
														</div>
													</div>
											
												@endforeach
											</div>
										</div>	
										
									</div>
								</div>
									
								<div class="col-lg-6 pd-l-5" id="res-two">
									<div class="airlines-list">
										<div class="title-bar">
											<div class="row">
												<div class="col-md-2">
													<div class="bar-heading">
														<h5>Airline</h5>
													</div>
												</div>
												<div class="col-md-2">
													<div class="bar-heading">
														<h5>Depart</h5>
													</div>
												</div>
												<div class="col-md-2">
													<div class="bar-heading">
														<h5>Arrive</h5>
													</div>
												</div>
												<div class="col-md-2">
													<div class="bar-heading">
														<h5>Duration</h5>
													</div>
												</div>
												<div class="col-md-2">
													<div class="bar-heading">
														<h5>Price</h5>
													</div>
												</div>
												<div class="col-md-2 res-none">
													<div class="bar-heading">
														<h5>&nbsp;</h5>
													</div>
												</div>
											</div>
										</div>
										<div class="showtable">
											<div class="newtable"  id="listflight" >
												@foreach($array as $arrays1)
												<?php
												//depature time
												$devdate = explode('T',$arrays1['Segments'][0][0]['Origin']['DepTime']);
												$depnewdate= date("d-M-y" , strtotime($devdate[0]));
												$depnewtime = date("h:i" , strtotime($devdate[1]));
												$newdepnewdate= date("Y-m-d" , strtotime($devdate[0]));
												
												//arrival time
												$arvdate = explode('T',$arrays1['Segments'][0][0]['Destination']['ArrTime']);
												$arvnewdate= date("d-M-y" , strtotime($arvdate[0]));
												$arvtime = date("h:i" , strtotime($arvdate[1]));
												$newarvnewdate= date("Y-m-d" , strtotime($arvdate[0]));
												
												$ts1 = strtotime($depnewtime.''.$newdepnewdate);
												$ts2 = strtotime($newarvnewdate.''.$arvtime);
												$diff = abs($ts1 - $ts2) / 3600;
												$lastvc =  number_format((float)$diff, 2, '.', '');
												$new=explode('.',$lastvc);
												$newtime=$new[0].'H :'.$new[1].'M';
												

												 $imgsou =  'assets/images/flag/'.$arrays1['Segments'][0][0]['Airline']['AirlineCode'].'.gif';
												$stopcount = count($arrays1['Segments'][0]);
												$uniq = $arrays1['Segments'][0][0]['Airline']['AirlineName'];
												
												?>
												
													<div class="flight-list listflight">
														<div class="row">
															<div class="col-md-2">
																<div class="flight-name">
																	<img src="{{$imgsou}}" class="img-responsive flight-logo">
																	<p class="flight-name-heading">{{$arrays1['Segments'][0][0]['Airline']['AirlineName']}}</p>
																	<p>{{$arrays1['Segments'][0][0]['Airline']['AirlineCode']}} - {{$arrays1['Segments'][0][0]['Airline']['FlightNumber']}}</p>
																</div>
															</div>
															<div class="col-md-2">
																<div class="flight-depart">
																	<p class="flight-depart-heading">{{$depnewtime}} | {{$depnewdate}} </p>
																	<?php
																	for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
																	{
																		echo $flightroute = '<p class="flight-depart-text">'.$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode'].' <i class="fa fa-plane fa-rotate-45"></i>    '.$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'].'</p>';
																	}
																	?>
																</div>
															</div>
															<div class="col-md-2">
																<div class="flight-arrival">
																	<p class="flight-arrival-heading">{{$arvtime}} | {{$arvnewdate}}</p>
																</div>
															</div>
															<div class="col-md-2">
																<div class="flight-duration">
																	<p class="flight-duration-heading">{{$newtime}}</p>
																	<p>{{$stopcount}} Stop</p>
																</div>
															</div>
															<div class="col-md-2">
																<div class="flight-price">
																	<p class="flight-price-heading"><i class="fa fa-inr"></i> {{$arrays1['Fare']['PublishedFare']}}</p>
																	<p>2 Seats Left</p>
																</div>
															</div>
															<div class="col-md-2">
																<div class="flight-book">
																	<p><a href="#" class="btn flight-book-btn">Book</a></p>
																	<a href="javascript:void()" class="details">Details</a>
																</div>
															</div>
														</div>
														<div class="flight-fare">
															<div class="row">
																<div class="col-md-4">
																	<h5 class="fare-rules">Fare Rules</h5>
																</div>
																<div class="col-md-4">
																	<!-- <h5 class="airline-mark">Airline Mark : <span> {{$arrays1['AirlineRemark']}}</span></h5> -->
																</div>
																<div class="col-md-4">
																	@if($arrays1['IsRefundable'])
																	<h5 class="fare-type refundable"><i class="fa fa-money"></i> Refundable</h5>
																	@else
																	<h5 class="fare-type non-refundable"><i class="fa fa-money"></i> Non-Refundable</h5>
																	@endif

																</div>
															</div>
														</div>
														
														
														
														<div class="flight-details-tabs">
															<ul class="nav nav-tabs">
																<li><a data-toggle="tab" href="#home" class="active">Home</a></li>
																<li><a data-toggle="tab" href="#menu1">Menu 1</a></li>
																<li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
																<li><a data-toggle="tab" href="#menu3">Menu 3</a></li>
															</ul>

															<div class="tab-content">
																<div id="home" class="tab-pane fade in active">
																  1
																</div>
																<div id="menu1" class="tab-pane fade">
																  2
																</div>
																<div id="menu2" class="tab-pane fade">
																  3
																</div>
																<div id="menu3" class="tab-pane fade">
																  4
																</div>
															</div>
														</div>
													</div>
											
												@endforeach
											</div>
										</div>	
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3" id="res-one">
						<div class="flight-filter">
							<h3>Sort & Filter</h3>
							<div class="filter">
								<h4><i class="fa fa-inr"></i>&nbsp; Price</h4>
								<div class="inner-filter">
									<div class="range-slider">
										<input type="text" class="js-range-slider" value="" />
									</div>
								</div>
							</div>
							<div class="filter">
								<h4><i class="fa fa-clock-o"></i>&nbsp; Time</h4>
								<div class="inner-filter">
									<div id="time-range">
										<p><span class="slider-time" style="float:left;">9:00 AM</span><span class="slider-time2" style="float:right;">5:00 PM</span>

										</p>
										<div class="sliders_step1">
											<div id="slider-range"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="filter">
								<h4><i class="fa fa-map-marker"></i>&nbsp; Fare Type</h4>
								<div class="inner-filter">
									<label class="checkcontainer">Non-Refundable
										<input type="checkbox" class="refundable" id="nonrefundableflight" value="0" name="refundableflight[]">
										<span class="checkmark"></span>
									</label>
									<label class="checkcontainer">Refundable
										<input type="checkbox" class="refundable" id="refundableflight" value="1" name="refundableflight[]">
										<span class="checkmark"></span>
									</label>
								</div>
							</div>
							<div class="filter">
								<h4><i class="fa fa-map-marker"></i>&nbsp; Stop</h4>
								<div class="inner-filter">
									<label class="checkcontainer">Non Stop
										<input type="checkbox" class="stopflight" id="nonstopflight" value="1" name="stopflight[]">
										<span class="checkmark"></span>
									</label>
									<label class="checkcontainer">Multi Stop
										<input type="checkbox" class="stopflight" id="stopflight" value="2" name="stopflight[]">
										<span class="checkmark"></span>
									</label>
								</div>
							</div>
							<div class="filter">
								
								<h4><i class="fa fa-plane"></i>&nbsp; Airlines</h4>
								<div class="inner-filter">
									
									@foreach ($unique as $flightsearch) 
									
									<label class="checkcontainer">{{$flightsearch['name']}}
										<input type="checkbox" class="flightsearch" name="flightsearch[]" value="{{$flightsearch['code']}}">
										<span class="checkmark"></span>
									</label>
									@endforeach
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		@else
	<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="intro_content nano-content">
								<div class="no-flight">
									<h3>No Flight Found</h3>
									<a href="{{url('/index')}}" class="btn btn-back">Go Back</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endif
	</div>
</div>
	<!-- Footer -->
	@include('pages.include.footer')
	<!-- Copyright -->
	@include('pages.include.copyright')
	<script>
		//refunable
		$(document).on('click','.refundable',function(){
		 var refunable = $('.refundable:checked').map(function () {
	        return this.value
	     }).get().join(',');
	     var checklength = $(".refundable:checked").length;
	    
	   	
	   		 $.ajax({
		    		url: "{{route('checkrefunable')}}",
		    		data : {'refunable' : refunable,
		    				'checklength' : checklength,
		    				},
		    		type :'GET',
		    		success : function(data)
		    		{
		    			$('.showtable').html(data);
		    			$('#listflight').hide();
		    			

		    		}
		    });
	   });
		//stopflight
		$(document).on('click','.stopflight',function(){
		 var stopflight = $('.stopflight:checked').map(function () {
	        return this.value
	     }).get().join(',');
	     var checkstoplength = $(".stopflight:checked").length;

	     $.ajax({
		    		url: "{{route('checkstopflight')}}",
		    		data : {'stopflight' : stopflight,
		    				'checkstoplength' : checkstoplength,
		    				},
		    		type :'GET',
		    		success : function(data)
		    		{
		    			
		    			$('.showtable').html(data);
		    			$('#listflight').hide();
		    			

		    		}
		    });
	    });
	    // flightsearch
	    $(document).on('click','.flightsearch',function(){
	    var flightsearch = $('.flightsearch:checked').map(function () {
	        return this.value
	     }).get().join(',');
	     var flightsearchlength = $(".flightsearch:checked").length;
	     
	     $.ajax({
		    		url: "{{route('checkflightcodesearch')}}",
		    		data : {'flightsearch' : flightsearch,
		    				'flightsearchlength' : flightsearchlength,
		    				},
		    		type :'GET',
		    		success : function(data)
		    		{
		    			
		    			$('.showtable').html(data);
		    			$('#listflight').hide();
		    			

		    		}
		    });

	    });
	</script>
	<script>

		 $(".js-range-slider").ionRangeSlider({
        type: "double",
        min: 0,
        max: 10000,
        from: 1000,
        to: 8000,
        onChange: function (data) {
            console.dir(data);
        }
    });
	</script>
	<script>
		$(document).ready(function(){
		  $(".flight-book .details").click(function(){
		    $(".flight-details-tabs").toggle();
		  });
		  $(".btn-modify").click(function(){
		    $(".result-modify").toggle();
		  });
		});
</script>
</body>
</html>