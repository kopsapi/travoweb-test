<style>
        body{
            font-family: 'Open Sans', sans-serif;
        }
        .h-travel p {
            color: #333;
            font-size: 15px;
        }

        .t-addr {
            font-weight: 600;
            color: black;
            font-size: 17px;
        }
        p.tell-us {
            text-align: center;
            font-size: 16px;
            color: black;
            margin-top: -9px;
        }
        p.h-care {
            text-align: center;
            color: black;
            font-size: 22px;
        }
        td.emty {
            padding: 0;
        }
        p.t-policy1 {
            margin-top: -20px;
            margin-left: 25px;
        }
        p.h-heading {
            margin-bottom: 15px;
            font-size: 18px;
            color: black;
            font-weight: 600;
        }table.h-table {
             border-collapse: collapse;
         }

        .t-policy, .t-policy1 {
            padding: 10px;
            color: #4e4e4e;
            font-weight: 600;
            position: relative;
            font-size: 14px !important;
            padding-left: 15px;
        }
        .t-policy:before,.t-policy1:before {
            position: absolute;
            content: "";
            width: 6px;
            height: 6px;
            background-color: #fa9e1b;
            left: 0;
            margin-right: 0px;
            top: 50%;
            border-radius: 12px;
            transform: translateY(-50%);
        }
        .caret-img{
            width: 9px;
            margin-right: 3px;
        }

        td.h-foot p {
            color: black;
            text-align: center;
            margin-bottom: 0;
        }
        .super_container {
            width: 100%;
            overflow: hidden;
            background: white;
        }

        p.h-free {
            margin-bottom: 0;
            font-size: 17px;
            font-weight: 600;
            color: black;
        }
        .table th, .table td {
            padding: 17px;
            vertical-align: top;
            border: 1px solid #8c8f91;
        }
        .table th {
            text-align: left;
        }
        .table td{
            text-align: right;
        }
        .table th {
            display: table-cell;
            vertical-align: inherit;
            text-align: left;

        }
        .table {
            width: 100%;
            margin-bottom: 1rem;
            font-size: 13px;
            color: #212529;
        }


        table.table {
            border-collapse: separate;;

        }

        p.h-time {
            margin-left: 20px;
            color: black;
            font-weight: 400 !important;
        }

        td.c-in p>img.c-icon {
            width: 25px;
            /* margin-top: 0px !important; */
            margin-bottom: -5px;
        }
        td.c-in p {
            margin-bottom: 0;
            font-size: 17px;
            font-weight: 700;
            text-align: left !important;
        }

        p.h-name {
            font-size: 25px;
            font-weight: 600;
            margin-bottom: 0;
        }
        p.h-addr, p.h-phn {
            font-size: 17px;
            font-weight: 500;
            margin-bottom: 0;
        }
        .c-icon{
            width: 25px;
            margin-top: 7px;

        }
        p.guest>span {
            font-size: 17px;
            font-weight: 600
        }
        p.guest {
            color: black;
            font-size: 18px;
            font-weight: 600;
            margin: 5px 0;
        }
        td {
            margin: 16px;
            padding: 10px 0;
        }
        .hr-style {
            border: none;
            border-bottom: 1px solid #fa9e1b;
            padding: 13px 0px;
        }
        .hr-style1 {
            border: none;
            border-bottom: 1px solid #fa9e1b;
            padding: 0px 0px;
        }
        table {
            width: 80%;
            margin-left: auto;
            margin-right: auto;
        }
        p.h-confirm {
            color: black;
        }img.m_-6025389592419173505c-icon.CToWUd {
             margin-bottom: -8px;
         }

        .v-icon {
            width: 48px;
            /* margin-top: -4px; */
            margin-bottom: -15px;
            text-align: right;
            margin-right: -4px;
        }
        .h-confirm {
            font-size: 25px;
            margin-bottom: 0;
        }
        .l-icon {
            width: 180px;
            padding: 20px 0;
        }
    </style>
<body>
    <?php
    $hotelbookdetailsarray=session()->get('hotelbookdetailsarray1');
   
    ?>
<div class="super_container">
    <table class="h-table" style="font-size: 16px;">
        <tr class="hr-style">
            <td  style="border: none;border-bottom: 1px solid #fa9e1b;padding: 13px 0px;"><img src="{{asset('assets/images/logo.png')}}" class="l-icon" style="width: 170px"></td>
            <td colspan="2"  style="border: none;border-bottom: 1px solid #fa9e1b;padding: 13px 0px;"> <p class="h-confirm" style="text-align:right;">
                    <img src="{{asset('assets/images/icon-tick-28.jpg')}}" style="width: 25px; height: 25px ; margin-bottom: -10px" class="v-icon">
                    <span style="font-size: 28px">Confirmation Voucher</span></p></td>

        </tr>

        <tr class="hr-style" >
            <td  style="border: none;border-bottom: 1px solid #fa9e1b;padding: 0px 0px;" colspan="3">
                <p class="guest"><span style="color: red;">Guest Name: </span>{{ucfirst($hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['FirstName'])}} {{ucfirst($hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['LastName'])}}</p></td>
        </tr>

        <tr>
            <td>

                <p style="color: #fa9e1b;font-size: 20px" class="h-name">{{$hotelbookdetailsarray['HotelName']}}</p>
                <p style="color: #333" class="h-addr">
                    {{$hotelbookdetailsarray['AddressLine1']}}

                </p>
                <p style="color:#333;" class="h-phn"><b>{{$hotelbookdetailsarray['AddressLine2']}} </p>
            </td>

            <td class="">

                <img src="{{asset('assets/images/t-clock.jpg')}}" style="width: 19px; height: 19px; margin-bottom:-10px;" class="">
                <span style="color: blue;font-size: 21px"> Check in</span>
                <p class="h-time"><?php 
                    $getcheckindatetime=explode('T',$hotelbookdetailsarray['CheckInDate']);

                    $getcheckindate=date('F d,Y',strtotime($getcheckindatetime[0]));
                    echo $getcheckindate;
                    ?></p>
            </td>

            <td class="">
                <img src="{{asset('assets/images/t-clock.jpg')}}" style="width: 19px; height: 19px; margin-bottom:-10px;" class="">
                <span style="color: blue;font-size: 21px"> Check out</span>
                <p class=""><?php 
                        $getcheckindatetime=explode('T',$hotelbookdetailsarray['CheckOutDate']);

                        $getcheckindate=date('F d,Y',strtotime($getcheckindatetime[0]));
                        echo $getcheckindate;
                        ?></p>
            </td>
        </tr>
        
        <tr class="hr-style">
            <td colspan="3">
                <table  class="table" border="" style="width: 100%; border-collapse: collapse">
                <tbody style="text-align:left;">
                    <tr>
                        <td>
                            <table  class="table" border="" style="width: 100%; border-collapse: collapse">
                                <?php
                               
                                $bookingdate=explode('T',$hotelbookdetailsarray['BookingDate']);
                                $bookdate=date('F d,Y',strtotime($bookingdate[0]));
                                $booktime=date('h:i a',strtotime($bookingdate[1]));

                                $amenties="";
                                for($inclusion=0;$inclusion< count($hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion']);$inclusion++)
                                {

                                    $amenties.=$hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion'][$inclusion];
                                    if($inclusion<count($hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion'])-1)
                                    {
                                        $amenties.=" , ";
                                    }
                                } 
                                ?>
                                <tbody style="text-align:left;">
                                <tr>
                                    <th style="padding: 10px; font-size: 15px;">Booking ID:</th>
                                    <td style="padding: 10px; font-size: 15px;" class="hr-style">{{$hotelbookdetailsarray['BookingId']}}</td>
                                </tr>
                                <tr>
                                    <th style="padding: 10px; font-size: 15px;">Hotel Booking ID:</th>
                                    <td style="padding: 10px; font-size: 15px;" class="hr-style">{{$hotelbookdetailsarray['BookingId']}}</td>
                                </tr>
                                <!-- <tr>
                                    <th style="padding: 10px; font-size: 15px;">Advance Receipt Number:</th>
                                    <td style="padding: 10px; font-size: 15px;" class="hr-style">HDAR000028526560</td>
                                </tr> -->
                                <tr>
                                    <th style="padding: 10px; font-size: 15px;">Date of Booking:</th>
                                    <td style="padding: 10px; font-size: 15px;" class="hr-style">{{$bookdate}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                        <td style="vertical-align: baseline">
                            <table  class="table" border="" style="width: 100%; border-collapse: collapse;">
                                
                                <tbody style="text-align:left;">
                                <tr style="vertical-align:baseline">
                                    <th style="padding: 10px; font-size: 15px;">Room Type:</th>
                                    <td style="padding: 10px; font-size: 15px;" class="hr-style">{{$hotelbookdetailsarray['HotelRoomsDetails'][0]['RoomTypeName']}} {{$amenties}}</td>
                                </tr>
                                @for($rooms=0;$rooms< count($hotelbookdetailsarray['HotelRoomsDetails']);$rooms++)
                               <?php
                               $adultcount=0;
                               $childcount=0;
                               $childage1='';
                               $childval='';
                               $childnew='';
                               ?> 
                                <tr>
                                    <th style="padding: 10px; font-size: 15px;">Room {{$rooms+1}}</th>

                                    <td style="padding: 10px; font-size: 15px;" class="hr-style">
                                         @for($passenger=0;$passenger< count($hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger']);$passenger++)
                                        <?php 
                                        $childage = $hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['Age'];
                                       
                                         if($hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['PaxType']==1)
                                           $adultcount++;
                                       else
                                         $childcount++;
                                        $childval='Child';
                                        if($hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['PaxType']==2)
                                        {
                                            $childage1.=$childage.",";
                                            $childnew = "[".$childage1."]";
                                        }
                                        else
                                        {
                                            $childage1='';
                                        }
                                        
                                        
                                     ?>
                                     @endfor
                                        {{$adultcount}} Adults / {{$childcount}} {{$childval}} {{$childnew}} </td>
                                </tr> 
                                 @endfor
                                    
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
                </table>
                
                
            </td>
        </tr>
        <tr class="hr-style">
            <td  style="border: none;border-bottom: 1px solid #fa9e1b;padding: 13px 0px;" colspan="3">
                <p class="h-free"><span style="color: #189ad4;font-size: 17px;
    font-weight: 600;">Inclusions:</span> FREE 
            @for($inclusion=0;$inclusion< count($hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion']);$inclusion++)
            {{$hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion'][$inclusion]}}
             
            @endfor</p></td>
        </tr>

        <tr class="hr-style">
             <?php
                $RoomPrice=0;
                $Tax=0;
                $ExtraGuestCharge=0;
                $ChildCharge=0;
                $OtherCharges=0;
                $AgentCommission=0;
                $AgentMarkUp=0;
                $PublishedPrice=0;
                $ServiceTax=0;
                $TDS=0;
                $TotalGSTAmount=0;
                $total_fare=0;
                $gst=0;
                $total_gst=0;
                $grand_total=0;
                $mainothercharges=0;
                for($i=0;$i<count($hotelbookdetailsarray['HotelRoomsDetails']);$i++)
                {
                    $RoomPrice+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['RoomPrice'];
                    $Tax+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['Tax'];
                    $ExtraGuestCharge+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['ExtraGuestCharge'];
                    $ChildCharge+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['ChildCharge'];

                    $OtherCharges+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['OtherCharges'];
                    $AgentCommission+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['AgentCommission'];
                    $AgentMarkUp+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['AgentMarkUp'];
                    $PublishedPrice+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['PublishedPrice'];
                    $ServiceTax+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['ServiceTax'];
                    $TDS+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['TDS'];
                    $TotalGSTAmount+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['TotalGSTAmount'];

                    $total_fare=$RoomPrice+$Tax+$ExtraGuestCharge+$ChildCharge+$OtherCharges+$AgentCommission
                    + $AgentMarkUp+$ServiceTax+$TDS+$TotalGSTAmount;
                    $mainothercharges= $ExtraGuestCharge+$ChildCharge+$OtherCharges+$AgentCommission
                    + $AgentMarkUp+$ServiceTax+$TDS+$TotalGSTAmount;
                    $gst=($total_fare*5)/100;
                    $total_gst=round($gst,2);

                    $grand_total=$total_fare+$total_gst;

                }
                if($hotelbookdetailsarray['hotelfaicon']=='fa-inr')
                {
                    $curr="Rs.";
                }
                else
                {
                    $curr="$";
                }
                ?>
            <td colspan="3"  style="border: none;border-bottom: 1px solid #fa9e1b;padding: 13px 0px;">
                <table  class="table" border="" style="width: 100%; border-collapse: collapse">
                    <tbody style="text-align:left;">
                    <tr>
                        <th style="color: #fa9e1b; padding: 10px; font-size: 15px;">Description</th>
                        <td style="color: #fa9e1b; padding: 10px; font-size: 15px;">Amount</td>
                    </tr>
                    <tr>
                        <th style="padding: 10px; font-size: 15px;">Room Charges Collected on Behalf of Hotel</th>
                        <td style="padding: 10px; font-size: 15px;" class="hr-style">{{$curr}} {{$hotelbookdetailsarray['hotelmarginprice']}}</td>
                    </tr>
                    <tr>
                        <th style="padding: 10px; font-size: 15px;">Tax </th>
                        <td style="padding: 10px; font-size: 15px;" class="hr-style">{{$curr}} {{$hotelbookdetailsarray['hotelservice_tax']}}</td>
                    </tr>
                    

                    <tr>
                        <th style="padding: 10px; font-size: 15px;">GST on Convenience Fees</th>
                        <td style="padding: 10px; font-size: 15px;" class="hr-style">{{$curr}} {{$hotelbookdetailsarray['hotelmargingst']}}
                        </td>
                    </tr>

                    <tr>
                        <th style="padding: 10px; font-size: 15px;">Net Amount Paid</th>
                        <td style="padding: 10px; font-size: 15px;" class="hr-style">{{$curr}} {{$hotelbookdetailsarray['hotelamount']}}</td>
                    </tr>


                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="emty">&nbsp;</td>
        </tr>

        <tr>
            <td colspan="3" class="alert1" style="color: #721c24;background-color: #f8d7da; border-color: #f5c6cb; padding: 0.75rem 1.25rem; margin-bottom: 1rem; border: 1px solid #f5c6cb; border-radius: 0.25rem; font-size: 14px;">
                <img src="{{asset('assets/images/c-r.png')}}" class="caret-img" style="width: 10px;margin-bottom: -2px;">
                Important Note: Booked & Payable at travoweb.com</td>
        </tr>
        <tr>
            <td colspan="3" class="emty">&nbsp;</td>
        </tr>

        <tr class="">
            <td class="alert2" colspan="3" style=" color: #155724; background-color: #d4edda; border-color: #c3e6cb; padding: 0.75rem 1.25rem; margin-bottom: 1rem; border: 1px solid  #c3e6cb; border-radius: 0.25rem; font-size: 14px;">
                <img src="{{asset('assets/images/c-r1.png')}}" class="caret-img" style="width: 10px;
    margin-bottom: -2px;"
                > Description of Service: Reservation services for accommodation</td>
        </tr>
        <tr>
            <td colspan="3" style=" border: none;border-bottom: 1px solid #fa9e1b;padding: 0px 0px;">&nbsp;</td>
        </tr>

        <tr class="hr-style">
            <td colspan="3">
                <p class="t-policy"></td>

        </tr>

        <tr class="hr-style">
        <tr>
            <td colspan="3" class="" style="text-align: left"><p class="h-heading">CANCELLATION POLICY</p></td>
        </tr>
        <tr>
            <td colspan="3">
                    <ul style="margin: 0">
                        <?php
                              $cancellationpolicy=array();
                              for($cancel=0;$cancel<count($hotelbookdetailsarray['HotelRoomsDetails'][0]['CancellationPolicies']);$cancel++)
                              {
                               $cancellationpolicy=$hotelbookdetailsarray['HotelRoomsDetails'][0]['CancellationPolicies'];
                               $fromdatetime=explode('T',$cancellationpolicy[$cancel]['FromDate']);
                               $fromdate=date('d-F-Y',strtotime($fromdatetime[0]));
                               $fromtime=date('h:i a',strtotime($fromdatetime[1]));

                               $todatetime=explode('T',$cancellationpolicy[$cancel]['ToDate']);
                               $todate=date('d-F-Y',strtotime($todatetime[0]));
                               $totime=date('h:i a',strtotime($todatetime[1]));

                               echo '<li class="t-policy"> '.$cancellationpolicy[$cancel]['Charge'].'%'.' of total amount will be charged , If cancelled between '.$fromdate.' '.$fromtime.' and '.$todate.' '.$totime.'</li>';

                           }
                        ?>
                    </ul>
            </td>

        </tr>
         <tr>
            <td colspan="3" class="" style="text-align: left"><p class="h-heading">HOTEL POLICY</p></td>
        </tr>
        <tr>
            <td colspan="3" >
                <ul style="margin: 0">
                    <?php
                          $cancellationpolicy=explode('|',$hotelbookdetailsarray['HotelPolicyDetail']);
                          for($policy=0;$policy<count($cancellationpolicy);$policy++)
                          {
                            if($policy<=1)
                            {
                                continue;
                            }
                            if($cancellationpolicy[$policy]!='')
                                echo ' <li class="t-policy">'.$cancellationpolicy[$policy]."</li>";

                        }
                    ?>
                </ul>
            </td>
        </tr>
      

        

        <tr class="hr-style">
            <td colspan="3" class="hr-style" style="text-align:center;">
                <p class="h-care"><img src="{{asset('assets/images/24-7-icon.png')}}" class="c-icon" style="width: 30px"> Care Support -<span style="font-size: 15px; font-weight: normal; color: #333">It is FASTER to WRITE to US.<p></p></p>

                <p class="tell-us">To tell us your issue go to <a href="#"> www.travoweb.com/support/</a></p>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="emty">&nbsp;</td>
        </tr>

        <tr>
            <td colspan="3" style="color: #721c24;background-color: #f8d7da; border-color: #f5c6cb; padding: 0.75rem 1.25rem; margin-bottom: 1rem; border: 1px solid #f5c6cb; border-radius: 0.25rem; font-size: 14px;">Disclaimer:- Hotel and GST charges are collected on behalf of the hotel.
                These details are just for information and cannot be used for GST claim.</td>
        </tr>
        <tr>
            <td colspan="3" style=" border: none;border-bottom: 1px solid #fa9e1b;padding: 0px 0px;">&nbsp;</td>
        </tr>



        <tr>
            <td colspan="3" style=" border: none;border-bottom: 1px solid #fa9e1b;padding: 0px 0px;">&nbsp;</td>
        </tr>

    </table>



</div>

</body>



</html>