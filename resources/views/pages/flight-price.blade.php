@include('pages.include.header')
<body>
<style>
.flight-book-list .flight-depart .fa-plane
{
	color: #fa9e1b;
    font-size: 16px;
}
.flight-book-list .flight-arrival .fa-plane
{
	color: #fa9e1b;
    font-size: 16px;
}
</style>
    <div class="super_container">
        <!-- Header -->@include('pages.include.topheader')
        <div class="home" style="height:20vh;">
            <!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->
            <div class="home_content">
                <div class="home_title">Results </div>
            </div>
        </div>
        <!-- Intro -->
		<div class="flight-book">
			<div class="intro">
				<section class="flight-book-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="intro_content">
									<div class="row">
										<div class="col-lg-2 res-mb-20">
											<h4><i class="fa fa-map-marker"></i> Amritsar</h4>
										</div>
										<div class="col-lg-1 res-mb-20">
											<i class="fa fa-plane flight-i fa-rotate-45"></i>
										</div>
										<div class="col-lg-2 res-mb-20">
											<h4><i class="fa fa-map-marker"></i> Delhi</h4>
										</div>
										<div class="col-lg-2 res-mb-20">
											<h4><i class="fa fa-calendar"></i> 30 Feb 2019 </h4>
										</div>
										<!-- <div class="col-lg-2 res-mb-20">
											<h4><i class="fa fa-calendar"></i> 31 Feb 2019</h4>
										</div>
										<div class="col-lg-2 res-mb-20">
											<h4 class="flight-class"><small>ADULTS</small> 2 <small>CHILD</small> 0 </h4>
										</div> -->
										<div class="col-lg-3 res-mb-20 res-none">
											&nbsp;
										</div>

										<div class="col-lg-2">
											<button type="button" class="btn btn-refundable">Refundable</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				
				<section class="flight-book-details">
					<div class="container">
						<div class="row">
							<div class="col-lg-8">
								<div class="title-book-bar">
									<div class="row">
										<div class="col-md-2">
											<div class="bar-heading">
												<h5>Airline</h5>
											</div>
										</div>
										<div class="col-md-2">
											<div class="bar-heading">
												<h5>Depart</h5>
											</div>
										</div>
										<div class="col-md-2">
											<div class="bar-heading">
												<h5>Arrive</h5>
											</div>
										</div>
										<div class="col-md-2">
											<div class="bar-heading">
												<h5>Fare Type</h5>
											</div>
										</div>
										<div class="col-md-2">
											<div class="bar-heading">
												<h5>Stops</h5>
											</div>
										</div>
										<div class="col-md-2">
											<div class="bar-heading">
												<h5>Duration</h5>
											</div>
										</div>
									</div>
								</div>
								
								<div class="flight-book-list">
									<div class="row flight-border">
										<div class="col-md-2">
											<div class="flight-name">
												<img src="http://travoweb.com/apiflight/assets/images/test_1.jpg" class="img-responsive flight-logo">
												<p class="flight-name-heading">Indigo</p>
												<p>6E - 2895 L</p>
											</div>
										</div>
										<div class="col-md-2">
											<div class="flight-depart">
												<p class="flight-depart-heading">22:00 | 11-Jun-19</p>
												<p class="flight-depart-text">ATQ <i class="fa fa-plane fa-rotate-45"></i>&nbsp; DEL</p>
											</div>
										</div>
										<div class="col-md-2">
											<div class="flight-arrival">
												<p class="flight-arrival-heading">22:00 | 11-Jun-19</p>
												<p class="flight-arrival-text">ATQ <i class="fa fa-plane fa-rotate-45"></i>&nbsp; DEL</p>
											</div>
										</div>
										<div class="col-md-2">
											<div class="flight-depart">
												<p class="flight-depart-text">Refundable</p>
											</div>
										</div>
										<div class="col-md-2">
											<div class="flight-arrival">
												<p style="color:#008cff">Demo</p>
											</div>
										</div>
										<div class="col-md-2">
											<div class="flight-duration">
												<p class="flight-duration-heading">00H : 50M</p>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<div class="flight-name">
												<img src="http://travoweb.com/apiflight/assets/images/test_1.jpg" class="img-responsive flight-logo">
												<p class="flight-name-heading">Indigo</p>
												<p>6E - 2895 L</p>
											</div>
										</div>
										<div class="col-md-2">
											<div class="flight-depart">
												<p class="flight-depart-heading">22:00 | 11-Jun-19</p>
												<p class="flight-depart-text">ATQ <i class="fa fa-plane fa-rotate-45"></i>&nbsp; DEL</p>
											</div>
										</div>
										<div class="col-md-2">
											<div class="flight-arrival">
												<p class="flight-arrival-heading">22:00 | 11-Jun-19</p>
												<p class="flight-arrival-text">ATQ <i class="fa fa-plane fa-rotate-45"></i>&nbsp; DEL</p>
											</div>
										</div>
										<div class="col-md-6">
											&nbsp;
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
										</div>
									</div>
								</div>
								
								
								<div class="flight-traveller">
									<div class="container">
										<div class="row">
											<div class="adult-details">
												<h4>Adult</h4>
												<div class="p-10">
													<table class="table table-bordered">
														<thead>
															<tr>
																<th>Name</th>
																<th>Email</th>
																<th>Phone</th>
																<th>Country</th>
																<th>Address</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>Gaurav</td>
																<td>dmeo@gmail.com</td>
																<td>+91-12345-67890</td>
																<td>India</td>
																<td>Test</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								
								
								
								<div class="flight-traveller">
									<div class="container">
										<div class="row">
											<div class="col-lg-12">
												<form action="#" id="contact_form" class="contact_form">
													<p>Name should be same as in Government ID proof.</p>
													<h4>Adult 1</h4>
													<div class="row mb-20">
														<div class="col-md-4">
															<select class="input_field" placeholder="Name" name="title[]">
																<option>Title</option>
																<option>Mr.</option>
																<option>Ms.</option>
																<option>Mrs.</option>
															</select>
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field" placeholder="First and Middle Name" name="firstname[]">
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field" placeholder="Last Name" name="lastname[]">
														</div>
													</div>	
													<h4>Child 1</h4>
													<div class="row mb-20">
														<div class="col-md-4">
															<select class="input_field" placeholder="Name" name="childtitle[]">
																<option>Title</option>
																<option>Ms.</option>
																<option>Mstr.</option>
															</select>
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field" placeholder="First and Middle Name" name="childfirstname[]">
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field" placeholder="Last Name" name="childlastname[]">
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field" placeholder="Date of Birth" name="childdob[]">
														</div>
													</div>
													<h4>Infant 1</h4>
													<div class="row mb-20">
														<div class="col-md-4">
															<select class="input_field" placeholder="Name" name="infatitle[]">
																<option>Title</option>
																<option>Ms.</option>
																<option>Mstr.</option>
															</select>
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field" placeholder="First and Middle Name" name="infafistname[]">
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field" placeholder="Last Name" name="infalastname[]">
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field" placeholder="Date of Birth" name="infadob[]">
														</div>
													</div>
													<h4>Contact Information</h4>
													<div class="row mb-20">
														<div class="col-md-4">
															<input type="text" class="input_field" placeholder="Email">
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field" placeholder="Phone">
														</div>
													</div>
													<hr>
													<div class="gst-checkbox mb-20">
														<h4 style="display:inline-block">Enter GST details to claim benefits (optional) &nbsp;
														<label class="checkcontainer" style="display:inline-block; margin-bottom:17px;">
															<input type="checkbox" name="chkgst" id="chkgst" value="1" class="chkgst"> <span class="checkmark"></span>
														</label></h4>
														<p>Travelling for business purpose? Please enter Company GST details below.</p>
													</div>
													
													<div class="gst-details" id="showgst" style="display:none">
														<div class="row mb-20">
															<div class="col-md-4">
																<input type="text" class="input_field" placeholder="Company Address">
															</div>
															<div class="col-md-4">
																<input type="text" class="input_field" placeholder="Company's GST Email">
															</div>
															<div class="col-md-4">
																<input type="text" class="input_field" placeholder="GST Number">
															</div>
															<div class="col-md-4">
																<input type="text" class="input_field" placeholder="Company Name">
															</div>
															<div class="col-md-4">
																<input type="text" class="input_field" placeholder="Company Contact Number">
															</div>
														</div>
													</div>
													<!-- meal -->
													<div class="gst-checkbox mb-20">
														<h4 style="display:inline-block">Meal & Baggages &nbsp;
														<label class="checkcontainer" style="display:inline-block; margin-bottom:17px;">
															<input type="checkbox" name="meal" id="meal" value="1" class="meal"> <span class="checkmark"></span>
														</label></h4>
														
													</div>
													
													<div class="gst-details" id="showmeal" style="display: none" >
														<div class="row mb-20">
															<div class="col-md-4">
																<select class="input_field" placeholder="Name" name="infatitle[]">
																	<option>Demo</option>
																	<option>Demo</option>
																	<option>Demo</option>
																	<option>Demo</option>
																</select>
															</div>
															<div class="col-md-4">
																<select class="input_field" placeholder="Name">
																	<option>Demo</option>
																	<option>Demo</option>
																	<option>Demo</option>
																	<option>Demo</option>
																</select>
															</div>
															<div class="col-md-4">
																<select class="input_field" placeholder="Name">
																	<option>Demo</option>
																	<option>Demo</option>
																	<option>Demo</option>
																	<option>Demo</option>
																</select>
															</div>
														</div>
													</div>
													<!-- end meal -->
													<div class="note">
														<p><strong>Note:</strong> You will receive your ticket with above GST details.</p>
													</div>
													<div class="text-right">
														<button type="submit" id="form_submit_button" class="form_submit_button button trans_200">Continue<span></span><span></span><span></span></button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-lg-4">
								<div class="flight-book-price">
									<h3>Fare Breakup</h3>
									<div class="booking-price">
										<h4 class="traveller">Adult 1 Child 1</h4>
										<div class="inner-box">
											<div class="flight-total-price">
												<div class="inner-div">
													<div class="total-base-price left-price">Total Base Price <span class="base-price right-price"><i class="fa fa-inr"></i> 6,485</span></div>
												</div>
												<div class="inner-div">
													<div class="total-taxes left-price">Total Taxes & Fees <span class="tax right-price"><i class="fa fa-inr"></i>7,142</span></div>
												</div>
												<div class="inner-div air-fare-border">
													<div class="total-airfare left-price">Total Airfare <span class="airfare right-price"><i class="fa fa-inr"></i> 5222</span></div>
												</div>
												<div class="inner-div">
													<div class="travel-insurance left-price">Travel Insurance (1 x Rs.249) <span class="insurance right-price"><i class="fa fa-inr"></i> 249</span></div>
												</div>
												<div class="inner-div">
													<div class="total-convenience-fee left-price">Total Convenience Fee <span class="convenience right-price"><i class="fa fa-inr"></i> 279</span></div>
												</div>
												<div class="inner-div">
													<div class="grand-total left-price">Grand Total <span class="grand right-price"><i class="fa fa-inr"></i>3255</span></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
        </div>
    </div>
    <!-- Footer -->@include('pages.include.footer')
    <!-- Copyright -->@include('pages.include.copyright')
    <script>
        $(document).ready(function() {
            $(".btn-modify").click(function() {
                $(".hotel-result-modify").toggle();
            });
        });
    </script>
    <script>
    

    	$(document).on('click','input[name="chkgst"]',function(){
    		 if($(this).is(":checked")){
    		 	$('#showgst').show();
                
            }
            else if($(this).is(":not(:checked)")){
            	$('#showgst').hide();
               
            }
    	});
    	$(document).on('click','input[name="meal"]',function(){
    		 if($(this).is(":checked")){
    		 	$('#showmeal').show();
                
            }
            else if($(this).is(":not(:checked)")){
            	$('#showmeal').hide();
               
            }
    	})
    </script>
</body>
</html>