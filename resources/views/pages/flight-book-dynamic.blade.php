@include('pages.include.header')
<style>
	@media screen and (max-width: 430px){
		.e-div{
			max-width: 13% !important;
		}
		.flight-book-details .title-book-bar .bar-heading h5 {
			font-size: 12px;
			color: #FFF;
			font-weight: 600;
			text-align: center;
			margin-bottom: 0px;
		}
	}
	@media screen and (max-width: 650px){
		.field-set {

			padding: 10px !important;

		}
		div.gst-div {

			padding: 5px !important;

		}
		p.gst-book {

			font-size: 12px;

		}
		.meal-div{
			margin-bottom: 0px !important;
		}
	}

	.field-set {
		border: 1px solid #d8d8d8;
		padding: 16px 16px 0px;
		border-radius: 5px;
		margin-bottom: 25px;
		background: #f2f2f2;
	}
	div.gst-div {
		border: 1px solid #f7d163;
		background: #fff1de;
		padding: 21px 21px 0px;
		margin-bottom: 30px;
		border-radius: 6px;
	}
	p.gst-book {
		background: #fa9e1b;
		color: white;
		padding: 2px 10px;
		font-size: 15px;
		font-weight: 600;
		border-radius: 4px;
		border-bottom: 2px solid #fa9e1b;
	}
	i.fa.fa-plane.flight-i.fa-rotate-45 {
		color: #FF9800;
	}
	.select-g {

		border-bottom: 1px solid #949090 !important;
		border: 1px solid #949090;
		border-radius: 5px;
		padding: 2px;
		background: #ececec;
		color: black;
		height: 37px;
	}
	.meal-div{
		margin-bottom: 20px;
	}

</style>
<style>
	button.price-cng-btn {
		border: none;
		background: #fa9e1b;
		color: white;
		padding: 6px 18px;
		width: 100%;
		font-size: 18px;
		display: block;
		font-weight: 700;
		border-radius: 5px;
		cursor: pointer;
		transition: all .5s ease;

	}
	button.price-cng-btn:hover{
		background: #00206A;
	}

	.payment-card {
		background: white;
		padding: 20px 45px 30px;
		border: 1px solid #cccccc;
		margin-bottom: 25px;
		margin-top: 45px;
	}

	img.pay-img {

		width: 100%;

		height: auto;

		min-height: 100%;

		object-fit: cover;

		border-top-left-radius: 10px;

		border-bottom-left-radius: 10px;

	}

	.payment-section {

		margin-top: 4%;

		border: 1px solid #bebebe;

		margin-left: 130px;

		margin-right: 130px;

		border-top-left-radius: 10px;

		border-bottom-left-radius: 10px;

		box-shadow: 0 10px 25px 0 rgba(0, 0, 0, 0.4);

	}

	div.bt_title {

		color: black;

		text-align: center;

	}

	.pay-input{

		border-radius: 0;

		margin-bottom: 15px;

	}



	.pay-input:focus{

		border: none;

		border: 1px solid #01729e;

		border-radius: 0;

		box-shadow: none;

	}

	label.pay-label {

		color: #01729e;

		font-weight: 500;

	}

	input.submit.pay-btn {

		border: none;

		border: 1px solid;

		background: #01729e;

		color: white;

		padding: 10px 20px;

		display: block;

		margin: 20px auto 0;

	}



	span.span-1 {

		text-align: left;

		display: block;

		padding: 10px 20px;

		color: #ffc107;

		font-weight: 500;

	}

	span.span-2 {

		text-align: right;

		display: block;

		padding: 10px;

		color: #ffc107;

		font-weight: 500;

	}



	.icon-container {

		margin-bottom: 20px;

		padding: 7px 0;

		font-size: 24px;

	}

	.icon-container i{

		font-size: 30px;

	}

	h3.bt_title {
		text-align: center;
		margin-bottom: 30px;
		color: #fff;
		background: #1a86b0;
		padding: 10px 14px;
		margin-left: auto;
		margin-right: auto;
		border-radius: 5px;
		box-shadow: 0 4px 10px 0 rgba(19, 19, 19, 0.2), 0 4px 20px 0 rgba(0, 0, 0, 0.16);
	}

</style>
<body>
    <div class="super_container">
        <!-- Header -->@include('pages.include.topheader')
        <div class="home" style="height:20vh;">
            <!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->
            <div class="home_content">
                <div class="home_title">Results </div>
            </div>
        </div>
        <?php
        			 $params = array("testmode" => "on",
	    "private_live_key" => "sk_live_xxxxxxxxxxxxxxxxxxxxx",
	    "public_live_key" => "pk_live_xxxxxxxxxxxxxxxxxxxxx",
	    "private_test_key" => "sk_test_yICHd12qJq7YEret7hAxqTWj",
	    "public_test_key" => "pk_test_gdcMtVAnKmNzBAuQpSqVsOtj");


       // echo "<pre>";
       // print_r($meal_return);
       // echo "<hr>";
       // print_r($meal_return);
       // echo "</pre>";


        ?>
        <!-- Intro -->

		<div class="flight-book">
			<div class="intro">
				<section class="flight-book-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="intro_content">
									<div class="row">
										<?php
										$datechk = explode('T',$mainarray['airportorigndepdate']);
										?>
										<div class="col-lg-3 res-mb-20">
											<h4><i class="fa fa-map-marker"></i> {{$mainarray['airportorgincode']}} ({{$mainarray['airportorignname']}})</h4>
										</div>
										<div class="col-lg-1 res-mb-20">
											<i class="fa fa-plane flight-i fa-rotate-45"></i>
										</div>
										<div class="col-lg-3 res-mb-20">
											<h4><i class="fa fa-map-marker"></i> {{$mainarray['airportdename1']}} ({{$mainarray['airportdeptname']}})</h4>
										</div>
										<div class="col-lg-3 res-mb-20">
											<h4><i class="fa fa-calendar"></i> {{date("d M Y ", strtotime($datechk[0]))}} </h4>
										</div>
										<!-- <div class="col-lg-2 res-mb-20">
											<h4><i class="fa fa-calendar"></i> 31 Feb 2019</h4>
										</div>
										<div class="col-lg-2 res-mb-20">
											<h4 class="flight-class"><small>ADULTS</small> 2 <small>CHILD</small> 0 </h4>
										</div> -->


										<div class="col-lg-2">

											@if($resu['IsRefundable'])
											<button type="button" class="btn btn-refundable">Refundable</button>
											@else
											<button type="button" class="btn btn-non-refundable">Non Refundable</button>
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="flight-book-details">
					<div class="container">
						<div class="row">
							<div class="col-lg-8">
								<div class="title-book-bar">
									<div class="row">
										<div class="col-md-2">
											<div class="bar-heading">
												<h5>Airline</h5>
											</div>
										</div>
										<div class="col-md-3">
											<div class="bar-heading">
												<h5>Depart</h5>
											</div>
										</div>
										<div class="col-md-2 e-div">
											&nbsp;
										</div>
										<div class="col-md-3">
											<div class="bar-heading">
												<h5>Arrive</h5>
											</div>
										</div>
										<div class="col-md-2">
											<div class="bar-heading">
												<h5>Duration</h5>
											</div>
										</div>
									</div>
								</div>
								@foreach($resu['Segments'] as $segarray)
								<?php
								// echo  count($segarray);


								?>
								<div class="flight-book-list">
									<?php
									session(['flightorignn'=>$mainarray['airportorgincode'],'flightdepn'=>$mainarray['airportdename1'],"flightnamen"=>$segarray[0]['Airline']['AirlineName']]);
									for($seg=0;$seg<count($segarray);$seg++)
									{
										$imgsou =  'assets/images/flag/'.$segarray[$seg]['Airline']['AirlineCode'].'.gif';
										$arvdate11 = explode('T',$segarray[$seg]['Destination']['ArrTime']);
													$arvtime22 = date("H:s" , strtotime($arvdate11[1]));
													$arvnewdate11= date("d-M" , strtotime($arvdate11[0]));
													$arvtime1 = date("H:i:s" , strtotime($arvdate11[1]));
													$arvnewdate1= date("Y-m-d" , strtotime($arvdate11[0]));
													$devdate1 = explode('T',$segarray[$seg]['Origin']['DepTime']);
													$depnewdatedetail= date("d-M" , strtotime($devdate1[0]));
													$depnewtimedetail = date("H:s" , strtotime($devdate1[1]));
													$newdepnewdate= date("Y-m-d" , strtotime($devdate1[0]));
													$depnewtime = date("H:i:s" , strtotime($devdate1[1]));
													$stopcount = count($segarray);
													if($stopcount=='1')
													 {
														$mainstopflight = 'Non Stop';
													 }
													 else
													 {
														$stopcount1 = $stopcount-1;
														$mainstopflight = $stopcount1.' Stop';
													 }

													 $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newdepnewdate.''. $depnewtime);
													$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $arvnewdate1.''. $arvtime1);
													$totalDuration = $from->diffInSeconds($to);
													 $durationhour =  gmdate('H', $totalDuration);
													 $durationmin =  gmdate('s', $totalDuration);

													$newtime=$durationhour.'H : '.$durationmin.'M';

									?>
									<div class="row">
										<div class="col-md-2">
											<div class="flight-name">
												<img src="{{ $imgsou}}" class="img-responsive flight-logo">
												<p class="flight-name-heading">{{$segarray[$seg]['Airline']['AirlineName']}}</p>
												<p>{{$segarray[$seg]['Airline']['AirlineCode']}} - {{$segarray[$seg]['Airline']['FlightNumber']}}</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="flight-depart">
												<p class="flight-depart-heading">{{$depnewdatedetail}} | {{$depnewtimedetail}} </p>
												<p class="flight-depart-text">{{$segarray[$seg]['Origin']['Airport']['AirportCode']}}</p>
											</div>
										</div>
										<div class="col-md-2">
											<div class="flight-depart">
												<p class="flight-depart-text"><i class="fa fa-plane fa-rotate-45"></i></p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="flight-arrival">
												<p class="flight-arrival-heading">{{$arvnewdate11}} | {{$arvtime22}}</p>
												<p class="flight-arrival-text">{{$segarray[$seg]['Destination']['Airport']['AirportCode']}}</p>
											</div>
										</div>
										<div class="col-md-2">
											<div class="flight-duration">
												<p class="flight-duration-heading">{{$newtime}}</p>
												<p style="color:#008cff">{{$mainstopflight}}</p>
											</div>
										</div>
									</div>
								<?php } ?>
								<div class="row">
									<div class="col-md-10">
									</div>
								</div>
									<div class="flight-fare">
										<div class="row">
											<div class="col-md-12">
												<h5 class="fare-rules">Tip : <span>Airlines cancellation charges do apply. Please check cancellation and baggage policies for more details.</span></h5>
											</div>
										</div>
									</div>
								</div>
								@endforeach

								<!-- inbound start -->
								<?php
								if(!empty($resu_return))
								{
								?>
									@foreach($resu_return['Segments'] as $segarray)
									<?php
									// echo  count($segarray);
									?>
									<div class="flight-book-list">
										<?php

										for($seg=0;$seg<count($segarray);$seg++)
										{
											$imgsou =  'assets/images/flag/'.$segarray[$seg]['Airline']['AirlineCode'].'.gif';
											$arvdate11 = explode('T',$segarray[$seg]['Destination']['ArrTime']);
														$arvtime22 = date("H:s" , strtotime($arvdate11[1]));
														$arvnewdate11= date("d-M" , strtotime($arvdate11[0]));
														$arvtime1 = date("H:i:s" , strtotime($arvdate11[1]));
														$arvnewdate1= date("Y-m-d" , strtotime($arvdate11[0]));
														$devdate1 = explode('T',$segarray[$seg]['Origin']['DepTime']);
														$depnewdatedetail= date("d-M" , strtotime($devdate1[0]));
														$depnewtimedetail = date("H:s" , strtotime($devdate1[1]));
														$newdepnewdate= date("Y-m-d" , strtotime($devdate1[0]));
														$depnewtime = date("H:i:s" , strtotime($devdate1[1]));
														$stopcount = count($segarray);
														if($stopcount=='1')
														 {
															$mainstopflight = 'Non Stop';
														 }
														 else
														 {
															$stopcount1 = $stopcount-1;
															$mainstopflight = $stopcount1.' Stop';
														 }

														 $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newdepnewdate.''. $depnewtime);
														$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $arvnewdate1.''. $arvtime1);
														$totalDuration = $from->diffInSeconds($to);
														 $durationhour =  gmdate('H', $totalDuration);
														 $durationmin =  gmdate('s', $totalDuration);

														$newtime=$durationhour.'H : '.$durationmin.'M';
										?>
										<div class="row">
											<div class="col-md-2">
												<div class="flight-name">
													<img src="{{ $imgsou}}" class="img-responsive flight-logo">
													<p class="flight-name-heading">{{$segarray[$seg]['Airline']['AirlineName']}}</p>
													<p>{{$segarray[$seg]['Airline']['AirlineCode']}} - {{$segarray[$seg]['Airline']['FlightNumber']}}</p>
												</div>
											</div>
											<div class="col-md-3">
												<div class="flight-depart">
													<p class="flight-depart-heading">{{$depnewdatedetail}} | {{$depnewtimedetail}} </p>
													<p class="flight-depart-text">{{$segarray[$seg]['Origin']['Airport']['AirportCode']}}</p>
												</div>
											</div>
											<div class="col-md-2">
												<div class="flight-depart">
													<p class="flight-depart-text"><i class="fa fa-plane fa-rotate-45"></i></p>
												</div>
											</div>
											<div class="col-md-3">
												<div class="flight-arrival">
													<p class="flight-arrival-heading">{{$arvnewdate11}} | {{$arvtime22}}</p>
													<p class="flight-arrival-text">{{$segarray[$seg]['Destination']['Airport']['AirportCode']}}</p>
												</div>
											</div>
											<div class="col-md-2">
												<div class="flight-duration">
													<p class="flight-duration-heading">{{$newtime}}</p>
													<p style="color:#008cff">{{$mainstopflight}}</p>
												</div>
											</div>
										</div>
									<?php } ?>
									<div class="row">
										<div class="col-md-10">
										</div>
									</div>
										<div class="flight-fare">
											<div class="row">
												<div class="col-md-12">
													<h5 class="fare-rules">Tip : <span>Airlines cancellation charges do apply. Please check cancellation and baggage policies for more details.</span></h5>
												</div>
											</div>
										</div>
									</div>
									@endforeach
								<?php } ?>
								<!-- inbound out -->
								<div class="flight-traveller">
									<div class="container">
										<div class="row">
											<div class="col-lg-12">
												<form   id="contact_form" class="contact_form">
													@if($resu['IsRefundable'])
													<input type="hidden" name="refund" value="Refundable">
													<!-- <button type="button" class="btn btn-refundable">Refundable</button> -->
													@else
														<input type="hidden" name="refund" value="Non-Refundable">
													<!-- <button type="button" class="btn btn-non-refundable">Refundable</button> -->
													@endif
													 <input name="_token" type="hidden" id="csrf_token" value="{{ csrf_token() }}"/>
													<p>Name should be same as in Government ID proof.</p>

													<?php
													$totalbasefare=0;
													$totaltax=0;
													$countchild=0;
													$countInfant=0;
													$adultbasefare=0;
													$adulttaxfare=0;
													$childbasefare=0;
													$childtaxfare=0;
													$infantbasefare=0;
													$infanttaxfare=0;
													$adultbasefare_return=0;
													$adulttaxfare_return=0;
													$childbasefare_return=0;
													$childtaxfare_return=0;
													$infantbasefare_return=0;
													$infanttaxfare_return=0;
													$adulttxnfeeofrd=0;
													$adulttxnfeepub=0;
													$childtxnfeeofrd=0;
													$childtxnfeepub=0;
													$infanttxnfeeofrd=0;
													$infanttxnfeepub=0;
													$adulttxnfeeofrd_return=0;
													$adulttxnfeepub_return=0;
													$childtxnfeeofrd_return=0;
													$childtxnfeepub_return=0;
													$infanttxnfeeofrd_return=0;
													$infanttxnfeepub_return=0;
													 for($fare_i=0;$fare_i<count($resu['FareBreakdown']);$fare_i++)
													  {
														$totalbasefare +=$resu['FareBreakdown'][$fare_i]['BaseFare'];
														$totaltax +=$resu['FareBreakdown'][$fare_i]['Tax'];
														if($resu['FareBreakdown'][$fare_i]['PassengerType']=='1')
														{
															$countadults= $resu['FareBreakdown'][$fare_i]['PassengerCount'];
															$adultbasefare= $resu['FareBreakdown'][$fare_i]['BaseFare'];
															$adulttaxfare= $resu['FareBreakdown'][$fare_i]['Tax'];
															$adulttxnfeeofrd= $resu['FareBreakdown'][$fare_i]['AdditionalTxnFeeOfrd'];
															$adulttxnfeepub= $resu['FareBreakdown'][$fare_i]['AdditionalTxnFeePub'];
														}
														if($resu['FareBreakdown'][$fare_i]['PassengerType']=='2' && !empty($resu['FareBreakdown'][$fare_i]['PassengerType']))
														{
															$countchild=  $resu['FareBreakdown'][$fare_i]['PassengerCount'];
															$childbasefare= $resu['FareBreakdown'][$fare_i]['BaseFare'];
															$childtaxfare= $resu['FareBreakdown'][$fare_i]['Tax'];
															$childtxnfeeofrd= $resu['FareBreakdown'][$fare_i]['AdditionalTxnFeeOfrd'];
															$childtxnfeepub= $resu['FareBreakdown'][$fare_i]['AdditionalTxnFeePub'];
														}
														if($resu['FareBreakdown'][$fare_i]['PassengerType']=='3' && !empty($resu['FareBreakdown'][$fare_i]['PassengerType']))
														{
															$countInfant=  $resu['FareBreakdown'][$fare_i]['PassengerCount'];
															$infantbasefare= $resu['FareBreakdown'][$fare_i]['BaseFare'];
															$infanttaxfare= $resu['FareBreakdown'][$fare_i]['Tax'];
															$infanttxnfeeofrd= $resu['FareBreakdown'][$fare_i]['AdditionalTxnFeeOfrd'];
															$infanttxnfeepub= $resu['FareBreakdown'][$fare_i]['AdditionalTxnFeePub'];
														}
													  }

													  //returnflight
													  if(!empty($resu_return))
													  {
													  		for($fare_i=0;$fare_i<count($resu_return['FareBreakdown']);$fare_i++)
														  {

															if($resu_return['FareBreakdown'][$fare_i]['PassengerType']=='1')
															{

																$adultbasefare_return= $resu_return['FareBreakdown'][$fare_i]['BaseFare'];
																$adulttaxfare_return= $resu_return['FareBreakdown'][$fare_i]['Tax'];
																$adulttxnfeeofrd_return= $resu_return['FareBreakdown'][$fare_i]['AdditionalTxnFeeOfrd'];
																$adulttxnfeepub_return= $resu_return['FareBreakdown'][$fare_i]['AdditionalTxnFeePub'];
															}
															if($resu_return['FareBreakdown'][$fare_i]['PassengerType']=='2' && !empty($resu_return['FareBreakdown'][$fare_i]['PassengerType']))
															{

																$childbasefare_return= $resu_return['FareBreakdown'][$fare_i]['BaseFare'];
																$childtaxfare_return= $resu_return['FareBreakdown'][$fare_i]['Tax'];
																$childtxnfeeofrd_return= $resu_return['FareBreakdown'][$fare_i]['AdditionalTxnFeeOfrd'];
																$childtxnfeepub_return= $resu_return['FareBreakdown'][$fare_i]['AdditionalTxnFeePub'];
															}
															if($resu_return['FareBreakdown'][$fare_i]['PassengerType']=='3' && !empty($resu_return['FareBreakdown'][$fare_i]['PassengerType']))
															{

																$infantbasefare_return= $resu_return['FareBreakdown'][$fare_i]['BaseFare'];
																$infanttaxfare_return= $resu_return['FareBreakdown'][$fare_i]['Tax'];
																$infanttxnfeeofrd_return= $resu_return['FareBreakdown'][$fare_i]['AdditionalTxnFeeOfrd'];
																$infanttxnfeepub_return= $resu_return['FareBreakdown'][$fare_i]['AdditionalTxnFeePub'];
															}
														  }
													  }

													  //endreturnflight
													  for($adu=1;$adu<=$countadults;$adu++)
													  {
													?>

													<h4>Adult {{$adu}}</h4>
													<div class="row mb-20">
													<input type="hidden" value="{{$adu}}" class="aducount">
														<div class="col-md-4">
															<select class="input_field adulttitle filltext " placeholder="Name" id="adulttitle-{{$adu}}" name="titleadult[]">
																<option value="0">Title</option>
																<option value="Mr">Mr.</option>
																<option value="Ms">Ms.</option>
																<option value="Mrs">Mrs.</option>
															</select>
															<span class="textfield_error" ></span>
														</div>
														<div class="col-md-4">
															<input type="text"  id="adultfirstname-{{$adu}}"  class="input_field adultfirstname filltext" placeholder="First and Middle Name" name="firstnameadult[]">
															<span class="textfield_error"></span>
														</div>
														<div class="col-md-4">
															<input type="text" id="adultlastname-{{$adu}}"  class="input_field adultlastname filltext" placeholder="Last Name" name="lastnameadult[]">
															<span class="textfield_error"></span>
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field adultdob filltext" id="adultdob"  placeholder="Date of Birth" name="adultdob[]">
															<span class="textfield_error"></span>
														</div>
														<input type="hidden" name="adultbasefare" value="{{$adultbasefare}}">
														<input type="hidden" name="adulttaxfare" value="{{$adulttaxfare}}">
														<input type="hidden" name="adulttxnfeeofrd" value="{{$adulttxnfeeofrd}}">
														<input type="hidden" name="adulttxnfeepub" value="{{$adulttxnfeepub}}">
														<input type="hidden" name="adultbasefarereturn" value="{{$adultbasefare_return}}">
														<input type="hidden" name="adulttaxfarereturn" value="{{$adulttaxfare_return}}">
														<input type="hidden" name="adulttxnfeeofrdreturn" value="{{$adulttxnfeeofrd_return}}">
														<input type="hidden" name="adulttxnfeepubreturn" value="{{$adulttxnfeepub_return}}">


													</div>
													<?php }
														//childloop
														for($chu=1;$chu<=$countchild;$chu++)
														 {
													 ?>


													<h4>Child {{$chu}}</h4>
													<input type="hidden" value="{{$chu}}" class="childcountnew">
													<div class="row mb-20">
														<div class="col-md-4">
															<select class="input_field childttile filltext" placeholder="Name" id="childttile-{{$chu}}"  name="childtitle[]">
																<option value="0">Title</option>
																<option value="Mr">Mr.</option>
																<option value="Ms">Ms.</option>
																<option value="Mrs">Mrs.</option>
															</select>
															<span class="textfield_error"></span>
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field childfirstname filltext" id="childfirstname-{{$chu}}" placeholder="First and Middle Name" name="childfirstname[]">
															<span class="textfield_error"></span>
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field childlastname filltext" id="childlastname-{{$chu}}" placeholder="Last Name" name="childlastname[]">
															<span class="textfield_error"></span>
														</div>
														<div class="col-md-4">
															<input type="text" id="childdob" class="input_field childdob filltext" placeholder="Date of Birth" name="childdob[]">
															<span class="textfield_error"></span>
														</div>
														<input type="hidden" name="childbasefare" value="{{$childbasefare}}">
														<input type="hidden" name="childtaxfare" value="{{$childtaxfare}}">
														<input type="hidden" name="childtxnfeeofrd" value="{{$childtxnfeeofrd}}">
														<input type="hidden" name="childtxnfeepub" value="{{$childtxnfeepub}}">

														<input type="hidden" name="childbasefarereturn" value="{{$childbasefare_return}}">
														<input type="hidden" name="childtaxfarereturn" value="{{$childtaxfare_return}}">
														<input type="hidden" name="childtxnfeeofrdreturn" value="{{$childtxnfeeofrd_return}}">
														<input type="hidden" name="childtxnfeepubreturn" value="{{$childtxnfeepub_return}}">
													</div>
													<?php }
													for($infant=1;$infant<=$countInfant;$infant++)
													 {
													 ?>

													<h4>Infant {{$infant}}</h4>
													<input type="hidden" value="{{$infant}}" class="infanctcountnew">
													<div class="row mb-20">
														<div class="col-md-4">
															<select class="input_field infatitle filltext" class="infatitle" id="infatitle-{{$infant}}" placeholder="Name" name="infatitle[]">
																<option value="0">Title</option>
																<option value="Mstr">Mstr.</option>
																<option value="Miss">Miss.</option>

																<span class="textfield_error"></span>
															</select>
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field infafistname filltext " id="infafistname-{{$infant}}" placeholder="First and Middle Name" name="infafistname[]">
															<span class="textfield_error"></span>
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field infalastname filltext" id="infalastname-{{$infant}}" placeholder="Last Name" name="infalastname[]">
															<span class="textfield_error"></span>
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field infantdob filltext" placeholder="Date of Birth" name="infadob[]">
															<span class="textfield_error"></span>
														</div>
														<input type="hidden" name="infantbasefare" value="{{$infantbasefare}}">
														<input type="hidden" name="infanttaxfare" value="{{$infanttaxfare}}">
														<input type="hidden" name="infanttxnfeeofrd" value="{{$infanttxnfeeofrd}}">
														<input type="hidden" name="infanttxnfeepub" value="{{$infanttxnfeepub}}">

														<input type="hidden" name="infantbasefarereturn" value="{{$infantbasefare_return}}">
														<input type="hidden" name="infanttaxfarereturn" value="{{$infanttaxfare_return}}">
														<input type="hidden" name="infanttxnfeeofrdreturn" value="{{$infanttxnfeeofrd_return}}">
														<input type="hidden" name="infanttxnfeepubreturn" value="{{$infanttxnfeepub_return}}">
													</div>
													<?php } ?>
													<h4>Contact Information</h4>

													<div class="row mb-20">
														<div class="col-md-4">
															<input type="text" class="input_field email filltext"  name="email" placeholder="Email" value="{{session()->get('travo_useremail')}}">
															<span class="textfield_error"></span>
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field phone filltext"  id="phone" placeholder="Phone" name="phone" value="{{session()->get('travo_phone')}}">
															<span class="textfield_error"></span>
														</div>

													</div>
													<div class="row mb-20">
														<div class="col-md-4">
															<input type="text" class="input_field address filltext"  name="address1" placeholder="Address" value="{{session()->get('travo_address')}}">
															<span class="textfield_error"></span>
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field city filltext" name="city"   placeholder="Enter City Name">
															<span class="textfield_error"></span>
														</div>
														<div class="col-md-4">
															<select class="input_field country" placeholder="Country" name="country">
																<option value="0">Select Country</option>
																<option value="AF%%Afghanistan">Afghanistan</option>
																<option value="AX%%Aland Islands">Åland Islands</option>
																<option value="AL%%Albania">Albania</option>
																<option value="DZ%%Algeria">Algeria</option>
																<option value="AS%%American Samoa">American Samoa</option>
																<option value="AD%%Andorra">Andorra</option>
																<option value="AO%%Angola">Angola</option>
																<option value="AI%%Anguilla">Anguilla</option>
																<option value="AQ%%Antarctica">Antarctica</option>
																<option value="AG%%Antigua">Antigua and Barbuda</option>
																<option value="AR%%Argentina">Argentina</option>
																<option value="AM%%Armenia">Armenia</option>
																<option value="AW%%Aruba">Aruba</option>
																<option value="AU%%Australia">Australia</option>
																<option value="AT%%Austria">Austria</option>
																<option value="AZ%%Azerbaijan">Azerbaijan</option>
																<option value="BS%%Bahamas">Bahamas</option>
																<option value="BH%%Bahrain">Bahrain</option>
																<option value="BD%%Barbados">Bangladesh</option>
																<option value="BB%%">Barbados</option>
																<option value="BY%%Belarus">Belarus</option>
																<option value="BE%%Belgium">Belgium</option>
																<option value="BZ%%Belize">Belize</option>
																<option value="BJ%%Benin">Benin</option>
																<option value="BM%%Bermuda">Bermuda</option>
																<option value="BT%%Bhutan">Bhutan</option>
																<option value="BO%%Bolivia">Bolivia, Plurinational State of</option>
																<option value="BQ%%Bonaire">Bonaire, Sint Eustatius and Saba</option>
																<option value="BA%%Bosnia">Bosnia and Herzegovina</option>
																<option value="BW%%Botswana">Botswana</option>
																<option value="BV%%Bouvet Island">Bouvet Island</option>
																<option value="BR%%Brazil">Brazil</option>
																<option value="IO%%British Indian">British Indian Ocean Territory</option>
																<option value="BN%%Brunei Darussalam">Brunei Darussalam</option>
																<option value="BG%%Bulgaria">Bulgaria</option>
																<option value="BF%%Burkina Faso">Burkina Faso</option>
																<option value="BI%%Burundi">Burundi</option>
																<option value="KH%%Cambodia">Cambodia</option>
																<option value="CM%%Cameroon">Cameroon</option>
																<option value="CA%%Canada">Canada</option>
																<option value="CV%%Cape Verde">Cape Verde</option>
																<option value="KY%%Cayman Islands">Cayman Islands</option>
																<option value="CF%%Central African">Central African Republic</option>
																<option value="TD%%Chad">Chad</option>
																<option value="CL%%Chile">Chile</option>
																<option value="CN%%China">China</option>
																<option value="CX%%Christmas Island">Christmas Island</option>
																<option value="CC%%Cocos">Cocos (Keeling) Islands</option>
																<option value="CO%%Colombia">Colombia</option>
																<option value="KM%%Comoros">Comoros</option>
																<option value="CG%%Congo">Congo</option>
																<option value="CD%%Congo">Congo, the Democratic Republic of the</option>
																<option value="CK%%Cook Islands">Cook Islands</option>
																<option value="CR%%Costa Rica">Costa Rica</option>
																<option value="CI%%Cote d'Ivoire">Côte d'Ivoire</option>
																<option value="HR%%Croatia">Croatia</option>
																<option value="CU%%Cuba">Cuba</option>
																<option value="CW%%Curacao">Curaçao</option>
																<option value="CY%%Cyprus">Cyprus</option>
																<option value="CZ%%Czech">Czech Republic</option>
																<option value="DK%%Denmark">Denmark</option>
																<option value="DJ%%Djibouti">Djibouti</option>
																<option value="DM%%Dominica">Dominica</option>
																<option value="DO%%Dominican">Dominican Republic</option>
																<option value="EC%%Ecuador">Ecuador</option>
																<option value="EG%%Egypt">Egypt</option>
																<option value="SV%%El Salvador">El Salvador</option>
																<option value="GQ%%Equatorial Guinea">Equatorial Guinea</option>
																<option value="ER%%Eritrea">Eritrea</option>
																<option value="EE%%Estonia">Estonia</option>
																<option value="ET%%Ethiopia">Ethiopia</option>
																<option value="FK%%Falkland">Falkland Islands (Malvinas)</option>
																<option value="FO%%Faroe Islands">Faroe Islands</option>
																<option value="FJ%%Fiji">Fiji</option>
																<option value="FI%%Finland">Finland</option>
																<option value="FR%%France">France</option>
																<option value="GF%%Guiana">French Guiana</option>
																<option value="PF%%Polynesia">French Polynesia</option>
																<option value="TF%%French Southern">French Southern Territories</option>
																<option value="GA%%Gabon">Gabon</option>
																<option value="GM%%Gambia">Gambia</option>
																<option value="GE%%Georgia">Georgia</option>
																<option value="DE%%Germany">Germany</option>
																<option value="GH%%Ghana">Ghana</option>
																<option value="GI%%Gibraltar">Gibraltar</option>
																<option value="GR%%Greece">Greece</option>
																<option value="GL%%Greenland">Greenland</option>
																<option value="GD%%Grenada">Grenada</option>
																<option value="GP%%Guadeloupe">Guadeloupe</option>
																<option value="GU%%Guam">Guam</option>
																<option value="GT%%Guatemala">Guatemala</option>
																<option value="GG%%Guernsey">Guernsey</option>
																<option value="GN%%Guinea">Guinea</option>
																<option value="GW%%Guinea-Bissau">Guinea-Bissau</option>
																<option value="GY%%Guyana">Guyana</option>
																<option value="HT%%Haiti">Haiti</option>
																<option value="HM%%Heard Island">Heard Island and McDonald Islands</option>
																<option value="VA%%Holy See">Holy See (Vatican City State)</option>
																<option value="HN%%Honduras">Honduras</option>
																<option value="HK%%Hong Kong">Hong Kong</option>
																<option value="HU%%Hungary">Hungary</option>
																<option value="IS%%Iceland">Iceland</option>
																<option value="IN%%India">India</option>
																<option value="ID%%Indonesia">Indonesia</option>
																<option value="IR%%Iran">Iran, Islamic Republic of</option>
																<option value="IQ%%Iraq">Iraq</option>
																<option value="IE%%Ireland">Ireland</option>
																<option value="IM%%Isle">Isle of Man</option>
																<option value="IL%%Israel">Israel</option>
																<option value="IT%%Italy">Italy</option>
																<option value="JM%%Jamaica">Jamaica</option>
																<option value="JP%%Japan">Japan</option>
																<option value="JE%%Jersey">Jersey</option>
																<option value="JO%%Jordan">Jordan</option>
																<option value="KZ%%Kazakhstan">Kazakhstan</option>
																<option value="KE%%Kenya">Kenya</option>
																<option value="KI%%Kiribati">Kiribati</option>
																<option value="KP%%Korea">Korea, Democratic People's Republic of</option>
																<option value="KR%%Korea">Korea, Republic of</option>
																<option value="KW%%Kuwait">Kuwait</option>
																<option value="KG%%Kyrgyzstan">Kyrgyzstan</option>
																<option value="LA%%Lao">Lao People's Democratic Republic</option>
																<option value="LV%%Latvia">Latvia</option>
																<option value="LB%%Lebanon">Lebanon</option>
																<option value="LS%%Lesotho">Lesotho</option>
																<option value="LR%%Liberia">Liberia</option>
																<option value="LY%%Libya">Libya</option>
																<option value="LI%%Liechtenstein">Liechtenstein</option>
																<option value="LT%%Lithuania">Lithuania</option>
																<option value="LU%%Luxembourg">Luxembourg</option>
																<option value="MO%%Macao">Macao</option>
																<option value="MK%%Macedonia">Macedonia, the former Yugoslav Republic of</option>
																<option value="MG%%Madagascar">Madagascar</option>
																<option value="MW%%Malawi">Malawi</option>
																<option value="MY%%Malaysia">Malaysia</option>
																<option value="MV%%Maldives">Maldives</option>
																<option value="ML%%Malta">Mali</option>
																<option value="MT%%">Malta</option>
																<option value="MH%%Marshall">Marshall Islands</option>
																<option value="MQ%%Martinique">Martinique</option>
																<option value="MR%%Mauritania">Mauritania</option>
																<option value="MU%%Mauritius">Mauritius</option>
																<option value="YT%%Mayotte">Mayotte</option>
																<option value="MX%%Mexico">Mexico</option>
																<option value="FM%%Micronesia">Micronesia, Federated States of</option>
																<option value="MD%%Moldova">Moldova, Republic of</option>
																<option value="MC%%Monaco">Monaco</option>
																<option value="MN%%Mongolia">Mongolia</option>
																<option value="ME%%Montenegro">Montenegro</option>
																<option value="MS%%Montserrat">Montserrat</option>
																<option value="MA%%Mozambique">Morocco</option>
																<option value="MZ%%">Mozambique</option>
																<option value="MM%%Myanmar">Myanmar</option>
																<option value="NA%%Namibia">Namibia</option>
																<option value="NR%%Nauru">Nauru</option>
																<option value="NP%%Nepal">Nepal</option>
																<option value="NL%%Netherlands">Netherlands</option>
																<option value="NC%%New Caledonia">New Caledonia</option>
																<option value="NZ%%New Zealand">New Zealand</option>
																<option value="NI%%Nicaragua">Nicaragua</option>
																<option value="NE%%Niger">Niger</option>
																<option value="NG%%Nigeria">Nigeria</option>
																<option value="NU%%Niue">Niue</option>
																<option value="NF%%Norfolk Island">Norfolk Island</option>
																<option value="MP%%Northern Mariana">Northern Mariana Islands</option>
																<option value="NO%%Norway">Norway</option>
																<option value="OM%%Oman">Oman</option>
																<option value="PK%%Pakistan">Pakistan</option>
																<option value="PW%%Palau">Palau</option>
																<option value="PS%%Palestinian">Palestinian Territory, Occupied</option>
																<option value="PA%%Panama">Panama</option>
																<option value="PG%%Papua">Papua New Guinea</option>
																<option value="PY%%Paraguay">Paraguay</option>
																<option value="PE%%Peru">Peru</option>
																<option value="PH%%Pitcairn">Philippines</option>
																<option value="PN%%">Pitcairn</option>
																<option value="PL%%Portugal">Poland</option>
																<option value="PT%%">Portugal</option>
																<option value="PR%%Puerto Rico">Puerto Rico</option>
																<option value="QA%%Qatar">Qatar</option>
																<option value="RE%%Réunion">Réunion</option>
																<option value="RO%%Romania">Romania</option>
																<option value="RU%%Russian">Russian Federation</option>
																<option value="RW%%Rwanda">Rwanda</option>
																<option value="BL%%Saint Barthélemy">Saint Barthélemy</option>
																<option value="SH%%Saint Helena">Saint Helena, Ascension and Tristan da Cunha</option>
																<option value="KN%%Saint Kitts">Saint Kitts and Nevis</option>
																<option value="LC%%Saint Lucia">Saint Lucia</option>
																<option value="MF%%Saint Martin">Saint Martin (French part)</option>
																<option value="PM%%Saint Pierre">Saint Pierre and Miquelon</option>
																<option value="VC%%Saint Vincent">Saint Vincent and the Grenadines</option>
																<option value="WS%%Samoa">Samoa</option>
																<option value="SM%%San Marino">San Marino</option>
																<option value="ST%%Sao Tome">Sao Tome and Principe</option>
																<option value="SA%%Saudi Arabia">Saudi Arabia</option>
																<option value="SN%%Senegal">Senegal</option>
																<option value="RS%%Seychelles">Serbia</option>
																<option value="SC%%">Seychelles</option>
																<option value="SL%%Sierra Leone">Sierra Leone</option>
																<option value="SG%%Singapore">Singapore</option>
																<option value="SX%%Sint Maarten">Sint Maarten (Dutch part)</option>
																<option value="SK%%Slovakia">Slovakia</option>
																<option value="SI%%Slovenia">Slovenia</option>
																<option value="SB%%Solomon Islands">Solomon Islands</option>
																<option value="SO%%Somalia">Somalia</option>
																<option value="ZA%%South Africa">South Africa</option>
																<option value="GS%%South Georgia">South Georgia and the South Sandwich Islands</option>
																<option value="SS%%South Sudan">South Sudan</option>
																<option value="ES%%Spain">Spain</option>
																<option value="LK%%Sri Lanka">Sri Lanka</option>
																<option value="SD%%Suriname">Sudan</option>
																<option value="SR%%">Suriname</option>
																<option value="SJ%%Svalbard">Svalbard and Jan Mayen</option>
																<option value="SZ%%Swaziland">Swaziland</option>
																<option value="SE%%Sweden">Sweden</option>
																<option value="CH%%Switzerland">Switzerland</option>
																<option value="SY%%Syrian">Syrian Arab Republic</option>
																<option value="TW%%Taiwan">Taiwan, Province of China</option>
																<option value="TJ%%Tajikistan">Tajikistan</option>
																<option value="TZ%%Tanzania">Tanzania, United Republic of</option>
																<option value="TH%%Thailand">Thailand</option>
																<option value="TL%%Timor-Leste">Timor-Leste</option>
																<option value="TG%%Togo">Togo</option>
																<option value="TK%%Tonga">Tokelau</option>
																<option value="TO%%">Tonga</option>
																<option value="TT%%Trinidad and Tobago">Trinidad and Tobago</option>
																<option value="TN%%Tunisia">Tunisia</option>
																<option value="TR%%Turkey">Turkey</option>
																<option value="TM%%Turkmenistan">Turkmenistan</option>
																<option value="TC%%Turks and Caicos Islands">Turks and Caicos Islands</option>
																<option value="TV%%Tuvalu">Tuvalu</option>
																<option value="UG%%Uganda">Uganda</option>
																<option value="UA%%Ukraine">Ukraine</option>
																<option value="AE%%United Arab Emirates">United Arab Emirates</option>
																<option value="GB%%United Kingdom">United Kingdom</option>
																<option value="US%%United States">United States</option>
																<option value="UM%%United States Minor Outlying Islands">United States Minor Outlying Islands</option>
																<option value="UY%%Uruguay">Uruguay</option>
																<option value="UZ%%Uzbekistan">Uzbekistan</option>
																<option value="VU%%Vanuatu">Vanuatu</option>
																<option value="VE%%Venezuela, Bolivarian Republic of">Venezuela, Bolivarian Republic of</option>
																<option value="VN%%Viet Nam">Viet Nam</option>
																<option value="VG%%Virgin Islands, British">Virgin Islands, British</option>
																<option value="VI%%Virgin Islands, U.S.">Virgin Islands, U.S.</option>
																<option value="WF%%Wallis and Futuna">Wallis and Futuna</option>
																<option value="EH%%Western Sahara">Western Sahara</option>
																<option value="YE%%Yemen">Yemen</option>
																<option value="ZM%%Zambia">Zambia</option>
																<option value="ZW%%Zimbabwe">Zimbabwe</option>
															</select>
															<span class="textfield_error"></span>
														</div>


													</div>
													<h4>Emergency Contact</h4>
													<div class="row mb-20">
														<div class="col-md-4">
															<input type="text" class="input_field eme_name filltext"  name="eme_name" placeholder="Name" >
															<span class="textfield_error"></span>
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field eme_phone filltext"  id="eme_phone" placeholder="Phone" name="eme_phone">
															<span class="textfield_error"></span>
														</div>
														<div class="col-md-4">
															<input type="text" class="input_field eme_relation filltext"  id="eme_relation" placeholder="Relation" name="eme_relation">
															<span class="textfield_error"></span>
														</div>

													</div>
													<hr>
													<?php

														if($resu['IsGSTMandatory']=='1')
														{
															$chkmaingst = 'Mandatory';
															$inputchecked="";
															$chkgstshow="display:none";
															$chkgstshow="";
															$gstaddress = "177 E VIJAY NAGAR, AMRITSAR BATALA ROAD,Amritsar 1 - Ward No.6 Punjab";
															$gstemail="info@travoweb.com";
															$gstnumber="03BDHPK8214M1ZG";
															$gstcompname="SEKAP TRAVEL";
															$gstcontactno="61490149833";

														}
														else
														{
															$chkmaingst = 'optional';
															$inputchecked='';
															$chkgstshow="display:none";
															$gstaddress = "";
															$gstemail="";
															$gstnumber="";
															$gstcompname="";
															$gstcontactno="";
														}
													?>

													<div class="gst-checkbox mb-20">
														<h4 style="display:inline-block">Enter GST details to claim benefits ({{$chkmaingst}}) &nbsp;
														<label class="checkcontainer" style="display:inline-block; margin-bottom:17px;">
															<input type="checkbox" name="chkgst" id="chkgst" value="1" class="chkgst" <?php echo $inputchecked; ?>> <span class="checkmark"></span>
														</label></h4>
														<p>Travelling for business purpose? Please enter Company GST details below.</p>
													</div>

													<div class="gst-details" id="showgst" style="<?php echo $chkgstshow; ?>">
														<div class="row mb-20">
															<div class="col-md-4">
																<input type="text" class="input_field gstaddress filltext_gst" id="gstaddress" placeholder="Company Address" name="gstaddress" value="<?php echo $gstaddress;?>">
																<span class="textfield_error"></span>
															</div>
															<div class="col-md-4">
																<input type="text" class="input_field gstemail filltext_gst" id="gstemail" placeholder="Company's GST Email" name="gstemail" value="<?php echo $gstemail ;?>">
																<span class="textfield_error"></span>
															</div>
															<div class="col-md-4">
																<input type="text" class="input_field gstnumber filltext_gst" id="gstnumber" placeholder="GST Number" name="gstnumber" value="<?php echo $gstnumber;?>" >
																<span class="textfield_error"></span>
															</div>
															<div class="col-md-4">
																<input type="text" class="input_field gstcompname filltext_gst" placeholder="Company Name" name="gstcompname" id="gstcompname" value="<?php echo $gstcompname;?>">
																<span class="textfield_error"></span>
															</div>
															<div class="col-md-4">
																<input type="text" class="input_field gstcontactno filltext_gst" name="gstcontactno" placeholder="Company Contact Number" id="gstcontactno" value="<?php echo $gstcontactno;?>" >
																<span class="textfield_error"></span>
															</div>
														</div>
													</div>
													<!-- meal -->
														<div class="field-set">
													<div class="gst-checkbox mb-20 meal-div">
														<h4 style="display:inline-block;margin-bottom: 0;">Meal & Baggages&nbsp;
														<label class="checkcontainer" style="display:inline-block; margin-bottom:17px;">
															<input type="checkbox" name="meal" id="meal" value="1" class="meal"> <span class="checkmark "></span>
														</label></h4>

													</div>

													<div class="gst-details gst-div" id="showmeal" style="display: none" >
														<div class="row mb-20">
															<div class="col-sm-6">
																<div class="class-hide">
																<p class="gst-book">Pre-book Meal</p>
																<?php
																if(!empty($meal['MealDynamic']) ||  !empty($meal['Meal']))
																{
																	echo $fhname='<div class="col-md-12"><p class="flight-depart-text">'.$mainarray['airportorgincode'].' <i class="fa fa-plane flight-i fa-rotate-45"></i>  '.$mainarray['airportdename1'].'</p></div>';
																}

																if(!empty($meal['MealDynamic']))
																{
																?>
																<div class="col-md-6">
																	<select class="input_field select-g" placeholder="Name" name="mealdynamic" id="mealshowprice">
																		<?php
																		if($meal['MealDynamic'][0][0]['mealcur']=='fa-inr')
																		{
																			$mealcur='Rs.';
																		}
																		else
																		{
																			$mealcur='$';
																		}
																		for($mealc=0;$mealc<count($meal['MealDynamic'][0]);$mealc++)
																		{

																			echo '<option value="'.$meal['MealDynamic'][0][$mealc]['Code'].'%'.$meal['MealDynamic'][0][$mealc]['Description'].'%'.$meal['MealDynamic'][0][$mealc]['mealfprice'].'%'.$meal['MealDynamic'][0][$mealc]['AirlineDescription'].'%'.$meal['MealDynamic'][0][$mealc]['mealmainprice'].'%'.$meal['MealDynamic'][0][$mealc]['mealmarginprice'].'">'.$meal['MealDynamic'][0][$mealc]['AirlineDescription'].' - '.$mealcur.' '.$meal['MealDynamic'][0][$mealc]['mealfprice'].'</option>';
																		}
																		?>
																	</select>
																</div>
															</div>
															</div>
															<div class="col-sm-6">
																<div class="class-hide">
																<p class="gst-book">Pre-book Excess Baggage</p>
																<?php
																}
																echo $fhname;
																if(!empty($meal['Baggage']))
																{
																?>
																<div class="col-md-6">
																	<select class="input_field select-g" placeholder="Name" name="baggage" id="baggage">
																		<?php
																		if($meal['Baggage'][0][0]['bagcur']=='fa-inr')
																		{
																			$mealcur='Rs.';
																		}
																		else
																		{
																			$mealcur='$';
																		}
																		for($bag=0;$bag<count($meal['Baggage'][0]);$bag++)
																		{

																			echo '<option value="'.$meal['Baggage'][0][$bag]['Weight'].'%'.$meal['Baggage'][0][$bag]['bagfprice'].'%'.$meal['Baggage'][0][$bag]['WayType'].'%'.$meal['Baggage'][0][$bag]['Code'].'%'.$meal['Baggage'][0][$bag]['Description'].'%'.$meal['Baggage'][0][$bag]['Origin'].'%'.$meal['Baggage'][0][$bag]['Destination'].'%'.$meal['Baggage'][0][$bag]['bagmainprice'].'%'.$meal['Baggage'][0][$bag]['bagmarginprice'].'">'.$meal['Baggage'][0][$bag]['Weight'].' Kg - '.$mealcur.' '.$meal['Baggage'][0][$bag]['bagfprice'].'</option>';
																		}

																		?>

																	</select>
																</div>
															</div>

															</div>


														<?php }
														else
														{
															echo '<input type="hidden" value="" name="baggage">';
														}
															if(!empty($meal['Meal']))
															{
														?>

																<div class="col-md-6">
																		<select class="input_field select-g" placeholder="Name" name="meal" id="lccmeal">
																			<?php

																			for($mealnew=0;$mealnew<count($meal['Meal']);$mealnew++)
																			{
																				echo '<option value="'.$meal['Meal'][$mealnew]['Code'].'%'.$meal['Meal'][$mealnew]['Description'].'">'.$meal['Meal'][$mealnew]['Description'].'</option>';
																			}

																			?>
																		</select>
																	</div>
														<?php } ?>

														</div>
														<!-- retrun flight meal -->

														<!-- <p>{{$mainarray['airportorgincode']}} <i class="fa fa-plane flight-i fa-rotate-45"></i> {{$mainarray['airportdename1']}}   </p> -->
														<div class="row mb-20" id="te">

															<?php
															if(!empty($meal_return))
															{
																echo '<div class="col-md-12" style="display:none !important;"><p class="flight-depart-text">'.$mainarray['airportdename1'].' <i class="fa fa-plane flight-i fa-rotate-45"></i>  '.$mainarray['airportorgincode'].'</p></div>';

															if(!empty($meal_return['MealDynamic']))
															{
															?>

															<div class="col-md-6">
																<select class="input_field select-g" placeholder="Name" name="mealdynamicreturn" id="mealdynamicreturn">
																	<?php
																	if($meal_return['MealDynamic'][0][0]['mealcur']=='fa-inr')
																		{
																			$mealcur='Rs.';
																		}
																		else
																		{
																			$mealcur='$';
																		}
																	for($meal_returnc=0;$meal_returnc<count($meal_return['MealDynamic'][0]);$meal_returnc++)
																		{


																		echo '<option value="'.$meal_return['MealDynamic'][0][$meal_returnc]['Code'].'%'.$meal_return['MealDynamic'][0][$meal_returnc]['Description'].'%'.$meal_return['MealDynamic'][0][$meal_returnc]['mealfprice'].'%'.$meal_return['MealDynamic'][0][$meal_returnc]['AirlineDescription'].'%'.$meal_return['MealDynamic'][0][$meal_returnc]['mealmainprice'].'%'.$meal_return['MealDynamic'][0][$meal_returnc]['mealmarginprice'].'">'.$meal_return['MealDynamic'][0][$meal_returnc]['AirlineDescription'].' - '.$mealcur.''.$meal_return['MealDynamic'][0][$meal_returnc]['mealfprice'].'</option>';

																		// echo '<option value="'.$meal_return['MealDynamic'][0][$meal_returnc]['Code'].'%'.$meal_return['MealDynamic'][0][$meal_returnc]['Description'].'%'.$cmealpriceret.'%'.$meal_return['MealDynamic'][0][$meal_returnc]['AirlineDescription'].'%'.$meal_return['MealDynamic'][0][$meal_returnc]['Price'].'">'.$meal_return['MealDynamic'][0][$meal_returnc]['AirlineDescription'].' - Rs. '.$cmealpriceret.'</option>';
																	}
																	?>
																</select>
															</div>
															<?php
															}
															if(!empty($meal_return['Baggage']))
															{
															?>
															<div class="col-md-6">
																<select class="input_field select-g" placeholder="Name" name="baggage_return" id="baggage_return">
																	<?php
																	if($meal_return['Baggage'][0][0]['bagcur']=='fa-inr')
																	{
																		$mealcur='Rs.';
																	}
																	else
																	{
																		$mealcur='$';
																	}

																	for($bag=0;$bag<count($meal_return['Baggage'][0]);$bag++)
																	{
																		$bagprice_rt = $meal_return['Baggage'][0][$bag]['Price'];
																		$totalbagprice_rt = ($bagprice_rt * 3) /100;
																		 $bagpricert=round($bagprice_rt + $totalbagprice_rt );
																		echo '<option value="'.$meal_return['Baggage'][0][$bag]['Weight'].'%'.$meal_return['Baggage'][0][$bag]['bagfprice'].'%'.$meal_return['Baggage'][0][$bag]['WayType'].'%'.$meal_return['Baggage'][0][$bag]['Code'].'%'.$meal_return['Baggage'][0][$bag]['Description'].'%'.$meal_return['Baggage'][0][$bag]['Origin'].'%'.$meal_return['Baggage'][0][$bag]['Destination'].'%'.$meal_return['Baggage'][0][$bag]['bagmainprice'].'%'.$meal_return['Baggage'][0][$bag]['bagmarginprice'].'">'.$meal_return['Baggage'][0][$bag]['Weight'].' Kg - '.$mealcur.' '.$meal_return['Baggage'][0][$bag]['bagfprice'].'</option>';
																	}
																	?>

																</select>
															</div>
															<?php }
															else
															{
																echo '<input type="hidden" value="" name="baggage_return">';
															}
																if(!empty($meal_return['Meal']))
																{
															?>

																<div class="col-md-6">
																		<select class="input_field select-g" placeholder="Name" name="mealreturn">
																			<?php

																			for($meal_returnnew=0;$meal_returnnew<count($meal_return['Meal']);$meal_returnnew++)
																			{
																				echo '<option value="'.$meal_return['Meal'][$meal_returnnew]['Code'].'%'.$meal_return['Meal'][$meal_returnnew]['Description'].'">'.$meal_return['Meal'][$meal_returnnew]['Description'].'</option>';
																			}

																			?>
																		</select>
																	</div>
														<?php } ?>

														</div>
													<?php } ?>
														<!-- end return flight meal -->
													</div>
														</div>
													<!-- end meal -->
													<div class="note">
														<p><strong>Note:</strong> You will receive your ticket with above GST details.</p>
													</div>
													<input type="hidden" name="maingradflightprice" id="maingradflightprice">
													<input type="hidden" name="flightbaseprice" id="flightbaseprice">
													<input type="hidden" name="flightbaseprice_r" id="flightbaseprice_r">

													<input type="hidden" name="flightbasemarginprice" id="flightbasemarginprice">
													<input type="hidden" name="flightbasemarginprice_r" id="flightbasemarginprice_r">

													<input type="hidden" name="flighttaxprice" id="flighttaxprice">
													<input type="hidden" name="flighttaxprice_r" id="flighttaxprice_r">
													<input type="hidden" name="flightotherprice" id="flightotherprice">
													<input type="hidden" name="flightotherprice_r" id="flightotherprice_r">
													<input type="hidden" name="flightcurrencyicon" id="flightcurrencyicon">
													<input type="hidden" name="convertcurrecny" value="{{$resu['Fare']['flightconvr']}}" id="convertcurrecny">

													<input type="hidden" name="totalmealprice" id="totalmealprice" value="0">
													<input type="hidden" name="totalorgmealprice" id="totalorgmealprice" value="0">
													<input type="hidden" name="bagweight" id="bagweight">
													<input type="hidden" name="newbagprice" id="newbagprice" value="0">
													<input type="hidden" name="orgbagprice" id="orgbagprice" value="0">
													<input type="hidden" name="totalmealname" id="totalmealname" value="">
													<!-- return -->
													<input type="hidden" name="totalmealprice_return" id="totalmealprice_return" value="0">
													<input type="hidden" name="totalorgmealprice_return" id="totalorgmealprice_return" value="0">
													<input type="hidden" name="bagweight_return" id="bagweight_return">
													<input type="hidden" name="newbagprice_return" id="newbagprice_return" value="0">
													<input type="hidden" name="orgbagprice_return" id="orgbagprice_return" value="0">
													<input type="hidden" name="totalmealname_return" id="totalmealname_return" value="">


													<!-- end return -->

													<input type="hidden" value="{{$countchild}}" id="childcount">
													<input type="hidden" value="{{$countInfant}}" id="infacount">
													<input type="hidden" name="journytype" value="{{$mainarray['journytype']}}">
													<input type="hidden" name="flightcabinclass" value="{{$mainarray['flightcabinclass']}}">
													<input type="hidden" name="lcccheck" value="{{$resu['IsLCC']}}">
													<input type="hidden" name="resultindex" value="{{$resu['ResultIndex']}}">
													<input type="hidden" id="currency" name="Currency" value="{{$resu['Fare']['Currency']}}">
													<input type="hidden" name="YQTax" value="{{$resu['Fare']['YQTax']}}">
													<input type="hidden" name="OtherCharges" value="{{$resu['Fare']['OtherCharges']}}">
													<input type="hidden" name="Discount" value="{{$resu['Fare']['Discount']}}">
													<input type="hidden" name="PublishedFare" value="{{$resu['Fare']['PublishedFare']}}">

													<input type="hidden" name="OfferedFare" value="{{$resu['Fare']['OfferedFare']}}">
													<input type="hidden" name="TdsOnCommission" value="{{$resu['Fare']['TdsOnCommission']}}">
													<input type="hidden" name="TdsOnPLB" value="{{$resu['Fare']['TdsOnPLB']}}">
													<input type="hidden" name="TdsOnIncentive" value="{{$resu['Fare']['TdsOnIncentive']}}">
													<input type="hidden" name="ServiceFee" value="{{$resu['Fare']['ServiceFee']}}">

													<?php
													if(!empty($resu_return))
													{
													?>
													<input type="hidden" name="lcccheck_return" value="{{$resu_return['IsLCC']}}">
													<input type="hidden" name="resultindex_return" value="{{$resu_return['ResultIndex']}}">
													<input type="hidden" name="Currency_return" value="{{$resu_return['Fare']['Currency']}}">
													<input type="hidden" name="YQTax_return" value="{{$resu_return['Fare']['YQTax']}}">
													<input type="hidden" name="OtherCharges_return" value="{{$resu_return['Fare']['OtherCharges']}}">
													<input type="hidden" name="Discount_return" value="{{$resu_return['Fare']['Discount']}}">
													<input type="hidden" name="PublishedFare_return" value="{{$resu_return['Fare']['PublishedFare']}}">

													<input type="hidden" name="OfferedFare_return" value="{{$resu_return['Fare']['OfferedFare']}}">
													<input type="hidden" name="TdsOnCommission_return" value="{{$resu_return['Fare']['TdsOnCommission']}}">
													<input type="hidden" name="TdsOnPLB_return" value="{{$resu_return['Fare']['TdsOnPLB']}}">
													<input type="hidden" name="TdsOnIncentive_return" value="{{$resu_return['Fare']['TdsOnIncentive']}}">
													<input type="hidden" name="ServiceFee_return" value="{{$resu_return['Fare']['ServiceFee']}}">
													<?php } ?>
													<div class="text-right" id="payment_btn">
													<button type="button" id="form_submit_button" class="form_submit_button button trans_200 makepyament" >Continue<span></span><span></span><span></span></button>
														<!-- @if(session()->has("travo_useremail"))
														<button type="button" id="form_submit_button" class="form_submit_button button trans_200 makepyament" >Make Payment<span></span><span></span><span></span></button>
														@else
														<button type="button"  id="form_submit_button" class="form_submit_button button trans_200 user_box_login">Login<span></span><span></span><span></span></button>
														<button type="button"  id="form_submit_button2" class="form_submit_button button trans_200  guest_box_login" >Guest<span></span><span></span><span></span></button>
														@endif -->
													</div>

												</form>
												<!--Strip payment gateway-->
												<div class="payment-card" id="paymentcard" style="display: none">

													<div class="dropin-page">

														<form action="{{route('flightstripepayment')}}" method="POST" id="payment-form">



															<h3 class="bt_title">Payment Method </h3>

															<span class="payment-errors" style="color:red"></span>

															<div class="form-row row">

																<div class="col-12">
																	<div class="icon-container">
																		<i class="fa fa-cc-visa" style="color:navy;"></i>
																		<i class="fa fa-cc-amex" style="color:blue;"></i>
																		<i class="fa fa-cc-mastercard" style="color:red;"></i>
																		<i class="fa fa-cc-discover" style="color:orange;"></i>
																	</div>
																</div>
																<div class="col-12">

																	<label class="pay-label"><span><i class="fa fa-credit-card-alt" style="color: #000000;"></i> Card Holder</span></label>

																	<span class="text-danger err-msg password textfield_error textfield"  id="name_error" style="display: none;font-size: 11px"> </span>

																	<input type="text" size="20" id="name" name="name" class="form-control pay-input textfield" placeholder="Card Holder Name">





																</div>

															</div>

															<div class="form-row row">

																<div class="col-12">

																	<label class="pay-label"><span><i class="fa fa-credit-card-alt" style="color: #000000;"></i> Card Number</span></label>

																	<span class="text-danger err-msg password textfield_error textfield"  id="card_error" style="display: none;font-size: 11px"> </span>

																	<input type="text" size="20" id="card" data-stripe="number" class="form-control pay-input textfield leadage" placeholder="Enter Your Card No">



																</div>

															</div>

															<div class="form-row row">

																<div class="col-sm-6" >

																	<div class="row">

																		<div class="col-sm-6">

																			<label class="pay-label"><span><i class="fa fa-ban" style="color: #000000;"></i> Expiration</span></label>

																			<span class="text-danger err-msg password textfield_error textfield"  id="expmonth_error" style="display: none;font-size: 11px"> </span>

																			<input type="text" size="2" id="expmonth" data-stripe="exp_month" class="form-control pay-input textfield leadage" placeholder="Month" maxlength="2">

																		</div>

																		<div class="col-sm-6">

																			<label class="pay-label"><span>&nbsp;</span> </label>

																			<span class="text-danger err-msg password textfield_error textfield"  id="expyear_error" style="display: none;font-size: 11px"> </span>

																			<input type="text" size="2" id="expyear" data-stripe="exp_year" class="form-control pay-input textfield leadage" placeholder="Year" maxlength="2">

																		</div>

																	</div>

																</div>

																<div class="col-sm-6">

																	<label class="pay-label"><span><i class="fa fa-credit-card" style="color: #000000;"></i> CVC</span></label>

																	<span class="text-danger err-msg password textfield_error textfield"  id="cvc_error" style="display: none;font-size: 11px"> </span>

																	<input type="text" size="4" id="cvc" data-stripe="cvc" class="form-control pay-input textfield leadage" placeholder="Enter Your CVC" maxlength="4">



																</div>



															</div>





															<input type="submit" class="submit pay-btn" value="Make Payment">

														</form>

													</div>

												</div>

											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="flight-book-price">
									<h3>Fare Breakup</h3>
									<?php
									$totalbasefare=0;
									$totaltax=0;
									$child='';
									$Infant='';
									for($fare_i=0;$fare_i<count($resu['FareBreakdown']);$fare_i++)
									  {
										$totalbasefare +=$resu['FareBreakdown'][$fare_i]['BaseFare'];
										$totaltax +=$resu['FareBreakdown'][$fare_i]['Tax'];
										if($resu['FareBreakdown'][$fare_i]['PassengerType']=='1')
										{
											$adults= 'Adults : '.$resu['FareBreakdown'][$fare_i]['PassengerCount'];
										}
										if($resu['FareBreakdown'][$fare_i]['PassengerType']=='2' && !empty($resu['FareBreakdown'][$fare_i]['PassengerType']))
										{
											$child=  ', Child :' .$resu['FareBreakdown'][$fare_i]['PassengerCount'];
										}
										if($resu['FareBreakdown'][$fare_i]['PassengerType']=='3' && !empty($resu['FareBreakdown'][$fare_i]['PassengerType']))
										{
											$Infant=  ', Infant :' .$resu['FareBreakdown'][$fare_i]['PassengerCount'];
										}
									  }
									$publishvaleu=round($resu['Fare']['flightfare']) + round($resu['Fare']['flighttax']) + round($resu['Fare']['flightothercharges']);
									$basefare1 = round($resu['Fare']['flightfare']);
									$taxfare1 = round( $resu['Fare']['flighttax']);
									$othervharges = round($resu['Fare']['flightothercharges']);

									$ServiceFee = $resu['Fare']['ServiceFee'];
									$flightlastprice = round($resu['Fare']['flightmainprice']);
									if(!empty($resu_return))
									{
										$publishvaleu_return=round($resu_return['Fare']['rflightfare']) + round($resu_return['Fare']['rflighttax'])+ round($resu_return['Fare']['rflightothercharges']);
										$basefare_return = round($resu_return['Fare']['rflightfare']);
										$taxfare_return = round( $resu_return['Fare']['rflighttax']);
										$othervharges_return =round( $resu_return['Fare']['rflightothercharges']);
										$flightlastprice_return =round( $resu_return['Fare']['rflightmainprice']);


									}
									else
									{
										$publishvaleu_return=0;
										$basefare_return=0;
										$taxfare_return=0;
										$ServiceFeereturn=0;
										$othervharges_return=0;
										$flightlastprice_return=0;
									}

									$newpublishvalue = $publishvaleu + $publishvaleu_return;
									$basefare=$basefare1 + $basefare_return ;
									$taxfare = $taxfare1 + $taxfare_return;

									$otherchar = $othervharges+$othervharges_return;
									$mainvalue = number_format($newpublishvalue);
									?>
									<div class="booking-price">
										<input type="hidden" value="{{$newpublishvalue}}" id="totalnetprice">
										<input type="hidden" value="{{$basefare1}}" id="marginflightprice">
										<input type="hidden" value="{{$basefare_return}}" id="r_marginflightprice">
										<input type="hidden" value="{{$flightlastprice}}" id="mflightprice">
										<input type="hidden" value="{{$flightlastprice_return}}" id="r_mflightprice">
										<input type="hidden" value="{{$taxfare1}}" id="mtaxfare">
										<input type="hidden" value="{{$taxfare_return}}" id="r_mtaxfare">
										<input type="hidden" value="{{$othervharges}}" id="mothercharge">
										<input type="hidden" value="{{$othervharges_return}}" id="r_mothercharge">



										<input type="hidden" value="{{$resu['Fare']['flightcurrency']}}" id="curacu">

										<input type="hidden" value="0" id="mainnetprice">
										<h4 class="traveller">{{$adults}} {{$child}} {{$Infant}}</h4>
										<div class="inner-box">
											<div class="flight-total-price">
												<div class="inner-div">
													<div class="total-base-price left-price">Total Base Price <span class="base-price right-price"><i class="fa {{$resu['Fare']['flightcurrency']}}"></i> {{number_format($basefare)}}</span></div>
												</div>
												<div class="inner-div">
													<div class="total-taxes left-price">Total Taxes  <span class="tax right-price"><i class="fa {{$resu['Fare']['flightcurrency']}}"></i>{{number_format($taxfare)}}</span></div>
												</div>
												<div class="inner-div">
													<div class="total-taxes left-price">Other Charges <span class="tax right-price"><i class="fa {{$resu['Fare']['flightcurrency']}}"></i>{{number_format($otherchar)}}</span></div>
												</div>

												<div class="inner-div" id="checkbag" >
													<div class="total-taxes left-price" id="bagprice"></div>
												</div>
												<div class="inner-div" id="checkbag" >
													<div class="total-taxes left-price" id="bagprice_return_show"></div>
												</div>

												<div class="inner-div" id="checkmeal" >
													<div class="total-taxes left-price" id="mealprice"></div>
												</div>
												<div class="inner-div" id="checkmeal" >
													<div class="total-taxes left-price" id="mealprice_r"></div>
												</div>
												<div class="inner-div air-fare-border">
													<div class="total-airfare left-price">Total Airfare <span class="airfare right-price" id="netprice"><i class="fa {{$resu['Fare']['flightcurrency']}}"></i> {{$mainvalue}}</span></div>
												</div>
												<!-- <div class="inner-div">
													<div class="travel-insurance left-price" >GST (5%) <span class="insurance right-price" id="gstprice"><i class="fa fa-inr"></i> </span></div>
												</div> -->
												<div class="inner-div" style="display: none">
													<div class="travel-insurance left-price">Travel Insurance (1 x Rs.249) <span class="insurance right-price"><i class="fa {{$resu['Fare']['flightcurrency']}}"></i> 249</span></div>
												</div>
												<div class="inner-div" style="display: none">
													<div class="total-convenience-fee left-price">Total Convenience Fee <span class="convenience right-price"><i class="fa {{$resu['Fare']['flightcurrency']}}"></i> 279</span></div>
												</div>
												<div class="inner-div">
													<div class="grand-total left-price">Grand Total <span class="grand right-price" id="grandtotal"><i class="fa {{$resu['Fare']['flightcurrency']}}" style="font-size:20px"></i>{{$mainvalue}}</span></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php
								if($mainarray['pricechangereturn'] =='' ||$mainarray['pricechangeoneway']=="")
								{

								}
								else
								{
								?>
								<div class="price-cng-div">
								<form action="{{route('flightbook')}}" method="post">
											 <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
											<input type="hidden"  name="resultindex" value="{{$resu['ResultIndex']}}">
											<?php
											if(!empty($resu_return))
											{
											?>
											<input type="hidden"  name="resultindex1" value="{{$resu_return['ResultIndex']}}">
											<?php } ?>
											<input type="hidden" name="journytype" value="{{$mainarray['journytype']}}">
											<input type="hidden"  id="flightorign" value="{{$mainarray['airportorignname']}}">
											<input type="hidden" name="flightdep" id="flightdep" value="{{$mainarray['airportdeptname']}}">
											<input type="hidden" name="flightname" value="{{$mainarray['airlinename']}}">
											<button type="button" class="price-cng-btn flight-book-btn">Price Update
											</button>

										</form>
										</div>
								<?php
								}
								?>



							</div>
						</div>
					</div>
				</section>
			</div>
        </div>
    </div>
    <div class="modal fade myModal" id="myModaln1" role="dialog" data-backdrop="static" data-keyboard="false" style="background-color: #f0f8ffd1;">
	<div class="modal-dialog">
	<center><img src="{{ asset('assets/images/newf.gif')   }}" style="height: auto;width: auto; display: block;margin:60% auto;position: relative;top:50%;transform: translateY(-50%)"></center>
	</div>
</div>
	<div class='session-counter'>
		<p class='timer' data-minutes-left=15>Your booking session will expire in </p>
		<section class='actions'></section>
	</div>
	<style>
		.session-counter {
			background: #000000d9;
			color: white;
			padding: 5px;
			position: fixed;
			bottom: 0;
			display: block;
			width: 100%;
			z-index: 1;
		}

		.session-counter p{
			margin: 0;
			color: white;
			font-size: 17px;
			font-weight: 500;
			text-align: center;
			z-index: 1;
		}
		.jst-hours,.jst-minutes,.jst-seconds {
			 display: inline;
			 color: orange;
		 }

	</style>

    <!-- Footer -->@include('pages.include.footer')
    <!-- Copyright -->@include('pages.include.copyright')
    <script>
        $(document).ready(function() {
            $(".btn-modify").click(function() {
                $(".hotel-result-modify").toggle();
            });
        });
    </script>
    <script>


    	$(document).on('click','input[name="chkgst"]',function(){
    		 if($(this).is(":checked")){

    		 	$('#showgst').show();
    		 	$('#gstaddress').val("");
    		 	$('#gstemail').val("");
    		 	$('#gstnumber').val("");
    		 	$('#gstcompname').val("");
    		 	$('#gstcontactno').val("");





            }
            else if($(this).is(":not(:checked)")){
            	$('#showgst').hide();

            }
    	});
    	$(document).on('click','input[name="meal"]',function(){
    		 if($(this).is(":checked")){
    		 	$('#showmeal').show();

            }
            else if($(this).is(":not(:checked)")){
            	$('#showmeal').hide();

            }
    	})
    </script>
    <script>
		$(function () {
	    //Departure
	    var date = new Date();
		date.setDate(date.getDate());
	    $('.adultdob').datepicker({
	    	autoclose: true,
	    	todayHighlight: true,
	    	format: 'dd/mm/yyyy',


	    })
	    //Date picker
	    $('.childdob').datepicker({
	    	autoclose: true,
	    	todayHighlight: true,
	    	format: 'dd/mm/yyyy',

	    })
	    //Check In
	    $('.infantdob').datepicker({
	    	autoclose: true,
	    	todayHighlight: true,
	    	format: 'dd/mm/yyyy',

	    })
	    //Date picker


})
</script>
<script>
	$(document).on('click','.makepyament',function()
	{

		 var d = new Date();
		var strDate1 = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
		var strDate = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
		var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var error=0;
		var year1=0;
		var adultyear=0;
		var gstinformat = new RegExp('^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$');
		$('.aducount').each(function(){
		 	var aducount =$(this).val();
		 	if($("#adultlastname-"+aducount).val()=='')
			{
				if($('#adultfirstname-'+aducount).val()!='')
				{
					var adultfirstname= $('#adultfirstname-'+aducount).val();
					$("#adultlastname-"+aducount).val(adultfirstname);
					var title = $("#adulttitle-"+aducount).val();
					$('#adultfirstname-'+aducount).val(title);
				}


			}

		});
		if($('#childcount').val() > 0)
		{
			$('.childcountnew').each(function(){
				var childchu =$(this).val();
				if($('#childlastname-'+childchu).val() =='')
				{
					var childfname = $('#childfirstname-'+childchu).val();
					if(childfname !='')
					{
						$('#childlastname-'+childchu).val(childfname);
						var childtt = $('#childttile-'+childchu).val();
						$('#childfirstname-'+childchu).val(childtt);
					}


				}
			});
		}
		if($('#infacount').val() > 0)
		{
			$('.infanctcountnew').each(function(){
				var infacount = $(this).val();
				if($('#infalastname-'+infacount).val()=='' )
				{
					if($('#infafistname-'+infacount).val()!='')
					{
						var infafname = $('#infafistname-'+infacount).val();
						$('#infalastname-'+infacount).val(infafname);
						var titleinfa = $('#infatitle-'+infacount).val();
						$('#infafistname-'+infacount).val(titleinfa);


					}
				}
			});
		}
		$(".filltext").each(function()
		{
			if($(".adulttitle").val()=="0")
			{
				error++;

				$(".adulttitle").siblings('.textfield_error').text("Select Adult Title");

				$(".adulttitle").siblings('.textfield_error').css({
					"color":"red",
					"font-size": "11px",

				});
				$(".adulttitle").focus();
			}
			if($(".childtitle").val()=="0")
			{
				error++;
				$(".childtitle").siblings('.textfield_error').text("Select Child Title");
				$(".childtitle").siblings('.textfield_error').css({
					"color":"red",
					"font-size": "11px",
				});
				$(".childtitle").focus();
			}
			if($(".infatitle").val()=="0")
			{
				error++;
				$(".infatitle").siblings('.textfield_error').text("Select Infant Title");
				$(".infatitle").siblings('.textfield_error').css({
					"color":"red",
					"font-size": "11px",
				});
				$(".infatitle").focus();
			}
			if($(".adulttitle").val()!="0")
			{

				$(".adulttitle").siblings('.textfield_error').text("");
				$(".adulttitle").siblings('.textfield_error').css({
					"color":"red",
					"font-size": "11px",
				});
			}
			if($(".childtitle").val()!="0")
			{

				$(".childtitle").siblings('.textfield_error').text("");
				$(".childtitle").siblings('.textfield_error').css({
					"color":"red",
					"font-size": "11px",
				});
			}
			if($(".infatitle").val()!="0")
			{

				$(".infatitle").siblings('.textfield_error').text("");
				$(".infatitle").siblings('.textfield_error').css({
					"color":"red",
					"font-size": "11px",
				});
			}
			if($(".country").val()=="0")
			{
				error++;

				$(".country").siblings('.textfield_error').text("Select Country");
				$(".country").siblings('.textfield_error').css({
					"color":"red",
					"font-size": "11px",
				});
			}
			if($(".country").val()!="0")
			{


				$(".country").siblings('.textfield_error').text("");
				$(".country").siblings('.textfield_error').css({
					"color":"red",
					"font-size": "11px",
				});
			}

			if($(".childlastname").val()=='')
			{
				var childfname = $(".childfirstname").val();
				$(".childlastname").val(childfname);
				var childti = $(".childtitle").val();
				$(".childfirstname").val(childti);

			}
			if($(".infalastname").val()=="")
			{
				var infafname = $(".infafistname").val();
				$(".infalastname").val(infafname);
				var infat = $(".infatitle").val();
				$(".infafistname").val(infat);

			}
			if($(this).val().trim()=='')
			{

				error++;
				$(this).siblings('.textfield_error').text("Cannot be empty");
				$(this).siblings('.textfield_error').css({
					"color":"red",
					"font-size": "11px",
				});
				$(this).focus();
			}
			if($(this).val().trim()!='')
			{

				$(this).siblings('.textfield_error').text("");
				$(this).siblings('.textfield_error').css({
					"color":"red",

				});
			}
			// if($(".childtitle").val()=="")
			// {
			// 	error++;
			// 	$(".childtitle").siblings('.textfield_error').text("Cannot be empty dob ");
			// 	$(".childtitle").siblings('.textfield_error').css({
			// 		"color":"red",
			// 		"font-size": "11px",
			// 	});
			// }
			if($(".adultdob").val().trim()!="")
			{
				var adultdob= $(".adultdob").val();

				var partsFrom = adultdob.split('/');
		        var partsTo = strDate.split('/');
		        var date1 = new Date(partsFrom[2], partsFrom[1] - 1, partsFrom[0]);
		        var date2 = new Date(partsTo[2], partsTo[1] - 1, partsTo[0]);
				var diff = Math.floor(date2.getTime() - date1.getTime());
		        var day = 1000 * 60 * 60 * 24;
		         var days = Math.floor(diff / day);
	              var months = Math.floor(days / 31);
	              var years = Math.floor(months / 12);
				adultyear=years;


			}
			if($(".adultdob").val().trim()!="" &&   adultyear <= 17 )
			{

				error++;
				$(".adultdob").siblings('.textfield_error').text("Please Adult age shoud be greater then 18 years");
				$(".adultdob").siblings('.textfield_error').css({
					"color":"red",
					"font-size": "11px",
				});
				$(".adultdob").focus();

			}
			if($('#childcount').val() > 0)
			{
				if($(".childdob").val().trim()!="" )
				{
					var childdob= $(".childdob").val();
					var partsFrom = childdob.split('/');
			         var partsTo = strDate.split('/');
			         var date1 = new Date(partsFrom[2], partsFrom[1] - 1, partsFrom[0]);
			         var date2 = new Date(partsTo[2], partsTo[1] - 1, partsTo[0]);
					var diff = Math.floor(date2.getTime() - date1.getTime());
			        var day = 1000 * 60 * 60 * 24;
					var days = Math.floor(diff / day);
		            var months = Math.floor(days / 31);
		            var years = Math.floor(months / 12);
		            year1=years;
				}
				if($(".childdob").val().trim()!="" && (year1 < 2  || year1 >= 17))
				{
					error++;
					$(".childdob").siblings('.textfield_error').text("Please child age shoud be between 2 years to 17 years");
					$(".childdob").siblings('.textfield_error').css({
						"color":"red",
						"font-size": "11px",
					});
					$(".childdob").focus();

				}
			}
			if($('#infacount').val() > 0)
			{
				if($(".infantdob").val().trim()!="" )
				{
					var infantdob= $(".infantdob").val();
					var partsFrom = infantdob.split('/');
			         var partsTo = strDate.split('/');
			         var date1 = new Date(partsFrom[2], partsFrom[1] - 1, partsFrom[0]);
			         var date2 = new Date(partsTo[2], partsTo[1] - 1, partsTo[0]);
					var diff = Math.floor(date2.getTime() - date1.getTime());
			        var day = 1000 * 60 * 60 * 24;
					var days = Math.floor(diff / day);
		            var months = Math.floor(days / 31);
		            var years = Math.floor(months / 12);
		            year1=years;
				}
				if($(".infantdob").val().trim()!="" && (year1 > 2))
				{
					error++;
					$(".infantdob").siblings('.textfield_error').text("Please child age shoud be below 2 years ");
					$(".infantdob").siblings('.textfield_error').css({
						"color":"red",
						"font-size": "11px",
					});
					$(".infantdob").focus();
				}
			}
				if ($('.email ').val() !='' && !emailfilter.test($('.email ').val()))
				 {
				 	error++;
					$('.email').siblings('.textfield_error').text("Email Id not valid");
					$('.email').siblings('.textfield_error').css({
						"color":"red",
						"font-size": "11px",
					});
					$(".email").focus();
				 }
				 if($('#phone').val().length < 10 || $('#phone').val().length >15 )
				 {
				 	error++;
					$('.phone').siblings('.textfield_error').text("Mobile number should be between of 10 to 15-digits");
					$('.phone').siblings('.textfield_error').css({
						"color":"red",
						"font-size": "11px",
					});
					$(".phone").focus();
				 }
				 if($('#eme_phone').val().length < 10 || $('#eme_phone').val().length >15 )
				 {
				 	error++;
					$('.eme_phone').siblings('.textfield_error').text("Mobile number should be between of 10 to 15-digits");
					$('.eme_phone').siblings('.textfield_error').css({
						"color":"red",
						"font-size": "11px",
					});
					$(".eme_phone").focus();
				 }



		});
		if($('input[name="chkgst"]').is(":checked"))
		{
			$(".filltext_gst").each(function()
			{
				if($(this).val().trim()=='')
				{
					error++;
					$(this).siblings('.textfield_error').text("Cannot be empty");
					$(this).siblings('.textfield_error').css({
						"color":"red",
						"font-size": "11px",
					});
				}
				 if ($('.gstnumber ').val() !='' && !gstinformat.test($('.gstnumber ').val()))
				 {
				 	error++;
					$(".gstnumber").siblings('.textfield_error').text("GST number not valid");
					$(".gstnumber").siblings('.textfield_error').css({
						"color":"red",
						"font-size": "11px",
					});
				 }
				 if ($('.gstemail  ').val() !='' && !emailfilter.test($('.gstemail  ').val()))
				 {
				 	error++;
					$('.gstemail ').siblings('.textfield_error').text("Email Id not valid");
					$('.gstemail ').siblings('.textfield_error').css({
						"color":"red",
						"font-size": "11px",
					});
				 }
				 if($('#gstcontactno').val().length < 10 || $('#gstcontactno').val().length >15 )
				 {
				 	error++;
					$('.gstcontactno').siblings('.textfield_error').text("Mobile number should be between of 10 to 15-digits");
					$('.gstcontactno').siblings('.textfield_error').css({
						"color":"red",
						"font-size": "11px",
					});
				 }
				if($(this).val().trim()!='')
				{

				$(this).siblings('.textfield_error').text("");
				$(this).siblings('.textfield_error').css({
					"color":"red",
				});
				}
			})
		}
		else
		{
			$(".filltext_gst").each(function()
			{

				$(this).val('');
				$(this).siblings('.textfield_error').text("");
			})
		}

		if(error ==0)
		{
			$('#myModaln1').modal('show');
			var amount = $('#maingradflightprice').val();
			var currency=$('#currency').val();
			var customerfname=$('.adultfirstname').val();
			var customerlname=$('.adultlastname').val();
			var customername =customerfname + ' ' + customerlname;
			var customerEmail = $('.email').val();
			var customerPhone = $('#phone').val();
			var csrf_token = $('#csrf_token').val();
			var dataString = $("#contact_form").serialize();
			var convertcurr =$('#convertcurrecny').val();

			$.ajax({
					url : "{{route('request1')}}",
					data : {
							'amount' : amount,
							'currency' : currency,
							'customername' : customername,
							'customerEmail' : customerEmail,
							'customerPhone' : customerPhone,
							'csrf_token' : csrf_token,
							'dataString' : dataString,
							'customerfname' : customerfname,
							'convertcurr':convertcurr,
							'customerlname' :customerlname,
							},
					type:'get',

					success : function(data)
					{
						if(data=='INR')
						{
							window.location="{{url('request')}}";
						}
						else
						{
							$('#paymentcard').fadeIn(1500);
							$('.makepyament').hide();
						}
						$('#myModaln1').modal('hide');



					},
			});
		}


	});
</script>
<script>
	$(document).on('change','#baggage',function(){
		var bagprice=$(this).val();
		bagprice1 = bagprice.split('%');
		var mainbagprice = bagprice1[1];
		var mainorgprice = bagprice1[7];
		var bagweight = bagprice1[0];
		var curacu = $('#curacu').val();
		$('#bagprice').html('Baggage Charges ('+bagprice1[0]+'Kg) <span class="tax right-price"><i class="fa '+curacu+'"></i>'+ mainbagprice +'</span>');
		var mainprice= $('#totalnetprice').val();
		var lastprice = $('#mainnetprice').val();
		var newbagprice = parseInt(lastprice) + parseInt(mainbagprice);
		var netprice = parseInt(mainprice) + parseInt(newbagprice);

		$('#netprice').html('<i class="fa '+curacu+'"></i>'+ netprice +'');
		$('#mainnetprice').val(newbagprice);
		$('#bagweight').val(bagweight);
		$('#newbagprice').val(mainbagprice);
		$('#orgbagprice').val(mainorgprice);

		$('#grandtotal').html('<i class="fa '+curacu+'" style="font-size:20px"></i>'+ netprice +'');
		$('#maingradflightprice').val(netprice);


	});
	// gstprice
	$(document).ready(function(){
		var netpricenew = $('#totalnetprice').val();
		var curacu = $('#curacu').val();
		var marginflightprice=$('#marginflightprice').val();
		var r_marginflightprice=$('#r_marginflightprice').val();
		var mflightprice=$('#mflightprice').val();
		var r_mflightprice=$('#r_mflightprice').val();
		var mtaxfare=$('#mtaxfare').val();
		var r_mtaxfare=$('#r_mtaxfare').val();
		var mothercharge=$('#mothercharge').val();
		var r_mothercharge=$('#r_mothercharge').val();

		$('#grandtotal').html('<i class="fa '+curacu+'" style="font-size:20px"></i>'+ netpricenew +'');
		$('#maingradflightprice').val(netpricenew);
		$('#flightbaseprice').val(mflightprice);
		$('#flightbaseprice_r').val(r_mflightprice);
		$('#flightbasemarginprice').val(marginflightprice);
		$('#flightbasemarginprice_r').val(r_marginflightprice);
		$('#flighttaxprice').val(mtaxfare);
		$('#flighttaxprice_r').val(r_mtaxfare);
		$('#flightotherprice').val(mothercharge);
		$('#flightotherprice_r').val(r_mothercharge);
		$('#flightcurrencyicon').val(curacu);
	})
</script>
<script>
	$(document).on('change','#mealshowprice',function(){
		var mealprice=$(this).val();
		mealprice1 = mealprice.split('%');
		mealtotalprice = mealprice1[2];
		mealorgprice = mealprice1[4];
		var curacu = $('#curacu').val();
		var lastprice = $('#mainnetprice').val();
		$('#mealprice').html('Meal Charges <span class="tax right-price"><i class="fa '+curacu+'"></i>'+ mealtotalprice +'</span>');
		var mainprice= $('#totalnetprice').val();

		var newmealprice = parseInt(lastprice) + parseInt(mealtotalprice);

		var netprice = parseInt(mainprice) + parseInt(newmealprice);
		$('#netprice').html('<i class="fa '+curacu+'"></i>'+ netprice +'');
		$('#mainnetprice').val(newmealprice);
		// var gst = (netprice * 5 )/100;
		// var gst1 = Math.round(gst)
		// $('#gstprice').html('<i class="fa fa-inr"></i>'+ gst1 +'');
		// var gtotal =  parseInt(netprice) + parseInt(gst1);
		$('#grandtotal').html('<i class="fa '+curacu+'" style="font-size:20px" ></i>'+ netprice +'');
		$('#totalmealprice').val(mealtotalprice);
		$('#totalorgmealprice').val(mealorgprice);
		$('#totalmealname').val(mealprice1[3]);
		$('#maingradflightprice').val(netprice);


	});

</script>
<script>
	$(document).on('change','#mealdynamicreturn',function(){

		var mealprice=$(this).val();
		mealprice1 = mealprice.split('%');
		mealtotalprice = mealprice1[2];
		mealorgprice = mealprice1[4];
		var curacu = $('#curacu').val();
		var lastprice = $('#mainnetprice').val();
		$('#mealprice_r').html('Meal Charges Return <span class="tax right-price"><i class="fa '+curacu+'"></i>'+ mealtotalprice +'</span>');
		var mainprice= $('#totalnetprice').val();

		var newmealprice = parseInt(lastprice) + parseInt(mealtotalprice);

		var netprice = parseInt(mainprice) + parseInt(newmealprice);
		$('#netprice').html('<i class="fa '+curacu+'"></i>'+ netprice +'');
		$('#mainnetprice').val(newmealprice);
		// var gst = (netprice * 5 )/100;
		// var gst1 = Math.round(gst)
		// $('#gstprice').html('<i class="fa fa-inr"></i>'+ gst1 +'');
		// var gtotal =  parseInt(netprice) + parseInt(gst1);
		$('#grandtotal').html('<i class="fa '+curacu+'" style="font-size:20px" ></i>'+ netprice +'');
		$('#totalmealprice_return').val(mealtotalprice);
		$('#totalorgmealprice_return').val(mealorgprice);
		$('#totalmealname_return').val(mealprice1[3]);
		$('#maingradflightprice').val(netprice);

			});

</script>
<script>
	$(document).on('change','#baggage_return',function(){
		var bagprice=$(this).val();
		bagprice1 = bagprice.split('%');
		var mainbagprice = bagprice1[1];
		var mainorgprice = bagprice1[7];
		var bagweight = bagprice1[0];
		var curacu = $('#curacu').val();
		$('#bagprice_return_show').html('Baggage Charges Return ('+bagprice1[0]+'Kg) <span class="tax right-price"><i class="fa '+curacu+'"></i>'+ mainbagprice +'</span>');
		var mainprice= $('#totalnetprice').val();
		var lastprice = $('#mainnetprice').val();
		var newbagprice = parseInt(lastprice) + parseInt(mainbagprice);
		var netprice = parseInt(mainprice) + parseInt(newbagprice);

		$('#netprice').html('<i class="fa '+curacu+'"></i>'+ netprice +'');
		$('#mainnetprice').val(newbagprice);
		$('#bagweight_return').val(bagweight);
		$('#newbagprice_return').val(mainbagprice);
		$('#orgbagprice_return').val(mainorgprice);
		// var gst = (netprice * 5 )/100;
		// var gst1 = Math.round(gst)
		// $('#gstprice').html('<i class="fa fa-inr"></i>'+ gst1 +'');
		// var gtotal =  parseInt(netprice) + parseInt(gst1);
		$('#grandtotal').html('<i class="fa '+curacu+'" style="font-size:20px"></i>'+ netprice +'');
		$('#maingradflightprice').val(netprice);
	});
</script>
<!-- <script>
	$(document).on('click','.makepyament',function(){
		alert('makepayment');
	})
</script> -->
<script>
$(function () {
   //Departure
   var date = new Date();
   date.setDate(date.getDate());
   $('.departure').datepicker({
   	autoclose: true,
   	todayHighlight: true,
   	format: 'dd/mm/yyyy',
   	startDate: date

   })
   //Date picker
   $('#return').datepicker({
   	autoclose: true,
   	todayHighlight: true,
   	format: 'dd/mm/yyyy',
   	startDate: date
   })
   //Check In
   $('#check-in').datepicker({
   	autoclose: true,
   	todayHighlight: true,
   	format: 'dd/mm/yyyy',
   	startDate: date
   })
   //Date picker
   $('#check-out').datepicker({
   	autoclose: true,
   	todayHighlight: true,
   	format: 'dd/mm/yyyy',
   	startDate: date
   })

})
</script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<!-- TO DO : Place below JS code in js file and include that JS file -->
<script>
    $(document).on('blur', '.textfield', function()
    {
        var id=this.id;
        var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if($('#'+id).val().trim()!="")
        {
            $('#'+id+'_error').text('');
            $('#'+id+'_error').hide();
        }
        if(id=="name" && $('#'+id).val().trim()=="")
        {

            $('#'+id+'_error').text('Please Enter Card Holder Name');
            $('#'+id+'_error').show();
        }
        if(id=="card" && $('#'+id).val().trim()=="")
        {
            $('#'+id+'_error').text('Please Enter Card Number');
            $('#'+id+'_error').show();
        }
        if(id=="expmonth" && $('#'+id).val().trim()=="")
        {
            $('#'+id+'_error').text('Enter Month');
            $('#'+id+'_error').show();
        }
        if(id=="expyear" && $('#'+id).val().trim()=="")
        {
            $('#'+id+'_error').text('Enter Year');
            $('#'+id+'_error').show();
        }
        if(id=="cvc" && $('#'+id).val().trim()=="")
        {
            $('#'+id+'_error').text('Enter CVC');
            $('#'+id+'_error').show();
        }
    });
</script>

<script>

    Stripe.setPublishableKey('<?php echo $params['public_test_key']; ?>');
    $(function () {
        var $form = $('#payment-form');
        $form.submit(function (event) {
        $('.textfield_error').text('');
        $('.textfield_error').hide();
        var name=$('#name').val();
        var card=$('#card').val();
        var expmonth=$('#expmonth').val();
        var expyear=$('#expyear').val();
        var cvv=$('#cvv').val();
        if(name=="")
        {
            $('#name_error').text("Please Enter Name");
            $('#name_error').show();
        }
        else if(card=="")
        {
            $('#card_error').text("Please Enter Card Number");
            $('#card_error').show();
        }
        else if(expmonth=="")
        {
            $('#expmonth_error').text("Enter exp month ");
            $('#expmonth_error').show();
        }
        else if(expyear=="")
        {
            $('#expyear_error').text("Enter exp year ");
            $('#expyear_error').show();
        }
        else if(cvv=="")
        {
            $('#cvv_error').text("Enter CVV Number ");
            $('#cvv_error').show();
        }
        else
        {
            $form.find('.submit').prop('disabled', true);
            // Request a token from Stripe:
            Stripe.card.createToken($form, stripeResponseHandler);
            // Prevent the form from being submitted:

        }
        return false;

        });
    });
    function stripeResponseHandler(status, response) {
        // Grab the form:

    var $form = $('#payment-form');

    if (response.error) {
        // Problem!
    // Show the errors on the form:
    $form.find('.payment-errors').text(response.error.message);
    $form.find('.submit').prop('disabled', false);
    // Re-enable submission
    } else {
    // Token was created!
    // Get the token ID:
    var token = response.id;
    // Insert the token ID into the form so it gets submitted to the server:
    $form.append($('<input type="hidden" name="stripeToken">').val(token));
    // Submit the form:
    $form.get(0).submit();
    }
    };

</script>
</body>

</html>