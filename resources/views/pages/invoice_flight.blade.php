<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php
    $flightbook=session()->get('flightaraay1');
    $flightbook_return=session()->get('flightaraay2');

    $segment = unserialize($flightbook->segment);
        $bookdate1 = explode('T',$segment[0]['Origin']['DepTime']);
                    $bookingdate= date("d M Y" , strtotime($bookdate1[0]));
                    $bookingtime = date("H:s" , strtotime($bookdate1[1]));
                    $imgsou =  'assets/images/flag/'.$segment[0]['Airline']['AirlineCode'].'.gif';
                    
        ?>
<table style="width: 80% ; margin: 80px auto; border: 2px solid #e0e0e0; padding: 20px">
    <tr>
        <td style="color: #fa9e1b; font-size: 22px; font-weight: bold">E-ticket</td>
        <td colspan="2" style="text-align: right"><img src="{{asset('assets/images/logo.png')}}" width="150px"></td>
    </tr>
    <tr>
        <td colspan="3" style="padding-top: 30px">
            <p class="book-id">Booking ID : {{$flightbook->bookingId}}</p>
            <p class="book-id"> Booking Date: {{$bookingdate}} {{$bookingtime}}</p>
        </td>

    </tr>
    <tr>
        <td style="border-bottom: 1px solid #fa9e1b">
            <p class="o-flight">Onward Flight</p>
            <p class="t-flight">{{$flightbook->orign}} to {{$flightbook->destination}}</p>
        </td>
        <td colspan="2"  style="border-bottom: 1px solid #fa9e1b; text-align:right;">
            <p class="o-flight">PNR</p>
            <p class="t-flight">{{$flightbook->pnrno}}</p>
        </td >
    </tr>
    <tr>
        <td><p class="t-flight">{{$segment[0]['Airline']['AirlineName']}}  {{$segment[0]['Airline']['AirlineCode']}}-{{$segment[0]['Airline']['FlightNumber']}}</p></td>
        @if($flightbook->refund=='Refundable')
        <td  colspan="2" style="color: green; font-weight: bold; text-align: right;font-size: 19px">Refundable</td>
        @else
        <td  colspan="2" style="color: red; font-weight: bold; text-align: right;font-size: 19px">Non Refundable</td>
        @endif
    </tr>
    <?php
        for($seg_i=0;$seg_i<count($segment);$seg_i++)
        {
            $arvdate1 = explode('T',$segment[$seg_i]['Destination']['ArrTime']);
                                $arvtime2 = date("H:s" , strtotime($arvdate1[1]));
                                $arvtime1 = date("H:i:s" , strtotime($arvdate1[1]));
                                $arvnewdate1= date("Y-m-d" , strtotime($arvdate1[0]));
                                 $arvnewdatenew= date("l d M Y " , strtotime($arvdate1[0]));
                                $devdate1 = explode('T',$segment[$seg_i]['Origin']['DepTime']);
                                $depnewdatedetail= date("l d M Y" , strtotime($devdate1[0]));
                                $depnewtimedetail = date("H:s" , strtotime($devdate1[1]));
                                $depdatetime = date("H:i:s" , strtotime($devdate1[1]));
                                $depdatedistance= date("Y-m-d" , strtotime($devdate1[0]));
                                $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $depdatedistance.''. $depdatetime);
                                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $arvnewdate1.''. $arvtime1);
                                $totalDuration = $from->diffInSeconds($to);
                                $durationhour =  gmdate('H', $totalDuration);
                                $durationmin =  gmdate('s', $totalDuration);
                                $newtime=$durationhour.'H : '.$durationmin.'M';
        ?>
    <tr>
        <td colspan="1" style="border-bottom: 1px solid #fa9e1b">
            <p>{{$segment[$seg_i]['Origin']['Airport']['AirportCode']}} {{$depnewtimedetail}}</p>
            <p>{{$depnewdatedetail}}</p>
           <p> {{$segment[$seg_i]['Origin']['Airport']['CityCode']}}, {{$segment[$seg_i]['Origin']['Airport']['AirportName']}}, Terminal {{$segment[$seg_i]['Origin']['Airport']['Terminal']}}</p>

        </td>
        <td colspan="1" style="border-bottom: 1px solid #fa9e1b">
            <img src="{{asset('assets/images/clock-img.png')}}" style="width: 20px;height: 20px;    display: block;
    margin: auto;">
            <p style="text-align:center;">{{$newtime}}
                {{$flightbook->flightcabinclass}}</p>
        </td>
        <td colspan="1" style="text-align:right;border-bottom: 1px solid #fa9e1b">
            <p>
                {{$segment[$seg_i]['Destination']['Airport']['AirportCode']}} {{$arvtime2}}</p>
            <p>{{$arvnewdatenew}}</p>
            <p>{{$segment[$seg_i]['Destination']['Airport']['CityCode']}}, {{$segment[$seg_i]['Destination']['Airport']['AirportName']}}, Terminal {{$segment[$seg_i]['Destination']['Airport']['Terminal']}}
            </p>
        </td>
    </tr>
   <?php } ?>
    <tr>
        <td colspan="3">
            <table border="1" style="width: 100% ; margin: 80px auto; border-collapse: collapse">
                 <tr>
                    <th style="text-align: center;color: #fa9e1b;">
                        <p>TRAVELLER</p>
                    </th>
                    <th style="text-align: center;color: #fa9e1b;">
                        <p>TICKET ID</p>
                    </th>
                    <th style="text-align: center;color: #fa9e1b;">
                        <p>TICKET NO.</p>
                    </th>
                </tr>
                <?php
                    $passengers = unserialize($flightbook->passenger_detail);
                  
                    for($pass=0;$pass< count($passengers);$pass++)
                    {
                        if($passengers[$pass]['PaxType']=='1')
                        {
                            $passcheck='Adults';
                        }
                        else if($passengers[$pass]['PaxType']=='2')
                        {
                            $passcheck='Child';
                        }
                        else if($passengers[$pass]['PaxType']=='3')
                        {
                            $passcheck='Infant';
                        }
                        else
                        {
                            $passcheck='';
                        }
                    ?>
                <tr>
                    <td style="text-align: center">
                        <p>{{$passengers[$pass]['Title']}}. {{ucfirst($passengers[$pass]['FirstName'])}} {{ucfirst($passengers[$pass]['LastName'])}} ({{$passcheck}})</p>
                    </td>
                    @if(!empty($passengers[$pass]['Ticket']['TicketId']))
                    <td style="text-align: center">
                        <p>{{$passengers[$pass]['Ticket']['TicketId']}}</p>
                    </td>
                    <td style="text-align: center">
                        <p>{{$passengers[$pass]['Ticket']['TicketNumber']}}</p>
                    </td>
                     @else
                     <td style="text-align: center">
                        <p>{{$flightbook->pnrno}}</p>
                    </td>
                    @endif

                </tr>
                <?php } ?>
               
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="    background: #f0f0f0;border-radius: 5px;">
            <h3 style="text-align: center;color: #ffbf05;">Important</h3>
            <ul style="padding: 10px 60px;">
                <li style="margin-bottom: 20px">
                    Please carry your Government ID proof for all passengers to show during security check and
                    check-in. Name on Government ID proof should be same as on your ticket.
                </li>
                <li>
                    Please carry your Government ID proof for all passengers to show during security check and
                    check-in. Name on Government ID proof should be same as on your ticket.
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td colspan="3">
        <h3 style="text-align: center;color: #ffbf05;">Baggage Policy</h3>
            <table border="1" style="background: #fdf5ea; border-color: #516bf0;">
                <?php
                    $flightfare = unserialize($flightbook->fare);
                        
                    if($flightbook->mealprice !='' || $flightbook->mealprice =='0')
                    {
                         $mealprice = $flightbook->mealprice;
                    }
                    else
                    {
                         $mealprice=0;
                    }
                    if($flightbook->bag_price !='' || $flightbook->bag_price =='0')
                    {
                        $baggprice = $flightbook->bag_price;
                    }
                    else
                    {
                        $baggprice=0;
                    }
                    $othercharges=$flightbook->flightothercharges;  
                    $basefare =$flightbook->flightmarginprice;
                    $taxfare = $flightbook->flighttax;
                    

                    $totalamount =  $basefare +  $taxfare + $othercharges + $mealprice + $baggprice ;
                    if($flightbook->currency_icon=='fa-inr')
                    {
                        
                         $imgicon = "Rs.";

                    }
                    else
                    {
                        $imgicon="$";
                    }
                    // $gst =( $totalamount1 * 5 ) /100;
                    // $totalamount = $totalamount1 + $gst;
                    ?>
                <tr>

                    <th colspan="1" style="padding: 15px">Check-In(Adult & Child)</th>
                    <td  colspan="2" style="text-align:right; padding: 15px">As per Airline Policy</td>

                </tr>

                <tr>
                    <th style="padding: 15px">Hand-Baggage(Adult & Child)</th>
                    <td style="text-align:right;padding: 15px">7 KG / person</td>

                </tr>

                <tr>

                    <th style="padding: 15px">Terms & Conditions</th>

                    <td style="text-align:right; padding: 15px">

                        <p>Please check with the airline on the dimensions of the baggage</p>

                        <p>The baggage policy is only indicative and can change any time. You are advised to check with the airline before travel to know latest baggage policy</p>

                        <p>You are advised to check with the airline for extra baggage charges</p>

                    </td>

                </tr>

            </table>


        </td>
    </tr>

    <tr>
        <td colspan="3">
            <h3 style="text-align: center;color: #ffbf05;">Cancellation Policy</h3>
            <table class="" border="1" style="background: #fdf5ea; border-color: #516bf0;">

                <tr>

                    <th style="padding: 15px">BOOKINGS & ITINERARY</th>

                    <td style="text-align:right; padding: 15px">The reference number on your booking is confirmed and the same can be checked with the airlines. Kindly check your Booking carefully for
                        correct dates,times of all flights and that all passengers names are correct as per their passport. It is the traveller's reponsibility to check their
                        details on booking. No name changes are allowed once the ticket is issued.
                        </td>

                </tr>

                <tr>

                    <th style="padding: 15px" >E TICKETS

                    </th>

                    <td style="text-align:right;">Etickets may issued for within 48hours of making the payment. In the event you do not receive the e-ticket from us please contact the Travel
                        Agency. We will not be responsible for the ticket if the aforementioned time limit has passed and the ticket is not received by the passenger.
                        For further clarifications please, visit our website www.sekaptravels.com.
                        
                        </td>

                </tr>

                <tr>

                    <th  style="padding: 15px">VISA REQUIREMENTS
                    </th>

                    <td style="text-align:right; padding: 15px">We do not deal in Visa therefore, please refer to the local counselate for Visa requirements.
                    </td>

                </tr>




                <tr>

                    <th  style="padding: 15px">FARES
                    </th>

                    <td style="text-align:right; padding: 15px">Prices quoted at time of booking or enquiry are not guaranteed until paid in full. Fares are subject to change according to availability.

                    </td>

                </tr>
                <tr>

                    <th  style="padding: 15px">CANCELLATION OR CHANGES

                    </th>

                    <td style="text-align:right; padding: 15px">Cancellation and change in dates may vary according to class or fare rules on which the ticket is issued. Some tickets may be
                        non-refundable and non-changeable. The tickets with flexibility can be changed by paying penalty plus fare difference (which varies according
                        to the availability).
                        Any refund of the ticket shall be processed within 6 to 8 weeks of the date when refund is applied.
                        
                    </td>

                </tr>

                <tr>

                    <th  style="padding: 15px">CHECKED BAGGAGE


                    </th>

                    <td style="text-align:right; padding: 15px">The baggage varies from airlines to the classes booked. Please ask for more information about baggage from our consultant.
                    </td>

                </tr>
                <tr>

                    <th  style="padding: 15px">Terms & Conditions</th>

                    <td style="text-align:right; padding: 15px">

                        <p>We accept cancellations, only before 24 Hrs from departure time</p>

                        <p>For cancellations, within 24 hours before departure you need to contact the airline. Post cancellation by airline, you can contact Paytm for refund, if any</p>

                        <p>Convenience fee is non-refundable. Any cashback availed will be adjusted in final refund amount</p>

                    </td>

                </tr>

            </table>


        </td>
    </tr>
    <tr>
        <td colspan="3">
            <p style="color: #ffab41;font-size: 19px;font-weight: bold;">Fare & Payment Details</p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="color: #4c4b4b;font-weight: 700;">Base Fare</p>
            <p style="color: #4c4b4b;font-weight: 700;">Tax</p>
            @if($baggprice !='0')
            <p style="color: #4c4b4b;font-weight: 700;">Extra Baggage Charges({{$flightbook->bag_weight}} Kg)</p>
            @endif
            @if($mealprice !='0')
            <p style="color: #4c4b4b;font-weight: 700;">Meal Charges({{$flightbook->mealname}})</p>
            @endif
            <p style="color: #4c4b4b;font-weight: 700;">Other Charges</p>
            <p style="color: #4c4b4b;font-weight: 700;">Total</p>

        </td>
        <td colspan="2" style="text-align:right;">
            <p style="color:#15298e;font-weight: bold;">{{$imgicon}} {{number_format($basefare,2)}}</p>
            <p style="color:#15298e;font-weight: bold;">{{$imgicon}} {{number_format($taxfare,2)}}</p>
            @if($baggprice !='0')
            <p style="color:#15298e;font-weight: bold;">{{$imgicon}} {{number_format($baggprice,2)}}</p>
            @endif
            @if($mealprice !='0')
            <p style="color:#15298e;font-weight: bold;">{{$imgicon}} {{number_format($mealprice,2)}}</p>
            @endif
            <p style="color:#15298e;font-weight: bold;">{{$imgicon}} {{number_format($othercharges,2)}}</p>
            <p style="color:#15298e;font-weight: bold;">{{$imgicon}} {{number_format($totalamount,2)}}</p>



        </td>
    </tr>

</table>



<!-- flight return -->
<?php
if(!empty($flightbook_return))
{
    $segment_return = unserialize($flightbook_return->segment);
        $bookdate1_return = explode('T',$segment_return[0]['Origin']['DepTime']);
                    $bookingdate_return= date("d M Y" , strtotime($bookdate1_return[0]));
                    $bookingtime_return = date("H:s" , strtotime($bookdate1_return[1]));
                    $imgsou_return =  'assets/images/flag/'.$segment_return[0]['Airline']['AirlineCode'].'.gif';
?>
<table style="width: 80% ; margin: 80px auto; border: 2px solid #e0e0e0; padding: 20px">
    <tr>
        <td style="color: #fa9e1b; font-size: 22px; font-weight: bold">E-ticket</td>
        <td colspan="2" style="text-align: right"><img src="{{asset('assets/images/logo.png')}}" width="150px"></td>
    </tr>
    <tr>
        <td colspan="3" style="padding-top: 30px">
            <p class="book-id">Booking ID : {{$flightbook_return->bookingId}}</p>
            <p class="book-id"> Booking Date: {{$bookingdate_return}} {{$bookingtime_return}}</p>
        </td>

    </tr>
    <tr>
        <td style="border-bottom: 1px solid #fa9e1b">
            <p class="o-flight">Onward Flight</p>
            <p class="t-flight">{{$flightbook_return->orign}} to {{$flightbook_return->destination}}</p>
        </td>
        <td colspan="2"  style="border-bottom: 1px solid #fa9e1b; text-align:right;">
            <p class="o-flight">PNR</p>
            <p class="t-flight">{{$flightbook_return->pnrno}}</p>
        </td >
    </tr>
    <tr>
        <td><p class="t-flight">{{$segment_return[0]['Airline']['AirlineName']}}  {{$segment_return[0]['Airline']['AirlineCode']}}-{{$segment_return[0]['Airline']['FlightNumber']}}</p></td>
        @if($flightbook_return->refund=='Refundable')
        <td  colspan="2" style="color: green; font-weight: bold; text-align: right;font-size: 19px">Refundable</td>
        @else
        <td  colspan="2" style="color: red; font-weight: bold; text-align: right;font-size: 19px">Non Refundable</td>
        @endif
    </tr>
    <?php
        for($seg_i=0;$seg_i<count($segment_return);$seg_i++)
        {
            $arvdate1 = explode('T',$segment_return[$seg_i]['Destination']['ArrTime']);
                                $arvtime2 = date("H:s" , strtotime($arvdate1[1]));
                                $arvtime1 = date("H:i:s" , strtotime($arvdate1[1]));
                                $arvnewdate1= date("Y-m-d" , strtotime($arvdate1[0]));
                                 $arvnewdatenew= date("l d M Y " , strtotime($arvdate1[0]));
                                $devdate1 = explode('T',$segment_return[$seg_i]['Origin']['DepTime']);
                                $depnewdatedetail= date("l d M Y" , strtotime($devdate1[0]));
                                $depnewtimedetail = date("H:s" , strtotime($devdate1[1]));
                                $depdatetime = date("H:i:s" , strtotime($devdate1[1]));
                                $depdatedistance= date("Y-m-d" , strtotime($devdate1[0]));
                                $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $depdatedistance.''. $depdatetime);
                                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $arvnewdate1.''. $arvtime1);
                                $totalDuration = $from->diffInSeconds($to);
                                $durationhour =  gmdate('H', $totalDuration);
                                $durationmin =  gmdate('s', $totalDuration);
                                $newtime=$durationhour.'H : '.$durationmin.'M';
        ?>
    <tr>
        <td colspan="1" style="border-bottom: 1px solid #fa9e1b">
            <p>{{$segment_return[$seg_i]['Origin']['Airport']['AirportCode']}} {{$depnewtimedetail}}</p>
            <p>{{$depnewdatedetail}}</p>
           <p> {{$segment_return[$seg_i]['Origin']['Airport']['CityCode']}}, {{$segment_return[$seg_i]['Origin']['Airport']['AirportName']}}, Terminal {{$segment_return[$seg_i]['Origin']['Airport']['Terminal']}}</p>

        </td>
        <td colspan="1" style="border-bottom: 1px solid #fa9e1b">
            <img src="{{asset('assets/images/clock-img.png')}}" style="width: 20px;height: 20px;    display: block;
    margin: auto;">
            <p style="text-align:center;">{{$newtime}}
                {{$flightbook_return->flightcabinclass}}</p>
        </td>
        <td colspan="1" style="text-align:right;border-bottom: 1px solid #fa9e1b">
            <p>
                {{$segment_return[$seg_i]['Destination']['Airport']['AirportCode']}} {{$arvtime2}}</p>
            <p>{{$arvnewdatenew}}</p>
            <p>{{$segment_return[$seg_i]['Destination']['Airport']['CityCode']}}, {{$segment_return[$seg_i]['Destination']['Airport']['AirportName']}}, Terminal {{$segment_return[$seg_i]['Destination']['Airport']['Terminal']}}
            </p>
        </td>
    </tr>
   <?php } ?>
    <tr>
        <td colspan="3">
            <table border="1" style="width: 100% ; margin: 80px auto; border-collapse: collapse">
                 <tr>
                    <th style="text-align: center;color: #fa9e1b;">
                        <p>TRAVELLER</p>
                    </th>
                    <th style="text-align: center;color: #fa9e1b;">
                        <p>TICKET ID</p>
                    </th>
                    <th style="text-align: center;color: #fa9e1b;">
                        <p>TICKET NO.</p>
                    </th>
                </tr>
                <?php
                    $passengers_return = unserialize($flightbook_return->passenger_detail);
                  
                    for($pass=0;$pass< count($passengers_return);$pass++)
                    {
                        if($passengers_return[$pass]['PaxType']=='1')
                        {
                            $passcheck_return='Adults';
                        }
                        else if($passengers_return[$pass]['PaxType']=='2')
                        {
                            $passcheck_return='Child';
                        }
                        else if($passengers[$pass]['PaxType']=='3')
                        {
                            $passcheck_return='Infant';
                        }
                        else
                        {
                            $passcheck_return='';
                        }
                    ?>
                <tr>
                    <td style="text-align: center">
                        <p>{{$passengers_return[$pass]['Title']}}. {{ucfirst($passengers_return[$pass]['FirstName'])}} {{ucfirst($passengers_return[$pass]['LastName'])}} ({{$passcheck_return}})</p>
                    </td>
                    @if(!empty($passengers_return[$pass]['Ticket']['TicketId']))
                    <td style="text-align: center">
                        <p>{{$passengers_return[$pass]['Ticket']['TicketId']}}</p>
                    </td>
                    <td style="text-align: center">
                        <p>{{$passengers_return[$pass]['Ticket']['TicketNumber']}}</p>
                    </td>
                    @else
                    <td style="text-align: center">
                        <p>{{$flightbook_return->pnrno}}</p>
                    </td>
                    @endif

                </tr>
                <?php } ?>
               
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="    background: #f0f0f0;border-radius: 5px;">
            <h3 style="text-align: center;color: #ffbf05;">Important</h3>
            <ul style="padding: 10px 60px;">
                <li style="margin-bottom: 20px">
                    Please carry your Government ID proof for all passengers to show during security check and
                    check-in. Name on Government ID proof should be same as on your ticket.
                </li>
                <li>
                    Please carry your Government ID proof for all passengers to show during security check and
                    check-in. Name on Government ID proof should be same as on your ticket.
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td colspan="3">
        <h3 style="text-align: center;color: #ffbf05;">Baggage Policy</h3>
            <table border="1" style="background: #fdf5ea; border-color: #516bf0;">
                <?php
                    $flightfare_return = unserialize($flightbook_return->fare);
                        
                    if($flightbook_return->mealprice !='' || $flightbook_return->mealprice =='0')
                    {
                         $mealprice = $flightbook_return->mealprice;
                    }
                    else
                    {
                         $mealprice=0;
                    }
                    if($flightbook_return->bag_price !='' || $flightbook_return->bag_price =='0')
                    {
                        $baggprice = $flightbook_return->bag_price;
                    }
                    else
                    {
                        $baggprice=0;
                    }
                    $othercharges_return=$flightbook_return->flightothercharges;
                    $basefare_return =$flightbook_return->flightmarginprice;
                    $taxfare_return =$flightbook_return->flighttax;
                   

                    $totalamount =  $basefare +  $taxfare + $othercharges + $mealprice + $baggprice ;
                    // $gst =( $totalamount1 * 5 ) /100;
                    // $totalamount = $totalamount1 + $gst;
                    if($flightbook_return->currency_icon=='fa-inr')
                    {
                       
                         $imgicon = "Rs.";

                    }
                    else
                    {
                        $imgicon="$";
                    }
                    ?>
                <tr>

                    <th colspan="1" style="padding: 15px">Check-In(Adult & Child)</th>
                    <td  colspan="2" style="text-align:right; padding: 15px">As per Airline Policy</td>

                </tr>

                <tr>
                    <th style="padding: 15px">Hand-Baggage(Adult & Child)</th>
                    <td style="text-align:right;padding: 15px">7 KG / person</td>

                </tr>

                <tr>

                    <th style="padding: 15px">Terms & Conditions</th>

                    <td style="text-align:right; padding: 15px">

                        <p>Please check with the airline on the dimensions of the baggage</p>

                        <p>The baggage policy is only indicative and can change any time. You are advised to check with the airline before travel to know latest baggage policy</p>

                        <p>You are advised to check with the airline for extra baggage charges</p>

                    </td>

                </tr>

            </table>


        </td>
    </tr>

    <tr>
        <td colspan="3">
            <h3 style="text-align: center;color: #ffbf05;">Cancellation Policy</h3>
            <table class="" border="1" style="background: #fdf5ea; border-color: #516bf0;">
                <tr>

                    <th style="padding: 15px">BOOKINGS & ITINERARY</th>

                    <td style="text-align:right; padding: 15px">The reference number on your booking is confirmed and the same can be checked with the airlines. Kindly check your Booking carefully for
                        correct dates,times of all flights and that all passengers names are correct as per their passport. It is the traveller's reponsibility to check their
                        details on booking. No name changes are allowed once the ticket is issued.
                        </td>

                </tr>

                <tr>

                    <th style="padding: 15px" >E TICKETS
                    </th>

                    <td style="text-align:right;">Etickets may issued for within 48hours of making the payment. In the event you do not receive the e-ticket from us please contact the Travel
                        Agency. We will not be responsible for the ticket if the aforementioned time limit has passed and the ticket is not received by the passenger.
                        For further clarifications please, visit our website www.sekaptravels.com.
                        
                        </td>

                </tr>

                <tr>

                    <th  style="padding: 15px">VISA REQUIREMENTS
                    </th>

                    <td style="text-align:right; padding: 15px">We do not deal in Visa therefore, please refer to the local counselate for Visa requirements.
                    </td>

                </tr>




                <tr>

                    <th  style="padding: 15px">FARES
                    </th>

                    <td style="text-align:right; padding: 15px">Prices quoted at time of booking or enquiry are not guaranteed until paid in full. Fares are subject to change according to availability.

                    </td>

                </tr>
                <tr>

                    <th  style="padding: 15px">CANCELLATION OR CHANGES

                    </th>

                    <td style="text-align:right; padding: 15px">Cancellation and change in dates may vary according to class or fare rules on which the ticket is issued. Some tickets may be
                        non-refundable and non-changeable. The tickets with flexibility can be changed by paying penalty plus fare difference (which varies according
                        to the availability).
                        Any refund of the ticket shall be processed within 6 to 8 weeks of the date when refund is applied.
                        
                    </td>

                </tr>

                <tr>

                    <th  style="padding: 15px">CHECKED BAGGAGE


                    </th>

                    <td style="text-align:right; padding: 15px">The baggage varies from airlines to the classes booked. Please ask for more information about baggage from our consultant.
                    </td>

                </tr>
                <tr>

                    <th  style="padding: 15px">Terms & Conditions</th>

                    <td style="text-align:right; padding: 15px">

                        <p>We accept cancellations, only before 24 Hrs from departure time</p>

                        <p>For cancellations, within 24 hours before departure you need to contact the airline. Post cancellation by airline, you can contact Paytm for refund, if any</p>

                        <p>Convenience fee is non-refundable. Any cashback availed will be adjusted in final refund amount</p>

                    </td>

                </tr>
            </table>


        </td>
    </tr>
    <tr>
        <td colspan="3">
            <p style="color: #ffab41;font-size: 19px;font-weight: bold;">Fare & Payment Details</p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="color: #4c4b4b;font-weight: 700;">Base Fare</p>
            <p style="color: #4c4b4b;font-weight: 700;">Tax</p>
            @if($baggprice !='0')
            <p style="color: #4c4b4b;font-weight: 700;">Extra Baggage Charges ({{$flightbook_return->bag_weight}} kg) </p>
            @endif
            @if($mealprice !='0')
            <p style="color: #4c4b4b;font-weight: 700;">Meal Charges ({{$flightbook_return->mealname}})</p>
            @endif
            <p style="color: #4c4b4b;font-weight: 700;">Other Charges</p>
            <p style="color: #4c4b4b;font-weight: 700;">Total</p>

        </td>
        <td colspan="2" style="text-align:right;">
            <p style="color:#15298e;font-weight: bold;">{{$imgicon}} {{number_format($basefare,2)}}</p>
            <p style="color:#15298e;font-weight: bold;">{{$imgicon}} {{number_format($taxfare,2)}}</p>
            @if($baggprice !='0')
            <p style="color:#15298e;font-weight: bold;">{{$imgicon}} {{number_format($baggprice,2)}}</p>
            @endif
            @if($mealprice !='0')
            <p style="color:#15298e;font-weight: bold;">{{$imgicon}} {{number_format($mealprice,2)}}</p>
            @endif
            <p style="color:#15298e;font-weight: bold;">{{$imgicon}} {{number_format($othercharges,2)}}</p>
            <p style="color:#15298e;font-weight: bold;">{{$imgicon}} {{number_format($totalamount,2)}}</p>



        </td>
    </tr>

</table>

<?php } ?>
<!-- end flight return -->
<style>
    p.book-id {
        color: #15298e;
        font-size: 17px;
        font-weight: bold;
    }
    p.o-flight {
        color: #434344;
        font-size: 16px;
        font-weight: 700;
    }
    p.t-flight {
        color: #15298e;
        font-weight: bold;
        font-size: 18px;
    }
    body{
        font-family: 'Open Sans', sans-serif;
    }
</style>
</body>
</html>