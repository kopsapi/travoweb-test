@include('pages.include.header')
<script type="text/javascript" src="{{asset('assets/js/jssor.core.js')}}" ></script>
<script type="text/javascript" src=" {{asset('assets/js/jssor.utils.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jssor.slider.js')}}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script>
    jssor_slider1_starter = function (containerId) {

        var _SlideshowTransitions = [
            //Fade in L
            {
                $Duration: 1200,
                $During: {$Left: [0.3, 0.7]},
                $FlyDirection: 1,
                $Easing: {$Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleHorizontal: 0.3,
                $Opacity: 2
            }
            //Fade out R
            , {
                $Duration: 1200,
                $SlideOut: true,
                $FlyDirection: 2,
                $Easing: {$Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleHorizontal: 0.3,
                $Opacity: 2
            }
            //Fade in R
            , {
                $Duration: 1200,
                $During: {$Left: [0.3, 0.7]},
                $FlyDirection: 2,
                $Easing: {$Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleHorizontal: 0.3,
                $Opacity: 2
            }
            //Fade out L
            , {
                $Duration: 1200,
                $SlideOut: true,
                $FlyDirection: 1,
                $Easing: {$Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleHorizontal: 0.3,
                $Opacity: 2
            }

            //Fade in T
            , {
                $Duration: 1200,
                $During: {$Top: [0.3, 0.7]},
                $FlyDirection: 4,
                $Easing: {$Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleVertical: 0.3,
                $Opacity: 2
            }
            //Fade out B
            , {
                $Duration: 1200,
                $SlideOut: true,
                $FlyDirection: 8,
                $Easing: {$Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleVertical: 0.3,
                $Opacity: 2
            }
            //Fade in B
            , {
                $Duration: 1200,
                $During: {$Top: [0.3, 0.7]},
                $FlyDirection: 8,
                $Easing: {$Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleVertical: 0.3,
                $Opacity: 2
            }
            //Fade out T
            , {
                $Duration: 1200,
                $SlideOut: true,
                $FlyDirection: 4,
                $Easing: {$Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleVertical: 0.3,
                $Opacity: 2
            }

            //Fade in LR
            , {
                $Duration: 1200,
                $Cols: 2,
                $During: {$Left: [0.3, 0.7]},
                $FlyDirection: 1,
                $ChessMode: {$Column: 3},
                $Easing: {$Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleHorizontal: 0.3,
                $Opacity: 2
            }
            //Fade out LR
            , {
                $Duration: 1200,
                $Cols: 2,
                $SlideOut: true,
                $FlyDirection: 1,
                $ChessMode: {$Column: 3},
                $Easing: {$Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleHorizontal: 0.3,
                $Opacity: 2
            }
            //Fade in TB
            , {
                $Duration: 1200,
                $Rows: 2,
                $During: {$Top: [0.3, 0.7]},
                $FlyDirection: 4,
                $ChessMode: {$Row: 12},
                $Easing: {$Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleVertical: 0.3,
                $Opacity: 2
            }
            //Fade out TB
            , {
                $Duration: 1200,
                $Rows: 2,
                $SlideOut: true,
                $FlyDirection: 4,
                $ChessMode: {$Row: 12},
                $Easing: {$Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleVertical: 0.3,
                $Opacity: 2
            }

            //Fade in LR Chess
            , {
                $Duration: 1200,
                $Cols: 2,
                $During: {$Top: [0.3, 0.7]},
                $FlyDirection: 4,
                $ChessMode: {$Column: 12},
                $Easing: {$Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleVertical: 0.3,
                $Opacity: 2
            }
            //Fade out LR Chess
            , {
                $Duration: 1200,
                $Cols: 2,
                $SlideOut: true,
                $FlyDirection: 8,
                $ChessMode: {$Column: 12},
                $Easing: {$Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleVertical: 0.3,
                $Opacity: 2
            }
            //Fade in TB Chess
            , {
                $Duration: 1200,
                $Rows: 2,
                $During: {$Left: [0.3, 0.7]},
                $FlyDirection: 1,
                $ChessMode: {$Row: 3},
                $Easing: {$Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleHorizontal: 0.3,
                $Opacity: 2
            }
            //Fade out TB Chess
            , {
                $Duration: 1200,
                $Rows: 2,
                $SlideOut: true,
                $FlyDirection: 2,
                $ChessMode: {$Row: 3},
                $Easing: {$Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $ScaleHorizontal: 0.3,
                $Opacity: 2
            }

            //Fade in Corners
            , {
                $Duration: 1200,
                $Cols: 2,
                $Rows: 2,
                $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]},
                $FlyDirection: 5,
                $ChessMode: {$Column: 3, $Row: 12},
                $Easing: {
                    $Left: $JssorEasing$.$EaseInCubic,
                    $Top: $JssorEasing$.$EaseInCubic,
                    $Opacity: $JssorEasing$.$EaseLinear
                },
                $ScaleHorizontal: 0.3,
                $ScaleVertical: 0.3,
                $Opacity: 2
            }
            //Fade out Corners
            , {
                $Duration: 1200,
                $Cols: 2,
                $Rows: 2,
                $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]},
                $SlideOut: true,
                $FlyDirection: 5,
                $ChessMode: {$Column: 3, $Row: 12},
                $Easing: {
                    $Left: $JssorEasing$.$EaseInCubic,
                    $Top: $JssorEasing$.$EaseInCubic,
                    $Opacity: $JssorEasing$.$EaseLinear
                },
                $ScaleHorizontal: 0.3,
                $ScaleVertical: 0.3,
                $Opacity: 2
            }

            //Fade Clip in H
            , {
                $Duration: 1200,
                $Delay: 20,
                $Clip: 3,
                $Assembly: 260,
                $Easing: {$Clip: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $Opacity: 2
            }
            //Fade Clip out H
            , {
                $Duration: 1200,
                $Delay: 20,
                $Clip: 3,
                $SlideOut: true,
                $Assembly: 260,
                $Easing: {$Clip: $JssorEasing$.$EaseOutCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $Opacity: 2
            }
            //Fade Clip in V
            , {
                $Duration: 1200,
                $Delay: 20,
                $Clip: 12,
                $Assembly: 260,
                $Easing: {$Clip: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $Opacity: 2
            }
            //Fade Clip out V
            , {
                $Duration: 1200,
                $Delay: 20,
                $Clip: 12,
                $SlideOut: true,
                $Assembly: 260,
                $Easing: {$Clip: $JssorEasing$.$EaseOutCubic, $Opacity: $JssorEasing$.$EaseLinear},
                $Opacity: 2
            }
        ];

        var options = {
            $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
            $AutoPlayInterval: 1500,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
            $PauseOnHover: 3,                                //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 3

            $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
            $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
            $SlideDuration: 800,                                //Specifies default duration (swipe) for slide in milliseconds

            $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                $Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
                $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                $ShowLink: true                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
            },

            $DirectionNavigatorOptions: {                       //[Optional] Options to specify and enable direction navigator or not
                $Class: $JssorDirectionNavigator$,              //[Requried] Class to create direction navigator instance
                $ChanceToShow: 1                               //[Required] 0 Never, 1 Mouse Over, 2 Always
            },

            $ThumbnailNavigatorOptions: {                       //[Optional] Options to specify and enable thumbnail navigator or not
                $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
                $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

                $ActionMode: 1,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                $SpacingX: 6,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                $DisplayPieces: 10,                             //[Optional] Number of pieces to display, default value is 1
                $ParkingPosition: 360                          //[Optional] The offset position to park thumbnail
            }
        };

        var jssor_slider1 = new $JssorSlider$(containerId, options);
        //responsive code begin
        //you can remove responsive code if you don't want the slider scales while window resizes
        function ScaleSlider() {
            var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
            if (parentWidth)
                jssor_slider1.$SetScaleWidth(Math.max(Math.min(parentWidth, 800), 300));
            else
                $JssorUtils$.$Delay(ScaleSlider, 30);
        }

        ScaleSlider();
        $JssorUtils$.$AddEvent(window, "load", ScaleSlider);

        if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
            $JssorUtils$.$OnWindowResize(window, ScaleSlider);
        }
        //responsive code end
    };
</script>
<style>
    @import url("https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/blitzer/jquery-ui.min.css");

    .ui-datepicker td span,
    .ui-datepicker td a {
        padding-bottom: 1em;
    }
    iframe {
    width: 100%;
}


    p.thank-p {
        font-size: 21px;
        color: #F44336;
        margin-bottom: 0;
    }

    p.info-p {
        color: black;
        margin-bottom: 0;
    }

    p.p-head1 {
        margin-bottom: 0;
        font-size: 18px;
        color: #ababab;
    }

    .ui-datepicker td[title]::after {
        content: attr(title);
        display: block;
        position: relative;
        font-size: .8em;
        height: 1.25em;
        margin-top: -1.25em;
        text-align: right;
        padding-right: .25em;
    }

    #ui-datepicker-div {
        z-index: 12 !important;
    }

    .f-logo {
        width: 90px;
    }

    span.close.close-btn {
        color: white;
        font-size: 26px;
        font-weight: 600;
        cursor: pointer;
    }

    .modal-header.h-bg {
        background: #31124b;
        padding: 10px 21px;
        border-bottom-color: white;
    }

    .modal-footer.f-bg {
        background: #e2e3ea;
    }

    .btn1 {
        font-size: 17px;
        font-weight: 500;
        color: #fff;
        text-transform: uppercase;
        background: #fa9e1b;
        border: none;
        outline: none;
        padding: 8px 17px;
        border-radius: 5px;
        cursor: pointer;
    }

    .tab-pane {
        color: black;
    }

    .search_extras_item div {
        display: inline-block;
        cursor: pointer;
    }

    .flight-details-tabs .nav > li > a {
        position: relative;
        display: block;
        padding: 10px 15px;
        white-space: pre !important;
        border: 1px solid #FF9800 !important;
        border-top-left-radius: 6px !important;
        border-top-right-radius: 25px !important;
    }

    .flight-details-tabs .nav-tabs > li {

        margin-bottom: 0px !important;

    }

    .ul-res .nav::-webkit-scrollbar {
        width: 10px;
        height: 8px;

    }

    .ul-res .nav::-webkit-scrollbar-thumb {
        background: #8c8989;
    }

    .ul-res .nav::-webkit-scrollbar-track {
        background: #d8d8d8;
    }

    .ul-res .nav {
        flex-wrap: nowrap !important;
        overflow-x: auto;
        overflow-y: hidden;
        -webkit-overflow-scrolling: touch;
    }

    .flight-details-tabs .nav-tabs > li > a.active, .nav-tabs > li > a.active:focus, .nav-tabs > li > a.active:hover {
        color: #FFF;
        cursor: default;
        background-color: #fa9e1b;
        border: 1px solid #FF9800 !important;
        border-radius: 0 !important;
        padding: 10px 15px !important;
        border-top-left-radius: 6px !important;
        border-top-right-radius: 25px !important;
    }

    .search_extras label {
        display: block;
        position: relative;
        font-size: 15px;
        font-weight: 400;
        padding-left: 25px;
        margin-bottom: 0px;
        cursor: pointer;
        color: #FFFFFF;
    }

    div#bookAndshare {
        background: antiquewhite;
    }

    a.active {
        border-radius: 20px !important;
        padding: 9px 14px !important;
    }

    .dropdown a:hover {
        background-color: #ddd;
    }

    button.view-h-btn {
        display: block;
        width: auto;
        margin: 50px auto 0;
        padding: 15px 31px;
        background: #00206a;
        border: none;
        color: white;
    }

    .slider-div {
        margin-left: 30px;
        margin-right: 30px;
    }

    span.hs-price {
        position: absolute;
        top: 15px;
        left: 33px;
        color: white;
        font-size: 16px;
        font-weight: 700;
    }

    h3.h-head {
        color: #00206a;
        font-weight: 600;
        text-align: center;
        font-size: 35px;
        margin: -20px 0 66px;
    }

    .hotel-div {
        height: 210px;
        min-height: 210px;
        border-radius: 5px;
        border: 1px solid #FF9800;
    }

    button.b-btn {
        border: none;
        background: #FF9800;
        color: white;
        padding: 7px 10px;
        border-radius: 5px;
        display: block;
        margin-left: auto;
    }

    .r-star {
        position: relative;
        top: 30px;
        left: 11px;
    }

    span.fa.fa-star.checked {
        color: #FFC107;
    }

    span.fa.fa-star {
        color: #ffffff;
    }

    button.b-btn {
        border: none;
        background: #FF9800;
        color: white;
        padding: 7px 10px;
        border-radius: 5px;
        display: block;
        margin-left: auto;
    }

    button.b-btn:hover {

        background: #00206A;

    }

    .book-btn-div {
        position: relative;
        top: 115px;
        right: 10px;
        border: none;
    }

    img.h-img {
        width: 100%;
        position: relative;
        height: 100%;
        border-radius: 5px;
    }

    .over-content {
        position: absolute;
        top: 0px;
        width: 100%;
        left: 0px;
        background: #00000085;
        height: 100%;
        border-radius: 5px;
    }

    span.hotel-name-tag {
        color: white;
        background: #E91E63;
        padding: 6px 10px;
        /* top: 10px; */
        position: relative;
        top: 17px;
        left: 10px;
        border-radius: 5px;
    }

    h3.pkg-title {
        color: white;
        background: #00206a;
        text-align: center;
        padding: 6px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
    }

    .mySlides img {
        height: 100%;
        min-height: 100%;
    }

    dfn {
        color: #00206a;
        font-size: 18px;
        font-weight: 600;
    }

    p.innerprice {
        color: #F44336;
        font-size: 19px;
        font-weight: 600;
        margin-bottom: -6px;
    }

    .extra-pkg i {
        color: #607D8B;
        padding: 0 7px 0 0;
    }

    .extra-pkg {
        border-top: 1px solid lightgray;
        border-bottom: 1px solid lightgray;
        padding: 5px 0;
    }

    .filter h4 {
        background: #00206A;
        color: #FFF;
        padding: 8px 15px;
    }

    .pkg-detail {
        padding: 10px 20px;
    }

    .social-pkg a {
        font-size: 15px;
        color: black;
    }

    .social-pkg {
        margin: 10px 0;
    }


    .form-container {
        background-color: #f2f2f2;
        padding: 5px 20px 15px 20px;
        border: 1px solid lightgrey;
        border-radius: 3px;
    }

    .pkg-input {
        width: 100%;
        margin-bottom: 10px;
        padding: 8px 12px;
        border: 1px solid #ccc;
        border-radius: 3px;
    }

    .pkg-label {
        margin-bottom: 10px;
        display: block;
        color: black;
    }

    .pkg-btn {
        background-color: #FF9800;
        color: white;
        padding: 9px 15px;
        margin: 19px 0;
        border: none;
        width: auto;
        border-radius: 3px;
        cursor: pointer;
        font-size: 17px;
    }

    .pkg-btn:hover {
        background-color: #00206a;
    }


    hr {
        border: 1px solid lightgrey;
    }

    .pkg-radio {
        display: inline;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        color: black;
        font-size: 17px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }


    .pkg-radio input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    .checkmark {
        position: absolute;
        top: 3px;
        left: 8px;
        height: 18px;
        width: 18px;
        background-color: #eee;
        border-radius: 50%;
    }

    .pkg-radio:hover input ~ .checkmark {
        background-color: #ccc;
    }

    .pkg-radio input:checked ~ .checkmark {
        background-color: #2196F3;
    }

    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    .pkg-radio input:checked ~ .checkmark:after {
        display: block;
    }

    .pkg-radio .checkmark:after {
        top: 4px;
        left: 4px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
    }

    .ul-res .nav {

        flex-wrap: nowrap !important;
        overflow-x: auto !important;

    }

    .ul-active {
        display: none;
    }

    .tab-modal .modal-header {
        background: #20295b;
        color: white;
    }

    .tab-modal .modal-header button.close {
        color: white;
    }

    .show {
        padding: 10px !important;
    }

    .over-div {
        display: none;
    }

    .s-icon {
        padding: 13px 10px;
        font-size: 19px;
        width: 45px;
        height: 45px;
        text-align: center;
        text-decoration: none;
        margin: 5px 2px;
        border-radius: 50%;
    }

    .s-icon:hover {
        opacity: 0.7;
    }

    .fa-facebook {
        background: #3B5998;
        color: white;
    }

    .fa-twitter.s-icon {
        background: #55ACEE;
        color: white;
    }

    .fa-google-plus {
        background: #dd4b39;
        color: white;
    }

    .fa-linkedin {
        background: #007bb5;
        color: white;
    }

    .hover-div:hover .over-div {
        display: block !important;
    }

    .over-div {
        /* display: none; */
        position: absolute;
        z-index: 999;
    }

    div#bookAndshare ul li a {
        padding: 5px 20px;
        display: inline-block;
    }

    div#bookAndshare {
        background: white;
        width: 350px !important;
        padding-bottom: 20px;
        border: 1px solid #cccccc;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        border-bottom: 2px solid #00206a !important;
    }

    .filter h4 {

        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
    }

    div#bookAndshare ul li {
        float: left;
        display: block;
        width: 50%;
    }

    img.pdf-icon {
        height: 36px;
    }

    iframe.pkg-map {
        width: 100%;
        margin: 0;
        padding: 0;
    }

    .sight-p p {
        color: black;
    }

    img.sight-img {
        height: 180px;

        float: left;
        padding: 3px;
        border: 1px solid #00206a;
        margin-right: 15px;
    }

    .inclusion-ul li i, .exclusion-ul li i, .highlight-ul li i, .terms-ul li i {
        color: #00206A;
    }

    ul.terms-ul li {
        padding-bottom: 10px;
    }

    img.sight-img {
        height: 135px;
        padding: 3px;
        border: 1px solid #00206a;
    }



    button.expnd {
        border: none;
        background: none;
        padding: 5px;
        cursor: pointer;
    }

    a.expnd:hover {
        color: black;
        text-decoration: none;
        border: none;
        background: none;
    }

    .day_txtbox h3 {
        color: #FF9800;
        font-size: 20px;
        font-weight: 600;
        margin: 0;
    }


    .day-wise-div {
        margin: 20px 30px 30px 0;
        border: 1px solid;
        padding: 15px;
        border-radius: 5px;
    }

    .day_txtbox p {
        color: black;
        margin: 0;

    }

    .day_txtbox {
        padding: 0 10px 0px 10px;
    }

    .day_box p {
        margin: 0;
        color: white;
    }

    .day_box {
        color: white;
        background: black;
        display: inline-block;
        padding: 5px 20px;
        text-align: center;
        border-radius: 5px;
    }

    li.pkg-option a.nav-link {
        border: 1px solid #00206a !important;
        border-radius: 0 !important;
        border-radius: 5px !important;
        padding: 10px 15px !important;
        border-bottom-left-radius: 5px !important;
        border-bottom-right-radius: 5px !important;
        margin-right: 10px;
    }

    li.pkg-option a.nav-link.active {
        border: 1px solid #00206a !important;
        background: #00206a;
        color: white;
        padding: 10px 11px !important;
        border-top-left-radius: 5px !important;
        border-top-right-radius: 5px !important;
        border-bottom-left-radius: 5px !important;
        border-bottom-right-radius: 5px !important;
        margin-right: 10px;
        margin-top: 10px;
        margin-bottom: 5px;
    }

    .filter {
        border-top-left-radius: 5px !important;
        border-top-right-radius: 5px !important;
    }

    ul.nav.nav-tabs li.pkg-option {
        padding: 0 !important;
        margin: 0;
    }

    div#Description {
        padding: 0;
    }

    h3.pkg-des {
        color: #00206a;
    }

    p.pkg-p {
        color: black;
    }

    h3.pkg-des {
        color: #00206a;
        padding-bottom: 5px;
        border-bottom: 1px solid #c1c1c1;
    }

    .tab-content {
        border: 1px solid #ddd;
        margin: 0 !important;
        padding: 13px;
        border-top: none;
    }

    .flight-details-tabs {

        border-top: none;

    }

    .flight-details-tabs .tab-content .tab-pane li:before {
        content: none !important;
    }

    p.itn_para,p.sight-p{
        color: black;
    }

    .card-header {
        padding: 0 !important;
        margin-bottom: 0;
        color: white;
        background-color: #00206a;
        border-bottom: none !important;
        border-radius: 7px !important;
    }

    .card-header > a:after {
        content: "\f067"; /* fa-chevron-down */
        font-family: 'FontAwesome';
        position: absolute;
        right: 10px;
        color:white;
    }

    .card:first-child a:after {
        content: "\f068" !important; /* fa-chevron-down */
        font-family: 'FontAwesome';
        position: absolute;
        right: 10px;
        color:white;
    }

    .card:first-child a[aria-expanded="false"]:after {
        content: "\f067" !important; /* fa-chevron-down */
        font-family: 'FontAwesome';
        position: absolute;
        right: 10px;
        color:white;
    }
    a.card-link {
        padding: 13px;
    }

    .card-header > a[aria-expanded="true"]:after {
        content: "\f068"; /* fa-chevron-up */
    }

    .card {
        border: 1px solid rgb(0, 32, 106);
        margin: 10px 0;
        border-radius: 10px;
    }

    a.card-link {
        color: #fff;
        display: block;
    }

    .extra-card {
        background-color: #00206a !important;
        border-radius: 0 !important;
        border-top-left-radius: 10px !important;
        border-top-right-radius: 10px !important;
    }
    .extra-card a{
        color: #fff !important;
    }

    .show1{
        padding: 10px !important;
    }

    .table td {
        padding: .75rem;
        vertical-align: top;
        border: 1px solid #e9ecef !important;
    }

    ul li>span:before {
        content: "\f0da";
        font-family: FontAwesome;
        display: inline-block;
        padding-right: 3px;
        vertical-align: middle;
        font-weight: 900;
        color: #002073;
        padding-bottom: 2px;
    }



    ul li>span:before {
        margin-right: 7px;
    }

    @media screen and (max-width: 450px) {
        .flight-details-tabs .nav-tabs > li {
            width: 50% !important;

        }

        .flight-details-tabs .nav > li > a {

            padding: 25px 10px !important;
            text-align: center !important;
            border: 1px solid #fa9e1b !important;
            border-radius: 6px !important;
            font-size: 12px;
            margin-bottom: 20px;
            min-height: 85px;
            white-space: normal !important;

        }

        .flight-details-tabs .nav-tabs > li > a.active {
            padding: 25px 10px !important;
            text-align: center !important;
            border: 1px solid #fa9e1b !important;
            border-radius: 6px !important;
            font-size: 12px;
            margin-bottom: 20px;
            min-height: 85px;

        }

    }

    @media screen and (max-width: 500px) {

        .bar-heading h5 {
            font-size: 12px !important;
        }

        .flight-name p {
            font-size: 10px !important;
        }


        .flight-list p {

            font-size: 11px !important;
        }


    }

    @media screen and (max-width: 550px) {
        .s-icon {
            padding: 8px 4px;
            font-size: 16px;
            width: 31px;
            height: 31px;
            text-align: center;
            text-decoration: none;
            margin: 5px 2px;
            border-radius: 50%;
        }

        div#bookAndshare ul li a {
            padding: 5px 8px;
            display: inline-block;
        }


        div#bookAndshare {
            background: white;
            width: 235px !important;
            padding-bottom: 20px;
            border: 1px solid #cccccc;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            border-bottom: 2px solid #00206a !important;
        }

        .home {
            height: 11vh !important;
        }

        h3.pkg-title span {
            font-size: 13px;
        }

        .card-body,.container.tab-pane{
            padding: 0 !important;
        }
        .hotel-book-details {
            padding: 15px 15px;
        }

        .col-lg-5.form-div {
            padding-left: 0;
        }

        ul.nav.nav-tabs {
            padding: 0px 20px 20px;
        }

        .flight-details-tabs .nav-tabs > li {
            width: 33.33% !important;

        }

        .flight-details-tabs .nav > li > a {
            padding: 10px 10px !important;
            text-align: center !important;
            border: 1px solid #00206a !important;
            border-radius: 6px !important;
            font-size: 12px;
            margin-bottom: 20px;
            min-height: 73px;
        }

        .flight-details-tabs .nav-tabs > li > a.active {
            padding: 10px 10px !important;
            text-align: center !important;
            border: 1px solid #00206a !important;
            border-radius: 6px !important;
            font-size: 12px;
            margin-bottom: 20px;
            min-height: 73px;
        }

        h3.h-head {
            font-size: 20px !important;
            margin: -20px 0 20px !important;
        }

        .ul-res {
            display: none;
        }

        .ul-active {
            display: block !important;
        }

        .tab-pane ul {
            padding: 0 !important;
        }

        .day-wise-div {
            margin: 0 !important;
            margin-bottom: 20px;

            padding: 15px;

        }

        .day_txtbox {
            padding: 14px 10px 0px 0px !important;
        }

        .modal .tab-content {
            padding: 0;
        }

        li.pkg-option a.nav-link, li.pkg-option a.nav-link.active {

            margin: 0px !important;
        }

        ul.nav.nav-tabs li.pkg-option {
            padding: 0 !important;
            margin: 10px auto !important;
            width: 40% !important;
        }

        .day_box {

            display: block !important;

            width: 90px !important;

            margin: auto !important;

        }

        .day-wise-div {
            text-align: center !important;
            margin-bottom: 20px !important;
        }

        div#Inclusions {
            padding-bottom: 30px !important;
        }

        div#Inclusions {
            padding-bottom: 30px !important;
        }



        h3.pkg-des {
            color: #00206a;
            padding-bottom: 5px;
            font-size: 20px;
            border-bottom: 1px solid #c1c1c1;
        }

        .day_txtbox h3 {
            color: #FF9800;
            font-size: 15px;
            font-weight: 600;
            margin: 0;
        }

        iframe.pkg-map {
            width: 100%;
            margin: 0;
            height: 181px;
            padding: 0;
        }

        .flex-column.tab-col {
            -ms-flex-direction: row !important;
            flex-direction: row !important;
        }

        ul.nav.nav-tabs.flex-column.tab-col li {
            width: 31.33% !important;

            margin: auto;
            text-align: center;
            padding: 0 !important;

        }

        ul.nav.nav-tabs.flex-column.tab-col li a {
            min-height: 20px;
            padding: 4px 2px !important;
        }

        ul.nav.nav-tabs.flex-column.tab-col li a.active {
            min-height: 20px;
            padding: 4px 2px !important;
        }

        img.sight-img {
            height: 150px;
            width: 100%;
            padding: 3px;
            border: 1px solid #00206a;
        }

        h3.pkg-title {

            font-size: 15px;

        }

        h3.pkg-title span {
            padding: 5px;
        }

        .slides-res {
            flex: 0 0 90% !important;
            max-width: 90% !important;
            margin: 0 auto 45px auto !important;
        }

        button.b-btn ,.pkg-btn,.alert.alert-success,a.card-link,div#accordion{
            font-size: 13px !important;

        }

        span.hs-price {
            position: absolute;
            top: 10px;
            left: 33px;
            color: white;
            font-size: 14px;
            font-weight: 700;
        }

        .filter h4 {
            background: #00206A;
            color: #FFF;
            padding: 8px 15px;
            font-size: 13px;
        }

    }

    @media screen and (max-width: 650px) {

        .col-sm-6.col-md-3.slides-res {
            flex: 0 0 100%;
            max-width: 100%;
            margin: 0 auto 45px auto;
        }

        .showtable_returninter div .flight-list .row .col-md-3 {
            padding: 0;
        }

        .showtable_returninter div .flight-list .flight-fare .row .col-md-3 {
            padding: 10px;
        }

        .slides-res {
            flex: 0 0 90% !important;
            max-width: 90% !important;
            margin: 0 auto 45px auto !important;
        }

        p.innerprice {
            color: #F44336;
            font-size: 15px;
            font-weight: 600;
            margin-bottom: -6px;
        }
        dfn {
            color: #00206a;
            font-size: 14px;
            font-weight: 600;
        }

        .social-pkg a, .alert.alert-info {
            font-size: 12px;
        }

        .pkg-label,.pkg-radio {
            font-size: 13px;
        }

        .checkmark {
            top: 1px !important;

        }

        .pkg-input::placeholder{
            font-size: 13px !important;
        }


    }

    @media screen and (max-width: 760px) {
        .col-sm-6.col-md-3.slides-res {
            flex: 0 0 50%;
            max-width: 50%;
            margin: 0 auto 45px auto;
        }

        button.view-h-btn {

            margin: 0 auto 0;

        }

    }

    @media screen and (max-width: 992px) {
        .flex-column.tab-col {
            -ms-flex-direction: row !important;
            flex-direction: row !important;
            margin-bottom: 20px !important;
        }

        .col-lg-5.form-div {
            padding-left: 0 !important;
        }
    }

    @media screen and (max-width: 1280px) {
        .col-sm-6.col-md-3.slides-res {
            flex: 0 0 45%;
            max-width: 40%;
            margin: 0 auto 45px auto;
        }
    }



</style>
<style>
    /* jssor slider direction navigator skin 05 css */
    /*
    .jssord05l              (normal)
    .jssord05r              (normal)
    .jssord05l:hover        (normal mouseover)
    .jssord05r:hover        (normal mouseover)
    .jssord05ldn            (mousedown)
    .jssord05rdn            (mousedown)
    */
    .jssord05l, .jssord05r, .jssord05ldn, .jssord05rdn {
        position: absolute;
        cursor: pointer;
        display: block;
        background: url(img/d17.png) no-repeat;
        overflow: hidden;
    }

    .jssord05l {
        background-position: -10px -40px;
    }

    .jssord05r {
        background-position: -70px -40px;
    }

    .jssord05l:hover {
        background-position: -130px -40px;
    }

    .jssord05r:hover {
        background-position: -190px -40px;
    }

    .jssord05ldn {
        background-position: -250px -40px;
    }

    .jssord05rdn {
        background-position: -310px -40px;
    }


    /* jssor slider thumbnail navigator skin 01 css */
    /*
    .jssort01 .p           (normal)
    .jssort01 .p:hover     (normal mouseover)
    .jssort01 .pav           (active)
    .jssort01 .pav:hover     (active mouseover)
    .jssort01 .pdn           (mousedown)
    */
    .jssort01 .w {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
    }

    .jssort01 .c {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 74px;
        height: 74px;
        border: #000 2px solid;
    }

    .jssort01 .p:hover .c, .jssort01 .pav:hover .c, .jssort01 .pav .c {
        background: url(assets/images/t1.png) center center;
        border-width: 0px;
        top: 2px;
        left: 2px;
        width: 70px;
        height: 70px;
    }

    .jssort01 .p:hover .c, .jssort01 .pav:hover .c {
        top: 0px;
        left: 0px;
        width: 72px;
        height: 72px;
        border: #fff 1px solid;
    }
</style>

<body>

<div class="super_container">

    <div class="modal fade modify-flight" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Modify Search</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    @include('pages.include.topheader')

    <div class="home" style="height:20vh;">
        <div class="home_content">
            <div class="home_title">Results</div>
        </div>
    </div>


    <section class="hotel-book-details" style="border-top: 1px solid #d6d6d6; width: 100%">

        <div class="container">
            <h3 style="" class="pkg-title row" id="showmtitle">
                <span class="col-6" style="text-align:left;">{{$pckgdetails->packagename}} {{$pckgdetails->packagedays}} Days / {{$pckgdetails->packagenight}} Nights</span>
                <span class="col-6" style="text-align: right"> <i class="fa {{$pckgdetails->pricecurr}}" style="color: #f99d1b;"></i> {{$pckgdetails->newpricenew}}</span>
            </h3>

            <div class="row">
                <div class="col-lg-7 form-div" style="padding-left: 0;padding-right: 0">

                    <div class="container" style="">

                        <div class="row">

                            <div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 800px;
        height: 456px; background: #191919; overflow: hidden;">

                                <!-- Loading Screen -->
                                <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                                    <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
                                    </div>
                                    <div style="position: absolute; display: block; background: url(asset(assets/images/loading1.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
                                    </div>
                                </div>

                                <!-- Slides Container -->
                                <div u="slides"
                                     style="cursor: move; position: absolute; left: 0px; top: 0px; width: 800px; height: 356px; overflow: hidden;">
                                    <?php $pckgimageval = unserialize($pckgdetails->packimage);
                                    for($j = 0;$j < count($pckgimageval);$j++)
                                    {
                                    ?>
                                    <div>
                                        <img u="image"
                                             src="{{asset('assets/uploads/packageimage')}}/{{$pckgimageval[$j]}}"/>
                                        <img u="thumb"
                                             src="{{asset('assets/uploads/packageimage')}}/{{$pckgimageval[$j]}}"/>
                                    </div>
                                    <?php } ?>
                                </div>


                                <span u="arrowleft" class="jssord05l"
                                      style="width: 40px; height: 40px; top: 158px; left: 8px;">
                                 </span>

                                <span u="arrowright" class="jssord05r"
                                      style="width: 40px; height: 40px; top: 158px; right: 8px">
                                </span>

                                <div u="thumbnavigator" class="jssort01" style="position: absolute; width: 800px; height: 100px; left:0px; bottom: 0px;">


                                    <div u="slides" style="cursor: move;">
                                        <div u="prototype" class="p"
                                             style="position: absolute; width: 74px; height: 74px; top: 0; left: 0;">
                                            <div class=w>
                                                <thumbnailtemplate
                                                        style=" width: 100%; height: 100%; border: none;position:absolute; top: 0; left: 0;"></thumbnailtemplate>
                                            </div>
                                            <div class=c>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <a style="display: none" href="http://www.jssor.com">jQuery Gallery</a>

                                <script>
                                    jssor_slider1_starter('slider1_container');
                                </script>
                            </div>

                            <img width=0 height=0/>
                        </div>

                        <div class="row">

                            <div class="flight-filter mt-4 " style="width: 100%">

                                <div class="filter" style="border-bottom: 2px solid #00206a">
                                    <h4 style="margin-bottom: 0"><i class="fa fa-briefcase"></i> {{$pckgdetails->packagename}}</h4>
                                    <div class="pkg-detail" style="position:relative" id="showmshare">
                                        <p class="innerprice">Starting From </p>
                                        <p class="pkg-sm-res" style="margin-bottom: 0"><dfn>
                                                <i class="fa {{$pckgdetails->pricecurr}}" style="color: #f99d1b;"></i> {{$pckgdetails->newpricenew}}</dfn>
                                            <span style="color: #2d3438;margin-top: -5px;display: block">Per Person</span>
                                        </p>

                                        <div class="extra-pkg">
                                            <i class="fa fa-glass iconselect"></i>
                                            <i class="fa fa-bus iconselect"></i>
                                            <i class="fa fa-cutlery iconselect"></i>
                                            <i class="fa fa-eye iconselect"></i>
                                        </div>

                                        <div class="social-pkg">
                                            <div class="hover-div" style="display: inline;">
                                                <a href="#" style="position: relative;" class="a-social">
                                                    <span class="fa fa-share iconfont18"></span> Share With Friends
                                                </a>
                                                <?php

                                                $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                                                        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
                                                    $_SERVER['REQUEST_URI'];

                                                ?>
                                                <div class="swfriend_popup resp_dnone over-div" id="bookAndshare">
                                                    <h4><span>Bookmark &amp; Share</span></h4>
                                                    <ul>
                                                        <li>
                                                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?=urlencode($url)?>" target="_blank">
                                                                <span class="fa fa-facebook s-icon"></span> Facebook
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=<?=urlencode($url)?>" target="_blank">
                                                                <span class="fa fa-twitter s-icon"></span> Twitter
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="https://plus.google.com/share?url={{ urlencode($url) }}" target="_blank">
                                                                <span class="fa fa-google-plus s-icon"></span> Google +
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?=urlencode($url)?>" target="_blank">
                                                                <span class="fa fa-linkedin s-icon"></span> Linkdin
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="social-pkg">
                                       
                                            <a href="mailto"><span class="fa fa-envelope iconfont18"></span>

                                                Send This Tour To Friends
                                            </a>
                                        </div>
                                        <div class="social-pkg">
                                            <a href="#" id="print_btn"><span class="fa fa-print iconfont18"></span>
                                                Print This Package
                                            </a>
                                        </div>
                                        <div class="alert alert-info">
                                            <b>Please fill your details</b> to help us call you back for more details
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-5 form-div" style="padding-right: 0px" id="showmform">

                    <div class="row">

                        <div class="col-lg-12">

                            <div class="form-container">

                                <form action="#">

                                    <div class="row pt-4">

                                        <div class="col-md-6">
                                            <input type="hidden" id="pckg_id" value="{{$pckgdetails->pckg_id}}">
                                            <input type="hidden" id="pckg_name" value="{{$pckgdetails->packagename}}">
                                            <input type="hidden" id="pckg_mainid" value="{{$pckgdetails->id}}">
                                            <input type="hidden" id="pckg_price" value="{{$pckgdetails->newpricenew}}">
                                            <label class="pkg-label" for="fname"><i class="fa fa-user"></i> First Name</label>
                                            <input class="pkg-input" id="firstname" type="text" placeholder="First Name">
                                            <span id="firstname_error" style="color:red;display: none"> </span>
                                        </div>

                                        <div class="col-md-6">
                                            <label class="pkg-label" for="cname"><i class="fa fa-user"></i> Last Name</label>
                                            <input class="pkg-input" type="text" id="lastname" name="cardname" placeholder="Last Name">
                                            <span id="lastname_error" style="color:red;display: none"> </span>
                                        </div>

                                        <div class="col-md-6">
                                            <label class="pkg-label" for="email"><i class="fa fa-envelope"></i> Email</label>
                                            <input class="pkg-input" type="text" id="email" name="email" placeholder="Your E-mail">
                                            <span id="email_error" style="color:red;display: none"> </span>
                                        </div>

                                        <div class="col-md-6">
                                            <label class="pkg-label" for="ccnum"><i class="fa fa-phone"></i> Mobile</label>
                                            <input class="pkg-input leadage" id="mobile" type="text" placeholder="Enter Your Mobile No">
                                            <span id="mobile_error" style="color:red;display: none"> </span>
                                        </div>

                                        <div class="col-md-6">
                                            <label class="pkg-label" for="adr"><i class="fa fa-user-o"></i> Adult</label>
                                            <select class="pkg-input" id="adult">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        </div>

                                        <div class="col-md-6">
                                            <label class="pkg-label" for="expmonth"><i class="fa fa-child"></i> Child</label>
                                            <select class="pkg-input" id="child">
                                                <option value="0">Select Child</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="pkg-label" for="countryname"><i class="fa fa-user"></i> Country Name</label>
                                            <input class="pkg-input" type="text" id="countryname" name="countryname" placeholder="Country Name">
                                            <span id="countryname_error" style="color:red;display: none"> </span>
                                        </div>
                                         <div class="col-md-6">
                                            <label class="pkg-label" for="ccnum"><i class="fa fa-phone"></i> Alternative Mobile</label>
                                            <input class="pkg-input leadage" id="alter_mobile" type="text" placeholder="Alternative Mobile No">
                                            <span id="mobile_error" style="color:red;display: none"> </span>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="pkg-label" for="city"><i class="fa fa-calendar"></i> Travel Date</label>
                                            <input class="pkg-input pckgdate" type="text" id="pckgdate" name="city" placeholder="Travel Date" autocomplete="off">
                                        </div>

                                        <div class="col-md-12">
                                            <label class="pkg-label" for="city"><i class=" fa fa-wechat"></i> Enquiry</label>
                                            <textarea class="pkg-input" id="travel_msg" placeholder="Enquiry Text"></textarea>
                                            <span id="travel_msg_error" style="color:red;display: none"> </span>
                                        </div>

                                        <div class="col-12">

                                            <label class="pkg-label"><b>Preferred time to contact</b></label>

                                            <label class="pkg-radio">Now
                                                <input type="radio" checked="checked" value="1" name="pertime" class="pertime">
                                                <span class="checkmark"></span>
                                            </label>

                                            <label class="pkg-radio">Specific Time
                                                <input type="radio" name="pertime" value="2" class="pertime">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>

                                        <div class="chkenq col-md-12" style="display: none">

                                            <div class="col-md-12">
                                                <label class="pkg-label" for="city"><i class="fa fa-institution"></i>Specific Date</label>
                                                <input class="pkg-input spec_date" type="text" id="pckgenquirydate" name="city" placeholder="Specific  Date" autocomplete="off" value="<?php echo $currentDateTime = date('d/m/Y'); ?>">
                                            </div>

                                            <div class="col-md-12">

                                                <label class="pkg-label" for="adr"><i class="fa fa-address-card-o"></i>Specific Time</label>

                                                <select class="pkg-input" id="spec_time">
                                                    <option value="09:00 - 11:00  AM"> 09:00 - 11:00 AM</option>
                                                    <option value="11:00 - 01:00  PM"> 11:00 - 01:00 PM</option>
                                                    <option value="01:00 - 03:00  PM"> 01:00 - 03:00 PM</option>

                                                    <option value=" 03:00 - 05:00  PM"> 03:00 - 05:00 PM</option>
                                                    <option value=" 05:00 - 07:00  PM"> 05:00 - 07:00 PM</option>
                                                    <option value=" 07:00 - 09:00  PM"> 07:00 - 09:00 PM</option>
                                                    <option value="  09:00 - 11:00  PM"> 09:00 - 11:00 PM</option>
                                                </select>
                                            </div>

                                        </div>

                                        <div class="col-12">
                                            <button class="pkg-btn enq_send" type="button">Send Query</button>
                                        </div>

                                        <div class="col-12">
                                            <div class="alert alert-success">For Details : +61 2 5924 0804,</div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="hotel-book-details" style="border-top: 1px solid #d6d6d6; width: 100%">
        <div class="container" style="padding: 0">
            <div class="flight-details-tabs" style="display: block;">
                <div class="ul-res">
                    <div>
                        <ul class="nav nav-tabs">
                            <li>
                                <a class="nav-link active" data-toggle="tab" href="#Description">Description</a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#Itinerary">Itinerary</a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#Inclusions">Inclusions</a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#Hotel">Hotel Details & Price</a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#Highlights">Exclusion</a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#cancelpolicy">Cancellation Policy</a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#Map">Map</a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#Terms">Terms & Conditions</a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#Downloads">Downloads</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div id="Description" class="tab-pane active">
                            <br>
                            <h3 class="pkg-des">{{$pckgdetails->packgtitle}}</h3>
                            <p class="pkg-p"><?php echo $pckgdetails->maindescription;?></p>


                        </div>
                        <div id="Itinerary" class="container tab-pane fade">
                            <ul class="nav nav-tabs" style="border: none !important;">
                                <li class="pkg-option">
                                    <a class="nav-link  active" data-toggle="pill" href="#home">Daywise Itinerary</a>
                                </li>
                                <li class="pkg-option">
                                    <a class="nav-link" data-toggle="pill" href="#menu1">View Full Itinerary</a>
                                </li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content" style="border: none; padding-left: 0">
                                <div class="tab-pane  active" id="home" style="padding-left: 0">
                                    <?php
                                    $inttitle = unserialize($pckgdetails->inttitle);
                                    $intdescription = unserialize($pckgdetails->intdescription);
                                    $meallist = unserialize($pckgdetails->meallist);

                                    for($t = 0;$t < count($inttitle);$t++)
                                    {
                                    ?>
                                    <div class="day-wise-div">
                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-lg-1">
                                                <div class="day_box">
                                                    <b>{{$t+1}}</b>
                                                    <p>Day</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 col-md-10 col-lg-11">
                                                <div class="day_txtbox">
                                                    <h3>{{$inttitle[$t]}}</h3>

                                                    <p id="minimize-tab-{{$t+1}}" style="display: block">
                                                        <?php
                                                        // $
                                                        // echo $ch =  substr($intdescription[$t], 0,100 );
                                                        echo substr($intdescription[$t], 0, 100);
                                                        ?>

                                                        <button type="button" class="expnd newexpend" value="sh"
                                                                id="{{$t+1}}">
                                                            <i class="fa fa-chevron-circle-down"></i></button>
                                                    </p>
                                                    <div id="maximize-tab-{{$t+1}}" style="display:none;">
                                                        <p> {{$intdescription[$t]}}

                                                            <button type="button" class="expnd newexpend" value="hd"
                                                                    id="{{$t+1}}">
                                                                <i class="fa fa-chevron-circle-up"></i></button>
                                                        </p>
                                                        <?php
                                                        if ($meallist[$t] == '1') {
                                                            $mealch = 'Included';
                                                        } else {
                                                            $mealch = 'N.A';
                                                        }
                                                        ?>
                                                        <p><b>Meals:</b> {{$mealch}}<br></p>
                                                        <p></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <?php } ?>


                                </div>
                                <!--  <div class="tab-pane container fade" id="menu1" ></div> -->
                                <div class="tab-pane" id="menu1" style="padding-left: 0">
                                    <?php
                                    $inttitle = unserialize($pckgdetails->inttitle);
                                    $intdescription = unserialize($pckgdetails->intdescription);
                                    $meallist = unserialize($pckgdetails->meallist);

                                    for($t = 0;$t < count($inttitle);$t++)
                                    {
                                    ?>
                                    <div class="day-wise-div">
                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-lg-1">
                                                <div class="day_box">
                                                    <b>{{$t+1}}</b>
                                                    <p>Day</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 col-md-10 col-lg-11">
                                                <div class="day_txtbox">
                                                    <h3>{{$inttitle[$t]}}</h3>


                                                    <div id="max-tab" style="display:block;">
                                                        <p>{{$intdescription[$t]}}

                                                        </p>
                                                        <?php
                                                        if ($meallist[$t] == '1') {
                                                            $mealch = 'Included';
                                                        } else {
                                                            $mealch = 'N.A';
                                                        }
                                                        ?>
                                                        <p><b>Meals:</b> {{$mealch}}<br></p>
                                                        <p></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <?php } ?>


                                </div>
                            </div>
                        </div>
                        <div id="Inclusions" class="container tab-pane fade">
                            <br>
                        <?php echo $pckgdetails->Inclusionsdescription;?>
                        <!--  <h3 class="pkg-des">Inclusion</h3> -->
                            <!-- <ul style="list-style: none" class="inclusion-ul">
                                <li><i class="fa fa-caret-right"></i> Driver Service.</li>
                                <li><i class="fa fa-caret-right"></i> Guide Service.</li>
                                <li><i class="fa fa-caret-right"></i> Water.</li>
                                <li><i class="fa fa-caret-right"></i> Entrance Fees.</li>
                            </ul> -->
                            <!--
                            <br>
                            <h3 class="pkg-des">Exclusion</h3>
                            <ul style="list-style: none" class="exclusion-ul">
                                <li><i class="fa fa-caret-right"></i> Driver Service.</li>
                                <li><i class="fa fa-caret-right"></i> Guide Service.</li>
                                <li><i class="fa fa-caret-right"></i> Water.</li>
                                <li><i class="fa fa-caret-right"></i> Entrance Fees.</li>
                            </ul> -->
                        </div>
                        <div id="Hotel" class="container tab-pane fade">
                            <br>
                            <h3 class="pkg-des">Hotel Details & Price</h3>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>Hotel Name</th>
                                        <th>Star</th>
                                        <th>Twin Sharing</th>
                                        <th>Child With Bed</th>
                                        <th>Child Without Bed</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{$pckgdetails->hotelname}}</td>
                                        <td>{{$pckgdetails->hotelstar}}</td>
                                        <td>{{$pckgdetails->twinshare}}</td>
                                        <td>{{$pckgdetails->childwithbed}}</td>
                                        <td>{{$pckgdetails->childwithoutbed}}</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="Highlights" class="container tab-pane fade">

                            <h3 class="pkg-des" style="padding-bottom: 8px; margin-bottom: 20px">Exclusion</h3>
                        <?php echo $pckgdetails->highlitedescription;?>
                        <!-- <ul style="list-style: none" class="highlight-ul">

                                <li><i class="fa fa-caret-right"></i> Ananuri, Kazbegi (Tbilisi) .</li>


                            </ul> -->
                        </div>
                      <div id="cancelpolicy" class="container tab-pane fade">

                            <h3 class="pkg-des" style="padding-bottom: 8px; margin-bottom: 20px">Cancellation Policy</h3>
                        <?php echo $pckgdetails->cancelpolicy;?>
                        <!-- <ul style="list-style: none" class="highlight-ul">

                                <li><i class="fa fa-caret-right"></i> Ananuri, Kazbegi (Tbilisi) .</li>


                            </ul> -->
                        </div>

                        <div id="Map" class="container tab-pane fade"><br>
                            <p class="pkg-map"><?php echo $pckgdetails->maploc;?></p>
                            <!-- <iframe class="pkg-map"
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27169.85794126198!2d74.84327394295215!3d31.654865304803494!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39196493fdde71ef%3A0x5a89f3cec3595534!2sRanjit+Avenue%2C+Amritsar%2C+Punjab!5e0!3m2!1sen!2sin!4v1565420622666!5m2!1sen!2sin"
                                    width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                        </div>
                        <div id="Terms" class="container tab-pane fade">
                            <br>
                            <h3 class="pkg-des">Terms & Conditions</h3>
                        <?php echo $pckgdetails->termcondition;?>
                        <!--  <ul style="list-style: none" class="terms-ul">

                                <li> <i class="fa fa-caret-right"></i> Hotel standard Check-In / Check-out time are 1400 hrs and 1200 hrs respectively.</li>

                                <li> <i class="fa fa-caret-right"></i> Prices are subject to change with any sudden increase in the cost by hote
                                    ,transportation department and any new tax by the government.
                                    Rates include only those items specified in your itinerary..
                                </li>

                                <li> <i class="fa fa-caret-right"></i> Package rates are valid as per the above mentioned validity dat
                                    es and for the room category specified. Should the period of stay
                                    or room type change, above rates will not be valid..
                                </li>

                                <li> <i class="fa fa-caret-right"></i> Hotel rates are not valid during trade fairs, exhibitions and special events. A surcharge
                                    will be levied.
                                </li>

                                <li> <i class="fa fa-caret-right"></i> Above rates are valid on a minimum of 2 adults travelling together.</li>
                            </ul> -->
                        </div>
                        <?php $packid=$pckgdetails->id."%%".$pckgdetails->pricecurr."%%".$pckgdetails->newpricenew;
                        $newpackid=base64_encode($packid);
                        ?>
                        <div id="Downloads" class="container tab-pane fade">
                            <br>
                            <h3 class="pkg-des">Downloads</h3>
                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/PDF_file_icon.svg/833px-PDF_file_icon.svg.png"
                                 class="pdf-icon">

                            <span class="pdf-dl"><a href="{{action('Packagecontroller@package_pdf', $newpackid )}}" class="trip-btn" target="_blank">Download PDF1</a>
                            </span>
                        </div>
                    </div>

                </div>

                <div class="ul-active">

                    <div id="accordion">

                        <div class="card">
                            <div class="card-header">
                                <a class="card-link" data-toggle="collapse" href="#mydes">Description</a>
                            </div>
                            <div id="mydes" class="collapse show  show1" data-parent="#accordion">
                                <div class="card-body">
                                    <div id="Description" class="tab-pane active">
                                        <br>
                                         <h3 class="pkg-des">{{$pckgdetails->packgtitle}}</h3>
                                        <p class="pkg-p"><?php echo $pckgdetails->maindescription;?></p>
                                       
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#myitnerary">
                                    Itinerary
                                </a>
                            </div>
                            <div id="myitnerary" class="collapse  show1" data-parent="#accordion">
                                <div class="card-body">
                                    <div id="Itinerary" class="container tab-pane">
                                        <ul class="nav nav-tabs" style="border: none !important;">
                                            <li class="pkg-option">
                                                <a class="nav-link  active" style="min-height: 25px !important;"
                                                   data-toggle="pill" href="#daywise1">Daywise </a>
                                            </li>
                                            <li class="pkg-option">
                                                <a class="nav-link" style="min-height: 25px !important;" data-toggle="pill"
                                                   href="#full1">View Full </a>
                                            </li>

                                        </ul>


                                        <!-- Tab panes -->
                                        <div class="tab-content" style="border: none; padding-left: 0">
                                            <div class="tab-pane active" id="daywise1" style="padding-left: 0">
                                                  <?php
                                                    $inttitle = unserialize($pckgdetails->inttitle);
                                                    $intdescription = unserialize($pckgdetails->intdescription);
                                                    $meallist = unserialize($pckgdetails->meallist);

                                                    for($t = 0;$t < count($inttitle);$t++)
                                                    {
                                                    ?>
                                                    <div class="day-wise-div">
                                                        <div class="row">
                                                            <div class="col-sm-3 col-md-2 col-lg-1">
                                                                <div class="day_box">
                                                                    <b>{{$t+1}}</b>
                                                                    <p>Day</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-9 col-md-10 col-lg-11">
                                                                <div class="day_txtbox">
                                                                    <h3>{{$inttitle[$t]}}</h3>

                                                                    <p id="minimize-tab-{{$t+1}}" style="display: block">
                                                                        <?php
                                                                        // $
                                                                        // echo $ch =  substr($intdescription[$t], 0,100 );
                                                                        echo substr($intdescription[$t], 0, 100);
                                                                        ?>

                                                                        <button type="button" class="expnd newexpend" value="sh"
                                                                                id="{{$t+1}}">
                                                                            <i class="fa fa-chevron-circle-down"></i></button>
                                                                    </p>
                                                                    <div id="maximize-tab-{{$t+1}}" style="display:none;">
                                                                        <p> {{$intdescription[$t]}}

                                                                            <button type="button" class="expnd newexpend" value="hd"
                                                                                    id="{{$t+1}}">
                                                                                <i class="fa fa-chevron-circle-up"></i></button>
                                                                        </p>
                                                                        <?php
                                                                        if ($meallist[$t] == '1') {
                                                                            $mealch = 'Included';
                                                                        } else {
                                                                            $mealch = 'N.A';
                                                                        }
                                                                        ?>
                                                                        <p><b>Meals:</b> {{$mealch}}<br></p>
                                                                        <p></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <?php } ?>

                                            </div>

                                            <div class="tab-pane " id="full1" style="padding-left: 0">
                                               <?php
                                                    $inttitle = unserialize($pckgdetails->inttitle);
                                                    $intdescription = unserialize($pckgdetails->intdescription);
                                                    $meallist = unserialize($pckgdetails->meallist);

                                                    for($t = 0;$t < count($inttitle);$t++)
                                                    {
                                                    ?>
                                                    <div class="day-wise-div">
                                                        <div class="row">
                                                            <div class="col-sm-3 col-md-2 col-lg-1">
                                                                <div class="day_box">
                                                                    <b>{{$t+1}}</b>
                                                                    <p>Day</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-9 col-md-10 col-lg-11">
                                                                <div class="day_txtbox">
                                                                    <h3>{{$inttitle[$t]}}</h3>


                                                                    <div id="max-tab" style="display:block;">
                                                                        <p>{{$intdescription[$t]}}

                                                                        </p>
                                                                        <?php
                                                                        if ($meallist[$t] == '1') {
                                                                            $mealch = 'Included';
                                                                        } else {
                                                                            $mealch = 'N.A';
                                                                        }
                                                                        ?>
                                                                        <p><b>Meals:</b> {{$mealch}}<br></p>
                                                                        <p></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <?php } ?>


                                            </div>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#myinclusion2">
                                    Inclusions
                                </a>
                            </div>
                            <div id="myinclusion2" class="collapse  show1" data-parent="#accordion">
                                <div class="card-body">
                                    <div id="Inclusions" class="container tab-pane ">
                                        <br>
                                       <?php echo $pckgdetails->Inclusionsdescription;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#myhotel1">
                                    Hotel Details & Price
                                </a>
                            </div>
                            <div id="myhotel1" class="collapse  show1" data-parent="#accordion">
                                <div class="card-body">
                                    <div id="Hotel" class="container tab-pane">
                                        <br>
                                        <h3 class="pkg-des">Hotel Details & Price</h3>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead class="thead-dark">
                                                <tr>
                                                    <th>Hotel Name</th>
                                                    <th>Star</th>
                                                    <th>Twin Sharing</th>
                                                    <th>Child With Bed</th>
                                                    <th>Child Without Bed</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{{$pckgdetails->hotelname}}</td>
                                                    <td>{{$pckgdetails->hotelstar}}</td>
                                                    <td>{{$pckgdetails->twinshare}}</td>
                                                    <td>{{$pckgdetails->childwithbed}}</td>
                                                    <td>{{$pckgdetails->childwithoutbed}}</td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#myhighlight">Highlights</a>
                            </div>
                            <div id="myhighlight" class="collapse  show1" data-parent="#accordion">
                                <div class="card-body">
                                    <div id="Highlights" class="container tab-pane">
                                        <?php echo $pckgdetails->highlitedescription;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#jhb">Cancellation Policy</a>
                            </div>
                            <div id="jhb" class="collapse  show1" data-parent="#accordion">
                                <div class="card-body">
                                    <div id="Cancellation" class="container tab-pane">
                                        fggfhfccjhf fhhghfhg fggjhfhghf
                                        <?php echo $pckgdetails->cancelpolicy;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#mymapl">Map</a>
                            </div>
                            <div id="mymapl" class="collapse  show1" data-parent="#accordion">
                                <div class="card-body">
                                    <div id="Map" class="container tab-pane"><br>
                                       <?php echo $pckgdetails->maploc;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#myterm">Terms & Conditions</a>
                            </div>
                            <div id="myterm" class="collapse  show1" data-parent="#accordion">
                                <div class="card-body">
                                    <div id="Terms" class="container tab-pane">
                                        <?php echo $pckgdetails->termcondition;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#mydownload">Downloads</a>
                            </div>
                             <?php $packid=$pckgdetails->id."%%".$pckgdetails->pricecurr."%%".$pckgdetails->newpricenew;
                        $newpackid=base64_encode($packid);
                        ?>
                            <div id="mydownload" class="collapse  show1" data-parent="#accordion">
                                <div class="card-body">
                                    <div id="Downloads" class="container tab-pane">
                                        <br>
                                        <h3 class="pkg-des">Downloads</h3>
                                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/PDF_file_icon.svg/833px-PDF_file_icon.svg.png"
                                             class="pdf-icon">
                                        <span class="pdf-dl"> <a href="{{action('Packagecontroller@package_pdf', $newpackid )}}" class="trip-btn" target="_blank">Download PDF1</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
</div>



<div class="modal fade text-center py-5" style="top:30px" id="myModal_charges">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content" style="border-radius: 7px;">
            <div class="modal-header h-bg">
                <p class="p-head1">Package Enquiry</p>
                <span class="close close-btn" data-dismiss="modal">&times;</span>

            </div>
            <div class="modal-body">

                <h3 class="pt-3 mb-0 h-cancel success_modal"></h3>
                <!-- <p class="t-id-info">Your Change Request Id:1234</p> -->
                <button class="btn1 text-white mb-5 success_ok" style="margin-top:20px; margin-bottom: 0px !important">
                    Ok
                </button>
                <!--   <a role="button" class="btn1 text-white mb-5 success_ok" href="https://www.sunlimetech.com" target="_blank">Submit</a> -->
            </div>
            <div class="modal-footer f-bg mt-3">
                <span class="d-block mr-auto f-help " style="color: black"><i class="fa fa-phone"></i> Helpline:<a
                            href="#"> +61 2 5924 0804</a></span>
                <span class="f-help"><img src="{{asset('assets/images/logo.png')}}" class="f-logo"></span>
            </div>
        </div>
    </div>
</div>

<div class="modal fade myModal" id="myModaln1" role="dialog" data-backdrop="static" data-keyboard="false"
     style="background-color: #f0f8ffd1;">
    <div class="modal-dialog">
        <center><img src="{{ asset('assets/images/newf.gif')   }}"
                     style="height: auto;width: auto; display: block;margin:60% auto;position: relative;top:50%;transform: translateY(-50%)">
        </center>
    </div>
</div>
<!-- Footer -->
@include('pages.include.footer')
<!-- Copyright -->
@include('pages.include.copyright')
<script>

    $(document).on('click','.newexpend',function(){
        var id= this.id;
        var newval=$(this).val();

        if(newval=="sh")
        {
            $('#maximize-tab-'+id).show();
            $('#minimize-tab-'+id).hide();
        }
        else
        {
            $('#maximize-tab-'+id).hide();
            $('#minimize-tab-'+id).show();
        }



    });
</script>
<script>


    function expnd1() {


        var min = document.getElementById("minimize-modal-tab");
        var max = document.getElementById("maximize-modal-tab");
        if (max.style.display == "none") {

            min.style.display = "none";
            max.style.display = "block";
        } else {


            max.style.display = "none";
            min.style.display = "block";
        }
    }

    function collsp() {


        var min = document.getElementById("min-tab");
        var max = document.getElementById("max-tab");
        if (min.style.display == "none") {

            min.style.display = "block";
            max.style.display = "none";
        } else {


            max.style.display = "block";
            min.style.display = "none";
        }
    }
    function collsp1() {


        var min = document.getElementById("min-modal-tab");
        var max = document.getElementById("max-modal-tab");
        if (min.style.display == "none") {

            min.style.display = "block";
            max.style.display = "none";
        } else {


            max.style.display = "block";
            min.style.display = "none";
        }
    }
</script>
<script>
    $(function () {
        //Departure
        var date = new Date();
        date.setDate(date.getDate());
        $('.pckgdate').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy',
            startDate: date

        })
        //Date picker
        $('#pckgenquirydate').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy',
            startDate: date
        })
        //Check In
        $('#check-in').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy',
            startDate: date
        })
        //Date picker
        $('#check-out').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy',
            startDate: date
        })

    })
</script>
<script>
    $(document).on('click','.pertime',function(){
        var pertime = $("input[name='pertime']:checked").val();
        if(pertime=='2')
        {
            $('.chkenq').show();
        }
        else
        {
            $('.chkenq').hide();
        }
    })
</script>
<script>
    $(document).ready(function () {

        $(".leadage").keypress(function (e) {

            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    });

</script>
<script>
    $(document).on('click','.enq_send',function(){
        var lastname= $('#lastname').val();
        var firstname=$('#firstname').val();
        var email= $('#email').val();
        var mobile=$('#mobile').val();
        var adult= $('#adult').val();
        var child=$('#child').val();
        var countryname=$('#countryname').val();
        var pckgdate= $('.pckgdate').val();
        var alter_mobile=$('#alter_mobile').val();
        var travel_msg=$('#travel_msg').val();
        var pertime = $("input[name='pertime']:checked").val();
        var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(pertime=='2')
        {
            var travel_type="Specific Time";
            var specific_date=$('#pckgenquirydate').val();
            var specific_time=$('#spec_time').val();
        }
        else
        {
            var travel_type="Now";
            var specific_date="";
            var specific_time="";
        }
        if(firstname=="")
        {
            $('#firstname_error').text(" Enter Name");
            $('#firstname_error').show();
            $('#firstname').focus();
        }
        else if(lastname=="")
        {
            $('#firstname_error').hide();
            $('#lastname_error').text(" Enter last Name");
            $('#lastname_error').show();
            $('#lastname').focus();
        }
        else if(countryname=="")
        {
            $('#lastname_error').hide();
            $('#countryname_error').text(" Enter Country Name");
            $('#countryname_error').show();
            $('#countryname_error').focus();
        }
        else if(email=="")
        {
            $('#countryname_error').hide();
            $('#email_error').text(" Enter Email");
            $('#email_error').show();
            $('#email').focus();
        }
        else if (email !='' && !emailfilter.test(email))
        {
            $('#email_error').hide();
            $('#email_error').text("Email Not Valid");
            $('#email_error').show();
            $('#email').focus();
        }
        else if(mobile=="")
        {
            $('#email_error').hide();
            $('#mobile_error').text(" Enter Mobile");
            $('#mobile_error').show();
            $('#mobile').focus();
        }
        else if(mobile !='' &&  $('#mobile').val().length < 10 || $('#mobile').val().length >15 )
        {
            $('#email_error').hide();
            $('#mobile_error').text(" Mobile number should be between of 10 to 15-digits");
            $('#mobile_error').show();
            $('#mobile').focus();
        }
        else
        {
            $('#myModaln1').modal('show');
            var pckg_id= $('#pckg_id').val();
            var pckg_name=$('#pckg_name').val();
            var pckg_mainid=$('#pckg_mainid').val();
            var pckg_price= $('#pckg_price').val();
            $.ajax({
                url :"{{route('package_enquiry')}}",
                data : {'firstname':firstname,
                    'lastname':lastname,
                    'email':email,
                    'mobile':mobile,
                    'adult':adult,
                    'child':child,
                    'pckgdate':pckgdate,
                    'travel_msg':travel_msg,
                    'travel_type':travel_type,
                    'specific_date':specific_date,
                    'specific_time':specific_time,
                    'pckg_id':pckg_id,
                    'pckg_name':pckg_name,
                    'pckg_mainid':pckg_mainid,
                    'pckg_price':pckg_price,
                    'alter_mobile':alter_mobile,
                    'countryname':countryname,
                },
                type : 'get',
                success:function(response)
                {
                    $('#myModaln1').modal('hide');
                    if(response=='fail')
                    {

                    }
                    else
                    {
                        var chkst = response.split('%%');
                        var pckid=chkst[1];
                        $('#myModal_charges').modal('show');
                        $('.success_modal').html("<p class='thank-p'>Thanks for submitting.</p> <p class='info-p'>Your request has been created with id "+pckid+"</p>");
                    }

                }
            })
        }
    })
</script>
<script type="text/javascript">
    $(document).on('click', ".success_ok", function ()
    {
        $('#myModal_charges').modal('hide');
        location.reload();
    });
</script>
<script>
    $(document).on("click","#print_btn",function()
    {
        //Hiding extra content for printing
        $(".header").hide();
        $(".home").hide();
        $("#showmform").hide();
        $("#showmtitle").hide();
        $("#showmshare").hide();
        $('#managebook').hide();

        $(".footer").hide();
        $(".copyright").hide();
        $('#Description').addClass("active");
        $('#Description').removeClass("fade");
        $('#Itinerary').addClass("active");
        $('#Itinerary').removeClass("fade");
        $('#Inclusions').addClass("active");
        $('#Inclusions').removeClass("fade");
        $('#Hotel').addClass("active");
        $('#Hotel').removeClass("fade");
        $('#Highlights').addClass("active");
        $('#Highlights').removeClass("fade");
        $('#Sightseeing').addClass("active");
        $('#Sightseeing').removeClass("fade");
        window.print();


        //displating hidden extra content after printing

        $(".header").show();
        $(".home").show();
        $("#showmform").show();
        $("#showmtitle").show();
        $("#showmshare").show();
        $('#managebook').show();

        $(".footer").show();
        $(".copyright").show();

        $('#Itinerary').removeClass("active");
        $('#Itinerary').addClass("fade");
        $('#Inclusions').removeClass("active");
        $('#Inclusions').addClass("fade");

        $('#Hotel').removeClass("active");
        $('#Hotel').addClass("fade");
        $('#Highlights').removeClass("active");
        $('#Highlights').addClass("fade");
        $('#Sightseeing').removeClass("active");
        $('#Sightseeing').addClass("fade");

    });
</script>
<script>
    $('.card:first-child a').parent().addClass('extra-card');
    $("a.card-link").click(function(){

        $('a.card-link').parent().not(this.parentNode).removeClass('extra-card');
        $(this.parentNode).toggleClass('extra-card');
    })

</script>
</body>




