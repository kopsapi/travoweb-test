@include('pages.include.header')

<body>
	<style>
		.intro {
			width: 100%;
			padding-top: 100px;
			padding-bottom: 0px;

		}

		.add_content {
			z-index: 9;
		}

		.about-header {
			background: url("{{asset('assets/images/about-h.jpg')}}");
			height: 450px;
			margin-top: 126px;
			position: relative;
			background-repeat: no-repeat;
			background-size: cover;
			background-position: center center;
		}

		h1.about-title {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			color: white;
			background: rgba(49, 18, 75, 0.8);
			padding: 10px 35px;
		}

		h1.h1-title {
			color: white;
			font-size: 20px;
		}

		.milestones {
			background: white;
		}

		.content-overlay {
			background: rgba(49, 18, 75, 0.8);
			position: absolute;
			width: 100%;
			height: 100%;
			top: 0;
			left: 0;
			z-index: 1;
		}

		p.p-para {
			font-size: 21px;
			color: #ffffff;
			background: linear-gradient(to right, #fa9e1b, #8d4fff, #fa9e1b);
			padding: 10px;
			border-radius: 5px;
		}

		ul.points {

			text-align: initial;
		}

		.column {
			column-count: 2;
		}

		ul.points li {
			padding: 10px;
			color: #8f50fb;
			font-size: 16px;
			font-weight: 600;
		}

		ul.points li:before {
			/* font-family: fontawesome; */
			font-family: FontAwesome;
			content: "\f105";
			display: inline-block;
			padding-right: 5px;
			color: #f49a26;
		}

		.milestone_icon img {
			width: auto;
			height: 50PX;
		}

		@media screen and (max-width:768px) {
			.column {
				column-count: 1;
			}

			ul.points {
				text-align: left !important;
			}
		}
		.newerror {
    color: red;
    font-size: 13px;
    
    height: 20px !important;
    display: block;
}

input#contact_form_phone {
    width: 100%;
    margin-right: 29px;
}

.contact_form_subject {
    width: 100%;
    margin-top: 0px !important;
 
}
.contact_form_name {
    width: 100%;
    margin-right: 30px;
}
.contact_form_email{
	 width: 100%;
  
	</style>
	<div class="super_container">
		<!-- Header -->
		@include ('pages.include.topheader')
		<div class="about-header">
			<h1 class="about-title">Contact Us</h1>
		</div>
		<!-- Intro -->
		<div class="intro">
			<div class="container" style="margin-bottom: 80px">
				<div class="row">
                    <div class="col">
                    
                    <div class="contact_form_container">
                    <div class="contact_title text-center">get in touch</div>
                   <form  id="querysend" class="contact_form" method="post">
						 <input name="_token" type="hidden" id="csrf_token" value="{{ csrf_token() }}"/>
						 <input type="hidden" name="pagename" value="contactus">
						 <div class="row">
						 	<div class="col-md-6">
						 		<input type="text" name="contact_name" id="contact_form_name" class="contact_form_name input_field" placeholder="Name" required="required" autocomplete="off" >
								<span class="newerror" id="contact_form_name_error" style="visibility: hidden;"></span>
						 	</div>
						 	<div class="col-md-6">
						 		<input type="text" name="contact_email" id="contact_form_email" class="contact_form_email input_field" placeholder="E-mail" required="required" autocomplete="off" >
								<span class="newerror" id="contact_form_email_error" style="visibility: hidden;">Email not valid</span>
						 	</div>
						 </div>
						  <div class="row">
						 	<div class="col-md-6">
						 		<input type="text" name="contact_phone" id="contact_form_phone" class="contact_form_phone input_field" placeholder="Phone" required="required" autocomplete="off" >
						<span class="newerror" id="contact_form_phone_error">
							
						</span>
						 	</div>
						 	<div class="col-md-6">
						 		<input type="text" name="contact_subject" id="contact_form_subject" class="contact_form_subject input_field" placeholder="Subject" required="required" autocomplete="off">
						 	</div>
						 </div>
						 <div class="row">
						 	<div class="col-md-12">
						 		<textarea name="contact_message" id="contact_form_message" class="text_field contact_form_message" name="message" rows="4" placeholder="Message" required="required" autocomplete="off"></textarea>
						 	</div>
						 </div>
						
						
						
						
						
						<button type="button" id="form_submit_button_query" class="form_submit_button button">send message<span></span><span></span><span></span></button>
					</form>
                    </div>
                    </div>
                    </div>
			</div>
			
			<!-- Footer -->

			@include ('pages.include.footer')
			@include ('pages.include.copyright')
		</div>
	</div>
</body>

</html>
<script>

	$(document).on('click','#form_submit_button_query',function(){
		var contact_form_name=$('#contact_form_name').val();
		var contact_form_email=$('#contact_form_email').val();
		var contact_form_subject=$('#contact_form_subject').val();
		var contact_form_message=$('#contact_form_message').val();
		var contact_form_phone=$('#contact_form_phone').val();
		var filter = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var error=0;

		if(contact_form_name=="")
		{
			error++;
			$("#contact_form_name_error").focus();
			$('#contact_form_name_error').text("Please Enter Name");
			
			$("#contact_form_name_error").css('visibility', 'visible');

		}
		else
		{
			
			$("#contact_form_name_error").css('visibility', 'hidden');
		}
		if(contact_form_email=="")
		{
			error++;
			$("#contact_form_email_error").focus();
			$('#contact_form_email_error').text("Please Enter Email");
			
			$("#contact_form_email_error").css('visibility', 'visible');

		}
		else if(contact_form_email!="" && !filter.test(contact_form_email))
		{
		
			error++;
			$("#contact_form_email_error").focus();
			$('#contact_form_email_error').text("Please Enter Valid Email");
			$("#contact_form_email_error").css('visibility', 'visible');
		}
		else
		{
			$("#contact_form_email_error").css('visibility', 'hidden');
		}
	

		if(error==0)
		{
			
			 var formData=$('#querysend').serialize()
			$.ajax({
						url:"{{route('query_insert')}}",
						data:formData,
						type:"POST",
						success:function(data)
						{
							if(data=="success")
							{
								swal({

                                   title: "Successfully Enquiry Submit",

                                   text: "",

                                   type: "success",

                                   showCancelButton: false,

                                   confirmButtonColor: "#DD6B55",

                                   confirmButtonText: "Ok",

                                   cancelButtonText:false,

                                   closeOnConfirm: false,

                                   closeOnCancel: false

                               }, function(isConfirm) {

                                   if (isConfirm) {

                                     location.reload();



                                 } 

                           });
							}
							else
							{
								swal("Error!", "unable to Enquiry submit!", "error");
							}
						}
			})
		}

	})
</script>