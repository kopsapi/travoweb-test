@include('pages.include.header')

<body>

	<div class="super_container">

		<!-- Header -->

		@include('pages.include.topheader')

		<div class="home">

			<div class="home_background parallax-window" data-parallax="scroll"
				data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div>

			<div class="home_content">

				<div class="home_title">FAQs</div>

			</div>

		</div>

		<!-- Intro -->

		<div class="intro">

			<div class="container">

				<div class="row">

					<div class="col-lg-12">

						<div class="intro_content nano-content">

							<div class="intro_title" style="margin-top:100px">FAQs</div>

							<h3>Do I need to re-confirm my airline booking?</h3>

							<p>No, once your flight is booked, you do not need to reconfirm it. As soon as the booking
								process is complete, we will send you an email with details of your confirmed booking.
								This email contains the information you will need to check in for your flight. We also
								recommended that you familiarize yourself with the terms and conditions on your e-ticket
								as this relates to airline policy. If you don’t see a confirmation, give us a call at
								+61-2-5924-0804 or email our Customer Support at info@travoweb.com</p>



							<h3>How do I cancel a hotel reservation?</h3>



							<p>Hotel cancellation policies varies. Certain reservations are non-cancellable and non-
								changeable. Others require you to pay a penalty if you cancel. Still others are fully
								refundable. Check the hotel cancellation policy when you book. If changes to your pre-
								paid reservation are allowed, any changes to a pre-paid reservation are only permitted
								as a cancellation and booking of a new reservation. Please bear in mind that while
								Travoweb does not charge fees for changes or cancellations, depending on the rate plan,
								some hotels may impose non-refundable rate plans or fees that Travoweb is required to
								pass along. Such fees are highest when cancellations occur within 48 hours of your
								scheduled travel dates.</p>



							<h3>How do I cancel a flight reservation?</h3>



							<p>If you booked your flight ticket with Travoweb and need to cancel your booking, please
								call our help line on +61-2-5924-0804. Travoweb Customer Service is available from 08:00
								to 20:00 IST, 7 days a week. Please note that if your flight is non-refundable, you will
								not receive a refund. In addition to any airline refund charges, Travoweb will impose an
								INR 550 processing charge.</p>



							<p>To review the airline’s rules and regulations associated with your ticket, please read
								itinerary carefully.</p>



							<h3>How do I change a flight reservation?</h3>



							<p>If your travel plans change and you need a different flight than what you originally
								booked, please consider the following:</p>



							<ul class="ul-icon">

								<li>The airlines will assess a fee for most changes to your flights. The change fee
									varies with every flight individually. The fee will be passed on to you.</li>

								<li>When changing your reservation, the new flights selected must be on the same
									airline. It may be helpful to familiarize yourself with the alternative flights.
								</li>

								<li>If there are differences in the airfare between the flights originally booked and
									the new flights selected, the difference will be charged to you.</li>

								<li>Most airlines do not allow changes to the names of the travellers originally booked
									without incurring a change fee.</li>

							</ul>



							<p>If you want to change your flight arrangements, call Travoweb on +91 6239 775233.
								Customer Support is available from 08:00 to 20:00, 7 days a week.</p>



							<h3>If I need to cancel my flight, will I be credited or refunded?</h3>



							<p>Before you make any changes to your reservations, check the airline rules and
								restrictions that apply to your flight; they will be listed with your itinerary online.
								These rules are outlined in the Rules & Restrictions link below your flight information.
								As a general guide, most airline reservations are non-refundable, and some airlines do
								not allow any changes to booked reservations. Most airlines do not allow exceptions to
								their rules and restrictions.</p>



							<p>Some airlines will issue a flight credit for future travel. These credits are usually
								valid for one year after the date of issue and can only be used by the traveller on the
								original ticket on their respective airline. Our customer service representatives can
								tell you what rules and restrictions apply to your flight credit, including the airline
								change fee per ticket, and any additional fare you may be charged. If your flight
								reservation allows for a refund, Travoweb will process the refund the day you cancel
								your reservation. The time it takes to post the refund to your credit card account
								depends on the airline and your credit card company.</p>



							<h3>How do I get a boarding pass?</h3>



							<p>Your boarding pass will be issued when you check in for your flight online or at the
								airport.</p>



							<ul class="ul-icon">

								<li>Check-in online, up to 24-hours before your flight departure, and print out your
									boarding pass at home or when you arrive at the airport. To check-in online, you
									will need the airline confirmation code, a 6 digit alphanumeric code (e.g. L84RTL)
									available in your itinerary. Please note this is not your Travoweb itinerary number.
									Certain airlines allow you to look up your reservation with your traveler name and
									the credit card used to book the flight.</li>

								<li>Airport Check-in: You can receive a boarding pass from a check-in kiosk or the
									check- in agent when you arrive at the airport.</li>

							</ul>



							<h3>Airline General Policy</h3>



							<p>AIRLINE RULES, ITINERARY CHANGES AND PRICE CHANGES:</p>



							<p>Airline tickets available through this Site are subject to the published conditions of
								carriage and rules, including but not limited to cancellation policies, of the
								applicable airline. The contract of carriage in use by the applicable airline, when
								issued, shall be between the applicable airline and the passenger. Airlines retain the
								right to adjust flight times and schedules at any time - schedule changes can result in
								an itinerary that falls outside of contractual agreements. Airlines may also in their
								discretion change or cancel flights or itineraries. Travoweb endeavors to publish and
								maintain accurate prices and information for its airline services. Airlines provide us
								with the price and other information related to these services. In the event, however,
								that a service is listed or provided to us at an incorrect price or with incorrect
								information due to typographical error or other error in pricing or service information
								received from an airline, Travoweb and/or the airline retain the right to refuse or
								cancel any Requests placed for such service. Travoweb and/or the airline shall have the
								right to refuse or cancel any such Requests whether or not the order has been confirmed
								and/or your credit card charged. If your credit card has already been charged for the
								purchase and your Request is canceled because of incorrect airline information, Travoweb
								and/or the airline will promptly issue a credit to your credit card account in the
								amount of the charge. In the event that an airline publishes a fare at one price and
								later determines not to honor that fare at that price, Travoweb is not responsible and
								cannot honor the price that was originally listed by the airline. Only the airline can
								determine whether or not to honor the miss-stated price.</p>



							<h3 style="color:#31124b;">Airline Baggage Policy</h3>



							<h3>BAGGAGE AND OTHER ANCILLARY AIRLINE FEES</h3>



							<p>The airlines may require you to pay additional fees at the airport for certain services
								and/ or if you exceed certain limits as dictated by the airline, such as the number of
								bags or weight allowed, and some airlines do not offer a free baggage allowance.</p>

						</div>

					</div>

				</div>

			</div>

		</div>

		<!-- Footer -->

		@include('pages.include.footer')

		<!-- Copyright -->

		@include('pages.include.copyright')

	</div>

</body>

</html>