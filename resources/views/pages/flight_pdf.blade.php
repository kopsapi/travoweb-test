<!DOCTYPE html>
<html>

<head>
<title>Booking Flight</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
<style>
	body {
		font-family: 'Roboto', sans-serif;
	}
	
	.container {
		width: 100%;
	}
	
	.row {
		margin-right: -15px;
		margin-left: -15px;
	}
	
	.row:after,
	.row:before {
		display: table;
		content: " ";
		clear: both;
	}
	
	.col-lg-12 {
		width: 100%;
		float: left;
		padding-left: 15px;
		padding-right: 15px;
	}
	
	.col-md-1 {
		width: 8.33333333%;
		float: left;
		padding-left: 15px;
		padding-right: 15px;
	}
	
	.col-md-2 {
		width: 10.66666667%;
		float: left;
		padding-left: 15px;
		padding-right: 15px;
	}
	
	.col-md-3 {
		width: 25%;
		float: left;
		padding-left: 15px;
		padding-right: 15px;
	}
	
	.col-md-4 {
		width: 29%;
		float: left;
		padding-left: 15px;
		padding-right: 15px;
	}
	
	.col-md-5 {
		width: 39%;
		float: left;
		padding-left: 15px;
		padding-right: 15px;
	}
	
	.col-md-6 {
		width: 46%;
		float: left;
		padding-left: 15px;
		padding-right: 15px;
	}
	
	.col-md-7 {
		width: 58.33333333%;
		float: left;
		padding-left: 15px;
		padding-right: 15px;
	}
	
	.col-md-8 {
		width: 60.66666667%;
		float: left;
		padding-left: 15px;
		padding-right: 15px;
	}
	
	.col-md-9 {
		width: 75%;
		float: left;
		padding-left: 15px;
		padding-right: 15px;
	}
	
	.col-md-10 {
		width: 83.33333333%;
		float: left;
		padding-left: 15px;
		padding-right: 15px;
	}
	
	.col-md-11 {
		width: 91.66666667%;
		float: left;
		padding-left: 15px;
		padding-right: 15px;
	}
	
	.col-md-12 {
		width: 90%;
		float: left;
		padding-left: 15px;
		padding-right: 15px;
	}
	
	.invoice-content {
		padding: 5px 20px;
		border: 1px solid #DDD;
		box-shadow: 0px 0px 17px 1px rgba(0, 0, 0, 0.1);
	}
	
	.e-ticket {
		color: #00206A;
		font-weight: 600;
		margin-bottom: 10px;
	}
	
	.e-ticket .booking-id {
		margin-bottom: 5px;
		font-size:14px;
	}
	.e-ticket .booked-on {
		margin-bottom: 5px;
		font-size:14px;
	}
	
	.e-ticket h4 {
		color: #fa9e1b;
		font-weight: 600;
	}
	
	.onward-icon i {
		font-size: 40px;
		color: #fa9e1b;
	}
	
	.onward-icon {
		float: left;
		width: 50px;
	}
	
	.onward-flight {
		margin-bottom: 20px;
		padding-bottom: 10px;
		border-bottom: 1px solid #fa9e1b;
	}
	
	.onward-flight-details h5 {
		margin-bottom: 0px;
		color: #333;
		font-weight: 600;
	}
	
	.onward-flight-details h4 {
		color: #00206A;
		font-weight: 600;
		font-size:16px;
	}
	
	.onward-flight-no {
		text-align: right;
	}
	
	.onward-flight-no h5 {
		margin-bottom: 5px;
		color: #333;
		font-weight: 600;
	}
	
	.onward-flight-no h4 {
		color: #00206A;
		font-weight: 700;
	}
	
	.flight-logo {
		float: left;
		width: 50px;
	}
	
	.flight-name-no {}
	
	.flight-name-no h4 {
		color: #00206A;
		margin-top: 6px;
	}
	
	.flight-ref {
		text-align: right;
	}
	
	.flight-ref a {
		color: green;
		font-size: 16px;
	}
	.flight-ref {
		color: green;
		font-size: 16px;
	}
	
	.flight-non-ref {
		text-align: right;
	}
	
	.flight-non-ref a {
		color: red;
		font-size: 16px;
	}
	.flight-non-ref{
		color: red;
		font-size: 16px;
	}
	
	.invoice-flight-name {
		margin-bottom: 30px;
	}
	
	.invoice-flight-details {
		margin-bottom: 20px;
		padding-bottom: 20px;
		border-bottom: 1px solid #fa9e1b;
	}
	
	.invoice-flight-details .flight-and-time {
		color: #333;
	}
	
	.invoice-flight-details .flight-and-time .day {
		font-size: 16px;
		font-weight: 600;
		margin-bottom: 5px;
	}
	
	.invoice-flight-details .flight-and-time .city-time {
		font-size: 20px;
		margin-bottom: 5px;
	}
	
	.invoice-flight-details .flight-and-time .airport {
		font-weight: 600;
	}
	
	.invoice-flight-duration {
		text-align: center;
	}
	
	.invoice-flight-duration .clock-icon {
		margin-bottom: 8px;
		text-align: center;
	}
	
	.invoice-flight-duration .clock-icon i {
		font-size: 25px;
		color: #fa9e1b;
	}
	
	.invoice-flight-duration .hours {
		font-size: 16px;
		margin-bottom: 3px;
		color: #00206A;
		font-weight: 600;
	}
	
	.invoice-flight-duration .invoice-flight-class {
		font-size: 16px;
		color: #00206A;
		font-weight: 600;
	}
	
	.traveller-details {
		margin-bottom: 30px;
	}
	
	.traveller-details .traveller {
		color: #fa9e1b;
		font-weight: 600;
		text-transform: uppercase;
		margin-bottom: 10px;
	}
	
	.traveller-details .ticket-id {
		color: #fa9e1b;
		font-weight: 600;
		text-transform: uppercase;
		text-align: right;
		margin-bottom: 10px;
	}
	.traveller-details .ticket-no {
		color: #fa9e1b;
		font-weight: 600;
		text-transform: uppercase;
		text-align: right;
		margin-bottom: 10px;
	}
	
	.traveller-details .traveller-name {
		font-size: 15px;
		color: #00206A;
		font-weight: 600;
		line-height:26px;
	}
	
	.traveller-details .traveller-ticket-no {
		font-size: 15px;
		color: #00206A;
		font-weight: 600;
		text-align: right;
		line-height:26px;
	}
	
	.traveller-details .traveller-ticket-id {
		font-size: 15px;
		color: #00206A;
		font-weight: 600;
		text-align: right;
		line-height:26px;
	}
	.invoice-important {
		background: #EEE;
		box-shadow: 0px 0px 17px 1px rgba(0, 0, 0, 0.1);
		padding: 20px 10px;
		border: 1px solid #DDD;
		border-radius: 5px;
		margin-bottom: 30px;
	}
	
	.invoice-important .heading {
		font-size: 18px;
		color: #fa9e1b;
		font-weight: 600;
		text-align: center;
		margin-bottom: 10px;
		text-transform: uppercase;
	}
	
	.invoice-important ul li {
		font-size: 14px;
		text-align: justify;
		color: #333;
		line-height: 20px;
		position: relative;
		margin-bottom: 10px;
		font-weight: 500;
	}
	
	.baggage-policy {
		margin-bottom: 30px;
	}
	
	.baggage-policy .heading {
		font-size: 18px;
		color: #fa9e1b;
		font-weight: 600;
		margin-bottom: 10px;
		display: inline-block;
	}
	
	.baggage-policy .heading i {
		margin-right: 10px;
		display: inline-block;
	}
	
	.baggage-policy .col-md-12 {
		width: 100%;
	}
	
	.baggage-policy .table {
		background: transparent;
		width: 100%;
	}
	
	.baggage-policy table th {
		white-space: nowrap;
	}
	
	.baggage-policy .table-bordered>tr>th {
		border: 1px solid #ddd;
		vertical-align: bottom;
	}
	
	.table>tr>th {
		padding: 8px;
		line-height: 1.42857143;
		text-align: left;
		background: #F5F5F5 !important;
		font-size:14px;
		font-weight:500;
	}
	
	.table-bordered>tr>td {
		border: 1px solid #ddd;
		padding: 8px;
		line-height: 1.42857143;
		vertical-align: top;
	}
	
	.baggage-policy table th,
	.baggage-policy table td {
		background: #FFF4e7;
		color: #00206A;
		border: 1px solid #00206A;
	}
	
	.baggage-policy table td p {
		margin-bottom: 5px;
	}
	
	.baggage-policy table th,
	.baggage-policy table th p
	{
		font-weight: 500;
		font-size:14px;
		padding-left:10px;
		padding-right:10px;
	}
	.baggage-policy table td,
	.baggage-policy table td p {
		color: #333;
		font-weight: 500;
		font-size:12px;
		padding-left:15px;
		padding-right:15px;
	}
	
	.cancellation-policy {
		margin-bottom: 30px;
	}
	
	.cancellation-policy .col-md-12 {
		width: 100%;
	}
	
	.cancellation-policy .heading {
		font-size: 18px;
		color: #fa9e1b;
		font-weight: 600;
		margin-bottom: 10px;
	}
	
	.cancellation-policy .heading i {
		margin-right: 10px;
	}
	
	.cancellation-policy table th {
		white-space: nowrap;
	}
	
	.cancellation-policy table th,
	.cancellation-policy table td {
		background: #FFF4e7;
		color: #00206A;
		border: 1px solid #00206A;
	}
	
	.cancellation-policy table td p {
		margin-bottom: 5px;
	}
	.cancellation-policy table th,
	.cancellation-policy table th p
	{
		font-weight: 500;
		font-size:14px;
		padding-left:10px;
		padding-right:10px;
	}
	.cancellation-policy table td,
	.cancellation-policy table td p {
		color: #333;
		font-weight: 500;
		font-size:12px;
		padding-left:10px;
		padding-right:10px;
	}
	.cancellation-policy table td ul {
		padding-inline-start: 0px !important;
	}
	
	.fare-payment-policy {
		margin-bottom: 20px;
	}
	
	.fare-payment-policy .heading {
		font-size: 18px;
		color: #fa9e1b;
		font-weight: 600;
		margin-bottom: 10px;
	}
	
	.fare-payment-policy .fare-left {
		font-size: 14px;
		color: #333;
		font-weight: 500;
		margin-bottom: 5px;
		font-weight: 600;
	}
	
	.fare-payment-policy .fare-right {
		font-size: 14px;
		color: #00206A;
		font-weight: 600;
		margin-bottom: 7px;
		text-align: right;
	}
	
	.fare-payment-policy .fare-right i {
		font-size: 18px;
		color: #fa9e1b;
		text-align: right !important;
	}
	
	.total-fare {
		padding-top: 10px;
		border-top: 1px solid #fa9e1b;
		border-bottom: 1px solid #fa9e1b;
		padding-bottom: 4px;
		padding-left: 10px;
		padding-right: 10px;
	}
	
	.fare-payment-policy .pl-15 {
		padding-left: 15px;
		font-weight: 500;
	}
	
	.total-fare .fare-left,
	.total-fare .fare-right {
		color: #00206A;
		font-weight: 600;
		font-size: 16px;
	}
	
	.text-right {
		text-align: right;
	}
	
	.img-responsive {
		display: block;
		vertical-align: middle;
		width: auto;
		height: auto;
		max-width: 100%;
	}
	
	.rupee {
		height: 12px;
		margin-top: 3px;
	}
	
	.clock {
		height: 25px;
	}
.pdf-footer
{
	margin-top:30px;
}
.pdf-footer .earphone img
{
	width:30px;
}
.pdf-footer .earphone
{
	float:left;
	width:40px;
}
.pdf-footer .support
{
	margin-left:50px;
}
.pdf-footer .support .small-text
{
	font-size:12px;
	color:#666;
}
.pdf-footer .support .text-bold
{
	font-size:14px;
	color:#000;
	white-space:nowrap;
}
</style>
</head>

<body>
    <div class="invoice">
        <div class="container">
            <?php
				$segment = unserialize($flightbook->segment);
				$bookdate1 = explode('T',$segment[0]['Origin']['DepTime']);
				$bookingdate= date("d M Y" , strtotime($bookdate1[0]));
				$bookingtime = date("H:s" , strtotime($bookdate1[1]));
				$imgsou =  'assets/images/flag/'.$segment[0]['Airline']['AirlineCode'].'.gif';
				$imgsou_new =  "http://travoweb.com/apiflight/assets/images/flag/".$segment[0]['Airline']['AirlineCode'].".gif";
			?>
                <div class="invoice-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="e-ticket">
                                <h4>E-Ticket</h4>
                                <div class="booking-id">Booking ID : {{$flightbook->bookingId}}</div>
                                <div class="booked-on">Booking ID : {{$bookingdate}} {{$bookingtime}}</div>
                            </div>
                        </div>
						<div class="col-md-6">
							<div class="logo" style="float:right; padding-top:10px;">
									<img src="{{ asset('assets/images/logo.png') }}" alt="" style="width: 100px">
							</div>
						</div>
                    </div>
                    <div class="onward-flight">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="onward-flight-details">
                                    <h5>Onward Flight</h5>
                                    <h4>{{$flightbook->orign}} to {{$flightbook->destination}}</h4> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="onward-flight-no">
                                    <h5>PNR</h5>
                                    <h4>{{$flightbook->pnrno}}</h4> </div>
                            </div>
                        </div>
                    </div>
                    <div class="invoice-flight-name">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="flight-name-no">
                                    <h4>{{$segment[0]['Airline']['AirlineName']}}  {{$segment[0]['Airline']['AirlineCode']}}-{{$segment[0]['Airline']['FlightNumber']}}</h4> </div>
                            </div>
                            <div class="col-md-6">
                                @if($flightbook->refund=='Refundable')
                                <div class="flight-ref"> Refundable</a>
                                </div>
                                @else
                                <div class="flight-non-ref"> Non Refundable</a>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <?php
                    for($seg_i=0;$seg_i<count($segment);$seg_i++)
                    {
                        $arvdate1 = explode('T',$segment[$seg_i]['Destination']['ArrTime']);
                                            $arvtime2 = date("H:s" , strtotime($arvdate1[1]));
                                            $arvtime1 = date("H:i:s" , strtotime($arvdate1[1]));
                                            $arvnewdate1= date("Y-m-d" , strtotime($arvdate1[0]));
                                             $arvnewdatenew= date("l d M Y " , strtotime($arvdate1[0]));
                                            $devdate1 = explode('T',$segment[$seg_i]['Origin']['DepTime']);
                                            $depnewdatedetail= date("l d M Y" , strtotime($devdate1[0]));
                                            $depnewtimedetail = date("H:s" , strtotime($devdate1[1]));
                                            $depdatetime = date("H:i:s" , strtotime($devdate1[1]));
                                            $depdatedistance= date("Y-m-d" , strtotime($devdate1[0]));
                                            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $depdatedistance.''. $depdatetime);
                                            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $arvnewdate1.''. $arvtime1);
                                            $totalDuration = $from->diffInSeconds($to);
                                            $durationhour =  gmdate('H', $totalDuration);
                                            $durationmin =  gmdate('s', $totalDuration);
                                            $newtime=$durationhour.'H : '.$durationmin.'M';
                    ?>
                        <div class="invoice-flight-details">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="flight-and-time">
                                        <div class="city-time" style="font-size:15px "><strong>{{$segment[$seg_i]['Origin']['Airport']['AirportCode']}}</strong> {{$depnewtimedetail}}</div>
                                        <div class="day" style="font-size:15px ">{{$depnewdatedetail}}</div>
                                        <div class="airport" style="font-size:15px ">{{$segment[$seg_i]['Origin']['Airport']['CityCode']}}, {{$segment[$seg_i]['Origin']['Airport']['AirportName']}}, Terminal {{$segment[$seg_i]['Origin']['Airport']['Terminal']}}</div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="invoice-flight-duration">
                                        <div class="clock-icon"><img src="{{ asset('assets/images/clock-img.png')}}" class="img-responsive clock"> </div>
                                        <div class="hours" style="font-size:15px ">{{$newtime}}</div>
                                        <div class="invoice-flight-class" style="font-size:15px ">{{$flightbook->flightcabinclass}}</div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="flight-and-time text-right">
                                        <div class="city-time" style="font-size:15px "><strong>{{$segment[$seg_i]['Destination']['Airport']['AirportCode']}}</strong>{{$arvtime2}}</div>
                                        <div class="day" style="font-size:15px ">{{$arvnewdatenew}}</div>
                                        <div class="airport" style="font-size:15px ">{{$segment[$seg_i]['Destination']['Airport']['CityCode']}}, {{$segment[$seg_i]['Destination']['Airport']['AirportName']}}, Terminal {{$segment[$seg_i]['Destination']['Airport']['Terminal']}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php }?>
                            <div class="traveller-details">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="traveller">Traveller</div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="ticket-no">Ticket ID</div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="ticket-id">Ticket No.</div>
                                    </div>
                                </div>
                                <?php
                        $passengers = unserialize($flightbook->passenger_detail);

                        for($pass=0;$pass< count($passengers);$pass++)
                        {
                            if($passengers[$pass]['PaxType']=='1')
                            {
                                $passcheck='Adults';
                            }
                            else if($passengers[$pass]['PaxType']=='2')
                            {
                                $passcheck='Child';
                            }
                            else if($passengers[$pass]['PaxType']=='3')
                            {
                                $passcheck='Infant';
                            }
                            else
                            {
                                $passcheck='';
                            }
                        ?>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="traveller-name">{{$passengers[$pass]['Title']}}. {{ucfirst($passengers[$pass]['FirstName'])}} {{ucfirst($passengers[$pass]['LastName'])}} ({{$passcheck}})</div>
                                        </div>
                                        @if(!empty($passengers[$pass]['Ticket']['TicketId']))
                                        <div class="col-md-4">
                                            <div class="traveller-ticket-no">{{$passengers[$pass]['Ticket']['TicketId']}}</div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="traveller-ticket-id">{{$passengers[$pass]['Ticket']['TicketNumber']}}</div>
                                        </div>
                                        @else
                                        <div class="col-md-4">
                                            <div class="traveller-ticket-id">{{$flightbook->pnrno}}</div>
                                        </div>
                                        @endif
                                    </div>
                                    <?php } ?>
                            </div>
                            <div class="invoice-important">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="heading">Important</div>
                                        <ul>
                                            <li>Please carry your Government ID proof for all passengers to show during security check and check-in. Name on Government ID proof should be same as on your ticket.</li>
                                            <li>We recommended you to reach airport 2 hrs before departure time. Airline check-in counters typically close 1hr prior to departure time.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="baggage-policy">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="heading">Baggage Policy</div>
                                        <table class="table table-responsive table-bordered">
                                            <tr>
                                                <th>Check-In(Adult & Child)</th>
                                                <td>As per Airline Policy</td>
                                            </tr>
                                            <tr>
                                                <th>Hand-Baggage(Adult & Child)</th>
                                                <td>7 KG / person</td>
                                            </tr>
                                            <tr>
                                                <th>Terms & Conditions</th>
                                                <td>
                                                    <p>Please check with the airline on the dimensions of the baggage</p>
                                                    <p>The baggage policy is only indicative and can change any time. You are advised to check with the airline before travel to know latest baggage policy</p>
                                                    <p>You are advised to check with the airline for extra baggage charges</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="cancellation-policy">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="heading">Cancellation Policy</div>
                                        <table class="table">
                                            <tr>
                                                <th>Airline Cancellation Charges</th>
                                                <td>As per airline policy.Paytm doesnt charge any additional cancellation processing fee.</td>
                                            </tr>
                                            <tr>
                                                <th>Direct cancellation with airline</th>
                                                <td>Post cancellation with airline, please contact Paytm customer care for refund</td>
                                            </tr>
                                            <tr>
                                                <th>Booking modifications</th>
                                                <td>Date/Flight change is allowed upto 24 Hrs before departure as per airline policy. Contact our Paytm flights customer care - 0120 4880880 for modifications. Most airlines dont allow name amendments.</td>
                                            </tr>
                                            <tr>
                                                <th>Terms & Conditions</th>
                                                <td>
                                                    <p>We accept cancellations, only before 24 Hrs from departure time</p>
                                                    <p>For cancellations, within 24 hours before departure you need to contact the airline. Post cancellation by airline, you can contact Paytm for refund, if any</p>
                                                    <p>Convenience fee is non-refundable. Any cashback availed will be adjusted in final refund amount</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <?php
                    $flightfare = unserialize($flightbook->fare);

                    if($flightbook->mealprice !='' || $flightbook->mealprice =='0')
                    {
                         $mealprice = $flightbook->mealprice;
                    }
                    else
                    {
                         $mealprice=0;
                    }
                    if($flightbook->bag_price !='' || $flightbook->bag_price =='0')
                    {
                    	$baggprice = $flightbook->bag_price;
                    }
                    else
                    {
                        $baggprice=0;
                    }
                    $othercharges=$flightbook->flightothercharges;  
                	 // $othercharges = round($flightfare['OtherCharges'] +  $flightfare['TdsOnPLB'] + $flightfare['TdsOnIncentive']+$flightfare['TdsOnCommission']);
                	$basefare =$flightbook->flightmarginprice;
                	$taxfare = $flightbook->flighttax;

                    $totalamount =  $basefare +  $taxfare + $othercharges + $mealprice + $baggprice ;
                    // $gst =( $totalamount1 * 5 ) /100;
                    // $totalamount = $totalamount1 + $gst;
                    if($flightbook->currency_icon=='fa-inr')
                    {
                    	
                    	 $imgicon = "Rs.";

                    }
                    else
                    {
                    	$imgicon="$";
                    }
                    ?>
                                <div class="fare-payment-policy">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="heading">Fare & Payment Details</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="fare-left">Base Fare</div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="fare-right">{{$imgicon}} {{number_format($basefare,2)}}</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="fare-left">Tax</div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="fare-right">{{$imgicon}} {{number_format($taxfare,2)}}</div>
                                        </div>
                                    </div>
                                    @if($baggprice !='0')
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="fare-left">Extra Baggage Charges({{$flightbook->bag_weight}} Kg)</div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="fare-right">{{$imgicon}}{{number_format($baggprice,2)}}</div>
                                        </div>
                                    </div>
                                    @endif @if($mealprice!='0')
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="fare-left">Meal Charges({{$flightbook->mealname}})</div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="fare-right">{{$imgicon}} {{number_format($mealprice,2)}}</div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="fare-left ">Other Charges</div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="fare-right">{{$imgicon}}{{number_format($othercharges,2)}}</div>
                                        </div>
                                    </div>

                                    <div class="total-fare">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="fare-left">Total Fare</div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="fare-right">{{$imgicon}} {{number_format($totalamount,2)}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                </div>
        </div>
    </div>

	<div class="pdf-footer">
		<hr style="width: 50%;border: none;border-bottom: 1px solid #5e5e5e; margin: auto">
		<div class="row">

			<div class="col-md-12" style="width:100%; margin:0 auto;font-size: 13px;text-align: center">

				<img src="{{asset('assets/images/logo.png')}}" style="width: 120px;margin-top: 10px" class="img-fluid d-block mx-auto">
				
			{{-- 	<p style="margin: 5px!important; color: black;"><b> Ph</b> +61 490149833</p> --}}
				@if($flightbook->currency_icon=='fa-inr')
				<p style="margin: 5px!important; color: black;"><b> GST No.</b> 03BDHPK8214M1ZG</p>
				@endif
				<p style="margin: 5px!important;"><span></span>  <a href="#">info@travoweb.com</a>, <span></span>Travoweb.com</p>

			</div>
		</div>
	</div>
	<hr>
    <!-- return invoice -->
    <?php
	if(!empty($flightbook_return))
	{
		$segment_return = unserialize($flightbook_return->segment);
		$bookdate1 = explode('T',$segment_return[0]['Origin']['DepTime']);
		$bookingdate_return= date("d M Y" , strtotime($bookdate1[0]));
		$bookingtime_return = date("H:s" , strtotime($bookdate1[1]));
		$imgsou_return =  'assets/images/flag/'.$segment_return[0]['Airline']['AirlineCode'].'.gif';
	?>
	<div class="invoice">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="logo" style="margin-top:15px" style="margin-bottom:15px">
						<center>
							<img src="{{ asset('assets/images/logo.png') }}" alt="" style="width: 150px"></center>
					</div>
					<!--  <div class="intro_content nano-content">
						<div class="intro_title">INVOICE</div>
					</div> -->
				</div>
			</div>

			<div class="invoice-content">
				<div class="row">
					<div class="col-lg-12">
						<div class="e-ticket">
							<h4>E-Ticket</h4>
							<div class="booking-id">Booking ID : {{$flightbook_return->bookingId}}</div>
							<div class="booked-on">Booking ID : {{$bookingdate_return}} {{$bookingtime_return}}</div>
						</div>
					</div>
				</div>
				<div class="onward-flight">
					<div class="row">
						<div class="col-md-6">
							<div class="onward-flight-details">
								<h5>Onward Flight</h5>
								<h4>{{$flightbook_return->orign}} to {{$flightbook_return->destination}}</h4> </div>
						</div>
						<div class="col-md-6">
							<div class="onward-flight-no">
								<h5>PNR</h5>
								<h4>{{$flightbook_return->pnrno}}</h4> </div>
						</div>
					</div>
				</div>
				<div class="invoice-flight-name">
					<div class="row">
						<div class="col-md-6">
							<div class="flight-name-no">
								<h4>{{$segment_return[0]['Airline']['AirlineName']}}  {{$segment_return[0]['Airline']['AirlineCode']}}-{{$segment_return[0]['Airline']['FlightNumber']}}</h4> </div>
						</div>
						<div class="col-md-6">
							@if($flightbook_return->refund=='Refundable')
							<div class="flight-ref"> <a href="javascript:void()">Refundable</a> </div>
							@else
							<div class="flight-non-ref"> <a href="javascript:void()">Non Refundable</a> </div>
							@endif
						</div>
					</div>
				</div>
				<?php
				for($seg_i=0;$seg_i<count($segment_return);$seg_i++)
				{
					$arvdate1 = explode('T',$segment_return[$seg_i]['Destination']['ArrTime']);
										$arvtime2 = date("H:s" , strtotime($arvdate1[1]));
										$arvtime1 = date("H:i:s" , strtotime($arvdate1[1]));
										$arvnewdate1= date("Y-m-d" , strtotime($arvdate1[0]));
										 $arvnewdatenew= date("l d M Y " , strtotime($arvdate1[0]));
										$devdate1 = explode('T',$segment_return[$seg_i]['Origin']['DepTime']);
										$depnewdatedetail= date("l d M Y" , strtotime($devdate1[0]));
										$depnewtimedetail = date("H:s" , strtotime($devdate1[1]));
										$depdatetime = date("H:i:s" , strtotime($devdate1[1]));
										$depdatedistance= date("Y-m-d" , strtotime($devdate1[0]));
										$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $depdatedistance.''. $depdatetime);
										$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $arvnewdate1.''. $arvtime1);
										$totalDuration = $from->diffInSeconds($to);
										$durationhour =  gmdate('H', $totalDuration);
										$durationmin =  gmdate('s', $totalDuration);
										$newtime_return=$durationhour.'H : '.$durationmin.'M';
				?>
					<div class="invoice-flight-details">
						<div class="row">
							<div class="col-md-5">
								<div class="flight-and-time">
									<div class="city-time" style="font-size:15px "><strong>{{$segment_return[$seg_i]['Origin']['Airport']['AirportCode']}}</strong> {{$depnewtimedetail}}</div>
									<div class="day" style="font-size:15px ">{{$depnewdatedetail}}</div>
									<div class="airport" style="font-size:15px ">{{$segment_return[$seg_i]['Origin']['Airport']['CityCode']}}, {{$segment_return[$seg_i]['Origin']['Airport']['AirportName']}}, Terminal {{$segment_return[$seg_i]['Origin']['Airport']['Terminal']}}</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="invoice-flight-duration">
									<div class="clock-icon"><img src="{{ asset('assets/images/clock-img.png')}}" class="img-responsive clock"> </div>
									<div class="hours" style="font-size:15px ">{{$newtime_return}}</div>
									<div class="invoice-flight-class" style="font-size:15px ">{{$flightbook_return->flightcabinclass}}</div>
								</div>
							</div>
							<div class="col-md-5">
								<div class="flight-and-time text-right">
									<div class="city-time" style="font-size:15px "><strong>{{$segment_return[$seg_i]['Destination']['Airport']['AirportCode']}}</strong>{{$arvtime2}}</div>
									<div class="day" style="font-size:15px ">{{$arvnewdatenew}}</div>
									<div class="airport" style="font-size:15px ">{{$segment_return[$seg_i]['Destination']['Airport']['CityCode']}}, {{$segment_return[$seg_i]['Destination']['Airport']['AirportName']}}, Terminal {{$segment_return[$seg_i]['Destination']['Airport']['Terminal']}}</div>
								</div>
							</div>
						</div>
					</div>
					<?php }?>
						<div class="traveller-details">
							<div class="row">
								<div class="col-md-4">
									<div class="traveller">Traveller</div>
								</div>
								<div class="col-md-4">
									<div class="ticket-id">Ticket ID</div>
								</div>
								<div class="col-md-4">
									<div class="ticket-no">Ticket No.</div>
								</div>
							</div>
							<?php
					$passengers_return = unserialize($flightbook_return->passenger_detail);

					for($pass=0;$pass< count($passengers_return);$pass++)
					{
						if($passengers_return[$pass]['PaxType']=='1')
						{
							$passcheck='Adults';
						}
						else if($passengers_return[$pass]['PaxType']=='2')
						{
							$passcheck='Child';
						}
						else if($passengers_return[$pass]['PaxType']=='3')
						{
							$passcheck='Infant';
						}
						else
						{
							$passcheck='';
						}
					?>
								<div class="row">
									<div class="col-md-4">
										<div class="traveller-name">{{$passengers_return[$pass]['Title']}}. {{ucfirst($passengers_return[$pass]['FirstName'])}} {{ucfirst($passengers_return[$pass]['LastName'])}} ({{$passcheck}})</div>
									</div>
									@if(!empty($passengers_return[$pass]['Ticket']['TicketId']))
		                            <div class="col-md-4">
		                                <div class="traveller-ticket-id">{{$passengers_return[$pass]['Ticket']['TicketId']}}</div>
		                            </div>
		                            <div class="col-md-4">
		                                <div class="traveller-ticket-id">{{$passengers_return[$pass]['Ticket']['TicketNumber']}}</div>
		                            </div>
		                            @else
		                            <div class="col-md-4">
		                                <div class="traveller-ticket-id">{{$flightbook_return->pnrno}}</div>
		                            </div>
									@endif
								</div>
								<?php } ?>
						</div>
						<div class="invoice-important">
							<div class="row">
								<div class="col-md-12">
									<div class="heading">Important</div>
									<ul>
										<li>Please carry your Government ID proof for all passengers to show during security check and check-in. Name on Government ID proof should be same as on your ticket.</li>
										<li>We recommended you to reach airport 2 hrs before departure time. Airline check-in counters typically close 1hr prior to departure time.</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="baggage-policy">
							<div class="row">
								<div class="col-md-12">
									<div class="heading">Baggage Policy</div>
									<table class="table table-responsive table-bordered">
										<tr>
											<th>Check-In(Adult & Child)</th>
											<td>As per Airline Policy</td>
										</tr>
										<tr>
											<th>Hand-Baggage(Adult & Child)</th>
											<td>7 KG / person</td>
										</tr>
										<tr>
											<th>Terms & Conditions</th>
											<td>
												<p>Please check with the airline on the dimensions of the baggage</p>
												<p>The baggage policy is only indicative and can change any time. You are advised to check with the airline before travel to know latest baggage policy</p>
												<p>You are advised to check with the airline for extra baggage charges</p>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div class="cancellation-policy">
							<div class="row">
								<div class="col-md-12">
									<div class="heading">Cancellation Policy</div>
									<table class="table table-responsive table-bordered">
										<tr>
											<th>Airline Cancellation Charges</th>
											<td>As per airline policy.Paytm doesnt charge any additional cancellation processing fee.</td>
										</tr>
										<tr>
											<th>Direct cancellation with airline</th>
											<td>Post cancellation with airline, please contact Paytm customer care for refund</td>
										</tr>
										<tr>
											<th>Booking modifications</th>
											<td>Date/Flight change is allowed upto 24 Hrs before departure as per airline policy. Contact our Paytm flights customer care - 0120 4880880 for modifications. Most airlines dont allow name amendments.</td>
										</tr>
										<tr>
											<th>Terms & Conditions</th>
											<td>
												<p>We accept cancellations, only before 24 Hrs from departure time</p>
												<p>For cancellations, within 24 hours before departure you need to contact the airline. Post cancellation by airline, you can contact Paytm for refund, if any</p>
												<p>Convenience fee is non-refundable. Any cashback availed will be adjusted in final refund amount</p>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<?php
				$flightfare_return = unserialize($flightbook_return->fare);

				if($flightbook_return->mealprice !='' || $flightbook_return->mealprice =='0')
				{
					 $mealprice_return = $flightbook_return->mealprice;
				}
				else
				{
					 $mealprice_return=0;
				}
				if($flightbook_return->bag_price !='' || $flightbook_return->bag_price =='0')
				{
					$baggprice_return = $flightbook_return->bag_price;
					// $baggprice_return = $flightfare_return['TotalBaggageCharges'];
				}
				else
				{
					$baggprice_return=0;
				}
				$othercharges_return=$flightbook_return->flightothercharges;
				// $othercharges_return = round($flightfare_return['OtherCharges'] +  $flightfare_return['TdsOnPLB'] + $flightfare_return['TdsOnIncentive']+$flightfare_return['TdsOnCommission']);
				$basefare_return =$flightbook_return->flightmarginprice;
                $taxfare_return =$flightbook_return->flighttax;

				$totalamount_return =  $basefare_return +  $taxfare_return + $othercharges_return + $mealprice_return + $baggprice_return ;
				if($flightbook_return->currency_icon=='fa-inr')
                {
                	
                	 $imgicon = "Rs.";

                }
                else
                {
                	$imgicon="$";
                }
				?>
				<div class="fare-payment-policy">
					<div class="row">
						<div class="col-md-12">
							<div class="heading">Fare & Payment Details</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<div class="fare-left">Base Fare</div>
						</div>
						<div class="col-md-4">
							<div class="fare-right">{{$imgicon}} {{number_format($basefare_return,2)}}</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<div class="fare-left">Tax</div>
						</div>
						<div class="col-md-4">
							<div class="fare-right">{{$imgicon}} {{number_format($taxfare_return,2)}}</div>
						</div>
					</div>
					@if($baggprice_return !='0')
					<div class="row">
						<div class="col-md-8">
							<div class="fare-left">Extra Baggage Charges ({{$flightbook_return->bag_weight}} Kg)</div>
						</div>
						<div class="col-md-4">
							<div class="fare-right">{{$imgicon}} {{number_format($baggprice_return,2)}}</div>
						</div>
					</div>
					@endif @if($mealprice_return!='0')
					<div class="row">
						<div class="col-md-8">
							<div class="fare-left">Meal Charges ({{$flightbook_return->mealname}})</div>
						</div>
						<div class="col-md-4">
							<div class="fare-right">{{$imgicon}} {{number_format($mealprice_return,2)}}</div>
						</div>
					</div>
					@endif
					<div class="row">
						<div class="col-md-8">
							<div class="fare-left ">Other Charges</div>
						</div>
						<div class="col-md-4">
							<div class="fare-right">{{$imgicon}} {{number_format($othercharges_return,2)}}</div>
						</div>
					</div>

					<div class="total-fare">
						<div class="row">
							<div class="col-md-8">
								<div class="fare-left">Total Fare</div>
							</div>
							<div class="col-md-4">
								<div class="fare-right"><img src="{{ asset('assets/images/rupee.png')}}" class="img-responsive rupee"> {{number_format($totalamount_return,2)}}</div>
							</div>
						</div>
					</div>
				</div>

				<div class="pdf-footer">
					<hr style="width: 50%;border: none;border-bottom: 1px solid #5e5e5e; margin: auto">
					<div class="row">

						<div class="col-md-12" style="width:100%; margin:0 auto;font-size: 13px;text-align: center">

							<img src="{{asset('assets/images/logo.png')}}" style="width: 120px;margin-top: 10px" class="img-fluid d-block mx-auto">
							
							<p style="margin: 5px!important; color: black;"><b> Ph</b> +61 490149833</p>
							@if($flightbook_return->currency_icon=='fa-inr')
							<p style="margin: 5px!important; color: black;"><b> GST No.</b> 03BDHPK8214M1ZG</p>
							@endif
							<p style="margin: 5px!important;"><span> Email</span>  <a href="#">info@travoweb.com</a>, <span><i class="fa fa-globe"></i> Website:</span>Travoweb.com</p>

						</div>
					</div>
				</div>
				<hr>
			</div>
		</div>
	</div>
	<?php } ?>
</body>
</html>