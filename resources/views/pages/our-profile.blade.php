@include('pages.include.header')

<body>

    <div class="super_container">

        <!-- Header -->

        @include('pages.include.topheader')

        <div class="home">

            <div class="home_background parallax-window" data-parallax="scroll"
                data-image-src="{{ asset('assets/images/about_background.jpg') }}"></div>

            <div class="home_content">

                <div class="home_title">Our Profile</div>

            </div>

        </div>

        <!-- Intro -->

        <div class="intro">

            <div class="container">

                <div class="row">

                    <div class="col-lg-12">

                        <div class="intro_content nano-content">

                            <div class="intro_title" style="margin-top:100px">Our Profile</div>

                            <h3>A Quick Glance</h3>

                            <p>Travoweb is not a conventional travel agency; it is a different Class of its own
                                creation.
                                Seamlessly combining Consulting, Concierge Services, Event Management and customized
                                travel packages, we transcend far beyond a simple travel service.
                                Instead we offer travel solutions, expertly tailored to suit the corporate world or
                                personalized exquisitely to individual taste.
                                Thorough research and cutting edge technology are blended with a highly personalized
                                service to offer an infinite spectrum of unique and innovative travel ideas.
                                We seek to offer you the best while we open the gates to new destinations and
                                experiences. Our promise is to push the boundaries of your journeys.</p>






                            <h3>Our Philosophy</h3>



                            <p>Our measure of success is expanding horizons and exceeding expectations to a point of
                                surprise.

                                At the core is an unwavering belief in individualism. We place the utmost importance on
                                personal choices, preferences and tastes.


                                Today’s world is one of globalization, mass production and bland uniformity. We instead
                                aspire to break away from all things common, ordinary and uninspiring, by nurturing
                                inimitable individual distinctiveness.

                                Our measure of success is expanding horizons and exceeding expectations to a point of
                                surprise.

                                We make our business personal, exactly the way that travel and hospitality were intended
                                to be before they became detached industries. We develop long term relationships in
                                place of cordial financial transactions, refining and redefining the art of hospitality
                                along the way.</p>





                            <h3>Where We Meet</h3>



                            <p>Being in a class of our means that we offer our Guests something extraordinary different
                                from the first time we meet.
                                We believe that complete privacy is essential in creating exceptionally tailored travel
                                options. And so we have done away with the standard and impersonal counters, replacing
                                them with intimate lounges.

                                We meet each Guest one-on-one, ensuring the utmost attention to individual tastes and
                                needs.

                                No two lounges are alike; each has its own distinct lay-out and ambiance, meaning that
                                we cater to the moods and tastes of our Guests down to the rooms that we meet them in.
                            </p>



                            <h3>Our Consultants</h3>



                            <p>G-family believes in establishing a partnership with the members of its team. We do not
                                hire employees, but instead cultivate consultants.

                                Our staff is carefully selected and continuously trained in all the aspects that we
                                touch upon – from interpersonal communication and client care to research and product
                                development.

                                Reflecting the spirit of international travel and cross-cultural exploration, we have
                                created a truly multinational company. Our consultants hail from all sides of the globe,
                                further adding insight and value as we help you explore different destinations and
                                cultures.
                            </p>



                            
                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!-- Footer -->

        @include('pages.include.footer')

        <!-- Copyright -->

        @include('pages.include.copyright')

    </div>

</body>

</html>