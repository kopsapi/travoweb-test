@include('pages.include.header')
<style>
	@import url("https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/blitzer/jquery-ui.min.css");

	.ui-datepicker td span,
	.ui-datepicker td a {
		padding-bottom: 1em;
	}

	span.m-close {
		position: absolute;
		font-size: 27px;
		font-weight: 700;
		top: 0;
		color: white;
		right: 19px;
		cursor: pointer;
	}

	.ui-datepicker td[title]::after {
		content: attr(title);
		display: block;
		position: relative;
		font-size: .8em;
		height: 1.25em;
		margin-top: -1.25em;
		text-align: right;
		padding-right: .25em;
	}

	#ui-datepicker-div {
		z-index: 12 !important;
	}

	.table-condensed>thead>tr>th,
	.table-condensed>thead>tr>td {
		padding: 5px;
	}

	.dropdown-menu {
		position: absolute;
		top: 100%;
		left: 0;
		z-index: 1000;
		display: none;
		float: left;
		min-width: 160px;
		margin: 2px 0 0;
		font-size: 14px;
		text-align: left;
		list-style: none;
		background-color: #fff;
		-webkit-background-clip: padding-box;
		background-clip: padding-box;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, .15);
		border-radius: 4px;
		-webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
		box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
	}

	.dropdown-menu {
		box-shadow: none;
		border-color: #eee;
	}

	.search_panel {
		padding-top: 50px;
		padding-bottom: 50px;
	}

	.search_panel_content {
		flex-wrap: wrap;
	}

	.extras {
		width: 100%;
		margin-bottom: 10px;
	}

	.search_extras_item {
		width: 50%;
		float: left;
		margin-bottom: 10px;
	}

	.search_extras_item div {
		display: inline-block;
		cursor: pointer;
	}

	.search_extras_cb {
		display: block;
		position: relative;
		width: 15px;
		height: 15px;
		-webkit-appearance: none;
		-moz-appearance: none;
		-ms-appearance: none;
		-o-appearance: none;
		appearance: none;
		background-color: #FFFFFF;
		border: 1px solid #FFFFFF;
		padding: 9px;
		margin-top: 4px;
		border-radius: 50%;
		display: inline-block;
		position: relative;
		cursor: pointer;
		float: left;
	}

	.search_extras_cb:checked::after {
		display: block;
		position: absolute;
		top: 2px;
		left: 2px;
		border-radius: 50%;
		width: calc(100% - 4px);
		height: calc(100% - 4px);
		content: '';
		background: #fa9e1b;
	}

	.flight-details-tabs .nav>li>a {
		position: relative;
		display: block;
		padding: 10px 15px;
		border: 1px solid #FF9800 !important;
		border-top-left-radius: 6px !important;
		border-top-right-radius: 25px !important;
	}

	.flight-details-tabs .nav-tabs>li>a.active,
	.nav-tabs>li>a.active:focus,
	.nav-tabs>li>a.active:hover {
		color: #FFF;
		cursor: default;
		background-color: #fa9e1b;
		border: 1px solid #FF9800 !important;
		border-radius: 0 !important;
		padding: 10px 15px !important;
		border-top-left-radius: 6px !important;
		border-top-right-radius: 25px !important;
	}

	.search_extras label {
		display: block;
		position: relative;
		font-size: 15px;
		font-weight: 400;
		padding-left: 25px;
		margin-bottom: 0px;
		cursor: pointer;
		color: #FFFFFF;
	}

	@media only screen and (max-width: 1730px) {
		.search_extras_item {
			width: 20%;
		}
	}

	@media only screen and (max-width: 1730px) {
		.search_panel {
			display: none !important;
			width: 100%;
			height: 100%;
			-webkit-animation: fadeEffect 1s;
			animation: fadeEffect 1s;
			margin-top: 0px;
		}
	}

	@media only screen and (max-width: 350px) {
		.d-div {
			display: none;
		}
	}
	
</style>
<style>
	.flight-book-btn {
		cursor: pointer;
	}

	.dropbtn {
		background-color: #3498DB;
		color: white;
		padding: 16px;
		font-size: 16px;
		border: none;
		cursor: pointer;
	}

	.dropbtn:hover,
	.dropbtn:focus {
		background-color: #2980B9;
	}

	.dropdown {
		position: relative;
		display: inline-block;
	}

	.dropdown-content {
		display: none;
		position: absolute;
		background-color: #f1f1f1;
		min-width: 160px;
		overflow: auto;
		box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
		z-index: 1;
	}

	.dropdown-content a {
		color: black;
		padding: 12px 16px;
		text-decoration: none;
		display: block;
	}

	.exchange-icon {
		font-size: 17px;
		color: #fa9e1b;
		padding-top: 0px !important;
		margin-top: -13px !important;
	}

	.cheap-flight .flight-list {
		margin-bottom: 0px;
		border-left: 0;
		padding-bottom: 52px !important;
		border-right: 0;
		background: #FEE1BE;
		border-bottom: 2px solid #fa9e1b;
	}

	@media screen and (max-width: 450px) {
		.cheap-flight .flight-list {

			padding-bottom: 83px !important;

		}

	}

	@media screen and (max-width: 500px) {
		.cheap-btn {
			padding: 11px 10px !important;
			font-size: 11px !important;
		}

		.bar-heading h5 {
			font-size: 12px !important;
		}
	}

	a.active {
		border-radius: 20px !important;
		padding: 9px 14px !important;
	}

	a.a-done-btn {
		text-decoration: none;
		padding: 10px;
		background: #fa9e1b;
		color: white;
		margin: 20px auto auto auto;
		width: 120px;
		display: block;
		text-align: center;
		border-radius: 20px;
	}

	.a-done-btn {
		text-decoration: none;
		padding: 10px;
		background: #fa9e1b;
		color: white;
		margin: 20px auto auto auto;
		width: 120px;
		display: block;
		text-align: center;
		border-radius: 20px;
		cursor: pointer;
	}

	.dropdown a:hover {
		background-color: #ddd;
	}

	.show {
		display: block;
	}

	@media screen and (max-width: 500px) {

		.flight-list .flight-book .flight-book-btn {
			padding: 4px 6px;
			font-size: 11px;

		}

		.flight-name p {
			font-size: 10px !important;
		}

		.flight-list .flight-book .details {

			font-size: 11px;
		}

		.flight-list .flight-price .flight-price-heading {
			font-size: 11px;
		}

		.flight-list p {

			font-size: 11px !important;
		}

		.flight-list .flight-fare .fare-type {
			font-size: 11px;
			font-weight: 600;
			text-align: right;
		}

		.flight-list .flight-fare .fare-rules {
			color: #00206A;
			font-size: 11px;
			font-weight: 600;
			text-align: left !important;
		}

		.flight-list .flight-fare .fare-type {
			font-size: 11px;
			font-weight: 600;
			text-align: right;
		}

		h5.fare-rules {
			text-align: left !important;
		}
	}

	h5.fare-type.refundable {
		text-align: right !important;
		padding-right: 10px;
	}

	h5.fare-rules {
		text-align: left !important;
		padding-left: 10px;
	}
</style>
<style>
	a.exchange-icon {
		/* margin-top: 19px; */
		padding-top: 28px !important;
	}

	.mbsc-mobiscroll .mbsc-stepper-cont {
		padding: 1.75em 12.875em 1.75em 1em;
	}

	.mbsc-control-w {
		max-width: none;
		margin: 0;
		font-size: 1em;
		font-weight: normal;
	}

	.mbsc-checkbox,
	.mbsc-switch,
	.mbsc-btn,
	.mbsc-radio,
	.mbsc-segmented,
	.mbsc-stepper-cont {
		position: relative;
		display: block;
		margin: 0;
		z-index: 0;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}

	.mbsc-desc {
		display: block;
		font-size: .75em;
		opacity: .6;
	}

	.mbsc-stepper {
		position: absolute;
		display: block;
		width: auto;
		right: 1em;
		top: 50%;
	}

	.mbsc-form .mbsc-stepper-val-left .mbsc-stepper input {
		left: 0;
	}

	.mbsc-mobiscroll .mbsc-stepper input {
		color: #454545;
	}

	.mbsc-stepper input {
		position: absolute;
		left: 4.142857em;
		width: 4.142857em;
		height: 100%;
		padding: 0;
		margin: 0;
		border: 0;
		outline: 0;
		box-shadow: none;
		font-size: .875em;
		text-align: center;
		opacity: 1;
		z-index: 4;
		background: transparent;
		-webkit-appearance: none;
		-moz-appearance: textfield;
		appearance: none;
	}

	.mbsc-stepper .mbsc-segmented-item {
		width: 3.625em;
	}

	.mbsc-segmented .mbsc-segmented-item {
		margin: 0;
		display: table-cell;
		position: relative;
		vertical-align: top;
		text-align: center;
		font-size: 1em;
	}

	.mbsc-stepper-cont.mbsc-stepper-val-left .mbsc-stepper .mbsc-segmented-item:nth-child(2) .mbsc-segmented-content,
	.mbsc-stepper-cont.mbsc-stepper-val-right .mbsc-stepper .mbsc-segmented-item:last-child .mbsc-segmented-content {
		border: 0;
		background: transparent;
	}

	.mbsc-mobiscroll .mbsc-segmented-content {
		border: 0.142857em solid #4eccc4;
		color: #4eccc4;
		height: 2.28571428em;
		margin: 0 -.071428em;
		line-height: 2.28575em;
		padding: 0 .285714em;
		text-transform: uppercase;
	}

	.mbsc-segmented-content {
		position: relative;
		display: block;
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
		font-size: .875em;
		font-weight: normal;
		z-index: 2;
	}

	.mbsc-segmented .mbsc-segmented-item {
		margin: 0;
		display: table-cell;
		position: relative;
		vertical-align: top;
		text-align: center;
		font-size: 1em;
	}

	.mbsc-mobiscroll .mbsc-segmented input:disabled~.mbsc-segmented-item .mbsc-segmented-content,
	.mbsc-mobiscroll .mbsc-segmented .mbsc-segmented-item.mbsc-stepper-control.mbsc-disabled .mbsc-segmented-content,
	.mbsc-mobiscroll .mbsc-segmented .mbsc-segmented-item input:disabled+.mbsc-segmented-content {
		color: #d6d6d6;
		border-color: #d6d6d6;
	}

	.mbsc-mobiscroll .mbsc-segmented input:disabled~.mbsc-segmented-item .mbsc-segmented-content,
	.mbsc-mobiscroll .mbsc-segmented .mbsc-segmented-item.mbsc-stepper-control.mbsc-disabled .mbsc-segmented-content,
	.mbsc-mobiscroll .mbsc-segmented .mbsc-segmented-item input:disabled+.mbsc-segmented-content {
		background: transparent;
	}

	.mbsc-mobiscroll .mbsc-segmented-content {
		border: 0.142857em solid #4eccc4;
		color: #4eccc4;
		height: 2.28571428em;
		margin: 0 -.071428em;
		line-height: 2.28575em;
		padding: 0 .285714em;
		text-transform: uppercase;
	}

	.mbsc-mobiscroll .mbsc-segmented-content {
		border: 0.142857em solid #4eccc4;
		color: #4eccc4;
	}

	.mbsc-mobiscroll .mbsc-segmented-content {
		height: 2.28571428em;
		margin: 0 -.071428em;
		line-height: 2.28575em;
		padding: 0 .285714em;
		text-transform: uppercase;
	}

	.mbsc-disabled .mbsc-segmented-content,
	.mbsc-segmented input:disabled,
	.mbsc-segmented input:disabled~.mbsc-segmented-item .mbsc-segmented-content {
		cursor: not-allowed;
	}

	.mbsc-segmented-item .mbsc-control,
	.mbsc-stepper .mbsc-segmented-content {
		cursor: pointer;
	}

	.mbsc-segmented input:disabled~.mbsc-segmented-item .mbsc-segmented-content,
	.mbsc-disabled .mbsc-segmented-content,
	.mbsc-segmented input:disabled+.mbsc-segmented-content {
		z-index: 0;
	}

	.traveller-dropdown .dropdown-menu hr {
		margin-top: 7px !important;
		margin-bottom: 7px !important;
	}

	.traveller-dropdown .dropdown-menu {
		min-width: 300px !important;
		margin-bottom: 0px;
		background: #FFF4e7;
		border: 1px solid #CCC;
		margin-top: 10px;
	}

	.traveller-dropdown .dropdown-menu .row {
		margin-bottom: 0px;
	}

	.traveller-dropdown .dropdown-menu .col-md-12 {
		margin-bottom: 0px;
	}

	.traveller-dropdown .dropdown-menu .trave-drop {
		padding-left: 15px;
		padding-right: 15px;
		margin-bottom: 0px;
	}

	.traveller-dropdown .dropdown-menu .trave-drop .travel-left {
		width: 60%;
		float: left;
		color: #00206A;
		font-weight: 600;
		margin-bottom: 0px;
		font-size: 16px;
		line-height: 36px;
	}

	.traveller-dropdown .dropdown-menu .trave-drop .plus-minus .minus {
		color: #fa9e1b;
		width: 35%;
		float: left;
		font-size: 24px;
		margin-bottom: 0px;
		text-align: center;
	}

	.traveller-dropdown .dropdown-menu .trave-drop .plus-minus .text {
		color: #00206A;
		width: 30%;
		float: left;
		font-size: 17px;
		line-height: 37px;
		font-weight: 600;
		margin-bottom: 0px;
		text-align: center;
	}

	.traveller-dropdown .dropdown-menu .trave-drop .plus-minus .plus {
		color: #fa9e1b;
		width: 35%;
		float: left;
		font-size: 24px;
		margin-bottom: 0px;
		text-align: center;
	}

	.traveller-dropdown .dropdown-menu .trave-drop .child-age label {
		color: #333;
		margin-bottom: 3px;
		font-weight: 600;
	}

	.traveller-dropdown .dropdown-menu .trave-drop .child-age select {
		border: 1px solid #CCC;
		padding-left: 10px;
	}

	.traveller-dropdown .dropdown-menu .trave-drop .child-age:nth-child(1),
	.traveller-dropdown .dropdown-menu .trave-drop .child-age:nth-child(4),
	.traveller-dropdown .dropdown-menu .trave-drop .child-age:nth-child(7),
	.traveller-dropdown .dropdown-menu .trave-drop .child-age:nth-child(10) {
		margin-left: 0px;
	}

	.traveller-dropdown .dropdown-menu .trave-drop .child-age {
		width: 30%;
		float: left;
		margin-top: 10px;
		margin-left: 13px;
	}

	.traveller-dropdown .dropdown-menu .trave-drop .plus-minus {
		width: 40%;
		float: left;
		margin-bottom: 0px;
	}

	.t-err {
		color: red;
		visibility: hidden;
	}
	.flight-book {
    padding-right: 5px !important;
}
</style>

<body>
	<style>
		.box-hover {
			background-color: black;
			position: absolute;
			width: 290px;
			height: 185px;
			margin: 5px 5px 0 5px;
			display: none;
			opacity: 0.7;
		}

		@media screen and (max-width: 350px) {


			.flight-list .flight-book {
				padding: 3px !important;
				font-size: 11px !important;

			}
		}

		@media screen and (max-width: 650px) {
			.showtable_returninter div .flight-list .row .col-md-3 {
				padding: 0;
			}

			.showtable_returninter div .flight-list .flight-fare .row .col-md-3 {
				padding: 10px;
			}
			.round-partition .pd-r-5 {
    padding-right: 5px;
    padding-left: 5px;
}

		}

		.showtable_returninter p.flight-price-heading {
			font-size: 15px !important;
			font-weight: 700;
			color: #00206a;
		}

		.cheap-flight .flight-list {
			margin-bottom: 0px;
			border-left: 0;
			padding-bottom: 15px !important;
			border-right: 0;
			background: #FEE1BE;
			border-bottom: 2px solid #fa9e1b;
		}
		.flight-price {
    padding-right: 5px;
}
.showtable_returninter p.flight-price-heading i {
    color: #fa9e1b;
}
 .title-bar .col-3{
	padding-left: 3px;
    padding-right: 3px;
 }
 .flight-fare .row .col-3, .flight-fare .row .col-5, .flight-fare .row .col-md-4 {
    padding: 3px;
}
label.checkcontainer i {
    font-size: 14px;
    margin-right: 3px;
}
.checkcontainer {
    display: block;
    position: relative;
    padding-left: 30px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 16px;
    font-weight: 600;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    color: #00206A;
}
.filter.hotel-card-filter .inner-filter {
    max-height: 335px !important;
    overflow: auto;
}

.filter.hotel-card-filter .inner-filter::-webkit-scrollbar {
  width: 10px;
}

/* Track */
.filter.hotel-card-filter .inner-filter::-webkit-scrollbar-track {
  background: #FFF4e7; 
}
 
/* Handle */
.filter.hotel-card-filter .inner-filter::-webkit-scrollbar-thumb {
	margin-top: 37px;
	padding-top: 37px !important;
  background: #fa9e1b; 
}

/* Handle on hover */
.filter.hotel-card-filter .inner-filter::-webkit-scrollbar-thumb:hover {
  background: #ff8d00; 
}
.filter.hotel-card-filter .inner-filter div label
{
	font-size: 13px;
}
.filter.hotel-card-filter .inner-filter div label .checkcontainer .checkmark:after
{
	    left: 4px;
    top: 1px;
    width: 6px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
.filter.hotel-card-filter .inner-filter div label .checkmark
{
	position: absolute;
    top: 3px;
    left: 0;
    height: 15px;
    width: 15px;
    background-color: #FFF;
    border: 1px solid #CCC;
    border-radius: 3px;
}
	span.m-close {
		position: absolute;
		font-size: 27px;
		font-weight: 700;
		top: 0;
		color: white;
		right: 19px;
		cursor: pointer;
	}
    .price i.fa-usd {

        font-size: 25px !important;
        margin-bottom: 2px;

	}
	button.btn.btn-primary.filter-btn {
    background: orange;
    border: none;
    position: sticky;
    bottom: 80px;
    width: auto;
    height: auto;
    z-index: content: '\f095';
    width: auto;
    display: none;
	margin: 0 0 0 auto;
	transition: all .5s ease;
    width: auto;
    border-radius: 50px;
}
.filter-res-div {
	width: 100%;
	transition: all .5s ease;
    height: auto;
    background:none;
    z-index: 20;
	padding: 10px;
	right: 10px;
    margin: 0 !important;
}
.desktop-filter-btn {
    margin-right: 114px !important;
    width: 256px !important;
    border-radius: 5px !important;
}
@media screen and (max-width:992px){
	button.btn.btn-primary.filter-btn {
   
	display: block;
}

}
.modal .flight-filter h3 {
    color: #ffffff;
    background: orange;
    padding: 5px 10px;
    border-radius: 5px;
    font-size: 16px;
    margin-bottom: 20px;
}
.filter h4 {
    background: #18206a;
    color: white;
    padding: 5px 10px;
}
.filter h4 i {
    color: orange;
}
.scroll-div::-webkit-scrollbar {
    width: 10px;
}
.scroll-div::-webkit-scrollbar-thumb {
    margin-top: 37px;
    padding-top: 37px !important;
    background: #fa9e1b;
}
.scroll-div::-webkit-scrollbar-track {
    background: #FFF4e7;
}
.done-btn {
    margin: auto;
    display: block;
    border-radius: 50px;
    background: #18206a;
    width: 140px;
    border: none;
}
.loc {
    font-family: calibri;
    font-size: 12px;
}
.hotel-title {
    font-family: calibri;
	margin-bottom: 0;
}
.rating-div i {
    color: #fa9e1b;
}
h4.price {
    font-family: calibri;
}
.hotel-result-list .hotel-result-price .price i {
    color: #fa9e1b;
    font-size: 24px;
}
.hotel-result-list .hotel-result-price {
    text-align: right;
    height: 100%;
    border-left: 2px solid #CCC;
    background: #FFF4e7;
    padding: 30px 15px;
}
.g-details {
    margin-bottom: 10px;
}
.hotel-result-list {
    border-radius: 10px;
}
.flight-filter ,.hotel-result-details .flight-filter h3,.hotel-result-details .flight-filter .filter h4{
    font-family: calibri;
    /* font-size: 19px !important; */
}
.checkcontainer {
    display: block;
    position: relative;
    padding-left: 30px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 17px;
    font-weight: 600;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    color: #00206A;
}
label.checkcontainer i {
    font-size: 14px;
}
.inner-filter {
    z-index: 0;
}
p.tooltip {
    z-index: 0;
}
@media screen and (max-width: 600px) {
	.search-breadcrumb h4.flight-class {
		font-size: 14px;
	}
	.search-breadcrumb .plane {
		float: right;
		color: #fa9e1b;
		font-size: 29px;
	}
	.home {
			height: 13vh !important;
		}
		.intro_content {
    margin-top: 0px;
}
.search-breadcrumb h4 {
    color: #FFF;
    font-size: 15px;
}
}
	</style>
		<div class="modal" id="myModal">
			<div class="modal-dialog">
			  <div class="modal-content" style="margin-top: 5%;">
		  
				<!-- Modal Header -->
				<div class="modal-header">
				  <h4 class="modal-title" style="    color: orange;">Filters</h4>
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
		  
				<!-- Modal body -->
				<div class="modal-body scroll-div" style="height: 470px;
				overflow-y: auto;">
				  <div class="filter-res"></div>
				  <button type="button" class="btn btn-danger done-btn" data-dismiss="modal">Done</button>
				</div>
		  
				<!-- Modal footer -->
				
		  
			  </div>
			</div>
		  </div>
		
	<div class="super_container">
		<!-- Modal -->
		<div class="modal fade modify-flight" id="myModal" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Modify Search</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Header -->
		@include('pages.include.topheader')
		<div class="home" style="height:20vh;">
			<!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->
			<div class="home_content">
				<div class="home_title">Results </div>
			</div>
		</div>
		<!-- Intro -->
		<div class="result">
			<div class="intro">


				<?php

			// echo "<pre>";
			// print_r($hotelarray);
			// echo "</pre>";


			// foreach($resu[0] as $arrays1)
			// {

			// 	echo "<pre>";
			// print_r($arrays1['flight_fare']);
			// echo "</pre>";
			// }
			$data_string;
			// echo date("d M Y l" ,$mainarray['depdate']);

			if(!empty($array)){
			}else
			{
				"No data found";
			}
			?>

				@if(!empty($resu))
				<section class="search-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="intro_content">
									<div class="row">
										<div class="col-lg-3 col-6 res-mb-20">
											<input type="hidden" value="<?php echo $mainarray['JourneyType'];?>"
												id="journytype">
											<input type="hidden" value="{{$mainarray['FlightCabinName']}}"
												id="FlightCabinName">


											<h4>{{$mainarray['flightorign']}} <i
													class="fa fa-plane plane fa-rotate-45"></i></h4>
											<h5><small>{{$mainarray['airportorigncityname']}}
													({{$mainarray['airportorignname']}})</small></h5>
										</div>
										<div class="col-lg-3 col-6 res-mb-20">
											<h4>{{$mainarray['flightdep']}}</h4>
											<h5><small>{{$mainarray['airportdeptcityname']}}
													({{$mainarray['airportdeptname']}})</small></h5>
										</div>
										<div class="col-lg-4 col-6 res-mb-20">
											<h4 class="flight-class"><i class="fa fa-calendar"></i>&nbsp;
												{{date("d M Y l", strtotime($mainarray['depdate']))}}</h4>

											<?php
											foreach($array as $arrays1)
											{
												$child='';
												$Infant='';
												$adultscount =0;
												$childcount=0;
												$infantcount=0;
												for($fare_i=0;$fare_i<count($arrays1['FareBreakdown']);$fare_i++)
												{
													if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='1')
													{
														$adultscount = $arrays1['FareBreakdown'][$fare_i]['PassengerCount'];
														$adults='<small>ADULTS</small>'.$arrays1['FareBreakdown'][$fare_i]['PassengerCount'];
													}
													if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='2' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
													{
														$childcount=$arrays1['FareBreakdown'][$fare_i]['PassengerCount'];
														$child=  '<small>CHILD</small>'.$arrays1['FareBreakdown'][$fare_i]['PassengerCount'];
													}
													if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='3' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
													{
														$infantcount =$arrays1['FareBreakdown'][$fare_i]['PassengerCount'];
														$Infant=  ' <small>Infant</small>'.$arrays1['FareBreakdown'][$fare_i]['PassengerCount'];
													}
												}
											}
											$totaltr=$adultscount+$childcount+$infantcount;
											?>
											<h4 style="margin-bottom:0px;" class="flight-class">
												<?php echo $adults.' '.$child.''.$Infant  ?> <small>Class</small>
												{{$mainarray['FlightCabinName']}}

											</h4>
										</div>
										<div class="col-lg-2 col-6">
											<button type="button" class="btn btn-modify">Modify</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="result-modify">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="search">
									<!-- Search Contents -->
									<div class="fill_height">
										<!-- Search Panel -->
										<div class="search_panel active">
											<span class="m-close closemodal" onclick="close1()">&times;</span>
											<form action="{{route('flightsearch')}}" method="post" id="search_form_2"
												class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start flights">
												<?php

												if($mainarray['JourneyType']=='1')
												{
													$dischk = 'checked';
													$dischk1='';
													$dischk2='';
												}
												else if($mainarray['JourneyType']=='2')
												{
													$dischk1 = 'checked';
													$dischk='';
													$dischk2='';

												}
												else if($mainarray['JourneyType']=='3')
												{
													$dischk2 = 'checked';
													$dischk1='';
													$dischk='';
												}
												else
												{
													$dischk='';
													$dischk1='';
													$dischk2='';
												}

												?>
												<div class="extras">
													<ul class="search_extras clearfix">
														<li class="search_extras_item">
															<div class="clearfix">
																<input type="radio" id="search_extras_1" name="one"
																	value="1" class="search_extras_cb"
																	<?php echo $dischk;?>>
																<label for="search_extras_1">One Way</label>
															</div>
														</li>
														<li class="search_extras_item">
															<div class="clearfix">
																<input type="radio" id="search_extras_2" name="one"
																	value="2" class="search_extras_cb"
																	<?php echo $dischk1;?>>
																<label for="search_extras_2">Round Trip</label>
															</div>
														</li>
														<li class="search_extras_item">
															<div class="clearfix">
																<input type="radio" id="search_extras_3" name="one"
																	value="3" class="search_extras_cb"
																	<?php echo $dischk2;?>>
																<label for="search_extras_3">Multi City</label>
															</div>
														</li>
													</ul>
												</div>
												<div class="search_item">
													{{ csrf_field() }}
													<div>Flying From</div>
													<input type="text" class="search_input autocomplete flight_from-1"
														name="flight_from[]" required="required"
														placeholder="City or Airport" autocomplete="off"
														id="autocomplete" value="{{$mainarray['orign'][0]}}">
													<i class="fa fa-map-marker fa-icon"></i>
												</div>
												<a href="javascript:void()" class="exchange-icon exchangeicon" id="1">
													<i class="fa fa-exchange"></i>
												</a>
												<div class="search_item">
													<div>Flying To</div>
													<input type="text" class="search_input autocomplete flight_to-1"
														name="flight_to[]" required="required"
														placeholder="City or Airport" autocomplete="off"
														id="autocomplete1" value="{{$mainarray['destinew1'][0]}}">
													<i class="fa fa-map-marker fa-icon"></i>

												</div>
												<div class="search_item">
													<div>Departures</div>

													<input type="text" class="search_input departure"
														name="flight_dep[]" placeholder="YYYY-MM-DD" autocomplete="off"
														id="departure" value="{{$mainarray['dep_date'][0]}}">
													<i class="fa fa-calendar fa-icon"></i>
												</div>
												@if($mainarray['JourneyType']=='2')
												<div class="search_item" id="restuntype">
													@else
													<div class="search_item" id="restuntype" style="display: none">
														@endif
														<div>Return Date</div>
														<input type="text" class="search_input returnchk"
															name="flight_return" placeholder="YYYY-MM-DD"
															autocomplete="off" id="return"
															value="<?php if($mainarray['flight_return1']==''){ echo '';}else  { echo $mainarray['flight_return1'] ; } ?>">
														<i class="fa fa-calendar fa-icon"></i>
													</div>

													<div class="search_item">

														<div>Travelers</div>
														<div class=" traveller-dropdown" style="margin-bottom:0px;">
															<div class="searchflight" style="margin-bottom:0px;">
																<input type="text" class="search_input totaltravel "
																	name="flight_nocheck"
																	value=" {{$totaltr}} Travelers" autocomplete="off">
																<i class="fa fa-user fa-icon"></i>
															</div>
															<div class="dropdown-menu dropshowflight">
																<div class="row">
																	<div class="col-md-12">
																		<div class="trave-drop">
																			<div class="travel-left">Adults</div>
																			<div class="plus-minus">
																				<div class="minus adultminus">
																					<i class="fa fa-minus-circle"></i>
																				</div>
																				<input type="hidden" name="adults"
																					class="adultval"
																					value="{{$adultscount}}"
																					maxlength="9" />
																				<div class="text adultcount">
																					{{$adultscount}}</div>
																				<div class="plus adultplus">
																					<i class="fa fa-plus-circle"></i>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<hr>
																<div class="row">
																	<div class="col-md-12">
																		<div class="trave-drop">
																			<div class="travel-left">Children
																				<small>Ages (2-17)</small>
																			</div>
																			<div class="plus-minus ">
																				<div class="minus childminus">
																					<i class="fa fa-minus-circle"></i>
																				</div>
																				<input type="hidden" name="children"
																					id="childval" class="childval"
																					value="{{$childcount}}" />

																				<div class="text childcount">
																					{{$childcount}}</div>
																				<div class="plus childplus">
																					<i class="fa fa-plus-circle"></i>
																				</div>
																			</div>
																		</div>
																	</div>

																</div>

																<div class="row">
																	<div class="col-md-12">
																		<div class="trave-drop">
																			<div class="travel-left">Infants <small>Age
																					Under 0-2</small></div>
																			<div class="plus-minus">
																				<div class="minus infantminus">
																					<i class="fa fa-minus-circle"></i>
																				</div>
																				<input type="hidden" name="infant"
																					id="infantval" class="infantval"
																					value="{{$infantcount}}" />
																				<div class="text infantcount">
																					{{$infantcount}}</div>
																				<div class="plus infantplus">
																					<i class="fa fa-plus-circle"></i>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<hr>
																<div class="row">
																	<div class="col-md-12">
																		<div class="trave-drop">
																			<div class="travel-left">Class </div>
																			<select
																				class="dropdown_item_select search_input class-icon"
																				name="flight_class">

																				<option value="2%Economy"
																					<?php if($mainarray['FlightCabinName']=='Economy'){echo "selected";} ?>>
																					Economy</option>
																				<option value="3%Premium Economy"
																					<?php if($mainarray['FlightCabinName']=='Premium Economy'){echo "selected";} ?>>
																					Premium Economy</option>
																				<option value="4%Business"
																					<?php if($mainarray['FlightCabinName']=='Business'){echo "selected";} ?>>
																					Business</option>

																				<option value="6%First"
																					<?php if($mainarray['FlightCabinName']=='First'){echo "selected";} ?>>
																					First Class</option>
																			</select>
																		</div>
																	</div>
																</div>

																<div class="container">
																	<div class="row">
																		<div class="col-md-12">
																			<span class="a-done-btn"
																				id="flightdone">Done</span>

																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<!-- <div class="search_item" style="display: none">
												<div>Adults</div>
												<select name="adults" id="adults_1" class="dropdown_item_select search_input">
													<option value="1" <?php if($adultscount=='1') {echo "selected";} else {echo ""; } ?> >01</option>
													<option value="2" <?php if($adultscount=='2') {echo "selected";} else {echo ""; } ?>>02</option>
													<option value="3" <?php if($adultscount=='3') {echo "selected";} else {echo ""; } ?>>03</option>
													<option value="4" <?php if($adultscount=='4') {echo "selected";} else {echo ""; } ?>>04</option>
													<option value="5" <?php if($adultscount=='5') {echo "selected";} else {echo ""; } ?>>05</option>
													<option value="6" <?php if($adultscount=='6') {echo "selected";} else {echo ""; } ?>>06</option>
													<option value="7" <?php if($adultscount=='7') {echo "selected";} else {echo ""; } ?>>07</option>
													<option value="8" <?php if($adultscount=='8') {echo "selected";} else {echo ""; } ?>>08</option>
													<option value="9" <?php if($adultscount=='9') {echo "selected";} else {echo ""; } ?>>09</option>
												</select>
												<i class="fa fa-user fa-icon"></i>
											</div>
											<div class="search_item" style="display: none">
												<div>children</div>
												<select name="children" id="children_1" class="dropdown_item_select search_input children-icon">
													<option value="0" <?php if($childcount=='0') {echo "selected";} else {echo ""; } ?> >0</option>
													<option value="1" <?php if($childcount=='1') {echo "selected";} else {echo ""; } ?>>01</option>
													<option value="2" <?php if($childcount=='2') {echo "selected";} else {echo ""; } ?>>02</option>
													<option value="3" <?php if($childcount=='3') {echo "selected";} else {echo ""; } ?>>03</option>
												</select>
												<i class="fa fa-child fa-icon"></i>
											</div>
											<div class="search_item" style="display: none">
												<div>Class</div>
												<select class="dropdown_item_select search_input class-icon" name="flight_class">
													<option value="1%All" <?php if($mainarray['FlightCabinName']=='All') {echo "selected";} else {echo ""; } ?>>All</option>
													<option value="2%Economy" <?php if($mainarray['FlightCabinName']=='Economy') {echo "selected";} else {echo ""; } ?>>Economy</option>
													<option value="3%Premium Economy" <?php if($mainarray['FlightCabinName']=='Premium Economy') {echo "selected";} else {echo ""; } ?>>Premium Economy</option>
													<option value="4%Business" <?php if($mainarray['FlightCabinName']=='Business') {echo "selected";} else {echo ""; } ?>>Business</option>
													<option value="5%Premium Business" <?php if($mainarray['FlightCabinName']=='Premium Business') {echo "selected";} else {echo ""; } ?>>Premium Business</option>
													<option value="6%First" <?php if($mainarray['FlightCabinName']=='First') {echo "selected";} else {echo ""; } ?>>First</option>
												</select>
												<i class="fa fa-plane fa-icon"></i>
											</div> -->
													@if($mainarray['JourneyType']=='3')
													<div class="multicity-main">
														@else
														<div class="multicity-main" style="display:none !important;">
															@endif

															<div class="showmultinew" id="showmulti">
																<?php
																				if(count($mainarray['orign']) > '1')
																				{
																					for($multinew=1; $multinew<count($mainarray['orign']);$multinew++)
																					{
																						$lastmainid = $multinew+1;
																						echo '<span style="overflow:visible" class="more_options_list multicity-another-flight search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start" id="mul-'.$lastmainid.'">
														<div class="search_item">
														  
														  <div>Flying From</div>
														  <input type="text" class="search_input autocomplete flight_from-'.$lastmainid.' " name="flight_from[]" required="required" placeholder="City or Airport" autocomplete="off" id="autocomplete"  value="'.$mainarray['orign'][$multinew].'">
														  <i class="fa fa-map-marker fa-icon"></i>
														</div>
														<a href="javascript:void()" class="exchange-icon exchangeicon" id="'.$lastmainid.'">
														  <i class="fa fa-exchange"></i>
														</a>
														<div class="search_item">
														  <div>Flying To</div>
														  <input type="text" class="search_input autocomplete flight_to-'.$lastmainid.'" name="flight_to[]" required="required" placeholder="City or Airport" autocomplete="off" id="autocomplete1" value="'.$mainarray['destinew1'][$multinew].'">
														  <i class="fa fa-map-marker fa-icon"></i>
														  
														</div>
														<div class="search_item">
														  <div>Departures</div>
														  <input type="text" class="search_input flight_dep-'.$lastmainid.' departure" name="flight_dep[]"  placeholder="YYYY-MM-DD" autocomplete="off" id="departure" value="'.$mainarray['dep_date'][$multinew].'">
														  <i class="fa fa-calendar fa-icon"></i>
														</div>
														<a href="javascript:void()" class="close-icon remove" id="'.$lastmainid.'">
														  <i class="fa fa-times"></i>
														</a>

														<div class="search_item">
														  &nbsp;
														</div>
														
													  </span>';
																					}
																				}
																				else
																				{

																				}

																				?>
															</div>
															<div
																class="more_options-<?php echo count($mainarray['orign']) ;?>">
																<div class="more_options_trigger">
																	<a href="javascript:void()" class="add_more"
																		id="add_more-<?php echo count($mainarray['orign']) ;?>">Add
																		Another Flight</a>
																</div>
															</div>

														</div>
														<div class="float-right">
															<button
																class="button search_button">search<span></span><span></span><span></span></button>
														</div>
											</form>
										</div>

										<!-- Search Panel -->


										<!-- Search Panel -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="flight-details">
					<!-- CHEPEAT FLIGHT SHOW DIV -->
					<div class="filter-res-div">
						<button type="button" class="btn btn-primary filter-btn" data-toggle="modal" data-target="#myModal">
							<i class="fa fa-filter"></i>
								<span id="span-filter">Filter</span>
						</button>
					</div>
					<div class="col-lg-12 cheap-flight">
						<div class="airlines-list">
							<div class="showtable1">
								<input type="hidden" value="{{$resu[0][0]['flightcurrency']}}" id="recur">
								<?php

								if($mainarray['JourneyType']=='2')
								{
								if(!empty($resu[1]))
								{
								?>
								<div class="flight-list listflight">

									<div class="row">
										<?php $imgsou =  'assets/images/flag/'.$resu[0][0]['Segments'][0][0]['Airline']['AirlineCode'].'.gif';
										$imgsou1 =  'assets/images/flag/'.$resu[1][0]['Segments'][0][0]['Airline']['AirlineCode'].'.gif';

										$publishvaleu=round($resu[0][0]['flight_base_fare'] + $resu[0][0]['flight_base_tax']);
										$mainvalue = number_format($publishvaleu);
										$publishvaleuret=round($resu[1][0]['flight_base_fare'] + $resu[1][0]['flight_base_tax']);
										$returnvalue = number_format($publishvaleuret);
										$totalfare = $publishvaleu + $publishvaleuret;
										$devdatechep = explode('T',$resu[0][0]['Segments'][0][0]['Origin']['DepTime']);
										$depnewtimechep = date("H:s" , strtotime($devdatechep[1]));

										$devdatechepreturn = explode('T',$resu[1][0]['Segments'][0][0]['Origin']['DepTime']);
										$depnewtimechepreturn = date("H:s" , strtotime($devdatechepreturn[1]));

										?>
										<div class="col-md-2">
											<input type="hidden" value="{{$publishvaleu}}" id="cheapflight">
											<input type="hidden" value="{{$publishvaleuret}}" id="cheapflight1">
											<div class="flight-name">

												<img src="{{$imgsou}}" class="img-responsive flight-logo"
													id="cheapflightimg">
												<p class="flight-name-heading cheapflightname" id="cheapflightname">
													{{$resu[0][0]['Segments'][0][0]['Airline']['AirlineName']}}</p>
												<p id="cheapflightcode">
													{{$resu[0][0]['Segments'][0][0]['Airline']['AirlineCode']}} -
													{{$resu[0][0]['Segments'][0][0]['Airline']['FlightNumber']}}</p>
											</div>
										</div>
										<div class="col-md-2">
											<div class="flight-price flight-depart ">

												<?php
												for($seg_i=0;$seg_i<count($resu[0][0]['Segments'][0]);$seg_i++)
												{
													echo $flightroute = '<p class="flight-depart-text cheapflightroute">'.$resu[0][0]['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode'].' <i class="fa fa-plane fa-rotate-45"></i>    '.$resu[0][0]['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'].'</p>';
													$arvdatechep = explode('T',$resu[0][0]['Segments'][0][$seg_i]['Destination']['ArrTime']);
													$arvtimechep = date("H:s" , strtotime($arvdatechep[1]));
													echo $timechepest = '<p class="flight-depart-text" id="chepflightdep">'.$depnewtimechep.' <i class="fa fa-plane fa-rotate-45"></i>    '.$arvtimechep.'</p>';

												}
												?>
												<p class="flight-price-heading cheapflightprice"><i
														class="fa {{$resu[0][0]['flightcurrency']}}"></i> {{$mainvalue}}
												</p>
											</div>
										</div>
										<div class="col-md-1">
											<div class="cheap-flight-return-icon">
												<i class="fa fa-exchange"></i>
											</div>
										</div>
										<div class="col-md-2">
											<div class="flight-name">

												<img src="{{$imgsou1}}" class="img-responsive flight-logo"
													id="rcheapflightimg">
												<p class="flight-name-heading rcheapflightname" id="rcheapflightname">
													{{$resu[1][0]['Segments'][0][0]['Airline']['AirlineName']}}</p>
												<p id="rcheapflightcode">
													{{$resu[1][0]['Segments'][0][0]['Airline']['AirlineCode']}} -
													{{$resu[1][0]['Segments'][0][0]['Airline']['FlightNumber']}}</p>
											</div>
										</div>
										<div class="col-md-2">
											<div class="flight-price flight-depart">
												<?php
												for($seg_i=0;$seg_i<count($resu[1][0]['Segments'][0]);$seg_i++)
												{
													echo $flightroute = '<p class="flight-depart-text rcheapflightroute">'.$resu[1][0]['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode'].' <i class="fa fa-plane fa-rotate-45"></i>    '.$resu[1][0]['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'].'</p>';
													$arvdatechep = explode('T',$resu[1][0]['Segments'][0][$seg_i]['Destination']['ArrTime']);
													$arvtimechep = date("H:s" , strtotime($arvdatechep[1]));
													echo $timechepest = '<p class="flight-depart-text" id="rchepflightdep">'.$depnewtimechepreturn.' <i class="fa fa-plane fa-rotate-45"></i>    '.$arvtimechep.'</p>';

												}
												?>
												<p class="flight-price-heading rcheapflightprice"><i
														class="fa {{$resu[1][0]['flightcurrency']}}"></i>
													{{$returnvalue}}</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="flight-price flight-book flight-name">
												<p class="flight-price-heading" id="cheapprice"><i
														class="fa {{$resu[0][0]['flightcurrency']}}"></i>
													{{number_format($publishvaleu + $publishvaleuret,2)}}</p>
												<p class="flight-name-heading">Price Including All Taxes </p>
												<form action="{{route('flightbook')}}" method="post">
													<input name="_token" type="hidden" value="{{ csrf_token() }}" />
													<input type="hidden" id="cheapresultindex" name="resultindex"
														value="{{$resu[0][0]['ResultIndex']}}">
													<input type="hidden" name="flightcabinclass"
														value="{{$mainarray['FlightCabinName']}}">
													<input type="hidden" id="cheapresultindex1" name="resultindex1"
														value="{{$resu[1][0]['ResultIndex']}}">

													<input type="hidden" name="journytype"
														value="{{$mainarray['JourneyType']}}">
													<input type="hidden" name="flightorign" id="flightorign"
														value="{{$mainarray['flightorign']}}">
													<input type="hidden" name="flightdep" id="flightdep"
														value="{{$mainarray['flightdep']}}">
													<input type="hidden" name="flightname"
														value="{{$resu[0][0]['Segments'][0][0]['Airline']['AirlineName']}}">
													<button class="btn flight-book-btn cheap-btn">Book</button>
												</form>

											</div>
										</div>

									</div>

								</div>
								<?php
								}
								}
								?>
							</div>
						</div>
					</div>
					<!-- CHEPEAT FLIGHT END DIV -->
					<div class="container">
						<div class="row">
							<div class="col-lg-9" id="res-two">
								<?php
								if($mainarray['JourneyType']=='1')
								{
								?>
								<div class="airlines-list">
									<div class="title-bar">
										<div class="row">
											<div class="col-2">
												<div class="bar-heading">
													<h5>Airline</h5>
												</div>
											</div>
											<div class="col-2">
												<div class="bar-heading">
													<h5>Depart</h5>
												</div>
											</div>
											<div class="col-2">
												<div class="bar-heading">
													<h5>Arrive</h5>
												</div>
											</div>
											<div class="col-2">
												<div class="bar-heading">
													<h5>Duration</h5>
												</div>
											</div>
											<div class="col-2">
												<div class="bar-heading">
													<h5>Price</h5>
												</div>
											</div>

										</div>
									</div>
									<div class="showtable">
										<div class="newtable" id="listflight">
											@foreach($resu[0] as $arrays1)
											<?php
												$timetotal=0;
												//depature time
												for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
												{
													$arvdate1 = explode('T',$arrays1['Segments'][0][$seg_i]['Destination']['ArrTime']);
													$arvtime2 = date("H:s" , strtotime($arvdate1[1]));
													$arvtime1 = date("H:i:s" , strtotime($arvdate1[1]));
													$arvnewdate1= date("d-M-y" , strtotime($arvdate1[0]));
												}


												$arvnewdate1;
												$devdate = explode('T',$arrays1['Segments'][0][0]['Origin']['DepTime']);
												$depnewdate= date("d-M-y" , strtotime($devdate[0]));
												$depnewtime2 = date("H:s" , strtotime($devdate[1]));
												$depnewtime = date("H:i:s" , strtotime($devdate[1]));
												$newdepnewdate= date("Y-m-d" , strtotime($devdate[0]));

												//arrival time
												$arvdate = explode('T',$arrays1['Segments'][0][0]['Destination']['ArrTime']);
												$arvnewdate= date("d-M-y" , strtotime($arvdate[0]));
												$arvtime = date("H:s:i" , strtotime($arvdate[1]));
												$newarvnewdate= date("Y-m-d" , strtotime($arvnewdate1));
												$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newdepnewdate.''. $depnewtime);
												$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newarvnewdate.''. $arvtime1);

												$totalDuration = $from->diffInSeconds($to);
												$durationhour =  gmdate('H', $totalDuration);
												$durationmin =  gmdate('s', $totalDuration);

												$newtime=$durationhour.'H : '.$durationmin.'M';

												$imgsou =  'assets/images/flag/'.$arrays1['Segments'][0][0]['Airline']['AirlineCode'].'.gif';
												$stopcount = count($arrays1['Segments'][0]);
												$uniq = $arrays1['Segments'][0][0]['Airline']['AirlineName'];
												if(!empty($arrays1['Segments'][0][0]['NoOfSeatAvailable']))
												{

												$publishvaleu=round($arrays1['flight_fare']);
												$mainvalue = number_format($publishvaleu);
												if($stopcount=='1')
												{
													$mainstopflight = 'Non Stop';
												}
												else
												{
													$stopcount1 = $stopcount-1;
													$mainstopflight = $stopcount1.' Stop';
												}

												?>

											<div class="flight-list listflight">
												<div class="row">
													<div class="col-md-2">
														<div class="flight-name">
															<img src="{{$imgsou}}" class="img-responsive flight-logo">
															<p class="flight-name-heading">
																{{$arrays1['Segments'][0][0]['Airline']['AirlineName']}}
															</p>
															<p>{{$arrays1['Segments'][0][0]['Airline']['AirlineCode']}}
																-
																{{$arrays1['Segments'][0][0]['Airline']['FlightNumber']}}
															</p>
														</div>
													</div>
													<div class="col-md-2">
														<div class="flight-depart">
															<p class="flight-depart-heading">{{$depnewtime2}} |
																{{$depnewdate}} </p>
															<?php
																for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
																{
																	echo $flightroute = '<p class="flight-depart-text">'.$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode'].' <i class="fa fa-plane fa-rotate-45"></i>    '.$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'].'</p>';
																}
																?>
														</div>
													</div>
													<div class="col-md-2">
														<div class="flight-arrival">
															<p class="flight-arrival-heading">{{$arvtime2}} |
																{{$arvnewdate1}}</p>
														</div>
													</div>
													<div class="col-md-2">
														<div class="flight-duration">
															<p class="flight-duration-heading">{{$newtime}}</p>
															<p style="color:#008cff">{{$mainstopflight}}</p>
														</div>
													</div>
													<div class="col-md-2">
														<div class="flight-price">
															<p class="flight-price-heading"><i
																	class="fa {{$arrays1['flightcurrency']}}"></i>
																{{$mainvalue}} </p>
															<p>{{$arrays1['Segments'][0][0]['NoOfSeatAvailable']}} Seats
																Left</p>
														</div>
													</div>
													<div class="col-md-2">
														<div class="flight-book">
															<form action="{{route('flightbook')}}" method="post">
																<input name="_token" type="hidden"
																	value="{{ csrf_token() }}" />
																<input type="hidden" name="resultindex"
																	value="{{$arrays1['ResultIndex']}}">
																<input type="hidden" name="flightcabinclass"
																	value="{{$mainarray['FlightCabinName']}}">
																<input type="hidden" name="journytype"
																	value="{{$mainarray['JourneyType']}}">
																<input type="hidden" name="flightorign" id="flightorign"
																	value="{{$mainarray['flightorign']}}">
																<input type="hidden" name="flightdep"
																	value="{{$mainarray['flightdep']}}" id="flightdep">
																<input type="hidden" name="flightname"
																	value="{{$arrays1['Segments'][0][0]['Airline']['AirlineName']}}">
																<button class="btn flight-book-btn">Book</button>
															</form>
															<!-- <p><a href="{{route('flightbook')}}" class="btn flight-book-btn">Book</a></p> -->
															<a href="javascript:void()" class="details"
																id="{{$arrays1['ResultIndex']}}">Details</a>
														</div>
													</div>
												</div>
												<div class="flight-fare">
													<div class="row">
														<div class="col-6" style="padding: 0;padding-left: 5px">
															<h5 class="fare-rules">Fare Rules</h5>
														</div>

														<div class="col-6" style="padding: 0;padding-right: 5px">
															@if($arrays1['IsRefundable'])
															<h5 class="fare-type refundable"><i class="fa fa-money"></i>
																Refundable</h5>
															@else
															<h5 class="fare-type non-refundable"><i
																	class="fa fa-money"></i> Non-Refundable</h5>
															@endif

														</div>
													</div>
												</div>
												<div class="flight-details-tabs"
													id="flight-{{$arrays1['ResultIndex']}}">
													<ul class="nav nav-tabs">
														<li><a data-toggle="tab"
																href="#home-{{$arrays1['ResultIndex']}}"
																class="active">Flight information</a></li>
														<li><a data-toggle="tab"
																href="#menu1-{{$arrays1['ResultIndex']}}">Fare
																Details</a></li>
														<li><a data-toggle="tab"
																href="#menu2-{{$arrays1['ResultIndex']}}">Baggage
																information</a></li>
														<li><a data-toggle="tab"
																href="#menu3-{{$arrays1['ResultIndex']}}">Cancellation
																Rules</a></li>
													</ul>

													<div class="tab-content">
														<div id="home-{{$arrays1['ResultIndex']}}"
															class="tab-pane fade-in active">
															<div class="flight-list listflight">
																<div class="row">
																	<?php
																		for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
																		{
																		$arvdate11 = explode('T',$arrays1['Segments'][0][$seg_i]['Destination']['ArrTime']);
																		$arvtime22 = date("H:s" , strtotime($arvdate11[1]));
																		$arvnewdate11= date("d-M" , strtotime($arvdate11[0]));
																		$devdate1 = explode('T',$arrays1['Segments'][0][$seg_i]['Origin']['DepTime']);
																		$depnewdatedetail= date("d-M" , strtotime($devdate1[0]));
																		$depnewtimedetail = date("H:s" , strtotime($devdate1[1]));
																		?>
																	<div class="col-md-4">
																		<div class="flight-name">
																			<img src="{{$imgsou}}"
																				class="img-responsive flight-logo">
																			<p class="flight-name-heading">
																				{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName']}}
																			</p>
																			<p>{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode']}}
																				-
																				{{$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber']}}
																			</p>
																		</div>
																	</div>
																	<div class="col-md-4">
																		<div class="flight-depart">

																			<p class="flight-depart-text">
																				{{$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['CityName']}}
																				({{$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode']}})
																			</p>
																			<p class="flight-depart-heading">
																				{{$depnewdatedetail}} |
																				{{$depnewtimedetail}} </p>

																		</div>
																	</div>
																	<div class="col-md-4">
																		<div class="flight-depart">

																			<p class="flight-depart-text">
																				{{$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['CityName']}}
																				({{$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode']}})
																			</p>
																			<p class="flight-depart-heading">
																				{{$arvnewdate11}} | {{$arvtime22}} </p>

																		</div>
																	</div>
																	<?php }
																		?>
																</div>
															</div>
														</div>
														<div id="menu1-{{$arrays1['ResultIndex']}}"
															class="tab-pane fade">
															<table class="table table-bordered">
																<tbody>
																	<?php
																	$totalbasefare=0;
																	$totaltax=0;
																	$child='';
																	$Infant='';
																	for($fare_i=0;$fare_i<count($arrays1['FareBreakdown']);$fare_i++)
																	{
																		$totalbasefare +=$arrays1['FareBreakdown'][$fare_i]['BaseFare'];
																		$totaltax +=$arrays1['FareBreakdown'][$fare_i]['Tax'];
																		if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='1')
																		{
																			$adults=  $arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Adults';
																		}
																		if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='2' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
																		{
																			$child=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Child';
																		}
																		if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='3' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
																		{
																			$Infant=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Infant';
																		}
																	}
																	$totalfare= $arrays1['flight_base_fare'] + $arrays1['flight_base_tax'];
																	?>
																	<tr>
																		<td>Base Fare ({{$adults}} {{$child}}
																			{{$Infant}})</td>
																		<td><i
																				class="fa {{$arrays1['flightcurrency']}}"></i>
																			{{$arrays1['flight_base_fare']}}</td>
																	</tr>
																	<tr>
																		<td>Taxes and Fees ({{$adults}} {{$child}}
																			{{$Infant}})</td>
																		<td><i
																				class="fa {{$arrays1['flightcurrency']}}"></i>
																			{{$arrays1['flight_base_tax']}}</td>

																	</tr>
																	<tr>
																		<td>Total Fare (Roundoff) ({{$adults}}
																			{{$child}} {{$Infant}})</td>
																		<td><i
																				class="fa {{$arrays1['flightcurrency']}}"></i>
																			{{round($totalfare)}}</td>

																	</tr>
																</tbody>
															</table>
														</div>
														<div id="menu2-{{$arrays1['ResultIndex']}}"
															class="tab-pane fade">
															<div class="flight-list listflight">
																<div class="row">
																	<?php
																		for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
																		{ ?>
																	<div class="col-md-4">
																		<div class="flight-name">
																			<img src="{{$imgsou}}"
																				class="img-responsive flight-logo">
																			<p class="flight-name-heading">
																				{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName']}}
																			</p>
																			<p>{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode']}}
																				-
																				{{$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber']}}
																			</p>
																		</div>
																	</div>
																	<table class="table table-bordered">
																		<tbody>
																			<tr>
																				<td>Baggage</td>
																				<td>{{$arrays1['Segments'][0][$seg_i]['Baggage']}}
																				</td>
																			</tr>
																			<tr>
																				<td>Cabin Baggage</td>
																				<td>7 Kg</td>
																			</tr>
																		</tbody>
																	</table>
																	<?php  }
																		?>
																</div>
															</div>
														</div>
														<div id="menu3-{{$arrays1['ResultIndex']}}"
															class="tab-pane fade">
															<h4>Terms & Conditions</h4>
															<ul>
																<li>Airlines stop accepting cancellation/rescheduling
																	requests 24 - 72 hours before departure of the
																	flight, depending on the airline. </li>
																<li>The rescheduling/cancellation fee may also vary
																	based on fluctuations in currency conversion rates.
																</li>
																<li>Rescheduling Charges = Rescheduling/Change Penalty +
																	Fare Difference (if applicable)</li>
																<li>In case of restricted cases , no amendments
																	/cancellation allowed. </li>
																<li>Airline penalty needs to be reconfirmed prior to any
																	amendments or cancellation.</li>
																<li>Disclaimer: Airline Penalty changes are indicative
																	and can change without prior notice</li>
																<li>NA means Not Available. Please check with airline
																	for penalty information.</li>
																<li>The charges are per passenger per sector.</li>
															</ul>
														</div>
													</div>
												</div>


												<!-- <div class="flight-details-tabs">
                                                        <div class="row">
                                                            <ul class="nav nav-tabs">
                                                                <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
                                                                <li><a data-toggle="tab" href="#menu1">Menu 1</a></li>
                                                                <li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
                                                                <li><a data-toggle="tab" href="#menu3">Menu 3</a></li>
                                                            </ul>

                                                            <div class="tab-content">
                                                                <div id="home" class="tab-pane fade in active">
                                                                  <h3>HOME</h3>
                                                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                                </div>
                                                                <div id="menu1" class="tab-pane fade">
                                                                  <h3>Menu 1</h3>
                                                                  <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                                </div>
                                                                <div id="menu2" class="tab-pane fade">
                                                                  <h3>Menu 2</h3>
                                                                  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                                                                </div>
                                                                <div id="menu3" class="tab-pane fade">
                                                                  <h3>Menu 3</h3>
                                                                  <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> -->
											</div>
											<?php } ?>
											@endforeach
										</div>
									</div>

								</div>
								<?php }
								if( $mainarray['JourneyType']=='3' )
								{
								?>
								<div class="airlines-list">
									<div class="title-bar">
										<div class="row">
											<div class="col-2">
												<div class="bar-heading">
													<h5>Trip</h5>
												</div>
											</div>
											<div class="col-3">
												<div class="bar-heading">
													<h5>Airline</h5>
												</div>
											</div>
											<div class="col-2">
												<div class="bar-heading">
													<h5>Depart</h5>
												</div>
											</div>
											<div class="col-3">
												<div class="bar-heading">
													<h5>Duration</h5>
												</div>
											</div>
											<div class="col-2">
												<div class="bar-heading">
													<h5>Arrive</h5>
												</div>
											</div>
											{{-- <div class="col-md-2 res-none">
												<div class="bar-heading">
													<h5>&nbsp;</h5>
												</div>
											</div> --}}
										</div>
									</div>
									<div class="showtable2">
										<div class="newtable" id="listflight">
											@foreach($resu[0] as $arrays1)
											<?php

												$imgsou =  'assets/images/flag/'.$arrays1['Segments'][0][0]['Airline']['AirlineCode'].'.gif';

												$uniq = $arrays1['Segments'][0][0]['Airline']['AirlineName'];
												if(!empty($arrays1['Segments'][0][0]['NoOfSeatAvailable']))
												{

												$publishvaleu=round($arrays1['flight_fare'] );
												$mainvalue = number_format($publishvaleu);

												?>

											<div class="flight-list listflight">
												<!-- show new div -->
												<div class="row">
													<?php
														$trip=1;
														for($seg_i=0;$seg_i<count($arrays1['Segments']);$seg_i++)
														{
														$arvdate11 = explode('T',$arrays1['Segments'][$seg_i][0]['Destination']['ArrTime']);
														$arvtime22 = date("H:i" , strtotime($arvdate11[1]));
														$arvnewdate11= date("d-M" , strtotime($arvdate11[0]));
														$arvtimedis = date("H:i:s" , strtotime($arvdate11[1]));

														$arvnewdatedis= date("Y-m-d" , strtotime($arvdate11[0]));
														$devdate1 = explode('T',$arrays1['Segments'][$seg_i][0]['Origin']['DepTime']);
														$depnewdatedetail= date("d-M" , strtotime($devdate1[0]));
														$depnewtimedetail = date("H:i" , strtotime($devdate1[1]));
														$deptimedis = date("H:i:s" , strtotime($devdate1[1]));
														$depnewdatedis= date("Y-m-d" , strtotime($devdate1[0]));
														$stopcountnew = count($arrays1['Segments'][$seg_i]);
														if($stopcountnew=='1')
														{
															$mainstopflight = 'Non Stop';
														}
														else
														{
															$stopcount1 = $stopcountnew-1;
															$mainstopflight = $stopcount1.' Stop';

														}
														for($seg_ii=0;$seg_ii<count($arrays1['Segments'][$seg_i]);$seg_ii++)
														{

															$flightroute= $arrays1['Segments'][$seg_i][$seg_ii]['Origin']['Airport']['CityName'].'('. $arrays1['Segments'][$seg_i][$seg_ii]['Origin']['Airport']['AirportCode']. ') -> '.$arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['CityName'] . '('.$arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['AirportCode'].')';
															$destimain= $arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['CityName'] . '('.$arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['AirportCode'].')';
														}
														$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $depnewdatedis.''. $deptimedis);
														$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $arvnewdatedis.''. $arvtimedis);

														$totalDuration = $from->diffInSeconds($to);
														$durationhour =  gmdate('H', $totalDuration);
														$durationmin =  gmdate('s', $totalDuration);

														$newtimenew=$durationhour.'H : '.$durationmin.'M';

														?>
													<div class="col-md-2">
														<h5 class="trip-title">Trip {{$trip++}}</h5>
													</div>
													<div class="col-md-3">
														<div class="flight-name">
															<img src="{{$imgsou}}" class="img-responsive flight-logo">
															<p class="flight-name-heading">
																{{$arrays1['Segments'][$seg_i][0]['Airline']['AirlineName']}}
															</p>
															<p>{{$arrays1['Segments'][$seg_i][0]['Airline']['AirlineCode']}}
																-
																{{$arrays1['Segments'][$seg_i][0]['Airline']['FlightNumber']}}
															</p>
														</div>
													</div>
													<div class="col-md-3">
														<div class="flight-depart">

															<p class="flight-depart-text">
																{{$arrays1['Segments'][$seg_i][0]['Origin']['Airport']['CityName']}}
																({{$arrays1['Segments'][$seg_i][0]['Origin']['Airport']['AirportCode']}})
															</p>
															<p class="flight-depart-heading">{{$depnewdatedetail}} |
																{{$depnewtimedetail}} </p>

														</div>
													</div>
													<div class="col-md-2">
														<div class="flight-duration">
															<p class="flight-duration-heading">{{$newtimenew}}</p>
															<p class="tooltip"
																style="color:#008cff;opacity: 1 !important">
																{{$mainstopflight}}
																<span class="tooltiptext">{{$flightroute}}</span></p>
														</div>
													</div>
													<div class="col-md-2">
														<div class="flight-depart">

															<p class="flight-depart-text">{{$destimain}}</p>
															<p class="flight-depart-heading">{{$arvnewdate11}} |
																{{$arvtimedis}} </p>

														</div>
													</div>
													<?php }
														?>
												</div>
												<!-- end show new div -->
												<div class="row">
													<div class="col-md-8">
														<!-- <div class="flight-name">
															<img src="{{$imgsou}}" class="img-responsive flight-logo">
															<p class="flight-name-heading">{{$arrays1['Segments'][0][0]['Airline']['AirlineName']}}</p>
															
														</div> -->
													</div>

													<div class="col-md-2">
														<div class="flight-price">
															<p class="flight-price-heading"><i
																	class="fa {{$arrays1['flightcurrency']}}"></i>
																{{$mainvalue}}</p>
															<p>{{$arrays1['Segments'][0][0]['NoOfSeatAvailable']}} Seats
																Left</p>
														</div>
													</div>
													<div class="col-md-2">
														<div class="flight-book">
															<form action="{{route('flightbook')}}" method="post">
																<input name="_token" type="hidden"
																	value="{{ csrf_token() }}" />
																<input type="hidden" name="resultindex"
																	value="{{$arrays1['ResultIndex']}}">
																<input type="hidden" name="journytype"
																	value="{{$mainarray['JourneyType']}}">
																<input type="hidden" name="flightcabinclass"
																	value="{{$mainarray['FlightCabinName']}}">
																<input type="hidden" name="journytype"
																	value="{{$mainarray['JourneyType']}}">
																<input type="hidden" name="flightname"
																	value="{{$arrays1['Segments'][0][0]['Airline']['AirlineName']}}">
																<input type="hidden" name="flightorign" id="flightorign"
																	value="{{$mainarray['flightorign']}}">
																<input type="hidden" name="flightdep"
																	value="{{$mainarray['flightdep']}}" id="flightdep">
																<button class="btn flight-book-btn">Book</button>
															</form>
															<a href="javascript:void()" class="details"
																id="{{$arrays1['ResultIndex']}}">Details</a>
														</div>
													</div>
												</div>

												<div class="flight-fare">
													<div class="row">
														<div class="col-md-2">
															<h5 class="fare-rules">Fare Rules</h5>
														</div>
														<div class="col-md-7">
															<!-- <h5 class="airline-mark">Airline Mark : <span> {{$arrays1['AirlineRemark']}}</span></h5> -->
														</div>
														<div class="col-md-3">
															@if($arrays1['IsRefundable'])
															<h5 class="fare-type refundable"><i class="fa fa-money"></i>
																Refundable</h5>
															@else
															<h5 class="fare-type non-refundable"><i
																	class="fa fa-money"></i> Non-Refundable</h5>
															@endif

														</div>
													</div>

												</div>
												<div class="flight-details-tabs"
													id="flight-{{$arrays1['ResultIndex']}}">
													<ul class="nav nav-tabs">
														<li><a data-toggle="tab"
																href="#home-{{$arrays1['ResultIndex']}}"
																class="active">Flight information</a></li>
														<li><a data-toggle="tab"
																href="#menu1-{{$arrays1['ResultIndex']}}">Fare
																Details</a></li>
														<li><a data-toggle="tab"
																href="#menu2-{{$arrays1['ResultIndex']}}">Baggage
																information</a></li>
														<li><a data-toggle="tab"
																href="#menu3-{{$arrays1['ResultIndex']}}">Cancellation
																Rules</a></li>
													</ul>

													<div class="tab-content">
														<div id="home-{{$arrays1['ResultIndex']}}"
															class="tab-pane fade-in active">
															<div class="flight-list listflight">
																<div class="row">
																	<?php
																		$trip=1;
																		for($seg_i=0;$seg_i<count($arrays1['Segments']);$seg_i++)
																		{
																		$arvdate11 = explode('T',$arrays1['Segments'][$seg_i][0]['Destination']['ArrTime']);
																		$arvtime22 = date("H:i" , strtotime($arvdate11[1]));
																		$arvnewdate11= date("d-M" , strtotime($arvdate11[0]));
																		$arvtimedis = date("H:i:s" , strtotime($arvdate11[1]));

																		$arvnewdatedis= date("Y-m-d" , strtotime($arvdate11[0]));
																		$devdate1 = explode('T',$arrays1['Segments'][$seg_i][0]['Origin']['DepTime']);
																		$depnewdatedetail= date("d-M" , strtotime($devdate1[0]));
																		$depnewtimedetail = date("H:i" , strtotime($devdate1[1]));
																		$deptimedis = date("H:i:s" , strtotime($devdate1[1]));
																		$depnewdatedis= date("Y-m-d" , strtotime($devdate1[0]));
																		$stopcountnew = count($arrays1['Segments'][$seg_i]);
																		if($stopcountnew=='1')
																		{
																			$mainstopflight = 'Non Stop';
																		}
																		else
																		{
																			$stopcount1 = $stopcountnew-1;
																			$mainstopflight = $stopcount1.' Stop';

																		}
																		for($seg_ii=0;$seg_ii<count($arrays1['Segments'][$seg_i]);$seg_ii++)
																		{

																			$flightroute= $arrays1['Segments'][$seg_i][$seg_ii]['Origin']['Airport']['CityName'].'('. $arrays1['Segments'][$seg_i][$seg_ii]['Origin']['Airport']['AirportCode']. ') -> '.$arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['CityName'] . '('.$arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['AirportCode'].')';
																			$destimain= $arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['CityName'] . '('.$arrays1['Segments'][$seg_i][$seg_ii]['Destination']['Airport']['AirportCode'].')';
																		}
																		$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $depnewdatedis.''. $deptimedis);
																		$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $arvnewdatedis.''. $arvtimedis);

																		$totalDuration = $from->diffInSeconds($to);
																		$durationhour =  gmdate('H', $totalDuration);
																		$durationmin =  gmdate('s', $totalDuration);

																		$newtimenew=$durationhour.'H : '.$durationmin.'M';

																		?>
																	<div class="col-md-2">
																		<p>Trip {{$trip++}}</p>
																	</div>
																	<div class="col-md-3">
																		<div class="flight-name">
																			<img src="{{$imgsou}}"
																				class="img-responsive flight-logo">
																			<p class="flight-name-heading">
																				{{$arrays1['Segments'][$seg_i][0]['Airline']['AirlineName']}}
																			</p>
																			<p>{{$arrays1['Segments'][$seg_i][0]['Airline']['AirlineCode']}}
																				-
																				{{$arrays1['Segments'][$seg_i][0]['Airline']['FlightNumber']}}
																			</p>
																		</div>
																	</div>
																	<div class="col-md-3">
																		<div class="flight-depart">

																			<p class="flight-depart-text">
																				{{$arrays1['Segments'][$seg_i][0]['Origin']['Airport']['CityName']}}
																				({{$arrays1['Segments'][$seg_i][0]['Origin']['Airport']['AirportCode']}})
																			</p>
																			<p class="flight-depart-heading">
																				{{$depnewdatedetail}} |
																				{{$depnewtimedetail}} </p>

																		</div>
																	</div>
																	<div class="col-md-2">
																		<div class="flight-duration">
																			<p class="flight-duration-heading">
																				{{$newtimenew}}</p>
																			<p class="tooltip"
																				style="color:#008cff;opacity: 1 !important">
																				{{$mainstopflight}}
																				<span
																					class="tooltiptext">{{$flightroute}}</span>
																			</p>
																		</div>
																	</div>
																	<div class="col-md-2">
																		<div class="flight-depart">

																			<p class="flight-depart-text">
																				{{$destimain}})</p>
																			<p class="flight-depart-heading">
																				{{$arvnewdate11}} | {{$arvtimedis}} </p>

																		</div>
																	</div>
																	<?php }
																		?>
																</div>
															</div>
														</div>
														<div id="menu1-{{$arrays1['ResultIndex']}}"
															class="tab-pane fade">
															<table class="table table-bordered">
																<tbody>
																	<?php
																	$totalbasefare=0;
																	$totaltax=0;
																	$child='';
																	$Infant='';
																	for($fare_i=0;$fare_i<count($arrays1['FareBreakdown']);$fare_i++)
																	{
																		$totalbasefare +=$arrays1['FareBreakdown'][$fare_i]['BaseFare'];
																		$totaltax +=$arrays1['FareBreakdown'][$fare_i]['Tax'];
																		if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='1')
																		{
																			$adults=  $arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Adults';
																		}
																		if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='2' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
																		{
																			$child=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Child';
																		}
																		if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='3' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
																		{
																			$Infant=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Infant';
																		}
																	}
																	$totalfare= $arrays1['flight_base_fare'] + $arrays1['flight_base_tax'];
																	?>
																	<tr>
																		<td>Base Fare ({{$adults}} {{$child}}
																			{{$Infant}})</td>
																		<td><i
																				class="fa {{$arrays1['flightcurrency']}}"></i>
																			{{$arrays1['flight_base_fare']}}</td>
																	</tr>
																	<tr>
																		<td>Taxes and Fees ({{$adults}} {{$child}}
																			{{$Infant}})</td>
																		<td><i
																				class="fa {{$arrays1['flightcurrency']}}"></i>
																			{{$arrays1['flight_base_tax']}}</td>

																	</tr>
																	<tr>
																		<td>Total Fare (Roundoff) ({{$adults}}
																			{{$child}} {{$Infant}})</td>
																		<td><i
																				class="fa {{$arrays1['flightcurrency']}}"></i>
																			{{round($totalfare)}}</td>

																	</tr>
																</tbody>
															</table>
														</div>
														<div id="menu2-{{$arrays1['ResultIndex']}}"
															class="tab-pane fade">
															<div class="flight-list listflight">
																<div class="row">
																	<?php
																		for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
																		{ ?>
																	<div class="col-md-4">
																		<div class="flight-name">
																			<img src="{{$imgsou}}"
																				class="img-responsive flight-logo">
																			<p class="flight-name-heading">
																				{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName']}}
																			</p>
																			<p>{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode']}}
																				-
																				{{$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber']}}
																			</p>
																		</div>
																	</div>
																	<table class="table table-bordered">
																		<tbody>
																			<tr>
																				<td>Baggage</td>
																				<td>{{$arrays1['Segments'][0][$seg_i]['Baggage']}}
																				</td>
																			</tr>
																			<tr>
																				<td>Cabin Baggage</td>
																				<td>7 Kg
																					<!-- $arrays1['Segments'][0][$seg_i]['CabinBaggage']; -->
																				</td>
																			</tr>
																		</tbody>
																	</table>
																	<?php  }
																		?>
																</div>
															</div>
														</div>
														<div id="menu3-{{$arrays1['ResultIndex']}}"
															class="tab-pane fade">
															<h4>Terms & Conditions</h4>
															<ul>
																<li>Airlines stop accepting cancellation/rescheduling
																	requests 24 - 72 hours before departure of the
																	flight, depending on the airline. </li>
																<li>The rescheduling/cancellation fee may also vary
																	based on fluctuations in currency conversion rates.
																</li>
																<li>Rescheduling Charges = Rescheduling/Change Penalty +
																	Fare Difference (if applicable)</li>
																<li>In case of restricted cases , no amendments
																	/cancellation allowed. </li>
																<li>Airline penalty needs to be reconfirmed prior to any
																	amendments or cancellation.</li>
																<li>Disclaimer: Airline Penalty changes are indicative
																	and can change without prior notice</li>
																<li>NA means Not Available. Please check with airline
																	for penalty information.</li>
																<li>The charges are per passenger per sector.</li>
															</ul>
														</div>
													</div>
												</div>

											</div>
											<?php } ?>
											@endforeach
										</div>
									</div>

								</div>
								<?php }

								if($mainarray['JourneyType']=='2')
								{
								if(!empty($resu[1]))
								{
								?>
								<div class="round-partition">
									<div class="row">
										<div class="col-lg-6 pd-r-5" id="res-two">
											<div class="airlines-list">
												<div class="title-bar">
													<div class="row">
														<div class="col-md-2">
															<div class="bar-heading">
																<h5>Airline</h5>
															</div>
														</div>
														<div class="col-2 col-sm-3">
															<div class="bar-heading">
																<h5>Depart</h5>
															</div>
														</div>
														<div class="col-2 col-sm-3">
															<div class="bar-heading">
																<h5>Arrive</h5>
															</div>
														</div>
														<div class="col-md-2">
															<div class="bar-heading">
																<h5>Duration</h5>
															</div>
														</div>
														<div class="col-md-2">
															<div class="bar-heading">
																<h5>Price</h5>
															</div>
														</div>
													{{-- 	<div class="col-md-2 res-none">
															<div class="bar-heading">
																<h5>&nbsp;</h5>
															</div>
														</div> --}}
													</div>
												</div>
												<div class="showtable_return1">
													<div class="newtable" id="listflight">

														@foreach($resu[0] as $arrays1)
														<?php
															$timetotal=0;

															//depature time
															for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
															{
																$arvdate1 = explode('T',$arrays1['Segments'][0][$seg_i]['Destination']['ArrTime']);
																$arvtime2 = date("H:s" , strtotime($arvdate1[1]));
																$arvtime1 = date("H:i:s" , strtotime($arvdate1[1]));
																$arvnewdate1= date("d-M-y" , strtotime($arvdate1[0]));
																$arvnewdatec= date("d-M" , strtotime($arvdate1[0]));
															}
															$devdate = explode('T',$arrays1['Segments'][0][0]['Origin']['DepTime']);
															$depnewdate= date("d-M" , strtotime($devdate[0]));
															$depnewtime2 = date("H:s" , strtotime($devdate[1]));
															$depnewtime = date("H:i:s" , strtotime($devdate[1]));
															$newdepnewdate= date("Y-m-d" , strtotime($devdate[0]));

															//arrival time
															$arvdate = explode('T',$arrays1['Segments'][0][0]['Destination']['ArrTime']);
															$arvnewdate= date("d-M-y" , strtotime($arvdate[0]));
															$arvtime = date("H:s:i" , strtotime($arvdate[1]));
															$newarvnewdate= date("Y-m-d" , strtotime($arvnewdate1));
															$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newdepnewdate.''. $depnewtime);
															$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newarvnewdate.''. $arvtime1);

															$totalDuration = $from->diffInSeconds($to);
															$durationhour =  gmdate('H', $totalDuration);
															$durationmin =  gmdate('s', $totalDuration);

															$newtime=$durationhour.'H : '.$durationmin.'M';

															$imgsou =  'assets/images/flag/'.$arrays1['Segments'][0][0]['Airline']['AirlineCode'].'.gif';
															$stopcount = count($arrays1['Segments'][0]);
															$uniq = $arrays1['Segments'][0][0]['Airline']['AirlineName'];
															if(!empty($arrays1['Segments'][0][0]['NoOfSeatAvailable']))
															{
															$publishvaleu=round($arrays1['flight_fare'] );
															$mainvalue = number_format($publishvaleu);
															if($stopcount=='1')
															{
																$mainstopflight = 'Non Stop';
															}
															else
															{
																$stopcount1 = $stopcount-1;
																$mainstopflight = $stopcount1.' Stop';
															}
															?>

														<div class="flight-list listflight chepflight1"
															id="{{$arrays1['ResultIndex']}}">
															<input type="hidden" value="{{$publishvaleu}}"
																id="publishprice-{{$arrays1['ResultIndex']}}">
															<div class="row">
																<div class="col-md-2">
																	<div class="flight-name">
																		<img src="{{$imgsou}}"
																			class="img-responsive flight-logo"
																			id="flightimg-{{$arrays1['ResultIndex']}}">
																		<p class="flight-name-heading chkflight"
																			id="flightname-{{$arrays1['ResultIndex']}}">
																			{{$arrays1['Segments'][0][0]['Airline']['AirlineName']}}
																		</p>
																		<p id="flightcode-{{$arrays1['ResultIndex']}}">
																			{{$arrays1['Segments'][0][0]['Airline']['AirlineCode']}}
																			-
																			{{$arrays1['Segments'][0][0]['Airline']['FlightNumber']}}
																		</p>
																	</div>
																</div>
																<div class="col-md-3">
																	<div class="flight-depart">
																		<p class="flight-depart-heading"
																			id="flightdeptime-{{$arrays1['ResultIndex']}}">
																			{{$depnewtime2}} | {{$depnewdate}} </p>

																		<?php
																			for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
																			{
																				echo $flightroute = '<p class="flight-depart-text flightroute-'.$arrays1['ResultIndex'].'" id="flightroute-'.$arrays1['ResultIndex'].'">'.$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode'].' <i class="fa fa-plane fa-rotate-45"></i>    '.$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'].'</p>';
																			}
																			?>
																	</div>
																</div>
																<div class="col-md-3">
																	<div class="flight-arrival">
																		<p class="flight-arrival-heading"
																			id="flightarvtime-{{$arrays1['ResultIndex']}}">
																			{{$arvtime2}} | {{$arvnewdatec}}</p>
																	</div>
																</div>
																<div class="col-md-2">
																	<div class="flight-duration">
																		<p class="flight-duration-heading">{{$newtime}}
																		</p>
																		<p style="color:#008cff">{{$mainstopflight}}</p>
																	</div>
																</div>
																<div class="col-md-2">
																	<div class="flight-price">
																		<p class="flight-price-heading"
																			id="flightprice-{{$arrays1['ResultIndex']}}">
																			<i
																				class="fa {{$arrays1['flightcurrency']}}"></i>
																			{{$mainvalue}}</p>
																		<p>{{$arrays1['Segments'][0][0]['NoOfSeatAvailable']}}
																			Seats Left</p>
																	</div>
																</div>

															</div>
															<div class="flight-fare">
																<div class="row">
																	<div class="col-3 text-left">
																		<h5 class="fare-rules"
																			style="text-align: left !important;">Fare
																			Rules</h5>
																	</div>
																	
																	<div class="col-5 text-right">
																		@if($arrays1['IsRefundable'])
																		<h5 class="fare-type refundable"><i
																				class="fa fa-money"></i> Refundable</h5>
																		@else
																		<h5 class="fare-type non-refundable"><i
																				class="fa fa-money"></i> Non-Refundable
																		</h5>
																		@endif

																	</div>
																	<div class="col-md-4">
																		<div class="flight-book" style="text-align:right;margin-top: -5px;
">
																			<!-- <form action="{{route('flightbook')}}" method="post">
																			 <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
																			<input type="hidden" name="resultindex" value="{{$arrays1['ResultIndex']}}">
																			<button class="btn flight-book-btn">Book</button>
																		</form> -->
																			<a href="javascript:void()" class="details"
																				id="{{$arrays1['ResultIndex']}}">Details</a>
																		</div>
																	</div>
																</div>
															</div>
															<div class="flight-details-tabs"
																id="flight-{{$arrays1['ResultIndex']}}">
																<ul class="nav nav-tabs">
																	<li><a data-toggle="tab"
																			href="#home-{{$arrays1['ResultIndex']}}"
																			class="active">Flight information</a></li>
																	<li><a data-toggle="tab"
																			href="#menu1-{{$arrays1['ResultIndex']}}">Fare
																			Details</a></li>
																	<li><a data-toggle="tab"
																			href="#menu2-{{$arrays1['ResultIndex']}}">Baggage
																			information</a></li>
																	<li><a data-toggle="tab"
																			href="#menu3-{{$arrays1['ResultIndex']}}">Cancellation
																			Rules</a></li>
																</ul>

																<div class="tab-content">
																	<div id="home-{{$arrays1['ResultIndex']}}"
																		class="tab-pane fade-in active">
																		<div class="flight-list listflight">
																			<div class="row">
																				<?php
																					for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
																					{
																					$arvdate11 = explode('T',$arrays1['Segments'][0][$seg_i]['Destination']['ArrTime']);
																					$arvtime22 = date("H:s" , strtotime($arvdate11[1]));
																					$arvnewdate11= date("d-M" , strtotime($arvdate11[0]));
																					$devdate1 = explode('T',$arrays1['Segments'][0][$seg_i]['Origin']['DepTime']);
																					$depnewdatedetail= date("d-M" , strtotime($devdate1[0]));
																					$depnewtimedetail = date("H:s" , strtotime($devdate1[1]));
																					?>
																				<div class="col-md-4">
																					<div class="flight-name">
																						<img src="{{$imgsou}}"
																							class="img-responsive flight-logo">
																						<p class="flight-name-heading">
																							{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName']}}
																						</p>
																						<p>{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode']}}
																							-
																							{{$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber']}}
																						</p>
																					</div>
																				</div>
																				<div class="col-md-4">
																					<div class="flight-depart">

																						<p class="flight-depart-text">
																							{{$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['CityName']}}
																							({{$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode']}})
																						</p>
																						<p
																							class="flight-depart-heading">
																							{{$depnewdatedetail}} |
																							{{$depnewtimedetail}} </p>

																					</div>
																				</div>
																				<div class="col-md-4">
																					<div class="flight-depart">

																						<p class="flight-depart-text">
																							{{$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['CityName']}}
																							({{$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode']}})
																						</p>
																						<p
																							class="flight-depart-heading">
																							{{$arvnewdate11}} |
																							{{$arvtime22}} </p>

																					</div>
																				</div>
																				<?php }
																					?>
																			</div>
																		</div>
																	</div>
																	<div id="menu1-{{$arrays1['ResultIndex']}}"
																		class="tab-pane fade">
																		<table class="table table-bordered">
																			<tbody>
																				<?php
																				$totalbasefare=0;
																				$totaltax=0;
																				$child='';
																				$Infant='';
																				for($fare_i=0;$fare_i<count($arrays1['FareBreakdown']);$fare_i++)
																				{
																					$totalbasefare +=$arrays1['FareBreakdown'][$fare_i]['BaseFare'];
																					$totaltax +=$arrays1['FareBreakdown'][$fare_i]['Tax'];
																					if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='1')
																					{
																						$adults=  $arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Adults';
																					}
																					if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='2' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
																					{
																						$child=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Child';
																					}
																					if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='3' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
																					{
																						$Infant=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Infant';
																					}
																				}
																				$totalfare= $arrays1['flight_base_fare'] + $arrays1['flight_base_tax'];
																				?>
																				<tr>
																					<td>Base Fare ({{$adults}}
																						{{$child}} {{$Infant}})</td>
																					<td><i
																							class="fa {{$arrays1['flightcurrency']}}"></i>
																						{{$arrays1['flight_base_fare']}}
																					</td>
																				</tr>
																				<tr>
																					<td>Taxes and Fees ({{$adults}}
																						{{$child}} {{$Infant}})</td>
																					<td><i
																							class="fa {{$arrays1['flightcurrency']}}"></i>
																						{{$arrays1['flight_base_tax']}}
																					</td>

																				</tr>
																				<tr>
																					<td>Total Fare (Roundoff)
																						({{$adults}} {{$child}}
																						{{$Infant}})</td>
																					<td><i
																							class="fa {{$arrays1['flightcurrency']}}"></i>
																						{{round($totalfare)}}</td>

																				</tr>
																			</tbody>
																		</table>
																	</div>
																	<div id="menu2-{{$arrays1['ResultIndex']}}"
																		class="tab-pane fade">
																		<div class="flight-list listflight">
																			<div class="row">
																				<?php
																					for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
																					{ ?>
																				<div class="col-md-4">
																					<div class="flight-name">
																						<img src="{{$imgsou}}"
																							class="img-responsive flight-logo">
																						<p class="flight-name-heading">
																							{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName']}}
																						</p>
																						<p>{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode']}}
																							-
																							{{$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber']}}
																						</p>
																					</div>
																				</div>
																				<table class="table table-bordered">
																					<tbody>
																						<tr>
																							<td>Baggage</td>
																							<td>{{$arrays1['Segments'][0][$seg_i]['Baggage']}}
																							</td>
																						</tr>
																						<tr>
																							<td>Cabin Baggage</td>
																							<td>7 Kg</td>
																						</tr>
																					</tbody>
																				</table>
																				<?php  }
																					?>
																			</div>
																		</div>
																	</div>
																	<div id="menu3-{{$arrays1['ResultIndex']}}"
																		class="tab-pane fade">
																		<h4>Terms & Conditions</h4>
																		<ul>
																			<li>Airlines stop accepting
																				cancellation/rescheduling requests 24 -
																				72 hours before departure of the flight,
																				depending on the airline. </li>
																			<li>The rescheduling/cancellation fee may
																				also vary based on fluctuations in
																				currency conversion rates.</li>
																			<li>Rescheduling Charges =
																				Rescheduling/Change Penalty + Fare
																				Difference (if applicable)</li>
																			<li>In case of restricted cases , no
																				amendments /cancellation allowed. </li>
																			<li>Airline penalty needs to be reconfirmed
																				prior to any amendments or cancellation.
																			</li>
																			<li>Disclaimer: Airline Penalty changes are
																				indicative and can change without prior
																				notice</li>
																			<li>NA means Not Available. Please check
																				with airline for penalty information.
																			</li>
																			<li>The charges are per passenger per
																				sector.</li>
																		</ul>
																	</div>
																</div>
															</div>

														</div>
														<?php } ?>
														@endforeach
													</div>
												</div>

											</div>
										</div>

										<div class="col-lg-6 pd-l-5" id="res-two">
											<div class="airlines-list">
												<div class="title-bar">
													<div class="row">
														<div class="col-md-2">
															<div class="bar-heading">
																<h5>Airline</h5>
															</div>
														</div>
														<div class="col-2 col-sm-3">
															<div class="bar-heading">
																<h5>Depart</h5>
															</div>
														</div>
														<div class="col-2 col-sm-3">
															<div class="bar-heading">
																<h5>Arrive</h5>
															</div>
														</div>
														<div class="col-md-2">
															<div class="bar-heading">
																<h5>Duration</h5>
															</div>
														</div>
														<div class="col-md-2">
															<div class="bar-heading">
																<h5>Price</h5>
															</div>
														</div>
														{{-- <div class="col-md-2 res-none">
															<div class="bar-heading">
																<h5>&nbsp;</h5>
															</div>
														</div> --}}
													</div>
												</div>
												<div class="showtable_return2">
													<div class="newtable" id="listflight">
														<?php
														if(!empty($resu[1]))
														{
														?>
														@foreach($resu[1] as $arrays1)
														<?php
															$timetotal=0;
															//depature time

															for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
															{
																$arvdate1 = explode('T',$arrays1['Segments'][0][$seg_i]['Destination']['ArrTime']);
																$arvtime2 = date("H:s" , strtotime($arvdate1[1]));
																$arvtime1 = date("H:i:s" , strtotime($arvdate1[1]));
																$arvnewdate1= date("d-M-y" , strtotime($arvdate1[0]));
																$arvnewdate1c= date("d-M" , strtotime($arvdate1[0]));
															}


															$arvnewdate1;
															$devdate = explode('T',$arrays1['Segments'][0][0]['Origin']['DepTime']);
															$depnewdate= date("d-M" , strtotime($devdate[0]));
															$depnewtime2 = date("H:s" , strtotime($devdate[1]));
															$depnewtime = date("H:i:s" , strtotime($devdate[1]));
															$newdepnewdate= date("Y-m-d" , strtotime($devdate[0]));

															//arrival time
															$arvdate = explode('T',$arrays1['Segments'][0][0]['Destination']['ArrTime']);
															$arvnewdate= date("d-M-y" , strtotime($arvdate[0]));
															$arvtime = date("H:s:i" , strtotime($arvdate[1]));
															$newarvnewdate= date("Y-m-d" , strtotime($arvnewdate1));
															$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newdepnewdate.''. $depnewtime);
															$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newarvnewdate.''. $arvtime1);

															$totalDuration = $from->diffInSeconds($to);
															$durationhour =  gmdate('H', $totalDuration);
															$durationmin =  gmdate('s', $totalDuration);

															$newtime=$durationhour.'H : '.$durationmin.'M';

															$imgsou =  'assets/images/flag/'.$arrays1['Segments'][0][0]['Airline']['AirlineCode'].'.gif';
															$stopcount = count($arrays1['Segments'][0]);
															$uniq = $arrays1['Segments'][0][0]['Airline']['AirlineName'];
															if(!empty($arrays1['Segments'][0][0]['NoOfSeatAvailable']))
															{
															$publishvaleu=round($arrays1['flight_fare'] );
															$mainvalue = number_format($publishvaleu);
															if($stopcount=='1')
															{
																$mainstopflight = 'Non Stop';
															}
															else
															{
																$stopcount1 = $stopcount-1;
																$mainstopflight = $stopcount1.' Stop';
															}
															?>

														<div class="flight-list listflight chepflight2"
															id="{{$arrays1['ResultIndex']}}">
															<input type="hidden" value="{{$publishvaleu}}"
																id="rpublishprice-{{$arrays1['ResultIndex']}}">
															<div class="row">
																<div class="col-md-2">
																	<div class="flight-name">
																		<img src="{{$imgsou}}"
																			class="img-responsive flight-logo"
																			id="rflightimg-{{$arrays1['ResultIndex']}}">
																		<p class="flight-name-heading"
																			id="rflightname-{{$arrays1['ResultIndex']}}">
																			{{$arrays1['Segments'][0][0]['Airline']['AirlineName']}}
																		</p>
																		<p id="rflightcode-{{$arrays1['ResultIndex']}}">
																			{{$arrays1['Segments'][0][0]['Airline']['AirlineCode']}}
																			-
																			{{$arrays1['Segments'][0][0]['Airline']['FlightNumber']}}
																		</p>
																	</div>
																</div>
																<div class="col-md-3">
																	<div class="flight-depart">
																		<p class="flight-depart-heading"
																			id="rflightdeptime-{{$arrays1['ResultIndex']}}">
																			{{$depnewtime2}} | {{$depnewdate}} </p>
																		<?php
																			for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
																			{
																				echo $flightroute = '<p class="flight-depart-text rflightroute-'.$arrays1['ResultIndex'].'">'.$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode'].' <i class="fa fa-plane fa-rotate-45"></i>    '.$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode'].'</p>';
																			}
																			?>
																	</div>
																</div>
																<div class="col-md-3">
																	<div class="flight-arrival">
																		<p class="flight-arrival-heading"
																			id="rflightarvtime-{{$arrays1['ResultIndex']}}">
																			{{$arvtime2}} | {{$arvnewdate1c}}</p>
																	</div>
																</div>
																<div class="col-md-2">
																	<div class="flight-duration">
																		<p class="flight-duration-heading">{{$newtime}}
																		</p>
																		<p style="color:#008cff">{{$mainstopflight}}</p>
																	</div>
																</div>
																<div class="col-md-2">
																	<div class="flight-price">
																		<p class="flight-price-heading"
																			id="rflightprice-{{$arrays1['ResultIndex']}}">
																			<i
																				class="fa {{$arrays1['flightcurrency']}}"></i>
																			{{$mainvalue}}</p>
																		<p>{{$arrays1['Segments'][0][0]['NoOfSeatAvailable']}}
																			Seats Left</p>
																	</div>
																</div>

															</div>
															<div class="flight-fare">
																<div class="row">
																	<div class="col-3">
																		<h5 class="fare-rules"
																			style="text-align: left !important;">Fare
																			Rules</h5>
																	</div>
																	
																	<div class="col-5 text-right">
																		@if($arrays1['IsRefundable'])
																		<h5 class="fare-type refundable"><i
																				class="fa fa-money"></i> Refundable</h5>
																		@else
																		<h5 class="fare-type non-refundable"><i
																				class="fa fa-money"></i> Non-Refundable
																		</h5>
																		@endif

																	</div>
																	<div class="col-md-4">
																		<div class="flight-book"
																			style="margin-top:-5px;text-align:right">
																			<!-- <form action="{{route('flightbook')}}" method="post">
																			 <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
																			<input type="hidden" name="resultindex" value="{{$arrays1['ResultIndex']}}">
																			<button class="btn flight-book-btn">Book</button>
																		</form> -->
																			<a href="javascript:void()"
																				class="details">Details</a>
																		</div>
																	</div>
																</div>
															</div>
															<div class="flight-details-tabs"
																id="flight-{{$arrays1['ResultIndex']}}">
																<ul class="nav nav-tabs">
																	<li><a data-toggle="tab"
																			href="#home-{{$arrays1['ResultIndex']}}"
																			class="active">Flight information</a></li>
																	<li><a data-toggle="tab"
																			href="#menu1-{{$arrays1['ResultIndex']}}">Fare
																			Details</a></li>
																	<li><a data-toggle="tab"
																			href="#menu2-{{$arrays1['ResultIndex']}}">Baggage
																			information</a></li>
																	<li><a data-toggle="tab"
																			href="#menu3-{{$arrays1['ResultIndex']}}">Cancellation
																			Rules</a></li>
																</ul>

																<div class="tab-content">
																	<div id="home-{{$arrays1['ResultIndex']}}"
																		class="tab-pane fade-in active">
																		<div class="flight-list listflight">
																			<div class="row">
																				<?php
																					for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
																					{
																					$arvdate11 = explode('T',$arrays1['Segments'][0][$seg_i]['Destination']['ArrTime']);
																					$arvtime22 = date("H:s" , strtotime($arvdate11[1]));
																					$arvnewdate11= date("d-M" , strtotime($arvdate11[0]));
																					$devdate1 = explode('T',$arrays1['Segments'][0][$seg_i]['Origin']['DepTime']);
																					$depnewdatedetail= date("d-M" , strtotime($devdate1[0]));
																					$depnewtimedetail = date("H:s" , strtotime($devdate1[1]));
																					?>
																				<div class="col-md-4">
																					<div class="flight-name">
																						<img src="{{$imgsou}}"
																							class="img-responsive flight-logo">
																						<p class="flight-name-heading">
																							{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName']}}
																						</p>
																						<p>{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode']}}
																							-
																							{{$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber']}}
																						</p>
																					</div>
																				</div>
																				<div class="col-md-4">
																					<div class="flight-depart">

																						<p class="flight-depart-text">
																							{{$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['CityName']}}
																							({{$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode']}})
																						</p>
																						<p
																							class="flight-depart-heading">
																							{{$depnewdatedetail}} |
																							{{$depnewtimedetail}} </p>

																					</div>
																				</div>
																				<div class="col-md-4">
																					<div class="flight-depart">

																						<p class="flight-depart-text">
																							{{$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['CityName']}}
																							({{$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode']}})
																						</p>
																						<p
																							class="flight-depart-heading">
																							{{$arvnewdate11}} |
																							{{$arvtime22}} </p>

																					</div>
																				</div>
																				<?php }
																					?>
																			</div>
																		</div>
																	</div>
																	<div id="menu1-{{$arrays1['ResultIndex']}}"
																		class="tab-pane fade">
																		<table class="table table-bordered">
																			<tbody>
																				<?php
																				$totalbasefare=0;
																				$totaltax=0;
																				$child='';
																				$Infant='';
																				for($fare_i=0;$fare_i<count($arrays1['FareBreakdown']);$fare_i++)
																				{
																					$totalbasefare +=$arrays1['FareBreakdown'][$fare_i]['BaseFare'];
																					$totaltax +=$arrays1['FareBreakdown'][$fare_i]['Tax'];
																					if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='1')
																					{
																						$adults=  $arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Adults';
																					}
																					if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='2' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
																					{
																						$child=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Child';
																					}
																					if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='3' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
																					{
																						$Infant=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Infant';
																					}
																				}
																				$totalfare= $arrays1['flight_base_fare'] + $arrays1['flight_base_tax'];
																				?>
																				<tr>
																					<td>Base Fare ({{$adults}}
																						{{$child}} {{$Infant}})</td>
																					<td><i
																							class="fa {{$arrays1['flightcurrency']}}"></i>
																						{{$arrays1['flight_base_fare']}}
																					</td>
																				</tr>
																				<tr>
																					<td>Taxes and Fees ({{$adults}}
																						{{$child}} {{$Infant}})</td>
																					<td><i
																							class="fa {{$arrays1['flightcurrency']}}"></i>
																						{{$arrays1['flight_base_tax']}}
																					</td>

																				</tr>
																				<tr>
																					<td>Total Fare (Roundoff)
																						({{$adults}} {{$child}}
																						{{$Infant}})</td>
																					<td><i
																							class="fa {{$arrays1['flightcurrency']}}"></i>
																						{{round($totalfare)}}</td>

																				</tr>
																			</tbody>
																		</table>
																	</div>
																	<div id="menu2-{{$arrays1['ResultIndex']}}"
																		class="tab-pane fade">
																		<div class="flight-list listflight">
																			<div class="row">
																				<?php
																					for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
																					{ ?>
																				<div class="col-md-4">
																					<div class="flight-name">
																						<img src="{{$imgsou}}"
																							class="img-responsive flight-logo">
																						<p class="flight-name-heading">
																							{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName']}}
																						</p>
																						<p>{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode']}}
																							-
																							{{$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber']}}
																						</p>
																					</div>
																				</div>
																				<table class="table table-bordered">
																					<tbody>
																						<tr>
																							<td>Baggage</td>
																							<td>{{$arrays1['Segments'][0][$seg_i]['Baggage']}}
																							</td>
																						</tr>
																						<tr>
																							<td>Cabin Baggage</td>
																							<td>7 Kg</td>
																						</tr>
																					</tbody>
																				</table>
																				<?php  }
																					?>
																			</div>
																		</div>
																	</div>
																	<div id="menu3-{{$arrays1['ResultIndex']}}"
																		class="tab-pane fade">
																		<h4>Terms & Conditions</h4>
																		<ul>
																			<li>Airlines stop accepting
																				cancellation/rescheduling requests 24 -
																				72 hours before departure of the flight,
																				depending on the airline. </li>
																			<li>The rescheduling/cancellation fee may
																				also vary based on fluctuations in
																				currency conversion rates.</li>
																			<li>Rescheduling Charges =
																				Rescheduling/Change Penalty + Fare
																				Difference (if applicable)</li>
																			<li>In case of restricted cases , no
																				amendments /cancellation allowed. </li>
																			<li>Airline penalty needs to be reconfirmed
																				prior to any amendments or cancellation.
																			</li>
																			<li>Disclaimer: Airline Penalty changes are
																				indicative and can change without prior
																				notice</li>
																			<li>NA means Not Available. Please check
																				with airline for penalty information.
																			</li>
																			<li>The charges are per passenger per
																				sector.</li>
																		</ul>
																	</div>
																</div>
															</div>


															<!-- <div class="flight-details-tabs">
                                                                    <div class="row">
                                                                        <ul class="nav nav-tabs">
                                                                            <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
                                                                            <li><a data-toggle="tab" href="#menu1">Menu 1</a></li>
                                                                            <li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
                                                                            <li><a data-toggle="tab" href="#menu3">Menu 3</a></li>
                                                                        </ul>

                                                                        <div class="tab-content">
                                                                            <div id="home" class="tab-pane fade in active">
                                                                              <h3>HOME</h3>
                                                                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                                            </div>
                                                                            <div id="menu1" class="tab-pane fade">
                                                                              <h3>Menu 1</h3>
                                                                              <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                                            </div>
                                                                            <div id="menu2" class="tab-pane fade">
                                                                              <h3>Menu 2</h3>
                                                                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                                                                            </div>
                                                                            <div id="menu3" class="tab-pane fade">
                                                                              <h3>Menu 3</h3>
                                                                              <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> -->
														</div>
														<?php } ?>
														@endforeach
														<?php } else{ ?>
														<!-- <div class="container">
															<div class="row">
																<div class="col-lg-12">
																	<div class="intro_content nano-content">
																		<div class="no-flight">
																			<h3>No Return  Flight Found</h3>
																			<a href="{{url('/index')}}" class="btn btn-back">Go Back</a>
																		</div>
																	</div>
																</div>
															</div>
														</div> -->
														<?php }?>
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>
								<?php }  else { ?>
								<!-- //international -->
								<div class="round-partition">
									<div class="row">
										<div class="col-lg-12 pd-r-5" id="res-two">
											<div class="airlines-list">
												<div class="title-bar">
													<div class="row">
														<div class="col-md-3">
															<div class="bar-heading">
																<h5>Airline</h5>
															</div>
														</div>
														<div class="col-md-3">
															<div class="bar-heading">
																<h5>Depart</h5>
															</div>
														</div>
														<div class="col-md-3">
															<div class="bar-heading">
																<h5>Arrive</h5>
															</div>
														</div>
														<div class="col-md-3">
															<div class="bar-heading">
																<h5>Duration</h5>
															</div>
														</div>

													</div>
												</div>
												<div class="showtable_returninter">
													<div class="newtable" id="listflight">

														@foreach($resu[0] as $arrays1)
														<?php
															$timetotal=0;

															//depature time

															//internation

															//endinternation

															$publishvaleu=round($arrays1['flight_fare'] );
															$mainvalue = number_format($publishvaleu);

															?>

														<div class="flight-list listflight chepflight1"
															id="{{$arrays1['ResultIndex']}}">
															<input type="hidden" value="{{$publishvaleu}}"
																id="publishprice-{{$arrays1['ResultIndex']}}">
															<?php
																for($seg_new=0;$seg_new<count($arrays1['Segments']);$seg_new++)
																{
																for($seg_i=0;$seg_i<count($arrays1['Segments'][$seg_new]);$seg_i++)
																{
																	$arvdate1 = explode('T',$arrays1['Segments'][$seg_new][$seg_i]['Destination']['ArrTime']);
																	$arvtime2 = date("H:s" , strtotime($arvdate1[1]));
																	$arvtime1 = date("H:i:s" , strtotime($arvdate1[1]));
																	$arvnewdate1= date("d-M-y" , strtotime($arvdate1[0]));
																	$arvnewdatec= date("d-M" , strtotime($arvdate1[0]));
																}
																$devdate = explode('T',$arrays1['Segments'][$seg_new][0]['Origin']['DepTime']);
																$depnewdate= date("d-M" , strtotime($devdate[0]));
																$depnewtime2 = date("H:s" , strtotime($devdate[1]));
																$depnewtime = date("H:i:s" , strtotime($devdate[1]));
																$newdepnewdate= date("Y-m-d" , strtotime($devdate[0]));

																//arrival time
																$arvdate = explode('T',$arrays1['Segments'][$seg_new][0]['Destination']['ArrTime']);
																$arvnewdate= date("d-M-y" , strtotime($arvdate[0]));
																$arvtime = date("H:s:i" , strtotime($arvdate[1]));
																$newarvnewdate= date("Y-m-d" , strtotime($arvnewdate1));
																$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newdepnewdate.''. $depnewtime);
																$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $newarvnewdate.''. $arvtime1);

																$totalDuration = $from->diffInSeconds($to);
																$durationhour =  gmdate('H', $totalDuration);
																$durationmin =  gmdate('s', $totalDuration);

																$newtime=$durationhour.'H : '.$durationmin.'M';

																$imgsou =  'assets/images/flag/'.$arrays1['Segments'][$seg_new][0]['Airline']['AirlineCode'].'.gif';
																$stopcount = count($arrays1['Segments'][$seg_new]);
																$uniq = $arrays1['Segments'][$seg_new][0]['Airline']['AirlineName'];
																if($stopcount=='1')
																{
																	$mainstopflight = 'Non Stop';
																}
																else
																{
																	$stopcount1 = $stopcount-1;
																	$mainstopflight = $stopcount1.' Stop';
																}
																?>
															<hr style="width: 100%">
															<div class="row">
																<div class="col-md-3">
																	<div class="flight-name">
																		<img src="{{$imgsou}}"
																			class="img-responsive flight-logo"
																			id="flightimg-{{$arrays1['ResultIndex']}}">
																		<p class="flight-name-heading chkflight"
																			id="flightname-{{$arrays1['ResultIndex']}}">
																			{{$arrays1['Segments'][$seg_new][0]['Airline']['AirlineName']}}
																		</p>
																		<p id="flightcode-{{$arrays1['ResultIndex']}}">
																			{{$arrays1['Segments'][$seg_new][0]['Airline']['AirlineCode']}}
																			-
																			{{$arrays1['Segments'][$seg_new][0]['Airline']['FlightNumber']}}
																		</p>
																	</div>
																</div>
																<div class="col-md-3">
																	<div class="flight-depart">
																		<p class="flight-depart-heading"
																			id="flightdeptime-{{$arrays1['ResultIndex']}}">

																			{{$depnewtime2}} | {{$depnewdate}} </p>

																		<?php
																			for($seg_i=0;$seg_i<count($arrays1['Segments'][$seg_new]);$seg_i++)
																			{
																				echo $flightroute = '<p class="flight-depart-text flightroute-'.$arrays1['ResultIndex'].'" id="flightroute-'.$arrays1['ResultIndex'].'">'.$arrays1['Segments'][$seg_new][$seg_i]['Origin']['Airport']['AirportCode'].' <i class="fa fa-plane fa-rotate-45"></i>    '.$arrays1['Segments'][$seg_new][$seg_i]['Destination']['Airport']['AirportCode'].'</p>';
																			}
																			?>
																	</div>
																</div>
																<div class="col-md-3">
																	<div class="flight-arrival">
																		<p class="flight-arrival-heading"
																			id="flightarvtime-{{$arrays1['ResultIndex']}}">
																			{{$arvtime2}} | {{$arvnewdatec}}</p>
																	</div>
																</div>
																<div class="col-md-3">
																	<div class="flight-duration">
																		<p class="flight-duration-heading">{{$newtime}}
																		</p>
																		<p style="color:#008cff">{{$mainstopflight}}</p>
																	</div>
																</div>
																<!-- <div class="col-md-2">
																	<div class="flight-price">
																		<p class="flight-price-heading" id="flightprice-{{$arrays1['ResultIndex']}}"><i class="fa {{$arrays1['flightcurrency']}}"></i> {{$mainvalue}}</p>
																		<p> </p>
																	</div>
																</div> -->

															</div>
															<?php }

																?>

															<div class="row">
																<div class="col-md-12">
																	<div class="flight-book">
																		<form action="{{route('flightbook')}}"
																			method="post">
																			<input name="_token" type="hidden"
																				value="{{ csrf_token() }}" />
																			<input type="hidden" id="cheapresultindex"
																				name="resultindex"
																				value="{{$arrays1['ResultIndex']}}">
																			<input type="hidden" name="flightcabinclass"
																				value="{{$mainarray['FlightCabinName']}}">
																			<input type="hidden" name="journytype"
																				value="{{$mainarray['JourneyType']}}">
																			<input type="hidden" name="flightorign"
																				id="flightorign"
																				value="{{$mainarray['flightorign']}}">
																			<input type="hidden" name="flightdep"
																				id="flightdep"
																				value="{{$mainarray['flightdep']}}">
																			<input type="hidden" name="flightname"
																				value="{{$resu[0][0]['Segments'][0][0]['Airline']['AirlineName']}}">

																			<button class="btn flight-book-btn"
																				style="margin-left: auto;display: block; float: right">Book</button>
																		</form>
																		<a href="javascript:void()" class="details"
																			id="{{$arrays1['ResultIndex']}}"
																			style="display: block;text-align: right;padding-right: 5px;padding-top: 8px;float:right; margin-right: 15px;font-size: 14px">Details</a>
																		<p style="float:right; display: block; margin: 9px 20px"
																			class="flight-price-heading"
																			id="flightprice-{{$arrays1['ResultIndex']}}">
																			<i
																				class="fa {{$arrays1['flightcurrency']}}"></i>
																			{{$mainvalue}}</p>

																	</div>
																</div>
															</div>






															<div class="flight-fare">
																<div class="row">
																	<div class="col-md-3">
																		<h5 class="fare-rules">Fare Rules</h5>
																	</div>
																	<div class="col-md-5">
																		<!-- <h5 class="airline-mark">Airline Mark : <span> {{$arrays1['AirlineRemark']}}</span></h5> -->
																	</div>
																	<div class="col-md-4">
																		@if($arrays1['IsRefundable'])
																		<h5 class="fare-type refundable"><i
																				class="fa fa-money"></i> Refundable</h5>
																		@else
																		<h5 class="fare-type non-refundable"><i
																				class="fa fa-money"></i> Non-Refundable
																		</h5>
																		@endif

																	</div>
																</div>
															</div>
															<div class="flight-details-tabs"
																id="flight-{{$arrays1['ResultIndex']}}">
																<ul class="nav nav-tabs">
																	<li><a data-toggle="tab"
																			href="#home-{{$arrays1['ResultIndex']}}"
																			class="active">Flight information</a></li>
																	<li><a data-toggle="tab"
																			href="#menu1-{{$arrays1['ResultIndex']}}">Fare
																			Details</a></li>
																	<li><a data-toggle="tab"
																			href="#menu2-{{$arrays1['ResultIndex']}}">Baggage
																			information</a></li>
																	<li><a data-toggle="tab"
																			href="#menu3-{{$arrays1['ResultIndex']}}">Cancellation
																			Rules</a></li>
																</ul>

																<div class="tab-content">
																	<div id="home-{{$arrays1['ResultIndex']}}"
																		class="tab-pane fade-in active">
																		<div class="flight-list listflight">
																			<div class="row">
																				<?php
																					for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
																					{
																					$arvdate11 = explode('T',$arrays1['Segments'][0][$seg_i]['Destination']['ArrTime']);
																					$arvtime22 = date("H:s" , strtotime($arvdate11[1]));
																					$arvnewdate11= date("d-M" , strtotime($arvdate11[0]));
																					$devdate1 = explode('T',$arrays1['Segments'][0][$seg_i]['Origin']['DepTime']);
																					$depnewdatedetail= date("d-M" , strtotime($devdate1[0]));
																					$depnewtimedetail = date("H:s" , strtotime($devdate1[1]));
																					$imgsou =  'assets/images/flag/'.$arrays1['Segments'][0][0]['Airline']['AirlineCode'].'.gif';
																					?>
																				<div class="col-md-4">
																					<div class="flight-name">
																						<img src="{{$imgsou}}"
																							class="img-responsive flight-logo">
																						<p class="flight-name-heading">
																							{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName']}}
																						</p>
																						<p>{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode']}}
																							-
																							{{$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber']}}
																						</p>
																					</div>
																				</div>
																				<div class="col-md-4">
																					<div class="flight-depart">

																						<p class="flight-depart-text">
																							{{$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['CityName']}}
																							({{$arrays1['Segments'][0][$seg_i]['Origin']['Airport']['AirportCode']}})
																						</p>
																						<p
																							class="flight-depart-heading">
																							{{$depnewdatedetail}} |
																							{{$depnewtimedetail}} </p>

																					</div>
																				</div>
																				<div class="col-md-4">
																					<div class="flight-depart">

																						<p class="flight-depart-text">
																							{{$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['CityName']}}
																							({{$arrays1['Segments'][0][$seg_i]['Destination']['Airport']['AirportCode']}})
																						</p>
																						<p
																							class="flight-depart-heading">
																							{{$arvnewdate11}} |
																							{{$arvtime22}} </p>

																					</div>
																				</div>
																				<?php }
																					?>
																				<?php
																					for($seg_i=0;$seg_i<count($arrays1['Segments'][1]);$seg_i++)
																					{
																					$arvdate11 = explode('T',$arrays1['Segments'][1][$seg_i]['Destination']['ArrTime']);
																					$arvtime22 = date("H:s" , strtotime($arvdate11[1]));
																					$arvnewdate11= date("d-M" , strtotime($arvdate11[1]));
																					$devdate1 = explode('T',$arrays1['Segments'][1][$seg_i]['Origin']['DepTime']);
																					$depnewdatedetail= date("d-M" , strtotime($devdate1[1]));
																					$depnewtimedetail = date("H:s" , strtotime($devdate1[1]));
																					$imgsou =  'assets/images/flag/'.$arrays1['Segments'][1][0]['Airline']['AirlineCode'].'.gif';
																					?>
																				<div class="col-md-4">
																					<div class="flight-name">
																						<img src="{{$imgsou}}"
																							class="img-responsive flight-logo">
																						<p class="flight-name-heading">
																							{{$arrays1['Segments'][1][$seg_i]['Airline']['AirlineName']}}
																						</p>
																						<p>{{$arrays1['Segments'][1][$seg_i]['Airline']['AirlineCode']}}
																							-
																							{{$arrays1['Segments'][1][$seg_i]['Airline']['FlightNumber']}}
																						</p>
																					</div>
																				</div>
																				<div class="col-md-4">
																					<div class="flight-depart">

																						<p class="flight-depart-text">
																							{{$arrays1['Segments'][1][$seg_i]['Origin']['Airport']['CityName']}}
																							({{$arrays1['Segments'][1][$seg_i]['Origin']['Airport']['AirportCode']}})
																						</p>
																						<p
																							class="flight-depart-heading">
																							{{$depnewdatedetail}} |
																							{{$depnewtimedetail}} </p>

																					</div>
																				</div>
																				<div class="col-md-4">
																					<div class="flight-depart">

																						<p class="flight-depart-text">
																							{{$arrays1['Segments'][1][$seg_i]['Destination']['Airport']['CityName']}}
																							({{$arrays1['Segments'][1][$seg_i]['Destination']['Airport']['AirportCode']}})
																						</p>
																						<p
																							class="flight-depart-heading">
																							{{$arvnewdate11}} |
																							{{$arvtime22}} </p>

																					</div>
																				</div>
																				<?php }
																					?>
																			</div>
																		</div>
																	</div>
																	<div id="menu1-{{$arrays1['ResultIndex']}}"
																		class="tab-pane fade">
																		<table class="table table-bordered">
																			<tbody>
																				<?php
																				$totalbasefare=0;
																				$totaltax=0;
																				$child='';
																				$Infant='';
																				for($fare_i=0;$fare_i<count($arrays1['FareBreakdown']);$fare_i++)
																				{
																					$totalbasefare +=$arrays1['FareBreakdown'][$fare_i]['BaseFare'];
																					$totaltax +=$arrays1['FareBreakdown'][$fare_i]['Tax'];
																					if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='1')
																					{
																						$adults=  $arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Adults';
																					}
																					if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='2' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
																					{
																						$child=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Child';
																					}
																					if($arrays1['FareBreakdown'][$fare_i]['PassengerType']=='3' && !empty($arrays1['FareBreakdown'][$fare_i]['PassengerType']))
																					{
																						$Infant=  ',' .$arrays1['FareBreakdown'][$fare_i]['PassengerCount']. ' Infant';
																					}
																				}
																				$totalfare= $arrays1['flight_base_fare'] + $arrays1['flight_base_tax'];
																				?>
																				<tr>
																					<td>Base Fare ({{$adults}}
																						{{$child}} {{$Infant}})</td>
																					<td><i
																							class="fa {{$arrays1['flightcurrency']}}"></i>
																						{{$arrays1['flight_base_fare']}}
																					</td>
																				</tr>
																				<tr>
																					<td>Taxes and Fees ({{$adults}}
																						{{$child}} {{$Infant}})</td>
																					<td><i
																							class="fa {{$arrays1['flightcurrency']}}"></i>
																						{{$arrays1['flight_base_tax']}}
																					</td>

																				</tr>
																				<tr>
																					<td>Total Fare (Roundoff)
																						({{$adults}} {{$child}}
																						{{$Infant}})</td>
																					<td><i
																							class="fa {{$arrays1['flightcurrency']}}"></i>
																						{{round($totalfare)}}</td>

																				</tr>
																			</tbody>
																		</table>
																	</div>
																	<div id="menu2-{{$arrays1['ResultIndex']}}"
																		class="tab-pane fade">
																		<div class="flight-list listflight">
																			<div class="row">
																				<?php
																					for($seg_i=0;$seg_i<count($arrays1['Segments'][0]);$seg_i++)
																					{ ?>
																				<div class="col-md-4">
																					<div class="flight-name">
																						<img src="{{$imgsou}}"
																							class="img-responsive flight-logo">
																						<p class="flight-name-heading">
																							{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineName']}}
																						</p>
																						<p>{{$arrays1['Segments'][0][$seg_i]['Airline']['AirlineCode']}}
																							-
																							{{$arrays1['Segments'][0][$seg_i]['Airline']['FlightNumber']}}
																						</p>
																					</div>
																				</div>
																				<table class="table table-bordered">
																					<tbody>
																						<tr>
																							<td>Baggage</td>
																							<td>{{$arrays1['Segments'][0][$seg_i]['Baggage']}}
																							</td>
																						</tr>
																						<tr>
																							<td>Cabin Baggage</td>
																							<td>7 Kg</td>
																						</tr>
																					</tbody>
																				</table>
																				<?php  }
																					?>
																			</div>
																		</div>
																	</div>
																	<div id="menu3-{{$arrays1['ResultIndex']}}"
																		class="tab-pane fade">
																		<h4>Terms & Conditions</h4>
																		<ul>
																			<li>Airlines stop accepting
																				cancellation/rescheduling requests 24 -
																				72 hours before departure of the flight,
																				depending on the airline. </li>
																			<li>The rescheduling/cancellation fee may
																				also vary based on fluctuations in
																				currency conversion rates.</li>
																			<li>Rescheduling Charges =
																				Rescheduling/Change Penalty + Fare
																				Difference (if applicable)</li>
																			<li>In case of restricted cases , no
																				amendments /cancellation allowed. </li>
																			<li>Airline penalty needs to be reconfirmed
																				prior to any amendments or cancellation.
																			</li>
																			<li>Disclaimer: Airline Penalty changes are
																				indicative and can change without prior
																				notice</li>
																			<li>NA means Not Available. Please check
																				with airline for penalty information.
																			</li>
																			<li>The charges are per passenger per
																				sector.</li>
																		</ul>
																	</div>
																</div>
															</div>

														</div>



														@endforeach
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>
								<?php
								} //internationflight close if
								} //mainarrray loop
								?>
							</div>
							<div class="col-lg-3" id="res-one">
								<div class="flight-filter">
									<h3>Sort & Filter</h3>
									<div class="filter">
										<h4><i class="fa {{$arrays1['flightcurrency']}}"></i>&nbsp; Price</h4>
										<div class="inner-filter">
											<?php
											$filprice=array();
											$currency="";
											foreach($resu[0] as $arrays1)
											{

												$filprice[]=round($arrays1['flight_fare']);

											}

											$minprice1=min($filprice);

											$maxprice1=max($filprice);

											$mainprice = $maxprice1 - $minprice1;
											$minprice = $minprice1;
											$minagain = $minprice;
											$mainprice1 = $mainprice/3	;
											for($ipt=0;$ipt<3;$ipt++)
											{

											$maxprice = $minagain+$mainprice1;
											$minprice11 = $minagain;
											$minagain = $maxprice+1;



											?>
											<label class="checkcontainer"><i
													class="fa {{$arrays1['flightcurrency']}}"></i><?php echo round($minprice11);?>
												- <i class="fa {{$arrays1['flightcurrency']}}"></i>
												<?php echo round($maxprice);?>
												<input type="checkbox" class="priceable" id="priceable1"
													value="<?php echo round($minprice11);?>-<?php echo round($maxprice);?>">
												<span class="checkmark"></span>
											</label>

											<?php } ?>
										</div>
									</div>
									<div class="filter">
										<h4><i class="fa fa-clock-o"></i>&nbsp; Time</h4>
										<div class="inner-filter">
											<label class="checkcontainer">Morning (00:00 - 11:00)
												<input type="checkbox" class="timeable" id="timeflight"
													value="00:00-11:00" name="timeflight[]">
												<span class="checkmark"></span>
											</label>
											<label class="checkcontainer">Afternoon (11:00 - 16:00)
												<input type="checkbox" class="timeable" id="timeflight1"
													value="11:00-16:00" name="timeflight[]">
												<span class="checkmark"></span>
											</label>
											<label class="checkcontainer">Evening (16:00 - 21:00)
												<input type="checkbox" class="timeable" id="timeflight2"
													value="16:00-21:00" name="timeflight[]">
												<span class="checkmark"></span>
											</label>
											<label class="checkcontainer"> Night (21:00 - 00:00 )
												<input type="checkbox" class="timeable" id="timeflight3"
													value="21:00-23:59" name="timeflight[]">
												<span class="checkmark"></span>
											</label>
										</div>
									</div>
									<div class="filter">
										<h4><i class="fa fa-map-marker"></i>&nbsp; Fare Type</h4>
										<div class="inner-filter">
											<label class="checkcontainer">Non-Refundable
												<input type="checkbox" class="refundable" id="nonrefundableflight"
													value="non_refundable" name="refundableflight[]">
												<span class="checkmark"></span>
											</label>
											<label class="checkcontainer">Refundable
												<input type="checkbox" class="refundable" id="refundableflight"
													value="refundable" name="refundableflight[]">
												<span class="checkmark"></span>
											</label>
										</div>
									</div>
									<div class="filter">
										<h4><i class="fa fa-map-marker"></i>&nbsp; Stop</h4>
										<div class="inner-filter">
											<label class="checkcontainer">Multi Stop
												<input type="checkbox" class="stopflight" id="nonstopflight" value="1"
													name="stopflight[]">
												<span class="checkmark"></span>
											</label>
											<label class="checkcontainer">Non Stop
												<input type="checkbox" class="stopflight" id="stopflight" value="2"
													name="stopflight[]">
												<span class="checkmark"></span>
											</label>
										</div>
									</div>
									<div class="filter">

										<h4><i class="fa fa-plane"></i>&nbsp; Airlines</h4>
										<div class="inner-filter">

											@foreach ($unique as $flightsearch)

											<label class="checkcontainer">{{$flightsearch['name']}}
												<input type="checkbox" class="flightsearch" name="flightsearch[]"
													value="{{$flightsearch['code']}}">
												<span class="checkmark"></span>
											</label>
											@endforeach

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>



					<style>
						button.view-h-btn {
							display: block;
							width: auto;
							margin: 50px auto 0;
							padding: 15px 31px;
							background: #00206a;
							border: none;
							color: white;
						}

						.slider-div {
							margin-left: 30px;
							margin-right: 30px;
						}

						span.hs-price {
							position: absolute;
							top: 15px;
							left: 33px;
							color: white;
							font-size: 16px;
							font-weight: 700;
						}

						a.ind-btn {
							position: absolute;
							top: 50%;

							transform: translateY(-50%);
							color: black;
							background: whitesmoke;
							padding: 17px 16px;
							font-size: 20px;
							font-weight: 900 !important;
						}

						a.ind-btn:hover {
							background: #FF9800;
							color: white;
						}

						.right-btn {
							right: -99px;
							position: absolute;
						}

						.flight-details {
							padding: 30px 0px 0 !important;
						}

						h3.h-head {
							color: #00206a;
							font-weight: 600;
							text-align: center;
							font-size: 35px;
							margin: -20px 0 66px;
						}

						.hotel-div {
							height: 210px;
							min-height: 210px;
							border-radius: 5px;
							border: 1px solid #FF9800;
						}

						button.b-btn {
							border: none;
							background: #FF9800;
							color: white;
							padding: 7px 10px;
							border-radius: 5px;
							display: block;
							margin-left: auto;
						}

						.r-star {
							position: relative;
							top: 30px;
							left: 11px;
						}

						h2.h2-head {
							font-size: 19px;
							margin-bottom: 0;
							color: white;
							position: relative;
							top: 29px;
							left: 11px;
						}

						span.fa.fa-star.checked {
							color: #FFC107;
						}

						span.fa.fa-star {
							color: #ffffff;
						}

						button.b-btn {
							border: none;
							background: #FF9800;
							color: white;
							padding: 7px 10px;
							border-radius: 5px;
							display: block;
							margin-left: auto;
						}

						button.b-btn:hover {

							background: #00206A;

						}

						.book-btn-div {
							position: relative;
							top: 115px;
							right: 10px;
							border: none;
						}

						img.h-img {
							width: 100%;
							position: relative;
							height: 100%;
							border-radius: 5px;
						}

						.over-content {
							position: absolute;
							top: 0px;
							width: 100%;
							left: 0px;
							background: #00000085;
							height: 100%;
							border-radius: 5px;
						}

						span.hotel-name-tag {
							color: white;
							background: #E91E63;
							padding: 6px 10px;
							/* top: 10px; */
							position: relative;
							top: 17px;
							left: 10px;
							border-radius: 5px;
						}

						@media screen and (max-width:1280px) {
							.col-sm-6.col-md-3.slides-res {
								flex: 0 0 45%;
								max-width: 40%;
								margin: 0 auto 45px auto;
							}
						}

						@media screen and (max-width:760px) {
							.col-sm-6.col-md-3.slides-res {
								flex: 0 0 50%;
								max-width: 50%;
								margin: 0 auto 45px auto;
							}

							button.view-h-btn {

								margin: 0 auto 0;

							}

						}

						@media screen and (max-width:650px) {
							.col-sm-6.col-md-3.slides-res {
								flex: 0 0 100%;
								max-width: 100%;
								margin: 0 auto 45px auto;
							}

						}
					</style>

				</section>
				@else
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="intro_content nano-content">
								<div class="no-flight">
									<h3>No Flight Found</h3>
									<a href="{{url('/index')}}" class="btn btn-back">Go Back</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endif
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade myModaln1" id="myModaln1" data-backdrop="static" data-keyboard="false" role="dialog"
		style="background-color: #f0f8ffd1;">
		<div class="modal-dialog">
			<center><img src="{{ asset('assets/images/newf.gif')   }}"
					style="height: auto;width: auto; display: block;margin:60% auto;position: relative;top:50%;transform: translateY(-50%)">
			</center>
			<center>Please Wait</center>
		</div>
	</div>


	<!--<div class='session-counter'>-->
	<!--	<p class='timer' data-minutes-left=15>Your booking session will expire in </p>-->
	<!--	<section class='actions'></section>-->
	<!--</div>-->
	<style>
		.jst-hours,
		.jst-minutes,
		.jst-seconds {
			display: inline;
			color: orange;
		}

		.flight-list:hover {
			background: #ffece0;
		}

		.flight-list:active {
			background: #ffece0;
		}

		.a-select {
			background: #ffece0;
		}

		.session-counter {
			background: rgba(0, 0, 0, 0.72);
			color: white;
			padding: 5px;
			position: fixed;
			bottom: 0;
			display: block;
			width: 100%;
			z-index: 1;
		}

		.session-counter p {
			margin: 0;
			color: white;
			font-size: 17px;
			font-weight: 500;
			text-align: center;
			z-index: 1;
		}

		.multi .carousel-inner .active.left {
			left: -25%;
		}

		.multi .carousel-inner .next {
			left: 25%;
		}

		.multi .carousel-inner .prev {
			left: -25%;
		}

		.multi .carousel-control {
			width: 4%;
		}

		.multi .carousel-control.left,
		.multi .carousel-control.right {
			margin-left: 15px;
			background-image: none;
		}

		.nano-content.no-filter-header h3 {
			font-size: 17px;
			color: #fa9e1b;
			font-weight: 700;
			text-transform: uppercase;
			background: white;
			padding: 15px;
			border-radius: 41px;
			font-size: 25px !IMPORTANT;
			width: 50%;
			margin: 0 auto 30px;
			display: block;
		}

		p.sub-para {
			font-size: 24px;
			color: #fa9e1b;
			font-weight: 700;
			text-transform: uppercase;
			text-align: center;
		}
	</style>
	<!-- Footer -->
	@include('pages.include.footer')
	<!-- Copyright -->
	@include('pages.include.copyright')
	<script>
		var slideIndex = 1;
		showSlides(slideIndex);

		// Next/previous controls
		function plusSlides(n) {
			showSlides(slideIndex += n);
		}

		// Thumbnail image controls
		function currentSlide(n) {
			showSlides(slideIndex = n);
		}

		function showSlides(n) {
			var i;
			var slides = document.getElementsByClassName("slides-res");


			if (n > slides.length) {
				slideIndex = 1
			}
			if (n < 1) {
				slideIndex = slides.length
			}
			for (i = 0; i < slides.length - 3; i++) {
				slides[i].style.display = "none";
			}

			slides[slideIndex - 1].style.display = "block";

		}
	</script>
	<script>
		//refunable
		$(document).on('click', '.refundable', function () {
			//console.log(refunable);
			flter_data();
		});

		$(document).on('click', '.stopflight', function () {
			flter_data();
		});

		$(document).on('click', '.flightsearch', function () {
			flter_data();
		});

		$(document).on('click', '.priceable', function () {
			flter_data();
		});

		$(document).on('click', '.timeable', function () {
			flter_data();
		});

		function flter_data() {

			var refunable = $('.refundable:checked').map(function () {
				return this.value
			}).get().join(',');
			var price = $('.priceable:checked').map(function () {
				return this.value
			}).get().join(',');
			var time = $('.timeable:checked').map(function () {
				return this.value
			}).get().join(',');
			var stops = $('.stopflight:checked').map(function () {
				return this.value
			}).get().join(',');
			var airlines = $('.flightsearch:checked').map(function () {
				return this.value
			}).get().join(',');
			var journytype = $('#journytype').val();
			var flightcabinclass = $('#FlightCabinName').val();
			var flightorign = $('#flightorign').val();
			var flightdep = $('#flightdep').val();
			$('#myModaln1').modal('show');
			$.ajax({
				url: "{{route('checkrefunable')}}",
				data: {
					'refund': refunable,
					'price': price,
					'time': time,
					'stops': stops,
					'airlines': airlines,
					'journytype': journytype,
					'flightcabinclass': flightcabinclass,
					'flightorign': flightorign,
					'flightdep': flightdep,
				},
				type: 'GET',
				dataType: "json",
				success: function (data) {

					if (data.value == null || (data.value == null && (data.hasOwnProperty('value_intrreturn') &&
							data.value_intrreturn == null)) || (data.value == null && (data.hasOwnProperty(
							'value1') && data.value1 == null))) {
						if (journytype == '1') {
							$('.showtable').html(
								'<div class="container"><div class="row"><div class="col-lg-12"><div class="intro_content nano-content no-filter-header"><div class="no-flight"><h3 >No Flights Found </h3><p class="sub-para"> Try a different combination of filters</p></div></div></div></div></div>'
								);
						} else if (journytype == '3') {
							$('.showtable2').html(
								'<div class="container"><div class="row"><div class="col-lg-12"><div class="intro_content nano-content no-filter-header"><div class="no-flight"><h3 >No Flights Found </h3><p class="sub-para"> Try a different combination of filters</p></div></div></div></div></div>'
								);
						} else if (journytype == '2') {
							if (data.value_intrreturn == null) {
								$('.showtable_return1').html(
									'<div class="container"><div class="row"><div class="col-lg-12"><div class="intro_content nano-content no-filter-header"><div class="no-flight"><h3 >No Flights Found </h3><p class="sub-para"> Try a different combination of filters</p></div></div></div></div></div>'
									);
								$('.showtable_return2').html(
									'<div class="container"><div class="row"><div class="col-lg-12"><div class="intro_content nano-content no-filter-header"><div class="no-flight"><h3 >No Flights Found </h3><p class="sub-para"> Try a different combination of filters</p></div></div></div></div></div>'
									);

							} else {

								$('.showtable_returninter').html(
									'<div class="container"><div class="row"><div class="col-lg-12"><div class="intro_content nano-content no-filter-header"><div class="no-flight"><h3 >No Flights Found </h3><p class="sub-para"> Try a different combination of filters</p></div></div></div></div></div>'
									);
							}
						}
						setTimeout(function () {
							$('#myModaln1').modal('hide');
						}, 500);
						$("html, body").animate({
							scrollTop: 0
						}, "slow");
					} else {
						if (journytype == '1') {
							$('.showtable').html(data.value);
							$('#listflight').hide();

						} else if (journytype == '3') {
							$('.showtable2').html(data.value);
							$('#listflight').hide();
						} else if (journytype == '2') {
							if (data.value_intrreturn == null) {
								$('.showtable_return1').html(data.value);
								$('.showtable_return2').html(data.value1);
								$('#listflight').hide();


							} else {

								$('.showtable_returninter').html(data.value_intrreturn);
								$('#listflight').hide();


							}

						} else {

						}

						setTimeout(function () {
							$('#myModaln1').modal('hide')
						}, 500);
						$("html, body").animate({
							scrollTop: 0
						}, "slow");
					}

				}
			});
		}
	</script>

	<script>
		$(document).on('click', '.details', function () {
			var detailid = this.id;
			$("#flight-" + detailid).toggle();

		});
		$(document).ready(function () {
			// $(".flight-book .details").click(function(){
			// 	var detailid = this.id;
			// alert(detailid);
			//   $("#flight-"+detailid).toggle();
			// });
			$(".btn-modify").click(function () {
				$(".result-modify").toggle();
			});
		});
	</script>
	<script>
		$(function () {
			//Departure
			var date = new Date();
			date.setDate(date.getDate());
			$('.departure').datepicker({
				autoclose: true,
				todayHighlight: true,
				format: 'dd/mm/yyyy',
				startDate: date

			});
			//Date picker
			$('#return').datepicker({
				autoclose: true,
				todayHighlight: true,
				format: 'dd/mm/yyyy',
				startDate: date
			});
			//Check In
			$('#check-in').datepicker({
				autoclose: true,
				todayHighlight: true,
				format: 'dd/mm/yyyy',
				startDate: date
			});
			//Date picker
			$('#check-out').datepicker({
				autoclose: true,
				todayHighlight: true,
				format: 'dd/mm/yyyy',
				startDate: date
			});

		});
	</script>
	<script>
		$("input[name='one']").change(function () {
			if ($("input[name='one']:checked").val() == '1') {
				$('#restuntype').hide();
				$('.multicity-main').hide();
			} else if ($("input[name='one']:checked").val() == '2') {
				$('#restuntype').show();
				$('.multicity-main').hide();
			} else if ($("input[name='one']:checked").val() == '3') {
				$('.multicity-main').show();
				$('#restuntype').hide();
			} else {
				$('.multicity-main').hide();
				$('#restuntype').hide();
			}
		});
	</script>
	<script>
		//SCRIPTS FOR BOOKING AREA
		$("#autocomplete1").on('keyup', function () {
			if ($("#autocomplete").val().toLowerCase() == $(this).val().toLowerCase()) {
				alert("From & To airports cannot be the same");
			}
		});
		$("#search_form_2").submit(function (e) {


			var depdate = $("#departure").val();
			var rettype = $('.returnchk').val();

			var dateOne = new Date(depdate); //Year, Month, Date  
			var dateTwo = new Date(rettype); //Year, Month, Date

			var error = 0;
			if ($("#autocomplete").val().trim() == "") {
				error++;
				alert("Please Enter From Airport");
			}
			if ($("#autocomplete1").val().trim() == "") {
				error++;
				alert("Please Enter Departure Date");
			}
			if ($("#departure").val().trim() == "") {
				error++;
				alert("Please Enter Departure Date");
			}
			if ($("input[name='one']:checked").val() == '2' && $('.returnchk').val().trim() == '') {
				error++;
				alert("Please Enter Return Date");
			}
			if ($("input[name='one']:checked").val() == '2' && $('.returnchk').val().trim() != '' && dateOne >=
				dateTwo) {
				error++;
				alert("Please Enter Return Date Equal or Greater Than Departure Date");
			}
			if ($("#autocomplete").val().toLowerCase() == $("#autocomplete1").val().toLowerCase()) {
				error++;
				alert("From & To airports cannot be the same");
			}

			if (error == 1) {
				e.preventDefault();
				// var formdata=new FormData($("#search_form_1")[0]);

				// $.ajax({
				// 	url:"{{route('flightsearch')}}",
				// 	type:"POST",
				// 	data: formdata,
				// 	processData:false,
				// 	contentType:false,
				// 	success:function(response)
				// 	{
				// 	 console.log(response);
				// 	}
				// })
			}
		});
	</script>
	<script>
		$(document).on('click', '.add_more', function () {
			var id = this.id;
			var id1 = id.split("-");
			var lastid = id1[1];
			var flight_from = $('.flight_from-' + lastid).val();
			var flight_dep = $('.flight_dep-' + lastid).val();
			var flight_to = $('.flight_to-' + lastid).val();
			if (flight_from == '') {
				alert("Please Select Orign");
			} else if (flight_to == '') {
				alert("Please Select Destination");
			} else if (flight_dep == '') {
				alert("Please Select Departure Date");
			} else {
				$('#myModaln1').modal('show');
				$.ajax({
					url: "{{route('showmulticity')}}",
					data: {
						'lastid': lastid,
						'flight_to': flight_to,
					},
					type: 'GET',
					success: function (data) {

						$('.showmultinew').append(data);
						$('.more_options-' + lastid).hide();
						var date = new Date();
						date.setDate(date.getDate());
						$('.departure').datepicker({
							autoclose: true,
							todayHighlight: true,
							format: 'dd/mm/yyyy',
							startDate: date

						});
						setTimeout(function () {
							$('#myModaln1').modal('hide')
						}, 500);


					}
				});
			}


		});
	</script>
	<script>
		$(document).on('click', '.remove', function () {
			var id = this.id;
			var lastid = parseInt(id) - 1;
			$("span#mul-" + id).remove();
			if (id == '1') {
				$('.more_options-1').show();
			} else {
				$('.more_options-' + lastid).show();
			}


		});
	</script>
	<script>
		//mouse hover
		$(document).on('mouseenter mouseleave', '.chepflight1', function () {
			$(this).find('.box-hover').fadeToggle(100);
		});

		//end mouse hover
		$(document).on('click', '.chepflight1', function () {
			$('.chepflight1').removeClass('a-select');
			$(this).addClass('a-select');
			var resultindx = this.id;
			var arrcur = $('#recur').val();
			var flightname = $('#flightname-' + resultindx).text();
			var flightcode = $('#flightcode-' + resultindx).text();
			var flightdeptime = $('#flightdeptime-' + resultindx).text();
			var flightarvtime = $('#flightarvtime-' + resultindx).text();
			var flightimg = $('#flightimg-' + resultindx).attr('src');
			var flihtdeptime1 = flightdeptime.split("|");
			var flihtarvtime1 = flightarvtime.split("|");
			var flightrou = '';
			$('.flightroute-' + resultindx).each(function () {
				var flightroute = $(this).html();
				flightrou += flightroute + '<br>';
			});
			$('.cheapflightroute').html(flightrou);
			var flightprice = $('#flightprice-' + resultindx).text();
			$('#cheapflightname').text(flightname);
			$('#cheapflightcode').text(flightcode);
			$('#cheapflightimg').attr("src", flightimg);
			$('#chepflightdep').html(flihtdeptime1[0] + ' <i class="fa fa-plane fa-rotate-45"></i>  ' + flihtarvtime1[
				0]);
			$('.cheapflightprice').text(flightprice);
			var publishprice = $('#publishprice-' + resultindx).val();

			$('#cheapflight').val(publishprice);
			//showprice
			var secondflightprice = $('#cheapflight1').val();
			var newprice = parseInt(publishprice) + parseInt(secondflightprice);
			var lprice = parseFloat(newprice).toFixed(2);
			$('#cheapprice').html('<i class="fa ' + arrcur + '"></i>' + lprice);
			$('#cheapresultindex').val(resultindx);
		});

		// flightreturnseconddiv
		$(document).on('click', '.chepflight2', function () {
			$('.chepflight2').removeClass('a-select');
			$(this).addClass('a-select');
			var rresultindx = this.id;
			var flightname = $('#rflightname-' + rresultindx).text();
			var flightcode = $('#rflightcode-' + rresultindx).text();
			var flightdeptime = $('#rflightdeptime-' + rresultindx).text();
			var flightarvtime = $('#rflightarvtime-' + rresultindx).text();
			var arrcur = $('#recur').val();
			var flightimg = $('#rflightimg-' + rresultindx).attr('src');
			var flihtdeptime1 = flightdeptime.split("|");
			var flihtarvtime1 = flightarvtime.split("|");
			var flightrou = '';
			$('.rflightroute-' + rresultindx).each(function () {
				var flightroute = $(this).html();
				flightrou += flightroute + '<br>';
			});
			$('.rcheapflightroute').html(flightrou);
			var flightprice = $('#rflightprice-' + rresultindx).text();
			$('#rcheapflightname').text(flightname);
			$('#rcheapflightcode').text(flightcode);
			$('#rcheapflightimg').attr("src", flightimg);
			$('#rchepflightdep').html(flihtdeptime1[0] + ' <i class="fa fa-plane fa-rotate-45"></i>  ' +
				flihtarvtime1[0]);
			$('.rcheapflightprice').text(flightprice);
			var rpublishprice = $('#rpublishprice-' + rresultindx).val();

			$('#cheapflight1').val(rpublishprice);
			var firstflightprice = $('#cheapflight').val();
			var newprice1 = parseInt(firstflightprice) + parseInt(rpublishprice);
			var lprice = parseFloat(newprice1).toFixed(2);
			$('#cheapprice').html('<i class="fa ' + arrcur + ' "></i>' + lprice);
			$('#cheapresultindex1').val(rresultindx);

		});
	</script>
	<script>
		$(document).on('click', '.searchflight', function () {
			$('.dropshowflight').toggle();
		})

		$(document).on('click', '.adultplus', function () {
			var chkval = $('.adultval').val();
			var chkplus = $('.childval').val();
			var infantval = $('.infantval').val();
			if (chkval < 9) {
				var chkval1 = parseInt(chkval) + 1;
				$('.adultval').val(chkval1);
				$('.adultcount').text(chkval1);
				var totaltravelcount = parseInt(chkval1) + parseInt(chkplus) + parseInt(infantval);
				$('.totaltravel').val(totaltravelcount + ' Travelers');

			} else {
				$('.adultval').val(chkval);
				$('.adultcount').text(chkval);
				$('.totaltravel').val(chkval + ' Travelers');
				var totaltravelcount = parseInt(chkval) + parseInt(chkplus) + parseInt(infantval);
				$('.totaltravel').val(totaltravelcount + ' Travelers');
			}
		});
		$(document).on('click', '.adultminus', function () {
			var chkval = $('.adultval').val();
			var chkplus = $('.childval').val();
			var infantval = $('.infantval').val();
			if (chkval != 1) {
				var chkval1 = parseInt(chkval) - 1;
				$('.adultval').val(chkval1);
				$('.adultcount').text(chkval1);
				var totaltravelcount = parseInt(chkval1) + parseInt(chkplus) + parseInt(infantval);
				$('.totaltravel').val(totaltravelcount + ' Travelers');
			} else {
				$('.adultval').val(1);
				$('.adultcount').text(1);
				var totaltravelcount = parseInt(chkval) + parseInt(chkplus) + parseInt(infantval);
				$('.totaltravel').val(totaltravelcount + ' Travelers');
			}
		});
		$(document).on('click', '.childplus', function () {
			var chkval = $('.adultval').val();
			var chkplus = $('.childval').val();
			var infantval = $('.infantval').val();
			if (chkplus < 9) {

				var chkplus1 = parseInt(1) + parseInt(chkplus);
				$('#childval').val(chkplus1);
				$('.childcount').text(chkplus1);
				var totaltravelcount = parseInt(chkval) + parseInt(chkplus1) + parseInt(infantval);
				$('.totaltravel').val(totaltravelcount + ' Travelers');

			} else {
				$('#childval').val(chkplus);
				$('.childcount').text(chkplus);
				var totaltravelcount = parseInt(chkval) + parseInt(chkplus) + parseInt(infantval);
				$('.totaltravel').val(totaltravelcount + ' Travelers');
			}
		});
		$(document).on('click', '.childminus', function () {
			var chkval = $('.adultval').val();
			var chkplus = $('.childval').val();
			var infantval = $('.infantval').val();
			if (chkplus != 0) {
				var chkplus1 = parseInt(chkplus) - parseInt(1);
				$('#childval').val(chkplus1);
				$('.childcount').text(chkplus1);
				var totaltravelcount = parseInt(chkval) + parseInt(chkplus1) + parseInt(infantval);
				$('.totaltravel').val(totaltravelcount + ' Travelers');
			} else {
				$('#childval').val(0);
				$('.childcount').text(0);
				var totaltravelcount = parseInt(chkval) + parseInt(0) + parseInt(infantval);
				$('.totaltravel').val(totaltravelcount + ' Travelers');
			}
		});
		$(document).on('click', '.infantplus', function () {
			var chkval = $('.adultval').val();
			var chkplus = $('.childval').val();
			var infantval = $('.infantval').val();

			if (infantval < 9) {

				var infantval1 = parseInt(1) + parseInt(infantval);
				$('#infantval').val(infantval1);
				$('.infantcount').text(infantval1);
				var totaltravelcount = parseInt(chkval) + parseInt(chkplus) + parseInt(infantval1);
				$('.totaltravel').val(totaltravelcount + ' Travelers');
			} else {
				$('#infantval').val(infantval);
				$('.infantcount').text(infantval);
				var totaltravelcount = parseInt(chkval) + parseInt(chkplus) + parseInt(0);
				$('.totaltravel').val(totaltravelcount + ' Travelers');
			}
		});
		$(document).on('click', '.infantminus', function () {
			var chkval = $('.adultval').val();
			var chkplus = $('.childval').val();
			var infantval = $('.infantval').val();
			if (infantval != 0) {
				var infantval1 = parseInt(infantval) - parseInt(1);
				$('#infantval').val(infantval1);
				$('.infantcount').text(infantval1);
				var totaltravelcount = parseInt(chkval) + parseInt(chkplus) + parseInt(infantval1);
				$('.totaltravel').val(totaltravelcount + ' Travelers');
			} else {
				$('#infantval').val(0);
				$('.infantcount').text(0);
				var totaltravelcount = parseInt(chkval) + parseInt(chkplus) + parseInt(0);
				$('.totaltravel').val(totaltravelcount + ' Travelers');
			}
		});
		// 	$(function() {


		//     $(".minus").click(function() {
		//         var text = $(this).prev(":text");
		//         text.val(parseInt(text.val(), 10) - 1);
		//     });
		// });
	</script>
	<script>
		$(document).on('click', '#flightdone', function () {
			$('.dropshowflight').hide();
		});
	</script>
	<script>
		$(document).on('click', '.closemodal', function () {
			$(".result-modify").hide();
		})
	</script>
	<script>
		$(document).on('click', '.exchangeicon', function () {

			var chk = this.id;
			var flightfrom = $('.flight_from-' + chk).val();
			var flightto = $('.flight_to-' + chk).val();

			$('.flight_from-' + chk).val(flightto);
			$('.flight_to-' + chk).val(flightfrom);
		})
	</script>
<script>
var goDiv=document.getElementsByClassName("airlines-list")[1]
var returnDiv=document.getElementsByClassName("airlines-list")[2]
console.log(goDiv)
for(var i=0;i<goDiv.children.length;i++){
	goDiv.children[i].addEventListener("click",function(){
		console.log(returnDiv)
		returnDiv.scrollIntoView({behavior: "smooth", block: "nearest"});
	})
}
</script>
<script>
	var count = 0;
	var height = $(".flight-filter").innerHeight() + 100
	$(window).scroll(function (e) {

		var $el = $('.filter-res-div');
		var isPositionFixed = ($el.css('position') == 'fixed');
		if (count == 0) {
			/* if ($(this).scrollTop() > ($(".flight-filter").height() - $(".flight-filter").offset().top) + 100) {
				$(".flight-filter").css({
					'position': 'fixed',
					'top': '80px'
				});
			} */
			if ($(this).scrollTop() > height + $(".flight-filter").offset().top) {
				document.getElementsByClassName("filter-btn")[0].style.display = "block"
				document.getElementsByClassName("filter-btn")[0].classList.add("desktop-filter-btn")
				$el.css({
					'position': 'fixed',
					'top': '80px'
				});

				
				document.getElementsByClassName("filter-btn")[0].removeAttribute("data-toggle")
				document.getElementsByClassName("filter-btn")[0].addEventListener("click", function () {


					window.scrollTo({
						top: 0,
						behavior: 'smooth'
					})

					document.getElementsByClassName("filter-btn")[0].style.display = "none"
				})
			} else if ($(this).scrollTop() < height + $(".flight-filter").offset().top) {
				
				document.getElementsByClassName("filter-btn")[0].setAttribute("data-toggle", "modal")
				document.getElementsByClassName("filter-btn")[0].style.display = "none"
				document.getElementsByClassName("filter-btn")[0].classList.remove("desktop-filter-btn")
				$el.css({
					'position': 'static',
					'top': '80px'
				});


			}
		} else if (count == 1) {
			if ($(this).scrollTop() > 450 && !isPositionFixed) {
				$el.css({
					'position': 'fixed',
					'bottom': '120px'
				});
				document.getElementById("span-filter").innerText=""
			}

			if ($(this).scrollTop() < 450 && isPositionFixed) {
				$el.css({
					'position': 'static',
					'bottom': '120px'
				});
				document.getElementById("span-filter").innerText="Filter"
			}
		}

	});

	function myFunction(x) {
		if (x.matches) { // If media query matches
			count = 1;
			var target = document.getElementsByClassName("filter-res")[0];
			if (target.children.length == 0) {
				var source = document.getElementsByClassName("flight-filter")[0];
				source.cloneNode(true)
				target.append(source)
			}
		}
	}

	var x = window.matchMedia("(max-width: 992px)")
	myFunction(x) // Call listener function at run time
	x.addListener(myFunction)
</script>

</body>

</html>