@include('pages.include.header')<!-- Loader --><style> .loader {
    display: block;
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: #CCEAF7 url('{{ asset('assets/images/flight-loader.gif')}}') no-repeat center center;
    text-align: center;
    color: #999;
    opacity: 0.9;
}

</style><style> .invoice-content {
    padding: 50px 70px;
    border: 1px solid #DDD;
    box-shadow: 0px 0px 17px 1px rgba(0, 0, 0, 0.1);
}

.e-ticket {
    color: #00206A;
    font-weight: 600;
    margin-bottom: 20px;
}

.e-ticket .booking-id {
    margin-bottom: 5px;
}

.e-ticket h4 {
    color: #fa9e1b;
    font-weight: 600;
}

.onward-icon i {
    font-size: 40px;
    color: #fa9e1b;
}

.onward-icon {
    float: left;
    width: 50px;
}

.onward-flight-details {
    margin-left: 50px;
}

.onward-flight {
    margin-bottom: 30px;
    padding-bottom: 10px;
    border-bottom: 1px solid #fa9e1b;
}

.onward-flight-details h5 {
    margin-bottom: 5px;
    color: #333;
    font-weight: 600;
}

.onward-flight-details h4 {
    color: #00206A;
    font-weight: 600;
}

.onward-flight-no {
    text-align: right;
}

.onward-flight-no h5 {
    margin-bottom: 5px;
    color: #333;
    font-weight: 600;
}

.onward-flight-no h4 {
    color: #00206A;
    font-weight: 700;
}

.flight-logo {
    float: left;
    width: 50px;
}

.flight-name-no {
    margin-left: 50px;
}

.flight-name-no h4 {
    color: #00206A;
}

.flight-ref {
    text-align: right;
}

.flight-ref a {
    background: green;
    border: 1px solid green;
    color: #FFF;
    padding: 6px 20px;
    font-size: 16px;
    border-radius: 20px;
}

.flight-non-ref {
    text-align: right;
}

.flight-non-ref a {
    background: red;
    border: 1px solid red;
    color: #FFF;
    padding: 6px 20px;
    font-size: 16px;
    border-radius: 20px;
}

.invoice-flight-name {
    margin-bottom: 30px;
}

.invoice-flight-details {
    margin-bottom: 20px;
    padding-bottom: 20px;
    border-bottom: 1px solid #fa9e1b;
}

.invoice-flight-details .flight-and-time {
    color: #333;
}

.invoice-flight-details .flight-and-time .day {
    font-size: 16px;
    font-weight: 600;
    margin-bottom: 5px;
}

.invoice-flight-details .flight-and-time .city-time {
    font-size: 20px;
    margin-bottom: 5px;
}

.invoice-flight-details .flight-and-time .airport {
    font-weight: 600;
}

.invoice-flight-duration {
    text-align: center;
}

.invoice-flight-duration .clock-icon {
    margin-bottom: 8px;
}

.invoice-flight-duration .clock-icon i {
    font-size: 25px;
    color: #fa9e1b;
}

.invoice-flight-duration .hours {
    font-size: 16px;
    margin-bottom: 3px;
    color: #00206A;
    font-weight: 600;
}

.invoice-flight-duration .invoice-flight-class {
    font-size: 16px;
    color: #00206A;
    font-weight: 600;
}

.traveller-details {
    margin-bottom: 30px;
}

.traveller-details .traveller {
    color: #fa9e1b;
    font-weight: 600;
    text-transform: uppercase;
    margin-bottom: 10px;
}

.traveller-details .ticket-id {
    color: #fa9e1b;
    font-weight: 600;
    text-transform: uppercase;
    text-align: right;
    margin-bottom: 10px;
}

.traveller-details .traveller-name {
    font-size: 18px;
    color: #00206A;
    font-weight: 600;
}

.traveller-details .traveller-ticket-id {
    font-size: 18px;
    color: #00206A;
    font-weight: 600;
    text-align: right;
}

.invoice-important {
    background: #EEE;
    box-shadow: 0px 0px 17px 1px rgba(0, 0, 0, 0.1);
    padding: 20px 30px;
    border: 1px solid #DDD;
    border-radius: 5px;
    margin-bottom: 30px;
}

.invoice-important .heading {
    font-size: 22px;
    color: #fa9e1b;
    font-weight: 600;
    text-align: center;
    margin-bottom: 10px;
    text-transform: uppercase;
}

.invoice-important ul li:before {
    content: "\f0da";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
    color: #fa9e1b;
    font-size: 18px;
    position: absolute;
    top: 0px;
    left: 0;
}

.invoice-important ul li {
    font-size: 14px;
    text-align: justify;
    color: #333;
    line-height: 26px;
    position: relative;
    padding-left: 17px;
    margin-bottom: 10px;
    font-weight: 600;
}

.baggage-policy {
    margin-bottom: 30px;
}

.baggage-policy .heading {
    font-size: 22px;
    color: #fa9e1b;
    font-weight: 600;
    margin-bottom: 10px;
}

.baggage-policy .heading i {
    margin-right: 10px;
}

.baggage-policy table th {
    white-space: nowrap;
}

.baggage-policy table th,
.baggage-policy table td {
    background: #FFF4e7;
    color: #00206A;
    border: 1px solid #00206A;
}

.baggage-policy table td p {
    margin-bottom: 5px;
}

.baggage-policy table td,
.baggage-policy table td p {
    color: #333;
    font-weight: 600;
}

.cancellation-policy {
    margin-bottom: 30px;
}

.cancellation-policy .heading {
    font-size: 22px;
    color: #fa9e1b;
    font-weight: 600;
    margin-bottom: 10px;
}

.cancellation-policy .heading i {
    margin-right: 10px;
}

.cancellation-policy table th {
    white-space: nowrap;
}

.cancellation-policy table th,
.cancellation-policy table td {
    background: #FFF4e7;
    color: #00206A;
    border: 1px solid #00206A;
}

.cancellation-policy table td p {
    margin-bottom: 5px;
}

.cancellation-policy table td,
.cancellation-policy table td p {
    color: #333;
    font-weight: 600;
}

.fare-payment-policy {
    margin-bottom: 30px;
}

.fare-payment-policy .heading {
    font-size: 22px;
    color: #fa9e1b;
    font-weight: 600;
    margin-bottom: 10px;
}

.fare-payment-policy .fare-left {
    font-size: 15px;
    color: #333;
    font-weight: 500;
    margin-bottom: 5px;
    font-weight: 600;
}

.fare-payment-policy .fare-right {
    font-size: 16px;
    color: #00206A;
    font-weight: 600;
    margin-bottom: 7px;
    text-align: right;
}

.fare-payment-policy .fare-right i {
    font-size: 18px;
    color: #fa9e1b;
}

.total-fare {
    padding-top: 10px;
    border-top: 1px solid #fa9e1b;
    border-bottom: 1px solid #fa9e1b;
    padding-bottom: 4px;
    padding-left: 10px;
    padding-right: 10px;
}

.fare-payment-policy .pl-15 {
    padding-left: 15px;
    font-weight: 500;
}

.total-fare .fare-left,
.total-fare .fare-right {
    color: #00206A;
    font-weight: 600;
    font-size: 18px;
}

@media only screen and (max-width:992px) {
    .invoice-content {
        padding: 30px 15px;
    }
    .fare-payment-policy .col-md-8 {
        -ms-flex: 0 0 66.666667%;
        flex: 0 0 66.666667%;
        max-width: 66.666667%;
    }
    .fare-payment-policy .col-md-4 {
        -ms-flex: 0 0 33.333333%;
        flex: 0 0 33.333333%;
        max-width: 33.333333%;
    }
    .traveller-details .col-md-6 {
        -ms-flex: 0 0 50%;
        flex: 0 0 50%;
        max-width: 50%;
    }
    .onward-flight .col-md-6 {
        -ms-flex: 0 0 50%;
        flex: 0 0 50%;
        max-width: 50%;
    }
    .invoice-flight-name .flight-non-ref a {
        padding: 4px 10px;
        font-size: 14px;
        border-radius: 10px;
    }
    .invoice-flight-name .col-md-6 {
        -ms-flex: 0 0 50%;
        flex: 0 0 50%;
        max-width: 50%;
    }
    .invoice-flight-details .col-md-5 {
        -ms-flex: 0 0 41.666667%;
        flex: 0 0 41.666667%;
        max-width: 41.666667%;
        padding-left: 5px;
        padding-right: 5px;
    }
    .invoice-flight-details .invoice-flight-duration .invoice-flight-class {
        font-size: 14px;
        font-weight: 600;
    }
    .invoice-flight-details .invoice-flight-duration .hours {
        font-size: 14px;
        font-weight: 600;
    }
    .invoice-flight-details .col-md-2 {
        -ms-flex: 0 0 16.666667%;
        flex: 0 0 16.666667%;
        max-width: 16.666667%;
        padding-left: 5px;
        padding-right: 5px;
    }
    .invoice-flight-details .flight-and-time .city-time {
        font-size: 17px;
    }
    .invoice-flight-details .flight-and-time .day {
        font-size: 14px;
    }
    .invoice-flight-details .flight-and-time .airport {
        font-size: 12px;
    }
    .invoice-flight-name .flight-name-no {
        margin-left: 40px;
    }
    .invoice-flight-name .flight-logo {
        width: 30px;
    }
}

</style><!-- Loader -->
<body>
    <div class="loader"></div>
    <div class="super_container">
        <!-- Header -->@include('pages.include.topheader')
        <div class="home" style="height:20vh;">
            <!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->
            <div class="home_content">
                <div class="home_title">Results</div>
            </div>
        </div>
        
        <!-- Intro -->
        <div class="super_container">
            <!-- Intro -->
            <div class="container" data-spy="scroll" data-target=".header-nav">
                <!-- <ul class="header-nav"> <li><a onclick="f()" href="#upcoming" class="bar-item"><i class="fa fa-shopping-bag" style="padding:6px;   font-size: 17px"></i> UPCOMING</a></li> <li><a onclick="f()" href="#cancel" class="bar-item"><i class="	fa fa-close" style=" font-size: 17px"></i> CANCELLED</a></li> <li><a onclick="f()" href="#complete" class="bar-item"><i class="fa fa-check" style="padding:6px;  font-size: 17px"></i> COMPLETED</a></li> </ul> -->
                <ul class="nav nav-tabs header-nav">
                    <li class="bar-item"><a data-toggle="tab" href="#travel"><i class="fa fa-user" style=" font-size: 17px"></i> Travellers</a></li>
                    <li class="bar-item"><a data-toggle="tab" href="#setting"><i class="fa fa-cog" style="padding:6px;  font-size: 17px"></i> My Settings</a></li>
                </ul>
                <div class="row">
                    <div class="col-md-12">
                        <div class="tab-content">
                            <div id="travel" class="tab-pane fade in active show">
                                <div class="content" style="border-bottom-left-radius: 8px;border-bottom-right-radius: 8px">
                                    <h2 class="p-detail">Your Personal Information</h2>
                                    <div class="row">

                                    	<?php
										if($exist->user_image=='')
										{
											$imgpath="asset('assets/images/add-image.png')";
										}
										else
										{
											$imgpath="assets/profile_image/".$exist->user_image;
										}
										?>
                                        <div class="col-sm-12 col-md-2 mx-auto d-block"> <img class="img-fluid img-rounded user-avatar-img" src="{{$imgpath}}"> </div>
                                        <div class="col-sm-12 col-md-10 edit-btn">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <h3>{{ucfirst($exist->user_fname)}} {{ucfirst($exist->user_lname)}}</h3> </div>
                                                <div class="col-sm-6 nav nav-tabs" style="border: none;"> <a href="#add-user-edit" data-toggle="tab" class="trip-edit-btn"><i class="fa fa-pencil"></i> Edit</a> </div>
                                                <hr class="u-divider"> </div>
                                            <div class="user-t-info"> 
                                            	@if($exist->user_gender=='')
                                            	<a href="#">Add Gender</a>,
                                            	@else
                                            	<p style="margin-bottom: 0"><b>Gender: </b> {{ $exist->user_gender}}</p>
                                            		
                                            	@endif
                                            	@if($exist->user_dob=='')
                                            	<a href="#">Add Birth-date</a><br>
                                            	@else
                                            	<p style="margin-bottom: 0"><b>Dob: </b> {{ $exist->user_dob}}</p>
												@endif
                                                
                                                 
                                                @if($exist->user_phone=='')
                                                <a href="#">Add Mobile-no</a><br>
                                                @else
                                                <p style="margin-bottom: 0"><b>Mobile No: </b> {{ $exist->user_phone}}</p>
                                                @endif
                                                <p style="margin-bottom: 0"><b>Email:</b>{{$exist->user_email}}</p>
                                                <p style="margin-bottom: 0"><b>Address:</b>{{$exist->user_address}} 
                                                	, {{$exist->user_city}} 
                                                	, {{$exist->user_county}} 
                                                	, {{$exist->user_pincode}}
                                                </p>
                                                  </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <h2 class="p-detail">Your Travellers Information</h2> -->
                                <!-- <div class="container" data-spy="scroll" data-target="#a" data-offset="500">
                                    <div class="row" id="a">
                                        <div class="col-sm-3">
                                            <div class="content add-travel-div nav nav-tabs" style="border-radius: 8px;">
                                                <a href="#add-user" data-toggle="tab"><img class="img-fluid img-rounded add-travel" src="{{asset('assets/images/plus-user.png')}}"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <!-- <div id="add-user" class="tab-pane fade">
                                <div class="container">
                                    <div class="row content  mb-4 py-4" style="border-bottom-left-radius: 8px; border-bottom-right-radius: 8px">
                                        <div class="col-sm-6">
                                            <h2 class="t-detail">Your Traveller's  Information</h2> </div>
                                        <div class="col-sm-6"> <a href="#" class="t-cancel-btn"><i class="fa fa-close"></i> Cancel</a> 
                                        	<button class="t-ok-btn" ><i class="fa fa-save"></i>Save</button>
                                        	
                                        </div>
                                    </div>
                                </div>
                                <div class="content1" style="border-radius: 8px">
                                    <div class="row">
                                        <div class="col-sm-12 travel-btn">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3 class="t-info">Your name as on the passport</h3> </div>
                                            </div>
                                            <hr style="width: 100%">
                                            <div class="">
                                                <form class="form-inline t-form" action="#">
                                                    <select class="form-control mr-3 mb-4 travel-input">
                                                        <option>Mr.</option>
                                                        <option>Mrs.</option>
                                                    </select>
                                                    <input type="text" class="form-control mr-3 mb-4 travel-input" placeholder="First Name" id="pwd" value="{{ucfirst($exist->user_fname)}} ">
                                                   
                                                    <input type="text" class="form-control mr-3 mb-4 travel-input" placeholder="Last Name" id="pwd" value="{{ucfirst($exist->user_lname)}} "> 
                                                    <select class="form-control mr-3 mb-4 travel-input">
                                                        <option>Male</option>
                                                        <option>Female</option>
                                                    </select>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content1" style="border-radius: 8px">
                                    <div class="row">
                                        <div class="col-sm-12 travel-btn">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3 class="t-info">Your Contact Info</h3> </div>
                                            </div>
                                            <hr style="width: 100%">
                                            <div class="">
                                                <form class="form-inline t-form" action="#">
                                                    <input type="text" class="form-control mr-5 mb-4 travel-input" placeholder="Your Email" id="contact">
                                                    <input type="text" class="form-control mr-5 mb-4 travel-input" placeholder="Code" id="contact">
                                                    <input type="text" class="form-control mr-5 mb-4 travel-input" placeholder="Phone No" id="contact"> </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content1" style="border-radius: 8px">
                                    <div class="row">
                                        <div class="col-sm-12 travel-btn">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3 class="t-info">Date of Birth</h3> </div>
                                            </div>
                                            <hr style="width: 100%">
                                            <div class="">
                                                <form class="form-inline t-form" action="#">
                                                    <select class="form-control mr-5 mb-4 travel-input">
                                                        <option>Day</option>
                                                        <option>Mr.</option>
                                                        <option>Mrs.</option>
                                                    </select>
                                                    <select class="form-control mr-5 mb-4 travel-input">
                                                        <option>Month</option>
                                                        <option>Mr.</option>
                                                        <option>Mrs.</option>
                                                    </select>
                                                    <select class="form-control mr-5 mb-4 travel-input">
                                                        <option>Year</option>
                                                        <option>Mr.</option>
                                                        <option>Mrs.</option>
                                                    </select>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div id="add-user-edit" class="tab-pane fade">
                                <div class="container">

                                    <div class="row content  mb-4 py-4" style="border-bottom-left-radius: 8px; border-bottom-right-radius: 8px">

                                        <div class="col-sm-6">
                                            <h2 class="t-detail">Your Personnel Information</h2> </div>
                                        <div class="col-sm-6"> <a href="" class="t-cancel-btn"><i class="fa fa-close"></i> Cancel</a> 
                                        	<button class="t-ok-btn" id="userdetail" ><i class="fa fa-save"></i>Save</button>
                                        	<!-- <a href="#" class="t-ok-btn"><i class="fa fa-save"></i> Save</a> --> 
                                        </div>
                                        <div class="col-sm-12">
                                        	<div class="alert alert-danger alert-dismissible" id="error_div" style="display: none">
							                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							                    <i class="icon fa fa-ban"></i><span id="error"></span>
							                </div>
							                <div class="alert alert-success alert-dismissible" id="success_div" style="display: none">
							                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							                    <i class="icon fa fa-check"></i><span id="success"></span>
							                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content1" style="border-radius: 8px">
                                    <div class="row">
                                        <div class="col-sm-12 travel-btn">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3 class="t-info">Your name as on the passport</h3> </div>
                                            </div>
                                            <hr style="width: 100%">
                                            <div class="">
                                                <form class="form-inline t-form" action="#">
                                                    <select class="form-control mr-3 mb-4 travel-input" id="user_title">
                                                        <option value="Mr">Mr.</option>
                                                        <option value="Mrs">Mrs.</option>
                                                    </select>
                                                    <input type="text" class="form-control mr-3 mb-4 travel-input" placeholder="First Name" id="user_firstname" id="pwd" value="{{ucfirst($exist->user_fname)}}">
                                                    
                                                    <input type="text" class="form-control mr-3 mb-4 travel-input" placeholder="Last Name" id="user_lastname" value="{{ucfirst($exist->user_lname)}}"> 
                                                   <?php
                                                   
                                                   if($exist->user_gender=='Male')
                                                   {
                                                   		$chkgen='selected';
                                                   }
                                                   else
                                                   {
                                                   	$chkgen='';
                                                   }
                                                    if($exist->user_gender=='Female')
                                                   {
                                                   		$chkgen='selected';
                                                   }
                                                   else
                                                   {
                                                   	$chkgen='';
                                                   }

                                                   ?>

                                                    <select class="form-control mr-3 mb-4 travel-input" id="user_gender">
														
                                                        <option value="Male" <?php echo $chkgen;?>>Male</option>
                                                        <option value="Female" <?php echo $chkgen;?>>Female</option>
                                                    </select>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content1" style="border-radius: 8px">
                                    <div class="row">
                                        <div class="col-sm-12 travel-btn">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3 class="t-info">Your Contact Info</h3> </div>
                                            </div>
                                            <hr style="width: 100%">
                                            <div class="">
                                                <div class="form-inline t-form" >
                                                    <input type="text" class="form-control mr-5 mb-4 travel-input" placeholder="Your Email" id="email" value="{{$exist->user_email}}" readonly>
                                                    <input type="text" class="form-control mr-5 mb-4 travel-input" placeholder="Code" id="user_code" value="{{$exist->user_phone_code}}">
                                                    <input type="text" class="form-control mr-5 mb-4 travel-input" placeholder="Phone No" id="user_phone" value="{{$exist->user_phone}}"> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content1" style="border-radius: 8px">
                                    <div class="row">
                                        <div class="col-sm-12 travel-btn">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3 class="t-info">Date of Birth</h3> </div>
                                            </div>
                                            <hr style="width: 100%">
                                            <div class="">
                                                <div class="form-inline t-form" >
                                                	<?php
                                                	if($exist->user_dob !='')
                                                    {
                                                        $userdob1=explode("-",$exist->user_dob);
                                                         $user_dobday=$userdob1[2];
                                                         $user_month=$userdob1[1];
                                                         $user_year=$userdob1[0];
                                                    }
                                                	$user_dobday="";
                                                    $user_month="";
                                                    $user_year="";
                                                	if($user_dobday==1){ echo "selected";}
                                                	?>

                                                    <select class="form-control mr-5 mb-4 travel-input" id="user_dobday">
                                                        <option>Day</option>
                                                        <option value='1' <?php if($user_dobday==1){ echo "selected";}?>>1</option>
														<option value='2' <?php if($user_dobday==2){ echo "selected";}?>>2</option>
														<option value='3' <?php if($user_dobday==3){ echo "selected";}?>>3</option>
														<option value='4' <?php if($user_dobday==4){ echo "selected";}?>>4</option>
														<option value='5' <?php if($user_dobday==5){ echo "selected";}?>>5</option>
														<option value='6' <?php if($user_dobday==6){ echo "selected";}?>>6</option>
														<option value='7' <?php if($user_dobday==7){ echo "selected";}?>>7</option>
														<option value='8' <?php if($user_dobday==8){ echo "selected";}?>>8</option>
														<option value='9' <?php if($user_dobday==9){ echo "selected";}?>>9</option>
														<option value='10' <?php if($user_dobday==10){ echo "selected";}?>>10</option>
														<option value='11' <?php if($user_dobday==11){ echo "selected";}?>>11</option>
														<option value='12' <?php if($user_dobday==12){ echo "selected";}?>>12</option>
														<option value='13' <?php if($user_dobday==13){ echo "selected";}?>>13</option>
														<option value='14' <?php if($user_dobday==14){ echo "selected";}?>>14</option>
														<option value='15' <?php if($user_dobday==15){ echo "selected";}?>>15</option>
														<option value='16' <?php if($user_dobday==16){ echo "selected";}?>>16</option>
														<option value='17' <?php if($user_dobday==17){ echo "selected";}?>>17</option>
														<option value='18' <?php if($user_dobday==18){ echo "selected";}?>>18</option>
														<option value='19' <?php if($user_dobday==19){ echo "selected";}?>>19</option>
														<option value='20' <?php if($user_dobday==20){ echo "selected";}?>>20</option>
														<option value='21' <?php if($user_dobday==21){ echo "selected";}?>>21</option>
														<option value='22' <?php if($user_dobday==22){ echo "selected";}?>>22</option>
														<option value='23' <?php if($user_dobday==23){ echo "selected";}?>>23</option>
														<option value='24' <?php if($user_dobday==24){ echo "selected";}?>>24</option>
														<option value='25' <?php if($user_dobday==25){ echo "selected";}?>>25</option>
														<option value='26' <?php if($user_dobday==26){ echo "selected";}?>>26</option>
														<option value='27' <?php if($user_dobday==27){ echo "selected";}?>>27</option>
														<option value='28' <?php if($user_dobday==28){ echo "selected";}?>>28</option>
														
														<option value='29' <?php if($user_dobday==29){ echo "selected";}?>>29</option>
														<option value='30' <?php if($user_dobday==30){ echo "selected";}?>>30</option>
														<option value='31' <?php if($user_dobday==31){ echo "selected";}?>>31</option>
                                                    </select>
                                                    <select class="form-control mr-5 mb-4 travel-input" id="user_dobmonth">
                                                        <option>Month</option>
                                                        <option value='1' <?php if($user_month==1){ echo "selected";}?>>1</option>
														<option value='2'  <?php if($user_month==2){ echo "selected";}?>>2</option>
														<option value='3'  <?php if($user_month==3){ echo "selected";}?>>3</option>
														<option value='4'  <?php if($user_month==4){ echo "selected";}?>>4</option>
														<option value='5'  <?php if($user_month==5){ echo "selected";}?>>5</option>
														<option value='6'  <?php if($user_month==6){ echo "selected";}?>>6</option>
														<option value='7'  <?php if($user_month==7){ echo "selected";}?>>7</option>
														<option value='8'  <?php if($user_month==8){ echo "selected";}?>>8</option>
														<option value='9'  <?php if($user_month==9){ echo "selected";}?>>9</option>
														<option value='10'  <?php if($user_month==10){ echo "selected";}?>>10</option>
														<option value='11'  <?php if($user_month==11){ echo "selected";}?>>11</option>
														<option value='12' <?php if($user_month==12){ echo "selected";}?> >12</option>
                                                    </select>
                                                    <select class="form-control mr-5 mb-4 travel-input" id="user_dobyear">
                                                        <option>Year</option>
                                                        <option value='1947' <?php if($user_year==1947){ echo "selected";}?>>1947</option>
														<option value='1948' <?php if($user_year==1948){ echo "selected";}?>>1948</option>
														<option value='1949' <?php if($user_year==1949){ echo "selected";}?>>1949</option>
														<option value='1950' <?php if($user_year==1950){ echo "selected";}?>>1950</option>
														<option value='1951' <?php if($user_year==1951){ echo "selected";}?> >1951</option>
														<option value='1952' <?php if($user_year==1952){ echo "selected";}?>>1952</option>
														<option value='1953' <?php if($user_year==1953){ echo "selected";}?>>1953</option>
														<option value='1954' <?php if($user_year==1954){ echo "selected";}?>>1954</option>
														<option value='1955' <?php if($user_year==1955){ echo "selected";}?>>1955</option>
														<option value='1956' <?php if($user_year==1956){ echo "selected";}?>>1956</option>
														<option value='1957' <?php if($user_year==1957){ echo "selected";}?>>1957</option>
														<option value='1958' <?php if($user_year==1958){ echo "selected";}?>>1958</option>
														<option value='1959' <?php if($user_year==1959){ echo "selected";}?>>1959</option>
														<option value='1960' <?php if($user_year==1960){ echo "selected";}?>>1960</option>
														<option value='1961' <?php if($user_year==1961){ echo "selected";}?>>1961</option>
														<option value='1962' <?php if($user_year==1962){ echo "selected";}?>>1962</option>
														<option value='1963' <?php if($user_year==1963){ echo "selected";}?>>1963</option>
														<option value='1964' <?php if($user_year==1964){ echo "selected";}?>>1964</option>
														<option value='1965' <?php if($user_year==1965){ echo "selected";}?>>1965</option>
														<option value='1966' <?php if($user_year==1966){ echo "selected";}?>>1966</option>
														<option value='1967' <?php if($user_year==1967){ echo "selected";}?>>1967</option>
														<option value='1968' <?php if($user_year==1968){ echo "selected";}?>>1968</option>
														<option value='1969' <?php if($user_year==1969){ echo "selected";}?>>1969</option>
														<option value='1970' <?php if($user_year==1970){ echo "selected";}?>>1970</option>
														<option value='1971' <?php if($user_year==1971){ echo "selected";}?>>1971</option>
														<option value='1972' <?php if($user_year==1972){ echo "selected";}?>>1972</option>
														<option value='1973' <?php if($user_year==1973){ echo "selected";}?>>1973</option>
														<option value='1974' <?php if($user_year==1974){ echo "selected";}?>>1974</option>
														<option value='1975' <?php if($user_year==1975){ echo "selected";}?>>1975</option>
														<option value='1976' <?php if($user_year==1976){ echo "selected";}?>>1976</option>
														<option value='1977' <?php if($user_year==1977){ echo "selected";}?>>1977</option>
														<option value='1978' <?php if($user_year==1978){ echo "selected";}?>>1978</option>
														<option value='1979' <?php if($user_year==1979){ echo "selected";}?>>1979</option>
														<option value='1980' <?php if($user_year==1980){ echo "selected";}?>>1980</option>
														<option value='1981' <?php if($user_year==1981){ echo "selected";}?>>1981</option>
														<option value='1982' <?php if($user_year==1982){ echo "selected";}?>>1982</option>
														<option value='1983' <?php if($user_year==1983){ echo "selected";}?>>1983</option>
														<option value='1984' <?php if($user_year==1984){ echo "selected";}?>>1984</option>
														<option value='1985' <?php if($user_year==1985){ echo "selected";}?>>1985</option>
														<option value='1986' <?php if($user_year==1986){ echo "selected";}?>>1986</option>
														<option value='1987' <?php if($user_year==1987){ echo "selected";}?>>1987</option>
														<option value='1988' <?php if($user_year==1988){ echo "selected";}?>>1988</option>
														<option value='1989' <?php if($user_year==1989){ echo "selected";}?>>1989</option>
														<option value='1990' <?php if($user_year==1990){ echo "selected";}?>>1990</option>
														<option value='1991' <?php if($user_year==1991){ echo "selected";}?>>1991</option>
														<option value='1992' <?php if($user_year==1992){ echo "selected";}?>>1992</option>
														<option value='1993' <?php if($user_year==1993){ echo "selected";}?>>1993</option>
														<option value='1994' <?php if($user_year==1994){ echo "selected";}?>>1994</option>
														<option value='1995' <?php if($user_year==1995){ echo "selected";}?>>1995</option>
														<option value='1996' <?php if($user_year==1996){ echo "selected";}?>>1996</option>
														<option value='1997' <?php if($user_year==1997){ echo "selected";}?>>1997</option>
														<option value='1998' <?php if($user_year==1998){ echo "selected";}?>>1998</option>
														<option value='1999' <?php if($user_year==1999){ echo "selected";}?>>1999</option>
														<option value='2000' <?php if($user_year==2000){ echo "selected";}?>>2000</option>
														<option value='2001' <?php if($user_year==2001){ echo "selected";}?>>2001</option>
														<option value='2002' <?php if($user_year==2002){ echo "selected";}?>>2002</option>
														<option value='2003' <?php if($user_year==2003){ echo "selected";}?>>2003</option>
														<option value='2004' <?php if($user_year==2004){ echo "selected";}?>>2004</option>
														<option value='2005' <?php if($user_year==2005){ echo "selected";}?>>2005</option>
														<option value='2006' <?php if($user_year==2006){ echo "selected";}?>>2006</option>
														<option value='2007' <?php if($user_year==2007){ echo "selected";}?>>2007</option>
														<option value='2008' <?php if($user_year==2008){ echo "selected";}?>>2008</option>
														<option value='2009' <?php if($user_year==2009){ echo "selected";}?>>2009</option>
														<option value='2010' <?php if($user_year==2010){ echo "selected";}?>>2010</option>
														
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content1" style="border-radius: 8px">
                                    <div class="row">
                                        <div class="col-sm-12 travel-btn">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3 class="t-info">Your Address</h3> </div>
                                            </div>
                                            <hr style="width: 100%">
                                            <div class="">
                                                <div class="form-inline t-form" >
                                                    <input type="text" class="form-control mr-3 mb-4 travel-input" placeholder="Address" value="{{$exist->user_address}}" id="user_address" style="width: 100%">
                                                    <input type="text" class="form-control mr-3 mb-4  p-add travel-input" placeholder="City" id="user_city" value="{{$exist->user_city}}">
                                                    <input type="text" class="form-control mr-3 mb-4 p-add travel-input" placeholder="Pin/Zip code" id="user_pincode" value="{{$exist->user_pincode}}">

                                                    <input type="text" class="form-control mr-3 mb-4 p-add travel-input" placeholder="Country" id="user_country" value="{{$exist->user_county}}">
                                                   </div>
                                                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content1" style="border-radius: 8px">
                                    <div class="row">
                                        <div class="col-sm-12 travel-btn">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3 class="t-info">Add a Photo (Optional)</h3> </div>
                                            </div>
                                            <hr style="width: 100%">
                                            <form id="face_form" class="form-inline t-form" enctype="multipart/form-data" method="post" >
                                               {{ csrf_field() }}
                                            <div class="">
                                                <div class="col-12 mx-auto d-block "> 
													<?php
													if($exist->user_image=='')
													{
														$imgpath="asset('assets/images/add-image.png')";
													}
													else
													{
														$imgpath="assets/profile_image/".$exist->user_image;
													}
													?>
                                                	<input type="hidden" id="org_img" value="{{$exist->user_image}}" >
                                                	<img class="img-fluid img-rounded user-avatar-img1" src="{{$imgpath}}" id="user_image" style="width:75px" > </div>
                                                	<input type="file" name="imageupoload" class="form-control mr-3 mb-4 p-add travel-input" onchange="return sendFace()"  >
                                                <div class="col-12 text-center"> <a href="#" class="text-center">Change Your Image</a>
                                                </span>&nbsp;&nbsp;<span id="bar" role="progressbar" style="width:0%;font-weight:bold;display:none; color:#88b93c;">0%</span> </div>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="setting" class="tab-pane fade mt-4">
                                <div class="content1" style="border-radius: 8px">
                                    <div class="row">
                                        <div class="col-sm-12 travel-btn">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3 class="t-p-info">Change Password</h3> </div>
                                            </div>
                                            <div class="col-sm-12">
	                                        	<div class="alert alert-danger alert-dismissible" id="error_div1" style="display: none">
								                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								                    <i class="icon fa fa-ban"></i><span id="error1"></span>
								                </div>
								                <div class="alert alert-success alert-dismissible" id="success_div1" style="display: none">
								                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								                    <i class="icon fa fa-check"></i><span id="success1"></span>
								                </div>
	                                        </div>
                                            <hr style="width: 100%">
                                            <div class="">

                                                <div class="form-inline t-form" action="#">
                                                    <input type="text" class="form-control mr-3 mb-4 travel-input textfield" placeholder="Current Password" id="oldpassword">

                                                    <span class="text-danger err-msg password textfield_error"  id="oldpassword_error" style="display:none"></span>
                                                    <input type="text" class="form-control mr-3 mb-4 travel-input textfield" placeholder="New Password" id="newpassword">
                                                    <span class="text-danger err-msg password textfield_error"  id="newpassword_error" style="display:none"></span>
                                                    <input type="text" class="form-control mr-3 mb-4 travel-input textfield" placeholder="Confirm Password" id="cpassword"> 
                                                    <span class="text-danger err-msg password textfield_error"  id="cpassword_error" style="display:none"></span>
                                                </div> 
                                                <button class="t-confirm" id="passwordchange" ><i class="fa fa-save"></i>Save</button>
                                               
                                                <!-- <a href="#" class="t-confirm"><i class="fa fa-save"></i> Save</a>  -->
                                                <a href="#" class="t-pass-save"> Cancel</a> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .p-add {
            width: 40% !important;
        }
        
        .user-avatar-img1 {
            padding-top: 10px !important;
            width: 115px;
        }
        
        input::placeholder {
            text-align: center!important;
        }
        
        .content1 {
            padding: 50px;
            background-color: white;
            box-shadow: 0 2px 5px rgba(0, 0, 0, .18);
            margin-bottom: 20px;
            text-align: center;
        }
        
        .travel-input {
            border: none;
            border-bottom: 1px solid #fa9e1b;
            border-radius: 0;
        }
        
        ul.header-nav .current a,
        ul.header-nav .active:hover,
        ul.header-nav .active:focus {
            background-color: transparent !important;
            border: none;
            color: #005285 !important;
        }
        
        ul.header-nav a {
            border-bottom: none !important;
        }
        
        ul.header-nav a.active {
            border-bottom: 2px solid #fa9e1b !important;
            border-top: none !important;
            border-right: none !important;
            border-left: none !important;
        }
        
        ul.header-nav {
            list-style-type: none;
            margin: 0;
            padding: 0px;
            overflow: hidden;
            background-color: #ffffff;
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
            box-shadow: 0 2px 20px 0 rgba(0, 0, 0, .1);
        }
        
        .add-travel-div {
            border-radius: 8px;
            width: 250px;
            height: 300px;
        }
        
        img.add-travel {
            border: 1px dotted #999;
        }
        
        .content.add-travel-div {
            padding: 81px 76px;
            height: 260px;
            width: 251px;
        }
        
        .header-nav li {
            float: left;
            position: relative;
        }
        
        .header-nav li a {
            display: block;
            color: #005285;
            font-weight: bold;
            font-size: 18px;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }
        
        body {
            background-color: #eeeeee;
        }
        
        .content {
            padding: 10px;
            background-color: white;
            box-shadow: 0 2px 5px rgba(0, 0, 0, .18);
            margin-bottom: 20px;
        }
        
        .content.info {
            border-radius: 8px;
        }
        
        .edit-btn {
            padding: 43px 20px !important;
        }
        
        .edit-btn .row .col-sm-6 h3 {
            color: black;
            margin-bottom: 0px;
            padding-top: 12px;
        }
        
        hr.u-divider {
            border-top: 1px solid #ff9a75;
            margin-top: 10px;
            width: 95%;
            margin-left: auto;
            margin-right: auto;
        }
        
        .edit-btn .row .col-sm-6 h3 {
            color: #dc3545;
            padding-top: 12px;
        }
        
        a.trip-edit-btn {
            text-decoration: none;
            color: white;
            background: #fa9e1b;
            padding: 10px 10px 10px 5px;
            width: 100px;
            text-align: center;
            border-radius: 30px;
            margin-left: auto;
            display: block;
        }
        
        a.t-ok-btn,
        a.t-cancel-btn {
            text-decoration: none;
            color: white;
            background: #2a1f3e;
            padding: 10px 10px 10px 5px;
            width: 100px;
            text-align: center;
            border-radius: 30px;
            margin-left: auto;
            display: inline-block;
            float: right;
            margin-right: 10px;
        }
        .t-ok-btn{
            text-decoration: none;
            color: white;
            background: #2a1f3e;
            padding: 10px 10px 10px 5px;
            width: 100px;
            text-align: center;
            border-radius: 30px;
            margin-left: auto;
            display: inline-block;
            float: right;
            margin-right: 10px;
        }
        
        a.t-pass-save {
            text-decoration: none;
            color: white;
            background: #ffaa9f;
            padding: 10px 10px 10px 5px;
            width: 100px;
            text-align: center;
            border-radius: 30px;
            margin-left: auto;
            margin-right: auto;
            display: inline-block;
            float: none;
        }
        
        a.t-confirm {
            text-decoration: none;
            color: white;
            background: #fa9e1b;
            padding: 10px 10px 10px 5px;
            width: 100px;
            text-align: center;
            border-radius: 30px;
            margin-left: auto;
            margin-right: 15px;
            display: inline-block;
        }
        .t-confirm {
            text-decoration: none;
            color: white;
            background: #fa9e1b;
            padding: 10px 10px 10px 5px;
            width: 100px;
            text-align: center;
            border-radius: 30px;
            margin-left: auto;
            margin-right: 15px;
            display: inline-block;
        }
        
        a.t-cancel-btn {
            background: #dc3545;
            margin-right: 0px;
        }
        
        @media screen and (max-width: 600px) {
            .col-sm-3 img {
                text-align: center;
            }
            img.img-fluid.img-rounded.p-5 {
                text-align: center;
                display: block;
                margin: auto;
                padding: auto;
            }
        }
        
        @media screen and (max-width: 630px) {
            .p-add {
                width: 100% !important;
            }
        }
        
        @media screen and (max-width: 575px) {
            .edit-btn {
                text-align: center;
            }
            h2.t-detail {
                text-align: center;
            }
        }
        
        @media screen and (max-width: 700px) {
            a.t-ok-btn,
            a.t-cancel-btn {
                float: none;
            }
            a.t-ok-btn {
                float: left;
            }
            a.t-cancel-btn {
                float: right;
            }
            input#contact,
            input#pwd,
            select.form-control.mr-5.mb-4.travel-input {
                width: 100%;
            }
        }
        
        @media screen and (max-width: 578px) {
            a.trip-edit-btn {
                width: 100px;
                display: block;
                margin-top: 10px;
                margin-left: auto;
                margin-right: auto;
            }
        }
        
        @media screen and (max-width: 570px) {
            h2.t-detail {
                text-align: center !important;
            }
            a.trip-edit-btn {
                text-decoration: none;
                color: white;
                background: #fa9e1b;
                padding: 10px 10px 10px 5px;
                width: 100px;
                text-align: center;
                border-radius: 30px;
                margin-left: auto;
                display: block;
            }
        }
        
        @media screen and (max-width: 770px) {
            .user-avatar-img {
                padding-top: 0px !important;
                width: 115px;
                margin-left: auto !important;
                margin-right: auto !important;
                display: block !important;
            }
            .edit-btn {
                padding: 0px;
            }
            .user-t-info {
                text-align: center
            }
        }
        
        @media screen and (max-width: 783px) {
            .header-nav li {
                float: none;
            }
        }
        
        h3 .fa-star {
            padding: 2px;
            color: rgba(255, 179, 2, 1);
        }
        
        h2.p-detail {
            text-align: center;
            color: blue;
            margin-bottom: 30px;
            font-weight: 600;
            font-size: 26px;
        }
        
        h2.t-detail {
            text-align: left;
            color: blue;
            margin-bottom: 30px;
            font-weight: 600;
            font-size: 26px;
        }
        
        .user-avatar-img {
            padding-top: 75px;
            width: 115px;
            margin-left: 20px;
        }
        
        .travel-btn {
            padding: 0 30px;
        }
        
        h3.t-info {
            color: black;
            text-align: center;
            margin-bottom: 0;
        }
        
        h3.t-p-info {
            color: black;
            text-align: center;
            margin-bottom: 0;
        }
        
        .t-form {
            display: block;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }
    </style>
    <!-- Footer -->@include('pages.include.footer')
    <!-- Copyright -->@include('pages.include.copyright')
    <script>
        $(document).ready(function() {
            $(".loader").fadeOut("slow");
        });
    </script>
    <script>
    	$(document).on('click','#userdetail', function(){
    		$('#success').text('');
            $('#success_div').hide();
            $('#error').text('');
            $('#error_div').hide();
    		var user_title=$('#user_title').val();
    		var user_firstname=$('#user_firstname').val();
    		var user_lastname=$('#user_lastname').val();
    		var user_gender=$('#user_gender').val();
    		var user_code=$('#user_code').val();
    		var user_phone=$('#user_phone').val();
    		var user_dobday=$('#user_dobday').val();
    		var user_dobmonth=$('#user_dobmonth').val();
    		var user_dobyear=$('#user_dobyear').val();
    		var user_address=$('#user_address').val();
    		var user_city=$('#user_city').val();
    		var user_pincode=$('#user_pincode').val();
    		var user_country=$('#user_country').val();
    		var org_img=$('#org_img').val();
    		$.ajax({
    				url : "{{route('userdetailupdate')}}",
    				type:'GET',
    				data : {
    							'user_title' :user_title,
    							'user_firstname' :user_firstname,
    							'user_lastname':user_lastname,
    							'user_gender' :user_gender,
    							'user_code':user_code,
    							'user_phone' :user_phone,
    							'user_dobday' :user_dobday,
    							'user_dobmonth' :user_dobmonth,
    							'user_dobyear' :user_dobyear,
    							'user_address' :user_address,
    							'user_city':user_city,
    							'user_pincode':user_pincode,
    							'user_country' :user_country,
    							'org_img':org_img,
								},
					success:function(result)
					{
						if(result=='sucess')
						{
							$('#success').text('User Details Update Sussessfully');
                       		$('#success_div').show();
						}
						else
						{
							$('#error').text('Unable to Update Details');
                        	$('#error_div').show();
						}
					}
    		});



    	})
    </script>
     <script type="text/javascript">
	    function sendFace()
	    {
	        
	        var form_data = new FormData($('#face_form')[0]);
	        
	        $.ajax(
	        {
	            url: '{{route("adminimage_upload")}}',
	            data: form_data,
	            mimeType: "multipart/form-data",
	            type: 'POST',
	            contentType: false,
	            processData: false,
	            xhr: function () {
	                    var xhr = new window.XMLHttpRequest();
	                    xhr.upload.addEventListener("progress", function (evt) {
	                        if (evt.lengthComputable) {
	                            var percentComplete = evt.loaded / evt.total;
	                            percentComplete = parseInt(percentComplete * 100);
	                             $('#bar').text(percentComplete + '%');
	                            $('#bar').css('width', percentComplete + '%');
	                        }
	                    }, false);
	                    return xhr;
	                },
	            success: function (data) 
	            {
	            	
	                if(data=="no")
	                {
	                    $('#image_error').text('Attachment should be JPEG|JPG|PNG');
	                    $('#image_error').show();
	                }
	                else
	                {
	                    $('#bar').text('0%');
	                    $('#bar').hide();
	                    $('#image_error').text('');
	                    $('#image_error').hide();
	                    $('#org_img').val(data);
	                    var path='assets/profile_image/' + data;
	                    $("#user_image").attr("src",path);
	                }
	            }
	        });
	    }
	</script>
	
	<script>
		$(document).on('keypress', '#newpassword', function()
	    {
	    	var oldpassword = $('#oldpassword').val();
	    	if(oldpassword=='')
	    	{
	    		$('#oldpassword_error').text('Please Enter Current Password');
	            $('#oldpassword_error').show();
	    	}
	    	else
	    	{
	    		$.ajax({
	    				url : "{{route('oldpasswordcheck')}}",
	    				data : {'oldpassword' : oldpassword},
	    				type : 'GET',
	    				success : function(data)
	    				{

	    					if(data=="success")
	    					{
	    						$('#oldpassword_error').text('');
	            				$('#oldpassword_error').show();
	    					}
	    					else
	    					{
	    						$('#oldpassword_error').text('Current Password is Incorrect');
	            				$('#oldpassword_error').show();
	    					}

	    				}
	    		});
	    	}
	    });
	</script>
	<script>
		$(document).on('blur', '.textfield', function()
	    {
	    	var id=this.id;
	    	
	    	if($('#'+id).val().trim()!="")
	        {
	            $('#'+id+'_error').text('');
	            $('#'+id+'_error').hide();
	        }
	        if(id=="oldpassword" && $('#'+id).val().trim()=="")
	        {
	            $('#'+id+'_error').text('Please Enter Current Password');
	            $('#'+id+'_error').show();
	        }
	        if(id=="newpassword" && $('#'+id).val().trim()=="")
	        {
	            $('#'+id+'_error').text('Please Enter New Password');
	            $('#'+id+'_error').show();
	        }
	        if(id=="oldpassword" && $('#'+id).val().trim()!="" && $('#newpassword').val().trim()!="" )
	        {
	        	var oldpassword = $('#oldpassword').val();
	        	$.ajax({
	    				url : "{{route('oldpasswordcheck')}}",
	    				data : {'oldpassword' : oldpassword},
	    				type : 'GET',
	    				success : function(data)
	    				{
	    					if(data=="success")
	    					{

	    					}
	    					else
	    					{
	    						$('#oldpassword_error').text('Current Password is Incorrect');
	            				$('#oldpassword_error').show();
	    					}

	    				}
	    		});
	        }
	        if(id=="cpassword" && $('#'+id).val().trim()=="")
	        {
	            $('#'+id+'_error').text('Please Enter Confirm Password');
	            $('#'+id+'_error').show();
	        }
	         if(id=="cpassword" && $('#'+id).val().trim()!="" && $('#newpassword').val().trim()!="" && $('#'+id).val().trim()!=$('#newpassword').val().trim())
	        {
	            $('#'+id+'_error').text('New Password should be same as Retype Password');
	            $('#'+id+'_error').show();
	        }
	    });
	</script>
	<script>
		$(document).on('click','#passwordchange',function(){
			
			var oldpassword= $('#oldpassword').val();
			var newpassword = $('#newpassword').val();
			var cpassword = $('#cpassword').val();
			if(oldpassword=='')
			{
				 $('#oldpassword_error').text('Please Enter Current Password');
	            $('#oldpassword_error').show();
			}
			else if(newpassword=="")
			{
				$('#newpassword_error').text('Please Enter New Password');
	            $('#newpassword_error').show();
			}
			else if(oldpassword!='' && newpassword!="")
			{

				$.ajax({
	    				url : "{{route('oldpasswordcheck')}}",
	    				data : {'oldpassword' : oldpassword},
	    				type : 'GET',
	    				success : function(data)
	    				{

	    					if(data=="success")
	    					{
	    						$('#oldpassword_error').text('');
	            				$('#oldpassword_error').show();
	            				if(cpassword=="")
	            				{
	            					$('#cpassword_error').text('Please Enter Confirm Password');
	            					$('#cpassword_error').show();
	            				}
	            				else if(newpassword != cpassword)
	            				{
	            					$('#cpassword_error').text('New Password should be same as Retype Password');
  									$('#cpassword_error').show();
	            				}
	            				else
	            				{
	            					$.ajax({
		            							url : '{{route("usernewpassword")}}',
		            							data : {'newpassword' : newpassword},
		            							type : 'GET',
		            							success : function(response)
		            							{
		            									if(response=='success')
														{
															$('#success1').text('Password Change Successfully');
								                       		$('#success_div1').show();
								                       		window.setTimeout(function(){
															 window.location.href = "{{route('logout')}}";

													   		 }, 5000);
								                       		
														}
														else
														{
															$('#error1').text('Unable to Change Password');
								                        	$('#error_div1').show();
														}
		            							}
	            							});
	            				}

	    					}
	    					else
	    					{
	    						$('#oldpassword_error').text('Current Password is Incorrect');
	            				$('#oldpassword_error').show();
	    					}

	    				}
	    		});
				    
				  
			}

		});
	</script>


</body>

</html>