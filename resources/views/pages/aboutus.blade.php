@include('pages.include.header')

<body>
	<style>
		.about-list .footer_nav a {
    		color: #8f50fb;}
		.intro {
			width: 100%;
			padding-top: 100px;
			padding-bottom: 0px;

		}

		.add_content {
			z-index: 9;
		}

		.about-header {
			background: url("{{asset('assets/images/about-h.jpg')}}");
			height: 450px;
			margin-top: 126px;
			position: relative;
			background-repeat: no-repeat;
			background-size: cover;
			background-position: center center;
		}

		h1.about-title {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			color: white;
			background: rgba(49, 18, 75, 0.8);
			padding: 10px 35px;
		}

		h1.h1-title {
			color: white;
			font-size: 20px;
		}

		.milestones {
			background: white;
		}

		.content-overlay {
			background: rgba(49, 18, 75, 0.8);
			position: absolute;
			width: 100%;
			height: 100%;
			top: 0;
			left: 0;
			z-index: 1;
		}

		p.p-para {
			font-size: 21px;
			color: #ffffff;
			background: linear-gradient(to right, #fa9e1b, #8d4fff, #fa9e1b);
			padding: 10px;
			border-radius: 5px;
		}

		ul.points {

			text-align: initial;
		}

		.column {
			column-count: 2;
		}

		ul.points li {
			padding: 10px;
			color: #8f50fb;
			font-size: 16px;
			font-weight: 600;
		}

		ul.points li:before {
			/* font-family: fontawesome; */
			font-family: FontAwesome;
			content: "\f105";
			display: inline-block;
			padding-right: 5px;
			color: #f49a26;
		}

		.milestone_icon img {
			width: auto;
			height: 50PX;
		}

		@media screen and (max-width:768px) {
			.column {
				column-count: 1;
			}

			ul.points {
				text-align: left !important;
			}
			.button.intro_button {
    position: relative;
    bottom: 0;
    left: 33%;
}
.add_content {
    padding: 10px !important;
}
		}
	</style>
	<div class="super_container">
		<!-- Header -->
		@include ('pages.include.topheader')
		<div class="about-header">
			<h1 class="about-title">About Us</h1>
		</div>
		<!-- Intro -->
		<div class="intro">
			<div class="container">
				<div class="row">
					<div class="col-lg-7">
						<div class="intro_image"><img src="{{ asset('assets/images/intro.png') }}" alt=""></div>
					</div>
					<div class="col-lg-5">
						<div class="intro_content">
							<div class="intro_title">WE ARE TRAVOWEB</div>
							<!-- <p class="intro_text">Travo Web is a Pty Limited licensed travel agent. Travo Web is not a
								direct supplier of products and services,
								or controlling the costs appropriate to any of the travel alternatives (Flights,
								accommodation services, car rentals, cruise packages).
								Travo Web has no obligation regarding any additional or mentioned servicesprovided or
								not provided by any other party.
								The tickets, vouchers, receipts and coupons are allotted suitable duties and terms and
								conditions of offer of providers and these terms.
								They are issued by us as operator as it were.We do not extend or guarantee for special
								airfare or discounted promotions.</p> -->

								<div class="footer_content footer_menu about-list">
									<ul class="clearfix">
										<li class="footer_nav"><a href="">1000+ Destinations</a></li>
										<li class="footer_nav"><a href="">Super Fast Booking</a></li>
										<li class="footer_nav"><a href="">Inclusive Packages</a></li>
										<li class="footer_nav"><a href="">Latest Model Vehicles</a></li>
										<li class="footer_nav"><a href="">Best Price In The Industry</a></li>
										<li class="footer_nav"><a href="">Accesibility managment</a></li>
									</ul>
								</div>

							<div class="button intro_button" style="position:relative;bottom: 0;left: 25%;">
								<div class="button_bcg"></div><a href="https://travoweb.com/">explore
									now<span></span><span></span><span></span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="stats">
				<div class="container">
					<div class="row">
						<div class="col text-center">
							<div class="section_title">Our Offerings</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-10 offset-lg-1 text-center">
							<p class="stats_text">While cost-effective Southeast Asia holiday packages are our domain,
								we also have other exciting tour packages in our kitty, including India, Australia,
								Dubai, Macau, China, Egypt, Mauritius, United States and Europe. With our reasonably
								priced packages and well-thought itineraries, we bring people who want to travel the
								world and expand their horizons closer to their dreams. We offer so much more than just
								affordable packages and destination information.</p>
							<p class="p-para">Our wide array of travel-related services includes</p>
							<div class="column">
								<ul class="points">
									<li>Domestic and international air ticketing</li>
									<li>
										Domestic and international holiday tour packages
									</li>
									<li>
										Inbound tour operation services
									</li>


								</ul>
								<ul class="points" style="text-align:right">

									<li>
										Corporate travel
									</li>
									<li>
										Car hire services
									</li>
									<li>
										Hotel reservations
									</li>

								</ul>
							</div>

						</div>
					</div>

				</div>
			</div>

			<!-- Add -->
			<div class="add">
				<div class="row">
					<div class="col" style="position:relative">
						<div class="content-overlay"></div>
						<div class="add_container">

							<div class="add_background"
								style="background-image:url({{ asset('assets/images/add.jpg') }}) "></div>
							<div class="add_content">
								<h1 class="h1-title">IT’S A BIG WORLD OUT THERE</h1>
								<h1 class="add_title" style="padding-left:0">GO EXPLORE</h1>
								<div class="add_subtitle">The reason why many people travel is because they love an
									adventure. There are several different types of adventures to choose from when you
									travel. An example of this is travelling to different beaches; some people enjoy
									diving while others prefer surfing.</div>
								<div class="button add_button">
									<div class="button_bcg"></div><a href="https://travoweb.com/">explore
										now<span></span><span></span><span></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Milestones -->
			<div class="milestones">
				<div class="container">
					<div class="row">

						<!-- Milestone -->
						<div class="col-lg-3 milestone_col">
							<div class="milestone text-center">
								<div class="milestone_icon"><img src=" {{ asset('assets/images/hotel.png') }}" alt="">
								</div>
								<div class="milestone_counter" data-end-value="255">0</div>
								<div class="milestone_text">Hotels</div>
							</div>
						</div>

						<!-- Milestone -->
						<div class="col-lg-3 milestone_col">
							<div class="milestone text-center">
								<div class="milestone_icon"><img src="{{ asset('assets/images/flight.png') }}" alt="">
								</div>
								<div class="milestone_counter" data-end-value="1176">0</div>
								<div class="milestone_text">Flights</div>
							</div>
						</div>

						<!-- Milestone -->
						<div class="col-lg-3 milestone_col">
							<div class="milestone text-center">
								<div class="milestone_icon"><img src="{{ asset('assets/images/pack.png') }}" alt="">
								</div>
								<div class="milestone_counter" data-end-value="39">0</div>
								<div class="milestone_text">Packages</div>
							</div>
						</div>

						<!-- Milestone -->
						<div class="col-lg-3 milestone_col">
							<div class="milestone text-center">
								<div class="milestone_icon"><img src="{{ asset('assets/images/customer.png') }}" alt="">
								</div>
								<div class="milestone_counter" data-end-value="127">0</div>
								<div class="milestone_text">Clients</div>
							</div>
						</div>

					</div>
				</div>
			</div>
			<!-- Footer -->

			@include ('pages.include.footer')
			@include ('pages.include.copyright')
		</div>
	</div>
</body>

</html>