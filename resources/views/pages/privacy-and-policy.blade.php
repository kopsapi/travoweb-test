@include('pages.include.header')

<body>
    <style>
        .intro {
            width: 100%;
            padding-top: 100px;
            padding-bottom: 0px;

        }

        .add_content {
            z-index: 9;
        }

        .about-header {
            background: url("{{asset('assets/images/about-h.jpg')}}");
            height: 450px;
            margin-top: 126px;
            position: relative;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center center;
        }

        h1.about-title {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
            background: rgba(49, 18, 75, 0.8);
            padding: 10px 35px;
        }

        .intro_title {
            text-align: center;
        }

        ul.n-ul {
            margin-top: 30px;
            text-align: left;
        }

        ul.n-ul li {
            padding: 10px 20px;
            color: #9555ef;
            font-size: 15px;
            font-weight: 500;
        }

        ul.n-ul li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
            font-size: 20px;
            font-weight: 900;
        }

        .intro {
            background: white;
        }

        h1.h1-title {
            color: white;
            font-size: 20px;
        }

        .milestones {
            background: white;
        }

        .content-overlay {
            background: rgba(49, 18, 75, 0.8);
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 1;
        }

        p.p-para {
            font-size: 21px;
            color: #ffffff;
            background: linear-gradient(to right, #fa9e1b, #8d4fff, #fa9e1b);
            padding: 10px;
            border-radius: 5px;
        }

        ul.points {

            text-align: initial;
        }

        .column {
            column-count: 2;
        }

        ul.points li {
            padding: 10px;
            color: #8f50fb;
            font-size: 16px;
            font-weight: 600;
        }

        ul.points li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
        }

        .milestone_icon img {
            width: auto;
            height: 50PX;
        }

        @media screen and (max-width:768px) {
            .column {
                column-count: 1;
            }

            ul.points {
                text-align: left !important;
            }
        }

        div.cstm-accordion .card-header {
            /* background: black; */
            font-size: 21px;
            color: #ffffff;
            background: white;
            padding: 10px 30px;
            border-radius: 5px;
            border-radius: 2px;
            border: none;
        }

        div.cstm-accordion .card-header a {
            color: #fa9e1b;
            font-size: 15px;
            display: block;
            text-align: center;
            font-weight: 500;
            text-transform: uppercase;
        }

        div.cstm-accordion .card {
            /* border: none !IMPORTANT; */
            margin-bottom: 10px;
            border: 1px solid #dadada;
        }
    </style>
    <div class="super_container">
        <!-- Header -->
        @include ('pages.include.topheader')
        <div class="about-header">
            <h1 class="about-title">Privacy And Policy</h1>
        </div>
        <!-- Intro -->
        <div class="intro">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12">
                        <div class="intro_content">
                            <div class="intro_title">Privacy And Policy</div>
                            <p class="intro_text">
                                Travoweb Travels Private Limited ("Travoweb") (hereinafter “Travoweb”) website name
                                Travoweb.com recognizes the importance of privacy of its users and also of maintaining
                                confidentiality of the information provided by its users as a responsible data
                                controller and data processer.
                                This Privacy Policy provides for the practices for handling and securing user's Personal
                                Information (defined hereunder) by Travoweb and its subsidiaries and affiliates.
                                This Privacy Policy is applicable to any person (‘User’) who purchase, intend to
                                purchase, or inquire about any product(s) or service(s) made available by Travoweb
                                through any of Travoweb’s customer interface channels including its website, mobile
                                site, mobile app & offline channels including call centers and offices (collectively
                                referred herein as "Sales Channels").
                                For the purpose of this Privacy Policy, wherever the context so requires "you" or "your"
                                shall mean User and the term "we", "us", "our" shall mean Travoweb. For the purpose of
                                this Privacy Policy, Website means the website(s), mobile site(s) and mobile app(s).
                                By using or accessing the Website or other Sales Channels, the User hereby agrees with
                                the terms of this Privacy Policy and the contents herein. If you disagree with this
                                Privacy Policy please do not use or access our Website or other Sales Channels.
                                This Privacy Policy does not apply to any website(s), mobile sites and mobile apps of
                                third parties, even if their websites/products are linked to our Website. User should
                                take note that information and privacy practices of Travoweb’s business partners,
                                advertisers, sponsors or other sites to which Travoweb provides hyperlink(s), may be
                                materially different from this Privacy Policy. Accordingly, it is recommended that you
                                review the privacy statements and policies of any such third parties with whom they
                                interact.
                                This Privacy Policy is an integral part of your User Agreement with Travoweb and all
                                capitalized terms used, but not otherwise defined herein, shall have the respective
                                meanings as ascribed to them in the User Agreement.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="stats">
                <div class="container">

                    <div class="row">
                        <div class="col-lg-10 offset-lg-1 text-center">


                            <div id="accordion" class="cstm-accordion">

                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#collapseOne">
                                            USERS OUTSIDE THE GEOGRAPHICAL LIMITS OF INDIA
                                        </a>
                                    </div>
                                    <div id="collapseOne" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                Please note that the data shared with Travoweb shall be primarily
                                                processed in India and such other jurisdictions where a third party
                                                engaged by Travoweb may process the data on Travoweb’s behalf. By
                                                agreeing to this policy, you are providing Travoweb with your explicit
                                                consent to process your personal information for the purpose(s) defined
                                                in this policy. The data protection regulations in India or such other
                                                jurisdictions mentioned above may differ from those of your country of
                                                residence.
                                                If you have any concerns in the processing your data and wish to
                                                withdraw your consent, you may do so by writing to the following email
                                                id: info@Travoweb.com. However, if such processing of data is essential
                                                for us to be able to provide service to you, then we may not be able to
                                                serve or confirm your bookings after your withdrawal of consent. For
                                                instance, if you want to book any international holiday package in fixed
                                                departures (group bookings), then certain personal information of yours
                                                like contact details, gender, dietary preferences, choice of room with
                                                smoking facility, any medical condition which may require specific
                                                attention or facility etc. may have to be shared by us with our vendors
                                                in each city where you will stay, and they may further process this
                                                information for making suitable arrangements for you during the holiday.
                                                Such sharing and processing of information may extend to the hotel where
                                                you will stay or the tour manager who will be your guide during the
                                                travel.
                                                A withdrawal of consent by you for us to process your information may:
                                                <ul class="n-ul">
                                                    <li>severely inhibit our ability to serve you properly and in such
                                                        case, we may have to refuse the booking altogether, or</li>
                                                    <li>Unreasonably restrict us to service your booking (if a booking
                                                        is already made) which may further affect your trip or may
                                                        compel us to cancel your booking.</li>
                                                </ul>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                            TYPE OF INFORMATION (WE COLLECT)
                                        </a>
                                    </div>
                                    <div id="collapseTwo" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                The information as detailed below is collected for us to be able to
                                                provide the services chosen by you and also to fulfill our legal
                                                obligations as well as our obligations towards third parties as per our
                                                User Agreement.
                                                "Personal Information" of User shall include the information shared by
                                                the User and collected by us for the following purposes:

                                                Registration on the Website/ mobile application: Information which you
                                                provide while subscribing to or registering on the Website, mobile
                                                application including but not limited to information about your personal
                                                identity such as name, gender, marital status, religion, age etc., your
                                                contact details such as your email address, postal addresses, frequent
                                                flyer number, telephone (mobile or otherwise) and/or fax numbers. The
                                                information may also include information such as your banking details
                                                (including credit/debit card) and any other information relating to your
                                                income and/or lifestyle; billing information payment history etc. (as
                                                shared by you).

                                                Other information: We many also collect some other information and
                                                documents including but not limited to:
                                                <ul class="n-ul">
                                                    <li>Transactional history (other than banking details) about your
                                                        e-commerce activities, buying behavior.
                                                    <li>Your usernames, passwords, email addresses and other
                                                        security-related information used by you in relation to our
                                                        Services.</li>
                                                    <li>Data either created by you or by a third party and which you
                                                        wish to store on our servers such as image files, documents etc.
                                                    </li>
                                                    <li>Data available in public domain or received from any third party
                                                        including social media channels, including but not limited to
                                                        personal or non-personal information from your linked social
                                                        media channels (like name, email address, friend list, profile
                                                        pictures or any other information that is permitted to be
                                                        received as per your account settings) as a part of your account
                                                        information.</li>
                                                    <li>Information pertaining any other traveler(s) for who you make a
                                                        booking through your registered Travoweb account. In such case,
                                                        you must confirm and represent that each of the other
                                                        traveler(s) for whom a booking has been made, has agreed to have
                                                        the information shared by you disclosed to us and further be
                                                        shared by us with the concerned service provider(s).</li>
                                                    <li>If you request Travoweb to provide visa related services, then
                                                        copies of your passport, bank statements, originals of the
                                                        filled in application forms, photographs, and any other
                                                        information which may be required by the respective embassy to
                                                        process your visa application.</li>

                                                </ul>

                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                            HOW WE USE YOUR PERSONAL INFORMATION:
                                        </a>
                                    </div>
                                    <div id="collapseThree" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">The Personal Information collected maybe used in the
                                                following manner:
                                                While making a booking
                                                While making a booking, we may use Personal Information including,
                                                payment details which include cardholder name, credit/debit card number
                                                (in encrypted form) with expiration date, banking details, wallet
                                                details etc. as shared and allowed to be stored by you. We may also use
                                                the information of travelers list as available in or linked with your
                                                account. This information is presented to the User at the time of making
                                                a booking to enable you to complete your bookings expeditiously.
                                                We may also use your Personal Information for several reasons including
                                                but not limited to:
                                                <ul class="n-ul">
                                                    <li>confirm your reservations with respective service providers.
                                                    </li>
                                                    <li>keep you informed of the transaction status.</li>
                                                    <li>send booking confirmations either via sms or Whatsapp or any
                                                        other messaging service.</li>
                                                    <li>send any updates or changes to your booking(s).</li>
                                                    <li>allow our customer service to contact you, if necessary.</li>
                                                    <li>customize the content of our website, mobile site and mobile
                                                        app.</li>
                                                    <li>request for reviews of products or services or any other
                                                        improvements.</li>
                                                    <li>send verification message(s) or email(s).</li>
                                                    <li>validate/authenticate your account and to prevent any misuse or
                                                        abuse.</li>
                                                    <li>contact you on your birthday/anniversary to offer a special gift
                                                        or offer.</li>
                                                </ul>
                                                <p><b>Surveys:</b></p>

                                                We value opinions and comments from our Users and frequently conduct
                                                surveys, both online and offline. Participation in these surveys is
                                                entirely optional. Typically, the information received is aggregated,
                                                and used to make improvements to Website, other Sales Channels, services
                                                and to develop appealing content, features and promotions for members
                                                based on the results of the surveys. Identity of the survey participants
                                                is anonymous unless otherwise stated in the survey.
                                                Marketing Promotions, Research and Programs:
                                                Marketing promotions, research and programs help us to identify your
                                                preferences, develop programs and improve user experience. Travoweb
                                                frequently sponsors promotions to give its Users the opportunity to win
                                                great travel and travel related prizes. Personal Information collected
                                                by us for such activities may include contact information and survey
                                                questions. We use such Personal Information to notify contest winners
                                                and survey information to develop promotions and product improvements.
                                                As a registered User, you will also occasionally receive updates from us
                                                about fare sales in your area, special offers, new Travoweb services,
                                                other noteworthy items (like savings and benefits on airfares, hotel
                                                reservations, holiday packages and other travel services) and marketing
                                                programs.
                                                In addition, you may look forward to receiving periodic marketing
                                                emails, newsletters and exclusive promotions offering special deals.
                                                From time to time we may add or enhance services available on the
                                                Website. To the extent these services are provided, and used by you, we
                                                will use the Personal Information you provide to facilitate the
                                                service(s) requested. For example, if you email us with a question, we
                                                will use your email address, name, nature of the question, etc. to
                                                respond to your question. We may also store such Personal Information to
                                                assist us in making the Website the better and easier to use for our
                                                Users.
                                                Travoweb may from time to time launch reward programs by way of which
                                                users may stand to win travel related rewards or other rewards. We may
                                                use your Personal Information to enroll you in the rewards program and
                                                status of the same will be visible each time you log in to the Website.
                                                Depending on the reward program, each time you win a reward, Travoweb
                                                may share your Personal Information with a third party that will be
                                                responsible for fulfilling the reward to you. You may however choose to
                                                opt out of such reward programs by writing to us. For various purposes
                                                such as fraud detection, offering bookings on credit etc., we at times
                                                may verify information of customers on selective basis, including their
                                                credit information.


                                            </p>


                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse4">
                                            HOW LONG DO WE KEEP YOUR PERSONAL INFORMATION?
                                        </a>
                                    </div>
                                    <div id="collapse4" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                Travoweb will retain your Personal Information on its servers for as
                                                long as is reasonably necessary for the purposes listed in this policy
                                                exlusive travel offers, wallet cash-backs, etc. It also allows
                                                facilitating your Facebook and Google+ login.
                                                <p><b>Location:</b></p> This permission enables us to give you the
                                                benefit of location specific deals and provide you a personalized
                                                in-funnel experience. When you launch Travoweb app to make a travel
                                                booking, we auto-detect your location so that your nearest airport or
                                                city is auto-filled. We also require this permission to recommend you
                                                nearest hotels in case you are running late and want to make a quick
                                                last minute booking for the nearest hotel. Your options are personalized
                                                basis your locations. For international travel, this enables us to
                                                determine your time zone and provide information accordingly

                                                <p><b>SMS:</b></p> If you allow us to access your SMS, we read your SMS
                                                to auto fill or pre populate ‘OTP’ while making a transaction and to
                                                validate your mobile number. This provides you a seamless purchase
                                                experience while making a booking and you don’t need to move out of the
                                                app to read the SMS and then enter it in the app.

                                                <p><b>Phone:</b> </p>The app requires access to make phone calls so that
                                                you can make phone calls to hotels, airlines and our customer contact
                                                centers directly through the app.

                                                <p><b>Contacts:</b></p> If you allow us to access your contacts, it
                                                enables us to provide a lot of social features to you such as sharing
                                                your hotel/ flight/ holidays with your friends, inviting your friends to
                                                try our app, send across referral links to your friends, etc. We may
                                                also use this information to make recommendations for hotels where your
                                                friends have stayed. This information will be stored on our servers and
                                                synced from your phone.

                                                <p><b>Photo/ Media/ Files:</b></p> The libraries in the app use these
                                                permissions to allow map data to be saved to your phone's external
                                                storage, like SD cards. By saving map data locally, your phone doesn't
                                                need to re-download the same map data every time you use the app. This
                                                provides you a seamless Map based Hotel selection experience, even on
                                                low bandwidth network.

                                                <p><b>Wi-Fi connection information:</b></p> When you allow us the
                                                permission to detect your Wi-Fi connection, we optimize your experience
                                                such as more detailing on maps, better image loading, more hotel/
                                                flights/ package options to choose from, etc.

                                                <p><b>Device ID & Call information:</b> </p>This permission is used to
                                                detect your Android ID through which we can uniquely identify users. It
                                                also lets us know your contact details using which we pre-populate
                                                specific fields to ensure a seamless booking experience.

                                                <p><b>Calendar:</b></p> This permission enables us to put your travel
                                                plan on your calendar.
                                                <p><b>IOS Permissions:</b></p>
                                                <p><b>Notifications:</b></p> If you opt in for notifications, it enables
                                                us to send across exclusive deals, promotional offers, travel related
                                                updates, etc. on your device. If you do not opt for this, updates for
                                                your travel like PNR status, booking confirmation, refund (in case of
                                                cancellation), etc. will be sent through SMS.

                                                <p><b>Contacts:</b></p> If you opt in for contacts permission, it
                                                enables us to provide a lot of social features to you such as sharing
                                                your hotel/ flight/ holidays with your friends, inviting your friends to
                                                try our app, send across referral links to your friends, etc. We will
                                                also use this information to make recommendations for hotels where your
                                                friends have stayed. This information will be stored on our servers and
                                                synced from your phone.

                                                <p><b>Location:</b></p> This permission enables us to give you the
                                                benefit of location specific deals and provide you a personalized
                                                in-funnel experience. When you launch our app to make a travel booking,
                                                we auto-detect your location so that your nearest Airport or City is
                                                auto-filled. We require this permission to recommend your nearest hotels
                                                in case you are running late and want to make a quick last minute
                                                booking for the nearest hotel. Your options are personalized basis your
                                                locations. For international travel, this enables us to determine your
                                                time zone and provide information accordingly.

                                                <p><b>Cookies</b></p>
                                                This website/Application uses Cookies to save the User's session and to
                                                carry out other activities that are strictly necessary for the operation
                                                of this Application. Some of the services listed above collect
                                                statistics in an anonym zed and aggregated form and may not require the
                                                consent of the User or may be managed directly by the Owner – depending
                                                on how they are described – without the help of third parties. With
                                                regard to Cookies installed by third parties, Users can manage their
                                                preferences and withdrawal of their consent by clicking the related
                                                opt-out link (if provided), by using the means provided in the third
                                                party's privacy policy, or by contacting the third party.
                                                Notwithstanding the above, the Owner informs that Users may follow the
                                                instructions provided on the subsequently linked initiatives by the EDAA
                                                (EU), the Network Advertising Initiative (US), Digital Advertising
                                                Alliance (US) and the DAAC (Canada) or other similar services. Since the
                                                installation of third-party Cookies and other tracking systems through
                                                the services used within this website/Application cannot be technically
                                                controlled by the Owner, any specific references to Cookies and tracking
                                                systems installed by third parties are to be considered indicative. In
                                                order to obtain complete information, the User is kindly requested to
                                                consult the privacy policy for the respective third-party services
                                                listed in this document. Given the objective complexity surrounding the
                                                identification of technologies based on Cookies, Users are encouraged to
                                                contact the Owner should they wish to receive any further information on
                                                the use of Cookies by the Application.
                                                Additional information about Data collection and processing
                                                A. Legal action
                                                The User's Personal Data may be used for legal purposes by the Owner in
                                                court or in the stages leading to possible legal action arising from
                                                improper use of the Application or the related Service. The User
                                                declares to be aware that the Owner may be required to reveal personal
                                                data upon request of public authorities.
                                                B. System logs and maintenance
                                                For operation and maintenance purposes, the Application and any
                                                third-party services may collect files that record interaction with the
                                                Application (system logs) use other Personal Data (such as the IP
                                                address) for this purpose.
                                                C. How "Do Not Track" requests are handled
                                                The Application does not support "Do Not Track" requests. To determine
                                                whether any of the third-party services it uses honor the “Do Not Track”
                                                requests, please read their privacy policies.
                                                D. Changes to this privacy policy
                                                The Owner reserves the right to make changes to this privacy policy at
                                                any time by giving notice to its Users on this page and possibly within
                                                the Application and/or – as far as technically and legally feasible –
                                                sending a notice to Users via any contact information available to the
                                                Owner. If a User objects to any of the changes to the policy, the User
                                                must cease using the Application and can request that the Owner remove
                                                the Personal Data. Unless stated otherwise, the then-current privacy
                                                policy applies to all Personal Data the Owner has about Users.

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse5">
                                            HOW WE PROTECT YOUR PERSONAL INFORMATION?
                                        </a>
                                    </div>
                                    <div id="collapse5" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">All payments on the Website are secured. This means all
                                                Personal Information you provide is transmitted using encryption (you
                                                may add the name of any coding system that you use). Website has
                                                stringent security measures in place to protect the loss, misuse, and
                                                alteration of the information under our control. Whenever you change or
                                                access your account information, we offer the use of a secure server.
                                                Once your information is in our possession we adhere to strict security
                                                guidelines, protecting it against unauthorized access.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse6">
                                            WITHDRAWAL OF CONSENT AND PERMISSION
                                        </a>
                                    </div>
                                    <div id="collapse6" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                You may withdraw your consent to submit any or all Personal Information
                                                or decline to provide any permission on its Website as covered above at
                                                any time. In case, you choose to do so then your access to the Website
                                                may be limited, or we might not be able to provide the services to you.
                                                You may withdraw your consent by sending an email to info@Travoweb.com
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse7">
                                            YOUR RIGHTS QUA PERSONAL INFORMATION
                                        </a>
                                    </div>
                                    <div id="collapse7" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                You may access your Personal Information from your user account with
                                                Travoweb. You may also correct your personal information or delete such
                                                information (except some mandatory fields) from your user account
                                                directly. If you don’t have such a user account, then you write to
                                                info@Travoweb.com
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse8">
                                            ELIGIBILITY TO TRANSACT WITH Travoweb
                                        </a>
                                    </div>
                                    <div id="collapse8" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                You must atleast 18 years of age to transact directly with Travoweb and
                                                also to consent to the processing of your personal data.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse9">
                                            CHANGES TO THE PRIVACY POLICY
                                        </a>
                                    </div>
                                    <div id="collapse9" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                We reserve the rights to revise the Privacy Policy from time to time to
                                                suit various legal, business and customer requirement. We will duly
                                                notify the users as may be necessary.
                                                You may always submit concerns regarding this Privacy Policy via email
                                                to us . Travoweb shall endeavor to respond to all reasonable concerns
                                                and inquires.

                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <!-- Footer -->

            @include ('pages.include.footer')
            @include ('pages.include.copyright')
        </div>
    </div>
</body>

</html>