@include('pages.include.header')

<body>
    <style>
        .intro {
            width: 100%;
            padding-top: 100px;
            padding-bottom: 0px;

        }

        .add_content {
            z-index: 9;
        }

        .about-header {
            background: url("{{asset('assets/images/about-h.jpg')}}");
            height: 450px;
            margin-top: 126px;
            position: relative;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center center;
        }

        h1.about-title {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
            background: rgba(49, 18, 75, 0.8);
            padding: 10px 35px;
        }

        .intro_title {
            text-align: center;
        }

        ul.n-ul {
            margin-top: 30px;
            text-align: left;
        }

        ul.n-ul li {
            padding: 10px 20px;
            color: #9555ef;
            font-size: 15px;
            font-weight: 500;
        }

        ul.n-ul li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
            font-size: 20px;
            font-weight: 900;
        }

        .intro {
            background: white;
        }

        h1.h1-title {
            color: white;
            font-size: 20px;
        }

        .milestones {
            background: white;
        }

        .content-overlay {
            background: rgba(49, 18, 75, 0.8);
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 1;
        }

        p.p-para {
            font-size: 21px;
            color: #ffffff;
            background: linear-gradient(to right, #fa9e1b, #8d4fff, #fa9e1b);
            padding: 10px;
            border-radius: 5px;
        }

        ul.points {

            text-align: initial;
        }

        .column {
            column-count: 2;
        }

        ul.points li {
            padding: 10px;
            color: #8f50fb;
            font-size: 16px;
            font-weight: 600;
        }

        ul.points li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
        }

        .milestone_icon img {
            width: auto;
            height: 50PX;
        }

        @media screen and (max-width:768px) {
            .column {
                column-count: 1;
            }

            ul.points {
                text-align: left !important;
            }
        }

        div.cstm-accordion .card-header {
            /* background: black; */
            font-size: 21px;
            color: #ffffff;
            background: white;
            padding: 10px 30px;
            border-radius: 5px;
            border-radius: 2px;
            border: none;
        }

        div.cstm-accordion .card-header a {
            color: #fa9e1b;
            font-size: 15px;
            display: block;
            text-align: center;
            font-weight: 500;
            text-transform: uppercase;
        }

        div.cstm-accordion .card {
            /* border: none !IMPORTANT; */
            margin-bottom: 10px;
            border: 1px solid #dadada;
        }

        .heading-p {
            font-size: 17px;
            color: #fa9e1b;
            font-weight: 700;
            text-transform: uppercase;
        }
        .search {
    width: 100%;
    height: auto;
    /* background: linear-gradient(to right, #fa9e1b, #8d4fff); */
    z-index: 9;
    position: relative;
    top: 0;
    margin-bottom: 80px;
}
.search_tabs_container {
    position: relative;
    bottom: 100%;
    left: 0;
    width: 100%;
}
table.d-table tr td {
    padding: 10px;
    border: 1px solid #e0e0e0;
    color: black;
}
table.d-table {
    width: 100%;
    background: white;
}
table.d-table tr th {
    background: #4a3a7c;
    color: white;
    padding: 15px;
    border: 1px solid #4a3a7c;
}
.search_panel.active {
    background: #ffffff !IMPORTANT;
    border: 1px solid #cacaca;
    height: 500px;
    overflow: auto;
}
.search_tab:first-child {
    border-top-left-radius: 0;
    padding-left: 7px;
}
.search_tab:last-child {
    border-top-right-radius: 0;
    padding-right: 7px;
}
/* width */
.search_panel.active::-webkit-scrollbar {
    width: 7px;
}
.search_panel.active::-webkit-scrollbar-thumb {
    background: #7961c4;
    border-radius: 30px;
}
.search_panel.active::-webkit-scrollbar-track {
    background: #e4dbff;
}

/* Handle on hover */
.search_panel.active::-webkit-scrollbar-thumb:hover {
  background: #6652a5;
}
.search_tab.d-flex.flex-row.align-items-center.justify-content-lg-center.justify-content-start.active {
    background: #fa9e1b;
}
    </style>
    <div class="super_container">
        <!-- Header -->
        @include ('pages.include.topheader')
        <div class="about-header">
            <h1 class="about-title">Domestic Airline</h1>
        </div>
        <!-- Intro -->
        <div class="intro">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12">
                        <div class="intro_content">
                            <div class="intro_title">Domestic Airline</div>

                            <p class="heading-p" style="text-align:center">Search for three-letter airport code by city,
                                country, or code.</p>


                        </div>
                        <div class="search">

                            <div class="container fill_height">
                                <div class="row fill_height">
                                    <div class="col fill_height">

                                        <div class="search_panel active">
                                            <table class="d-table">
 
                                                <tbody>
                                                <tr>
                                                 <th>City Name</th>
                                                 <th>Airport Code</th>
                                                 <th>Airport Name</th>
                                                 <th>Country name</th>
                                                </tr>
                                                <tr>
                                                 <td>Agartala</td>
                                                 <td>IXA</td>
                                                 <td>Singerbhil</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Agatti Island</td>
                                                 <td>AGX</td>
                                                 <td>Agatti Island</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Agra</td>
                                                 <td>AGR</td>
                                                 <td>Kheria</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Ahmedabad</td>
                                                 <td>AMD</td>
                                                 <td>Ahmedabad</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Aizawl</td>
                                                 <td>AJL</td>
                                                 <td>Aizawl</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Akola</td>
                                                 <td>AKD</td>
                                                 <td>Akola</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Allahabad</td>
                                                 <td>IXD</td>
                                                 <td>Bamrauli</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Along</td>
                                                 <td>IXV</td>
                                                 <td>Along</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Amritsar</td>
                                                 <td>ATQ</td>
                                                 <td>Raja Sansi</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Aurangabad</td>
                                                 <td>IXU</td>
                                                 <td>Chikkalthana</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Bagdogra</td>
                                                 <td>IXB</td>
                                                 <td>Bagdogra</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Balurghat</td>
                                                 <td>RGH</td>
                                                 <td>Balurghat</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Bangalore</td>
                                                 <td>BLR</td>
                                                 <td>Hal</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Bareli</td>
                                                 <td>BEK</td>
                                                 <td>Bareli</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Belgaum</td>
                                                 <td>IXG</td>
                                                 <td>Sambre</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Bellary</td>
                                                 <td>BEP</td>
                                                 <td>Bellary</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Bhatinda</td>
                                                 <td>BUP</td>
                                                 <td>Bhatinda</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Bhavnagar</td>
                                                 <td>BHU</td>
                                                 <td>Bhavnagar</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Bhopal</td>
                                                 <td>BHO</td>
                                                 <td>Bhopal</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Bhubaneswar</td>
                                                 <td>BBI</td>
                                                 <td>Bhubaneswar</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Bhuj</td>
                                                 <td>BHJ</td>
                                                 <td>Rudra Mata</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Bhuntar</td>
                                                 <td>KUU</td>
                                                 <td>Kullu Manali</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Bikaner</td>
                                                 <td>BKB</td>
                                                 <td>Bikaner</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Bilaspur</td>
                                                 <td>PAB</td>
                                                 <td>Bilaspur</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Car Nicobar</td>
                                                 <td>CBD</td>
                                                 <td>Car Nicobar</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Chandigarh</td>
                                                 <td>IXC</td>
                                                 <td>Chandigarh</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Chennai/Madras</td>
                                                 <td>MAA</td>
                                                 <td>Meenambakkam</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Coimbatore</td>
                                                 <td>CJB</td>
                                                 <td>Peelamedu</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Cooch Behar</td>
                                                 <td>COH</td>
                                                 <td>Cooch Behar</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Cuddapah</td>
                                                 <td>CDP</td>
                                                 <td>Cuddapah</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Daman</td>
                                                 <td>NMB</td>
                                                 <td>Daman</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Daparizo</td>
                                                 <td>DAE</td>
                                                 <td>Daparizo</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Darjeeling</td>
                                                 <td>DAI</td>
                                                 <td>Darjeeling</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Dehra Dun</td>
                                                 <td>DED</td>
                                                 <td>Dehra Dun</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Delhi</td>
                                                 <td>DEL</td>
                                                 <td>Indira Gandhi Intl</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Deparizo</td>
                                                 <td>DEP</td>
                                                 <td>Deparizo</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Dhanbad</td>
                                                 <td>DBD</td>
                                                 <td>Dhanbad</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Dharamsala</td>
                                                 <td>DHM</td>
                                                 <td>Gaggal Airport</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Dibrugarh</td>
                                                 <td>DIB</td>
                                                 <td>Chabua</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Dimapur</td>
                                                 <td>DMU</td>
                                                 <td>Dimapur</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Diu</td>
                                                 <td>DIU</td>
                                                 <td>Diu</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Gawahati</td>
                                                 <td>GAU</td>
                                                 <td>Borjhar</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Gaya</td>
                                                 <td>GAY</td>
                                                 <td>Gaya</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Goa</td>
                                                 <td>GOI</td>
                                                 <td>Dabolim</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Gorakhpur</td>
                                                 <td>GOP</td>
                                                 <td>Gorakhpur</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Guna</td>
                                                 <td>GUX</td>
                                                 <td>Guna</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Gwalior</td>
                                                 <td>GWL</td>
                                                 <td>Gwalior</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Hissar</td>
                                                 <td>HSS</td>
                                                 <td>Hissar</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Hubli</td>
                                                 <td>HBX</td>
                                                 <td>Hubli</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Hyderabad</td>
                                                 <td>HYD</td>
                                                 <td>Begumpet Airport</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Imphal</td>
                                                 <td>IMF</td>
                                                 <td>Municipal</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Indore</td>
                                                 <td>IDR</td>
                                                 <td>Indore</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Jabalpur</td>
                                                 <td>JLR</td>
                                                 <td>Jabalpur</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Jagdalpur</td>
                                                 <td>JGB</td>
                                                 <td>Jagdalpur</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Jaipur</td>
                                                 <td>JAI</td>
                                                 <td>Sanganeer</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Jaisalmer</td>
                                                 <td>JSA</td>
                                                 <td>Jaisalmer</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Jammu</td>
                                                 <td>IXJ</td>
                                                 <td>Satwari</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Jamnagar</td>
                                                 <td>JGA</td>
                                                 <td>Govardhanpur</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Jamshedpur</td>
                                                 <td>IXW</td>
                                                 <td>Sonari</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Jeypore</td>
                                                 <td>PYB</td>
                                                 <td>Jeypore</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Jodhpur</td>
                                                 <td>JDH</td>
                                                 <td>Jodhpur</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Jorhat</td>
                                                 <td>JRH</td>
                                                 <td>Rowriah</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Kailashahar</td>
                                                 <td>IXH</td>
                                                 <td>Kailashahar</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Kamalpur</td>
                                                 <td>IXQ</td>
                                                 <td>Kamalpur</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Kandla</td>
                                                 <td>IXY</td>
                                                 <td>Kandla</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Kanpur</td>
                                                 <td>KNU</td>
                                                 <td>Kanpur</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Keshod</td>
                                                 <td>IXK</td>
                                                 <td>Keshod</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Khajuraho</td>
                                                 <td>HJR</td>
                                                 <td>Khajuraho</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Khowai</td>
                                                 <td>IXN</td>
                                                 <td>Khowai</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Kochi</td>
                                                 <td>COK</td>
                                                 <td>Kochi</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Kolhapur</td>
                                                 <td>KLH</td>
                                                 <td>Kolhapur</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Kolkata</td>
                                                 <td>CCU</td>
                                                 <td>Netaji Subhas Chandra</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Kota</td>
                                                 <td>KTU</td>
                                                 <td>Kota</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Kozhikode</td>
                                                 <td>CCJ</td>
                                                 <td>Kozhikode Airport</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Leh</td>
                                                 <td>IXL</td>
                                                 <td>Bakula Rimpoche</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Lilabari</td>
                                                 <td>IXI</td>
                                                 <td>Lilabari</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Lucknow</td>
                                                 <td>LKO</td>
                                                 <td>Amausi</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Ludhiana</td>
                                                 <td>LUH</td>
                                                 <td>Ludhiana</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Madurai</td>
                                                 <td>IXM</td>
                                                 <td>Madurai</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Malda</td>
                                                 <td>LDA</td>
                                                 <td>Malda</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Mangalore</td>
                                                 <td>IXE</td>
                                                 <td>Bajpe</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Mohanbari</td>
                                                 <td>MOH</td>
                                                 <td>Mohanbari</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Mumbai</td>
                                                 <td>BOM</td>
                                                 <td>Chhatrapati Shivaji</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Muzaffarnagar</td>
                                                 <td>MZA</td>
                                                 <td>Muzaffarnagar</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Muzaffarpur</td>
                                                 <td>MZU</td>
                                                 <td>Muzaffarpur</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Mysore</td>
                                                 <td>MYQ</td>
                                                 <td>Mysore</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Nagpur</td>
                                                 <td>NAG</td>
                                                 <td>Sonegaon</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Nanded</td>
                                                 <td>NDC</td>
                                                 <td>Nanded</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Nasik</td>
                                                 <td>ISK</td>
                                                 <td>Gandhinagar Arpt</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Neyveli</td>
                                                 <td>NVY</td>
                                                 <td>Neyveli</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Osmanabad</td>
                                                 <td>OMN</td>
                                                 <td>Osmanabad</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Pantnagar</td>
                                                 <td>PGH</td>
                                                 <td>Pantnagar</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Pasighat</td>
                                                 <td>IXT</td>
                                                 <td>Pasighat</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Pathankot</td>
                                                 <td>IXP</td>
                                                 <td>Pathankot</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Patna</td>
                                                 <td>PAT</td>
                                                 <td>Patna</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Pondicherry</td>
                                                 <td>PNY</td>
                                                 <td>Pondicherry</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Porbandar</td>
                                                 <td>PBD</td>
                                                 <td>Porbandar</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Port Blair</td>
                                                 <td>IXZ</td>
                                                 <td>Port Blair</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Pune</td>
                                                 <td>PNQ</td>
                                                 <td>Lohegaon</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Puttaparthi</td>
                                                 <td>PUT</td>
                                                 <td>Puttaprathe</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Raipur</td>
                                                 <td>RPR</td>
                                                 <td>Raipur</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Rajahmundry</td>
                                                 <td>RJA</td>
                                                 <td>Rajahmundry</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Rajkot</td>
                                                 <td>RAJ</td>
                                                 <td>Civil</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Rajouri</td>
                                                 <td>RJI</td>
                                                 <td>Rajouri</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Ramagundam</td>
                                                 <td>RMD</td>
                                                 <td>Ramagundam</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Ranchi</td>
                                                 <td>IXR</td>
                                                 <td>Ranchi</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Ratnagiri</td>
                                                 <td>RTC</td>
                                                 <td>Ratnagiri</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Rewa</td>
                                                 <td>REW</td>
                                                 <td>Rewa</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Rourkela</td>
                                                 <td>RRK</td>
                                                 <td>Rourkela</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Rupsi</td>
                                                 <td>RUP</td>
                                                 <td>Rupsi</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Salem</td>
                                                 <td>SXV</td>
                                                 <td>Salem</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Satna</td>
                                                 <td>TNI</td>
                                                 <td>Satna</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Shillong</td>
                                                 <td>SHL</td>
                                                 <td>Shillong</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Sholapur</td>
                                                 <td>SSE</td>
                                                 <td>Sholapur</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Silchar</td>
                                                 <td>IXS</td>
                                                 <td>Kumbhirgram</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Simla</td>
                                                 <td>SLV</td>
                                                 <td>Simla</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Srinagar</td>
                                                 <td>SXR</td>
                                                 <td>Srinagar</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Surat</td>
                                                 <td>STV</td>
                                                 <td>Surat</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Tezpur</td>
                                                 <td>TEZ</td>
                                                 <td>Salonibari</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Tezu</td>
                                                 <td>TEI</td>
                                                 <td>Tezu</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Thanjavur</td>
                                                 <td>TJV</td>
                                                 <td>Thanjavur</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Thiruvananthapura<span style="display:none">m</span></td>
                                                 <td>TRV</td>
                                                 <td>International</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Tiruchirapally</td>
                                                 <td>TRZ</td>
                                                 <td>Civil</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Tirupati</td>
                                                 <td>TIR</td>
                                                 <td>Tirupati</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Tuticorin</td>
                                                 <td>TCR</td>
                                                 <td>Tuticorin</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Udaipur</td>
                                                 <td>UDR</td>
                                                 <td>Dabok</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Vadodara</td>
                                                 <td>BDQ</td>
                                                 <td>Vadodara</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Varanasi</td>
                                                 <td>VNS</td>
                                                 <td>Varanasi</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Vijayawada</td>
                                                 <td>VGA</td>
                                                 <td>Vijayawada</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Vishakhapatnam</td>
                                                 <td>VTZ</td>
                                                 <td>Vishakhapatnam</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Warangal</td>
                                                 <td>WGC</td>
                                                 <td>Warangal</td>
                                                 <td>India</td>
                                                </tr>
                                                <tr>
                                                 <td>Zero</td>
                                                 <td>ZER</td>
                                                 <td>Zero</td>
                                                 <td>India</td>
                                                </tr>
                                                
                                               </tbody></table>
                                        </div>

                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->

            @include ('pages.include.footer')
            @include ('pages.include.copyright')
        </div>
    </div>
</body>

</html>