<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Package</title>
</head>


<style>
 table {
        font-family: 'Amiko', sans-serif;
        font-size: 14px;
        width: 100%;
    }

    table {
        width: 100%;
      
        border-collapse: collapse;
        border-radius: 5px;

    }

    img.pdf-h-img {
        display: block;


    }

    table tr td {
        padding: 0;
        border: none !IMPORTANT;
    }

    p.pdf-heading {
        margin: 0;
        
        padding: 15px;
       
        color: rgb(213, 33, 14);
    }

    p.basic-info b {
        color:  white;
        margin-right: 3px;

    }

    p.basic-info {
        padding: 0px;
        color:white;
    }


    p.day-p {
        margin: 0; 
        color: rgb(255, 255, 255);
        background: rgb(7, 103, 177);
        padding: 15px 0;

    }

    p.day-p span {
        background: rgb(213, 33, 14);
        padding: 15px 20px;
        margin: -15px 10px 0 0;
        color: white;
        display: block;
        float: left;

    }

    .day-content {
        margin: 0 0 0px 0;
        padding: 15px;
        background: #edeaff;
    }

    p.inclusion {
        color: #fff;
        background: rgb(213, 33, 14);
        color: white;
        padding: 15px;
        display: inline-block;
        margin-bottom: 0;

    }

    ul.i-ul {
        background: rgb(7, 103, 177);
        margin: 0;
        list-style: circle;
        color:white !important;
        padding-left: 0;
    }

    ul.i-ul li {
        padding: 20px 10px 0px 10px;
        color: rgb(213, 33, 14);
    }

    ul.i-ul li:last-child {
        padding: 20px 10px 20px 10px;
        color: rgb(213, 33, 14);
    }

    .day-img {
        width: 200px;
        height: 100px;
        display: block;
    }

    .card {
        border: 1px solid rgb(213, 33, 14);
        padding: 0 10px;
        background: #edeaff;
        margin-bottom: 15px;
    }

    .subtitle,
    .date,
    .room {
        color: gray;
        font-size: 14px;
        margin: 0;
    }

    .title {

        margin: 0;
    }

    .date-title,
    .room-title {

        margin-bottom: 0;
        margin-top: 5px;
    }
    .type{
        color: rgb(213, 33, 14);
        font-size: 17px;
        margin-top:0;
    }
    .inclusion-div{
        background: rgb(7, 103, 177);
        color: rgb(213, 33, 14);
    }
    .inclusion-div ul {
        background: rgb(7, 103, 177);
        margin: 0;
        padding: 10px 0;
        color:white !important;
        list-style: circle;
    }

    .inclusion-div  ul li {
        padding: 0px 0 5px 20px;
        color: rgb(213, 33, 14);
    }
.cancellation-div table
{
    width: 100% !important;
}
   
.cancellation-div 
{
    width: 100% !important;
}
.pdf-g-detail{
    color: white;
    background: rgb(213, 33, 14);
    padding:15px;
    display: inline-block;
    
    margin-left: 50px;
}
li::before {content: "•" !important; color: red important;}
table {
    width: 100% !important;
}

</style>

<body>
    <table id="customers" class="pdf-table" style="width: 100% !important;border-collapse: collapse;">
        <tr>
            <td colspan="3" style="padding: 0; border: none !important;" class="hello">
               <table style="margin-bottom:14px" class="hello">
                   <tr style="width:100%">
                       <td style=" padding: 0;
                       border: none !IMPORTANT;" colspan="1"><img class='logo' style="width:auto;height:60px" src="https://travoweb.com/assets/images/logo.png"></td>
                        <td style=" padding: 0;
                        border: none !IMPORTANT;" colspan="2"> {{-- <p class="pdf-heading"> </p> --}}</td>
                    </tr>
               </table>
               
            </td>
        </tr>
        <tr style="background: rgb(7, 103, 177);">
            <td colspan="2" style="padding: 0;border: none !important;width: 65% !important;">
                <div style="padding: 15px">
                    <p class="basic-info" style="color: white;margin: 0;"><b>Places covered: {{$tour_package->packagename}}</b></p>
                    <p class="basic-info" style="color: white;margin: 0;"><b>Package duration:</b> {{$tour_package->packagedays}} Days / {{$tour_package->packagenight}} Nights</p>
                   
            </td>
            <td style="text-align:center">
                <p style="color: white;">Cost for 2 Adults</p>
                <p class="pdf-g-detail" style="background: red !important;color: white !important;width: 110px !important;border-radius: 28px !important;padding: 10px !important;display: block !important;margin: 0 auto 10px !important;">
                  
                  {{$tour_package->currency}}{{$totaloffer}}
                   
                   </p>
                </div>
            </td>

        </tr>
        <tr>
            <?php $pckgimageval = unserialize($tour_package->packimage);
                ?>
            <td colspan="3" style=" padding: 0;
            border: none !IMPORTANT;">
                <img src="{{asset('assets/uploads/packageimage')}}/{{$pckgimageval[0]}}"
                    style="width:
          100%; height: 350px" class="pdf-h-img"></td>


        </tr>
          <?php
            $inttitle = unserialize($tour_package->inttitle);
            $intdescription = unserialize($tour_package->intdescription);
            $meallist = unserialize($tour_package->meallist);

            for($t = 0;$t < count($inttitle);$t++)
            {
            ?>
            <tr>
                <td colspan="3" style=" padding: 0;
                border: none !IMPORTANT;">
                    <p class="day-p" style="background: #0767b1;
    color: white;"><span style="background: red;color: white;padding: 10px;display: inline-block;margin-right: 10px;">Day {{$t+1}}</span>{{$inttitle[$t]}}
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="day-content">
                        <p>{{$intdescription[$t]}}</p>



                    </div>
                </td>
            </tr>
            <?php } ?>
       

 <tr>
            <td colspan="3">
              
                <p class="inclusion" style="background: #cccccc;color: black;padding: 10px;">Inclusion</p>
                 <ul class="i-ul">
                <?php echo $tour_package->Inclusionsdescription;?>
            </ul>
            </td>
        </tr>
        
        <tr>
            <td colspan="3">
                
                <p class="inclusion" style="background: #cccccc;color: black;padding: 10px;">Exclusion</p>
                  <ul class="i-ul">
                    <?php echo $tour_package->highlitedescription;?>
                </ul>
          
            </td>
        </tr>
       

        <tr>
            <td colspan="3">
                <p class="inclusion" style="background: #cccccc;color: black;padding: 10px;">Terms & Condition</p>
                <div class="inclusion-div">
                    <?php echo $tour_package->termcondition;?>
                
                </div>
              
            </td>
        </tr>
       

        <tr>
            <td colspan="3">
                <p class="inclusion" style="background: #cccccc;color: black;padding: 10px;">Cancellation Policy</p>
                <div class="cancellation-div">
                    <?php echo $tour_package->cancelpolicy;?>

                </div>
               
           

            </td>
        </tr>
       
    </table>


</body>

</html>