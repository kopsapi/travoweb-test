@include('pages.include.header')
<link rel="stylesheet" type="text/css" href="{{asset('assets/styles/blog_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/styles/blog_responsive.css')}}">
<body>
    <style>

        .sidebar_latest_posts {
    margin-top: 0;
}
        .intro {
            width: 100%;
            padding-top: 100px;
            padding-bottom: 0px;

        }

        .add_content {
            z-index: 9;
        }

        .about-header {
            background: url("{{asset('assets/images/about-h.jpg')}}");
            height: 450px;
            margin-top: 126px;
            position: relative;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center center;
        }

        h1.about-title {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
            background: rgba(49, 18, 75, 0.8);
            padding: 10px 35px;
        }

        .intro_title {
            text-align: center;
        }

        ul.n-ul {
            margin-top: 30px;
            text-align: left;
        }

        ul.n-ul li {
            padding: 10px 20px;
            color: #9555ef;
            font-size: 15px;
            font-weight: 500;
        }

        ul.n-ul li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
            font-size: 20px;
            font-weight: 900;
        }

        .intro {
            background: white;
        }

        h1.h1-title {
            color: white;
            font-size: 20px;
        }

        .milestones {
            background: white;
        }

        .content-overlay {
            background: rgba(49, 18, 75, 0.8);
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 1;
        }

        p.p-para {
            font-size: 21px;
            color: #ffffff;
            background: linear-gradient(to right, #fa9e1b, #8d4fff, #fa9e1b);
            padding: 10px;
            border-radius: 5px;
        }

        ul.points {

            text-align: initial;
        }

        .column {
            column-count: 2;
        }

        ul.points li {
            padding: 10px;
            color: #8f50fb;
            font-size: 16px;
            font-weight: 600;
        }

        ul.points li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
        }

        .milestone_icon img {
            width: auto;
            height: 50PX;
        }

        @media screen and (max-width:768px) {
            .column {
                column-count: 1;
            }

            ul.points {
                text-align: left !important;
            }
        }

        div.cstm-accordion .card-header {
            /* background: black; */
            font-size: 21px;
            color: #ffffff;
            background: white;
            padding: 10px 30px;
            border-radius: 5px;
            border-radius: 2px;
            border: none;
        }

        div.cstm-accordion .card-header a {
            color: #fa9e1b;
            font-size: 15px;
            display: block;
            text-align: center;
            font-weight: 500;
            text-transform: uppercase;
        }

        div.cstm-accordion .card {
            /* border: none !IMPORTANT; */
            margin-bottom: 10px;
            border: 1px solid #dadada;
        }

        .heading-p {
            font-size: 17px;
            color: #fa9e1b;
            font-weight: 700;
            text-transform: uppercase;
        }

        .contact_form_message {
            height: 126px;
            width: 100%;
            border: none;
            outline: none;
            margin-top: 0px;
            background: transparent;
            font-size: 12px;
            font-weight: 400;
            color: #FFFFFF;
            border-bottom: solid 2px #e1e1e1;
            padding-top: 11px;
        }

        .contact_form_container {
            padding-top: 74px;
            padding-left: 48px;
            padding-right: 48px;
            padding-bottom: 30px;
            margin-bottom: 80px;
            background: linear-gradient(to bottom, #002c, #8d4fff) !important;
            background: rgba(30, 10, 78, 0.84);
        }

        input.input_field {
            margin-bottom: 20px;
        }

        .f-box {
            display: flex;
            position: relative;
            padding: 23px 15px 17px;
            justify-content: space-between !important;
            align-items: flex-start;
            /* background: white; */
            background: url(https://travoweb.com/assets/images/about-h.jpg);
            /* border: 1px solid #fa9e1b; */
            margin-bottom: 30px;
            border-radius: 5px;
            color: white !important;
            background-size: cover;
            background-repeat: no-repeat;
            min-height: 150px;
        }

        p.ph-no {
            line-height: 1;
            margin-top: 10px;

        }

        .f-content p {
            color: #ffffff;
            margin-bottom: 0;
            text-align: left;
            font-size: 16px;
            /* line-height: 1.5; */
        }

        .f-icon {
            flex: 0 0 20%;
            background: #fa9e1b;
            color: #ffffff;
            text-align: center;
            padding: 20px;
            border-radius: 30px;
            width: 60px;
            height: 60px;
        }

        .f-icon i {
            font-size: 22px;
        }

        p.f-heading {
            color: #834ceb;
            background: #ffffff;
            padding: 5px 15px;
            border-radius: 5px;
            text-align: left;
            font-weight: 600;
            /* display: inline-block; */
        }

        .f-over {
            background: rgba(49, 18, 75, 0.8);
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            border-radius: 5px;
        }

        .f-content {
            flex: 0 0 65%;
        }
        .blog_post_image img {
    height: 100%;
    object-fit: cover;
    object-position: bottom;
}

.blog_post_image {
    height: 400px !important;
}
        
    </style>
    <div class="super_container">
        <!-- Header -->
        @include ('pages.include.topheader')
        <div class="about-header">
            <h1 class="about-title">Blogs</h1>
        </div>
        <!-- Intro -->
        <div class="intro">
            <div class="container">
                <div class="row">
                    <div class="blog">
                        <div class="container">
                            <div class="row">
                    
                                <!-- Blog Content -->
                    
                                <div class="col-lg-8">
                                    
                                    <div class="blog_post_container">
                    
                                        <!-- Blog Post -->
                                        
                                        <div class="blog_post">
                                            <div class="blog_post_image">
                                                <img src="assets/images/blog3.jpg" alt="https://unsplash.com/@anniespratt">
                                                <div class="blog_post_date d-flex flex-column align-items-center justify-content-center">
                                                    <div class="blog_post_day">26</div>
                                                    <div class="blog_post_month">Aug, 2019</div>
                                                </div>
                                            </div>
                                            <div class="blog_post_meta">
                                                <ul>
                                                    <li class="blog_post_meta_item"><a href="#">by Admin</a></li>
                                                 {{--    <li class="blog_post_meta_item"><a href="#">Uncategorized</a></li>
                                                    <li class="blog_post_meta_item"><a href="#">3 Comments</a></li> --}}
                                                </ul>
                                            </div>
                                            <div class="blog_post_title"><a href="#">Tips to travel light</a></div>
                                            <div class="blog_post_text">
                                                <p>
                                                    A bright early start saw us at the airport well in time for kick off and leaving the bright sunny shores of New Zealand for the eastern city of Dubai. As is usual with us, leaving home is never without its dramas be they small or large. This time we scored some very cheap duty free booze at DFS, one bottle of scotch I took with us for those little night caps so important on any trip, leaving the other three bottles (not all scotch by the way) to collect on our way home. Arriving for our short stopover in Melbourne the anal retentive Aussie customs would not let me take through my bottle as they said it was not in a sealed bag, duh, how daft are they, we were not even going in to Aussie but just passing through, no point in arguing with them though as that just gets one in trouble, so scotch less we continued on our way. After a dreadful flight (I don’t care how flash the new A380 dreamliner is 12 hours on a plane just plain sucks. 
Arriving in Dubai we find that due to construction on the runway the plane had to 
The Souk Dubai MallThe Souk Dubai MallThe Souk Dubai Mall
<div id="demo" class="collapse">
Jumping on the metro we set off to our hotel at the Marina and 30 minutes later we arrived at the closest stop to the hotel and a short cab ride lat filled er brought us to the front door. Cabs are really cheap in Dubai around 3.00 NZD for a 10 minuet ride. It was now 7.30 am and we are both feeling shattered with no sleep on the plane. We could not check in, no surprise there, and decided to start our day with a trip out to the Palms, the man-made island in Dubai in the shape, you guessed it of a palm tree. Unbeknownst to us the monorail out to the palms didn’t start until 10.00am so disappointed, but not in any way disillusioned we put plan “B” into action and headed off to see the Burj Kalif and Dubai mall. We had a tour to the top of the Burj Khalifa arranged for 12.00 so cruised the mall and surrounds until our tour left. The 
Dubai Mall AquariumDubai Mall Aquarium
Dubai Mall Aquarium

Arriving for our tour of the Burj Khalifa on time we set of up the highest building in the world, commenced in 2006 and finished in 2009 is is truly a marvel, both visually and from an engineering standpoint, we went to the viewing platform 124 stories high is less than 60 seconds. The view from the top is specular and I swear you can almost see the earth’s curvature. 
Being knackered we decided to head back to the hotel for a few hours’ sleep before heading out that night back to the mall to see the fountain. The fountain is outside the mall and a show starts every night from 6.30 and every half hour till 11.00. Set to music the show is stunning and hundreds turn out every half hour to see it. We had a prime spot in a little Italian restaurant some 20 meters from the edge of the fountain. See the photos Pam has put up of this. Well 
Dubai MallDubai Mall
Dubai Mall
park miles away from the terminal and we had a 20 minuet bus trip from the plane to the terminal. Still after 17 hours all up we were there. 
Dubai mall is something to see and is quite literally HUGE, the three storey indoor aquarium filled with shark’s and so many other types of fish is a marvel to see. 
tired after the first day Pam took some great night shots of the Khalifa and we headed to our hotel for the evening.
</div>
</p>
                                            </div>
                                            <div class="blog_post_link"><a href="#demo" data-toggle="collapse">read more</a></div>
                                        </div>
                    
                                        <!-- Blog Post -->
                                        
                                        
                                    </div>
                                        
                                    <div class="blog_navigation">
                                        <ul>
                                            <li class="blog_dot active"><div></div>01.</li>
                                           {{--  <li class="blog_dot"><div></div>02.</li>
                                            <li class="blog_dot"><div></div>03.</li> --}}
                                        </ul>
                                    </div>
                                </div>
                    
                                <!-- Blog Sidebar -->
                    
                                <div class="col-lg-4 sidebar_col">
                    
                                  
                                    <div class="sidebar_latest_posts">
                                        <div class="sidebar_title">Latest Posts</div>
                                        <div class="latest_posts_container">
                                           <ul>
                    
                                                <!-- Latest Post -->
                                                <li class="latest_post clearfix">
                                                    <div class="latest_post_image">
                                                        <a href="#"><img src="assets/images/latest_1.jpg" alt=""></a>
                                                    </div>
                                                    <div class="latest_post_content">
                                                        <div class="latest_post_title trans_200"><a href="{{url('/blog1')}}">A simple blog post</a></div>
                                                        <div class="latest_post_meta">
                                                            <div class="latest_post_author trans_200"><a href="#">by Jane Smith</a></div>
                                                            <div class="latest_post_date trans_200"><a href="#">Aug 25, 2016</a></div>
                                                        </div>
                                                    </div>
                                                </li>
                    
                                                <!-- Latest Post -->
                                                <li class="latest_post clearfix">
                                                    <div class="latest_post_image">
                                                        <a href="#"><img src="assets/images/latest_2.jpg" alt=""></a>
                                                    </div>
                                                    <div class="latest_post_content">
                                                        <div class="latest_post_title trans_200"><a href="{{url('/blog2')}}">Dream destination for you</a></div>
                                                        <div class="latest_post_meta">
                                                            <div class="latest_post_author trans_200"><a href="#">by Jane Smith</a></div>
                                                            <div class="latest_post_date trans_200"><a href="#">Aug 25, 2016</a></div>
                                                        </div>
                                                    </div>
                                                </li>
                    
                                                <!-- Latest Post -->
                                                <li class="latest_post clearfix">
                                                    <div class="latest_post_image">
                                                        <a href="#"><img src="assets/images/latest_3.jpg" alt=""></a>
                                                    </div>
                                                    <div class="latest_post_content">
                                                        <div class="latest_post_title trans_200"><a href="{{url('/blog3')}}">Tips to travel light</a></div>
                                                        <div class="latest_post_meta">
                                                            <div class="latest_post_author trans_200"><a href="#">by Jane Smith</a></div>
                                                            <div class="latest_post_date trans_200"><a href="#">Aug 25, 2016</a></div>
                                                        </div>
                                                    </div>
                                                </li>
                    
                                                <!-- Latest Post -->
                                                <li class="latest_post clearfix">
                                                    <div class="latest_post_image">
                                                        <a href="#"><img src="assets/images/latest_4.jpg" alt=""></a>
                                                    </div>
                                                    <div class="latest_post_content">
                                                        <div class="latest_post_title trans_200"><a href="{{url('/blog4')}}">How to pick your vacation</a></div>
                                                        <div class="latest_post_meta">
                                                            <div class="latest_post_author trans_200"><a href="#">by Jane Smith</a></div>
                                                            <div class="latest_post_date trans_200"><a href="#">Aug 25, 2016</a></div>
                                                        </div>
                                                    </div>
                                                </li>
                    
                                            </ul>
                                        </div>
                                    </div>
                    
                                    <!-- Sidebar Gallery -->
                                    <div class="sidebar_gallery">
                                        <div class="sidebar_title">Blog Gallery</div>
                                        <div class="gallery_container">
                                            <ul class="gallery_items d-flex flex-row align-items-start justify-content-between flex-wrap">
                                                <li class="gallery_item">
                                                    <a class="colorbox" href="#">
                                                        <img src="assets/images/gallery_1.jpg" alt="https://unsplash.com/@mantashesthaven">
                                                    </a>
                                                </li>
                                                <li class="gallery_item">
                                                    <a class="colorbox" href="#">
                                                        <img src="assets/images/gallery_2.jpg" alt="https://unsplash.com/@kensuarez">
                                                    </a>
                                                </li>
                                                <li class="gallery_item">
                                                    <a class="colorbox" href="#">
                                                        <img src="assets/images/gallery_3.jpg" alt="https://unsplash.com/@jakobowens1">
                                                    </a>
                                                </li>
                                                <li class="gallery_item">
                                                    <a class="colorbox" href="#">
                                                        <img src="assets/images/gallery_4.jpg" alt="https://unsplash.com/@seefromthesky">
                                                    </a>
                                                </li>
                                                <li class="gallery_item">
                                                    <a class="colorbox" href="#">
                                                        <img src="assets/images/gallery_5.jpg" alt="https://unsplash.com/@deannaritchie">
                                                    </a>
                                                </li>
                                                <li class="gallery_item">
                                                    <a class="colorbox" href="#">
                                                        <img src="assets/images/gallery_6.jpg" alt="https://unsplash.com/@benobro">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->

            @include ('pages.include.footer')
            @include ('pages.include.copyright')
        </div>
    </div>
</body>

</html>