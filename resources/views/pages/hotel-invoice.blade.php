@include('pages.include.header')<!-- Loader -->
<style>
    .loader {
        display: block;
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: #CCEAF7 url('{{ asset('assets/images/flight-loader.gif')}}') no-repeat center center;
        text-align: center;
        color: #999;
        opacity: 0.9;    }
        .detail b{
            font-size: 20px;
            color: blue;
            padding-left: 5px;
            font-family: monospace;
        }
        .col-sm-6.detail {
          text-align: right;
      }
      .hotel-time-img {
        width: 30px;

    }
    img.hotel-icon {
        width: 60px;
    }
    img.room-detail {
        width: 100px;
    }
    .col-md-6.hotel-info {
        padding: 25px;
    }

    a.hotel-email{
        text-decoration: none;
        color: #6cc4ee;
        border: none;
        background-color: transparent;
    }
    a.btn.btn-success.pull-right {
        margin-right: 10px;
    }
    a.hotel-email:hover{
        text-decoration: none;
        color: #4ca3ee;
        background-color: transparent;
    }
    .or {
        position: absolute;
        left: 50%;
        transform: translate(-50%, -116%);
        background-color: #f1f1f1;
        color: black;
        border: 1px solid #868e96;
        border-radius: 50%;
        padding: 8px 10px;
    }
    .col-12.need-help h3, .col-12.need-help p {
        color: black;
        text-align: center;
    }
    th.room-detail {
        color: blue;
    }
    th.table-head {
        color: #000;
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
        vertical-align: middle;

    }
    .table td{
        padding: 0.75rem;
        color: #000;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
    }

    .room-info h1 {
        color: #ea8218;
        text-align: center;
        font-size: 25px;
    }
    .room-info h1 img{
        color: #2b6eab;
    }
    iframe.loc-map {
        width: 100%;
        height: auto;
    }
    img.loc-img {
        width: 50px;
        /* margin-left: 10px; */
    }
    h2.loc{

        text-align: center;
        font-size: 27px;
        color: orange;
        font-weight: 500;
        margin-bottom: 20px;
    }

    img.offer {
        width: 59px;
        margin-top: 13px;
    }
    img.save-money {
        width: 57px;
    }
    img.promo {
        width: 50px;
        margin-right: 10px;
    }
    .hotel-limit{
        margin-top: 30px;
    }
    img.room-type {
        width: 325px;
    }
    .limit p,.limit1 p {
        color: black;
        text-align: center;
        /* top: 3px; */
        margin-top: -44px;
    }
    .limit {
        width: 50%;
        height: 19px;
        float: left;
        border-top: 14px solid #28a772;
        border-left: 2px solid  #28a772;
        margin-bottom: 48px;
    }
    .limit1 {
        width: 50%;
        height: 19px;
        float: left;
        border-top: 14px solid #ce5050;
        border-left: 2px solid #ce5050;
        margin-bottom: 48px;
    }
    .confirm h2,.confirm h4{
        color: black;
    }
    .confirm h4 {
        color: black;
        font-family: inherit;
    }
    .confirm h2{
        padding-bottom: 25px;
    }
    .hotel-id span {
        color: #017c60;
        font-size: 15px;
        font-weight: 700;
    }
    .confirm h4 i{
        color: #f6815a;
    }
    .confirm h4{
        color: #563eb3;
    }
    .total-amount span {
        color: #ff6900;
        font-weight: 700;
        font-size: 15px;
        text-transform: uppercase;
    }
    .hotel-img img{
        width: 100%;
        padding: 10px;
        height: 300px;
        border-radius: 21px;
    }
    .col-6.total-amount {
        text-align: right;
    }
    .detail p i {
        color: black;
        padding-left: 5px;

    }
    .confirm h2>img {
        width: 80px;
        margin-left: -12px;
    }
    a.hotel-name {
        text-decoration: none;
        font-size: 27px;
        color: darkblue;
        font-weight: 400;
        position: relative;
    }
    a.hotel-name:after {
        content: "";
        position: absolute;
        width: 100%;
        height: 4px;
        border-top: 3px solid #f6815a;

        bottom: -5px;
        left: 0;
    }
    .col-md-6.hotel-info p {
        color: black;
        margin-top: -10px;

    }
    .col-md-6.hotel-info p.addr{
        margin-top: 30px;
    }
    img.room-type-img {
        width: 22px;
        transform: rotate(90deg);
        margin-left: 0px;
        padding-top: 5px;
    }
    .col-md-6.hotel-info p a.hotel-email {
        text-decoration: none;
        border: 0;
        outline: 0;
        color: cornflowerblue;

    }
    .col-md-6.hotel-info p a.hotel-email:hover {
        background-color: #fffffb;
        color: #4c74ff;
    }
    a.manage-book {
        text-decoration: none;
        padding: 15px 20px;
        color: white;
        font-size: 25px;
        margin: 26px;
        background: linear-gradient(to right, rgba(255, 153, 83, 0.8), #ff5f5f);
        border:1px solid #ff5f5f;
        border-radius: 10px;
    }
    a.manage-book:hover {
        text-decoration: none;
        padding: 15px 20px;
        color: #ff5f5f;
        background: transparent;
        border:1px solid #ff5f5f;

    }
    .col-12.guest-info p {
        font-size: 19px;
        color: #ff6565;
        text-transform: capitalize;
    }

    .col-12.guest-info h5 {
        color: black;
        margin-left: 15px;
        font-size: 16px;
        position: relative;
    }
    .col-12.guest-info h5 i {

        padding-right: 8px;

    }
    .col-12.guest-info h5:before {
        position: absolute;
        content: "\f0da";
        font-family: "FontAwesome";
        top: -23%;
        left: -15px;
        color: #ff7819;
        font-size: 25px;
    }
    hr {
        background: linear-gradient(to right, rgba(39, 179, 243, 0.8), #f54e4e,#72469c,#96caf8);
        width: 100%;
        height: 2px;
    }
    .hotel-time span {
        color: blue;
        font-weight: 600;
        text-align: center;

        display: block;
    }
    .hotel-time-img {
        width: 25px;

    }
    .hotel-time p {
        margin-top: 0px;
        text-align: center;
    }
    .room-type h5 {
        color: #ff3a00;
        font-size: 32px;
        text-transform: uppercase;

    }
    .room-type p {
        margin-left: 0px;
        color: black;
        margin-bottom: 10px;
    }
    .room-type p span {
        margin-left: 7px;
    }
    .hotel-info p i{
        color: red;
    }
    :target:before {
        content: "";
        display: block;
        height: 180px;
        margin: -180px 0 0;
    }
</style>
<style>

    .over-img {
        position: relative;
        /* bottom: -8px; */
        top: 15%;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
        transition: all .5s ease;
        left: 50%;
        transform: translate(-50%,-50%);
        text-align: center;
        transition-delay: .5s;
    }
    .overlay {
        position: fixed;
        z-index: 999;
        top: 100%;
        left: 0;
        /* bottom: 0; */
        /* right: 0; */
        width: 100%;
        height: 0%;
        background-color: rgb(0,0,0);
        background-color: rgba(0,0,0, 0.8);
        overflow-x: hidden;
        transition: 1s;
        display: block;
    }
    .overlay-content {
        position: relative;
        top: 27%;
        left: 50%;
        transform: translate(-50%,-50%);
        text-align: center;
    }
    .over-img {
        position: absolute;
      
        top: 450px;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
        transition: all .5s ease;
        left: 50%;
        transform: translate(-50%,-50%);
        text-align: center;
        transition-delay: .5s;
    }
    @media screen and (max-width:500px) {
        .over-img img {
            height: 210px !important;
        }

    }
    .overlay-content p {
        color: white;
        font-size: 17px;
    }
    .overlay-content h3 {
        color: white;
        font-family: inherit;
        font-size: 25px;
    }
    a.closebtn {
        color: white;
        font-size: 40px;
        font-weight: bolder;
        text-decoration: none;
        right: 30px;
        top: 30px;
        cursor: pointer;
        position: absolute;
    }
    @keyframes fadein {
        from {bottom: 0; opacity: 0;}
        to {  bottom: -492px; opacity: 1;}
    }
    @media screen and (max-width: 650px) {
        .over-img {
            top: 450px;
        }
    }

</style>
<!-- Loader -->
<body>
    <div class="loader"></div>
    <div class="super_container">
        <!-- Header -->
        @include('pages.include.topheader')

        <div class="home" style="height:20vh;">
            <!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->

            <div class="home_content">
                <div class="home_title">Results</div>
            </div>
        </div>

        <?php
//         echo "<pre>";
// print_r($hotelbookdetailsarray);
// echo "</pre>";
        ?>


        <div id="myNav" class="overlay">

            <!-- Button to close the overlay navigation -->
            <a href="javascript:void(0)" class="closebtn closediv" onclick="closediv()">&times;</a>

            <!-- Overlay content -->
            <div class="overlay-content">
                <img src="{{ asset('assets/images/congratulations.png')}}" style="height: 95px; margin-bottom: 23px;">
                <p>Your Hotel {{$hotelbookdetailsarray['HotelName']}} Booking Has Been Confirmed</p>
            </div>
            <div class="over-img">
                <img src="{{ asset('assets/images/hand2.png')}}" style="height: 335px">
            </div>

        </div>
        <!-- Intro -->
        <div class="container">
            <div class="row">
                <div class="col-sm-6"></div>
                <div class="col-sm-6 detail">
                    <span>Booking Id:<b>{{$hotelbookdetailsarray['BookingId']}}</b></span>
                    <?php
                    $bookingdate=explode(' ',$hotelbookdetailsarray['created_on']);
                    $bookdate=date('F d,Y',strtotime($bookingdate[0]));
                    $booktime=date('h:i a',strtotime($bookingdate[1]));
                    $bookid=$hotelbookdetailsarray['BookingId'];
                    ?>
                    <p>Booked on:<i>{{$bookdate}}, {{$booktime}}</i></p>

                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                   <button id="print_btn" class="btn btn-success pull-right" >Print</button>
                   <a  href="{{action('HotelController@hotel_pdf', $bookid )}}" class="btn btn-success pull-right" target="_blank">PDF</a>
               </div>
               <div class="col-12 confirm">
                <h2><img src="{{asset('assets/images/icon-tick-28.jpg')}}">Booking Confirmed</h2>

                <h4><i class="fa fa-user-o"></i> Dear {{$hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['FirstName']}} {{$hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['LastName']}}</h4>
                <h4>Thank you for choosing Our Hotel</h4>

            </div>

            <div class="row">
                <div class="col-md-6 hotel-img">
                    <img src="{{$hotelbookdetailsarray['hotelpicture']}}" width=325 height=183>
                </div>
                <div class="col-md-6 hotel-info">
                    <a href="#" class="hotel-name">{{$hotelbookdetailsarray['HotelName']}}</a>
                    <p class="addr">{{$hotelbookdetailsarray['AddressLine1']}}</p>
                    <p>{{$hotelbookdetailsarray['AddressLine2']}}</p>
                    <!--      <p><i>Email:</i> <a href="#" class="hotel-email"> hotelshivtara@gmail.com </a></p> -->
                    <p><i>Getting there:</i><a href="#location" class="hotel-dir"> Show directions</a></p>
                </div>

            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12 guest-info">
                <p>Guest Name</p>
                @for($rooms=0;$rooms< count($hotelbookdetailsarray['HotelRoomsDetails']);$rooms++)
                <div class="row">
                    <div class="col-md-5">
                        <h5> Room {{$rooms+1}}</h5>
                    </div>
                    <div class="col-md-5">
                     @for($passenger=0;$passenger< count($hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger']);$passenger++)
                     <h5> <?php 
                     $title=$hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['Title'];
                     $fullname=strtolower($hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['FirstName']." ".$hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['LastName']);
                     $childage=$hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['Age'];
                     if($hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['PaxType']==1)
                     {
                        $pax="Adult";
                        $childa ="(Age " .$childage." year)";
                    }
                    else
                    {
                       $pax="Child";
                       $childa ="(Age " .$childage." year)";
                   }

                   echo $title." ".ucwords($fullname)." ( ".$pax." )  ". $childa. " "; 
                   ?></h5>
                   @endfor
               </div>
           </div>
           <br>
           @endfor
       </div>
   </div>
   <hr>
   <div class="row">
    <div class="col-6 hotel-time">
        <span>Check In</span><br>

        <p><img src="https://images.vexels.com/media/users/3/135660/isolated/preview/b325f84730498e3e5e9e8cca79ef3d0a-clock-round-icon-by-vexels.png" class="hotel-time-img"> <?php 
        $getcheckindatetime=explode('T',$hotelbookdetailsarray['CheckInDate']);

        $getcheckindate=date('F d,Y',strtotime($getcheckindatetime[0]));
        echo $getcheckindate;
        ?></p>
    </div>
    <div class="col-6 hotel-time">
        <span>Check Out</span><br>
        <p><img src="https://images.vexels.com/media/users/3/135660/isolated/preview/b325f84730498e3e5e9e8cca79ef3d0a-clock-round-icon-by-vexels.png" class="hotel-time-img"><?php 
        $getcheckindatetime=explode('T',$hotelbookdetailsarray['CheckOutDate']);

        $getcheckindate=date('F d,Y',strtotime($getcheckindatetime[0]));
        echo $getcheckindate;
        ?></p>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-6 hotel-id">
        <span><img src="https://cdn0.iconfinder.com/data/icons/building-vol-9/512/3-512.png" class="hotel-icon"> Hotel Booking Id:</span>
        <span>{{$hotelbookdetailsarray['BookingId']}}</span>
    </div>
    <div class="col-sm-6 total-amount">
        <?php
        $RoomPrice=0;
        $Tax=0;
        $ExtraGuestCharge=0;
        $ChildCharge=0;
        $OtherCharges=0;
        $AgentCommission=0;
        $AgentMarkUp=0;
        $PublishedPrice=0;
        $ServiceTax=0;
        $TDS=0;
        $TotalGSTAmount=0;
        $total_fare=0;
        $gst=0;
        $total_gst=0;
        $grand_total=0;
        for($i=0;$i<count($hotelbookdetailsarray['HotelRoomsDetails']);$i++)
        {
            $RoomPrice+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['RoomPrice'];
            $Tax+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['Tax'];
            $ExtraGuestCharge+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['ExtraGuestCharge'];
            $ChildCharge+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['ChildCharge'];

            $OtherCharges+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['OtherCharges'];
            $AgentCommission+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['AgentCommission'];
            $AgentMarkUp+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['AgentMarkUp'];
            $PublishedPrice+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['PublishedPrice'];
            $ServiceTax+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['ServiceTax'];
            $TDS+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['TDS'];
            $TotalGSTAmount+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['TotalGSTAmount'];

            $total_fare=$RoomPrice+$Tax+$ExtraGuestCharge+$ChildCharge+$OtherCharges+$AgentCommission
            + $AgentMarkUp+$ServiceTax+$TDS+$TotalGSTAmount;
            $gst=($total_fare*5)/100;
            $total_gst=round($gst,2);

            $grand_total=$total_fare+$total_gst;

        }

        ?>

        <span><img src="https://cdn.xl.thumbs.canstockphoto.com/invoice-bill-icon-eps-vector_csp47302143.jpg" class="hotel-icon">Total Amount:</span>
        <span> <i class="fa {{$hotelbookdetailsarray['hotelfaicon']}}"></i> {{$hotelbookdetailsarray['hotelamount']}}</span>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-6" id="room-img">
        <img src="https://amp.businessinsider.com/images/5527f47fdd0895c44f8b459e-750-422.jpg" class="room-type">
    </div>
    <div class="col-md-6 room-type">
        <h5>Room Type</h5>
        <p><img src="https://www.pngrepo.com/download/274116/triangle-caret.png" class="room-type-img"><span>
        {{$hotelbookdetailsarray['HotelRoomsDetails'][0]['RoomTypeName']}}</span></p>


        <?php
        $amenties="";
        for($inclusion=0;$inclusion< count($hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion']);$inclusion++)
        {

            $amenties.=$hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion'][$inclusion];
            if($inclusion<count($hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion'])-1)
            {
                $amenties.=" , ";
            }
        } 
        ?>
        @if($amenties!='')
        <p><img src="https://www.pngrepo.com/download/274116/triangle-caret.png" class="room-type-img"><span> With {{$amenties}}</span> </p>
        @endif

    </div>
</div>
<hr id="location">
       <!--  <div class="row">
            <div class="col-12 hotel-limit">

                <div class="limit"><p>Free Cancellation</p></div>

                <div class="limit1"><p>Non-Refundable</p></div>
            </div>
            <div class="col-12">
                <p><img src="https://cdn3.iconfinder.com/data/icons/medical-2-1/512/free-512.png" class="promo">Promo Code Applied:<span>GETSETGO</span></p>
                <p><img src="https://www.clipartmax.com/png/middle/157-1572223_save-money-icon-free-delivery.png" class="save-money"> You saved Rs. 418 on this booking</p>
                <p><img src="https://banner2.kisspng.com/20180529/fcq/kisspng-computer-icons-icon-design-special-offer-5b0da6a3ef8581.6087426315276212839811.jpg " class="offer"> Special Offer: 75 Credits will be credited to your Wallet once you check-out from the hotel.</p>
            </div>
        </div> -->
        <!--  <hr> -->
        <div class="row" id="location-invoice">
            <div class="col-12">
                <h2 class="loc"><img src="https://cdn0.iconfinder.com/data/icons/activity-2/32/116-512.png" class="loc-img">Hotel Location </h2>
                <iframe class="loc-map" src="https://maps.google.com/maps?q={{$hotelbookdetailsarray['Latitude']}},{{$hotelbookdetailsarray['Longitude']}}&hl=es;z=14&amp;output=embed"  frameborder="0" style="border:0" height="170"  allowfullscreen></iframe>
                <br />
                <small>
                 <a 
                 href="https://maps.google.com/maps?q={{$hotelbookdetailsarray['Latitude']}},{{$hotelbookdetailsarray['Longitude']}}&hl=es;z=14&amp;output=embed" 
                 style="color:#0000FF;text-align:left" 
                 target="_blank"
                 >
                 See map bigger
             </a>
         </small>
         <!--   <iframe class="loc-map" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d872801.4155179203!2d74.73973083359868!3d31.293226109097468!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1shotel+redison+blue!5e0!3m2!1sen!2sin!4v1560852584413!5m2!1sen!2sin"  frameborder="0" style="border:0" allowfullscreen></iframe> --> </div>
     </div>
     <hr>
     <div class="row">
      <div class="col-12 room-info">
          <h1>Room Detail</h1>
          <table class="table">

              <tbody>
                  <tr class="t-row">
                      <th class="table-head">Number of Guests</th>
                      <td>
                          <table class="table table-borderless">
                           @for($rooms=0;$rooms< count($hotelbookdetailsarray['HotelRoomsDetails']);$rooms++)
                           <?php
                           $adultcount=0;
                           $childcount=0;
                           ?>
                           <tr>
                             <th class="room-detail"> Room {{$rooms+1}}:</th>
                             @for($passenger=0;$passenger< count($hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger']);$passenger++)
                             <?php 

                             if($hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['PaxType']==1)
                               $adultcount++;
                           else
                             $childcount++;
                         ?>
                         @endfor
                         <td> Adults <span class="badge badge-pill badge-success mr-2">{{$adultcount}}</span>   , Children <span class="badge badge-pill badge-info mr-2">{{$childcount}}</span></td>
                     </tr>
                     @endfor
                 </table>

             </td>

         </tr>
         <tr class="t-row">
          <th class="table-head">Inclusions</th>
          <td>
            @for($inclusion=0;$inclusion< count($hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion']);$inclusion++)
            <i class="fa fa-check-square-o mr-2 "></i>Free {{$hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion'][$inclusion]}}
            <br>
            @endfor
            <i class="fa fa-check-square-o mr-2"></i>Accommodation</td>

        </tr>
        <tr class="t-row">
          <th class="table-head">Cancellation Policy</th>

          <td>
              <?php
              $cancellationpolicy=array();
              for($cancel=0;$cancel<count($hotelbookdetailsarray['HotelRoomsDetails'][0]['CancellationPolicies']);$cancel++)
              {
               $cancellationpolicy=$hotelbookdetailsarray['HotelRoomsDetails'][0]['CancellationPolicies'];
               $fromdatetime=explode('T',$cancellationpolicy[$cancel]['FromDate']);
               $fromdate=date('d-F-Y',strtotime($fromdatetime[0]));
               $fromtime=date('h:i a',strtotime($fromdatetime[1]));

               $todatetime=explode('T',$cancellationpolicy[$cancel]['ToDate']);
               $todate=date('d-F-Y',strtotime($todatetime[0]));
               $totime=date('h:i a',strtotime($todatetime[1]));

               echo '<i class="fa fa-dot-circle-o"></i> '.$cancellationpolicy[$cancel]['Charge'].' '.' of total amount will be charged , If cancelled between '.$fromdate.' '.$fromtime.' and '.$todate.' '.$totime;

           }
           ?>
            <!-- <i class="fa fa-caret-right"></i> FREE Cancellation until Dec 24, 2018 12:00 hours.<br><br>


              <i class="fa fa-caret-right"></i> Non-Refundable if cancelled after Dec 24, 2018 12:00 hours.<br><br> -->


                                     <!--  <i class="fa fa-caret-right"></i>  goCash used in the booking will be non refundable.<br><br>


                                      <i class="fa fa-caret-right"></i> Any Add On charges are non-refundable.<br><br>
                                  -->

                                  <!-- <i class="fa fa-caret-right"></i> You can not change the check-in or check-out date after Dec 24, 2018 12:00 hours.<br><br> -->
                              </td>
                          </tr>
                          <tr class="t-row">
                              <th class="table-head">Hotel Policy</th>
                              <!-- <td>The standard check-in time is 12:00 PM and the standard check-out time is 12:00 PM. </td> -->
                              <td>  <?php
                              $cancellationpolicy=explode('|',$hotelbookdetailsarray['HotelPolicyDetail']);
                              for($policy=0;$policy<count($cancellationpolicy);$policy++)
                              {
                                if($policy<=1)
                                {
                                    continue;
                                }
                                if($cancellationpolicy[$policy]!='')
                                    echo " <i class='fa fa-caret-right'></i> ".$cancellationpolicy[$policy]."<br><br>";

                            }
                            ?> </td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="alert alert-danger">

            ***NOTE***: Any increase in the price due to taxes will be borne by you and payable at the hotel.
        </div>
        <hr>
        <div class="row">
            <div class="col-12 need-help">
                <h3>NEED HELP??</h3>
                <p>For any questions related to the property, you can contact {{$hotelbookdetailsarray['HotelName']}} directly at: <a href="#">{{$hotelbookdetailsarray['AddressLine2']}}</a></p>
                <br>
                <br>
                <div class="or">OR</div>
                <p>If you would like to change or upgrade your reservation, visit <a href="#" class="hotel-email"> travoweb.com</a></p>
            </div>
        </div>
            <div class="pdf-footer">
                <hr style="width: 80%;height:0;border: none;border-bottom: 1px solid #5e5e5e; margin: auto; background: black !important;">
                <div class="row">

                    <div class="col-md-12" style="width:100%; margin:0 auto;font-size: 13px;text-align: center;color: black !important;">

                        <img src="{{asset('assets/images/logo.png')}}" style="width: 100px;margin-top: 10px" class="img-fluid d-block mx-auto">
                        
                        <p style="margin: 5px!important; color: black;"><b><i class="fa fa-phone"></i> Ph</b> +61 490149833</p>
                        @if($hotelbookdetailsarray['hotelfaicon']=='fa-inr')
                        <p style="margin: 5px!important; color: black;"><b><i class="fa fa-angle-double-right"></i> GST No.</b> 03BDHPK8214M1ZG</p>
                        @endif
                        <p style="margin: 5px!important; color: black;"><b><i class="fa fa-envelope"></i> Email</b>  <a href="#">info@travoweb.com</a>,
                            <span style="color: black"><b><i class="fa fa-globe"></i> Website:</b>Travoweb.com</span>
                         </p>


                    </div>
                </div>
            </div>
            <hr style="width: 100%;height:0;border: none;border-bottom: 1px solid #5e5e5e; margin:10px auto; background: #494949 !important;">
    </div>
    <!-- Intro -->

</div>
<!-- Footer -->
@include('pages.include.footer')
<!-- Copyright -->@include('pages.include.copyright')
<script>
    $(document).ready(function() {
        $(".loader").fadeOut("slow");
    });
</script>
<script>
    $(document).on("click","#print_btn",function()
    {
        //Hiding extra content for printing
        $(".header").hide();
        $(".home").hide();
        $(".trans_500").hide();
        $(".manage-book").hide();
        $(".hotel-img").hide();
        $("#location-invoice").hide();
        $(this).hide();
        $(".footer").hide();
        $(".copyright").hide();
        $("#room-img").hide();



        window.print();


        //displating hidden extra content after printing
        $(".header").show();
        $(".home").show();
        $(".trans_500").show();
        $(".manage-book").show();
        $(".hotel-img").show();
        $("#location-invoice").show();
        $(this).show();
        $(".footer").show();
        $(".copyright").show();
        $("#room-img").show();
});
</script>
    <script>
        $(document).ready(function(){

            $("#myNav").css({"height": "100%","top" : "0%"});

        })
        $(document).on('click','.closediv',function(){
            $('#myNav').css({"height": "0%","top" : "100%"});
        })
    </script>
</body>
</html>