@include('pages.include.header')
<style>
	.home
	{
		height: 465px;
		margin-bottom:30px;
	}
	.home .home_slider_container
	{
		background:#EEE;
	}
	.home .home_slider_background
	{
		opacity:1;
	}
	.home_slider_container {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		z-index: 1 !important;
		background: #31124b;
	}
	.owl-carousel .owl-item img {
		display: block;
		width: 100%;
		height: 100% !important;
	}

	@media screen and (max-width: 420px){

		.room-details.text-right {
			text-align: left !important;
			margin-top: 20px;
		}
		p.per {
			text-align: left !important;
		}

	}
	@media screen and (max-width: 350px){

		.room-details.text-right {
			margin-top: 43px;
			text-align: left !important;
		}

		.i-res {
			width: 100%;
			display: block;
			text-align: center;
		}
		.room-table td {
			color: black;
			text-align: center !important;
		}
		button.hotel-book-btn {
			margin-bottom: 10px;
		}
	}


.about-hotel p {
    font-family: calibri;
}
.row.d-card h3 {
    font-family: calibri;
}
.d-card {
    background: #f5f5f5;
    padding: 20px;
    border-radius: 10px;
    margin-bottom: 30px;
}
.room-meal-price.left-price {
    font-family: calibri;
    font-size: 15px !important;
    font-weight: 100 !important;
}
button.read-more {
    border: none;
    background: none;
    color: #fa9e1b;
    font-size: 16px;
    font-weight: 600;
}
.home{
	 height: 102px;
}
@media screen and (max-width:600px){
	.home{
	 height: 40px;
}
}
</style>
<body>
<div class="super_container">
	<!-- Header -->@include('pages.include.topheader')
	<div class="home">
	<!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->
		<div class="home_content">
			<div class="home_title">Hotel Information</div>
		</div>
	</div>
	<!-- Intro -->
	<div class="hotel-book">
		<div class="intro">
			<!-- <section class="hotel-book-breadcrumb">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="intro_content">
                                <div class="row">
                                    <div class="col-lg-3 res-mb-20">
                                        <h4><i class="fa fa-map-marker"></i> Amritsar-Punjab, India</h4> </div>
                                    <div class="col-lg-2 res-mb-20">
                                        <h4><i class="fa fa-calendar"></i> 30 Feb 2019 </h4> </div>
                                    <div class="col-lg-1 res-mb-20">
                                        <h4><i class="fa fa-bed bed"></i></h4> </div>
                                    <div class="col-lg-2 res-mb-20">
                                        <h4><i class="fa fa-calendar"></i> 31 Feb 2019</h4> </div>
                                    <div class="col-lg-2 res-mb-20">
                                        <h4 class="flight-class"><small>ADULTS</p> 2 <small>CHILD</p> 0 </h4> </div>
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-modify">Modify</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> -->

			<div class="hotel-book-modify" style="display: none;">
				<!-- Search Contents -->
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="search">
								<div class="fill_height">
									<div class="search_panel active">
										<form action="#" id="search_form_2" class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start hotels">
											<div class="search_item">
												<div>Going to</div>
												<div class="autocomplete-wrapper"><input type="text" class="destination search_input autocomplete" required="required" placeholder="City / Hotel" autocomplete="off" id="autocomplete2"><div class="autocomplete-results"></div></div> <i class="fa fa-map-marker fa-icon"></i> </div>
											<div class="search_item">
												<div>check in</div>
												<input type="text" class="check_in search_input" placeholder="YYYY-MM-DD" autocomplete="off" id="check-in"> <i class="fa fa-calendar fa-icon"></i> </div>
											<div class="search_item">
												<div>check out</div>
												<input type="text" class="check_out search_input" placeholder="YYYY-MM-DD" autocomplete="off" id="check-out"> <i class="fa fa-calendar fa-icon"></i> </div>
											<div class="search_item">
												<div>Rooms</div>
												<select name="adults" id="adults_2" class="dropdown_item_select search_input">
													<option>01</option>
													<option>02</option>
													<option>03</option>
													<option>04</option>
													<option>05</option>
													<option>06</option>
												</select> <i class="fa fa-building fa-icon"></i> </div>
											<div class="search_item">
												<div>adults</div>
												<select name="adults" id="adults_2" class="dropdown_item_select search_input">
													<option>01</option>
													<option>02</option>
													<option>03</option>
													<option>04</option>
													<option>05</option>
													<option>06</option>
												</select> <i class="fa fa-user fa-icon"></i> </div>
											<div class="search_item">
												<div>children</div>
												<select name="children" id="children_2" class="dropdown_item_select search_input">
													<option>00</option>
													<option>01</option>
													<option>02</option>
													<option>03</option>
												</select> <i class="fa fa-child fa-icon"></i> </div>
											<div class="text-center">
												<button class="button search_button">search<span></span><span></span><span></span></button>
											</div>
										</form>
									</div>
									<!-- Search Panel -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			@if($hotelinfoarray['Error']['ErrorCode']==0)
				<section class="hotel-book-details" style="   border-top: 1px solid #d6d6d6; width: 100%">
					<div class="container">
						<div class="row">
							<div class="col-lg-8">
								<div class="home">
									<div class="home_slider_container">
										<div class="owl-carousel owl-theme home_slider">
										<?php
										$countimages=count($hotelinfoarray['HotelDetails']['Images']);
										?>
										@for($images=0;$images<$countimages;$images++)
											<!-- Slider Item -->
												<div class="owl-item home_slider_item">
													<div class="home_slider_background">
														<img class="h-img-div" src="{{$hotelinfoarray['HotelDetails']['Images'][$images]}}">
													</div>
												</div>
											@endfor


										</div>

										<!-- Home Slider Nav - Prev -->
										<div class="home_slider_nav home_slider_prev">
											<svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 width="28px" height="33px" viewBox="0 0 28 33" enable-background="new 0 0 28 33" xml:space="preserve">
												<defs>
													<linearGradient id='home_grad_prev'>
														<stop offset='0%' stop-color='#fa9e1b'/>
														<stop offset='100%' stop-color='#fa9e1b'/>
													</linearGradient>
												</defs>
												<path class="nav_path" fill="#fa9e1b" d="M19,0H9C4.029,0,0,4.029,0,9v15c0,4.971,4.029,9,9,9h10c4.97,0,9-4.029,9-9V9C28,4.029,23.97,0,19,0z
												M26,23.091C26,27.459,22.545,31,18.285,31H9.714C5.454,31,2,27.459,2,23.091V9.909C2,5.541,5.454,2,9.714,2h8.571
												C22.545,2,26,5.541,26,9.909V23.091z"/>
												<polygon class="nav_arrow" fill="#fa9e1b" points="15.044,22.222 16.377,20.888 12.374,16.885 16.377,12.882 15.044,11.55 9.708,16.885 11.04,18.219
												11.042,18.219 "/>
											</svg>
										</div>

										<!-- Home Slider Nav - Next -->
										<div class="home_slider_nav home_slider_next">
											<svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 width="28px" height="33px" viewBox="0 0 28 33" enable-background="new 0 0 28 33" xml:space="preserve">
												<defs>
													<linearGradient id='home_grad_next'>
														<stop offset='0%' stop-color='#fa9e1b'/>
														<stop offset='100%' stop-color='#fa9e1b'/>
													</linearGradient>
												</defs>
												<path class="nav_path" fill="#fa9e1b" d="M19,0H9C4.029,0,0,4.029,0,9v15c0,4.971,4.029,9,9,9h10c4.97,0,9-4.029,9-9V9C28,4.029,23.97,0,19,0z
											M26,23.091C26,27.459,22.545,31,18.285,31H9.714C5.454,31,2,27.459,2,23.091V9.909C2,5.541,5.454,2,9.714,2h8.571
											C22.545,2,26,5.541,26,9.909V23.091z"/>
												<polygon class="nav_arrow" fill="#fa9e1b" points="13.044,11.551 11.71,12.885 15.714,16.888 11.71,20.891 13.044,22.224 18.379,16.888 17.048,15.554
											17.046,15.554 "/>
											</svg>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="hotel-book-list">
									<h4 class="hotel-title" style="color: #00206A;font-weight: 600">{{$hotelinfoarray['HotelDetails']['HotelName']}}
										<div style="margin-top: 5px">
											@for($star=1;$star<=$hotelinfoarray['HotelDetails']['StarRating'];$star++)
												<i class="fa fa-star" style="color: #fedb00;margin-right: -5px;"></i>&nbsp;
											@endfor
										</div>
									</h4>
								</div>

								<div class="main-amenities" style="    border-top: 1px solid #d0d0d0; width: 100%">
									<h3 style="margin-top: 15px">Amenities</h3>
									<div class="row">
										@for($amenties=0;$amenties< 4;$amenties++)
										@if(!empty($hotelinfoarray['HotelDetails']['HotelFacilities'][$amenties]))
											<div class="col-md-6">
												<div class="amenities">
													<div class="item" style="font-size:14px"><i class="fa fa-check"></i>

														{{$hotelinfoarray['HotelDetails']['HotelFacilities'][$amenties]}}
													</div>
												</div>
											</div>
											@endif

										@endfor
									</div>
									<div class="row">
										<div class="col-12 text-right" style="padding: 10px 0 0 0">
											<a class="view-all" href="#view" data-toggle="modal">All Amenities <i class="fa fa-angle-right"></i> </a>
										</div>

										<div class="modal" id="view">
											<div class="modal-dialog modal-dialog-centered">
												<div class="modal-content" style="margin: 0">

													<!-- Modal Header -->
													<div class="modal-header">
														<h4 class="modal-title" style="color: #00206A;font-weight: 600;">Property amenities</h4>
														<button type="button" class="close text-left" data-dismiss="modal">&times </button>
													</div>

													<!-- Modal body -->
													<div class="modal-body" style="height: 500px; overflow: auto;">
														<div class="row">

															<div class="a-head col-12">
																<h3>Popular Amenities</h3>
															</div>
															<?php
															$hotelcount1 = count($hotelinfoarray['HotelDetails']['HotelFacilities']);
															?>

															@for($amenties=0;$amenties< $hotelcount1;$amenties++)
																<div class="col-md-6">
																	<div class="amenities">
																		<div class="item" style="font-size:14px"><i class="	fa fa-check"></i>{{$hotelinfoarray['HotelDetails']['HotelFacilities'][$amenties]}}</div>
																	</div>
																</div>

															@endfor
														</div>
													</div>
													<div class="modal-footer">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<div class="hotel-book-list">
											<p style="margin-top: 10px;margin-bottom: 20px;text-align: center;"><i class="fa fa-map-marker"></i>&nbsp; {{$hotelinfoarray['HotelDetails']['Address']}}</p>
											{{--<div class="check-in-check-out"><i class="fa fa-clock-o"></i>&nbsp; Check-in 02:00 PM - Check-out 11:00 AM</div>--}}

										</div>
									</div>
									{{--newmap--}}
									<div class="col-12" id="location-invoice">


											<div class="h-loc">
											<iframe class="loc-map" src="https://maps.google.com/maps?q={{$hotelinfoarray['HotelDetails']['Latitude']}},{{$hotelinfoarray['HotelDetails']['Longitude']}}&hl=es;z=14&amp;output=embed"  frameborder="0" style="border:0" height="170"  allowfullscreen></iframe>
											<br />
											<small>
												<a
														href="https://maps.google.com/maps?q={{$hotelinfoarray['HotelDetails']['Latitude']}},{{$hotelinfoarray['HotelDetails']['Longitude']}}&hl=es;z=14&amp;output=embed"
														style="color:#0000FF;text-align:left"
														target="_blank"
												>
													See map bigger
												</a>
											</small>
											</div>
											<!--   <iframe class="loc-map" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d872801.4155179203!2d74.73973083359868!3d31.293226109097468!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1shotel+redison+blue!5e0!3m2!1sen!2sin!4v1560852584413!5m2!1sen!2sin"  frameborder="0" style="border:0" allowfullscreen></iframe> --> </div>
									</div>
									{{--endmap--}}
										{{--<div class="a-loc">--}}
											{{--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3397.550139760296!2d74.87908631463341!3d31.618784349440542!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39197ca9251bb169%3A0xd650cf646eea85db!2sHotel+Krishnas!5e0!3m2!1sen!2sin!4v1562315447030!5m2!1sen!2sin"  height="250" frameborder="0" style="width:100%; border:0" allowfullscreen></iframe>--}}
										{{--</div>--}}
									</div>
									<!-- 	<div class="col-md-4">
                                            <div class="amenities">
                                                <div class="item"><i class="fa fa-coffee"></i> Room Service</div>
                                                <div class="item"><i class="fa fa-wifi"></i> Bar</div>
                                                <div class="item"><i class="fa fa-coffee"></i> Restaurant</div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="amenities">
                                                <div class="item"><i class="fa fa-cutlery"></i> Breakfast</div>
                                                <div class="item"><i class="fa fa-wifi"></i> Swimming Pool</div>
                                            </div>
                                        </div> -->
								</div>
							</div>
						</div>
					</div>
					<style>
						.a-head.col-12 h3 {
							margin-bottom: 30px;
						}
						.view-all{
							text-align: right !important;
							color: #0808b1 !important;
							font-size: 14px;
							font-weight: 600;
						}
						.d-card {
    background: #f5f5f5;
    padding: 20px;
    border-radius: 10px;
    margin-bottom: 30px;
}

						.hotel-book-price .hotel-price .inner-box .hotel-total-price .inner-div .room-amenities-img .amenities {
							font-size: 22px !important;
							color: #00206A;
							font-weight: 600;
						}
						.hotel-book-price .hotel-price .inner-box .hotel-total-price .room-size .room {
							color: #fa9e1b;
							font-weight: 600;
							font-size: 22px !important;
							margin-bottom: 0;
						}
						.room-details.text-left i {
							font-size: 18px !important;
						}
						.hotel-book-price .hotel-price .inner-box .hotel-total-price .room-details {
							color: #fa9e1b;
							font-weight: 600;
							font-size: 16px!important;
						}
						.hotel-book-price .hotel-price .inner-box .hotel-total-price .room-meal-price-height {
							min-height: 60px;
							margin-bottom: -10px !important;
						}

					</style>


					<div class="container">
						<div class="row">
							<div class="col-12">
								<div class="hotel-book-price">
									<h3>Select Room</h3>
									<?php
									$st=0;
									$sequenceno="";
									for($hr=0;$hr<count($hotelroomsarray['HotelRoomsDetails']);$hr++)
									{

									$cancelpolicy=explode('#',$hotelroomsarray['HotelRoomsDetails'][$hr]['CancellationPolicy']);
										if(!empty($cancelpolicy[2]))
										{
											$cancelpolicy_show=str_replace('|','',$cancelpolicy[2]);
										}
										else
										{
											$cancelpolicy_show="";
										}

									$amenties_hotel='';

									for($amenties=0;$amenties<count($hotelroomsarray['HotelRoomsDetails'][$hr]['Amenities']);$amenties++)
									{
										$amenties_hotel.=$hotelroomsarray['HotelRoomsDetails'][$hr]['Amenities'][$amenties];
										if($amenties>=1)
										{
											$amenties_hotel.=',';
										}
									}
									if(($hotelroomsarray['HotelRoomsDetails'][$hr]['SequenceNo']==$sequenceno) && $hr!=0)
									{
										continue;
									}

									?>
									<div class="hotel-price">
                                        
										<div class="inner-box">
											<div class="hotel-total-price">
												<div class="row">
													<div class="col-md-3 col-sm-12">
												<div class="inner-div room-amenities-height">
													<div class="room-amenities-img left-price">
														<?php  $ch =$st++;  ?>
														
														<img src="{{$hotelinfoarray['HotelDetails']['Images'][0]}}" class="img-responsive room-img"></span>
													</div>
												</div>
													</div>
													<div class="col-md-5 col-sm-12">
														<div class="room-size">
														<p class="room">
														
														{{$hotelroomsarray['HotelRoomsDetails'][$hr]['RoomTypeName']}}</p>

														</div>
                                                       

														<div class="inner-div room-meal-price-height">
															@for($amenties=0;$amenties< 4;$amenties++)
															@if(!empty($hotelinfoarray['HotelDetails']['HotelFacilities'][$amenties]))
																<div class="col-md-12" style="padding: 0">
																	<div class="amenities a1">
																		<div class="item" style="font-size:14px"><i class="fa fa-check"></i>{{$hotelinfoarray['HotelDetails']['HotelFacilities'][$amenties]}}</div>
																	</div>
																</div>
																@endif

															@endfor
														</div>
													
														<div class="inner-div room-meal-price-height n-am">
															<div class="room-meal-price left-price">
																<!-- <input type="radio" class="meal-option" name="meal" id="meal-option"> <label for="meal-option"> -->
																@if($amenties_hotel!='')
																	
																	@if(strlen($amenties_hotel)<=180)
																	With {{$amenties_hotel}}
																	@else
																	With {{substr($amenties_hotel,0,180)}}
																	<div id="demo{{$hr}}" class="collapse">
																		{{substr($amenties_hotel,180,-1)}}
																	</div>
																	<button data-toggle="collapse" data-target="#demo{{$hr}}" class="read-more">Read More</button>

																	
																	@endif
																@else
																	{{--No Amenties--}}
																	@endif</label>

															</div>
														</div>
													</div>
													<div class="col-md-4 col-sm-12">

														<div class="room-details text-right"
															 href="#" style="cursor:pointer; font-weight: bold;font-size: 22px !important;"><i class="fa {{$hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmaincurrecny']}}">
															</i>
															<span class="room-price right-price" style="color: #00206A;">
																<!-- <strike><i class="fa fa-inr strike-inr"></i>{{$hotelroomsarray['HotelRoomsDetails'][$hr]['Price']['PublishedPriceRoundedOff']}}</strike><br> -->
																<?php echo round($hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmainprice'] + $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelservicetax'] + $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelothercharges'] ) ?>
																	</span>



														</div>
														<p style="text-align: right;font-size: 18px;" class="per"> Per room</p>

														<div class="inner-div book-btn-1">
															<form action="{{route('hotelbook')}}" method="post">
																{{ csrf_field() }}
																<input type="hidden" name="resultindex" value="{{$bookingarray['hotelresultindex']}}">
																<input type="hidden" name="hotelcode" value="{{$bookingarray['hotelcode']}}">
																<input type="hidden" name="hotelname" value="{{$bookingarray['hotelname']}}">
																<?php
																for($roomindexcount=0;$roomindexcount< count($hotelroomsarray['RoomCombinations']['RoomCombination']);$roomindexcount++)
																{
																$roomindexdata=$hotelroomsarray['RoomCombinations']['RoomCombination'][$roomindexcount]['RoomIndex'];
																if($roomindexdata[0]==$hotelroomsarray['HotelRoomsDetails'][$hr]['RoomIndex'])
																{
																$roomindexes=implode('-',$roomindexdata);
																?>
																<input type="hidden" name="roomindex" value="{{$roomindexes}}">
																<input type="hidden" name="hotelroomname" value="{{$hotelroomsarray['HotelRoomsDetails'][$hr]['RoomTypeName']}}">

																<?php
																}
																}
																?>

																<button class="hotel-book-btn" type="submit">Book Now</button>
															</form>
														</div>
													</div>
												</div>

												<div class="inner-div">
													<div class="row">
														<div class="col-sm-6">
															<div class="room-details"
																 data-toggle="collapse" href="#collapseExample{{$hr}}" role="button" aria-expanded="false" aria-controls="collapseExample" style="cursor:pointer">Details</div>
														</div>

													</div>
													<div class="collapse" id="collapseExample{{$hr}}">
														<div class="card card-body" style="margin-top: 20px">
															<h3 class="text-center">Price Breakup</h3>
															<table class="table room-table">

																<tr>
																	<th>RoomPrice</th>
																	<td><i class="i-res fa {{$hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmaincurrecny']}}" style="color: orange; font-size: 18px"></i> {{number_format($hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmainprice'],2)}}</td>
																</tr>
																<tr>
																	<th>Tax</th>
																	<td><i class="i-res fa {{$hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmaincurrecny']}}" style="color: orange; font-size: 18px"></i> {{ number_format($hotelroomsarray['HotelRoomsDetails'][$hr]['hotelservicetax'],2)}}</td>
																</tr>
																
																<tr>
																	<th>OtherCharges</th>
																	<td><i class="i-res fa {{$hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmaincurrecny']}}" style="color: orange; font-size: 18px"></i> {{number_format($hotelroomsarray['HotelRoomsDetails'][$hr]['hotelothercharges'],2)}}</td>
																</tr>
																<tr>
																	<th> Total Price</th>
																	<td> <i class="i-res fa {{$hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmaincurrecny']}}" style="color: orange; font-size: 18px"> </i> <?php echo number_format($hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmainprice'] + $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelservicetax'] + $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelothercharges'] ,2) ?></td>
																</tr>
																<tr>
																	<th>Round Off Total Price</th>
																	<td> <i class="i-res fa {{$hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmaincurrecny']}}" style="color: orange; font-size: 18px"> </i> <?php echo round($hotelroomsarray['HotelRoomsDetails'][$hr]['hotelmainprice'] + $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelservicetax'] + $hotelroomsarray['HotelRoomsDetails'][$hr]['hotelothercharges'] ) ?></td>
																</tr>
															</table>
														</div>
													</div>
												</div>








											</div>
										</div>
									</div>
								<?php
								$sequenceno=$hotelroomsarray['HotelRoomsDetails'][$hr]['SequenceNo'];
								}
								?>

								<!-- <div class="hotel-price">
										<h4 class="room-type">Standard King Room</h4>
										<div class="inner-box">
											<div class="hotel-total-price">
													<div class="room-size">
														<p class="room">Double</p>
													</div>
												<div class="inner-div room-amenities-height">
													<div class="room-amenities-img left-price">
														<a href="javascript:void()" class="amenities">Room Amenities</a><span class="right-price"><img src="http://travoweb.com/apiflight/assets/images/test_1.jpg" class="img-responsive room-img"></span>
													</div>
												</div>
												<div class="inner-div room-meal-price-height">
													<div class="room-meal-price left-price">
														<input type="radio" class="meal-option" name="meal" id="meal-option1"> <label for="meal-option1">With Breakfast</label> <span class="room-price right-price"><strike><i class="fa fa-inr strike-inr"></i> 5875</strike><br><i class="fa fa-inr"></i> 4700</span>
													</div>
												</div>
												<div class="inner-div">
													<div class="room-details text-right">Details</div>
												</div>
												<div class="inner-div">
													<button class="hotel-book-btn" type="submit">Book Now</button>
												</div>
											</div>
										</div>
									</div>

									<div class="hotel-price">
										<h4 class="room-type">Standard Room</h4>
										<div class="inner-box">
											<div class="hotel-total-price">
													<div class="room-size">
														<p class="room">Double</p>
													</div>
												<div class="inner-div room-amenities-height">
													<div class="room-amenities-img left-price">
														<a href="javascript:void()" class="amenities">Room Amenities</a><span class="right-price"><img src="http://travoweb.com/apiflight/assets/images/test_1.jpg" class="img-responsive room-img"></span>
													</div>
												</div>
												<div class="inner-div room-meal-price-height">
													<div class="room-meal-price left-price">
														<input type="radio" class="meal-option" name="meal" id="meal-option2"> <label for="meal-option2">With Breakfast</label> <span class="room-price right-price"><strike><i class="fa fa-inr strike-inr"></i> 5875</strike><br><i class="fa fa-inr"></i> 4700</span>
													</div>
												</div>
												<div class="inner-div">
													<div class="room-details text-right">Details</div>
												</div>
												<div class="inner-div">
													<button class="hotel-book-btn" type="submit">Book Now</button>
												</div>
											</div>
										</div>
									</div>

									<div class="hotel-price">
										<h4 class="room-type">Standard Twin Room</h4>
										<div class="inner-box">
											<div class="hotel-total-price">
													<div class="room-size">
														<p class="room">Twin</p>
													</div>
												<div class="inner-div room-amenities-height">
													<div class="room-amenities-img left-price">
														<a href="javascript:void()" class="amenities">Room Amenities</a><span class="right-price"><img src="http://travoweb.com/apiflight/assets/images/test_1.jpg" class="img-responsive room-img"></span>
													</div>
												</div>
												<div class="inner-div room-meal-price-height">
													<div class="room-meal-price left-price">
														<input type="radio" class="meal-option" name="meal" id="meal-option3"> <label for="meal-option3">With Breakfast</label> <span class="room-price right-price"><strike><i class="fa fa-inr strike-inr"></i> 5875</strike><br><i class="fa fa-inr"></i> 4700</span>
													</div>
												</div>
												<div class="inner-div">
													<div class="room-details text-right">Details</div>
												</div>
												<div class="inner-div">
													<button class="hotel-book-btn" type="submit">Book Now</button>
												</div>
											</div>
										</div>
									</div> -->
								</div>
							</div>
						</div>
						<div class="row  d-card">
							<div class="col-12">
								<h3 style="color:#00206A;font-weight: 600;margin: 20px 0;">
									About This Area
								</h3>
							</div>

							<div class="col-12">
								<div class="about-hotel">
									<p><?php echo $hotelinfoarray['HotelDetails']['Description'] ?></p>
								</div>
								@if(count($hotelinfoarray['HotelDetails']['HotelFacilities'])>=1 && $hotelinfoarray['HotelDetails']['HotelFacilities'][0]!='')

								@endif

								@if($hotelinfoarray['HotelDetails']['Attractions'][0]['Value']!='' && $hotelinfoarray['HotelDetails']['Attractions']!=null)
									<div class="hotel-impo-details">
										<h3>Attractions</h3>
										<?php
										for($att=0;$att<count($hotelinfoarray['HotelDetails']['Attractions']);$att++)
										{
											$attractions2='';
											$attract=explode('. ',$hotelinfoarray['HotelDetails']['Attractions'][$att]['Value']);
											if(count($attract)>1)
											{
												$attractions=str_replace('p>','ul>',$attract[1]);
												$attractions1=str_replace('<ul>','<ul><li>',$attractions);
												$attractions2=str_replace('<br','<li',$attractions1);
												$attractions2=preg_replace('/<li[^>]*>/i',' ',$attractions2,1);
												echo $attract[0].$attractions2;
											}
											else
											{
												echo "<ul><li>".$attract[0]."</li></ul>".$attractions2;
											}
										}
										?>
									</div>
								@endif


							</div>
							<div class="col-12">

								@if($hotelinfoarray['HotelDetails']['HotelPolicy']!=null && $hotelinfoarray['HotelDetails']['HotelPolicy'][0]!='')
									<div class="hotel-impo-details">
										<h3>Important Details</h3>
										<?php

										$hotelpolicy=explode('.|',$hotelinfoarray['HotelDetails']['HotelPolicy']); ?>

										<ul>
											<?php
											if(!empty($hotelpolicy[1]))
											{


											$hotelpolicy_points=explode('.',$hotelpolicy[1]);
											?>
											@foreach($hotelpolicy_points as $data)
												@if($data!='|')
													<li>{{$data}}</li>
												@endif
											@endforeach

											
										<?php } ?>
										<li><strong>IMPORTANT NOTE:</strong> {{$hotelpolicy[0]}}</li>
										</ul>
									</div>
								@endif
							</div>
						</div>
					</div>
				</section>
			@elseif($hotelinfoarray['Error']['ErrorCode']==6)
				<br>
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="intro_content nano-content">
								<div class="no-flight">
									<h3>Your Session has been expired</h3>
									<a href="{{url('/index')}}" class="btn btn-back">Go Back</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			@else
				<br>
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="intro_content nano-content">
								<div class="no-flight">
									<h3>No Rooms Found</h3>
									<button class="btn btn-back" onclick="window.history.back()">Go Back</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>
</div>
<style>
	.item i {
		margin-right: 5px;
	}
	.hotel-book-price .hotel-price .inner-box .hotel-total-price .inner-div .room-amenities-img .room-img {
		height: 200px !important;
		width: 250px !important;
		object-fit: cover;
		margin-top: 19px;
		border-radius: 7px !important;
	}
	.hotel-book-list .hotel-title {
		background:none !important;
		color: #000;
		padding: 6px 1px;
		border-radius: 4px;
		font-size: 21px;
	}

	.hotel-book-list p {
		color: #000;
		font-size: 13px !important;
		margin-bottom: 5px;
		font-weight: 600;
	}
	.hotel-book-price .hotel-price .inner-box .hotel-total-price .inner-div .hotel-book-btn {
		background: #1f0733;
		color: #FFF;
		padding: 6px 15px 7px;
		font-weight: 600;
		font-size: 22px;
		border-radius: 3px;
		width: auto !important;
		margin-left: auto !important;
		display: block !important;
		border: 1px solid #1f0733;
		cursor: pointer;
	}
	iframe.loc-map {
		width: 100%;
		height: 66px;
	}
	.hotel-book-price .hotel-price .inner-box .hotel-total-price .room-meal-price-height {
		min-height: 60px;
		margin-bottom: 0 !important;
	}
	.hotel-book-price .hotel-price .inner-box .hotel-total-price .inner-div .right-price {
		color: #00206A;
		 float: none !important;
		font-weight: 600;
		font-size: 16px;
	}
	.hotel-book-price .hotel-price .inner-box .hotel-total-price .room-size .room {
		color: #fa9e1b;
		font-weight: 600;
		font-size: 21px !important;
		margin-bottom: -65px !important;
	}

	.room-details.text-left i {
		font-size: 22px !important;
		text-align: right !important;
		display: block !important;
		position: absolute !important;
		right: 0px !important;
		/* top: 55%; */
		/* bottom: -190px !important; */
	}
	a.amenities {
		font-size: 18px !important;
	}
	@media  screen and (max-width: 768px){

		.hotel-book-price .hotel-price .inner-box .hotel-total-price .inner-div .room-amenities-img .room-img {
			height: 200px !important;
			width: 100% !important;
			object-fit: cover;
			margin-top: 19px;
			border-radius: 7px !important;
		}
		.hotel-book-price .hotel-price .inner-box .hotel-total-price .inner-div {
			margin-bottom: 0px !important;
		}
		p.per {
			margin-bottom: -10px !important;
		}
	}
	.item {
		color: #666 !important;
		margin: 8px 0px !important;
	}
	.room-size {
		margin-top: 11px !important;
	}
	.hotel-book-price .hotel-price .inner-box .hotel-total-price .inner-div .room-meal-price {
		font-weight: 600;
		font-size: 17px;
		padding: 5px 0 !important;
		margin: 0px 0px 10px 0 !important;
	}
	.inner-div.room-meal-price-height.n-am {
		position: relative;
		top: 55px;
	}
	.amenities.a1 {
		top: 56px !important;
	}
	.inner-div.book-btn-1 {
		position: relative;
		top: 22%;
	}
	.room-table {
		width: 100%;
		max-width: 100%;
		margin-bottom: 1rem;
		background-color: transparent;
		border: 1px solid #ccc !important;
	}
	.room-table th,.room-table td{
		border: 1px solid #ccc !important;
	}

	.room-table th{
		color:#00206A;
	}
	.room-table td{
		color:black;
	}

	table {
		border-collapse: collapse;
	}

</style>
<div class='session-counter'>
	<p class='timer' data-minutes-left=15>Your booking session will expire in </p>
	<section class='actions'></section>
</div>
<style>
	.session-counter {
		background: #000000d9;
		color: white;
		padding: 5px;
		position: fixed;
		bottom: 0;
		display: block;
		width: 100%;
		z-index: 1;
	}

	.session-counter p{
		margin: 0;
		color: white;
		font-size: 17px;
		font-weight: 500;
		text-align: center;
		z-index: 1;
	}
	.jst-hours,.jst-minutes,.jst-seconds {
		display: inline;
		color: orange;
	}

</style>
<!-- Footer -->@include('pages.include.footer')
<!-- Copyright -->@include('pages.include.copyright')
<script>
	$(document).ready(function() {
		$(".btn-modify").click(function() {
			$(".hotel-book-modify").toggle();
		});
	});
</script>
​<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>
</body>

</html>