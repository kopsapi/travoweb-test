@include('pages.include.header')<!-- Loader -->

<style>

    .loader {

        display: block;

        position: fixed;

        left: 0px;

        top: 0px;

        width: 100%;

        height: 100%;

        z-index: 9999;

        background: #CCEAF7 url('{{ asset('assets/images/flight-loader.gif')}}') no-repeat center center;

        text-align: center;

        color: #999;

        opacity: 0.9;

    }



    .detail b {

        font-size: 20px;

        color: blue;

        padding-left: 5px;

        font-family: monospace;

    }



    .col-sm-6.detail {

        text-align: right;

    }



    .hotel-time-img {

        width: 30px;



    }



    img.hotel-icon {

        width: 40px;

    }



    img.room-detail {

        width: 100px;

    }



    .hotel-info {

        padding: 25px;

    }



    .hotel-info {

        border: 1px solid #bfbfbf;

        padding: 5px 10px;

        border-bottom: 2px solid #1f3b7c;

    }



    a.hotel-email {

        text-decoration: none;

        color: #6cc4ee;

        border: none;

        background-color: transparent;

    }



    a.btn.btn-success.pull-right {

        margin-right: 10px;

    }



    a.hotel-email:hover {

        text-decoration: none;

        color: #4ca3ee;

        background-color: transparent;

    }



    .or {

        position: absolute;

        left: 50%;

        transform: translate(-50%, -116%);

        background-color: #f1f1f1;

        color: black;

        border: 1px solid #868e96;

        border-radius: 50%;

        padding: 8px 10px;

    }



    .col-12.need-help h3, .col-12.need-help p {

        color: black;

        text-align: center;

    }



    th.room-detail {

        color: blue;

    }



    th.table-head {

        color: #000;

        border-top-left-radius: 5px;

        border-bottom-left-radius: 5px;

        vertical-align: middle;



    }



    .table td {

        padding: 0.75rem;

        color: #000;

        border-top-right-radius: 5px;

        border-bottom-right-radius: 5px;

    }



    .room-info h1 {

        color: #ea8218;

        text-align: center;

        font-size: 25px;

    }



    .room-info h1 img {

        color: #2b6eab;

    }



    iframe.loc-map {

        width: 100%;

        height: auto;

    }



    img.loc-img {

        width: 50px;

        /* margin-left: 10px; */

    }



    h2.loc {



        text-align: center;

        font-size: 27px;

        color: orange;

        font-weight: 500;

        margin-bottom: 20px;

    }



    img.offer {

        width: 59px;

        margin-top: 13px;

    }



    img.save-money {

        width: 57px;

    }



    img.promo {

        width: 50px;

        margin-right: 10px;

    }



    .hotel-limit {

        margin-top: 30px;

    }



    img.room-type {

        width: 325px;

    }



    .limit p, .limit1 p {

        color: black;

        text-align: center;

        /* top: 3px; */

        margin-top: -44px;

    }



    .limit {

        width: 50%;

        height: 19px;

        float: left;

        border-top: 14px solid #28a772;

        border-left: 2px solid #28a772;

        margin-bottom: 48px;

    }



    .limit1 {

        width: 50%;

        height: 19px;

        float: left;

        border-top: 14px solid #ce5050;

        border-left: 2px solid #ce5050;

        margin-bottom: 48px;

    }



    .confirm h2{

        color: black;

    }







    .confirm h2 {

        padding-bottom: 25px;

    }



    .hotel-id span {

        color: #017c60;

        font-size: 15px;

        font-weight: 700;

    }





    .confirm h4 {

        margin: 0;

        font-size: 18px;

    }



    .total-amount span {

        color: #ff6900;

        font-weight: 700;

        font-size: 15px;

        text-transform: uppercase;

    }



    .hotel-img img {

        width: 100%;

        display: block;

        padding: 3px;

        height: 270px;

        border-radius: 2px;

        border: 1px solid #27368c;

        /* margin: auto; */

    }

    .col-6.total-amount {

        text-align: right;

    }



    .detail p i {

        color: black;

        padding-left: 5px;



    }



    .confirm h2 > img {

        width: 80px;

        margin-left: -12px;

    }

    a.hotel-name {

        text-decoration: none;

        font-size: 18px;

        color: white;

        background: #1f3b7c;

        font-weight: 400;

        display: block;

        padding: 5px 10px;

        border-top-left-radius: 5px;

        border-top-right-radius: 5px;

    }



    p.addr {

        color: #000000;

        font-size: 16px;

        margin: 0;

        font-weight: 600;

    }







    .col-md-6.hotel-info p {

        color: black;

        margin-top: -10px;



    }



    .col-md-6.hotel-info p.addr {

        margin-top: 30px;

    }



    img.room-type-img {

        width: 22px;

        transform: rotate(90deg);

        margin-left: 0px;

        padding-top: 5px;

    }



    .col-md-6.hotel-info p a.hotel-email {

        text-decoration: none;

        border: 0;

        outline: 0;

        color: cornflowerblue;



    }



    .col-md-6.hotel-info p a.hotel-email:hover {

        background-color: #fffffb;

        color: #4c74ff;

    }



    a.manage-book {

        text-decoration: none;

        padding: 15px 20px;

        color: white;

        font-size: 25px;

        margin: 26px;

        background: linear-gradient(to right, rgba(255, 153, 83, 0.8), #ff5f5f);

        border: 1px solid #ff5f5f;

        border-radius: 10px;

    }



    a.manage-book:hover {

        text-decoration: none;

        padding: 15px 20px;

        color: #ff5f5f;

        background: transparent;

        border: 1px solid #ff5f5f;



    }



    .col-12.guest-info p {

        font-size: 19px;

        color: #ff6565;

        text-transform: capitalize;

    }



    .col-12.guest-info h5 {

        color: black;

        margin-left: 15px;

        font-size: 16px;

        position: relative;

    }



    .col-12.guest-info h5 i {



        padding-right: 8px;



    }



    .col-12.guest-info h5:before {

        position: absolute;

        content: "\f0da";

        font-family: "FontAwesome";

        top: -23%;

        left: -15px;

        color: #ff7819;

        font-size: 25px;

    }



    hr {



        width: 100%;



    }



    .hotel-time span {

        color: blue;

        font-weight: 600;

        text-align: center;



        display: block;

    }



    .hotel-time-img {

        width: 25px;



    }



    .hotel-time p {

        margin-top: 0px;

        text-align: center;

    }



    .room-type h5 {

        color: #ff3a00;

        font-size: 32px;

        text-transform: uppercase;



    }



    .room-type p {

        margin-left: 0px;

        color: black;

        margin-bottom: 10px;

    }



    .room-type p span {

        margin-left: 7px;

    }



    .hotel-info p i {

        color: #FF9800;

    }

    :target:before {

        content: "";

        display: block;

        height: 180px;

        margin: -180px 0 0;

    }

    p.price-para {

        margin: 0;

        color: #1f3b7c;

        font-size: 17px;

        font-weight: 600;

    }

</style>

<style>

    .col-12.guest-info p {

        font-size: 19px;

        padding: 5px 10px;

        margin-top: 10px;

        line-height: 1.5;

        background: #FF9800;

        color: #ffffff;

        margin-bottom: 0;

        border-top-left-radius: 5px;

        border-top-right-radius: 5px;



    }

    .over-img {

        position: relative;

        /* bottom: -8px; */

        top: 15%;

        animation: fadein 0.5s, fadeout 0.5s 2.5s;

        transition: all .5s ease;

        left: 50%;

        transform: translate(-50%, -50%);

        text-align: center;

        transition-delay: .5s;

    }



    .overlay {

        position: fixed;

        z-index: 999;

        top: 100%;

        left: 0;

        /* bottom: 0; */

        /* right: 0; */

        width: 100%;

        height: 0%;

        background-color: rgb(0, 0, 0);

        background-color: rgba(0, 0, 0, 0.8);

        overflow-x: hidden;

        transition: 1s;

        display: block;

    }



    .overlay-content {

        position: relative;

        top: 27%;

        left: 50%;

        transform: translate(-50%, -50%);

        text-align: center;

    }



    .over-img {

        position: absolute;



        top: 450px;

        animation: fadein 0.5s, fadeout 0.5s 2.5s;

        transition: all .5s ease;

        left: 50%;

        transform: translate(-50%, -50%);

        text-align: center;

        transition-delay: .5s;

    }



    @media screen and (max-width: 500px) {

        .over-img img {

            height: 210px !important;

        }



    }



    .overlay-content p {

        color: white;

        font-size: 17px;

    }



    .overlay-content h3 {

        color: white;

        font-family: inherit;

        font-size: 25px;

    }



    a.closebtn {

        color: white;

        font-size: 40px;

        font-weight: bolder;

        text-decoration: none;

        right: 30px;

        top: 30px;

        cursor: pointer;

        position: absolute;

    }



    @keyframes fadein {

        from {

            bottom: 0;

            opacity: 0;

        }

        to {

            bottom: -492px;

            opacity: 1;

        }

    }



    @media screen and (max-width: 650px) {

        .over-img {

            top: 450px;

        }

    }



</style>

<style>





    th {

           color: black;

        

    }

    p.heading-a {



           margin: 0;

        color: black;

        font-size: 35px;

        font-weight: 600;

    }

    span.t-due {

        font-size: 16px;

    }

    p.t-price {

        color: #FF9800;

        font-size: 25px;

        font-weight: 500;

        margin: 0;

    }

    p.div-sec {

        display: flex;

        flex-direction: column;

        justify-content: center;

        /* margin: 0; */

        margin: auto 20px;

        padding: 10px;

        min-height: 135px;

        text-align: center !important;

        border: 1px solid #9E9E9E;

        border-radius: 5px;

    }

    span.invoice-no {

        font-size: 17px;

        color: black;

        /* display: flex; */

    }



    span.i-num {

        /* display: flex; */

        color: #FF5722;

        font-size: 16px;

        font-weight: 600;

    }

    p.div-sec {

        /* margin: 0; */

        margin: auto 20px;

        padding: 10px;

        min-height: 135px;

        text-align: center;

        border: 1px solid #9E9E9E;

        border-radius: 5px;

    }

    p.info-detail {

        margin-bottom: 0;

        /* margin-top: 28px; */

    }

    span.date-start {

        color: #213290;

        font-size: 16px;

        font-weight: 600;

    }

    span.date-due {

        color: #28bd95;

        font-size: 16px;

        font-weight: 600;

    }

    table.table-div {

        width: 100%;

    }

    .div-border {

        border: 1px solid #c7c7c7;

        border-radius: 4px;

        padding-top: 30px;
margin-top: 30px;
        margin-bottom: 30px;
        padding: 20px;

    }
  
</style>

<!-- Loader -->

<body>

<div class="loader"></div>

<div class="super_container">

    <!-- Header -->

    @include('pages.include.topheader')



    <div class="home" style="height:20vh;">

    <!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->



        <div class="home_content">

            <div class="home_title">Results</div>

        </div>

    </div>









    <!--intro-->

    <div class="container div-border" id="div-border">

   <table class="table-div" style="width:100%">

       <tr style="border-bottom: 1px solid #c0c0c0;margin-bottom: 10px">

           <td colspan="2" style="width: 50%"><p class="heading-a">Invoice</p></td>

           <td colspan="2" style="width: 50%;text-align: right">

               <span class="t-due">Total Paid</span>

               <p class="t-price"><i class="fa fa-{{strtolower($booklistdetails->currency)}}"></i>{{$bookbalnew->offer_price}}</p>

           </td>

       </tr>

       <tr>

           <td style="width: 25%">

               <p class="info-detail" style="margin-top: 28px">Issue To</p>

               <p class="info-detail">Travo Web</p>

               <p class="info-detail">7/25 Sheridan Lane Gundagai NSW 27221</p>

               <p class="info-detail">+61 2 5924 0804</p>

               <p class="info-detail">info@travoweb.com</p>

           </td>

           <td style="width: 25%" class="text-right">

               <p class="div-sec" style="margin-top: 28px">

               <span class="invoice-no">Enquiry No</span>

               <span class="i-num">{{$booklistdetails->enq_id}}</span>

               </p>

           </td>

           <td style="width: 25%" class="text-right">

               <p class="div-sec" style="margin-top: 28px">

               <span class="invoice-no">Invoice Date</span>

               <span class="date-start"><?php echo  date("d/m/Y", strtotime($paymenttable->orderdate)); ?></span>

               </p>

           </td>

           <td style="width: 25%" class="text-right">

               <p class="div-sec" style="margin-top: 28px">

               <span class="invoice-no">Installment Date</span>

               <span class="date-due"><?php echo  date("d/m/Y", strtotime($bookbalnew->intallmendate)); ?></span>

               </p>

           </td>



           

       </tr>

       <tr>

           <td colspan="4">

               <table class="table table-hover mt-4">

                   <tbody>

                   <tr>

                       <th>Name</th>

                       <td style="text-align: right">{{ucfirst($booklistdetails->firstname)}} {{ucfirst($booklistdetails->lastname)}}</td>



                   </tr>

                   <tr>

                       <th>Enquiry Id</th>

                       <td style="text-align: right">{{$booklistdetails->enq_id}}</td>



                   </tr>

                   <tr>

                       <th>Package Name</th>

                       <td style="text-align: right">{{$booklistdetails->pckg_name}}</td>



                   </tr>

                   <tr>

                       <th>Package Amount</th>

                       <td style="text-align: right"><i class="fa fa-{{strtolower($booklistdetails->currency)}}"></i> {{$booklistdetails->pckg_price}}</td>



                   </tr>

                   <tr>

                       <th>Package Offer Amount</th>

                       <td style="text-align: right"><i class="fa fa-{{strtolower($booklistdetails->currency)}}"></i>{{$bookbalnew->offer_main_price}} </td>



                   </tr>

                   <tr>

                       <th>Paid Amount</th>

                       <td style="text-align: right"><i class="fa fa-{{strtolower($booklistdetails->currency)}}"></i>{{$bookbalnew->offer_price}}</td>



                   </tr>

                   <tr>

                       <th>Balance Amount</th>

                       <td style="text-align: right"><i class="fa fa-{{strtolower($booklistdetails->currency)}}"></i>{{$bookbalnew->offer_balance}}</td>



                   </tr>

                   <tr>

                       <th>Due Date</th>

                       <td style="text-align: right"><b><?php echo  date("d/m/Y", strtotime($bookbalnew->intallmendate)); ?></b>

                           

                       </td>



                   </tr>

                   </tbody>

               </table>

           </td>



       </tr>





        <tr>

            <td colspan="4">



                <table class="table table-hover mt-4">

                    <tbody>

                    <tr>

                        <td style="width: 25%"></td>

                        <td style="width: 25%"></td>

                        <th style="width: 25%">Order-Id</th>

                        <td style="text-align: right;width: 25%">{{$paymenttable->orderid}}</td>



                    </tr>

                    

                    <tr>

                        <td style=";width: 25%"></td>

                        <td style="width: 25%"></td>

                        <th style="width: 25%">Reference No</th>

                        <td style="text-align: right;width: 25%">{{$paymenttable->referenceid}}</td>



                    </tr>

                    <tr>

                        <td style="width: 25%"></td>

                        <td style="width: 25%"></td>

                        <th style="width: 25%">Date / Time</th>

                        <td style="text-align: right;width: 25%"><?php echo  date("d/m/Y", strtotime($paymenttable->orderdate)); ?> - {{$paymenttable->ordertime}}</td>



                    </tr>



                    </tbody>

                </table>

            </td>

        </tr>



   </table>

            <div style="text-align: center; margin: 20px;">

                <button class="btn btn-primary" id="print_btn" style="cursor: pointer;"> <span class="fa fa-print iconfont18"></span>

             Print This Package </button>

            </div>

    </div>

</div>

</body>



<!-- Footer -->

@include('pages.include.footer')

<!-- Copyright -->@include('pages.include.copyright')

<script>

    $(document).on("click","#print_btn",function()

    {
        $("#div-border").removeClass("container")
        //Hiding extra content for printing

        $(".header").hide();

        $(".home").hide();

      

        $('#print_btn').hide();

        $(".footer").hide();

        $(".copyright").hide();

      

        window.print();





        //displating hidden extra content after printing



        $(".header").show();

        $(".home").show();

        $('#print_btn').show();



        $(".footer").show();

        $(".copyright").show();





    });

</script>