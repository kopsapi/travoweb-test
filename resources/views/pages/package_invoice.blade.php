@include('pages.include.header')<!-- Loader -->

<style>
.day-card {
    border: 1px solid gainsboro;
    padding: 5px;
    border-radius: 5px;
}
    .loader {

        display: block;

        position: fixed;

        left: 0px;

        top: 0px;

        width: 100%;

        height: 100%;

        z-index: 9999;

        background: #CCEAF7 url('{{ asset('assets/images/flight-loader.gif')}}') no-repeat center center;

        text-align: center;

        color: #999;

        opacity: 0.9;

    }



    .detail b {

        font-size: 20px;

        color: blue;

        padding-left: 5px;

        font-family: monospace;

    }



    .col-sm-6.detail {

        text-align: right;

    }



    .hotel-time-img {

        width: 30px;



    }



    img.hotel-icon {

        width: 40px;

    }



    img.room-detail {

        width: 100px;

    }



    .hotel-info {

        padding: 25px;

    }



    .hotel-info {

        border: 1px solid #bfbfbf;

        padding: 5px 10px;

        border-bottom: 2px solid #1f3b7c;

    }



    a.hotel-email {

        text-decoration: none;

        color: #6cc4ee;

        border: none;

        background-color: transparent;

    }



    a.btn.btn-success.pull-right {

        margin-right: 10px;

    }



    a.hotel-email:hover {

        text-decoration: none;

        color: #4ca3ee;

        background-color: transparent;

    }



    .or {

        position: absolute;

        left: 50%;

        transform: translate(-50%, -116%);

        background-color: #f1f1f1;

        color: black;

        border: 1px solid #868e96;

        border-radius: 50%;

        padding: 8px 10px;

    }



    .col-12.need-help h3, .col-12.need-help p {

        color: black;

        text-align: center;

    }



    th.room-detail {

        color: blue;

    }



    th.table-head {

        color: #000;

        border-top-left-radius: 5px;

        border-bottom-left-radius: 5px;

        vertical-align: middle;



    }



    .table td {

        padding: 0.75rem;

        color: #000;

        border-top-right-radius: 5px;

        border-bottom-right-radius: 5px;

    }



    .room-info h1 {

        color: #ea8218;

        text-align: center;

        font-size: 25px;

    }



    .room-info h1 img {

        color: #2b6eab;

    }



    iframe.loc-map {

        width: 100%;

        height: auto;

    }



    img.loc-img {

        width: 50px;

        /* margin-left: 10px; */

    }



    h2.loc {



        text-align: center;

        font-size: 27px;

        color: orange;

        font-weight: 500;

        margin-bottom: 20px;

    }



    img.offer {

        width: 59px;

        margin-top: 13px;

    }



    img.save-money {

        width: 57px;

    }



    img.promo {

        width: 50px;

        margin-right: 10px;

    }



    .hotel-limit {

        margin-top: 30px;

    }



    img.room-type {

        width: 325px;

    }



    .limit p, .limit1 p {

        color: black;

        text-align: center;

        /* top: 3px; */

        margin-top: -44px;

    }



    .limit {

        width: 50%;

        height: 19px;

        float: left;

        border-top: 14px solid #28a772;

        border-left: 2px solid #28a772;

        margin-bottom: 48px;

    }



    .limit1 {

        width: 50%;

        height: 19px;

        float: left;

        border-top: 14px solid #ce5050;

        border-left: 2px solid #ce5050;

        margin-bottom: 48px;

    }



    .confirm h2{

        color: black;

    }







    .confirm h2 {

        padding-bottom: 25px;

    }



    .hotel-id span {

        color: #017c60;

        font-size: 15px;

        font-weight: 700;

    }





    .confirm h4 {

        margin: 0;

        font-size: 18px;

    }



    .total-amount span {

        color: #ff6900;

        font-weight: 700;

        font-size: 15px;

        text-transform: uppercase;

    }



    .hotel-img img {

        width: 100%;

        display: block;

        padding: 3px;

        height: 270px;

        border-radius: 2px;

        border: 1px solid #27368c;

        /* margin: auto; */

    }

    .col-6.total-amount {

        text-align: right;

    }



    .detail p i {

        color: black;

        padding-left: 5px;



    }



    .confirm h2 > img {

        width: 80px;

        margin-left: -12px;

    }

    a.hotel-name {

        text-decoration: none;

        font-size: 18px;

        color: white;

        background: #1f3b7c;

        font-weight: 400;

        display: block;

        padding: 5px 10px;

        border-top-left-radius: 5px;

        border-top-right-radius: 5px;

    }



    p.addr {

        color: #000000;

        font-size: 16px;

        margin: 0;

        font-weight: 600;

    }



    



    .col-md-6.hotel-info p {

        color: black;

        margin-top: -10px;



    }



    .col-md-6.hotel-info p.addr {

        margin-top: 30px;

    }



    img.room-type-img {

        width: 22px;

        transform: rotate(90deg);

        margin-left: 0px;

        padding-top: 5px;

    }



    .col-md-6.hotel-info p a.hotel-email {

        text-decoration: none;

        border: 0;

        outline: 0;

        color: cornflowerblue;



    }



    .col-md-6.hotel-info p a.hotel-email:hover {

        background-color: #fffffb;

        color: #4c74ff;

    }



    a.manage-book {

        text-decoration: none;

        padding: 15px 20px;

        color: white;

        font-size: 25px;

        margin: 26px;

        background: linear-gradient(to right, rgba(255, 153, 83, 0.8), #ff5f5f);

        border: 1px solid #ff5f5f;

        border-radius: 10px;

    }



    a.manage-book:hover {

        text-decoration: none;

        padding: 15px 20px;

        color: #ff5f5f;

        background: transparent;

        border: 1px solid #ff5f5f;



    }



    .col-12.guest-info p {

        font-size: 19px;

        color: #ff6565;

        text-transform: capitalize;

    }



    .col-12.guest-info h5 {

        color: black;

        margin-left: 15px;

        font-size: 16px;

        position: relative;

    }



    .col-12.guest-info h5 i {



        padding-right: 8px;



    }



    .col-12.guest-info h5:before {

        position: absolute;

        content: "\f0da";

        font-family: "FontAwesome";

        top: -23%;

        left: -15px;

        color: #ff7819;

        font-size: 21px;

    }



    hr {



        width: 100%;

      

    }



    .hotel-time span {

        color: blue;

        font-weight: 600;

        text-align: center;



        display: block;

    }



    .hotel-time-img {

        width: 25px;



    }



    .hotel-time p {

        margin-top: 0px;

        text-align: center;

    }



    .room-type h5 {

        color: #ff3a00;

        font-size: 32px;

        text-transform: uppercase;



    }



    .room-type p {

        margin-left: 0px;

        color: black;

        margin-bottom: 10px;

    }



    .room-type p span {

        margin-left: 7px;

    }



    .hotel-info p i {

        color: #FF9800;

    }

    :target:before {

        content: "";

        display: block;

        height: 180px;

        margin: -180px 0 0;

    }

    p.price-para {

        margin: 0;

        color: #1f3b7c;

        font-size: 17px;

        font-weight: 600;

    }

</style>

<style>

    .pkg-info h5:before{

        position: absolute;

        content: "\f0da";

        font-family: "FontAwesome";

        top: -23%;

        left: -15px;

        color: #1f3b7c;

        font-size: 21px;

    }



    .pkg-info h5{

        color: black;

        margin-left: 15px;

        font-size: 16px;

        position: relative;

    }

    .col-12.guest-info p {

        font-size: 19px;

        padding: 5px 10px;

        margin-top: 10px;

        line-height: 1.5;

        background: #FF9800;

        color: #ffffff;

        margin-bottom: 0;

        border-top-left-radius: 5px;

        border-top-right-radius: 5px;



    }

    .over-img {

        position: relative;

        /* bottom: -8px; */

        top: 15%;

        animation: fadein 0.5s, fadeout 0.5s 2.5s;

        transition: all .5s ease;

        left: 50%;

        transform: translate(-50%, -50%);

        text-align: center;

        transition-delay: .5s;

    }



    .overlay {

        position: fixed;

        z-index: 999;

        top: 100%;

        left: 0;

        /* bottom: 0; */

        /* right: 0; */

        width: 100%;

        height: 0%;

        background-color: rgb(0, 0, 0);

        background-color: rgba(0, 0, 0, 0.8);

        overflow-x: hidden;

        transition: 1s;

        display: block;

    }



    .overlay-content {

        position: relative;

        top: 27%;

        left: 50%;

        transform: translate(-50%, -50%);

        text-align: center;

    }



    .over-img {

        position: absolute;



        top: 450px;

        animation: fadein 0.5s, fadeout 0.5s 2.5s;

        transition: all .5s ease;

        left: 50%;

        transform: translate(-50%, -50%);

        text-align: center;

        transition-delay: .5s;

    }



    @media screen and (max-width: 500px) {

        .over-img img {

            height: 210px !important;

        }



    }



    .overlay-content p {

        color: white;

        font-size: 17px;

    }



    .overlay-content h3 {

        color: white;

        font-family: inherit;

        font-size: 25px;

    }



    a.closebtn {

        color: white;

        font-size: 40px;

        font-weight: bolder;

        text-decoration: none;

        right: 30px;

        top: 30px;

        cursor: pointer;

        position: absolute;

    }



    @keyframes fadein {

        from {

            bottom: 0;

            opacity: 0;

        }

        to {

            bottom: -492px;

            opacity: 1;

        }

    }



    @media screen and (max-width: 650px) {

        .over-img {

            top: 450px;

        }

    }



</style>

<!-- Loader -->

<body>

<div class="loader"></div>

<div class="super_container">

    <!-- Header -->

    @include('pages.include.topheader')



    <div class="home" style="height:20vh;">

    <!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->



        <div class="home_content">

            <div class="home_title">Results</div>

        </div>

    </div>



    <div id="myNav" class="overlay" style="height: 0%; top: 100%;">



        <!-- Button to close the overlay navigation -->

        <a href="javascript:void(0)" class="closebtn closediv" onclick="closediv()">×</a>



        <!-- Overlay content -->

        <div class="overlay-content">

            <img src="https://travoweb.com/assets/images/congratulations.png"

                 style="height: 95px; margin-bottom: 23px;">

           <!--  <p>Your Package Are Booking</p> -->

        </div>

        <div class="over-img">

            <img src="https://travoweb.com/assets/images/hand2.png" style="height: 335px">

        </div>



    </div>





    <!--intro-->

    <div class="container">

        <div class="row">

            <div class="col-sm-6"></div>

            <div class="col-sm-6 detail">

                <span>Booking Id:<b>{{$booklistdetails->pckg_bookid}}</b></span>

                <p>Date:<i><?php echo  date("d/m/Y", strtotime($booklistdetails->pckgdate)); ?></i></p>



            </div>

        </div>

        <hr>

        <div class="row">

           {{-- <div class="col-md-12">

                <button id="print_btn" class="btn btn-success pull-right">Print</button>

                <a href="https://travoweb.com/hotel_pdf/1498553" class="btn btn-success pull-right"

                   target="_blank">PDF</a>

            </div>--}}

            <div class="col-md-12 confirm">

                <h2><img src="https://travoweb.com/assets/images/icon-tick-28.jpg">Booking Confirmed</h2>

                <div class="alert alert-success text-center">

                    <h4><i class="fa fa-user"></i> Dear {{ucfirst($booklistdetails->firstname)}} {{ucfirst($booklistdetails->lastname)}}. Thank you for choosing Our Package</h4>

                </div>





            </div>

        </div>



        <div class="row">
            <?php $pckgimageval= unserialize($tour_package->packimage) ;
            ?>
            <div class="col-md-5 hotel-img">
                <img src="{{asset('assets/uploads/packageimage')}}/{{$pckgimageval[0]}}"  width="325" height="183">
                <!-- <img src="https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA59yZzciIrL6I+FbPxfSrw1mj7k5ylOA5DNwHf9gR9WHkWzzph5v4Qfhb+Hy+Hi0GE8Jd1a8q1g8ozlF2v4vyOPA=="

                     width="325" height="183"> -->

            </div>

            <div class="col-md-7 guest-info">

                <a href="#" class="hotel-name">Package Name :{{ucfirst($booklistdetails->pckg_name)}} </a>

                <div class="hotel-info" style="padding-top: 10px;border-bottom-color: orange">

             

                     <div class="row">

                        <div class="col-md-6 pkg-info">

                            <h5>Package ID</h5>

                        </div>

                        <div class="col-md-6 pkg-info">

                            <h5> {{$booklistdetails->pckg_id}} </h5>

                        </div>

                        <div class="col-md-6 pkg-info">

                            <h5>Days</h5>

                        </div>

                        <div class="col-md-6 pkg-info">

                            <h5> {{$tour_package->packagedays}} Days / {{$tour_package->packagenight}}  Nights</h5>

                        </div>

                        <div class="col-md-6 pkg-info">

                            <h5>Price</h5>

                        </div>

                        <div class="col-md-6 pkg-info">

                            <h5> <i class="fa fa-{{strtolower($booklistdetails->currency)}}"></i> {{$booklistdetails->pckg_price}} </h5>

                        </div>

                    </div>



                </div>

                <div class="row">

                    <div class="col-12 guest-info">

                        <p>Guest Name</p>

                        <div class="hotel-info" style="padding-top: 10px;border-bottom-color: orange">



                            <div class="row">

                                <div class="col-md-6">

                                    <h5>Name</h5>

                                </div>

                                <div class="col-md-6">

                                    <h5> {{ucfirst($booklistdetails->firstname)}} {{ucfirst($booklistdetails->lastname)}} </h5>

                                </div>

                            </div>

                             <div class="row">

                                <div class="col-md-6">

                                    <h5>Mobile no </h5>

                                </div>

                                <div class="col-md-6">

                                    <h5> {{$booklistdetails->mobile}} </h5>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-6">

                                    <h5>Email</h5>

                                </div>

                                <div class="col-md-6">

                                    <h5> {{$booklistdetails->email}} </h5>

                                </div>

                            </div>

                        </div>



                        <br>

                    </div>

                </div>



            </div>



        </div>
    <hr>
    <div class="row">

        <div class="col-sm-6 hotel-id">

            <span><img src="https://cdn0.iconfinder.com/data/icons/building-vol-9/512/3-512.png" class="hotel-icon"> Package Booking Details:</span>

            <span>Adults : {{$booklistdetails->adult}}</span>
            <span>Child : {{$booklistdetails->child}}</span>

        </div>

        <div class="col-sm-6 total-amount text-right">



            <span><img src="https://cdn.xl.thumbs.canstockphoto.com/invoice-bill-icon-eps-vector_csp47302143.jpg"

                       class="hotel-icon">Total Amount:</span>

            <span> {{$booklistdetails->currency}} {{$booklistdetails->pckg_price}}</span>
            <span><img src="https://cdn.xl.thumbs.canstockphoto.com/invoice-bill-icon-eps-vector_csp47302143.jpg"

                       class="hotel-icon">Offer Amount:</span>

            <span> {{$booklistdetails->currency}} {{$totaloffer}}</span>

        </div>

    </div>

    <hr>




    


    <div class="row">

        <div class="col-12 room-info">

            <h1>Package Detail</h1>

            <table class="table">



                <tbody>

               <!--  <tr class="t-row">

                    <th class="table-head">Number of Guests</th>

                    <td>

                        <table class="table table-borderless">

                            <tbody>

                            <tr>

                                <th class="room-detail"> Room 1:</th>

                                <td> Adults <span class="badge badge-pill badge-success mr-2">1</span> , Children <span

                                            class="badge badge-pill badge-info mr-2">0</span></td>

                            </tr>

                            </tbody>

                        </table>



                    </td>



                </tr> -->

                <tr class="t-row">

                    <th class="table-head">Inclusions</th>

                    <td>
                        <?php echo $tour_package->Inclusionsdescription;?>

                        <!-- <i class="fa fa-check-square-o mr-2 "></i>Free BREAKFAST

                        <br>

                        <i class="fa fa-check-square-o mr-2"></i>Accommodation -->

                    </td>



                </tr>
                <tr class="t-row">

                    <th class="table-head">Exclusion</th>

                    <td>
                        <?php echo $tour_package->highlitedescription;?>
<!-- 
                        <i class="fa fa-check-square-o mr-2 "></i>Free BREAKFAST

                        <br>

                        <i class="fa fa-check-square-o mr-2"></i>Accommodation -->

                    </td>



                </tr>


                <tr class="t-row">

                    <th class="table-head">Cancellation Policy</th>



                    <td>

                        <i class="fa fa-dot-circle-o"></i> <?php echo $tour_package->cancelpolicy;?>          <!-- <i class="fa fa-caret-right"></i> FREE Cancellation until Dec 24, 2018 12:00 hours.<br><br>





              <i class="fa fa-caret-right"></i> Non-Refundable if cancelled after Dec 24, 2018 12:00 hours.<br><br> -->





                        <!--  <i class="fa fa-caret-right"></i>  goCash used in the booking will be non refundable.<br><br>





                         <i class="fa fa-caret-right"></i> Any Add On charges are non-refundable.<br><br>

                     -->



                        <!-- <i class="fa fa-caret-right"></i> You can not change the check-in or check-out date after Dec 24, 2018 12:00 hours.<br><br> -->

                    </td>

                </tr>

                <!-- <tr class="t-row">

                    <th class="table-head">Package Policy</th>

                   

                    <td><i class="fa fa-caret-right"></i> Infant 1 year(s) : Stay for free if using existing bedding.

                        Note, if you need a cot there may be an extra charge.Children 2 - 8 year(s) : Stay for free if

                        using existing bedding.Early check out will attract full cancellation charge unless otherwise

                        specified.<br><br></td>



                </tr> -->

                </tbody>

            </table>

        </div>

    </div>

    <div class="alert alert-danger text-center">



        ***NOTE***: Any increase in the price due to taxes will be borne by you and payable at the hotel.

    </div>

    <hr>

    <div class="row">

        <div class="col-12 need-help">

            <h3>NEED HELP??</h3>

            <p>For any questions related to the property, you can contact Hotel Fairway directly at: <a href="#">Phone

                    No: 0091 0183 5006363

                    Fax : 91-0183-5006363</a></p>

            <br>

            <br>

            <div class="or">OR</div>

            <p>If you would like to change or upgrade your reservation, visit <a href="#" class="hotel-email">

                    travoweb.com</a></p>

        </div>

    </div>

    <div class="pdf-footer">

        <hr style="width: 80%;height:0;border: none;border-bottom: 1px solid #5e5e5e; margin: auto; background: black !important;">

        <div class="row">



            <div class="col-md-12"

                 style="width:100%; margin:0 auto;font-size: 13px;text-align: center;color: black !important;">



                <img src="https://travoweb.com/assets/images/logo.png" style="width: 100px;margin-top: 10px"

                     class="img-fluid d-block mx-auto">



                <p style="margin: 5px!important; color: black;"><b><i class="fa fa-phone"></i> Ph</b> +61 490149833</p>

                <p style="margin: 5px!important; color: black;"><b><i class="fa fa-angle-double-right"></i> GST No.</b>

                    03BDHPK8214M1ZG</p>

                <p style="margin: 5px!important; color: black;"><b><i class="fa fa-envelope"></i> Email</b> <a href="#">info@travoweb.com</a>,

                    <span style="color: black"><b><i class="fa fa-globe"></i> Website:</b>Travoweb.com</span>

                </p>





            </div>

        </div>

    </div>

    <hr style="width: 100%;height:0;border: none;border-bottom: 1px solid #5e5e5e; margin:10px auto; background: #494949 !important;">

</div>

</div>

</body>

<!-- Footer -->

@include('pages.include.footer')

<!-- Copyright -->@include('pages.include.copyright')

<script>

    $(document).ready(function () {



        $("#myNav").css({"height": "100%", "top": "0%"});



    })

    $(document).on('click', '.closediv', function () {

        $('#myNav').css({"height": "0%", "top": "100%"});

    })

</script>