@include('pages.include.header')
<body>
	<div class="super_container">
		<!-- Header -->
		@include('pages.include.topheader')
		<div class="home">
			<div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg') }}"></div>
			<div class="home_content">
				<div class="home_title">Refund Policy</div>
			</div>
		</div>
		<!-- Intro -->
		<div class="intro">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="intro_content nano-content">
							<div class="intro_title" style="margin-top:50px">Refund Policy</div>
							<h3>BOOKINGS THROUGH OUR WEBSITE</h3>
							<p>By utilizing this Site, you speak to that you are of lawful age to go into and make restricting lawful commitments for any risk that may come about because of your utilization of this Site.</p>

							<p>This Site is offered to you, the client, adapted upon your acknowledgment, without change, of the agreement. Your utilization of this Site constitutes your acknowledgment of the agreement. Without notice to you and in Travoweb's sole carefulness, we maintain whatever authority is needed to change this agreement, in any way and whenever. Your proceeded with utilization of the Site, or buy of products as well as administrations following adjustments to the Site or this Understanding, will be regarded acknowledgment of such changes. This Site is given exclusively to help clients in collecting the information regarding travel, deciding the accessibility of travel-related products and enterprises, reserving authentic spot for you or for someone else for whom you are lawfully approved to act, or generally executing business with travel providers, and for no other reason. Should you utilize this site for the advantage of another (to whom you are legitimately approved to act), it is your obligation to impart the terms and states of this Consent to that individual, before reserving a spot for their benefit. You represent that all data provided by you on this Site is valid, exact, current and complete. Modification, adjustment, sub-permit, decipher, offer, figure out, decompile or dismantle any part of the Site or programming is illegal. You may just utilize this Site for reservations or buys and might not utilize this Site for some other purposes, including without constraint, to influence any to theoretical, false or fake reservation or any reservation in expectation of interest.</p>

							<h3>LINKS TO EXTERNAL WEBSITES</h3>

							<p>This Website may contain links that direct you outside of this Website. These links are provided for your convenience and are not an express or implied indication that we endorse or approve of the linked Website, its contents or any associated website, product or service. We accept no liability for loss or damage arising out of or in connection to your use of these sites.</p>

							<p>You may link to our articles or home page. However, you should not provide a link which suggests any association, approval or endorsement on our part in respect to your website, unless we have expressly agreed in writing. We may withdraw our consent to you linking to our site at any time by notice to you.</p>

							<h3>INFORMATION COLLECTION</h3>

							<p>Use of information you have provided us with, or that we have collected and retained relating to your use of the Website and/or our Services, is governed by our Privacy Policy. By using this Website and the Services associated with this Website, you are agreeing to the Privacy Policy. To view our Privacy Policy and read more about why we collect personal information from you and how we use that information, click here.</p>

							<h3>PRICES</h3>

							<p>All prices are that are available on our website are subject to availability and can be withdrawn or varied without notice. The price is only guaranteed once paid for in full by you. Please note that prices quoted by or travel agent are subject to change. Price changes may occur by reason of matters outside our control which increase the cost of the product or service. Such factors include adverse currency fluctuations, fuel surcharges, taxes and airfare increases. Please contact relevant airlines or hotels for up-to-date prices or quotations.</p>

							<h3>PAYMENTS AND REFUNDS</h3>

							<p>Once you book your tickets online, you will be sent an email confirmation with the details of your travel as per the information provided. For any changes of the bookings you will have to contact the facility provider directly. Travoweb Travels does not take any liability of the changes or any corrections.</p>

							<p>We require full payment for the reservations to hold the booking. If payment is not received, then bookings will automatically be cancelled. Payment made by the customer will be in between Travoweb Travels and the customer only.</p>

							<p>If you cancel your booking you may incur a cancellation charge. The amount of this charge will vary depending on when the cancellation is made and the terms and conditions of the supplier of travel services or product. If you are entitled to a refund please note Travoweb Travels is unable to provide you with funds until they are received from the supplier/airlines.</p>

							<h3>RESERVATIONS MODIFICATION CHARGES</h3>

							<p>Any modification after the bookings and payment will be charge minimum price of $100 per service per person including the charge levied by the supplier.</p>

							<h3>FEES AND TAXES</h3>

							<p>Travoweb Travels does not charge service fees for all the bookings. While booking your tickets or services, total amount will be inclusive of our service fee. If you still want to continue with the booking, the total amount which is displayed shall be charged from your method of payment. Travoweb Travels may at its discretion charge service fees on your booking.</p>

							<h3>PAYMENT BY CREDIT CREDIT</h3>

							<p>- card surcharges may apply to payments made by credit card. In the event you pay by credit card but the services are not provided by a third party provider of travel services or products you agree that you will not take steps to charge back your payment to Travoweb Travels.</p>

							<h3>PASSWORDS AND LOGINS</h3>

							<p>You are responsible for maintaining the confidentiality of your passwords and login details and for all activities carried out under your password and login.</p>

							<h3>CONFIDENTIALITY</h3>

							<p>All personal information you give us will be dealt with in a confidential manner in accordance with our Privacy Policy. However, due to circumstances outside of our control, we cannot guarantee that all aspects of your use of this Website will be confidential due to the potential ability of third parties to intercept and access such information.</p>

							<h3>ACKNOWLEDGEMENT</h3>

							<p>You acknowledge that you are 18 years of age or older and that you understand and agree with the above Booking and our Privacy Policy.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Footer -->
		@include('pages.include.footer')
		<!-- Copyright -->
		@include('pages.include.copyright')
	</div>
</body>
</html>