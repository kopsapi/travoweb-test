@include('pages.include.header')<!-- Loader --><style> .loader {
    display: block;
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: #CCEAF7 url('{{ asset('assets/images/flight-loader.gif')}}') no-repeat center center;
    text-align: center;
    color: #999;
    opacity: 0.9;
}

ul.header-nav .current a,
ul.header-nav .active:hover,
ul.header-nav .active:focus {
    background-color: transparent !important;
    border: none;
    color: #005285 !important;
}

ul.header-nav a {
    border-bottom: none !important;
}

ul.header-nav a.active {
    border-bottom: 2px solid #fa9e1b !important;
    border-top: none !important;
    border-right: none !important;
    border-left: none !important;
}

ul.header-nav {
    list-style-type: none;
    margin: 0;
    padding: 0px;
    overflow: hidden;
    background-color: #ffffff;
    border-top-left-radius: 6px;
    border-top-right-radius: 6px;
    box-shadow: 0 2px 20px 0 rgba(0, 0, 0, .1);
}

.header-nav li {
    float: left;
    position: relative;
}

.header-nav li a {
    display: block;
    color: #005285;
    font-weight: bold;
    font-size: 18px;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

body {
    background-color: #eeeeee;
}

.content {
    padding: 10px;
    background-color: white;
    margin-bottom: 20px;
}

.content.info {
    border-radius: 8px;
}

.desc {
    padding: 43px 20px;
}

.desc h3 {
    color: black;
}

.desc a {
    text-decoration: none;
    color: white;
    background: linear-gradient(to right, #ff8d68, #ffbc3b);
    padding: 15px 20px;
    width: 100px;
    border-radius: 30px;
}

@media screen and (max-width: 600px) {
    .col-sm-3 img {
        text-align: center;
    }
    img.img-fluid.img-rounded.p-5 {
        text-align: center;
        display: block;
        margin: auto;
        padding: auto;
    }
}

@media screen and (max-width: 575px) {
    .desc {
        text-align: center;
    }
}

@media screen and (max-width: 783px) {
    .header-nav li {
        float: none;
    }
}

h3 .fa-star {
    padding: 2px;
    color: rgba(255, 179, 2, 1);
}

.search-btn {
    padding: 6.5px 15px;
    color: white;
    font-size: 17px;
    background-color: #fa9e1b;
    border: none;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
}

.search-control {
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-image: none;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}

</style><style> label {
    color: white;
}

form.s-input input,
form.s-input select,
form.s-input textarea {
    border: none;
    border-bottom: 1px solid #ffffff;
    background-color: transparent;
    border-radius: 0;
    outline: none;
    color: #ffffff !important;
}

form.s-input input::placeholder {
    color: #ffffff !important;
}

select {
    width: 150px;
    height: 30px;
    padding: 5px;
    color: #ffffff !important;
}

select option {
    color: black !important;
}

select option:first-child {
    color: #ffffff !important;
}

form.s-input input:focus,
form.s-input select:focus,
form.s-input textarea:focus {
    background-color: transparent !important;
    outline: none;
    box-shadow: none!important;
}

.white-text {
    color: #ffffff !important;
}

.close1 {
    font-weight: bolder;
    font-size: 30px;
    margin-top: -4px;
    margin-right: -15px;
}

.p-modal {
    padding: 37px;
}

h3#myModalLabel strong {
    color: #fa9e1b;
}

.c-btn1 {
    padding: 10px 15px;
    border: none;
    color: white;
    background: #fa9e1b;
    width: 100%;
    border-radius: 5px;
}

</style><!-- Loader --><body>
    <div class="loader"></div>
    <div class="super_container">
        <!-- Header -->@include('pages.include.topheader')
        <div class="home" style="height:20vh;">
            <!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->
            <div class="home_content">
                <div class="home_title">Results</div>
            </div>
        </div>
        <!-- Intro -->
        <!-- <div class="container my-4"> <div class="input-group mb-3 s-id"> <input type="text" class="form-control" placeholder="Search"> <div class="input-group-append"> <button class="cng-btn" type="submit">Submit</button> </div> </div> </div> -->
        
            <div class="container-fluid">
                <div class="row">
                    <h3 class="h-cancel">Cancellation Detail</h3>
                    <hr class="c-detail"> </div>
                    @if($hotelcanceldetail->c_adminstatus=='0')
                    
                    <div class="row">
                    <h5 class="h-cancel">Refund amount will be transferred to your account within 7-15 working days</h5>
                    <hr class="c-detail"> </div>
                    @endif
            </div>
            <div class="container mb-4">
                <div class="h-table">
                    <table id="customer">
                        <tr>
                            <th>Request Id</th>
                            
                            <th>Refund Amount</th>
                            <th>Date</th>
                            <th>Status</th>
                        </tr>
                        <?php
                        if($hotelcanceldetail->c_adminstatus=='0')
                        {
                           $status =  "Pending";
                        }
                        else
                        {
                            $status = "Success";
                        }
                        ?>

                        <tr>
                            <td style="text-align: center">{{$hotelcanceldetail->change_request_id}}

                            </td>
                            <td style="text-align: center"> {{$hotelcanceldetail->c_refund_amount}}

                            </td>
                            <td style="text-align: center"><?php echo  $getcheckindate=date('d-m-Y',strtotime($hotelcanceldetail->c_create_date)); ?> 

                            </td>
                            <td style="text-align: center"> {{$status}}

                            </td>
                        </tr>
                    </table>
                </div>
            </div>
 <style>
  .h-table {
    padding: 100px;
}

.s-id {
    margin-left: auto;
    margin-right: auto;
    width: 60%;
}

.cng-btn {
    text-decoration: none;
    background-color: #fa9e1b;
    color: white;
    border: none;
    outline: none;
    padding: 15px 20px;
    border-bottom-right-radius: 5px;
    border-top-right-radius: 5px;
}

#customer {
    border-collapse: collapse;
    padding: 31px !important;
    width: 85%;
    margin-left: auto;
    margin-right: auto;
    /* margin-top: 80px; */
    /* margin-bottom: 80px; */
}

#customer td,
#customer th {
    border: 1px solid #ddd;
    padding: 14px 8px;
    color: black;
}

table#customer tr th:first-child {
    border-top-left-radius: 10px;
}

table#customer tr th:last-child {
    border-top-right-radius: 10px;
}

#customer tr:nth-child(even) {
    background-color: #f2f2f2;
}

#customer tr:hover {
    background-color: #ddd;
}

#customer th {
    padding: 14px 8px;
    font-size: 17px;
    text-align: left;
    background-color: #2d60c5;
    color: white;
}

h3.h-cancel {
    padding: 25px;
    /* margin: 0px; */
    margin-top: -5px;
    width: 100%;
    text-align: center;
    color: white;
    font-size: 40px;
    background: linear-gradient(to right, #002c, #8d4fff);
}
h5.h-cancel {
    padding: 25px;
    /* margin: 0px; */
    margin-top: -5px;
    width: 100%;
    text-align: center;
    color: #7547d2;
    font-size: 20px;
    
}
hr.c-detail {
    width: 28%;
    margin-left: auto;
    margin-right: auto;
    border-bottom: 2px solid #f8f9fa;
    margin-top: -31px;
}

#customer th {
    padding: 14px 8px;
    font-size: 17px;
    text-align: left;
    background-color: #1f0733;
    color: white;
}

</style> </div> </div> </div> <!-- Footer -->@include('pages.include.footer') <!-- Copyright -->@include('pages.include.copyright') <script> $(document).ready(function() {
    $(".loader").fadeOut("slow");
}

);
</script></body></html>