<?php
$chdata = session()->get('postData');
$customerName = $chdata['customerName'];
$orderAmount=$chdata['orderAmount'];
$customerPhone = $chdata['customerPhone'];
$customerEmail =  $chdata['customerEmail'];
$orderId=$chdata['orderId'];
$orderCurrency=$chdata['orderCurrency'];
if($orderCurrency=='INR')
{
  $icon = 'fa-inr';
}
else
{
  $icon = 'fa-usd';
}
$params = array("testmode" => "on",
    "private_live_key" => "sk_live_2QLNRClFjDAFUR56avW3gvun",
    "public_live_key" => "pk_live_LptMMIsCPx0fqef2pi3tlAXk",
    "private_test_key" => "sk_test_yICHd12qJq7YEret7hAxqTWj",
    "public_test_key" => "pk_test_gdcMtVAnKmNzBAuQpSqVsOtj");
?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Stripe Pay Custom Integration </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .payment-card {
            background: white;
            padding: 20px 45px 30px;
        }
        img.pay-img {
            width: 100%;
            height: auto;
            min-height: 100%;
            object-fit: cover;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
        }
        .payment-section {
            margin-top: 4%;
            border: 1px solid #bebebe;
            margin-left: 130px;
            margin-right: 130px;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
            box-shadow: 0 10px 25px 0 rgba(0, 0, 0, 0.4);
        }
        div.bt_title {
            color: black;
            text-align: center;
        }
        .pay-input{
            border-radius: 0;
           margin-bottom: 15px;
        }

        .pay-input:focus{
            border: none;
            border: 1px solid #01729e;
            border-radius: 0;
            box-shadow: none;
        }
        label.pay-label {
            color: #01729e;
            font-weight: 500;
        }
        input.submit.pay-btn {
             border: none;
             border: 1px solid;
             background: #01729e;
             color: white;
             padding: 10px 20px;
             display: block;
             margin: 20px auto 0;
         }

        span.span-1 {
            text-align: left;
            display: block;
            padding: 10px 20px;
            color: #ffc107;
            font-weight: 500;
        }
        span.span-2 {
            text-align: right;
            display: block;
            padding: 10px;
            color: #ffc107;
            font-weight: 500;
        }

        .icon-container {
            margin-bottom: 20px;
            padding: 7px 0;
            font-size: 24px;
        }
        .icon-container i{
            font-size: 30px;
        }
        h3.bt_title {
            text-align: center;
            margin-bottom: 30px;
            color: #fd5e60;
        }
    </style>
</head>
<body>
<link href="style.css" type="text/css" rel="stylesheet"/>
<div class="container">
    <div class="payment-section">
    <div class="row">
        <div class="col-4" style="padding-right: 0">
                       <img src="{{asset('assets/images/pay-img.jpg')}}"  class="pay-img">
        </div>
        <div class="col-8" style="padding-left: 0;">
          <div class="row bt_title">
              <div class="col-6" style="padding-right: 0">
                  <span class="span-1">Order No:{{$orderId}}</span>
              </div>

              <div class="col-6" style="padding-left: 0">
                  <span class="span-2">Price: <i class="fa {{$icon}}"></i>{{$orderAmount}}</span>
              </div>

          </div>
            <hr style="width: 100%;margin: 0">
    <div class="payment-card">
        <div class="dropin-page">
            <form action="" method="POST" id="payment-form">

                <h3 class="bt_title">Stripe Pay </h3>
                <span class="payment-errors" style="color:red"></span>
                <div class="form-row row">
                    <div class="col-12">
                        <label class="pay-label"><span><i class="fa fa-credit-card-alt" style="color: #000000;"></i> Card Holder</span></label>
                        <span class="text-danger err-msg password textfield_error textfield"  id="name_error" style="display: none;font-size: 11px"> </span>
                        <input type="text" size="20" id="name" name="name" class="form-control pay-input textfield" placeholder="Card Holder Name">
                        

                    </div>
                </div>
                <div class="form-row row">
                    <div class="col-12">
                    <label class="pay-label"><span><i class="fa fa-credit-card-alt" style="color: #000000;"></i> Card Number</span></label>
                    <span class="text-danger err-msg password textfield_error textfield"  id="card_error" style="display: none;font-size: 11px"> </span>
                        <input type="text" size="20" id="card" data-stripe="number" class="form-control pay-input textfield leadage" placeholder="Enter Your Card No">

                    </div>
                </div>
                <div class="form-row row">
                    <div class="col-sm-6" >
                        <div class="row">
                             <div class="col-sm-6">
                                 <label class="pay-label"><span><i class="fa fa-ban" style="color: #000000;"></i> Expiration</span></label>
                                 <span class="text-danger err-msg password textfield_error textfield"  id="expmonth_error" style="display: none;font-size: 11px"> </span>
                                 <input type="text" size="2" id="expmonth" data-stripe="exp_month" class="form-control pay-input textfield leadage" placeholder="Month" maxlength="2">
                             </div>
                            <div class="col-sm-6">
                                <label class="pay-label"><span>&nbsp;</span> </label>
                                <span class="text-danger err-msg password textfield_error textfield"  id="expyear_error" style="display: none;font-size: 11px"> </span>
                                <input type="text" size="2" id="expyear" data-stripe="exp_year" class="form-control pay-input textfield leadage" placeholder="Year" maxlength="2">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="pay-label"><span><i class="fa fa-credit-card" style="color: #000000;"></i> CVC</span></label>
                        <span class="text-danger err-msg password textfield_error textfield"  id="cvc_error" style="display: none;font-size: 11px"> </span>
                        <input type="text" size="4" id="cvc" data-stripe="cvc" class="form-control pay-input textfield leadage" placeholder="Enter Your CVC" maxlength="4">

                    </div>

                </div>

               
                <input type="submit" class="submit pay-btn" value="Make Payment">
            </form>
        </div>
    </div>
        </div>
    </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<!-- TO DO : Place below JS code in js file and include that JS file -->
<script>
    $(document).on('blur', '.textfield', function()
    {
        var id=this.id;
        var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if($('#'+id).val().trim()!="")
        {
            $('#'+id+'_error').text('');
            $('#'+id+'_error').hide();
        }
        if(id=="name" && $('#'+id).val().trim()=="")
        {
            
            $('#'+id+'_error').text('Please Enter Card Holder Name');
            $('#'+id+'_error').show();
        }
        if(id=="card" && $('#'+id).val().trim()=="")
        {
            $('#'+id+'_error').text('Please Enter Card Number');
            $('#'+id+'_error').show();
        }
        if(id=="expmonth" && $('#'+id).val().trim()=="")
        {
            $('#'+id+'_error').text('Enter Month');
            $('#'+id+'_error').show();
        }
        if(id=="expyear" && $('#'+id).val().trim()=="")
        {
            $('#'+id+'_error').text('Enter Year');
            $('#'+id+'_error').show();
        }
        if(id=="cvc" && $('#'+id).val().trim()=="")
        {
            $('#'+id+'_error').text('Enter CVC');
            $('#'+id+'_error').show();
        }
    });
</script>

<script>

    Stripe.setPublishableKey('<?php echo $params['public_test_key']; ?>');
    $(function () {
        var $form = $('#payment-form');
        $form.submit(function (event) {
        $('.textfield_error').text('');
        $('.textfield_error').hide();
        var name=$('#name').val();
        var card=$('#card').val();
        var expmonth=$('#expmonth').val();
        var expyear=$('#expyear').val();
        var cvv=$('#cvv').val();
        if(name=="")
        {
            $('#name_error').text("Please Enter Name");
            $('#name_error').show();
        }
        else if(card=="")
        {
            $('#card_error').text("Please Enter Card Number");
            $('#card_error').show();
        }
        else if(expmonth=="")
        {
            $('#expmonth_error').text("Enter exp month ");
            $('#expmonth_error').show();
        }
        else if(expyear=="")
        {
            $('#expyear_error').text("Enter exp year ");
            $('#expyear_error').show();
        }
        else if(cvv=="")
        {
            $('#cvv_error').text("Enter CVV Number ");
            $('#cvv_error').show();
        }
        else
        {
            $form.find('.submit').prop('disabled', true);
            // Request a token from Stripe:
            Stripe.card.createToken($form, stripeResponseHandler);
            // Prevent the form from being submitted:
            
        }
        return false;
    
        });
    });
    function stripeResponseHandler(status, response) {
        // Grab the form:

    var $form = $('#payment-form');
    
    if (response.error) {
        // Problem!
    // Show the errors on the form:
    $form.find('.payment-errors').text(response.error.message);
    $form.find('.submit').prop('disabled', false);
    // Re-enable submission
    } else {
    // Token was created!
    // Get the token ID:
    var token = response.id;
    // Insert the token ID into the form so it gets submitted to the server:
    $form.append($('<input type="hidden" name="stripeToken">').val(token));
    // Submit the form:
    $form.get(0).submit();
    }
    };

                </script>
                <script>
  $(document).ready(function () {
  
  $(".leadage").keypress(function (e) {
    
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       return false;
    }
   });
});

</script>
</body>
</html>