@include('pages.include.header')
<style>
	@import url("https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/blitzer/jquery-ui.min.css");

	.ui-datepicker td span,
	.ui-datepicker td a {
		padding-bottom: 1em;
	}


	.ui-datepicker td[title]::after {
		content: attr(title);
		display: block;
		position: relative;
		font-size: .8em;
		height: 1.25em;
		margin-top: -1.25em;
		text-align: right;
		padding-right: .25em;
	}
	#ui-datepicker-div
	{
		z-index:12 !important;
	}
	.table-condensed>thead>tr>th,.table-condensed>thead>tr>td
	{
		padding:5px;
	}
	.dropdown-menu {
		position: absolute;
		top: 100%;
		left: 0;
		z-index: 1000;
		display: none;
		float: left;
		min-width: 160px;
		margin: 2px 0 0;
		font-size: 14px;
		text-align: left;
		list-style: none;
		background-color: #fff;
		-webkit-background-clip: padding-box;
		background-clip: padding-box;
		border: 1px solid #ccc;
		border: 1px solid rgba(0,0,0,.15);
		border-radius: 4px;
		-webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
		box-shadow: 0 6px 12px rgba(0,0,0,.175);
	}
	.dropdown-menu {
		box-shadow: none;
		border-color: #eee;
	}
	.search_panel
	{
		padding-top:50px;
		padding-bottom:50px;
	}
	.search_panel_content {
		flex-wrap: wrap;
	}
	.extras {
		width: 100%;
		margin-bottom:10px;
	}
	.search_extras_item {
		width: 50%;
		float: left;
		margin-bottom: 10px;
	}
	.search_extras_item div {
		display: inline-block;
		cursor: pointer;
	}
	.search_extras_cb {
		display: block;
		position: relative;
		width: 15px;
		height: 15px;
		-webkit-appearance: none;
		-moz-appearance: none;
		-ms-appearance: none;
		-o-appearance: none;
		appearance: none;
		background-color: #FFFFFF;
		border: 1px solid #FFFFFF;
		padding: 9px;
		margin-top: 4px;
		border-radius: 50%;
		display: inline-block;
		position: relative;
		cursor: pointer;
		float: left;
	}
	.search_extras_cb:checked::after {
		display: block;
		position: absolute;
		top: 2px;
		left: 2px;
		border-radius:50%;
		width: calc(100% - 4px);
		height: calc(100% - 4px);
		content: '';
		background: #fa9e1b;
	}
	.search_extras label {
		display: block;
		position: relative;
		font-size: 15px;
		font-weight: 400;
		padding-left: 25px;
		margin-bottom: 0px;
		cursor: pointer;
		color: #FFFFFF;
	}
	@media only screen and (max-width: 1730px)
	{
		.search_extras_item {
			width: 20%;
		}
	}
	@media only screen and (max-width: 1730px)
	{
		.search_panel {
			display: none !important;
			width: 100%;
			height: 100%;
			-webkit-animation: fadeEffect 1s;
			animation: fadeEffect 1s;
			margin-top: 0px;
		}
	}
</style>
<style>
	.dropbtn {
		background-color: #3498DB;
		color: white;
		padding: 16px;
		font-size: 16px;
		border: none;
		cursor: pointer;
	}

	.dropbtn:hover, .dropbtn:focus {
		background-color: #2980B9;
	}

	.dropdown {
		position: relative;
		display: inline-block;
	}

	.dropdown-content {
		display: none;
		position: absolute;
		background-color: #f1f1f1;
		min-width: 160px;
		overflow: auto;
		box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
		z-index: 1;
	}

	.dropdown-content a {
		color: black;
		padding: 12px 16px;
		text-decoration: none;
		display: block;
	}
	.exchange-icon {
		font-size: 17px;
		color: #fa9e1b;
		padding-top: 0px!important;
		margin-top: -13px!important;
	}
	a.a-done-btn {
		text-decoration: none;
		padding: 10px;
		background: #fa9e1b;
		color: white;
		margin: 20px auto auto auto;
		width: 120px;
		display: block;
		text-align: center;
		border-radius: 20px;
	}
	.a-done-btn {
		text-decoration: none;
		padding: 10px;
		background: #fa9e1b;
		color: white;
		margin: 20px auto auto auto;
		width: 120px;
		display: block;
		text-align: center;
		border-radius: 20px;
		cursor: pointer;
	}
	.dropdown a:hover {background-color: #ddd;}

	.show {display: block;}
</style>
<style>
	.mbsc-mobiscroll .mbsc-stepper-cont
	{
		padding: 1.75em 12.875em 1.75em 1em;
	}
	.mbsc-control-w
	{
		max-width: none;
		margin: 0;
		font-size: 1em;
		font-weight: normal;
	}
	.mbsc-checkbox, .mbsc-switch, .mbsc-btn, .mbsc-radio, .mbsc-segmented, .mbsc-stepper-cont
	{
		position: relative;
		display: block;
		margin: 0;
		z-index: 0;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}
	.mbsc-desc
	{
		display: block;
		font-size: .75em;
		opacity: .6;
	}
	.mbsc-stepper
	{
		position: absolute;
		display: block;
		width: auto;
		right: 1em;
		top: 50%;
	}
	.mbsc-form .mbsc-stepper-val-left .mbsc-stepper input
	{
		left: 0;
	}
	.mbsc-mobiscroll .mbsc-stepper input
	{
		color: #454545;
	}
	.mbsc-stepper input
	{
		position: absolute;
		left: 4.142857em;
		width: 4.142857em;
		height: 100%;
		padding: 0;
		margin: 0;
		border: 0;
		outline: 0;
		box-shadow: none;
		font-size: .875em;
		text-align: center;
		opacity: 1;
		z-index: 4;
		background: transparent;
		-webkit-appearance: none;
		-moz-appearance: textfield;
		appearance: none;
	}
	.mbsc-stepper .mbsc-segmented-item
	{
		width: 3.625em;
	}
	.mbsc-segmented .mbsc-segmented-item
	{
		margin: 0;
		display: table-cell;
		position: relative;
		vertical-align: top;
		text-align: center;
		font-size: 1em;
	}
	.mbsc-stepper-cont.mbsc-stepper-val-left .mbsc-stepper .mbsc-segmented-item:nth-child(2) .mbsc-segmented-content, .mbsc-stepper-cont.mbsc-stepper-val-right .mbsc-stepper .mbsc-segmented-item:last-child .mbsc-segmented-content
	{
		border: 0;
		background: transparent;
	}
	.mbsc-mobiscroll .mbsc-segmented-content
	{
		border: 0.142857em solid #4eccc4;
		color: #4eccc4;
		height: 2.28571428em;
		margin: 0 -.071428em;
		line-height: 2.28575em;
		padding: 0 .285714em;
		text-transform: uppercase;
	}
	.mbsc-segmented-content
	{
		position: relative;
		display: block;
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
		font-size: .875em;
		font-weight: normal;
		z-index: 2;
	}
	.mbsc-segmented .mbsc-segmented-item
	{
		margin: 0;
		display: table-cell;
		position: relative;
		vertical-align: top;
		text-align: center;
		font-size: 1em;
	}
	.mbsc-mobiscroll .mbsc-segmented input:disabled ~ .mbsc-segmented-item .mbsc-segmented-content, .mbsc-mobiscroll .mbsc-segmented .mbsc-segmented-item.mbsc-stepper-control.mbsc-disabled .mbsc-segmented-content, .mbsc-mobiscroll .mbsc-segmented .mbsc-segmented-item input:disabled+.mbsc-segmented-content
	{
		color: #d6d6d6;
		border-color: #d6d6d6;
	}
	.mbsc-mobiscroll .mbsc-segmented input:disabled ~ .mbsc-segmented-item .mbsc-segmented-content, .mbsc-mobiscroll .mbsc-segmented .mbsc-segmented-item.mbsc-stepper-control.mbsc-disabled .mbsc-segmented-content, .mbsc-mobiscroll .mbsc-segmented .mbsc-segmented-item input:disabled+.mbsc-segmented-content
	{
		background: transparent;
	}
	.mbsc-mobiscroll .mbsc-segmented-content
	{
		border: 0.142857em solid #4eccc4;
		color: #4eccc4;
		height: 2.28571428em;
		margin: 0 -.071428em;
		line-height: 2.28575em;
		padding: 0 .285714em;
		text-transform: uppercase;
	}
	.mbsc-mobiscroll .mbsc-segmented-content
	{
		border: 0.142857em solid #4eccc4;
		color: #4eccc4;
	}
	.mbsc-mobiscroll .mbsc-segmented-content
	{
		height: 2.28571428em;
		margin: 0 -.071428em;
		line-height: 2.28575em;
		padding: 0 .285714em;
		text-transform: uppercase;
	}
	.mbsc-disabled .mbsc-segmented-content, .mbsc-segmented input:disabled, .mbsc-segmented input:disabled ~ .mbsc-segmented-item .mbsc-segmented-content
	{
		cursor: not-allowed;
	}
	.mbsc-segmented-item .mbsc-control, .mbsc-stepper .mbsc-segmented-content
	{
		cursor:pointer;
	}
	.mbsc-segmented input:disabled ~ .mbsc-segmented-item .mbsc-segmented-content, .mbsc-disabled .mbsc-segmented-content, .mbsc-segmented input:disabled+.mbsc-segmented-content
	{
		z-index: 0;
	}
	.traveller-dropdown .dropdown-menu hr
	{
		margin-top:7px !important;
		margin-bottom:7px !important;
	}
	.traveller-dropdown .dropdown-menu
	{
		min-width: 300px !important;
		margin-bottom: 0px;
		background: #FFF4e7;
		border: 1px solid #CCC;
		margin-top:10px;
	}
	.traveller-dropdown .dropdown-menu .row
	{
		margin-bottom:0px;
	}
	.traveller-dropdown .dropdown-menu .col-md-12
	{
		margin-bottom:0px;
	}
	.traveller-dropdown .dropdown-menu .trave-drop
	{
		padding-left:15px;
		padding-right:15px;
		margin-bottom:0px;
	}
	.traveller-dropdown .dropdown-menu .trave-drop .travel-left
	{
		width:60%;
		float:left;
		color: #00206A;
		font-weight: 600;
		margin-bottom: 0px;
		font-size: 16px;
		line-height: 36px;
	}
	.traveller-dropdown .dropdown-menu .trave-drop .plus-minus .minus
	{
		color:#fa9e1b;
		width:35%;
		float:left;
		font-size:24px;
		margin-bottom: 0px;
		text-align:center;
	}
	.traveller-dropdown .dropdown-menu .trave-drop .plus-minus .text
	{
		color: #00206A;
		width:30%;
		float:left;
		font-size:17px;
		line-height: 37px;
		font-weight: 600;
		margin-bottom: 0px;
		text-align:center;
	}
	.traveller-dropdown .dropdown-menu .trave-drop .plus-minus .plus
	{
		color:#fa9e1b;
		width:35%;
		float:left;
		font-size:24px;
		margin-bottom: 0px;
		text-align:center;
	}
	.traveller-dropdown .dropdown-menu .trave-drop .child-age label
	{
		color: #333;
		margin-bottom: 3px;
		font-weight:600;
	}
	.traveller-dropdown .dropdown-menu .trave-drop .child-age select
	{
		border: 1px solid #CCC;
		padding-left: 10px;
	}
	.traveller-dropdown .dropdown-menu .trave-drop .child-age:nth-child(1), .traveller-dropdown .dropdown-menu .trave-drop .child-age:nth-child(4), .traveller-dropdown .dropdown-menu .trave-drop .child-age:nth-child(7), .traveller-dropdown .dropdown-menu .trave-drop .child-age:nth-child(10)
	{
		margin-left:0px;
	}
	.traveller-dropdown .dropdown-menu .trave-drop .child-age
	{
		width:30%;
		float:left;
		margin-top: 10px;
		margin-left:13px;
	}
	.traveller-dropdown .dropdown-menu .trave-drop .plus-minus
	{
		width:40%;
		float:left;
		margin-bottom: 0px;
	}
	.t-err{
		color: red;
		visibility: hidden;
	}
	@media screen and (max-width: 768px){
		.btn.btn-success.g-details{
			margin-bottom: 10px;
		}


	}
	@media only screen and (max-width: 992px) {
		.hotel-result-list .hotel-result-price {
			padding: 24px 5px;
		}
	}
	@media only screen and (max-width: 320px) {
		.b-res{
			padding: 5px 6px !important;
		}
	}

.filter.hotel-card-filter .inner-filter {
    max-height: 335px !important;
    overflow: auto;
}

.filter.hotel-card-filter .inner-filter::-webkit-scrollbar {
  width: 10px;
}

/* Track */
.filter.hotel-card-filter .inner-filter::-webkit-scrollbar-track {
  background: #FFF4e7; 
}
 
/* Handle */
.filter.hotel-card-filter .inner-filter::-webkit-scrollbar-thumb {
	margin-top: 37px;
	padding-top: 37px !important;
  background: #fa9e1b; 
}

/* Handle on hover */
.filter.hotel-card-filter .inner-filter::-webkit-scrollbar-thumb:hover {
  background: #ff8d00; 
}
.filter.hotel-card-filter .inner-filter div label
{
	font-size: 13px;
}
.filter.hotel-card-filter .inner-filter div label .checkcontainer .checkmark:after
{
	    left: 4px;
    top: 1px;
    width: 6px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
.checkcontainer input:checked ~ .checkmark {
    background-color: #fa9e1b !important;
    border: 1px solid #f29410 !important;
}
.checkcontainer .checkmark:after
{
	    left: 4px;
    top: 1px;
    width: 6px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
.filter.hotel-card-filter .inner-filter div label .checkmark
{
	position: absolute;
    top: 3px;
    left: 0;
    height: 15px;
    width: 15px;
    background-color: #FFF;
    border: 1px solid #CCC;
    border-radius: 3px;
}
.checkmark
{
	position: absolute;
    top: 3px;
    left: 0;
    height: 15px;
    width: 15px;
    background-color: #FFF;
    border: 1px solid #CCC;
    border-radius: 3px;
}
	span.m-close {
		position: absolute;
		font-size: 27px;
		font-weight: 700;
		top: 0;
		color: white;
		right: 19px;
		cursor: pointer;
	}
    .price i.fa-usd {

        font-size: 25px !important;
        margin-bottom: 2px;

	}
	button.btn.btn-primary.filter-btn {
    background: orange;
    border: none;
    position: sticky;
    bottom: 80px;
    width: auto;
    height: auto;
    z-index: content: '\f095';
    width: auto;
    display: none;
	margin: 0 0 0 auto;
	transition: all .5s ease;
    width: auto;
    border-radius: 50px;
}
.filter-res-div {
	width: 100%;
	transition: all .5s ease;
    height: auto;
    background:none;
    z-index: 20;
	padding: 10px;
	right: 10px;
    margin: 0 !important;
}
.desktop-filter-btn {
    margin-right: 114px !important;
    width: 256px !important;
    border-radius: 5px !important;
}
@media screen and (max-width:992px){
	button.btn.btn-primary.filter-btn {
   
	display: block;
}

}
.modal .flight-filter h3 {
    color: #ffffff;
    background: orange;
    padding: 5px 10px;
    border-radius: 5px;
    font-size: 16px;
    margin-bottom: 20px;
}
.filter h4 {
    background: #18206a;
    color: white;
    padding: 5px 10px;
}
.filter h4 i {
    color: orange;
}
.scroll-div::-webkit-scrollbar {
    width: 10px;
}
.scroll-div::-webkit-scrollbar-thumb {
    margin-top: 37px;
    padding-top: 37px !important;
    background: #fa9e1b;
}
.scroll-div::-webkit-scrollbar-track {
    background: #FFF4e7;
}
.done-btn {
    margin: auto;
    display: block;
    border-radius: 50px;
    background: #18206a;
    width: 140px;
    border: none;
}
.loc {
    font-family: calibri;
    font-size: 12px;
}
.hotel-title {
    font-family: calibri;
	margin-bottom: 0;
}
.rating-div i {
    color: #fa9e1b;
}
h4.price {
    font-family: calibri;
}
.hotel-result-list .hotel-result-price .price i {
    color: #fa9e1b;
    font-size: 24px;
}
.hotel-result-list .hotel-result-price {
    text-align: right;
    height: 100%;
    border-left: 2px solid #CCC;
    background: #FFF4e7;
    padding: 30px 15px;
}
.g-details {
    margin-bottom: 10px;
}
.hotel-result-list {
    border-radius: 10px;
}
.flight-filter ,.hotel-result-details .flight-filter h3,.hotel-result-details .flight-filter .filter h4{
    font-family: calibri;
    /* font-size: 19px !important; */
}
.checkcontainer {
    display: block;
    position: relative;
    padding-left: 30px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 17px;
    font-weight: 600;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    color: #00206A;
}
label.checkcontainer i {
    font-size: 14px;
}
.inner-filter {
    z-index: 0;
}
.home {
    height: 130px;
}

@media screen and (max-width:600px){
	.home {
    height: 60px;
}
.hotel-result-price {
    padding: 20px 5px !important;
}
}


</style>
<body>

	<div class="modal" id="myModal">
		<div class="modal-dialog">
		  <div class="modal-content" style="margin-top: 5%;">
	  
			<!-- Modal Header -->
			<div class="modal-header">
			  <h4 class="modal-title" style="    color: orange;">Filters</h4>
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
	  
			<!-- Modal body -->
			<div class="modal-body scroll-div" style="height: 470px;
			overflow-y: auto;">
			  <div class="filter-res"></div>
			  <button type="button" class="btn btn-danger done-btn" data-dismiss="modal">Done</button>
			</div>
	  
			<!-- Modal footer -->
			
	  
		  </div>
		</div>
	  </div>
    <div class="super_container">
        <!-- Header -->@include('pages.include.topheader')
        <div class="home">
            <!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->
            <div class="home_content">
                <div class="home_title">Results </div>
            </div>
        </div>
        <?php
        // echo "<pre>";
        // print_r($hotelarray);
        // echo "</pre>";

        ?>
        <!-- Intro -->
		<div class="hotel-result">
			<div class="intro">
				<section class="hotel-search-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="intro_content">
									<div class="row">
										<div class="col-lg-3 res-mb-20">
										<h4><i class="fa fa-map-marker"></i>{{$submitted_data['City']}}-{{$submitted_data['State']}}, {{$submitted_data['Country']}}</h4> </div>
										<div class="col-lg-2 res-mb-20">
						<h4><i class="fa fa-calendar"></i> <?php echo date('d M Y',strtotime($submitted_data['checkin']));
														$start_date=date('d/m/Y',strtotime($submitted_data['checkin'])); ?> </h4> </div>
										<div class="col-lg-1 res-mb-20">
											<h4><i class="fa fa-bed bed"></i></h4> </div>
										<div class="col-lg-2 res-mb-20">
											<h4><i class="fa fa-calendar"></i> <?php echo date('d M Y',strtotime($submitted_data['checkout']));
											$end_date=date('d/m/Y',strtotime($submitted_data['checkout'])); ?> </h4> </div>
										<div class="col-lg-2 res-mb-20">
											<h4 class="flight-class"><small>ADULTS</small> {{$submitted_data['adult_count']}} <small>CHILD</small> {{$submitted_data['child_count']}} </h4> </div>
										<div class="col-lg-2">
											<button type="button" class="btn btn-modify">Modify</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<div class="hotel-result-modify">
					<!-- Search Contents -->
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="search">
									<div class="fill_height" id="mod-modal">
										<div class="search_panel active">
											<span class="m-close" onclick="close1()">&times;</span>
											<form action="{{ route('gethotels') }}" method="post" id="search_form_2" class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start hotels">
												<div class="search_item">
													{{ csrf_field() }}
													<div>Going to</div>
													<input list="cities1" type="text" name="destination" class="destination search_input" placeholder="City / Hotel" autocomplete="off" id="search-keyword" value="{{$submitted_data['City']}} ({{$submitted_data['Country']}})">
													<i class="fa fa-map-marker fa-icon"></i>
													<datalist id="cities1">
													</datalist>
												</div>
												<div class="search_item">
													
													<div>check in</div>
													<input type="text" name="check_in" class="check_in search_input" placeholder="DD/MM/YYYY" autocomplete="off" id="check-in" value="{{$start_date}}" readonly="readonly">
													<i class="fa fa-calendar fa-icon"></i>
												</div>
												<div class="search_item">
													
													<div>check out</div>
													<input type="text" name="check_out" class="check_out search_input" placeholder="DD/MM/YYYY" autocomplete="off" id="check-out" value="{{$end_date}}" readonly="readonly">
													<i class="fa fa-calendar fa-icon"></i>
												</div>


												<div class="search_item">

														<div>Rooms</div>
														<div class=" traveller-dropdown" style="margin-bottom:0px;">
															<div class="searchhotel" style="margin-bottom:0px;">
																<input type="text" class="search_input totalrooms "  name="flight_nocheck" value="{{$submitted_data['no_of_rooms']}}  Rooms" autocomplete="off">
																<i class="fa fa-user fa-icon"></i>
															</div>
															<div class="dropdown-menu dropshowhotel" >
																<div class="row">
																	<div class="col-md-12">
																		<div class="trave-drop">
																			<div class="travel-left">Rooms</div>
																			<div class="plus-minus">
																				<div class="minus roomminus">
																					<i class="fa fa-minus-circle"></i>
																				</div>
																				<input type="hidden" name="newroom" class="roomval" value="{{$submitted_data['no_of_rooms']}}" maxlength="9" />
																				<div class="text roomcount">{{$submitted_data['no_of_rooms']}}</div>
																				<div class="plus roomplus">
																					<i class="fa fa-plus-circle"></i>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<hr>
																<?php
																// echo "<pre>";
																// print_r($submitted_data['childagearray']);
																// echo "</pre>";
																?>
																<div class="row">
																	<div class="col-md-12 newroom" >
																		<?php
																		for($it=0;$it<$submitted_data['no_of_rooms'];$it++)
																		{
																		?>
																		<div class="roomcheckcount-{{$it+1}}">
																			<div class="row">
																				<div class="col-md-12" >

																					<div class="trave-drop">
																						<div class="travel-left">Room {{$it+1}}</div>
																						<div class="travel-left">
																							<small>Adults (12 + years)</small>
																						</div>
																						<div class="plus-minus ">
																							<div class="minus roomadultminus" id="{{$it+1}}" >
																								<i class="fa fa-minus-circle"></i>
																							</div>
																							<input type="hidden" name="roomadult-{{$it+1}}" id="adultval" class="roomadultval-{{$it+1}}" value="{{$submitted_data['adult_array'][$it]}}"  />

																							<div class="text roomadultcount-{{$it+1}}">{{$submitted_data['adult_array'][$it]}}</div>
																							<div class="plus roomadultplus" id="{{$it+1}}">
																								<i class="fa fa-plus-circle"></i>
																							</div>
																						</div>
																					</div>
																					<div class="trave-drop">
																						<div class="travel-left">
																							<small>Child (<12 years)</small>
																						</div>
																						<div class="plus-minus ">
																							<div class="minus roomchildminus" id="{{$it+1}}" >
																								<i class="fa fa-minus-circle"></i>
																							</div>
																							<input type="hidden" name="children-{{$it+1}}" id="roomchildval" class="roomchildval-{{$it+1}} chkchild" value="{{$submitted_data['child_array'][$it]}}"  />

																							<div class="text roomchildcount-{{$it+1}}">{{$submitted_data['child_array'][$it]}}</div>
																							<div class="plus roomchildplus" id="{{$it+1}}">
																								<i class="fa fa-plus-circle"></i>
																							</div>
																						</div>
																					</div>
																					<?php
																					if($submitted_data['child_array'][$it]==0)
																					{
																					?>
																					<div class="trave-drop showchilddiv-1" style="display: none">
	
																					</div>
																				<?php } else{ ?>
																				<div class="trave-drop showchilddiv-{{$it+1}}">
																					<?php
																					for($ct=0;$ct<$submitted_data['child_array'][$it];$ct++)
																					{
																						
																					?>
																					<div class="trave-drop showchilddiv-{{$ct+1}}">
																				    <div class="child-age childage-{{$ct+1}}">
																				        <label>Child {{$ct+1}} age</label>
																				       
																				        <select class="dropdown_item_select search_input agenew" name="childage-{{$it+1}}[]">

																							<option value="{{$submitted_data['childagearray'][$it][$ct]}}">{{$submitted_data['childagearray'][$it][$ct]}}</option>
																				            <option value="1">1</option>
																				            <option value="2">2</option>
																				            <option value="3">3</option>
																				            <option value="4">4</option>
																				            <option value="5">5</option>
																				            <option value="6">6</option>
																				            <option value="7">7</option>
																				            <option value="8">8</option>
																				            <option value="9">9</option>
																				            <option value="10">10</option>
																				            <option value="11">11</option>
																				            <option value="12">12</option>
																				            <select>
																				    </div>
																				</div>
																				<?php 
																				} 
																				//forloopclose
																				 } ?>

																				</div>
																			</div>

																		</div>
																	<?php } ?>
																	</div>

																	<hr>

																</div>
																<!-- close row div -->

																<div class="row">

																</div>
																<hr>


																<div class="container">
																	<div class="row">
																		<div class="col-md-12">
																			<!-- <a href="#" class="a-done-btn">Done</a> -->
																			<span class="a-done-btn" id="hoteldone">Done</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>

												</div>


												<div class="text-center">
													<button class="button search_button" id="search_hotel_btn">search<span></span><span></span><span></span></button>
												</div>
											</form>
										</div>
										<!-- Search Panel -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div></div>
			<?php
			// echo "<pre>";
			// print_r($hotelarray);
			// echo "</pre>";
			?>
			@if(!empty($hotelarray))
				@if($hotelarray['Error']['ErrorCode']==0)
				<section class="hotel-result-details">
					<div class="filter-res-div">
						<button type="button" class="btn btn-primary filter-btn" data-toggle="modal" data-target="#myModal">
							<i class="fa fa-filter"></i>
								<span id="span-filter">Filter</span>
						</button>
					</div>
				
					<div class="container">
						<div class="row">
							<div class="col-lg-9 hoteldata">
								<?php
								
								for($ht=0;$ht<count($hotelarray['HotelResults']);$ht++)
								{
								?>
								<form action="{{route('hotelinfo')}}" method="post">
									<div class="hotel-result-list">
										<div class="row">
											<div class="col-md-3">
												<div class="hotel-img"> <img src="{{$hotelarray['HotelResults'][$ht]['HotelPicture']}}" class="img-responsive">
												</div>

											</div>
											<div class="col-md-6">

												<div class="hotel-result-content">
													<h4 class="hotel-title">{{$hotelarray['HotelResults'][$ht]['HotelName']}}
														

													</h4>
													<div class="rating-div">
														<?php
														for($star=1;$star<=$hotelarray['HotelResults'][$ht]['StarRating'];$star++)
														{
															echo '<i class="fa fa-star"></i>&nbsp;';
														}
														?>
													</div>
													<small class="loc">{{$hotelarray['HotelResults'][$ht]['HotelAddress']}}</small>
													<br><br>

													{{csrf_field()}}
													<?php
													session(['hotelchkin'=>$start_date,'hotelchkout'=>$end_date]);
													?>
													<input type="hidden" name="hotelchkin" value="{{$start_date}}">
													<input type="hidden" name="hotelchkout" value="{{$end_date}}">
													<input type="hidden" name="hotelindex" value="{{$hotelarray['HotelResults'][$ht]['ResultIndex']}}">
													<input type="hidden" name="hotelcode" value="{{$hotelarray['HotelResults'][$ht]['HotelCode']}}">
													<input type="hidden" name="hotelname" value="{{$hotelarray['HotelResults'][$ht]['HotelName']}}">
													<button type="submit" class="btn btn-success g-details">Get Details</button>

													<!-- <h6 class="nearby">1.2 km from Golden Temple</h6> -->
													<!-- <div class="search-filter"> <a href="javascript:void()">Premium</a> <a href="javascript:void()">Couple Friendly</a> </div>
                                                    <ul class="hotel-services">
                                                        <li><i class="fa fa-wifi"></i>&nbsp; Free Wi-fi</li>
                                                        <li><i class="fa fa-cutlery"></i>&nbsp; Kitchen</li>
                                                        <li><i class="fa fa-sun-o"></i>&nbsp; Sun Deck</li>
                                                        <li><i class="fa fa-coffee"></i>&nbsp; Restaurant</li>
                                                    </ul>
                                                    <div class="more-options">
                                                        <p><span>More Options:</span> Breakfast Included</p>
                                                    </div> -->
												</div>
											</div>
											<div class="col-md-3">
												<div class="hotel-result-price">
													<!-- <h4 class="strike-price"> -->
													<!-- <i class="fa fa-inr"></i>  -->
												<!-- <strike>{{$hotelarray['HotelResults'][0]['Price']['CurrencyCode']}} {{number_format($hotelarray['HotelResults'][$ht]['Price']['PublishedPriceRoundedOff'])}}</strike> -->
													<!-- </h4> -->
													<h4 class="price">
														<i class="fa {{$hotelarray['HotelResults'][$ht]['hotelmaincurrecny']}}"></i>
														<!-- {{$hotelarray['HotelResults'][0]['Price']['CurrencyCode']}} {{number_format($hotelarray['HotelResults'][$ht]['Price']['PublishedPriceRoundedOff'])}}  -->
														 {{number_format($hotelarray['HotelResults'][$ht]['hotelmainprice'])}}</h4> <small>per night</small>
													<br>
													<button type="submit" class="btn btn-success b-res" style="margin-top: 8px;">Book</button>
												</div>

											</div>
										</div>
									</div>
								</form>
							<?php
							} ?>
							<!-- 	<div class="hotel-result-list">
									<div class="row">
										<div class="col-md-3">
											<div class="hotel-img"> <img src="{{ asset('assets/images/hotel-img.jpg')}}" class="img-responsive"> </div>
										</div>
										<div class="col-md-6">
											<div class="hotel-result-content">
												<h4 class="hotel-title">Hotel Ramada Amritsar <i class="fa fa-star"></i>&nbsp;<i class="fa fa-star"></i>&nbsp;<i class="fa fa-star"></i>&nbsp;<i class="fa fa-star"></i>&nbsp;<i class="fa fa-star"></i>&nbsp;</h4> <small class="loc">Hall Bazar</small>
												<h6 class="nearby">1.2 km from Golden Temple</h6>
												<div class="search-filter"> <a href="javascript:void()">Premium</a> <a href="javascript:void()">Couple Friendly</a> </div>
												<ul class="hotel-services">
													<li><i class="fa fa-wifi"></i>&nbsp; Free Wi-fi</li>
													<li><i class="fa fa-cutlery"></i>&nbsp; Kitchen</li>
													<li><i class="fa fa-sun-o"></i>&nbsp; Sun Deck</li>
													<li><i class="fa fa-coffee"></i>&nbsp; Restaurant</li>
												</ul>
												<div class="more-options">
													<p><span>More Options:</span> Breakfast Included</p>
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="hotel-result-price">
												<h4 class="strike-price"><i class="fa fa-inr"></i> <strike>5,722</strike></h4>
												<h4 class="price"><i class="fa fa-inr"></i> 4,628</h4> <small>per night</small> </div>
										</div>
									</div>
								</div> -->
							</div>
							<div class="col-lg-3">
								<div class="flight-filter">
									<h3>Sort & Filter</h3>
									<div class="filter">
										<h4><i class="fa fa-inr"></i>&nbsp; Price Per Night</h4>
										<div class="inner-filter">
											<!-- <div class="range-slider">
												<input type="text" class="js-range-slider" value="" /> </div> -->
												<?php
												$price=array();
												$currency="";
												 for($ht=0;$ht<count($hotelarray['HotelResults']);$ht++)
								 				{
								 					
								 					$price[]=$hotelarray['HotelResults'][$ht]['hotelmainprice'];
								 					$currency = $hotelarray['HotelResults'][$ht]['hotelmaincurrecny'];
								 				}
								 				$minprice=min($price);
								 				$maxprice=max($price);
								 				$commonvalue=round(($maxprice-$minprice)/3);
								 				$first=$minprice;
								 				$final=0;
								 				for($price_count=0;$price_count<3;$price_count++)
								 				{
								 					$final=$first+$commonvalue;
								 					
								 					
												?>
												
													
												 @if($price_count==0)
													<label class="checkcontainer"><i class="fa {{$currency}}"></i> {{round($first)}} - <i class="fa {{$currency}}"></i>  {{round($final)}}
													@else
														<label class="checkcontainer"><i class="fa {{$currency}}"></i> {{round($first+1)}} - <i class="fa {{$currency}}"></i> {{round($final)}}
													@endif
													 @if($price_count==0)
													<input type="checkbox" name="priceable" id="priceable{{$price_count}}" value="{{($first)}}-{{round($final)}}">
													@else
													<input type="checkbox" name="priceable" id="priceable{{$price_count}}" value="{{($first+1)}}-{{round($final)}}">
													@endif
												
                                                 
												
												<span class="checkmark"></span>
												</label>
												

												<?php $first=$final; } ?>
										</div>
									</div>
								<!-- 	<div class="filter">
										<h4><i class="fa fa-building"></i>&nbsp; Popular Filters</h4>
										<div class="inner-filter">
											<label class="checkcontainer">Pay @ Hotel Available
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Couple Friendly
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Free Breakfast
												<input type="checkbox"> <span class="checkmark"></span> </label>
										</div>
									</div> -->
								<!-- 	<div class="filter">
										<h4><i class="fa fa-map-signs"></i>&nbsp; Trip Type</h4>
										<div class="inner-filter">
											<label class="checkcontainer">Business
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Family
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Romantic
												<input type="checkbox"> <span class="checkmark"></span> </label>
										</div>
									</div> -->
									<!-- <div class="filter">
										<h4><i class="fa fa-map-marker"></i>&nbsp; Locality</h4>
										<div class="inner-filter">
											<label class="checkcontainer">Near Golden Temple
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Katra Ahluwalia
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Near Jallianwala Bagh
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Hussainpura
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Hall Bazar
												<input type="checkbox"> <span class="checkmark"></span> </label>
										</div>
									</div> -->
									<div class="filter">
										<h4><i class="fa fa-star"></i>&nbsp; Star Category</h4>
										<div class="inner-filter">
											<label class="checkcontainer">5 Star
												<input type="checkbox" name="filter_stars" value="5"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">4 Star
												<input type="checkbox" name="filter_stars" value="4"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">3 Star
												<input type="checkbox" name="filter_stars" value="3"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">2 Star
												<input type="checkbox" name="filter_stars" value="2"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">1 Star
												<input type="checkbox" name="filter_stars" value="1"> <span class="checkmark"></span> </label>
											<!-- <label class="checkcontainer">Unrated
												<input type="checkbox" name=""> <span class="checkmark"></span> </label> -->
										</div>
									</div>
									<div class="filter hotel-card-filter">
										<h4 style="margin-bottom: 0"><i class="fa fa-building"></i>&nbsp; Hotels Name</h4>
										<div class="inner-filter">
											
											 <div class="input-group mb-3 mt-3">
												  <input type="text" id="hsearch" class="form-control" placeholder="Search" style="height: 30px;padding-left: 10px">
												    <div class="input-group-append">
												      <button class="btn btn-success search-h-btn" type="submit" style="height: 30px;padding-top: 2.1px;">Go</button>  
												     </div>
												  </div>
											<div class="showsearch" style="display: none"> </div>
											<div class="oldsearch">
											<?php
											
											for($ht=0;$ht<count($hotelarray['HotelResults']);$ht++)
											{
											?>
											<label class="checkcontainer">{{$hotelarray['HotelResults'][$ht]['HotelName']}}
												<input type="checkbox" name="hotelname" value="{{$hotelarray['HotelResults'][$ht]['HotelName']}}"> <span class="checkmark"></span> </label>
											<?php } ?>
										</div>
											
										</div>
									</div>
									<!-- <div class="filter">
										<h4><i class="fa fa-star"></i>&nbsp; User Rating</h4>
										<div class="inner-filter">
											<label class="checkcontainer">4.5 & above (Excellent)
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">4 & above (Very Good)
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">3 & Above (Good)
												<input type="checkbox"> <span class="checkmark"></span> </label>
										</div>
									</div> -->
									<!-- <div class="filter">
										<h4><i class="fa fa-building"></i>&nbsp; Property Type</h4>
										<div class="inner-filter">
											<label class="checkcontainer">Apartment
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">BnB
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Bungalow
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Farm House
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Guest House
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Homestay
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Homestay
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Hotel
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Lodge
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Villa
												<input type="checkbox"> <span class="checkmark"></span> </label>
										</div>
									</div> -->
								<!-- 	<div class="filter">
										<h4><i class="fa fa-cutlery"></i>&nbsp; Facility</h4>
										<div class="inner-filter">
											<label class="checkcontainer">Free Wi-Fi
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Kitchen
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Luggage Storage
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Parking
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Rastaurant/Bar
												<input type="checkbox"> <span class="checkmark"></span> </label>
											<label class="checkcontainer">Swimming Pool
												<input type="checkbox"> <span class="checkmark"></span> </label>
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
				</section>
				@else
				<br>
				<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="intro_content nano-content">
							<div class="no-flight">
								<h3>No Hotels Found</h3>
								<a href="{{url('/index')}}" class="btn btn-back">Go Back</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif
			@else
			<br>
				<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="intro_content nano-content">
							<div class="no-flight">
								<h3>No Hotels Found</h3>
								<a href="{{url('/index')}}" class="btn btn-back">Go Back</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif
			</div>
        </div>
    </div>
	<div class='session-counter'>
		<p class='timer' data-minutes-left=15>Your booking session will expire in </p>
		<section class='actions'></section>
	</div>
	<style>
		.session-counter {
			background: #000000d9;
			color: white;
			padding: 5px;
			position: fixed;
			bottom: 0;
			display: block;
			width: 100%;
			z-index: 1;
		}

		.session-counter p{
			margin: 0;
			color: white;
			font-size: 17px;
			font-weight: 500;
			text-align: center;
			z-index: 1;
		}
		.jst-hours,.jst-minutes,.jst-seconds {
			display: inline;
			color: orange;
		}

	</style>
    <!-- Footer -->@include('pages.include.footer')
    <!-- Copyright -->@include('pages.include.copyright')
    <script>
	$(function () {
    //Departure
    var date = new Date();
    date.setDate(date.getDate());
    // $('.departure').datepicker({
    // 	autoclose: true,
    // 	todayHighlight: true,
    // 	format: 'dd/mm/yyyy',
    // 	startDate: date

    // })
    // //Date picker
    // $('#return').datepicker({
    // 	autoclose: true,
    // 	todayHighlight: true,
    // 	format: 'dd/mm/yyyy',
    // 	startDate: date
    // })
    //Check In
    $('#check-in').datepicker({
    	autoclose: true,
    	todayHighlight: true,
    	format: 'dd/mm/yyyy',
    	startDate: date
    })
    //Date picker
    $('#check-out').datepicker({
    	autoclose: true,
    	todayHighlight: true,
    	format: 'dd/mm/yyyy',
    	startDate: date
    })

})
</script>
    <script>
        $(document).ready(function() {
            $(".btn-modify").click(function() {
                $(".hotel-result-modify").toggle();
            });
        });
    </script>
<script>
	$(document).on('keyup','#search-keyword',function()
	{
		var keyword=$(this).val().trim();
		if(keyword!='')
		{
			$.ajax({
				url : '{{route("getcities")}}',
				type: 'GET',
				data: {'citykeyword' :keyword},
				success: function(response)
				{
					$('#cities1').html('');
    		// console.log(response);
    		$('#cities1').html(response);
    	}
    })
		}
	});

</script>
<script>
	$("#check-in").on("change",function (){ 
   // console.log($(this).val());
   var date = $('#check-in').datepicker('getDate', '+1d');
   date.setDate(date.getDate() + 1);
   $('#check-out').datepicker('setDate',date);
});
</script>
     <script>
    	$(document).on('change',"input[name='filter_stars'],input[name='priceable'],input[name='hotelname']",function()
    	{
    		var data_stars=[];
    		var data_price=[];
    		var data_hotelname=[];
    		$("input:checkbox[name=filter_stars]:checked").each(function()
    		{
    			data_stars.push($(this).val());
    		});

    		$("input:checkbox[name=priceable]:checked").each(function()
    		{
    			data_price.push($(this).val());
    		});
    		$("input:checkbox[name=hotelname]:checked").each(function()
    		{
    			data_hotelname.push($(this).val());
    		});
    		var stars=data_stars.join(",");
    		var price=data_price.join(",");
    		var hotelname=data_hotelname.join(",");
    		
    		 $.ajax({
    		 	url:"{{ route('searchhotels')}}",
    		 	type:"GET",
    		 	data:{'stars':stars,
    		          'price':price,
    		          'hotelname':hotelname,
    		      },
    		 	success:function(response)
    		 	{
    		 		$('.hoteldata').html(response);
    		 	}
    		 })
    		

    	})
    </script>
    <script>
	$(document).on('submit','#search_form_2',function(e)
	{
		var errors='';
		var city=$("#search-keyword").val();
		var indate=$("#check-in").val();
		var outdate=$("#check-out").val();
		var rooms=$("#rooms").val();
		var adults=$("#adults_2").val();
		var children=$("#children_2").val();
		if(city.trim()=='')
		{
			errors+="Going to field cannot be empty ," ;
		}
		if(indate.trim()=='')
		{
			errors+="Check-in date field cannot be empty ," ;
		}
		if(outdate.trim()=='')
		{
			errors+="Check-out date field cannot be empty ," ;
		}
		if(rooms.trim()=='')
		{
			errors+="Please select no. of rooms ," ;
		}
		if(adults.trim()=='')
		{
			errors+="Please select no. of adults ," ;
		}
		// if(errors=='')
		// {
		// 	console.log(city+'-'+indate+'-'+outdate+'-'+rooms+'-'+adults+'-'+children);
		// }
		if(errors!='')
		{
			e.preventDefault();
			alert(errors);
		}
	})
</script>

	<script>
		$(document).on('click','#flightdone',function(){
			$('.dropshowflight').hide();
		});
	</script>
	<!-- hotel dropsearch -->
	<script>
		$(document).on('click','.searchhotel',function(){

			$('.dropshowhotel').toggle();
		});
		$(document).on('click','.roomplus',function(){
			var rooms = $('.roomval').val();
			if(rooms < 9)
			{
				var rooms1=parseInt(rooms)+1;
				$('.roomval').val(rooms1);
				$('.roomcount').text(rooms1);
				$('.newroom').append('<div class="roomcheckcount-'+rooms1+'"><div class="row"><div class="col-md-12" ><div class="trave-drop"><div class="travel-left">Room '+rooms1+'</div><div class="travel-left"><small>Adults (12 + years)</small></div><div class="plus-minus "><div class="minus roomadultminus" id="'+rooms1+'"><i class="fa fa-minus-circle"></i></div><input type="hidden" name="roomadult-'+rooms1+'" id="adultval" class="roomadultval-'+rooms1+'" value="1"  /><div class="text roomadultcount-'+rooms1+'">1</div><div class="plus roomadultplus" id="'+rooms1+'"><i class="fa fa-plus-circle"></i></div></div></div><div class="trave-drop"><div class="travel-left"><small>Child (<12 years)</small></div><div class="plus-minus "><div class="minus roomchildminus"  id="'+rooms1+'" ><i class="fa fa-minus-circle"></i></div><input type="hidden" name="children-'+rooms1+'" id="roomchildval" class="roomchildval-'+rooms1+' chkchild" value="0"  /><div class="text roomchildcount-'+rooms1+'">0</div><div class="plus roomchildplus" id="'+rooms1+'"><i class="fa fa-plus-circle"></i></div></div></div><div class="trave-drop showchilddiv-'+rooms1+'" style="display: none"></div></div></div>');
				$('.totalrooms').val(rooms1 + " Rooms");
			}
			else
			{
				$('.roomval').val(rooms);
				$('.roomcount').text(rooms);
				$('.totalrooms').val(rooms+ " Rooms");

			}
		});
		//roome minus
		$(document).on('click','.roomminus',function(){
			var rooms = $('.roomval').val();

			if(rooms!=1)
			{
				var rooms1=parseInt(rooms)-1;
				$('.roomval').val(rooms1);
				$('.roomcount').text(rooms1);
				$('.roomcheckcount-'+rooms).remove();
				$('.totalrooms').val(rooms1 + " Rooms");

			}
			else
			{
				$('.roomval').val(1);
				$('.roomcount').text(1);
				$('.totalrooms').val( "1 Rooms");

			}
		});
		//roomadult
		$(document).on('click','.roomadultplus',function(){
			var newid = this.id;
			var roomadult = $('.roomadultval-'+newid).val();

			if(roomadult < 3)
			{
				var roomadult1=parseInt(roomadult)+1;
				$('.roomadultval-'+newid).val(roomadult1);
				$('.roomadultcount-'+newid).text(roomadult1);


			}
			else
			{
				$('.roomadultval-'+newid).val(roomadult);
				$('.roomadultcount-'+newid).text(roomadult);

			}
		});
		$(document).on('click','.roomadultminus',function(){
			var newid = this.id;
			var roomadult = $('.roomadultval-'+newid).val();

			if(roomadult!=1)
			{
				var roomadult1=parseInt(roomadult)-1;
				$('.roomadultval-'+newid).val(roomadult1);
				$('.roomadultcount-'+newid).text(roomadult1);

			}
			else
			{
				$('.roomadultval-'+newid).val(1);
				$('.roomadultcount-'+newid).text(1);

			}
		});
		//childadult
		$(document).on('click','.roomchildplus',function(){
			var newid = this.id;

			var roomchild = $('.roomchildval-'+newid).val();

			if(roomchild < 2)
			{
				var roomchild1=parseInt(roomchild)+1;
				$('.roomchildval-'+newid).val(roomchild1);
				$('.roomchildcount-'+newid).text(roomchild1);
				$('.showchilddiv-'+newid).show();
				$('.showchilddiv-'+newid).append('<div class="child-age childage-'+roomchild1+'"><label>Child '+roomchild1+' age</label><select class="dropdown_item_select search_input agenew" name="childage-'+newid+'[]"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><select></div>');
			}
			else
			{
				$('.roomchildval-'+newid).val(roomchild);
				$('.roomchildcount-'+newid).text(roomchild);

			}
		});
		$(document).on('click','.roomchildminus',function(){
			var newid = this.id;

			var roomchild = $('.roomchildval-'+newid).val();

			if(roomchild!=0)
			{
				var roomchild1=parseInt(roomchild)-1;
				$('.roomchildval-'+newid).val(roomchild1);
				$('.roomchildcount-'+newid).text(roomchild1);
				$('.childage-'+roomchild).remove();
			}
			else
			{
				$('.roomchildval-'+newid).val(0);
				$('.roomchildcount-'+newid).text(0);
				$('.childage-'+roomchild).remove();
				$('.showchilddiv-'+newid).hide();

			}
		});
	</script>
	<script>
		$(document).on('click','#hoteldone',function(){
			$('.dropshowhotel').hide();
		});
	</script>
	<script>
		/* When the user clicks on the button,
        toggle between hiding and showing the dropdown content */
		function myFunction() {
			document.getElementById("myDropdown").classList.toggle("show");
		}

		// Close the dropdown if the user clicks outside of it
		window.onclick = function(event) {
			if (!event.target.matches('.dropbtn')) {
				var dropdowns = document.getElementsByClassName("dropdown-content");
				var i;
				for (i = 0; i < dropdowns.length; i++) {
					var openDropdown = dropdowns[i];
					if (openDropdown.classList.contains('show')) {
						openDropdown.classList.remove('show');
					}
				}
			}
		}
	</script>
	<script>
		$(document).on('keyup','#hsearch',function(){
			var search =$(this).val();

			if(search=='')
			{
				$('#oldsearch').show();
				$('.showsearch').hide();
			}
			else
			{


			$.ajax({
					url : "{{route('searchhotelname')}}",
					data : {'search' : search},
					type:"GET",
					success : function(data)
					{
						
						$('.showsearch').html(data);
						$('.showsearch').show();
					},
			
			});
		}
		});
	</script>
<script>
	var count = 0;
	var height = $(".flight-filter").innerHeight() + 100
	$(window).scroll(function (e) {

		var $el = $('.filter-res-div');
		var isPositionFixed = ($el.css('position') == 'fixed');
		if (count == 0) {
			/* if ($(this).scrollTop() > ($(".flight-filter").height() - $(".flight-filter").offset().top) + 100) {
				$(".flight-filter").css({
					'position': 'fixed',
					'top': '80px'
				});
			} */
			if ($(this).scrollTop() > height + $(".flight-filter").offset().top) {
				document.getElementsByClassName("filter-btn")[0].style.display = "block"
				document.getElementsByClassName("filter-btn")[0].classList.add("desktop-filter-btn")
				$el.css({
					'position': 'fixed',
					'top': '80px'
				});

				
				document.getElementsByClassName("filter-btn")[0].removeAttribute("data-toggle")
				document.getElementsByClassName("filter-btn")[0].addEventListener("click", function () {


					window.scrollTo({
						top: 0,
						behavior: 'smooth'
					})

					document.getElementsByClassName("filter-btn")[0].style.display = "none"
				})
			} else if ($(this).scrollTop() < height + $(".flight-filter").offset().top) {
				
				document.getElementsByClassName("filter-btn")[0].setAttribute("data-toggle", "modal")
				document.getElementsByClassName("filter-btn")[0].style.display = "none"
				document.getElementsByClassName("filter-btn")[0].classList.remove("desktop-filter-btn")
				$el.css({
					'position': 'static',
					'top': '80px'
				});


			}
		} else if (count == 1) {
			if ($(this).scrollTop() > 450 && !isPositionFixed) {
				$el.css({
					'position': 'fixed',
					'bottom': '45px'
				});
				document.getElementById("span-filter").innerText=""
			}

			if ($(this).scrollTop() < 450 && isPositionFixed) {
				$el.css({
					'position': 'static',
					'bottom': '45px'
				});
				document.getElementById("span-filter").innerText="Filter"
			}
		}

	});

	function myFunction(x) {
		if (x.matches) { // If media query matches
			count = 1;
			var target = document.getElementsByClassName("filter-res")[0];
			if (target.children.length == 0) {
				var source = document.getElementsByClassName("flight-filter")[0];
				source.cloneNode(true)
				target.append(source)
			}
		}
	}

	var x = window.matchMedia("(max-width: 992px)")
	myFunction(x) // Call listener function at run time
	x.addListener(myFunction)
</script>

</body>

</html>