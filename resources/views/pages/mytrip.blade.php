@include('pages.include.header')
<!-- Loader -->
<style>
    .loader {
        display: block;
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: #CCEAF7 url('{{ asset('assets/images/flight-loader.gif')}}') no-repeat center center;
        text-align: center;
        color: #999;
        opacity: 0.9;
    }
    
    ul.header-nav .current a,
    ul.header-nav .active:hover,
    ul.header-nav .active:focus {
        background-color: transparent !important;
        border: none;
        color: #005285 !important;
    }
    
    ul.header-nav a {
        border-bottom: none !important;
    }
    
    ul.header-nav a.active {
        border-bottom: 2px solid #fa9e1b !important;
        border-top: none !important;
        border-right: none !important;
        border-left: none !important;
    }
    
    ul.header-nav {
        list-style-type: none;
        margin: 0;
        padding: 0px;
        overflow: hidden;
        background-color: #ffffff;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
        box-shadow: 0 2px 20px 0 rgba(0, 0, 0, .1);
    }
    
    .header-nav li {
        float: left;
        position: relative;
    }
    
    .header-nav li a {
        display: block;
        color: #005285;
        font-weight: bold;
        font-size: 18px;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
    }
    
    body {
        background-color: #eeeeee;
    }
    
    .content {
        padding: 10px;
        background-color: white;
        margin-bottom: 20px;
    }
    
    .content.info {
        border-radius: 8px;
    }
    
    .desc {
        padding: 43px 20px;
    }
    
    .desc h3 {
        color: black;
    }
    
    .desc a {
        text-decoration: none;
        color: white;
        background: linear-gradient(to right, #ff8d68, #ffbc3b);
        padding: 15px 20px;
        width: 100px;
        border-radius: 30px;
    }
    
    @media screen and (max-width: 600px) {
        .col-sm-3 img {
            text-align: center;
        }
        img.img-fluid.img-rounded.p-5 {
            text-align: center;
            display: block;
            margin: auto;
            padding: auto;
        }
    }
    
    @media screen and (max-width: 575px) {
        .desc {
            text-align: center;
        }
    }
    
    @media screen and (max-width: 783px) {
        .header-nav li {
            float: none;
        }
    }
    
    h3 .fa-star {
        padding: 2px;
        color: rgba(255, 179, 2, 1);
    }
    
    .search-btn {
        padding: 6.5px 15px;
        color: white;
        font-size: 17px;
        background-color: #fa9e1b;
        border: none;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    
    .search-control {
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-image: none;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }
    .f-logo {
        width: 90px;
    }
    p.t-id-info {
        margin-top: 10px;
    }
    span.close.close-btn {
        color: white;
        font-size: 26px;
        font-weight: 600;
        cursor: pointer;
    }
    .modal-header.h-bg {
        background: #31124b;
        padding: 10px 21px;
        border-bottom-color: white;
    }
    .modal-footer.f-bg {
        background: #e2e3ea;
    }
    p.t-id-info {
        margin-top: 10px;
        color: red !important;
        font-weight: 600;
    }

    .btn1 {
        font-size: 17px;
        font-weight: 500;
        color: #fff;
        text-transform: uppercase;
        background: #fa9e1b;
        border: none;
        outline: none;
        padding: 8px 17px;
        border-radius: 5px;
        cursor: pointer;
    }
</style>
<style>

    label{
        color: white;
    }
    form.s-input input,form.s-input select,form.s-input textarea{
        border: none;
        border-bottom: 1px solid #ffffff;
        background-color: transparent;
        border-radius: 0;
        outline: none;
        color: #ffffff !important;
    }
    form.s-input input::placeholder{
        color: #ffffff !important;
    }
    select{
        width: 150px;
        height: 30px;
        padding: 5px;
        color: #ffffff !important;
    }
    select option { color: black !important; }
    select option:first-child{
        color: black !important;
    }
    form.s-input input:focus,form.s-input select:focus,form.s-input textarea:focus{
        background-color: transparent !important;
        outline: none;
        box-shadow: none!important;
    }
    .white-text{
        color: #ffffff !important;
    }
    .modal-header span.close2 {
        color: white;
        border: none;
        cursor: pointer;
        outline: none;
        font-size: 30px;
        position: absolute;
        top: -30px;
        right: -13px;
        font-weight: bolder;
    }
    .close1 {
        font-weight: bolder;
        font-size: 30px;
        margin-top: -4px;
        margin-right: -15px;
    }
    .p-modal {
        padding: 37px;
    }
    h3#myModalLabel strong{
        color: #fa9e1b;
    }
    .c-btn1 {
        padding: 10px 15px;
        border: none;
        color: white;
        background: #fa9e1b;
        width: 100%;
        border-radius: 5px;
    }
    span.f-help {
        color: black;
        font-size: 16px;
    }
    h3.pt-3.mb-0.h-cancel {
        color: black;
    }
</style>

<!-- Loader -->

<body>
<div class="loader"></div>
<div class="super_container">
    <!-- Header -->@include('pages.include.topheader')
    <div class="home" style="height:20vh;">
    <!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->
        <div class="home_content">
            <div class="home_title">Results</div>
        </div>
    </div>

    <!-- Intro -->

        <div class="container" data-spy="scroll" data-target=".header-nav" data-offset="500">

            <!-- <ul class="header-nav">
                <li><a onclick="f()" href="#upcoming" class="bar-item"><i class="fa fa-shopping-bag" style="padding:6px;   font-size: 17px"></i> UPCOMING</a></li>
                <li><a onclick="f()" href="#cancel" class="bar-item"><i class="	fa fa-close"  style=" font-size: 17px"></i> CANCELLED</a></li>
                <li><a onclick="f()" href="#complete" class="bar-item"><i class="fa fa-check"  style="padding:6px;  font-size: 17px"></i> COMPLETED</a></li>
            </ul> -->

            <ul class="nav nav-tabs header-nav">
                <li class="bar-item"><a data-toggle="tab" class="active" href="#upcoming"><i class="fa fa-shopping-bag" style="padding:6px;   font-size: 17px"></i>  UPCOMING</a></li>
                <li class="bar-item"><a data-toggle="tab" href="#cancelled"><i class="fa fa-close"  style=" font-size: 17px"></i> CANCELLED</a></li>
                <li class="bar-item"><a data-toggle="tab" href="#completed"><i class="fa fa-check"  style="padding:6px;  font-size: 17px"></i> COMPLETED</a></li>
               <!--  <li class="bar-item ml-auto mr-3">
                    <form class="form-inline" action="/action_page.php">
                        <input class="search-control" style="margin-top: 10px" type="text" placeholder="Search">
                        <button class="search-btn" style="margin-top: 10px" type="submit"><i class="fa fa-search" style="color: white"></i> </button>
                    </form>
                </li> -->
            </ul>

            <?php
    // echo "<pre>";
    // print_r($whole_trip_data);
    // echo "</pre>";
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <?php
                        for($s=1;$s<=3;$s++)
                        {
                            if($s==1)
                            {
                                $st="upcom";
                                echo '<div id="upcoming" class="tab-pane fade in active show">';
                            }
                            if($s==2)
                            {
                                $st="cancel";
                                echo '<div id="cancelled" class="tab-pane fade ">';
                            }
                            if($s==3)
                            {
                                $st="complete";
                                echo '<div id="completed" class="tab-pane fade ">';
                            }

                            ?>
                            <!-- <div id="upcoming" class="tab-pane fade in active show"> -->
                               <?php
                               for($i=0;$i<count($whole_trip_data);$i++)
                               {

                                date_default_timezone_set('Asia/Kolkata');
                                 $canceltrip= $whole_trip_data[$i]['canceltrip'];
                                $change_request_id =  $whole_trip_data[$i]['change_request_id'];
                                $chkldate= strtotime(date('Y-m-d'));
                                $hotelbookid=$whole_trip_data[$i]['bookingid'];
                                $boodate=  strtotime($whole_trip_data[$i]['bookingdate']);
                                if($chkldate<$boodate && $st=="upcom" && $canceltrip=='0' )
                                {
                                     $chkclassid= "upcoming";
                                    ?>
                                    <div class="content" style="border-bottom-left-radius: 8px;border-bottom-right-radius: 8px">
                                        @if($whole_trip_data[$i]['triptype']=='Hotel')
                                        <div class="row">
                                            <div class="col-sm-4 col-md-3">
                                                <img class="img-fluid img-rounded p-5" src="{{ asset('assets/images/hotelicon.png')}}">

                                            </div>
                                            <div class="col-sm-8 col-md-9 desc">
                                                <h3> {{$whole_trip_data[$i]['hotelname']}}
                                                    <?php
                                                    for($star=0;$star<$whole_trip_data[$i]['hotel_star'];$star++)
                                                    {
                                                        echo '<span class="fa fa-star"></span>';
                                                    }
                                                    $checkin=date('d M Y',strtotime($whole_trip_data[$i]['check_in']));
                                                    $checkout=date('d M Y',strtotime($whole_trip_data[$i]['check_out']));
                                                    ?>

                                                </h3>
                                                <p>Check in - {{$checkin}} | Check out - {{$checkout}}  | {{$whole_trip_data[$i]['bookingid']}}</p>
                                                <!--      <a href="#" class="trip-btn">PLAN A TRIP</a> -->
                                                <a href="#" class="trip-btn cancelbtn-{{$whole_trip_data[$i]['bookingid']}}" data-toggle="modal" data-target="#darkModalForm-{{$whole_trip_data[$i]['bookingid']}}">CANCEL</a>
                                                <div class="modal fade show" id="darkModalForm-{{$whole_trip_data[$i]['bookingid']}}" style="    background-color: rgba(0,0,0,-1.5);">
                                                    <div class="modal-dialog form-dark1">
                                                        <!--Content-->
                                                        <div class="modal-content card card-image" style="background-repeat:no-repeat;background-size: cover; background-position:left top; background-image: url('{{asset('assets/images/modal-img.jpg')}}');">
                                                            <div class="text-white rgba-stylish-strong p-modal">

                                                                <div class="modal-header text-center pb-4" style="border-bottom: 1px solid #fa9e1b!important;">
                                                                    <h3 class="modal-title w-100 white-text font-weight-bold" id="myModalLabel"><strong>CANCELLATION</strong></h3>

                                                                    <span class="close2 white-text" data-dismiss="modal"style="color: #fa9e1b!important;">&times;</span>

                                                                </div>
                                                                  <div class="alert alert-success mainsuccess" style="display: none"></div>
                                                                  <div class="alert alert-danger mainerror" style="display: none"></div>
                                                                <div class="modal-body">

                                                                    <form class="s-input" id="formcancel">
                                                                        <div class="md-form mb-3">
                                                                            <label>Booking Id</label>
                                                                            <input type="text"  id="Form-email5" class="form-control validate white-text bookingid-{{$whole_trip_data[$i]['bookingid']}}" value="{{$whole_trip_data[$i]['bookingid']}}" name="bookingid" readonly="" style="background-color:transparent; ">
                                                                            <span class="errorbooking" style="color:red;font-size: 12px"></span>
                                                                        </div>

                                                                        <div class="md-form mb-3">
                                                                            <label>Request Type</label>
                                                                            <select class="form-control requesttype-{{$whole_trip_data[$i]['bookingid']}}">
                                                                                <option value="4">Hotel Cancel</option>
                                                                                
                                                                            </select>
                                                                            <span class="errorrequest" style="color:red;font-size: 12px"></span>
                                                                        </div>

                                                                        <!-- <div class="md-form mb-3">
                                                                            <label>Cancellation</label>
                                                                            <select class="form-control">
                                                                                <option>Hello</option>
                                                                                <option>Hello</option>
                                                                                <option>Hello</option>
                                                                                <option>Hello</option>
                                                                            </select>
                                                                        </div> -->

                                                                        <div class="md-form mb-3">
                                                                            <label>Remark</label>
                                                                            <textarea class="form-control remark-{{$whole_trip_data[$i]['bookingid']}}" id="remark" ></textarea>
                                                                            <span class="errorremark" style="color:red;font-size: 12px"></span>
                                                                        </div>

                                                                        <div class="row d-flex align-items-center ">
                                                                            <div class="text-center  col-md-12">
                                                                                <button type="button" id="{{$whole_trip_data[$i]['bookingid']}}" class="c-btn1 hotelcancle">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a href="{{action('HotelController@hotelviewinvoice', $hotelbookid )}}" class="trip-btn" target="_blank">VIEW INVOICE</a>
                                            </div>
                                        </div>
                                        @else
                                        <div class="row">
                                            <div class="col-sm-4 col-md-3">
                                                <img class="img-fluid img-rounded p-5" src="{{ asset('assets/images/flighticon.png')}}">

                                            </div>
                                            <div class="col-sm-8 col-md-9 desc">
                                                <h3>{{$whole_trip_data[$i]['orign']}} To  {{$whole_trip_data[$i]['destination']}}

                                                </h3>
                                                <?php
                                                $bookingdate=date('d M Y',strtotime($whole_trip_data[$i]['bookingdate']));
                                                ?>
                                                <p>Booking Id- {{$whole_trip_data[$i]['bookingid']}} | PNR No - {{$whole_trip_data[$i]['pnrno'] }}| {{$bookingdate}} | {{$whole_trip_data[$i]['ticketid'] }} 
                                                <!--      <a href="#" class="trip-btn">PLAN A TRIP</a> -->
                                                @if($whole_trip_data[$i]['ticketrfund']=='Non-Refundable')
                                                 | <span style="font-weight: 600;color:red">Non-Refundable</span></p>
                                                 @else
                                             </p>
                                                 <a href="#" class="trip-btn" data-toggle="modal" data-target="#darkModalForm-{{$whole_trip_data[$i]['bookingid']}}">CANCEL</a>
                                                <div class="modal fade show" id="darkModalForm-{{$whole_trip_data[$i]['bookingid']}}" style="    background-color: rgba(0,0,0,-1.5);">
                                                    <div class="modal-dialog form-dark1">
                                                        <!--Content-->
                                                        <div class="modal-content card card-image" style="background-repeat:no-repeat;background-size: cover; background-position:left top; background-image: url('{{asset('assets/images/modal-img.jpg')}}');">
                                                            <div class="text-white rgba-stylish-strong p-modal">

                                                                <div class="modal-header text-center pb-4" style="border-bottom: 1px solid #fa9e1b!important;">
                                                                    <h3 class="modal-title w-100 white-text font-weight-bold" id="myModalLabel"><strong>CANCELLATION</strong></h3>

                                                                    <span class="close1 white-text" data-dismiss="modal"style="right: 0;color: #fa9e1b!important;">&times;</span>

                                                                </div>

                                                                <div class="modal-body">
                                                                    <div class="alert alert-success mainsuccess" style="display: none"></div>
                                                                  <div class="alert alert-danger mainerror" style="display: none"></div>
                                                                    <form class="s-input">
                                                                        <input type="hidden" class="orign-{{$whole_trip_data[$i]['bookingid']}}" value="{{$whole_trip_data[$i]['orign']}}">
                                                                        <input type="hidden" class="destination-{{$whole_trip_data[$i]['bookingid']}}" value="{{$whole_trip_data[$i]['destination']}}">
                                                                        <input type="hidden" class="ticketid-{{$whole_trip_data[$i]['bookingid']}}" value="{{$whole_trip_data[$i]['ticketid']}}">
                                                                        <div class="md-form mb-3">
                                                                            <label>Booking Id</label>
                                                                            <input type="text" id="Form-email5" class="form-control validate white-text bookingid-{{$whole_trip_data[$i]['bookingid']}}" value="{{$whole_trip_data[$i]['bookingid']}}" readonly="" style="background-color:transparent; ">
                                                                            <span class="errorbooking" style="color:red;font-size: 12px"></span>
                                                                        </div>

                                                                        <div class="md-form mb-3">
                                                                            <label>Request Type</label>
                                                                            <select class="form-control requesttype-{{$whole_trip_data[$i]['bookingid']}}" >
                                                                                <option value="1">Full Cancellation</option>
                                                                                <option value="2">Partial Cancellation </option>
                                                                                <option value="3">Reissuance  </option>
                                                                                
                                                                            </select>
                                                                            <span class="errorrequesttype" style="color:red;font-size: 12px"></span>
                                                                        </div>
                                                                        <div class="md-form mb-3">
                                                                            <label>Cancellation Type</label>
                                                                            <select class="form-control canceltype-{{$whole_trip_data[$i]['bookingid']}}" >
                                                                                <option value="1">No Show </option>
                                                                                <option value="2">Flight Cancelled  </option>
                                                                                <option value="3">Others   </option>
                                                                                
                                                                            </select>
                                                                            <span class="errorcanceltype" style="color:red;font-size: 12px"></span>
                                                                        </div>

                                                                        <div class="md-form mb-3">
                                                                            <label>Remark</label>
                                                                            <textarea class="form-control remark-{{$whole_trip_data[$i]['bookingid']}}" id="remark" ></textarea>
                                                                            <span class="errorremark" style="color:red;font-size: 12px"></span>
                                                                        </div>

                                                                        <div class="row d-flex align-items-center ">
                                                                            <div class="text-center  col-md-12">
                                                                                 <button type="button" id="{{$whole_trip_data[$i]['bookingid']}}" class="c-btn1 flightcancle">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                
                                                <?php
                                                $chkpnr = $whole_trip_data[$i]['pnrno'];   
                                                 
                                                $chkretpnr="";
                                                    $pdfpnr1 = $chkpnr."-".$chkretpnr;      
                                                    $pdfpnr=base64_encode($pdfpnr1);
                                                 ?>
                                                <a href="{{action('FlightBookController@flight_pdf', $pdfpnr )}}" class="trip-btn" target="_blank">VIEW INVOICE</a>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    <?php 
                                }
                                
                                if($chkldate >= $boodate && $st=="complete" && $canceltrip=='0')
                                {
                                    $chkclassid= "completed";
                                    ?>
                                    <div class="content" style="border-bottom-left-radius: 8px;border-bottom-right-radius: 8px">
                                        @if($whole_trip_data[$i]['triptype']=='Hotel')
                                        <div class="row">
                                            <div class="col-sm-4 col-md-3">
                                                <img class="img-fluid img-rounded p-5" src="{{ asset('assets/images/hotelicon.png')}}">

                                            </div>
                                            <div class="col-sm-8 col-md-9 desc">
                                                <h3> {{$whole_trip_data[$i]['hotelname']}}
                                                    <?php
                                                    for($star=0;$star<$whole_trip_data[$i]['hotel_star'];$star++)
                                                    {
                                                        echo '<span class="fa fa-star"></span>';
                                                    }
                                                    $checkin=date('d M Y',strtotime($whole_trip_data[$i]['check_in']));
                                                    $checkout=date('d M Y',strtotime($whole_trip_data[$i]['check_out']));
                                                    ?>

                                                </h3>
                                                <p>Check in - {{$checkin}} | Check out - {{$checkout}} </p>
                                                <!--      <a href="#" class="trip-btn">PLAN A TRIP</a> -->
                                              
                                                <a href="{{action('HotelController@hotelviewinvoice', $hotelbookid )}}" class="trip-btn" target="_blank">VIEW INVOICE</a>
                                            </div>
                                        </div>
                                        @else
                                        <div class="row">
                                            <div class="col-sm-4 col-md-3">
                                                <img class="img-fluid img-rounded p-5" src="{{ asset('assets/images/flighticon.png')}}">

                                            </div>
                                            <div class="col-sm-8 col-md-9 desc">
                                                <h3>{{$whole_trip_data[$i]['orign']}} To  {{$whole_trip_data[$i]['destination']}}

                                                </h3>
                                                <?php
                                                $bookingdate=date('d M Y',strtotime($whole_trip_data[$i]['bookingdate']));
                                                ?>
                                                <p>Booking Id- {{$whole_trip_data[$i]['bookingid']}} | PNR No - {{$whole_trip_data[$i]['pnrno'] }}| {{$bookingdate}} </p>
                                                <!--      <a href="#" class="trip-btn">PLAN A TRIP</a> -->
                                               <!--  <a href="#" class="trip-btn">CANCEL</a> -->
                                               <?php
                                                $chkpnr = $whole_trip_data[$i]['pnrno'];   
                                                 
                                                $chkretpnr="";
                                                    $pdfpnr1 = $chkpnr."-".$chkretpnr;      
                                                    $pdfpnr=base64_encode($pdfpnr1);
                                                 ?>
                                                <a href="{{action('FlightBookController@flight_pdf', $pdfpnr )}}" class="trip-btn" target="_blank">VIEW INVOICE</a>
                                            </div>
                                        </div>
                                        @endif

                                    </div>
                                    <?php 

                                }
                                //cancel trip
                                if( $st=="cancel" && $canceltrip=='1')
                                {
                                     $chkclassid= "cancel";
                                    ?>
                                    <div class="content" style="border-bottom-left-radius: 8px;border-bottom-right-radius: 8px">
                                        @if($whole_trip_data[$i]['triptype']=='Hotel')
                                        <div class="row">
                                            <div class="col-sm-4 col-md-3">
                                                <img class="img-fluid img-rounded p-5" src="{{ asset('assets/images/hotelicon.png')}}">

                                            </div>
                                            <div class="col-sm-8 col-md-9 desc">
                                                <h3> {{$whole_trip_data[$i]['hotelname']}}
                                                    <?php
                                                    for($star=0;$star<$whole_trip_data[$i]['hotel_star'];$star++)
                                                    {
                                                        echo '<span class="fa fa-star"></span>';
                                                    }
                                                    $checkin=date('d M Y',strtotime($whole_trip_data[$i]['check_in']));
                                                    $checkout=date('d M Y',strtotime($whole_trip_data[$i]['check_out']));
                                                    ?>

                                                </h3>
                                                <p>Check in - {{$checkin}} | Check out - {{$checkout}} | Change Request Id {{$whole_trip_data[$i]['change_request_id']}} </p>
                                                <!--      <a href="#" class="trip-btn">PLAN A TRIP</a> -->
                                              
                                                <a href="{{action('HotelController@hotelcanceldetail', $change_request_id )}}" class="trip-btn" target="_blank">VIEW STATUS</a>
                                            </div>
                                        </div>
                                        @else
                                        <div class="row">
                                            <div class="col-sm-4 col-md-3">
                                                <img class="img-fluid img-rounded p-5" src="{{ asset('assets/images/flighticon.png')}}">

                                            </div>
                                            <div class="col-sm-8 col-md-9 desc">
                                                <h3>{{$whole_trip_data[$i]['orign']}} To  {{$whole_trip_data[$i]['destination']}}

                                                </h3>
                                                <?php
                                                $bookingdate=date('d M Y',strtotime($whole_trip_data[$i]['bookingdate']));
                                                ?>
                                                <p>Booking Id- {{$whole_trip_data[$i]['bookingid']}} | PNR No - {{$whole_trip_data[$i]['pnrno'] }}| {{$bookingdate}} </p>
                                                <!--      <a href="#" class="trip-btn">PLAN A TRIP</a> -->
                                               <!--  <a href="#" class="trip-btn">CANCEL</a> -->
                                                <a href="{{action('HotelController@hotelcanceldetail', $change_request_id )}}" class="trip-btn" target="_blank">VIEW STATUS</a>
                                            </div>
                                        </div>
                                        @endif

                                    </div>
                                    <?php 

                                }
                               
                                ?>




                            <?php } ?>
                        </div>
                    <?php } ?>
                    <div id="cancelled" class="tab-pane fade">
                        <div class="content" style="border-bottom-left-radius: 8px;border-bottom-right-radius: 8px">
                            <div class="row">
                                <div class="col-sm-4 col-md-3">
                                    <img class="img-fluid img-rounded p-5" src="https://imgak.mmtcdn.com/mima/images/Desktop/upcoming-empty.png">

                                </div>
                                <div class="col-sm-8 col-md-9 desc">
                                    <h3>Hotel Swati Deluxe dhaman
                                     <span class="fa fa-star"></span>
                                     <span class="fa fa-star"></span>
                                     <span class="fa fa-star"></span>
                                 </h3>
                                 <p>Check in - 26 June 2019 | Check out - 27 June 2019</p>
                                 <!--      <a href="#" class="trip-btn">PLAN A TRIP</a> -->
                                 <a href="#" class="trip-btn">CANCEL</a>
                                 <a href="http://travoweb.com/apiflight/hotelviewinvoice/1478792" class="trip-btn" target="_blank">VIEW INVOICE</a>
                             </div>
                         </div>
                     </div>
                     <div class="content" style="border-bottom-left-radius: 8px;border-bottom-right-radius: 8px">
                        <div class="row">
                            <div class="col-sm-4 col-md-3">
                                <img class="img-fluid img-rounded p-5" src="https://imgak.mmtcdn.com/mima/images/Desktop/upcoming-empty.png">

                            </div>
                            <div class="col-sm-8 col-md-9 desc">
                                <h3>Hotel Swati Deluxe
                                 <span class="fa fa-star"></span>
                                 <span class="fa fa-star"></span>
                                 <span class="fa fa-star"></span>
                             </h3>
                             <p>Check in - 27 June 2019 | Check out - 28 June 2019</p>
                             <!--      <a href="#" class="trip-btn">PLAN A TRIP</a> -->
                             <a href="#" class="trip-btn">CANCEL</a>
                             <a href="http://travoweb.com/apiflight/hotelviewinvoice/1479184" class="trip-btn" target="_blank">VIEW INVOICE</a>
                         </div>
                     </div>
                 </div>
             </div>
             <div id="completed" class="tab-pane fade">
                <div class="content" style="border-bottom-left-radius: 8px;border-bottom-right-radius: 8px">
                    <div class="row">
                        <div class="col-sm-4 col-md-3">
                            <img class="img-fluid img-rounded p-5" src="https://imgak.mmtcdn.com/mima/images/Desktop/cancelled-empty.png">

                        </div>
                        <div class="col-sm-8 col-md-9 desc">
                            <h3>Looks empty, you've no cancelled  bookings.</h3>
                            <p>When you book a trip, you will see your itinerary here.</p>
                        </div>
                    </div>
                </div>
                <div class="content" style="border-bottom-left-radius: 8px;border-bottom-right-radius: 8px">
                    <div class="row">
                        <div class="col-sm-4 col-md-3">
                            <img class="img-fluid img-rounded p-5" src="https://imgak.mmtcdn.com/mima/images/Desktop/completed-empty.png">
                        </div>
                        <div class="col-sm-8 col-md-9 desc">
                            <h3>Looks empty, you've no completed bookings.</h3>
                            <p>When you book a trip, you will see your itinerary here.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- start modal -->
<!-- <button class=""data-toggle="modal" data-target="#myModal_charges">Cancellation</button> -->
                <div class="modal fade text-center py-5" style="top:30px" id="myModal_charges">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content" style="border-radius: 7px;">
                            <div class="modal-header h-bg">
                                <span class="close close-btn" data-dismiss="modal">&times;</span>

                            </div>
                            <div class="modal-body">

                                <h3 class="pt-3 mb-0 h-cancel success_modal"> </h3>
                                <!-- <p class="t-id-info">Your Change Request Id:1234</p> -->
                                <button class="btn1 text-white mb-5 success_ok" style="margin-top:20px; margin-bottom: 0px !important">Submit</button>
                              <!--   <a role="button" class="btn1 text-white mb-5 success_ok" href="https://www.sunlimetech.com" target="_blank">Submit</a> -->
                            </div>
                            <div class="modal-footer f-bg mt-3">
                                <span class="d-block mr-auto f-help "><i class="fa fa-phone"></i> Helpline:<a href="#"> 34356894454</a></span>
                                <span class="f-help"><img src="{{asset('assets/images/logo.png')}}" class="f-logo"></span>
                            </div>
                        </div>
                    </div>
                </div>
<!-- Modal -->

    </div>
</div>
</div>
<div class="modal fade myModal" id="myModaln1" role="dialog" data-backdrop="static" data-keyboard="false" style="background-color: #f0f8ffd1;">
    <div class="modal-dialog">
    <center><img src="{{ asset('assets/images/newf.gif')   }}" style="height: auto;width: auto; display: block;margin:60% auto;position: relative;top:50%;transform: translateY(-50%)"></center>
    </div>
</div>
<!-- Footer -->@include('pages.include.footer')
<!-- Copyright -->@include('pages.include.copyright')
<script>
    $(document).ready(function() {
        $(".loader").fadeOut("slow");
    });
</script>
<script>
    $(document).on('click','.hotelcancle',function(){
        var bookid = this.id;
        var requesttype= $('.requesttype-'+bookid).val();
        var remark= $('.remark-'+bookid).val();
        var bookingid=$('.bookingid-'+bookid).val();
        if(bookingid=='')
        {
            $('.errorbooking').text("Please Enter Booking Id");
        }
        if(remark=='')
        {
           $('.errorremark').text("Please Enter Remark");
        }
        else
        {
            $('#myModaln1').modal('show');
            $.ajax({
                url :'{{route("hotelcancel")}}',
                data : { 'requesttype' :requesttype,
                        'remark' :remark,
                        'bookingid' :bookingid,

                      },
                type : 'get',
                success : function(response)
                {
                    $('#myModaln1').modal('hide');
                    if(response=='fail')
                    {
                        $('.mainerror').text("Please Enter Booking Id");
                        $('.mainerror').show();

                    }
                    else
                    {
                        $('.mainerror').hide();
                        var chkst = response.split('%%');
                        var errmsg = chkst[1];
                        var chgrequest=chkst[2];
                        if(errmsg !='')
                        {
                            $('.mainerror').text(errmsg);
                            $('.mainerror').show();
                        }
                        else
                        {
                            $('.mainerror').hide();
                            $('.mainsuccess').text("Successfully Hotel Cancel. Your Change Request Id "+ chgrequest);
                            $('.mainsuccess').show();
                            $('#darkModalForm-'+bookid).modal('hide');
                            $('#myModal_charges').modal('show');
                            $('.success_modal').text("Successfully Hotel Cancelled. Your Change Request Id "+ chgrequest +"Refund amount will be transferred to your account within 7-15 working days");
                            $('.cancelbtn-'+bookid).hide();
                        }
                        
                    }
                }
            })
        }

    })

</script>
<!-- flight cancel -->
<script>
    $(document).on('click','.flightcancle',function(){
        var bookid = this.id;
        $('.mainerror').hide();
        var requesttype= $('.requesttype-'+bookid).val();
        var remark= $('.remark-'+bookid).val();
        var orign=$('.orign-'+bookid).val();
        var destination=$('.destination-'+bookid).val();
        var ticketid=$('.ticketid-'+bookid).val();
        var bookingid=$('.bookingid-'+bookid).val();
        var canceltype= $('.canceltype-'+bookid).val();
        if(bookingid=='')
        {
            $('.errorbooking').text("Please Enter Booking Id");
        }
        if(remark=='')
        {
           $('.errorremark').text("Please Enter Remark");
        }
        else
        {
           $('#myModaln1').modal('show');
            $.ajax({
                url :'{{route("flightcancel")}}',
                data : { 'requesttype' :requesttype,
                        'remark' :remark,
                        'bookingid' :bookingid,
                        'orign' :orign,
                        'destination':destination,
                        'ticketid':ticketid,
                        'canceltype':canceltype,


                      },
                type : 'get',
                success : function(response)
                {
                   $('#myModaln1').modal('hide');
                     var chkst = response.split('%%');
                     var status=chkst[0];
                     var errmsg = chkst[1];
                     var chgrequest=chkst[2];
                    
                     if(errmsg!='')
                     {
                        $('.mainerror').text(errmsg);
                        $('.mainerror').show();
                     }
                     else if(status=='successfull' && chgrequest !="")
                     {
                        $('.mainerror').hide();
                        var chkst = response.split('%%');
                        var errmsg = chkst[1];
                        var chgrequest=chkst[2];
                         $('.mainerror').hide();
                            $('.mainsuccess').text("Successfully Flight Cancel. Your Change Request Id "+ chgrequest);
                            $('.mainsuccess').show();
                            $('#darkModalForm-'+bookid).modal('hide');
                            $('#myModal_charges').modal('show');
                            $('.success_modal').text("Successfully Flight Cancelled. Your Change Request Id "+ chgrequest +"Refund amount will be transferred to your account within 7-15 working days");
                            $('.cancelbtn-'+bookid).hide();
                     }
                     
                    else
                    {
                       
                        $('.mainerror').text(response);
                        $('.mainerror').show();
                    }
                    
                }
            })
        }

    })

</script>
<!-- end flight cancel -->
<script type="text/javascript">
$(document).on('click', ".success_ok", function ()
{
    $('#myModal_charges').modal('hide');
    location.reload();
});
</script>
<script>
    $(document).on('click','.modalopen',function(){
        $('#myModal_charges').modal('show');
    })
</script>
</body>

</html>