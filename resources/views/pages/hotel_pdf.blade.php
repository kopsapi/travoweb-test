<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
<style>
    body {
        font-family: 'Roboto', sans-serif;
    }
    .container{
        width: 100%;
    }

    .row{
        margin-right: -15px;
        margin-left: -15px;
    }

    .row:after,
    .row:before {
        display: table;
        content: " ";
        clear: both;
    }




    .col-md-6 {
        width: 46%;
        float: left;
        padding-left: 15px;
        padding-right: 15px;

    }

    .i-heading{
        font-size: 40px;
        font-weight: bolder;
        color: #598fe7;
    }

    .i-span{
        font-size: 38px;
        color: #fa9e1b;
        font-weight: bolder;
    }

    .h-confirm{
        font-size: 25px;
    }
    .col-md-7 {
        width: 58.33333333%;
        float: left;
        padding-left: 15px;
        padding-right: 15px;
    }

    .col-md-8 {
        width: 60.66666667%;
        float: left;
        padding-left: 15px;
        padding-right: 15px;
    }

    .col-md-9 {
        width: 75%;
        float: left;
        padding-left: 15px;
        padding-right: 15px;
    }

    .col-md-10 {
        width: 83.33333333%;
        float: left;
        padding-left: 15px;
        padding-right: 15px;
    }

    .col-md-11 {
        width: 91.66666667%;
        float: left;
        padding-left: 15px;
        padding-right: 15px;
    }

    .col-md-12 {
        width: 100%;
        float: left;
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-md-1 {
        width: 8.33333333%;
        float: left;
        padding-left: 15px;
        padding-right: 15px;
    }

   

    .col-md-4 {
        width: 29%;
        float: left;
        padding-left: 15px;
        padding-right: 15px;
    }

 

    .v-icon{
        width: 30px;
        margin-top: 5px;
        text-align: right;
        margin-right: 0;
    }
    .pdf-footer
    {
        margin-top:30px;
    }
    .pdf-footer .earphone img
    {
        width:30px;
    }
    .pdf-footer .earphone
    {
        float:left;
        width:40px;
    }
    .pdf-footer .support
    {
        margin-left:50px;
    }
    .pdf-footer .support .small-text
    {
        font-size:12px;
        color:#666;
    }
    .pdf-footer .support .text-bold
    {
        font-size:14px;
        color:#000;
        white-space:nowrap;
    }
    .c-icon{
        width: 25px;
        margin-top: 7px;

    }
    .hr-style{
        border:none;
        border-top: 2px solid #fa9e1b;
        width: 100%
    }
    .alert1{
        color: #721c24;
        background-color: #f8d7da;
        border-color: #f5c6cb;
        padding: 0.75rem 1.25rem;
        margin-bottom: 1rem;
        border: 1px solid #f5c6cb;
        border-radius: 0.25rem;
        font-size: 14px;
    }
    .alert2{
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb;
        padding: 0.75rem 1.25rem;
        margin-bottom: 1rem;
        border: 1px solid  #c3e6cb;
        border-radius: 0.25rem;
        font-size: 14px;

    }
    .caret-img{
        width: 9px;
        margin-right: 3px;
    }
    li.t-policy{

        padding: 10px;
        color: #4e4e4e;
        font-weight: normal;
        font-size: 13px;
    }
    .table th, .table td {
        padding: 0.75rem;
        vertical-align: top;
        border: 1px solid #8c8f91;
    }
    th {
        text-align: left;
    }
    td{
        text-align: right;
    }
    th {
        display: table-cell;
        vertical-align: inherit;
        text-align: left;

    }
    .table {
        width: 100%;
        margin-bottom: 1rem;
        font-size: 13px;
        color: #212529;
    }


    table {
        border-collapse: separate;;

    }
    ul {
        list-style: none;
    }

    ul li::before {
        content: "\2022";
        color: #fa9e1b;
        font-weight: bolder;
        display: inline-block;
        font-size: 17px;
        width: 1em;
        margin-left: -1em;
        margin-top: 3px;
    }
    p.t-info{
        font-size: 15px;
    }
    .l-icon{
        width:150px;
        padding: 20px 0;
    }
</style>

<div class="container" style="margin-top: -10px;">
    <div class="row"  style="margin-bottom: -30px">
        <div class="col-md-6"><img src="{{asset('assets/images/logo.png')}}" class="l-icon"></div>
        <div class="col-md-6">
            <p class="h-confirm" style="text-align:right;"><img src="{{asset('assets/images/icon-tick-28.jpg')}}" class="v-icon"> Confirmation Voucher</p>
        </div>
    </div>
    <hr class="hr-style">
        <p><span style="color: red">Guest Name: </span>{{ucfirst($hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['FirstName'])}} {{ucfirst($hotelbookdetailsarray['HotelRoomsDetails'][0]['HotelPassenger'][0]['LastName'])}}</p>
    <hr class="hr-style">
    <div class="container" style="font-size: 15px">
        <div class="row">
            <div class="col-md-4">
                <h3 style="color: #fa9e1b">{{$hotelbookdetailsarray['HotelName']}}</h3>
                <p style="color: #333">{{$hotelbookdetailsarray['AddressLine1']}}

                </p>
                <p style="color:#333;"> {{$hotelbookdetailsarray['AddressLine2']}}</p>
            </div>
            <div class="col-md-4" style="text-align: center">
                <p style="color: blue"><img src="{{asset('assets/images/t-clock.jpg')}}" class="c-icon"> Check IN</p>
                <h3><?php 
                    $getcheckindatetime=explode('T',$hotelbookdetailsarray['CheckInDate']);

                    $getcheckindate=date('F d,Y',strtotime($getcheckindatetime[0]));
                    echo $getcheckindate;
                    ?></h3>
            </div>
            <div class="col-md-4" style="text-align: center">
                <p style="color:blue;"><img src="{{asset('assets/images/t-clock.jpg')}}" class="c-icon"> Check OUT</p>
                <h3 style="text-align: center"><?php 
                        $getcheckindatetime=explode('T',$hotelbookdetailsarray['CheckOutDate']);

                        $getcheckindate=date('F d,Y',strtotime($getcheckindatetime[0]));
                        echo $getcheckindate;
                        ?></h3>
            </div>
        </div>
    </div>

    <hr class="hr-style">
    <div class="container">
    <div class="row">
        <div class="col-md-6">
        <table  class="table">
            <?php
                    $bookingdate=explode(' ',$hotelbookdetailsarray['created_on']);
                    $bookdate=date('F d,Y',strtotime($bookingdate[0]));
                    $booktime=date('h:i a',strtotime($bookingdate[1]));

                    $amenties="";
                    for($inclusion=0;$inclusion< count($hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion']);$inclusion++)
                    {

                        $amenties.=$hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion'][$inclusion];
                        if($inclusion<count($hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion'])-1)
                        {
                            $amenties.=" , ";
                        }
                    } 
                    ?>
                   
            <tbody>
                    <tr>
                        <th>Booking ID:</th>
                        <td>{{$hotelbookdetailsarray['BookingId']}}</td>
                    </tr>
                    <tr>
                        <th>Hotel Booking ID:</th>
                        <td>{{$hotelbookdetailsarray['BookingId']}}</td>
                    </tr>
                    <!-- <tr>
                        <th>Advance Receipt Number:</th>
                        <td>HDAR000028526560</td>
                    </tr> -->
                    <tr>
                        <th>Date of Booking:</th>
                        <td>{{$bookdate}}, {{$booktime}}</td>
                    </tr>
                    

            </tbody>
                </table>
        </div>

        <div class="col-md-6">
        <table class="table">
                    <tbody>
                    <tr>
                        <th>Room Type</th>
                        <td>{{$hotelbookdetailsarray['HotelRoomsDetails'][0]['RoomTypeName']}} {{$amenties}}</td>
                    </tr>
                    @for($rooms=0;$rooms< count($hotelbookdetailsarray['HotelRoomsDetails']);$rooms++)
                       <?php
                       $adultcount=0;
                       $childcount=0;
                       $childage1='';
                       $childval='';
                       ?>       
                    <tr>
                        <th>Room {{$rooms+1}}</th>

                        <td>
                             @for($passenger=0;$passenger< count($hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger']);$passenger++)
                            <?php 
                            $childage = $hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['Age'];
                           
                             if($hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['PaxType']==1)
                               $adultcount++;
                           else
                             $childcount++;
                            $childval='Child';
                            if($hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['PaxType']==2)
                            {
                                $childage1.=$childage.",";
                            }
                            else
                            {
                                $childage1='';
                            }
                            
                            $childnew = "[".$childage1."]";
                         ?>
                         @endfor
                            {{$adultcount}} Adults / {{$childcount}} {{$childval}} {{$childnew}} </td>
                    </tr> 
                       @endfor
                    
                    </tbody>
                </table>
        </div>
    </div>
    </div>




        <p><span style="color: #189ad4">Inclusions:</span> FREE 
            @for($inclusion=0;$inclusion< count($hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion']);$inclusion++)
            {{$hotelbookdetailsarray['HotelRoomsDetails'][0]['Inclusion'][$inclusion]}}
             
            @endfor
        </p>

        <hr class="hr-style">
    <br>
    <?php
        $RoomPrice=0;
        $Tax=0;
        $ExtraGuestCharge=0;
        $ChildCharge=0;
        $OtherCharges=0;
        $AgentCommission=0;
        $AgentMarkUp=0;
        $PublishedPrice=0;
        $ServiceTax=0;
        $TDS=0;
        $TotalGSTAmount=0;
        $total_fare=0;
        $gst=0;
        $total_gst=0;
        $grand_total=0;
        $mainothercharges=0;
        if($hotelbookdetailsarray['hotelfaicon']=='fa-inr')
        {
            $curr="Rs.";
        }
        else
        {
            $curr="$";
        }

        ?>
        <div class="container">
              <div class="row">
                <div class="col-md-12">
                   <table  class="table table-striped" style="width: 100%">
                <tbody>
                <tr>
                    <th style="color: #fa9e1b">Description</th>
                    <td style="color: #fa9e1b">Amount</td>

                </tr>
                <tr>
                    <th>Room Charges Collected on Behalf of Hotel</th>
                    <td>{{$curr}} {{$hotelbookdetailsarray['hotelmarginprice']}}</td>
                </tr>
                <tr>
                    <th>Tax </th>
                    <td> {{$curr}} {{$hotelbookdetailsarray['hotelservice_tax']}}</td>
                </tr>
                
                
                <tr>
                    <th>GST on Convenience Fees</th>
                    <td> {{$curr}} {{$hotelbookdetailsarray['hotelmargingst']}}
                    </td>
                </tr>
               
                <tr>
                    <th>Net Amount Paid</th>
                    <td>{{$curr}} {{$hotelbookdetailsarray['hotelamount']}}</td>
                </tr>
              

                </tbody>
            </table>
                </div>
              </div>
        </div>

<br>
    <hr class="hr-style">


    <div class="container">
        <div class="alert1"><img src="{{asset('assets/images/c-r.png')}}" class="caret-img"> Important Note: Booked & Payable at travoweb.com</div>
    </div>


    <div class="container">
        <div class="alert2"><img src="{{asset('assets/images/c-r1.png')}}" class="caret-img"> Description of Service: Reservation services for accommodation</div>
    </div>



  <!--   <div class="container">
        <p class="t-info"><img src="{{asset('assets/images/caret-right-512.png')}}" class="b-icon"> Additional Information</p>
    </div> -->
<!-- <hr class="hr-style">
    <div class="container">
        <p class="t-info"><img src="{{asset('assets/images/caret-right-512.png')}}" class="b-icon"> Hotel Policy:</p>
    </div>
    <hr class="hr-style">
    <div class="container">
        <p class="t-info"><img src="{{asset('assets/images/caret-right-512.png')}}" class="b-icon"> Cancellation & Amendment Policy</p>
    </div> -->
    <hr class="hr-style">

        <h4>Cancellation Policy
    </h4>
        <ul>
            <?php
              $cancellationpolicy=array();
              for($cancel=0;$cancel<count($hotelbookdetailsarray['HotelRoomsDetails'][0]['CancellationPolicies']);$cancel++)
              {
               $cancellationpolicy=$hotelbookdetailsarray['HotelRoomsDetails'][0]['CancellationPolicies'];
               $fromdatetime=explode('T',$cancellationpolicy[$cancel]['FromDate']);
               $fromdate=date('d-F-Y',strtotime($fromdatetime[0]));
               $fromtime=date('h:i a',strtotime($fromdatetime[1]));

               $todatetime=explode('T',$cancellationpolicy[$cancel]['ToDate']);
               $todate=date('d-F-Y',strtotime($todatetime[0]));
               $totime=date('h:i a',strtotime($todatetime[1]));

               echo '<li class="t-policy"> '.$cancellationpolicy[$cancel]['Charge'].' '.' of total amount will be charged , If cancelled between '.$fromdate.' '.$fromtime.' and '.$todate.' '.$totime.'</li>';

           }
           ?>
        </ul>

    <hr class="hr-style">



<div class="container">
    <h4>Hotel Policy
    </h4>
    <ul style="padding-bottom: 10px">
        <?php
          $cancellationpolicy=explode('|',$hotelbookdetailsarray['HotelPolicyDetail']);
          for($policy=0;$policy<count($cancellationpolicy);$policy++)
          {
            if($policy<=1)
            {
                continue;
            }
            if($cancellationpolicy[$policy]!='')
                echo ' <li class="t-policy">'.$cancellationpolicy[$policy]."</li>";

        }
        ?> 
    </ul>
</div>
    <hr class="hr-style">
   <!--  <div class="container">
        <h4>Cancellation Procedures
        </h4>
        <ul style="padding-bottom: 10px">
             <?php
              $cancellationpolicy=array();
              for($cancel=0;$cancel<count($hotelbookdetailsarray['HotelRoomsDetails'][0]['CancellationPolicies']);$cancel++)
              {
               $cancellationpolicy=$hotelbookdetailsarray['HotelRoomsDetails'][0]['CancellationPolicies'];
               $fromdatetime=explode('T',$cancellationpolicy[$cancel]['FromDate']);
               $fromdate=date('d-F-Y',strtotime($fromdatetime[0]));
               $fromtime=date('h:i a',strtotime($fromdatetime[1]));

               $todatetime=explode('T',$cancellationpolicy[$cancel]['ToDate']);
               $todate=date('d-F-Y',strtotime($todatetime[0]));
               $totime=date('h:i a',strtotime($todatetime[1]));

               echo '<li class="t-policy"> '.$cancellationpolicy[$cancel]['Charge'].'%'.' of total amount will be charged , If cancelled between '.$fromdate.' '.$fromtime.' and '.$todate.' '.$totime.'</li>';

           }
           ?>
           


        </ul>
    </div> -->

   <!--  <hr class="hr-style">
    <div class="container">
        <h4>Modifications & Refunds
        </h4>
        <ul style="padding-bottom: 10px">
            <li class="t-policy">As you are aware - that goibibo is just a booking agent. We do not control, own, create or direct hotels and their rates and inventory updates on our
                website. In case hotel does not honor the booking our team will work hard and shall suggest you the best alternative for your stay, which shall be as per your
                discretion</li>
            <li class="t-policy">Goibibo reserves the right to cancel any booking made by any travel agent using promotional offers for business purpose without any prior notice. The
                promotional offers are strictly meant for bookings booked for personal travel only.</li>
            <li class="t-policy">The detailed terms and conditions set out at <a href="#">Terms and Conditions</a> are incorporated herein by reference and shall accordingly apply to the booking.
            </li>
            <li class="t-policy">Applicable only for the Hotels booked to stay/travel within the territory of India</li>
            <li class="t-policy">Modification request GoTime shall start from acknowledgement of Customer’s request by Goibibo and end with successful resolution as specified above</li>
            <li class="t-policy">Company’s decision with regard to the calculation of GoTime and in case of delay, promotional GoCash, shall be final, binding and undisputable.
                GoTime Promise shall not be applicable in the following cases:</li>
            <li class="t-policy">Non refundable bookings</li>
            <li class="t-policy">Modification of booking on or after due check-in date</li>
            <li class="t-policy">• Modification of booking due to no-show</li>
            <li class="t-policy">• Booking made through Company’s Business Partners. (HDFC Smart Buy, Kotak Mobile App, Citi Premier Miles, Apps Daily, Samsung Galaxy Platform )</li>
            <li class="t-policy">Post-paid/Pay-at Hotel bookings</li>
            <li class="t-policy">Package bookings i.e. Hotel plus Hotel / Bus</li>
            <li class="t-policy">Promotional GoCash payable in case of delay in meeting GoTime Promise by Company per booking error acknowledged shall not exceed INR 5000.</li>
            <li class="t-policy">Company reserves the right to discontinue GoTime Promise at any time without assigning any reasons whatsoever</li>
            <li class="t-policy">Company powers to add to the list of exceptions where GoCash compensations are exempt</li>
            <li class="t-policy">Should you wish to cancel or modify reservations made with Goibibo, this can be done by calling our office at 1-860-2-585858/1800-208-1060.
                Cancellations and modifications must be made with Goibibo and not with the hotel as the contract that has been entered into is </li>
            <li class="t-policy">For any modification, User shall pay applicable cancellation/modification charges.</li>
            <li class="t-policy">All modifications are subject to availability and agreement of the Hotel. If the total of the modification is cheaper than the cost of the initial booking, then a
                refund of the difference in cost will be given.</li>
            <li class="t-policy">Cancellations or modifications notified directly to the hotel or other accommodation will not be effective. Requests for cancellations and modifications must
                be made directly through Goibibo.</li>
            <li class="t-policy">Once a booking is modified, it cannot be modified further or refunded</li>


        </ul>
    </div> -->

    <div class="container" style="text-align:center;">
        <h3><img src="{{asset('assets/images/24-7-icon.png')}}" class="c-icon"> Care Support -<span style="font-size: 15px; font-weight: normal; color: #333">It is FASTER to WRITE to US.<p></p></h3>

        <p>To tell us your issue go to <a href="#"> www.travoweb.com/support/</a></p>
    </div>
    <hr class="hr-style">
    <div class="container">
        <div class="alert1">Disclaimer:- Hotel and GST charges are collected on behalf of the hotel. These details are just for information and cannot be used for GST claim.
           </div>
    </div>

    <hr class="hr-style">



    <div class="pdf-footer">
        <hr style="width: 50%;border: none;border-bottom: 1px solid #5e5e5e; margin: auto">
        <div class="row">

            <div class="col-md-12" style="width:100%; margin:0 auto;font-size: 13px;text-align: center">

                <img src="{{asset('assets/images/logo.png')}}" style="width: 120px;margin-top: 10px" class="img-fluid d-block mx-auto">
                
                <p style="margin: 5px!important; color: black;"><b>Ph</b> +61 490149833</p>
                @if($hotelbookdetailsarray['hotelfaicon']=='fa-inr')
                <p style="margin: 5px!important; color: black;"><b> GST No.</b> 03BDHPK8214M1ZG</p>
                @endif
                <p style="margin: 5px!important;"><b> Email</b>  <a href="#">info@travoweb.com</a>,  <span style="margin: 5px!important;"> <b>Website:</b>Travoweb.com</span></p>


            </div>
        </div>
    </div>
    <hr>
</div>
<style>
   .t-addr{
       font-weight: 500;
       color: blue;
   }
</style>


</body>
</html>