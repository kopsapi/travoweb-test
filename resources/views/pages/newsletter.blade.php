<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <style type="text/css"></style>
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <title>Invoice Email</title><!-- Designed by https://github.com/kaytcat -->
    <!-- Robot header image designed by Freepik.com -->
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Droid+Sans);

        /* Take care of image borders and formatting */

        img {
            max-width: 600px;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        a {
            text-decoration: none;
            border: 0;
            outline: none;
            color: #bbbbbb;
        }

        a img {
    border: none;
    width: auto;
    height: 50px;
}

        /* General styling */

        td,
        h1,
        h2,
        h3 {
            font-family: Helvetica, Arial, sans-serif;
            font-weight: 400;
        }

        td {
            text-align: center;
        }

        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100%;
            height: 100%;
            color: #37302d;
            background: #ffffff;
            font-size: 16px;
        }
        a.redirect-btn {
    background: #008ace;
    color: white;
    padding: 10px;
    margin-top: 5px !important;
    display: inline-block;
}
        table {
            border-collapse: collapse !important;
        }

        .headline {
            color: #ffffff;
            font-size: 30px;
        }

        .force-full-width {
            width: 100% !important;
        }

        .force-width-80 {
            width: 80% !important;
        }
    </style>
    <style media="screen" type="text/css">
        @media screen {

            /*Thanks Outlook 2013! https://goo.gl/XLxpyl*/
            td,
            h1,
            h2,
            h3 {
                font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
            }
        }
    </style>
    <style media="only screen and (max-width: 480px)" type="text/css">
        /* Mobile styles */
        @media only screen and (max-width: 480px) {

            table[class="w320"] {
                width: 320px !important;
            }

            td[class="mobile-block"] {
                width: 100% !important;
                display: block !important;
            }


        }
    </style>
</head>


<body bgcolor="#ffffff" class="body"
    style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none">
    <table align="center" cellpadding="0" cellspacing="0" class="force-full-width" height="100%">
        <tbody>
            <tr>
                <td align="center" bgcolor="#ffffff" class="" valign="top" width="100%">
                    <center class="">
                        <table cellpadding="0" cellspacing="0" class="w320" style="margin: 0 auto;" width="600">
                            <tbody>
                                <tr>
                                    <td align="center" class="" valign="top">
                                        <table cellpadding="0" cellspacing="0" class="force-full-width"
                                            style="margin: 0 auto;">
                                            <tbody>
                                                <tr>
                                                    <td class="" style="font-size: 30px; text-align:center;"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table style="margin: 0 auto;background: white;border: 3px solid #008ace;border-bottom: none;border-top: none;" cellpadding="0" cellspacing="0"
                                            class="force-full-width" style="margin: 0 auto;">
                                            <tbody class="">
                                                <tr class="" style="background: #008ace;">
                                                    <td class=""><br>
                                                        <br>
                                                        <a class="" data-click-track-id="8768"
                                                            href="http://www.Travoweb.com" target="_blank"><img class=""
                                                                height="70"
                                                                src="https://travoweb.com/assets/images/logo-w.png"
                                                                width="70"></a>
                                                        <br>
                                                        <br></td>
                                                </tr>
                                                <tr class="">
                                                    <td class="headline" style="margin-top:20px"><b style="color:#464646;margin-top: 19px !important; display: block;" class="headline">Package Enq Id : {{ucfirst($chkarry['enqdetails']['enq_id'])}}  </b>
                                                        <!-- <span class=""
                                                            style="font-weight:lighter;">web</span>  --><!-- Premium Receipt -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <center class="">
                                                            <table cellpadding="0" cellspacing="0"
                                                                style="margin: 0 auto;" width="70%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="" style="color:#008ace;"><br>
                                                                           Dear  {{ucfirst($chkarry['enqdetails']['firstname'])}} {{ucfirst($chkarry['enqdetails']['lastname'])}}
                                                                            <br>
                                                                            <br></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </center>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class=""></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <?php
                                        if($chkarry['enqdetails']['currency']=='INR')
                                        {
                                            $cur="INR";
                                        }
                                        else
                                        {
                                            $cur="$";
                                        }
                                        ?>
                                        <table bgcolor="#f5774e" cellpadding="0" cellspacing="0"
                                            class="force-full-width" style="margin: 0 auto;">
                                            <tbody>
                                                <tr class="">
                                                    <td class=""
                                                        style="background-color:#ffffff; border-left:solid;border-bottom: solid; border-right:solid; border-color:#008ACE; color:#008ACE;">
                                                        <center class=""><br><br>
                                                            <table cellpadding="0" cellspacing="0"
                                                                class="force-width-80" style="margin:0 auto;">
                                                                <tbody class="">
                                                                    <tr class="">
                                                                        <td class=""
                                                                            style="text-align:left; color:#4A4A4A;">
                                                                            <span class=""
                                                                                style="color:#909090; font-weight:lighter;">Package
                                                                                Name</span>
                                                                        </td>
                                                                        <td class=""
                                                                            style="text-align:right; vertical-align:top; color:#4A4A4A;">
                                                                            <span class=""
                                                                                style="color:#4a4a4a; font-weight:normal; text-align:right;">{{$chkarry['enqdetails']['pckg_name']}}</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="">
                                                                        <td class=""
                                                                            style="text-align:left; color:#4A4A4A;">
                                                                            <br>
                                                                            <span class=""
                                                                                style="color:#909090; font-weight:lighter;">Date</span>
                                                                        </td>
                                                                        <td class=""
                                                                            style="text-align:right; vertical-align:top; color:#4A4A4A;">
                                                                            <br>
                                                                            <span class=""
                                                                                style="color:#4a4a4a; font-weight:normal; text-align:right;"><?php echo date("d-m-Y", strtotime($chkarry['enqdetails']['pckgdate']));?></span></td>
                                                                    </tr>
                                                                    <tr class="">
                                                                        <td class=""
                                                                            style="text-align:left; color:#4A4A4A;">
                                                                            <br>
                                                                            <span class=""
                                                                                style="color:#909090; font-weight:lighter;">Price To pay
                                                                                </span>
                                                                        </td>
                                                                        <td class=""
                                                                            style="text-align:right; vertical-align:top; color:#4A4A4A;">
                                                                            <br>
                                                                            <span class=""
                                                                                style="color:#4a4a4a; font-weight:normal; text-align:right;">{{$cur}} {{$chkarry['offer_pack']['offer_price']}}</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="">
                                                                        <td class=""
                                                                            style="text-align:left; color:#4A4A4A;">
                                                                            <br>
                                                                            <span class=""
                                                                                style="color:#909090; font-weight:lighter;">Balance Amount
                                                                                </span>
                                                                        </td>
                                                                        <td class=""
                                                                            style="text-align:right; vertical-align:top; color:#4A4A4A;">
                                                                            <br>
                                                                            <span class=""
                                                                                style="color:#4a4a4a; font-weight:normal; text-align:right;">{{$cur}} {{$chkarry['offer_pack']['offer_balance']}}</span>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- <tr class="">
                                                                        <td class=""
                                                                            style="text-align:left; color:#4A4A4A;">
                                                                            <br>
                                                                            <span class=""
                                                                                style="color:#909090; font-weight:lighter;">Total Amount
                                                                                </span>
                                                                        </td>
                                                                        <td class=""
                                                                            style="text-align:right; vertical-align:top; color:#4A4A4A;">
                                                                            <br>
                                                                            <span class=""
                                                                                style="color:#4a4a4a; font-weight:normal; text-align:right;">$36.99</span>
                                                                        </td>
                                                                    </tr> -->
                                                                    <?php
                                                                    $newpckid=base64_encode($chkarry['offer_pack']['pckg_id']);
                                                                    $newenqid=base64_encode($chkarry['offer_pack']['enq_id']);
                                                                    $newprice=base64_encode($chkarry['offer_pack']['offer_price']);
                                                                    if($chkarry['enqdetails']['currency']=='INR')
                                                                    {
                                                                        $pagedirect='request_package';
                                                                    }
                                                                    else
                                                                    {
                                                                        $pagedirect='packages_pay';
                                                                    }
                                                                    ?>
                                                                    <tr class="">
                                                                        <td class=""
                                                                            style="text-align:left; color:#4A4A4A;">
                                                                            <br>
                                                                            <span class=""
                                                                                style="color:#909090; font-weight:lighter;">
                                                                                </span>
                                                                        </td>
                                                                        <td class=""
                                                                            style="text-align:right; vertical-align:top; color:#4A4A4A;">
                                                                            <br>
                                                                            <?php
                                                                            echo "<a href='https://travoweb.com/".$pagedirect."?pck=".$newpckid."%%".$newprice."%%".$newenqid."' class='redirect-btn' >Pay Now</a>"
                                                                            ?>
                                                                            
                                                                            
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table cellpadding="0" cellspacing="0"
                                                                class="force-width-80" style="margin:0 auto;">
                                                                <tbody class="">
                                                                    <tr class="">
                                                                        <td class="mobile-block"></td>
                                                                        <td class="mobile-block"></td>
                                                                        <td class="mobile-block"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table cellpadding="0" cellspacing="0"
                                                                class="force-width-80" style="margin: 0 auto;">
                                                                <tbody>
                                                                    <tr class=""></tr>
                                                                </tbody>
                                                            </table>
                                                            <table cellpadding="0" cellspacing="0"
                                                                class="force-width-80" style="margin: 0 auto;">
                                                                <tbody>
                                                                    <tr class="">
                                                                        <td class=""
                                                                            style="text-align:left; color:#933f24;"><br>
                                                                            <center class=""
                                                                                style="color:#999999; border-top:1px solid #FAFAFA;">
                                                                                <br>
                                                                                You may keep this receipt for your
                                                                                records. For your questions, contact us
                                                                                anytime at <a class="" color:=""
                                                                                    data-click-track-id="8537"
                                                                                    font-weight:lighter=""
                                                                                    href="mailto:info@travoweb.com"
                                                                                    style="color:#008ACE;">info@travoweb.com</a>
                                                                            </center>
                                                                            <br>
                                                                            <br>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </center>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                     
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </center>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>