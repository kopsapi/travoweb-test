@include('pages.include.header')

<body>
    <style>
        .intro {
            width: 100%;
            padding-top: 100px;
            padding-bottom: 0px;

        }

        .add_content {
            z-index: 9;
        }

        .about-header {
            background: url("{{asset('assets/images/about-h.jpg')}}");
            height: 450px;
            margin-top: 126px;
            position: relative;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center center;
        }

        h1.about-title {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
            background: rgba(49, 18, 75, 0.8);
            padding: 10px 35px;
        }

        .intro_title {
            text-align: center;
        }

        ul.n-ul {
            margin-top: 30px;
        }

        ul.n-ul li {
            padding: 10px 20px;
            color: #9555ef;
            font-size: 15px;
            font-weight: 500;
        }

        ul.n-ul li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
            font-size: 20px;
            font-weight: 900;
        }

        .intro {
            background: white;
        }

        h1.h1-title {
            color: white;
            font-size: 20px;
        }

        .milestones {
            background: white;
        }

        .content-overlay {
            background: rgba(49, 18, 75, 0.8);
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 1;
        }

        p.p-para {
            font-size: 21px;
            color: #ffffff;
            background: linear-gradient(to right, #fa9e1b, #8d4fff, #fa9e1b);
            padding: 10px;
            border-radius: 5px;
        }

        ul.points {

            text-align: initial;
        }

        .column {
            column-count: 2;
        }

        ul.points li {
            padding: 10px;
            color: #8f50fb;
            font-size: 16px;
            font-weight: 600;
        }

        ul.points li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
        }

        .milestone_icon img {
            width: auto;
            height: 50PX;
        }

        @media screen and (max-width:768px) {
            .column {
                column-count: 1;
            }

            ul.points {
                text-align: left !important;
            }
        }

        div.cstm-accordion .card-header {
            /* background: black; */
            font-size: 21px;
            color: #ffffff;
            background: white;
            padding: 10px 30px;
            border-radius: 5px;
            border-radius: 2px;
            border: none;
        }

        div.cstm-accordion .card-header a {
            color: #fa9e1b;
            font-size: 15px;
            display: block;
            text-align: center;
            font-weight: 500;
            text-transform: uppercase;
        }

        div.cstm-accordion .card {
            /* border: none !IMPORTANT; */
            margin-bottom: 10px;
            border: 1px solid #dadada;
        }
    </style>
    <div class="super_container">
        <!-- Header -->
        @include ('pages.include.topheader')
        <div class="about-header">
            <h1 class="about-title">Terms And Conditions</h1>
        </div>
        <!-- Intro -->
        <div class="intro">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12">
                        <div class="intro_content">
                            <div class="intro_title">Terms And Conditions</div>
                            <p class="intro_text">This User Agreement along with Terms Of Service (collectively, the "User Agreement") forms the terms and conditions for the use of services and products of TRAVOWEB Private Limited ("TRAVOWEB").

                            Any person ("User") who inquiries about or purchases any products or services of TRAVOWEB through its websites, mobile applications, sales persons, offices, call centers, branch offices, franchisees, agents etc. (all the aforesaid platforms collectively referred to as "Sales Channels") agree to be governed by this User Agreement. The websites and the mobile applications of  TRAVOWEB are collectively referred to as 'Website'.

                            Both User and TRAVOWEB are individually referred to as 'Party' and collectively referred to as 'Parties' to the User Agreement.

                            "Terms of Service" available on TRAVOWEB's website details out terms & conditions applicable on various services or products facilitated by TRAVOWEB. The User should refer to the relevant Terms of Service applicable for the given product or service as booked by the User. Such Terms of Service are binding on the User.

                         TRAVOWEB is not a direct supplier of products and services, or controlling the costs appropriate to any of the travel alternatives (Flights, accommodation services, car rentals, cruise packages). 
                            TRAVOWEB has no obligation regarding any additional or mentioned services provided or not provided by any other party. The tickets, vouchers, receipts and coupons are allotted suitable duties and terms and conditions of offer of providers and these terms. They are issued by us as operator as it were. We do not extend or guarantee for special airfare or discounted promotions. The following general terms and conditions shall apply, however regional circumstances and regulations, contractual obligations to suppliers, and matters relating to location, product type and supply logistics may cause these to be varied slightly for specific products, or specific destinations. 



                                <div id="demo" class="collapse">
                                    <ul class="n-ul">
                                        <li>Sales of services as contained in our publications are made by TRAVOWEB only as agent for the person, business or company providing the services. The standards of accommodation and other services chosen are based on various factors, which are generally accepted as indicative of a certain class.</li>
                                        <li>TRAVOWEB does not accept responsibility for any change in prices or variation of services as shown, and all services and prices are subject to change without notice.</li>
                                        <li>TRAVOWEB does not accept any liability of whatever nature for the acts, omissions or default, whether negligent or otherwise, of those service providers in connections with your convention pursuant to a contract between them and yourselves and over whom we have no direct control. TRAVOWEB do not accept liability in contract or in tort (actionable wrong) for any injury, damage, loss, delay, additional expenses or inconvenience caused directly or indirectly by force majeure or other events which are beyond our control, or which are not preventable by reasonable diligence on our part including, but not limited to war, civil disturbance, fire, floods, unusually severe weather, acts of God, acts of Government or of any other authorities, accidents to or failure of machinery or equipment or industrial action.</li>
                                        <li>It is essential that you check with us, or your travel agent, for any changes or variations to the information shown on our web sites. This will allow us to advise you, or your agent, of any such changes that we are aware of either before you book your holiday, or prior to your departure.</li>
                                    </ul>
                                </div>

                            </p>
                            <div class="button intro_button" style="position:relative;bottom: 0;left: 100px;">
                                <div class="button_bcg"></div><a data-toggle="collapse" href="#demo">Read More
                                    <span></span><span></span><span></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="stats">
                <div class="container">
                    <div class="row">
                        <div class="col text-center">
                            <div class="section_title">ELIGIBILITY TO USE</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1 text-center">
                            <p class="stats_text">The User must be atleast 18 years of age and must possess the legal authority to enter into an agreement so as become a User and use the services of TRAVOWEB.

                            Before using the Website, approaching any Sales Channels or procuring the services of TRAVOWEB, the Users shall compulsorily read and understand this User Agreement, and shall be deemed to have accepted this User Agreement as a binding document that governs User's dealings and transactions with TRAVOWEB. If the User does not agree with any part of this Agreement, then the User must not avail TRAVOWEB's services and must not access or approach the Sales Channels of TRAVOWEB.
                            All rights and liabilities of the User and TRAVOWEB with respect to any services or product facilitated by TRAVOWEB shall be restricted to the scope of this User Agreement.

                            </p>

                            <div id="accordion" class="cstm-accordion">

                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#collapseOne">
                                            Website
                                        </a>
                                    </div>
                                    <div id="collapseOne" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                The Website is meant to be used by bonafide User(s) for a lawful use. 
                                                TRAVOWEB has taken due care and responsibility to verify and check all information on our web sites as at the time of compilation, however, as this information is supplied by the relevant accommodation & tour/cruise operators in our brochures, TRAVOWEB Travel accepts no responsibility for any inaccuracy or misdescription contained in the publications.
                                                The User Agreement grants a limited, non-exclusive, non-transferable right to use this Website as expressly permitted in this User Agreement. The User agrees not to interrupt or attempt to interrupt the operation of the Website in any manner whatsoever. 

                                                Access to certain features of the Website may only be available to registered User(s). The process of registration, may require the User to answer certain questions or provide certain information that may or may not be personal in nature. Some such fields may be mandatory or optional. User represents and warrants that all information supplied to TRAVOWEB is true and accurate. 

                                                TRAVOWEB reserves the right, in its sole discretion, to terminate the access to the Website and the services offered on the same or any portion thereof at any time, without notice, for general maintenance or any other reason whatsoever. 

                                                TRAVOWEB will always make its best endeavors to ensure that the content on its websites or other sales channels are free of any virus or such other malwares. However, any data or information downloaded or otherwise obtained through the use of the Website or any other Sales Channel is done entirely at the User's own discretion and risk and they will be solely responsible for any damage to their computer systems or loss of data that may result from the download of such data or information. 

                                                TRAVOWEB reserves the right to periodically make improvements or changes in its Website at any time without any prior notice to the User. User(s) are requested to report any content on the Website which is deemed to be unlawful, objectionable, libelous, defamatory, obscene, harassing, invasive to privacy, abusive, fraudulent, against any religious beliefs, spam, or is violative of any applicable law to info@TRAVOWEBtravels.com  On receiving such report, TRAVOWEB reserves the right to investigate and/or take such action as the Company may deem appropriate.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                            Bookings By Travel Agents
                                        </a>
                                    </div>
                                    <div id="collapseTwo" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">Except with the prior registration with TRAVOWEB as B2B agents, priority partner or a franchisee, and explicit permission of TRAVOWEB to use the Website, all travel agents, tour operators, consolidators or aggregators ("Travel Agents") are barred from using this Website for any commercial or resale purpose. If any such bookings are detected, TRAVOWEB reserves the right, including without limitation, to cancel all such bookings immediately without any notice to such travel agents and also to withhold payments or any refunds thereto. TRAVOWEB shall not be held liable for any incidental loss or damage that may arise from the bookings made by any person through such Travel Agents. The liability in case of such cancellations shall be solely borne by such Travel Agents.

                                            All discounts and offers mentioned on the Website are applicable only to the User(s) of TRAVOWEB for legitimate bookings.

                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                            LIMITED LIBILITY OF TRAVOWEB
                                        </a>
                                    </div>
                                    <div id="collapseThree" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">TRAVOWEB  always acts as a facilitator by connecting the User with the respective service providers like airlines, hotels, bus operators etc. (collectively referred to as "Service Providers"). TRAVOWEB’S liability is limited to providing the User with a confirmed booking as selected by the User.
                                            Any issues or concerns faced by the User at the time of availing any such services shall be the sole responsibility of the Service Provider. TRAVOWEB  will have no liability with respect to the acts, omissions, errors, representations, warranties, breaches or negligence on part of any Service Provider.
                                            </p>
                                            <p>Unless explicitly committed by TRAVOWEB as a part of any product or service:</p>
                                            <ul class="n-ul">
                                                <li>TRAVOWEB assumes no liability for the standard of services as provided by the respective Service Providers. </li>
                                                <li>TRAVOWEB provides no guarantee with regard to their quality or fitness as represented.</li>
                                                <li>TRAVOWEB doesn't guarantee the availability of any services as listed by a Service Provider.</li>
                                            </ul>
                                            <p class="acc-p">By making a booking, User understands TRAVOWEB merely provides a technology platform for booking of services and products and the ultimate liability rests on the respective Service Provider and not TRAVOWEB. Thus the ultimate contract of service is between User and Service Provider.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse4">
                                            User’s Responsibility
                                        </a>
                                    </div>
                                    <div id="collapse4" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">Users are advised to check the description of the services and products carefully before making a booking. User(s) agree to be bound by all the conditions as contained in booking confirmation or as laid out in the confirmed booking voucher. These conditions are also to  be     read in consonance with the User Agreement. 

                                            If a User intends to make a booking on behalf of another person, it shall be the responsibility of the User to inform such person about the terms of this Agreement, including all rules and restrictions applicable thereto. 

                                            The User warrants that they will abide by all such additional procedures and guidelines, as modified from time to time, in connection with the use of the services. The User further warrants that they will comply with all applicable laws and regulations of the concerned jurisdiction regarding use of the services for each transaction. 

                                            The services are provided on an "as is" and "as available" basis. TRAVOWEB may change the features or functionality of the services being provided at any time, in its sole discretion, without any prior notice. TRAVOWEB expressly disclaims all warranties of any kind, whether express or implied, including, but not limited to the implied warranties of merchantability, reasonably fit for all purposes. No advice or information, whether oral or written, which the User obtains from TRAVOWEB or through the services opted shall create any warranty not expressly made herein or in the terms and conditions of the services. 

                                            User also authorizes TRAVOWEB representative to contact such user over phone, message and email. This consent shall supersede any preferences set by such User through national customer preference register (NCPR) or any other similar preferences.

                                            User further undertakes to check the PNR with the airlines for confirmed bookings.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse5">
                                            SECURITY AND ACCOUNT RELATED INFORMATION
                                        </a>
                                    </div>
                                    <div id="collapse5" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">While registering on the Website, the User will have to choose a password to access that User's account and User shall be solely responsible for maintaining the confidentiality of both the password and the account as well as for all activities on the account. It is the duty of the User to notify  TRAVOWEB immediately in writing of any unauthorized use of their password or account or any other breach of security. TRAVOWEB will not be liable for any loss that may be incurred by the User as a result of unauthorized use of the password or account, either with or without the User's knowledge. The User shall not use anyone else's account at any time. 

                                            User understands that any information that is provided to this Website may be read or intercepted by others due to any breach of security at the User's end.

                                            TRAVOWEB keeps all the data in relation to credit card, debit card, bank information etc. secured and in an encrypted form in compliance with the applicable laws and regulations. However, for cases of fraud detection, offering bookings on credit (finance) etc., TRAVOWEB may at times verify certain information of its Users like their credit score, as and when required. 

                                            TRAVOWEBadopts the best industry standard to secure the information as provided by the User. However, TRAVOWEB cannot guarantee that there will never be any security breach of its systems which may have an impact on User's information too. 

                                            The data of the User as available with TRAVOWEB may be shared with concerned law enforcement agencies for any lawful or investigation purpose without the consent of the User. 

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse6">
                                            FEES AND PAYMENT
                                        </a>
                                    </div>
                                    <div id="collapse6" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">In addition to the cost of booking as charged by the Service Providers, TRAVOWEB reserves the right to charge certain fees in the nature of convenience fees or service fees. TRAVOWEB further reserves the right to alter any and all fees from time to time. Any such additional fees, including fee towards any modifications thereof, will be displayed to the User before confirming the booking or collecting the payment from such User. 

                                            In cases of short charging of the booking amount, taxes, statutory fee, convenience fee etc., owing to any technical error or other reason,  TRAVOWEB shall reserve the right to deduct, charge or claim the balance amount from the User and the User shall pay such balance amount to TRAVOWEB. In cases where the short charge is claimed prior to the utilization of the booking,  TRAVOWEB will be at liberty to cancel such bookings if the amount is not paid before the utilization date. 

                                            Any increase in the price charged by  TRAVOWEB on account of change in rate of taxes or imposition of new taxes, levies by Government shall have to be borne by the User. Such imposition of taxes, levies may be without prior notice and could also be retrospective but will always be as per applicable law. 

                                            In the rare circumstance of a booking not getting confirmed for any reason whatsoever,  TRAVOWEB will process the refund of the booking amount paid by the User and intimate the User about the same.  TRAVOWEB is not under any obligation to provide an alternate booking in lieu of or to compensate or replace the unconfirmed booking. All subsequent bookings will be treated as new transactions. Any applicable refund will be processed as per the defined policies of the service provider and  TRAVOWEB as the case may be. 

                                            The User shall be completely responsible for all charges, fees, duties, taxes, and assessments arising out of the use of the service, as per the applicable laws 

                                            The User agrees and understands that all payments shall only be made to bank accounts of TRAVOWEB.  TRAVOWEB or its agents, representatives or employees shall never ask a customer to transfer money to any private account or to an account not held in the name of TRAVOWEB. The User agrees that if that user transfers any amount against any booking or transaction to any bank account that is not legitimately held by  TRAVOWEB or to any personal account of any person,  TRAVOWEB shall not be held liable for the same. User shall not hold any right to recover from  TRAVOWEB any amount which is transferred by the User to any third party. 

                                            The User will not share his personal sensitive information like credit/debit card number, CVV, OTP, card expiry date, user IDs, passwords etc. with any person including the agents, employees or representatives of TRAVOWEB. The User shall immediately inform  TRAVOWEB if such details are demanded by any of its agents' employees or representatives.  TRAVOWEB shall not be liable for any loss that the User incurs for sharing the aforesaid details. 

                                            Refunds, if any, on cancelled bookings will always be processed to the respective account or the banking instrument (credit card, wallet etc.) from which payment was made for that booking. 

                                            Booking(s) made by the User through  TRAVOWEB are subject to the applicable cancellation policy as set out on the booking page or as communicated to the customers in writing.

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse7">
                                            INSURANCE
                                        </a>
                                    </div>
                                    <div id="collapse7" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">Unless explicitly provided by TRAVOWEB in any specific service or deliverable, obtaining sufficient insurance coverage is the obligation of the User. In no case TRAVOWEB shall accept any claims arising out of such scenarios. 

                                            Insurance, if any provided as a part of the service or product by TRAVOWEB shall be as per the terms and conditions of the third-party insurance company. TRAVOWEB merely acts as a facilitator in connecting the User with insurance company. The User shall contact the insurance company directly for any claims or disputes. TRAVOWEB shall not be held liable in case of partial acceptance or denial of the claims by the insurance company.

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse8">
                                            OBLIGATION TO OBTAIN VISA
                                        </a>
                                    </div>
                                    <div id="collapse8" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">International bookings made through TRAVOWEB are subject to the requirements of visa including but not limited to transit visa, OK TO BOARD which are to be obtained by the User as per the requirement of their travel bookings and the requirements of the countries the User intends to visit or transit through. 

                                            TRAVOWEB is not responsible for any issues, including inability to travel, arising out of such visa requirements and is also not liable to refund any amount to the User for being unable to utilize the booking due to absence or denial of visa, irrespective whether or not the User has availed the services of TRAVOWEB for the visa process too. Refund, if any, will be as per the applicable terms of booking and cancellation policy.

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse9">
                                            FORCE MAJEURE
                                        </a>
                                    </div>
                                    <div id="collapse9" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">There can be exceptional circumstances where TRAVOWEB and / or the Service Providers may be unable to honor the confirmed bookings due to various reasons like act of God, labor unrest, insolvency, business exigencies, government decisions, terrorist activity, any operational and technical issues, route and flight cancellations etc. or any other reason beyond the control of TRAVOWEB. If TRAVOWEB has advance knowledge of any such situations where dishonor of bookings may happen, it will make its best efforts to provide similar alternative to the User or refund the booking amount after deducting applicable service charges, if supported and refunded by that respective service operators. The User agrees that TRAVOWEB being merely a facilitator of the services and products booked, cannot be held responsible for any such Force Majeure circumstance. The User has to contact the Service Provider directly for any further resolutions and refunds. 

                                            The User agrees that in the event of non-confirmation of booking due to any technical reasons (like network downtime, disconnection with third party platforms such as payment gateways, banks etc.) or any other similar failures, TRAVOWEB obligation shall be limited refunding the booking amount, if any, received from the customer. Such refund shall completely discharge TRAVOWEB from all liabilities with respect to that transaction. Additional liabilities, if any, shall be borne by the User. 

                                            In no event shall TRAVOWEB and be liable for any direct, indirect, punitive, incidental, special or consequential damages, and any other damages like damages for loss of use, data or profits, arising out of or in any way connected with the use or performance of the Website or any other Sales Channel. 

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse10">
                                            RIGHT TO REFUSE
                                        </a>
                                    </div>
                                    <div id="collapse10" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">  TRAVOWEB at its sole discretion reserves the right to not accept any booking without assigning any reason thereof.

                                             TRAVOWEB will not provide any service or share confirmed booking details till such time the complete consideration is received from the User.

                                                In addition to other remedies and recourse available to  TRAVOWEB under this User Agreement or under applicable law,  TRAVOWEB may limit the User's activity, warn other users of the User's actions, immediately suspend or terminate the User's registration, or refuse to provide the User with access to the Website if:
 

                                                <ul class="n-ul">
                                                    <li>The User is in breach of this User Agreement; or</li>
                                                    <li>TRAVOWEB is unable to verify or authenticate any information provided by the User; or</li>
                                                    <li> TRAVOWEB believes that the User's actions may infringe on any third-party rights or breach any applicable law or otherwise result in any liability for the User, other users of TRAVOWEB, or  TRAVOWEB itself.</li>
                                                </ul>


                                                Once a User has been suspended or terminated, such User shall not register or attempt to register with  TRAVOWEB with different credentials, or use the Website in any manner whatsoever until such User is reinstated by TRAVOWEB.  TRAVOWEB may at any time in its sole discretion reinstate suspended users.

                                                If a User breaches this User Agreement,  TRAVOWEB reserves the right to recover any amounts due to be paid by the User to TRAVOWEB, and to take appropriate legal action as it deems necessary.

                                                The User shall not write or send any content to  TRAVOWEB which is, or communicate with  TRAVOWEB using language or content which is:
                                                <ul class="n-ul">
                                                    <li>abusive, threatening, offensive, defamatory, coercive, obscene, belligerent, glorifying violence, vulgar, sexually explicit, pornographic, illicit or otherwise objectionable;</li>
                                                    <li>contrary to any applicable law;</li>
                                                    <li>violates third parties' intellectual property rights;</li>
                                                    <li>in breach of any other part of these terms and conditions of use.</li>
                                                </ul>
                                               If the User violates any of the aforesaid terms, TRAVOWEB shall be at liberty to take appropriate legal action against the User.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse11">
                                            RIGHT TO CANCEL
                                        </a>
                                    </div>
                                    <div id="collapse11" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">The User expressly undertakes to provide TRAVOWEB with correct and valid information while making use of the Website under this User Agreement, and not to make any misrepresentation of facts. Any default on part of the User would disentitle the User from availing the services from TRAVOWEB. 

                                            In case TRAVOWEB discovers or has reasons to believe at any time during or after receiving a request for services from the User that the request for services is either unauthorized or the information provided by the User or any of the travelers is not correct or that any fact has been misrepresented by that User, TRAVOWEB shall be entitled to appropriate legal remedies against the User, including cancellation of the bookings, without any prior intimation to the User. In such an event, TRAVOWEB shall not be responsible or liable for any loss or damage that may be caused to the User or any other person in the booking, as a consequence of such cancellation of booking or services. 

                                            If any judicial, quasi-judicial, investigation agency, government authority approaches TRAVOWEB to cancel any booking, TRAVOWEB will cancel the same without approaching the concerned User whose booking has been cancelled. 

                                            The User shall not hold TRAVOWEB responsible for any loss or damage arising out of measures taken by TRAVOWEB for safeguarding its own interest and that of its genuine customers. This would also include TRAVOWEB denying or cancelling any bookings on account of suspected fraud transactions.

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse12">
                                            INDEMNIFICATION
                                        </a>
                                    </div>
                                    <div id="collapse12" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">The User agrees to indemnify, defend and hold harmless TRAVOWEB, its affiliates and their respective officers, directors, lawful successors and assigns from and against any and all losses, liabilities, claims, damages, costs and expenses (including legal fees and disbursements in connection therewith and interest chargeable thereon) asserted against or incurred by such indemnified persons, that arise out of, result from, or may be payable by virtue of, any breach of any representation or warranty provided by the User, or non-performance of any covenant by the User. 

                                            The User shall be solely liable for any breach of any country specific rules and regulations or general code of conduct and TRAVOWEB cannot be held responsible for the same.

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse13">
                                           MISCLLENAOUS
                                        </a>
                                    </div>
                                    <div id="collapse13" class="collapse acc-content" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="acc-p">SEVERABILITY: If any provision of this User Agreement is determined to be invalid or unenforceable in whole or in part, such invalidity or unenforceability shall attach only to such provision or part of such provision and the remaining part of such provision and all other provisions of this User Agreement shall continue to be in full force and effect. 
											 <p class="acc-p">
                                            JURISDICTION: This Agreement is subject to interpretation as per the laws of Australia, and the parties shall refer any unresolved disputes to the exclusive jurisdiction of courts in Gundagai, NSW.  </p>

                                            <p class="acc-p"> GOVERNING LAW: TRAVOWEB shall follow and be governed as per all the current prevailing laws of Australia including but not limited to Australian Consumer Law.</p>
 
                                            <p class="acc-p"> AMENDMENT TO THE USER AGREEMENT: TRAVOWEB reserves the right to change the User Agreement from time to time. The User is responsible for regularly reviewing the User Agreement. </p>

                                            <p class="acc-p"> CONFIDENTIALITY: Any information which is specifically mentioned by TRAVOWEB as confidential shall be maintained confidentially by the User and shall not be disclosed unless as required by law or to serve the purpose of this User Agreement and the obligations of both the parties herein. 
											</p>
                                            <p class="acc-p"> PRIVACY POLICY: User shall also refer to TRAVOWEB's Privacy Policy available on TRAVOWEB's website which governs use of the Websites. By using the Website, User agrees to the terms of the Privacy Policy and accordingly consents to the use of the User’s personal information by TRAVOWEB and its affiliates in accordance with the terms of the Privacy Policy.</p>

                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="row">
                                <div class="col text-center">
                                    <div class="section_title" style="margin: 30px 0;">FLIGHT TICKETS</div>
                                </div>
                            </div>

                            <div id="accordion2" class="cstm-accordion">
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#c1">
                                            TERMS OF THE AIRLINES
                                        </a>
                                    </div>
                                    <div id="c1" class="collapse acc-content" data-parent="#accordion2">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                The airline tickets available through the Website are subject to the terms & conditions of the concerned airline, including but not limited to cancellation and refund policies.

                                                TRAVOWEB merely acts as a facilitator to enable the User to book a flight ticket. The contract of service for utilization of the flight is always between the User and the concerned airline.

                                                Airlines retain the right to reschedule flight times, route, change or cancel flights or itineraries independent of and without prior intimation to TRAVOWEB. As a facilitator TRAVOWEB has no control or authority over the logistics of the airlines and therefore is not liable for any loss, direct or incidental, that a User may incur due to such change or cancellation of a flight.

                                                Different tickets on the same airline may carry different restrictions or include different services and price.

                                                The baggage allowance on given fare is as per the terms decided by the airline, and TRAVOWEB has no role to play in the same. Some of the fares shown in the booking flow are "hand baggage fares" which do not entitle the User for free check in baggage and therefore the User will be required to pay separately for check in baggage. The prices for adding check-in baggage to a booking may vary from airline to airline. The User is advised to contact the airlines for detailed costs.


                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#c2">
                                            CODE SHARE
                                        </a>
                                    </div>
                                    <div id="c2" class="collapse acc-content" data-parent="#accordion2">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                Some airlines enter into "code share" agreements with other Airlines. This means that on certain routes, the airline carrier selling or marketing the flight ticker does not fly its own aircraft to that destination. Instead, it contracts or partners with another airline to fly to that destination. The partner airline is listed as "operated by" in the booking flow.

                                                If your flight is a code share, it will be disclosed to you in the booking process and prior to payment.

                                                TRAVOWEB will disclose any such code-share arrangements to the User, only when the ticketing airline discloses it to TRAVOWEB in the first place.

                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#c3">
                                            PRICING
                                        </a>
                                    </div>
                                    <div id="c3" class="collapse acc-content" data-parent="#accordion2">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                The total price displayed on the Website on the payment page usually includes base fare, applicable government taxes and convenience fee. Users are required to pay the entire amount prior to the confirmation of their booking(s). In the event the User does not pay the entire amount, TRAVOWEB reserves its right to cancel the booking. User agrees to pay all statutory taxes, surcharges and fees, as applicable on the date of travel. 

                                                To avail infant fares, the age of the child must be under 24 months throughout the entire itinerary. This includes both onward and return journeys. If the infant is 24 months or above on the return journey, User will be required to make a separate booking using a child fare. Any infants or children must be accompanied by an adult as per the terms of the airlines. 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#c4">
                                            TRAVEL DOCUMENTS
                                        </a>
                                    </div>
                                    <div id="c4" class="collapse acc-content" data-parent="#accordion2">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                It shall be the sole responsibility of the User to ensure they are in possession of valid travel documents such as identity proof, passport, visa (including transit visa) etc. to undertake the travel. User agrees that in case of inability to travel for not carrying valid travel documents, TRAVOWEB shall in no way be held liable.

                                                User understands that the information (if any) provided by TRAVOWEB regarding the travel documents is only advisory in nature and can't be considered conclusive. The User shall ensure checking the requirements of travel with the respective airlines of the respective jurisdictions the User may transit through or choose to visit.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#c5">
                                            CHECK-IN TERMS
                                        </a>
                                    </div>
                                    <div id="c5" class="collapse acc-content" data-parent="#accordion2">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                User should check with the airlines directly regarding the check-in timings. Usually, check-in begins 2 hours before departure for domestic flights, and 3 hours before departure for international flights.

                                                User should carry valid identification proofs, passport, age proofs as may be required to prove the identity, nationality and age of the passengers travelling on a ticket, including infants.


                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#c6">
                                            USE OF FLIGHT SEGMENTS
                                        </a>
                                    </div>
                                    <div id="c6" class="collapse acc-content" data-parent="#accordion2">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                In the event User does not embark on the onward journey, the entire PNR pertaining to that booking shall be automatically cancelled by the airline. In such a scenario TRAVOWEB has no control in the said process nor will be obligated to provide alternate bookings to the User. The cancellation penalty in such an event shall be as per the applicable airline rules.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#c7">
                                            CHANGES TO EXISTING BOOKING
                                        </a>
                                    </div>
                                    <div id="c7" class="collapse acc-content" data-parent="#accordion2">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                Any changes that are made to any existing booking shall be subject to certain charges levied by the respective airline, apart from the service fee charged by TRAVOWEB.

                                                The User shall be obligated to pay applicable charges in the event of any alteration or modification to an existing booking. However, depending on the airline's policy and fare class, charges for changes or modifications to existing bookings may vary.


                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#c8">
                                            REFUND
                                        </a>
                                    </div>
                                    <div id="c8" class="collapse acc-content" data-parent="#accordion2">
                                        <div class="card-body">
                                            <p class="acc-p">
                                               Refunds will be processed as per the airline fare rules and cancellation policy. Such refunds shall be subject to TRAVOWEB receiving the same from the airlines. However, the convenience fee paid to TRAVOWEB paid at the time of booking is a non-refundable fee. 

                                                All cancellations made directly with the airline need to be intimated to TRAVOWEB, in order to initiate the process of refund. The processing time for refunds may vary depending on the mode of payment, bank etc. The refund shall be processed after deducting the TRAVOWEB service fee which is independent of the convenience fee as mentioned above. 

                                                The refund will be credited to the same account from which the payment was made. For example, if the User used a credit card, TRAVOWEB will make an appropriate charge reversal to the same credit card; like-wise if the User used a debit card, TRAVOWEB will credit the money to the same debit card. 

                                                In the event of cancellation and refund of partially utilized tickets, upfront discount and promo code discount availed at the time of booking would be deducted from the refund amount. 

                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col text-center">
                                    <div class="section_title" style="margin: 30px 0;">HOTELS</div>
                                </div>
                            </div>
                            <div id="accordion3" class="cstm-accordion">
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#a1">
                                            ROLE OF TRAVOWEB AND LIMITATION OF LIABILITY OF TRAVOWEB
                                        </a>
                                    </div>
                                    <div id="a1" class="collapse acc-content" data-parent="#accordion3">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                TRAVOWEB acts as a facilitator and merely provides an online platform to the User to select and book a particular hotel. Hotels in this context include all categories of accommodations such as hotels, home-stays, bed and breakfast stays, farm-houses and any other alternate accommodations. 

                                                All the information pertaining to the hotel including the category of the hotel, images, room type, amenities and facilities available at the hotel are as per the information provided by the hotel to TRAVOWEB. This information is for reference only. Any discrepancy that may exist between the website pictures and actual settings of the hotel shall be raised by the User with the hotel directly, and shall be resolved between the User and hotel. TRAVOWEB will have no responsibility in that process of resolution, and shall not take any liability for such discrepancies. 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#a2">
                                            INFORMATION FROM THE HOTEL AND THE TERMS OF THE HOTEL
                                        </a>
                                    </div>
                                    <div id="a2" class="collapse acc-content" data-parent="#accordion3">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                The hotel booking voucher which TRAVOWEB issues to a User is solely based on the information provided or updated by the hotel regarding the inventory availability. In no circumstances can TRAVOWEB be held liable for failure on part of a hotel to accommodate the User with a confirmed booking, the standard of service or any insufficiency in the services, or any other service related issues at the hotel. The liability of TRAVOWEB in case of denial of check-in by a hotel for any reason what-so-ever including over-booking, system or technical errors, or unavailability of rooms etc., will be limited to either providing a similar alternate accommodation at the discretion of TRAVOWEB (subject to availability at that time), or refunding the booking amount (to the extent paid) to the User. Any other service related issues should be directly resolved by the User with the hotel. 

                                                Hotels reserves the sole right of admission and TRAVOWEB has no say whatsoever in admission or denial for admission by the hotel. Unmarried or unrelated couples may not be allowed to check-in by some hotels as per their policies. Similarly, accommodation may be denied to guests posing as a couple if suitable proof of identification is not presented at the time check-in. Some hotels may also not allow local residents to check-in as guests. TRAVOWEB will not be responsible for any check-in denied by the hotel due to the aforesaid reasons or any other reason not under the control of TRAVOWEB. No refund would be applicable in case the hotel denies check-in under such circumstances. 

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#a3">
                                            RESPONSIBILITIES OF THE USER
                                        </a>
                                    </div>
                                    <div id="a3" class="collapse acc-content" data-parent="#accordion3">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                The User would be liable to make good any damage(s) caused by any act of him/ her/ or their accompanying guests (willful/negligent) to the property of the hotel in any manner whatsoever. The extent and the amount of the damage so caused would be determined by the concerned hotel. TRAVOWEB would not, in any way, intervene in the same. 

                                                The primary guest must be at least 18 years old to be able to check into the hotel. 

                                                The User has to be in possession of a valid identity proof and address proof, at the time of check-in. The hotel shall be within its rights to deny check-in to a User if a valid identity proof is not presented at the time of check-in. 

                                                Check-in time, check-out time, and any changes in those timings, will be as per hotel policy & terms. Early check-in or late check-out request is subject to availability and the hotel may charge an additional fee for providing such services. 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#a4">
                                            ADDITIONAL CHARGES BY THE HOTEL
                                        </a>
                                    </div>
                                    <div id="a4" class="collapse acc-content" data-parent="#accordion3">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                The booking amount paid by the User is only for stay at the hotel. Some bookings may include breakfast and/ or meals as confirmed at the time of booking. Any other services utilized by the User at the hotel, including laundry, room service, internet, telephone, extra food, drinks, beverages etc. shall be paid by the User directly to the hotel. 

                                                Hotels may charge a mandatory meal surcharge on festive periods like Christmas, New Year's Eve or other festivals as decided by the hotel. All additional charges (including mandatory meal surcharges) need to be cleared directly at the hotel. TRAVOWEB will have no control over waiving the same. 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#a5">
                                            PAYMENT FOR BOOKINGS AND ANY ADDITIONAL PAYMENTS
                                        </a>
                                    </div>
                                    <div id="a5" class="collapse acc-content" data-parent="#accordion3">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                Booking of a hotel can either be "Prepaid", or "Pay at hotel" as per the options made available by a hotel on the Website of TRAVOWEB. 

                                                In "Prepaid" model, the total booking amount is paid by the User at the time of booking itself. Such total booking amount includes the hotel reservation rate, taxes, service fees as may be charged on behalf of the actual service provider, and any additional booking fee or convenience fee charged by TRAVOWEB. 

                                                At the hotel's or TRAVOWEB's sole discretion on case to case basis, the User may also be provided with an option to make a part payment to TRAVOWEB at the time of confirmation of a booking. The balance booking amount shall be paid as per the terms of the bookings. For security purposes, the User must provide TRAVOWEB with correct credit or debit card details. TRAVOWEB may cancel the booking at its sole discretion in case such bank or credit card details as provided by the User are found incorrect. 

                                                Some banks and card issuing companies charge their account holders a transaction fee when the card issuer and the merchant location (as defined by the card brand, e.g. Visa, MasterCard, American Express) are in different countries. If a User has any questions about the fees or any exchange rate applied, they may contact their bank or the card issuing company through which payment was made. 

                                                Some accommodation suppliers may require User and/or the other persons, on behalf of whom the booking is made, to present a credit card or cash deposit upon check-in to cover additional expenses that may be incurred during their stay. Such deposit is unrelated to any payment received by TRAVOWEB and solely at the behest of the Hotel. 

                                                In "Pay at hotel" model, the concerned hotel will collect the entire payment against the booking at the time of check-in. In case of international bookings, the payment will be charged in local currency or in any other currency, as decided by the hotel. For security purposes, the User must provide TRAVOWEB with correct credit or debit card details. TRAVOWEB may cancel the booking at its sole discretion in case such bank or credit card details as provided by the User are found incorrect. 

                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col text-center">
                                    <div class="section_title" style="margin: 30px 0;">INTERNATIONAL AND DOMESTIC TOURS
                                    </div>
                                    <p style="margin-bottom: 30px">"Independent Contractors" means hotelier or hotel owner, owner of any airlines or shipping company or railway ferryboat owner, coach owner, or any other person or organization which has been selected to render services to the User. 

                                    "International Tour" means a tour operated by TRAVOWEB outside of India and includes, but is not limited to, brochure tours, special tours, packages, cruise, sightseeing, carnival, cosmos, star cruise etc. 

                                    "Tour Cost" means total booking cost of all the services booked by the User for his outbound tour. 

                                    "Booking Fees" means a non-refundable fee or token money deposited at the time of booking by the User. 

                                    "Brochure" means catalogue, leaflet, e-mail or any other document containing the details about the itinerary and activities on an outbound tour. 

                                    "Infant" means a person below the age of 2 years and child means a person above the age of 2 years and below the age of 12 years. 

                                </div>
                            </div>
                            <div id="accordion4" class="cstm-accordion">
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#b1">
                                            ROLE OF TRAVOWEB AND LIMITATION OF LIABILITY
                                        </a>
                                    </div>
                                    <div id="b1" class="collapse acc-content" data-parent="#accordion4">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                TRAVOWEB does not control or operate any airline, shipping company, coach, hotel, transport vehicles, restaurant, kitchen caravan or any other facility or service mentioned in the Brochure. 

                                                TRAVOWEB shall not be responsible for any delay, improper service and standard of service provided by any service provider or any Independent Contractor, or for any injury, death, loss or damage which is caused by the act or default of any hotel, airlines, shipping companies, cruise, coach owners, tour operators who are TRAVOWEB's independent contractors. 

                                                TRAVOWEB shall not be responsible for any act or actions of co-travelers, co-passengers which may result in injury, damage to the life or limb or property of the User, or which may lead to interference in enjoying or availing the services provided on the outbound tour. 

                                                TRAVOWEB being merely a facilitator , shall not be liable for the following: 


                                                <ul class="n-ul">
                                                    <li>Personal injury, sickness, accident, death, loss, delay, discomfort, increased expenses, incidental, consequential loss and/or damage or any kind of theft howsoever caused to the User or any person travelling with them.</li>
                                                    <li>Any act, omission, default of any travel agent or third party or any other person or by any servant or agent employed by them who may be engaged or concerned in the provision of accommodation, refreshment, carriage facility or service for the User or for any person traveling with him /her.</li>
                                                    <li>The temporary or permanent loss of, or damage to, baggage or personal belongings howsoever caused including willful negligence on the part of any person.</li>
                                                    <li>Any delay made in delivery of the service by the concerned service providers.</li>
                                                    <li>Failure on the part of airline to accommodate passengers despite having confirmed tickets for any reason whatsoever including overbooking, change of route etc. or failure on part of hotel to allow check-in despite confirmed booking for whatever reason.
                                                    </li>
                                                    <li>Any claims of any dispute with the tour manager.</li>
                                                    <li>Any claim arising due to delay at the airport and if the User has to wait at the airport or at the hotel for check-in due to any technical snag or any other reason not attributable to TRAVOWEB, TRAVOWEB shall not be liable for making any arrangements, including but not limited to food or any hotel arrangement in case of such delays.</li>
                                                    <li>Any damages caused to the User due to reasons beyond the control of TRAVOWEB.</li>
                                                </ul>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#b2">
                                            BROCHURE
                                        </a>
                                    </div>
                                    <div id="b2" class="collapse acc-content" data-parent="#accordion4">
                                        <div class="card-body">
                                            <p class="acc-p">
                                               All information given in the Brochure is based on the information available at the time of publication. TRAVOWEB reserves the right to change any information contained in the Brochure before or after booking the outbound tour due to any event(s) beyond the control of TRAVOWEB. 

                                                In case TRAVOWEB becomes aware of any change before the departure for an Outbound Tour, TRAVOWEB will take all reasonable steps to notify the User before the departure for that Outbound Tour. In the event of TRAVOWEB becoming aware of the changes post the departure for the International Tour, the TRAVOWEB  tour manager or local representative will inform the User about the change. 

                                                Apart from the instant Terms of Service and the User Agreement, some specific terms and conditions are mentioned against the respective INTERNATIONAL Tour in the Website as well as email communication and confirmed booking vouchers. Those terms and conditions also form part of the TRAVOWEB User Agreement & Terms Of Service and are to be read in consonance with the booking terms & conditions. 

                                                No claim of the User against any change in the Brochure regarding the Outbound Tour shall be entertained if such changes are due to reasons beyond the control of TRAVOWEB. 

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#b3">
                                            CANCELLATION CHARGES
                                        </a>
                                    </div>
                                    <div id="b3" class="collapse acc-content" data-parent="#accordion4">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                If the Outbound Tour is cancelled for any reason what-so-ever, including cases of visa rejection or voluntary cancellation by the User the cancellation charges as mentioned in the brochure or the itinerary will apply. In the absence of any such information, the following cancellation charges will apply: 

                                                <table class="table" style="text-align: left">
                                                    <thead>
                                                        <tr>
                                                            <th>Time period in which the cancellation is made</th>
                                                            <th>Charges</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>7 days or more, prior to departure</td>
                                                            <td>10% of Tour Cost </td>

                                                        </tr>
                                                        <tr>
                                                            <td>Between 7 and 3 days of departure</td>
                                                            <td>50% of Tour Cost</td>

                                                        </tr>
                                                        <tr>
                                                            <td>Within 3 days prior to date of departure, or no-show for any reason what-so-ever</td>
                                                            <td>100% of Tour Cost</td>

                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#b4">
                                            PAYMENT OF THE TOUR COST
                                        </a>
                                    </div>
                                    <div id="b4" class="collapse acc-content" data-parent="#accordion4">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                Payment shall be made as per the payment schedule provided on the brochure, Email or any other documented information as informed at the time of booking. 

                                                Payment can be made either online or by way of DD/cheque as specified to the User. 

                                                If any cheque towards the payment is dishonored, TRAVOWEB, without notice to the User, shall be entitled to cancel the entire booking without any liability and take recourse to appropriate legal remedy. 

                                                Final documents and airline tickets will be shared with the User only after the balance payment is made to TRAVOWEB by the User. PAN Card copy will be required when payments amounting to Rs.25,000/- or above are made by cash. 

                                                Rate of exchange taken for computing the cost of tour may vary due to changes in currency conversion rates. In the event of increase in the rate of exchange considered, the cost of tour may be amended and User shall pay any incremental tour cost. 

                                                TRAVOWEB will only provide a consolidated invoice of the entire tour cost. TRAVOWEB  is not obligated in any circumstance to provide break-up of the invoice for each of the service included in the package. 

                                                A transfer from one tour to another prior to the departure of the originally booked tour will be treated as a cancellation of the original tour and would attract the cancellation charges as stated hereunder and a fresh booking would have to be made at the prevalent rates. 


                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#b5">
                                            REFUNDS
                                        </a>
                                    </div>
                                    <div id="b5" class="collapse acc-content" data-parent="#accordion4">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                TRAVOWEB is only a facilitator and any refund for any service(s) which are not delivered by the service provider /Independent Contractors or for any reason for which the User is entitled for a refund is subject to TRAVOWEB receiving the amount from the said service provider. User acknowledges that TRAVOWEB shall not be held liable for any delay in refund or non-refund of the amount from the respective service provider or Independent Contractors of TRAVOWEB. In such events the User shall directly approach the service provider for any claims. 

                                                In case the User makes any changes in their accommodation while on the Outbound Tour, TRAVOWEB shall not refund or pay compensation in any manner whatsoever. The User would also be liable to pay any additional sum that is required to be paid consequent to the aforesaid changes made in the accommodation. 

                                                The refund for the foreign exchange components of the outbound tour will be refunded in INR only and will be at the prevailing day's rate of exchange on the date of refund. 

                                                In the event of any delay in the refund beyond the period specified herein, the entire liability of TRAVOWEB shall be refund of the said amount with interest calculated at the applicable bank rate till the date the refund is made. 

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#b6">
                                            AMENDMENTS
                                        </a>
                                    </div>
                                    <div id="b6" class="collapse acc-content" data-parent="#accordion4">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                In the event of TRAVOWEB amends or alters any tour or holiday after such a tour or holiday has been booked, the User shall have the option to continue with the tour or holiday as amended/ altered, or to accept any alternative tour or holiday which TRAVOWEB may offer. 

                                                In either of these cases above, the User shall not be entitled to nor TRAVOWEB be liable to the User for any damage, additional expenses or consequential loss suffered by the User. TRAVOWEB 's liability in such cases shall be only to refund the amount of the cost of the said tour to the User (after deducting the actual expenses incurred by TRAVOWEB  for the booking like visa, insurance premium, POE [what's this?] charges, and other overheads as applicable from case to case, without any interest on the same. 

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#b7">
                                            SITE COPYRIGHT
                                        </a>
                                    </div>
                                    <div id="b7" class="collapse acc-content" data-parent="#accordion4">
                                        <div class="card-body">
                                            <p class="acc-p">
                                                The copyright on all original/custom artwork, maps, navigation elements, presentation methods and design elements included in our web sites is held exclusively by travoweb. Copyright on all descriptive text is held by travoweb. Copyright on selected photographic images is also held by Travoweb.
                                                Photographic images are reproduced with the permission of TRAVOWEB and the product and/or service providers represented on the web site, or as shown where necessary to satisfy the copyright obligations associated with their electronic reproduction.
                                                Where joint copyright is held over proprietary text or images, TRAVOWEB reserves the right to pursue all copyright infringements on behalf of the primary copyright holder. Where the copyright for a photographic image is held exclusively by a third party under licence, TRAVOWEB Travel reserves the right to vigorously protect the copyright of those images on behalf of the third party.
                                                All HTML files used in travoweb web sites are declared to be proprietary software product developed for use by TRAVOWEB only, and as such are subject to international intellectual property conventions.
                                                All custom graphics used are declared to be works of art created for use by TRAVOWEB only, and as such are subject to international intellectual property conventions. Image files, HTML documents, and text files are not to be relocated to another server, or duplicated for commercial purposes without the express written permission of TRAVOWEB.
                                                TRAVOWEB, trading under its subsidiary business names, is a member of various Australian Regional Tourism Authorities, and as such exercises the right to reproduce images made available by these various organizations to their members for the purpose of generic and/or specific tourism promotion.
                                                Please contact us if you have any further queries about our security, privacy or data handling procedures.


                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <!-- Footer -->

            @include ('pages.include.footer')
            @include ('pages.include.copyright')
        </div>
    </div>
</body>

</html>