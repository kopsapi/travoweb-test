@include('pages.include.header')<!-- Loader -->
<style>

    .over-img {
        position: relative;
        /* bottom: -8px; */
        top: 15%;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
        transition: all .5s ease;
        left: 50%;
        transform: translate(-50%,-50%);
        text-align: center;
        transition-delay: .5s;
    }
    .overlay {
        position: fixed;
        overflow: hidden !important;
        z-index: 999;
        top: 100%;
        left: 0;
        /* bottom: 0; */
        /* right: 0; */
        width: 100%;
        height: 0%;
        background-color: rgb(0,0,0);
        background-color: rgba(0,0,0, 0.8);
        overflow-x: hidden;
        transition: 1s !important;
        display: block;
    }
    .overlay-content {
        position: relative;
        top: 24%;
        left: 50%;
        transform: translate(-50%,-50%);
        text-align: center;
    }
    .over-img {
        position: absolute;

        top: 450px;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
        transition: all .5s ease;
        left: 50%;
        transform: translate(-50%,-50%);
        text-align: center;
        transition-delay: .5s;
    }
    .overlay-content p {
        color: white;
        font-size: 17px;
    }
    .overlay-content h3 {
        color: white;
        font-family: inherit;
        font-size: 25px;
    }
    a.closebtn {
        color: white;
        font-size: 40px;
        font-weight: bolder;
        text-decoration: none;
        right: 30px;
        position: absolute;
    }
    @keyframes fadein {
        from {bottom: 0; opacity: 0;}
        to {  bottom: -492px; opacity: 1;}
    }
</style>
<style>
    .loader {
        display: block;
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: #CCEAF7 url('{{ asset('assets/images/flight-loader.gif')}}') no-repeat center center;
        text-align: center;
        color: #999;
        opacity: 0.9;
    }
</style>
<style>
    .invoice-content {
        padding: 50px 70px;
        border: 1px solid #DDD;
        box-shadow: 0px 0px 17px 1px rgba(0, 0, 0, 0.1);
        margin-bottom: 50px;
    }

    .e-ticket {
        color: #00206A;
        font-weight: 600;
        margin-bottom: 20px;
    }

    .e-ticket .booking-id {
        margin-bottom: 5px;
    }

    .e-ticket h4 {
        color: #fa9e1b;
        font-weight: 600;
    }

    .onward-icon i {
        font-size: 40px;
        color: #fa9e1b;
    }

    .onward-icon {
        float: left;
        width: 50px;
    }

    .onward-flight-details {
        margin-left: 50px;
    }

    .onward-flight {
        margin-bottom: 30px;
        padding-bottom: 10px;
        border-bottom: 1px solid #fa9e1b;
    }

    .onward-flight-details h5 {
        margin-bottom: 5px;
        color: #333;
        font-weight: 600;
    }

    .onward-flight-details h4 {
        color: #00206A;
        font-weight: 600;
    }

    .onward-flight-no {
        text-align: right;
    }

    .onward-flight-no h5 {
        margin-bottom: 5px;
        color: #333;
        font-weight: 600;
    }

    .onward-flight-no h4 {
        color: #00206A;
        font-weight: 700;
    }

    .flight-logo {
        float: left;
        width: 50px;
    }

    .flight-name-no {
        margin-left: 50px;
    }

    .flight-name-no h4 {
        color: #00206A;
    }

    .flight-ref {
        text-align: right;
    }

    .flight-ref a {
        background: green;
        border: 1px solid green;
        color: #FFF;
        padding: 6px 20px;
        font-size: 16px;
        border-radius: 20px;
    }

    .flight-non-ref {
        text-align: right;
    }

    .flight-non-ref a {
        background: red;
        border: 1px solid red;
        color: #FFF;
        padding: 6px 20px;
        font-size: 16px;
        border-radius: 20px;
    }

    .invoice-flight-name {
        margin-bottom: 30px;
    }

    .invoice-flight-details {
        margin-bottom: 20px;
        padding-bottom: 20px;
        border-bottom: 1px solid #fa9e1b;
    }

    .invoice-flight-details .flight-and-time {
        color: #333;
    }

    .invoice-flight-details .flight-and-time .day {
        font-size: 16px;
        font-weight: 600;
        margin-bottom: 5px;
    }

    .invoice-flight-details .flight-and-time .city-time {
        font-size: 20px;
        margin-bottom: 5px;
    }

    .invoice-flight-details .flight-and-time .airport {
        font-weight: 600;
    }

    .invoice-flight-duration {
        text-align: center;
    }

    .invoice-flight-duration .clock-icon {
        margin-bottom: 8px;
    }

    .invoice-flight-duration .clock-icon i {
        font-size: 25px;
        color: #fa9e1b;
    }

    .invoice-flight-duration .hours {
        font-size: 16px;
        margin-bottom: 3px;
        color: #00206A;
        font-weight: 600;
    }

    .invoice-flight-duration .invoice-flight-class {
        font-size: 16px;
        color: #00206A;
        font-weight: 600;
    }

    .traveller-details {
        margin-bottom: 30px;
    }

    .traveller-details .traveller {
        color: #fa9e1b;
        font-weight: 600;
        text-transform: uppercase;
        margin-bottom: 10px;
    }

    .traveller-details .ticket-id {
        color: #fa9e1b;
        font-weight: 600;
        text-transform: uppercase;
        text-align: right;
        margin-bottom: 10px;
    }

    .traveller-details .traveller-name {
        font-size: 18px;
        color: #00206A;
        font-weight: 600;
    }

    .traveller-details .traveller-ticket-id {
        font-size: 18px;
        color: #00206A;
        font-weight: 600;
        text-align: right;
    }

    .invoice-important {
        background: #EEE;
        box-shadow: 0px 0px 17px 1px rgba(0, 0, 0, 0.1);
        padding: 20px 30px;
        border: 1px solid #DDD;
        border-radius: 5px;
        margin-bottom: 30px;
    }

    .invoice-important .heading {
        font-size: 22px;
        color: #fa9e1b;
        font-weight: 600;
        text-align: center;
        margin-bottom: 10px;
        text-transform: uppercase;
    }

    .invoice-important ul li:before {
        content: "\f0da";
        font-family: FontAwesome;
        font-style: normal;
        font-weight: normal;
        text-decoration: inherit;
        color: #fa9e1b;
        font-size: 18px;
        position: absolute;
        top: 0px;
        left: 0;
    }

    .invoice-important ul li {
        font-size: 14px;
        text-align: justify;
        color: #333;
        line-height: 26px;
        position: relative;
        padding-left: 17px;
        margin-bottom: 10px;
        font-weight: 600;
    }

    .baggage-policy {
        margin-bottom: 30px;
    }

    .baggage-policy .heading {
        font-size: 22px;
        color: #fa9e1b;
        font-weight: 600;
        margin-bottom: 10px;
    }

    .baggage-policy .heading i {
        margin-right: 10px;
    }

    .baggage-policy table th {
        white-space: nowrap;
    }

    .baggage-policy table th, .baggage-policy table td {
        background: #FFF4e7;
        color: #00206A;
        border: 1px solid #00206A;
    }

    .baggage-policy table td p {
        margin-bottom: 5px;
    }

    .baggage-policy table td, .baggage-policy table td p {
        color: #333;
        font-weight: 600;
    }

    .cancellation-policy {
        margin-bottom: 30px;
    }

    .cancellation-policy .heading {
        font-size: 22px;
        color: #fa9e1b;
        font-weight: 600;
        margin-bottom: 10px;
    }

    .cancellation-policy .heading i {
        margin-right: 10px;
    }

    .cancellation-policy table th {
        white-space: nowrap;
    }

    .cancellation-policy table th, .cancellation-policy table td {
        background: #FFF4e7;
        color: #00206A;
        border: 1px solid #00206A;
    }

    .cancellation-policy table td p {
        margin-bottom: 5px;
    }

    .cancellation-policy table td, .cancellation-policy table td p {
        color: #333;
        font-weight: 600;
    }

    .fare-payment-policy {
        margin-bottom: 30px;
    }

    .fare-payment-policy .heading {
        font-size: 22px;
        color: #fa9e1b;
        font-weight: 600;
        margin-bottom: 10px;
    }

    .fare-payment-policy .fare-left {
        font-size: 15px;
        color: #333;
        font-weight: 500;
        margin-bottom: 5px;
        font-weight: 600;
    }

    .fare-payment-policy .fare-right {
        font-size: 16px;
        color: #00206A;
        font-weight: 600;
        margin-bottom: 7px;
        text-align: right;
    }

    .fare-payment-policy .fare-right i {
        font-size: 18px;
        color: #fa9e1b;
    }

    .total-fare {
        padding-top: 10px;
        border-top: 1px solid #fa9e1b;
        border-bottom: 1px solid #fa9e1b;
        padding-bottom: 4px;
        padding-left: 10px;
        padding-right: 10px;
    }

    .fare-payment-policy .pl-15 {
        padding-left: 15px;
        font-weight: 500;
    }

    .total-fare .fare-left, .total-fare .fare-right {
        color: #00206A;
        font-weight: 600;
        font-size: 18px;
    }

    @media only screen and (max-width: 992px) {
        .invoice-content {
            padding: 30px 15px;
        }

        .fare-payment-policy .col-md-8 {
            -ms-flex: 0 0 66.666667%;
            flex: 0 0 66.666667%;
            max-width: 66.666667%;
        }

        .fare-payment-policy .col-md-4 {
            -ms-flex: 0 0 33.333333%;
            flex: 0 0 33.333333%;
            max-width: 33.333333%;
        }

        .traveller-details .col-md-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .onward-flight .col-md-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .invoice-flight-name .flight-non-ref a {
            padding: 4px 10px;
            font-size: 14px;
            border-radius: 10px;
        }

        .invoice-flight-name .col-md-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .invoice-flight-details .col-md-5 {
            -ms-flex: 0 0 41.666667%;
            flex: 0 0 41.666667%;
            max-width: 41.666667%;
            padding-left: 5px;
            padding-right: 5px;
        }

        .invoice-flight-details .invoice-flight-duration .invoice-flight-class {
            font-size: 14px;
            font-weight: 600;
        }

        .invoice-flight-details .invoice-flight-duration .hours {
            font-size: 14px;
            font-weight: 600;
        }

        .invoice-flight-details .col-md-2 {
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
            max-width: 16.666667%;
            padding-left: 5px;
            padding-right: 5px;
        }

        .invoice-flight-details .flight-and-time .city-time {
            font-size: 17px;
        }

        .invoice-flight-details .flight-and-time .day {
            font-size: 14px;
        }

        .invoice-flight-details .flight-and-time .airport {
            font-size: 12px;
        }

        .invoice-flight-name .flight-name-no {
            margin-left: 40px;
        }

        .invoice-flight-name .flight-logo {
            width: 30px;
        }
    }
</style>

<!-- Loader -->
<body>
<div class="loader"></div>


<div id="myNav" class="overlay">

    <!-- Button to close the overlay navigation -->
    <a href="javascript:void(0)" class="closebtn closediv" onclick="closediv()">&times;</a>

    <!-- Overlay content -->
    <div class="overlay-content">
        <img src="{{ asset('assets/images/congratulations.png')}}" style="height: 95px; margin-bottom: 23px;">
        <p>Your Flight has been booked successfully</p>
    </div>
    <div class="over-img">
        <img src="{{ asset('assets/images/hand2.png')}}" style="height: 335px">
    </div>

</div>
<div class="super_container">
    <!-- Header -->
    @include('pages.include.topheader')
    <div class="home"
         style="height:20vh;">
    <!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src=""></div> -->
        <div class="home_content">
            <div class="home_title">Results</div>
        </div>
    </div>        <!-- Intro -->

    <div class="invoice">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="intro_content nano-content">
                        <div class="intro_title">INVOICE</div>
                    </div>
                </div>
            </div>
            <?php
            $chkpnr = $flightbook->pnrno;
            if (!empty($flightbook_return)) {

                $chkretpnr = $flightbook_return->pnrno;


            } else {

                $chkretpnr = "";
            }
            $pdfpnr1 = $chkpnr . "-" . $chkretpnr;
            $pdfpnr = base64_encode($pdfpnr1);

            ?>
            <div class="row">
                <div class="col-md-12" style="float: right">
                    <button id="print_btn" class="btn btn-success pull-right" style="margin-left: 10px;">Print</button>
                    <a href="{{action('FlightBookController@flight_pdf', $pdfpnr )}}" class="btn btn-success pull-right" target="_blank">PDF</a></div>
                <div class="col-md-6"></div>
            </div>
            <?php
            $segment = unserialize($flightbook->segment);
            $bookdate1 = explode('T', $segment[0]['Origin']['DepTime']);
            $bookingdate = date("d M Y", strtotime($bookdate1[0]));
            $bookingtime = date("H:s", strtotime($bookdate1[1]));
            $imgsou = 'assets/images/flag/' . $segment[0]['Airline']['AirlineCode'] . '.gif';
            $imgsou_new = "assets/images/flag/" . $segment[0]['Airline']['AirlineCode'] . ".gif";
            ?>
            <div class="invoice-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="e-ticket"><h4>E-Ticket</h4>
                            <div class="booking-id">Booking ID : {{$flightbook->bookingId}}</div>
                            <div class="booked-on">Booking ID : {{$bookingdate}} {{$bookingtime}} </div>
                        </div>
                    </div>
                </div>
                <div class="onward-flight">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="onward-icon"><i class="fa fa-plane fa-rotate-45"></i></div>
                            <div class="onward-flight-details"><h5>Onward Flight</h5><h4>{{$flightbook->orign}}
                                    to {{$flightbook->destination}}</h4></div>
                        </div>
                        <div class="col-md-6">
                            <div class="onward-flight-no"><h5>PNR</h5><h4>{{$flightbook->pnrno}}</h4></div>
                        </div>
                    </div>
                </div>
                <div class="invoice-flight-name">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="flight-logo"><img src="{{ $imgsou }}" style="width:30px;height:30px"></div>
                            <div class="flight-name-no">
                                <h4>{{$segment[0]['Airline']['AirlineName']}}  {{$segment[0]['Airline']['AirlineCode']}}
                                    -{{$segment[0]['Airline']['FlightNumber']}}</h4></div>
                        </div>
                        <div class="col-md-6">
                            @if($flightbook->refund=='Refundable')

                                <div class="flight-ref">
                                    Refundable</a>
                                </div>
                            @else
                                <div class="flight-non-ref"> Non
                                    Refundable</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <?php
                for($seg_i = 0;$seg_i < count($segment);$seg_i++)
                {
                    $arvdate1 = explode('T', $segment[$seg_i]['Destination']['ArrTime']);
                    $arvtime2 = date("H:s", strtotime($arvdate1[1]));
                    $arvtime1 = date("H:i:s", strtotime($arvdate1[1]));
                    $arvnewdate1 = date("Y-m-d", strtotime($arvdate1[0]));
                    $arvnewdatenew = date("l d M Y ", strtotime($arvdate1[0]));
                    $devdate1 = explode('T', $segment[$seg_i]['Origin']['DepTime']);
                    $depnewdatedetail = date("l d M Y", strtotime($devdate1[0]));
                    $depnewtimedetail = date("H:s", strtotime($devdate1[1]));
                    $depdatetime = date("H:i:s", strtotime($devdate1[1]));
                    $depdatedistance = date("Y-m-d", strtotime($devdate1[0]));
                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $depdatedistance . '' . $depdatetime);
                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $arvnewdate1 . '' . $arvtime1);
                    $totalDuration = $from->diffInSeconds($to);
                    $durationhour = gmdate('H', $totalDuration);
                    $durationmin = gmdate('s', $totalDuration);
                    $newtime = $durationhour . 'H : ' . $durationmin . 'M';
                    ?>
                <div class="invoice-flight-details">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="flight-and-time">
                                <div class="city-time">
                                    <strong>{{$segment[$seg_i]['Origin']['Airport']['AirportCode']}}</strong> {{$depnewtimedetail}}
                                </div>
                                <div class="day">{{$depnewdatedetail}}</div>
                                <div class="airport">{{$segment[$seg_i]['Origin']['Airport']['CityCode']}}
                                    , {{$segment[$seg_i]['Origin']['Airport']['AirportName']}},
                                    Terminal {{$segment[$seg_i]['Origin']['Airport']['Terminal']}}</div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="invoice-flight-duration">
                                <div class="clock-icon"><i class="fa fa-clock-o"></i></div>
                                <div class="hours">{{$newtime}}</div>
                                <div class="invoice-flight-class">{{$flightbook->flightcabinclass}}</div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="flight-and-time text-right">
                                <div class="city-time">
                                    <strong>{{$segment[$seg_i]['Destination']['Airport']['AirportCode']}}</strong>{{$arvtime2}}
                                </div>
                                <div class="day">{{$arvnewdatenew}}</div>
                                <div class="airport">{{$segment[$seg_i]['Destination']['Airport']['CityCode']}}
                                    , {{$segment[$seg_i]['Destination']['Airport']['AirportName']}},
                                    Terminal {{$segment[$seg_i]['Destination']['Airport']['Terminal']}}</div>
                            </div>
                        </div>
                    </div>
                </div> <?php } ?>
                <div class="traveller-details">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="traveller">Traveller</div>
                        </div>
                        <div class="col-md-4">
                            <div class="ticket-id">Ticket Id</div>
                        </div>
                        <div class="col-md-4">
                            <div class="ticket-id">Ticket No.</div>
                        </div>
                    </div> <?php
                    $passengers = unserialize($flightbook->passenger_detail);
                    for($pass = 0;$pass < count($passengers);$pass++)
                    {
                        if ($passengers[$pass]['PaxType'] == '1') {

                        $passcheck = 'Adults';
                    } else if ($passengers[$pass]['PaxType'] == '2') {
                        $passcheck = 'Child';
                    } else if ($passengers[$pass]['PaxType'] == '3') {
                        $passcheck = 'Infant';
                    } else {
                        $passcheck = '';

                    }
                        ?>


                    <div class="row">
                        <div class="col-md-4">
                            <div class="traveller-name">{{$passengers[$pass]['Title']}}
                                . {{ucfirst($passengers[$pass]['FirstName'])}} {{ucfirst($passengers[$pass]['LastName'])}}
                                ({{$passcheck}})
                            </div>
                        </div>
                        @if(!empty($passengers[$pass]['Ticket']['TicketId']))

                            <div class="col-md-4">
                                <div class="traveller-ticket-no">{{$passengers[$pass]['Ticket']['TicketId']}}</div>
                            </div>
                            <div class="col-md-4">
                                <div class="traveller-ticket-id">{{$passengers[$pass]['Ticket']['TicketNumber']}}</div>
                            </div> @else
                            <div class="col-md-4">
                                <div class="traveller-ticket-id">{{$flightbook->pnrno}}</div>
                            </div>
                        @endif
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="invoice-important">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading">Important</div>
                            <ul>
                                <li>Please carry your Government ID proof for all passengers to show during security
                                    check and check-in. Name on Government ID proof should be same as on your ticket.
                                </li>
                                <li>We recommended you to reach airport 2 hrs before departure time. Airline check-in
                                    counters typically close 1hr prior to departure time.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="baggage-policy">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading"><i class="fa fa-briefcase"></i> Baggage Policy</div>
                            <table class="table table-responsive table-bordered">
                                <tr>
                                    <th>Check-In(Adult & Child)</th>
                                    <td>As per Airline Policy</td>
                                </tr>
                                <tr>
                                    <th>Hand-Baggage(Adult & Child)</th>
                                    <td>7 KG / person</td>
                                </tr>
                                <tr>
                                    <th>Terms & Conditions</th>
                                    <td><p>Please check with the airline on the dimensions of the baggage</p>
                                        <p>The baggage policy is only indicative and can change any time. You are
                                            advised to check with the airline before travel to know latest baggage
                                            policy</p>
                                        <p>You are advised to check with the airline for extra baggage charges</p></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="cancellation-policy">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading"><i class="fa fa-close"></i> Cancellation Policy</div>
                            <table class="table table-responsive table-bordered">
                                <tr>

                                    <th style="padding: 15px">BOOKINGS & ITINERARY</th>
                
                                    <td style="text-align:right; padding: 15px">The reference number on your booking is confirmed and the same can be checked with the airlines. Kindly check your Booking carefully for
                                        correct dates,times of all flights and that all passengers names are correct as per their passport. It is the traveller's reponsibility to check their
                                        details on booking. No name changes are allowed once the ticket is issued.
                                        </td>
                
                                </tr>
                
                                <tr>
                
                                    <th style="padding: 15px" >E TICKETS
                                    </th>
                
                                    <td style="text-align:right;">Etickets may issued for within 48hours of making the payment. In the event you do not receive the e-ticket from us please contact the Travel
                                        Agency. We will not be responsible for the ticket if the aforementioned time limit has passed and the ticket is not received by the passenger.
                                        For further clarifications please, visit our website www.sekaptravels.com.
                                        
                                        </td>
                
                                </tr>
                
                                <tr>
                
                                    <th  style="padding: 15px">VISA REQUIREMENTS
                                    </th>
                
                                    <td style="text-align:right; padding: 15px">We do not deal in Visa therefore, please refer to the local counselate for Visa requirements.
                                    </td>
                
                                </tr>
                
                
                
                
                                <tr>
                
                                    <th  style="padding: 15px">FARES
                                    </th>
                
                                    <td style="text-align:right; padding: 15px">Prices quoted at time of booking or enquiry are not guaranteed until paid in full. Fares are subject to change according to availability.
                
                                    </td>
                
                                </tr>
                                <tr>
                
                                    <th  style="padding: 15px">CANCELLATION OR CHANGES
                
                                    </th>
                
                                    <td style="text-align:right; padding: 15px">Cancellation and change in dates may vary according to class or fare rules on which the ticket is issued. Some tickets may be
                                        non-refundable and non-changeable. The tickets with flexibility can be changed by paying penalty plus fare difference (which varies according
                                        to the availability).
                                        Any refund of the ticket shall be processed within 6 to 8 weeks of the date when refund is applied.
                                        
                                    </td>
                
                                </tr>
                
                                <tr>
                
                                    <th  style="padding: 15px">CHECKED BAGGAGE
                
                
                                    </th>
                
                                    <td style="text-align:right; padding: 15px">The baggage varies from airlines to the classes booked. Please ask for more information about baggage from our consultant.
                                    </td>
                
                                </tr>
                                <tr>
                
                                    <th  style="padding: 15px">Terms & Conditions</th>
                
                                    <td style="text-align:right; padding: 15px">
                
                                        <p>We accept cancellations, only before 24 Hrs from departure time</p>
                
                                        <p>For cancellations, within 24 hours before departure you need to contact the airline. Post cancellation by airline, you can contact Paytm for refund, if any</p>
                
                                        <p>Convenience fee is non-refundable. Any cashback availed will be adjusted in final refund amount</p>
                
                                    </td>
                
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
                $flightfare = unserialize($flightbook->fare);
                if ($flightbook->mealprice != '' || $flightbook->mealprice == '0') {

                    $mealprice = $flightbook->mealprice;
                } else {
                    $mealprice = 0;

                }
                if ($flightbook->bag_price != '' || $flightbook->bag_price == '0') {

                    $baggprice = $flightbook->bag_price;
                } else {
                    $baggprice = 0;

                }
                $othercharges = $flightbook->flightothercharges;
                // $othercharges = round($flightfare['OtherCharges'] +  $flightfare['TdsOnPLB'] + $flightfare['TdsOnIncentive']+$flightfare['TdsOnCommission']);
                                    	$basefare =$flightbook->flightmarginprice;
                                    	$taxfare = $flightbook->flighttax;
                                    	$totalamount =  $basefare +  $taxfare + $othercharges + $mealprice + $baggprice ;
                                    	// $gst =( $totalamount1 * 5 ) /100;
                //
                //   // $totalamount = $totalamount1 + $gst;


                 ?>
                <div class="fare-payment-policy">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading"><i class="fa fa-rupee"></i> Fare & Payment Details</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="fare-left">Base Fare</div>
                        </div>
                        <div class="col-md-4">
                            <div class="fare-right"><i
                                        class="fa {{$flightbook->currency_icon}}"></i> {{number_format($basefare,2)}}

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="fare-left">Tax</div>
                        </div>
                        <div class="col-md-4">
                            <div class="fare-right"><i
                                        class="fa {{$flightbook->currency_icon}}"></i> {{number_format($taxfare,2)}}

                            </div>
                        </div>
                    </div> @if($baggprice !='0')
                        <div class="row">
                            <div class="col-md-8">
                                <div class="fare-left">Extra Baggage Charges({{$flightbook->bag_weight}} Kg)
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="fare-right"><i
                                            class="fa {{$flightbook->currency_icon}}"></i> {{number_format($baggprice,2)}}
                                </div>
                            </div>
                        </div> @endif @if($mealprice!='0')
                        <div class="row">
                            <div class="col-md-8">
                                <div class="fare-left">Meal Charges ({{$flightbook->mealname}})</div>
                            </div>
                            <div class="col-md-4">
                                <div class="fare-right"><i
                                            class="fa {{$flightbook->currency_icon}}"></i> {{number_format($mealprice,2)}}
                                </div>
                            </div>
                        </div> @endif
                    <div class="row">
                        <div class="col-md-8">
                            <div class="fare-left">Other Charges</div>
                        </div>
                        <div class="col-md-4">
                            <div class="fare-right"><i
                                        class="fa {{$flightbook->currency_icon}}"></i> {{number_format($othercharges,2)}}
                            </div>
                        </div>
                    </div>
                    <div class="total-fare">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="fare-left">Total Fare</div>
                            </div>
                            <div class="col-md-4">
                                <div class="fare-right"><i
                                            class="fa {{$flightbook->currency_icon}}"></i> {{number_format($totalamount,2)}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- pdf code -->
                    
                </div>
            </div>
            <!-- email code -->

            <!-- end email code -->
            <?php
            if(!empty($flightbook_return))
            {
                $segment_return = unserialize($flightbook_return->segment);
                $bookdate1 = explode('T', $segment_return[0]['Origin']['DepTime']);
                $bookingdate = date("d M Y", strtotime($bookdate1[0]));
                $bookingtime = date("H:s", strtotime($bookdate1[1]));
                $imgsou = 'assets/images/flag/' . $segment_return[0]['Airline']['AirlineCode'] . '.gif';
                ?>
            <div class="invoice-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="e-ticket"><h4>E-Ticket</h4>
                            <div class="booking-id">Booking ID : {{$flightbook_return->bookingId}}</div>
                            <div class="booked-on">Booking ID : {{$bookingdate}} {{$bookingtime}} </div>
                        </div>
                    </div>
                </div>
                <div class="onward-flight">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="onward-icon"><i class="fa fa-plane fa-rotate-45"></i></div>
                            <div class="onward-flight-details"><h5>Onward Flight</h5><h4>{{$flightbook_return->orign}}
                                    to {{$flightbook_return->destination}}</h4></div>
                        </div>
                        <div class="col-md-6">
                            <div class="onward-flight-no"><h5>PNR</h5><h4>{{$flightbook_return->pnrno}}</h4></div>
                        </div>
                    </div>
                </div>
                <div class="invoice-flight-name">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="flight-logo"><img src="{{ $imgsou }}" style="width:30px;height:30px">
                            </div>
                            <div class="flight-name-no">
                                <h4>{{$segment_return[0]['Airline']['AirlineName']}}  {{$segment_return[0]['Airline']['AirlineCode']}}-{{$segment_return[0]['Airline']['FlightNumber']}}</h4></div>
                        </div>
                        <div class="col-md-6">
                            @if($flightbook_return->refund=='Refundable')

                                <div class="flight-ref">
                                    Refundable</a>
                                </div>
                            @else

                                <div class="flight-non-ref"> Non Refundable</a>
                                </div> @endif
                        </div>
                    </div>
                </div> <?php
                for($seg_i = 0;$seg_i < count($segment_return);$seg_i++)
                {
                    $arvdate1 = explode('T', $segment_return[$seg_i]['Destination']['ArrTime']);
                    $arvtime2 = date("H:s", strtotime($arvdate1[1]));
                    $arvtime1 = date("H:i:s", strtotime($arvdate1[1]));
                    $arvnewdate1 = date("Y-m-d", strtotime($arvdate1[0]));
                    $arvnewdatenew = date("l d M Y ", strtotime($arvdate1[0]));
                    $devdate1 = explode('T', $segment_return[$seg_i]['Origin']['DepTime']);
                    $depnewdatedetail = date("l d M Y", strtotime($devdate1[0]));
                    $depnewtimedetail = date("H:s", strtotime($devdate1[1]));
                    $depdatetime = date("H:i:s", strtotime($devdate1[1]));
                    $depdatedistance = date("Y-m-d", strtotime($devdate1[0]));
                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $depdatedistance . '' . $depdatetime);
                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $arvnewdate1 . '' . $arvtime1);
                    $totalDuration = $from->diffInSeconds($to);
                    $durationhour = gmdate('H', $totalDuration);
                    $durationmin = gmdate('s', $totalDuration);
                    $newtime = $durationhour . 'H : ' . $durationmin . 'M';
                    ?>
                <div class="invoice-flight-details">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="flight-and-time">
                                <div class="city-time">
                                    <strong>{{$segment_return[$seg_i]['Origin']['Airport']['AirportCode']}}</strong> {{$depnewtimedetail}}

                                </div>
                                <div class="day">{{$depnewdatedetail}}</div>
                                <div class="airport">{{$segment_return[$seg_i]['Origin']['Airport']['CityCode']}},
                                    {{$segment_return[$seg_i]['Origin']['Airport']['AirportName']}},
                                    Terminal {{$segment_return[$seg_i]['Origin']['Airport']['Terminal']}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="invoice-flight-duration">
                                <div class="clock-icon"><i class="fa fa-clock-o"></i></div>
                                <div class="hours">{{$newtime}}</div>
                                <div class="invoice-flight-class">{{$flightbook_return->flightcabinclass}}</div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="flight-and-time text-right">
                                <div class="city-time">
                                    <strong>{{$segment_return[$seg_i]['Destination']['Airport']['AirportCode']}}</strong>{{$arvtime2}}
                                </div>
                                <div class="day">{{$arvnewdatenew}}</div>
                                <div class="airport">{{$segment_return[$seg_i]['Destination']['Airport']['CityCode']}}
                                    , {{$segment_return[$seg_i]['Destination']['Airport']['AirportName']}},
                                    Terminal {{$segment_return[$seg_i]['Destination']['Airport']['Terminal']}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>
                <div class="traveller-details">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="traveller">Traveller</div>
                        </div>
                        <div class="col-md-4">
                            <div class="ticket-id">Ticket Id</div>
                        </div>
                        <div class="col-md-4">
                            <div class="ticket-id">Ticket No</div>
                        </div>
                    </div>
                    <?php
                    $passengers_return = unserialize($flightbook_return->passenger_detail);
                    for($pass = 0;$pass < count($passengers_return);$pass++)
                    {
                        if ($passengers_return[$pass]['PaxType'] == '1') {

                        $passcheck = 'Adults';

                    } else if ($passengers_return[$pass]['PaxType'] == '2') {
                        $passcheck = 'Child';
                    } else if ($passengers_return[$pass]['PaxType'] == '3') {
                        $passcheck = 'Infant';
                    } else {
                        $passcheck = '';
                    }
                        ?>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="traveller-name">{{$passengers_return[$pass]['Title']}}. {{ucfirst($passengers_return[$pass]['FirstName'])}} {{ucfirst($passengers_return[$pass]['LastName'])}}({{$passcheck}})
                            </div>
                        </div>
                        @if(!empty($passengers_return[$pass]['Ticket']['TicketId']))

                            <div class="col-md-4">
                                <div class="traveller-ticket-id">{{$passengers_return[$pass]['Ticket']['TicketId']}}</div>
                            </div>
                            <div class="col-md-4">
                                <div class="traveller-ticket-id">{{$passengers_return[$pass]['Ticket']['TicketNumber']}}</div>
                            </div> @else
                            <div class="col-md-4">
                                <div class="traveller-ticket-id">{{$flightbook_return->pnrno}}</div>
                            </div>
                        @endif
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="invoice-important">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading">Important</div>
                            <ul>
                                <li>Please carry your Government ID proof for all passengers to show during security
                                    check and check-in. Name on Government ID proof should be same as on your ticket.
                                </li>
                                <li>We recommended you to reach airport 2 hrs before departure time. Airline check-in
                                    counters typically close 1hr prior to departure time.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="baggage-policy">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading"><i class="fa fa-briefcase"></i> Baggage Policy</div>
                            <table class="table table-responsive table-bordered">
                                <tr>
                                    <th>Check-In(Adult & Child)</th>
                                    <td>As per Airline Policy</td>
                                </tr>
                                <tr>
                                    <th>Hand-Baggage(Adult & Child)</th>
                                    <td>7 KG / person</td>
                                </tr>
                                <tr>
                                    <th>Terms & Conditions</th>
                                    <td><p>Please check with the airline on the dimensions of the baggage</p>
                                        <p>The baggage policy is only indicative and can change any time. You are
                                            advised to check with the airline before travel to know latest baggage
                                            policy</p>
                                        <p>You are advised to check with the airline for extra baggage charges</p></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="cancellation-policy">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading"><i class="fa fa-close"></i> Cancellation Policy</div>
                            <table class="table table-responsive table-bordered">
                                <tr>

                                    <th style="padding: 15px">BOOKINGS & ITINERARY</th>
                
                                    <td style="text-align:right; padding: 15px">The reference number on your booking is confirmed and the same can be checked with the airlines. Kindly check your Booking carefully for
                                        correct dates,times of all flights and that all passengers names are correct as per their passport. It is the traveller's reponsibility to check their
                                        details on booking. No name changes are allowed once the ticket is issued.
                                        </td>
                
                                </tr>
                
                                <tr>
                
                                    <th style="padding: 15px" >E TICKETS
                                    </th>
                
                                    <td style="text-align:right;">Etickets may issued for within 48hours of making the payment. In the event you do not receive the e-ticket from us please contact the Travel
                                        Agency. We will not be responsible for the ticket if the aforementioned time limit has passed and the ticket is not received by the passenger.
                                        For further clarifications please, visit our website www.sekaptravels.com.
                                        
                                        </td>
                
                                </tr>
                
                                <tr>
                
                                    <th  style="padding: 15px">VISA REQUIREMENTS
                                    </th>
                
                                    <td style="text-align:right; padding: 15px">We do not deal in Visa therefore, please refer to the local counselate for Visa requirements.
                                    </td>
                
                                </tr>
                
                
                
                
                                <tr>
                
                                    <th  style="padding: 15px">FARES
                                    </th>
                
                                    <td style="text-align:right; padding: 15px">Prices quoted at time of booking or enquiry are not guaranteed until paid in full. Fares are subject to change according to availability.
                
                                    </td>
                
                                </tr>
                                <tr>
                
                                    <th  style="padding: 15px">CANCELLATION OR CHANGES
                
                                    </th>
                
                                    <td style="text-align:right; padding: 15px">Cancellation and change in dates may vary according to class or fare rules on which the ticket is issued. Some tickets may be
                                        non-refundable and non-changeable. The tickets with flexibility can be changed by paying penalty plus fare difference (which varies according
                                        to the availability).
                                        Any refund of the ticket shall be processed within 6 to 8 weeks of the date when refund is applied.
                                        
                                    </td>
                
                                </tr>
                
                                <tr>
                
                                    <th  style="padding: 15px">CHECKED BAGGAGE
                
                
                                    </th>
                
                                    <td style="text-align:right; padding: 15px">The baggage varies from airlines to the classes booked. Please ask for more information about baggage from our consultant.
                                    </td>
                
                                </tr>
                                <tr>
                
                                    <th  style="padding: 15px">Terms & Conditions</th>
                
                                    <td style="text-align:right; padding: 15px">
                
                                        <p>We accept cancellations, only before 24 Hrs from departure time</p>
                
                                        <p>For cancellations, within 24 hours before departure you need to contact the airline. Post cancellation by airline, you can contact Paytm for refund, if any</p>
                
                                        <p>Convenience fee is non-refundable. Any cashback availed will be adjusted in final refund amount</p>
                
                                    </td>
                
                                </tr>
                            </table>
                        </div>
                    </div>
                </div> <?php
                $flightfare_return = unserialize($flightbook_return->fare);
                if ($flightbook_return->mealprice != '' || $flightbook_return->mealprice == '0') {

                    $mealprice_return = $flightbook_return->mealprice;
                } else {
                    $mealprice_return = 0;
                }
                if ($flightbook_return->bag_price != '' || $flightbook_return->bag_price == '0') {

                    $baggprice_return = $flightbook_return->bag_price;
                } else {
                    $baggprice_return = 0;
                }
                $othercharges_return = $flightbook_return->flightothercharges;
                // $othercharges_return = round($flightfare_return['OtherCharges'] +  $flightfare_return['TdsOnPLB'] + $flightfare_return['TdsOnIncentive']+$flightfare_return['TdsOnCommission']);
                $basefare_return =$flightbook_return->flightmarginprice;
                $taxfare_return =$flightbook_return->flighttax;
                $totalamount_return =  $basefare_return +  $taxfare_return + $othercharges_return + $mealprice_return + $baggprice_return ;
                ?>
                <div class="fare-payment-policy">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading">Fare & Payment Details</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="fare-left">Base Fare</div>
                        </div>
                        <div class="col-md-4">
                            <div class="fare-right"><i
                                        class="fa {{$flightbook_return->currency_icon}}"></i>{{number_format($basefare_return,2)}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="fare-left">Tax</div>
                        </div>
                        <div class="col-md-4">
                            <div class="fare-right"><i
                                        class="fa {{$flightbook_return->currency_icon}}"></i>{{number_format($taxfare_return,2)}}
                            </div>
                        </div>
                    </div> @if($baggprice_return !='0')
                        <div class="row">
                            <div class="col-md-8">
                                <div class="fare-left">Extra Baggage Charges ({{$flightbook_return->bag_weight}}Kg)
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="fare-right"><i
                                            class="fa {{$flightbook_return->currency_icon}}"></i>{{number_format($baggprice_return,2)}}
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($mealprice_return!='0')
                        <div class="row">
                            <div class="col-md-8">
                                <div class="fare-left">Meal Charges({{$flightbook_return->mealname}})</div>
                            </div>
                            <div class="col-md-4">
                                <div class="fare-right"><i
                                            class="fa {{$flightbook_return->currency_icon}}"></i> {{number_format($mealprice_return,2)}}
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-8">
                            <div class="fare-left ">Other Charges</div>
                        </div>
                        <div class="col-md-4">
                            <div class="fare-right"><i
                                        class="fa {{$flightbook_return->currency_icon}}"></i> {{number_format($othercharges_return,2)}}
                            </div>
                        </div>
                    </div>
                    <div class="total-fare">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="fare-left">Total Fare</div>
                            </div>
                            <div class="col-md-4">
                                <div class="fare-right"><i
                                            class="fa fa-rupee"></i> {{number_format($totalamount_return,2)}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
    <!-- Intro -->
</div>
<!-- Footer -->
@include('pages.include.footer')
<!-- Copyright -->
@include('pages.include.copyright')
<script>
    $(document).ready(function () {
        $(".loader").fadeOut("slow");
    });
</script>
<script>
    $(document).on("click", "#print_btn", function () {
        $(".header").hide();
        $(".home").hide();
        $(".trans_500").hide();
        $('footer').hide();
        $("#location-invoice").hide();
        window.print();
        $(".header").show();
        $(".home").show();
        $(".trans_500").show();
        $('footer').hide();
    });
</script>
<script>
    $(document).ready(function(){

        $("#myNav").css({"height": "100%","top" : "0%"});

    }) ;
    $(document).on('click','.closediv',function(){
        $('#myNav').css({"height": "0%","top" : "100%"});
    })
</script>
</body>
</html>