@include ('pages.include.header');

<style>
.header {
    position: fixed;
    width: 100%;
    top: 0 !important;
    background: rgba(255, 255, 255, 0.9);
    z-index: 12;
}
.intro_item_content {
    cursor: pointer;
}
.home

{
	width: 100%;
	height: 465px;
	background: transparent;
}

.home_background

{

	position: absolute;

	top: 0;

	left: 0;

	width: 100%;

	height: 100%;

	background-repeat: no-repeat;

	background-size: cover;

	background-position: center center;

}

.home_content

{

	position: absolute;

	bottom: 106px;

	left: 50%;

	-webkit-transform: translateX(-50%);

	-moz-transform: translateX(-50%);

	-ms-transform: translateX(-50%);

	-o-transform: translateX(-50%);

	transform: translateX(-50%);

}

.home_title

{

	font-size: 72px;

	font-weight: 800;

	color: #FFFFFF;

	text-transform: uppercase;

}

.parallax-window {

    min-height: 400px;

    background: transparent;

}

</style>

</head>

<body>

<div class="super_container">

	<!-- Header -->

	@include ('pages.include.topheader')

	

	<div class="home">

		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/contact_background.jpg') }}"></div>

		<div class="home_content">

			<div class="home_title">Services</div>

		</div>

	</div>



	<div class="intro">

		<div class="container">

			<div class="row">

				<div class="col">

					<h2 class="intro_title text-center">We have the best tours</h2>

				</div>

			</div>

			<div class="row">

				<div class="col-lg-10 offset-lg-1">

					<div class="intro_text text-center">

						<p>IT’S A BIG WORLD OUT THERE
							GO EXPLORE</p>

					</div>

				</div>

			</div>

			<div class="row intro_items">

				<!-- Intro Item -->

				<div class="col-lg-4 intro_col" id="flight">

					<div class="intro_item">

						<div class="intro_item_overlay"></div>

						<div class="intro_item_background" style="background-image:url({{ asset('assets/images/air.jpg') }})"></div>

						<div class="intro_item_content d-flex flex-column align-items-center justify-content-center">

							<div class="intro_center text-center">

								<h1>Air Ticket</h1>

								<div class="rating rating_5">

									<i class="fa fa-star"></i>

									<i class="fa fa-star"></i>

									<i class="fa fa-star"></i>

									<i class="fa fa-star"></i>

									<i class="fa fa-star"></i>

								</div>

							</div>

						</div>

					</div>

				</div>

				<!-- Intro Item -->

				<!-- Intro Item -->

				<div class="col-lg-4 intro_col" id="hotel">

					<div class="intro_item">

						<div class="intro_item_overlay"></div>

						<div class="intro_item_background" style="background-image:url( {{ asset('assets/images/hotel.jpg') }} )"></div>

						<div class="intro_item_content d-flex flex-column align-items-center justify-content-center">

							<div class="intro_center text-center">

								<h1>Hotel Booking</h1>

								<div class="rating rating_5">

									<i class="fa fa-star"></i>

									<i class="fa fa-star"></i>

									<i class="fa fa-star"></i>

									<i class="fa fa-star"></i>

									<i class="fa fa-star"></i>

								</div>

							</div>

						</div>

					</div>

				</div>

				<!-- Intro Item -->

				<div class="col-lg-4 intro_col" id="package">

					<div class="intro_item">

						<div class="intro_item_overlay"></div>

						<div class="intro_item_background" style="background-image:url( {{ asset('assets/images/tour.jpg') }})"></div>

						<div class="intro_item_content d-flex flex-column align-items-center justify-content-center">

							<div class="intro_center text-center">

								<h1>Tour Packages</h1>

								<div class="rating rating_5">

									<i class="fa fa-star"></i>

									<i class="fa fa-star"></i>

									<i class="fa fa-star"></i>

									<i class="fa fa-star"></i>

									<i class="fa fa-star"></i>

								</div>

							</div>

						</div>

					</div>

				</div>



				<!-- Intro Item -->

			</div>

		</div>

	</div>

	<!-- Footer -->

	@include ('pages.include.footer')

	@include('pages.include.copyright')

	

</div>

<!-- jQuery 3 -->

<script>

$(".intro_col").click(function(){
	location.href="https://travoweb.com"
	localStorage.setItem("tab",this.id);
})
</script>

</body>

</html>