@include('pages.include.header')

<body>
    <style>
        .intro {
            width: 100%;
            padding-top: 100px;
            padding-bottom: 0px;

        }

        .add_content {
            z-index: 9;
        }

        .about-header {
            background: url("{{asset('assets/images/about-h.jpg')}}");
            height: 450px;
            margin-top: 126px;
            position: relative;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center center;
        }

        h1.about-title {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
            background: rgba(49, 18, 75, 0.8);
            padding: 10px 35px;
        }

        .intro_title {
            text-align: center;
        }

        ul.n-ul {
            margin-top: 30px;
            text-align: left;
        }

        ul.n-ul li {
            padding: 10px 20px;
            color: #9555ef;
            font-size: 15px;
            font-weight: 500;
        }

        ul.n-ul li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
            font-size: 20px;
            font-weight: 900;
        }

        .intro {
            background: white;
        }

        h1.h1-title {
            color: white;
            font-size: 20px;
        }

        .milestones {
            background: white;
        }

        .content-overlay {
            background: rgba(49, 18, 75, 0.8);
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 1;
        }

        p.p-para {
            font-size: 21px;
            color: #ffffff;
            background: linear-gradient(to right, #fa9e1b, #8d4fff, #fa9e1b);
            padding: 10px;
            border-radius: 5px;
        }

        ul.points {

            text-align: initial;
        }

        .column {
            column-count: 2;
        }

        ul.points li {
            padding: 10px;
            color: #8f50fb;
            font-size: 16px;
            font-weight: 600;
        }

        ul.points li:before {
            /* font-family: fontawesome; */
            font-family: FontAwesome;
            content: "\f105";
            display: inline-block;
            padding-right: 5px;
            color: #f49a26;
        }

        .milestone_icon img {
            width: auto;
            height: 50PX;
        }

        @media screen and (max-width:768px) {
            .column {
                column-count: 1;
            }

            ul.points {
                text-align: left !important;
            }
        }

        div.cstm-accordion .card-header {
            /* background: black; */
            font-size: 21px;
            color: #ffffff;
            background: white;
            padding: 10px 30px;
            border-radius: 5px;
            border-radius: 2px;
            border: none;
        }

        div.cstm-accordion .card-header a {
            color: #fa9e1b;
            font-size: 15px;
            display: block;
            text-align: center;
            font-weight: 500;
            text-transform: uppercase;
        }

        div.cstm-accordion .card {
            /* border: none !IMPORTANT; */
            margin-bottom: 10px;
            border: 1px solid #dadada;
        }
    </style>
    <div class="super_container">
        <!-- Header -->
        @include ('pages.include.topheader')
        <div class="about-header">
            <h1 class="about-title">Partner With Travoweb</h1>
        </div>
        <!-- Intro -->
        <div class="intro">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12">
                        <div class="intro_content">
                            <div class="intro_title">Partner With Travoweb</div>
                            <p class="intro_text" style="margin-bottom: 40px;">
                                www.travoweb.com is a reputed name among the modern Indian travelers. Founded with the
                                sole idea of offering most affordable travel solution,Travoweb has taken every
                                initiative for creating happy travelers and memories unlimited for them. It is a one
                                stop shop for all your travel needs and provides all travel solutions. If you are also
                                planning to start your travel business, come and associate with us to earn maximum
                                commission in the market.



                                The company was started with the latest technology called meta search and is about to
                                bring a revolution in travel industry by offering instant booking and comprehensive
                                range of travel products and services in comparing to the rest of the market. It is
                                known for its cutting-edge technology, best networking system and dedicated 24x7
                                customer support services. Therefore, start your dream business of a travel agency and
                                create your own brand in the industry.



                                At present, www.travoweb.com has a huge network of Travel affiliates, 50 franchise
                                outlets opening soon, 157 distributors, 800 corporate tie-ups and 240 white label



                                Whether you are an entrepreneur, travel agent, tour operator, coach operator or
                                hotelier, make your agency go online with your own website. Travoweb is an ideal
                                platform to become an entrepreneur. With B2C whitelabel solutions,www.travoweb.com
                                allows you starting your own travel business.

                                All you have to do is to promote your website and generate traffic for it. Deliver the
                                best travel solutions to your customers through your own website and we will take care
                                of everything, including payments. For sales made through your site, enjoy instant
                                credit of commission in your account.



                            </p>
                        </div>
                        <div class="intro">
                            <div class="container" style="margin-bottom: 80px">
                                <div class="row">
                                    <div class="col">
                                    
                                    <div class="contact_form_container">
                                    <div class="contact_title text-center">get in touch</div>
                                    <form action="#" id="contact_form" class="contact_form text-center">
                                    <input type="text" id="contact_form_name" class="contact_form_name input_field" placeholder="Company Name" required="required" data-error="Company Name is required.">
                                    <input type="text" id="contact_form_email" class="contact_form_email input_field" placeholder="E-mail" required="required" data-error="Email is required.">
                                    <input type="text" id="contact_form_subject" class="contact_form_subject input_field" placeholder="Phone no" required="required" data-error="Phone no is required.">
                                   <input type="text" id="contact_form_subject" class="contact_form_subject input_field" placeholder="Company Address" required="required" data-error="Company Address is required.">
                                   <textarea id="contact_form_message" class="text_field contact_form_message" name="message" rows="4" placeholder="Remarks" required="required" data-error="Please, write us a Remarks."></textarea>
                                    
                                   <button type="submit" id="form_submit_button" class="form_submit_button button trans_200">send message<span></span><span></span><span></span></button>
                                    </form>
                                    </div>
                                    </div>
                                    </div>
                            </div>
                            
                         
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->

            @include ('pages.include.footer')
            @include ('pages.include.copyright')
        </div>
    </div>
</body>

</html>