@include ('admin.include.topcss')
<style type="text/css">
    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }
    
    .pagination>.active>span {
        font-weight: bold;
        color: #fff;
        background-color: #337ab7;
    }
    
    ul.pagination {
        float: right;
        margin-right: 20px;
    }
</style>
<style>
    .section-start {
        padding: 30px 0;
    }
    
    .t-tran-detail {
        text-align: center;
        padding: 10px;
        background-color: #e26d78;
        color: white;
    }
    
    .c-items {
        border: 1px solid #c9c9c9;
        border-radius: 5px;
    }
    
    .onword-flight {
        text-align: left;
        color: white;
        padding: 10px;
        background-color: #21b1e7;
    }
    
    .t-note {
        padding: 13px;
        margin-top: -8px;
        font-size: 16px;
        border-left: 5px solid #21b1e7;
    }
    
    .flight-gif {
        width: 50px;
    }
    
    .pass-detail {
        text-align: center;
        padding: 10px;
        color: white;
        background-color: #e26d78;
    }
    
    .pass-contact {
        text-align: center;
        padding: 10px;
        background-color: #e26d78;
        color: white;
    }
    
    .t-total {
        font-size: 17px;
    }
    
    .t-badge {
        background-color: #ff6981;
        color: white;
        padding: 5px 10px;
    }
    
    .t-fare {
        text-align: center;
        padding: 10px;
        background-color: #fa9e1b;
        color: white;
    }
    
    .t-comm {
        text-align: center;
        padding: 10px;
        background-color: #fa9e1b;
        color: white;
    }
    
    .t-badge-comm {
        padding: 5px 10px;
        border-radius: 20px;
        font-size: 13px;
        background-color: #21b1e7;
    }
    
    .Commission tr td {
        text-align: center;
    }
     .modal-header.h-bg {
        background: #31124b;
        padding: 10px 21px;
        border-bottom-color: white;
    }
    .modal-footer.f-bg {
        background: #e2e3ea;
    }
    p.t-id-info {
        margin-top: 10px;
        color: red !important;
        font-weight: 600;
    }

    .btn1 {
        font-size: 17px;
        font-weight: 500;
        color: #fff;
        text-transform: uppercase;
        background: #fa9e1b;
        border: none;
        outline: none;
        padding: 8px 17px;
        border-radius: 5px;
        cursor: pointer;
    }
</style>
<!-- END HEAD -->

<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->@include ('admin.include.header')
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
            <!-- start sidebar menu -->@include ('admin.include.navbar')
            <!-- end sidebar menu -->
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Hotel Cancel Booking</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;<i class="fa fa-angle-right"></i> </li>
                                <li><a class="parent-item" href="">Hotel Cancel Booking</a>&nbsp;<i class="fa fa-angle-right"></i> </li>
                                <li class="active"> Hotel Cancel Booking</li>
                            </ol>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header>All Cancel Booking </header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                    <div class="row p-b-20">
                                        <div class="col-md-6 col-sm-6 col-6">
                                            </div>
                                    </div>
                                   <?php
                                 
                                   $hotelbookdetailsarray = unserialize($booklistdetails->whole_response);
                                    $hotelcancelwaynew = unserialize($hotelcancdetailway->cancel_allrequestdata);
                                    // echo "<pre>";
                                    // print_r($hotelcancelwaynew);
                                    // echo "</pre>";
                                    if($hotelcancelwaynew['ChangeRequestStatus']=='2') {
                                                $status="In Progress";
                                            }

                                            else if($hotelcancelwaynew['ChangeRequestStatus']=='1') {
                                                $status="Pending";
                                            }

                                            else if($hotelcancelwaynew['ChangeRequestStatus']=='3') {
                                                $status="Processed";
                                            }

                                            else if($hotelcancelwaynew['ChangeRequestStatus']=='4') {
                                                $status="Rejected";
                                            }

                                            else {
                                                $status="";
                                            }
                                   ?>
                                    <section class="section-start">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class=" c-items">
                                                        <h3 class="t-tran-detail">Transaction Detail</h3>
                                                        <div class="container">
                                                            <table class="table">
                                                                <tbody>
                                                                    <tr>
                                                                        <th>Booking Id:</th>
                                                                        <td>{{$hotelbookdetailsarray['BookingId']}}</td>
                                                                    </tr>
                                                                     <tr>
                                                                        <th>Change Request Id:</th>
                                                                        <td>{{$hotelcancelwaynew['ChangeRequestId']}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>API Status:</th>
                                                                        <td style="background-color: rgba(255,29,43,0.55)">
                                                                            {{$status}}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Transaction Date And Time:</th>
                                                                        <td>{{$orderdetails->tx_time}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Payment Status:</th>
                                                                        <td>{{$orderdetails->tx_status}}</td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <th>Confirmation:</th>
                                                                        <td>{{$hotelbookdetailsarray['ConfirmationNo']}}</td>
                                                                    </tr>
                                                                   <!--  <tr>
                                                                        <th>API Refund:</th>
                                                                        <td>{{$hotelbookdetailsarray['ConfirmationNo']}}</td>
                                                                    </tr> -->
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    
                                                    <section class="section-start">
                                                        <!-- cancelrule -->
                                                        <div class="c-items">
                                                            <h3 class="onword-flight">Cancellation Rules</h3>
                                                            
                                                            <table class="table">
                                                                <tbody>
                                                                   
                                                                        <?php
                                                                          $cancellationpolicy=array();
                                                                          for($cancel=0;$cancel<count($hotelbookdetailsarray['HotelRoomsDetails'][0]['CancellationPolicies']);$cancel++)
                                                                          {
                                                                           $cancellationpolicy=$hotelbookdetailsarray['HotelRoomsDetails'][0]['CancellationPolicies'];
                                                                           $fromdatetime=explode('T',$cancellationpolicy[$cancel]['FromDate']);
                                                                           $fromdate=date('d-F-Y',strtotime($fromdatetime[0]));
                                                                           $fromtime=date('h:i a',strtotime($fromdatetime[1]));

                                                                           $todatetime=explode('T',$cancellationpolicy[$cancel]['ToDate']);
                                                                           $todate=date('d-F-Y',strtotime($todatetime[0]));
                                                                           $totime=date('h:i a',strtotime($todatetime[1]));

                                                                           echo '<tr><th> '.$cancellationpolicy[$cancel]['Charge'].'%'.' of total amount will be charged , If cancelled between '.$fromdate.' '.$fromtime.' and '.$todate.' '.$totime.'</tr></th>';

                                                                       }
                                                                       ?>
                                                                  </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end cancle rule -->
                                                        <div class="c-items">
                                                            <h3 class="onword-flight">Hotel  Detail1</h3>
                                                            
                                                            <table class="table">
                                                                <tbody>
                                                                    <tr>

                                                                        <th> <img class="flight-gif" src="{{$booklistdetails->hotel_picture}}" style="width:150px"></th>
                                                                        <td>
                                                                            <table class="table table-borderless">
                                                                                
                                                                                <tr>
                                                                                    <td>Hotel Name : {{$hotelbookdetailsarray['HotelName']}} </td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td>Address :{{$hotelbookdetailsarray['AddressLine1']}} </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>City :{{$hotelbookdetailsarray['City']}} </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                   
                                                               
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                         <div class="mt-4 c-items">
                                                            <h3 class="pass-detail">Check In : <?php 
                                                                $getcheckindatetime=explode('T',$hotelbookdetailsarray['CheckInDate']);

                                                                $getcheckindate=date('d-m-Y',strtotime($getcheckindatetime[0]));
                                                                echo $getcheckindate;
                                                                ?> -> Check Out : <?php 
                                                                $getcheckindatetime=explode('T',$hotelbookdetailsarray['CheckOutDate']);

                                                                $getcheckindate=date('d-m-Y',strtotime($getcheckindatetime[0]));
                                                                echo $getcheckindate;
                                                                ?> </h3>
                                                           
                                                        </div>
                                                        <div class="mt-4 c-items">
                                                             @for($rooms=0;$rooms< count($hotelbookdetailsarray['HotelRoomsDetails']);$rooms++)
                                                            <h3 class="pass-detail">Passenger Details Room {{$rooms+1}} </h3>
                                                            <h5 class="onword-flight"><center>{{$hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['RoomTypeName']}} </center> </h5>
                                                            <div class="container">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Pax-Name</th>
                                                                            <th>Pax</th>
                                                                            <th>Age</th>
                                                                            
                                                                        </tr>
                                                                    </thead>
                                                                   
                                                                    <tbody>
                                                                    	@for($passenger=0;$passenger< count($hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger']);$passenger++)
                                                                        <?php 
                                                                             $title=$hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['Title'];
                                                                             $fullname=strtolower($hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['FirstName']." ".$hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['LastName']);
                                                                             $childage=$hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['Age'];
                                                                             if($hotelbookdetailsarray['HotelRoomsDetails'][$rooms]['HotelPassenger'][$passenger]['PaxType']==1)
                                                                                 {
                                                                                    $pax="Adult";
                                                                                    $childa ="(Age " .$childage." year)";
                                                                                }
                                                                                else
                                                                                {
                                                                                   $pax="Child";
                                                                                   $childa ="(Age " .$childage." year)";
                                                                               }
                                                                            ?>
                                                                      <tr>
                                                                                <td>{{$title}} {{ucwords($fullname)}}</td>
                                                                                <td>{{$pax}}</td>
                                                                                <td>{{$childage}}</td>
                                                                                </tr>
                                                                        
                                                                       @endfor
                                                                       
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                             @endfor
                                                        </div>
                                                        <div class="mt-4 c-items">
                                                            <h3 class="pass-contact">Lead Passenger Contact</h3>
                                                            <div class="container">
                                                                <table class="table table-striped table-responsive">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <th>Contact Email</th>
                                                                            <th>Contact Phone</th>
                                                                            <th>Total Room</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>{{$booklistdetails->lead_title}} {{$booklistdetails->lead_name}}</td>
                                                                            <td>{{$booklistdetails->lead_email}}</td>
                                                                            <td>{{$booklistdetails->lead_mobile}}</td>
                                                                            <td>{{$booklistdetails->no_of_rooms}}</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                               <?php
                                                    $RoomPrice=0;
                                                    $Tax=0;
                                                    $ExtraGuestCharge=0;
                                                    $ChildCharge=0;
                                                    $OtherCharges=0;
                                                    $AgentCommission=0;
                                                    $AgentMarkUp=0;
                                                    $PublishedPrice=0;
                                                    $ServiceTax=0;
                                                    $TDS=0;
                                                    $TotalGSTAmount=0;
                                                    $total_fare=0;
                                                    $gst=0;
                                                    $total_gst=0;
                                                    $grand_total=0;
                                                    $mainothercharges=0;
                                                    for($i=0;$i<count($hotelbookdetailsarray['HotelRoomsDetails']);$i++)
                                                    {
                                                        $RoomPrice+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['RoomPrice'];
                                                        $Tax+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['Tax'];
                                                        $ExtraGuestCharge+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['ExtraGuestCharge'];
                                                        $ChildCharge+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['ChildCharge'];

                                                        $OtherCharges+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['OtherCharges'];
                                                        $AgentCommission+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['AgentCommission'];
                                                        $AgentMarkUp+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['AgentMarkUp'];
                                                        $PublishedPrice+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['PublishedPrice'];
                                                        $ServiceTax+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['ServiceTax'];
                                                        $TDS+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['TDS'];
                                                        $TotalGSTAmount+=$hotelbookdetailsarray['HotelRoomsDetails'][$i]['Price']['TotalGSTAmount'];

                                                        $total_fare=$RoomPrice+$Tax+$ExtraGuestCharge+$ChildCharge+$OtherCharges+$AgentCommission
                                                        + $AgentMarkUp+$ServiceTax+$TDS+$TotalGSTAmount;
                                                        $mainothercharges= $ExtraGuestCharge+$ChildCharge+$OtherCharges+$AgentCommission
                                                        + $AgentMarkUp+$ServiceTax+$TDS+$TotalGSTAmount;
                                                        $gst=($total_fare*5)/100;
                                                        $total_gst=round($gst,2);

                                                        $grand_total=$total_fare+$total_gst;

                                                    }

                                                    ?>
                                                <div class="col-sm-4">
                                                    <div class="c-items">
                                                        <h3 class="t-fare">Fare Break-Up (Onword )</h3>
                                                        <div class="container">
                                                            <table class="table table-striped">
                                                                <tbody>
                                                                    <tr>
                                                                        <th>Room Price </th>
                                                                        <td><i class="fa {{$booklistdetails->hotelfaicon}}"></i> 
                                                                        {{$booklistdetails->hotelmarginprice}} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Tax</th>
                                                                        <td><i class="fa {{$booklistdetails->hotelfaicon}}"></i> {{$booklistdetails->hotelservice_tax}} </td>
                                                                    </tr>
                                                                   
                                                                     
                                                                        <th>GST </th>
                                                                        <td><i class="fa {{$booklistdetails->hotelfaicon}}"></i> {{$booklistdetails->hotelmargingst}}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <hr class="w-100">
                                                            <div class="t-total text-right">
                                                                <p>Total: <span style="font-size: 18px;width: 30px" class="t-badge">                                                             <i class="fa {{$booklistdetails->hotelfaicon}}"></i> {{$booklistdetails->hotelamount}}</span> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="c-items mt-5">
                                                        <h3 class="t-comm">Refund Amount</h3>
                                                        <div class="container">
                                                            <table class="table table-striped Commission">
                                                                <tbody>
                                                                    <span style="color:red; display: none;font-size: 11px" id="refunderrorm"></span>
                                                                    <span style="color:green; display: none;font-size: 11px" id="srefunderrorm"></span>
                                                                    <input type="hidden" name="bookingid" id="bookingid" value="{{$hotelbookdetailsarray['BookingId']}}">
                                                                    <input type="hidden" name="changereq" id="changereq" value="{{$hotelcancelwaynew['ChangeRequestId']}}">
                                                                    <input type="hidden" id="paxname" value="{{$booklistdetails->lead_title}} {{$booklistdetails->lead_name}} ">

                                                                    <input type="hidden" value="{{$booklistdetails->lead_mobile}}" id="leadphone"> 
                                                                    <input type="hidden" id="leademail" value="{{$booklistdetails->lead_email}}">
                                                                   
                                                                    <tr>
                                                                        <th>Total Amount</th>
                                                                        <td><i class="fa fa-rupee"></i> {{$grand_total}}</td>
                                                                    </tr>
                                                                    @if($hotelcancdetailway->c_adminstatus=='0')
                                                                    <tr>
                                                                        <th>Refund Amount </th>
                                                                        <td>
                                                                            <input type="text" name="refundamount" class="refundamount leadage" >
                                                                            <span style="color:red; display: none;font-size: 11px" id="refunderror"></span>
                                                                        </td>
                                                                    </tr>
                                                                    @else
                                                                    <tr>
                                                                        <th>Refund Amount</th>
                                                                        <td><i class="fa {{$booklistdetails->hotelfaicon}}"></i> {{$hotelcancdetailway->c_refund_amount
                                                                       }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Refund Date</th>
                                                                        <td>
                                                                            <?php
                                                                           echo  $getcheckindate=date('d-m-Y',strtotime($hotelcancdetailway->c_create_date));
                                                                           
                                                                            ?>
                                                                           </td>
                                                                    </tr>
                                                                    @endif
                                                                    
                                                                </tbody>
                                                                
                                                            </table>
                                                            <hr class="w-100">
                                                            @if($hotelcancdetailway->c_adminstatus=='0')
                                                                    <div class="t-total text-right">
                                                                        <button class="btn btn-primary" id="refundaccept">Accept</button>
                                                                    </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page content -->
        </div>
        <!-- <button class=""data-toggle="modal" data-target="#myModal_charges">Cancellation</button> -->
                <div class="modal fade text-center py-5" style="top:30px" id="myModal_charges">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content" style="border-radius: 7px;">
                            <div class="modal-header h-bg">
                                <span class="close close-btn" data-dismiss="modal">&times;</span>

                            </div>
                            <div class="modal-body">

                                <h3 class="pt-3 mb-0 h-cancel success_modal"> </h3>
                                
                                <button class="btn1 text-white mb-5 success_ok" style="margin-top:20px; margin-bottom: 0px !important">Submit</button>
                            
                            </div>
                            <div class="modal-footer f-bg mt-3">
                                <span class="d-block mr-auto f-help "><i class="fa fa-phone"></i> Helpline:<a href="#"> 34356894454</a></span>
                                <span class="f-help"><img src="{{asset('assets/images/logo.png')}}" class="f-logo"></span>
                            </div>
                        </div>
                    </div>
                </div>
<!-- Modal -->
        <!-- end page container -->
        <!-- start footer -->@include ('admin.include.footer')
        <!-- end footer -->
    </div> @include ('admin.include.downjs')</body>
    <script>
    $(document).ready(function () {
  
  $(".leadage").keypress(function (e) {
    
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       return false;
    }
   });
});

</script>
<script>
    $(document).on('click','#refundaccept',function(){
        $('#refunderrorm').hide();
        $('#refunderror').hide();
        $('#srefunderrorm').hide();
       var refundamount = $('.refundamount').val();
       var bookingid = $('#bookingid').val();
       var changereq = $('#changereq').val();
       var paxname =$('#paxname').val();
       var leadphone = $('#leadphone').val();
       var leademail = $('#leademail').val();
       if(refundamount=='')
       {
            $('#refunderror').text("Please Enter Refund Amount");
            $('#refunderror').show();
       }
       else
       {
         $('#refunderror').hide();
         $.ajax({ 
                url:"{{route('hotelrefund')}}",
                data : { 'refundamount' : refundamount,
                        'bookingid'      : bookingid,
                        'changereq' :changereq,
                        'paxname' :paxname,
                        'leadphone':leadphone,
                        'leademail':leademail,
                        },
                type : "GET",
                success : function(response)
                {
                    if(response=='success')
                    {
                        $('#srefunderrorm').text("Successfully Refund Amount Submit");
                        $('#srefunderrorm').show();
                        $('#myModal_charges').modal('show');
                        $('.success_modal').text("Successfully Refund Amount Submit");

                    }
                    else
                    {
                        $('#refunderrorm').text("Unable to refund this amount");
                        $('#refunderrorm').show();
                    }
                }

         });
         
       }
    })
</script>
<script type="text/javascript">
$(document).on('click', ".success_ok", function ()
{
    $('#myModal_charges').modal('hide');
    window.location="{{url('admin/adminhotelcancel')}}";
});
</script>
</html>