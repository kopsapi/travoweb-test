@include ('admin.include.topcss')

<style type="text/css">

    .pagination>li>a,

    .pagination>li>span {

        position: relative;

        float: left;

        padding: 6px 12px;

        line-height: 1.42857143;

        color: #337ab7;

        text-decoration: none;

        background-color: #fff;

        border: 1px solid #ddd;

    }



    .pagination>.active>span {

        font-weight: bold;

        color: #fff;

        background-color: #337ab7;

    }



    ul.pagination {

        float: right;

        margin-right: 20px;

    }

</style>

<style>

    .section-start {

        padding: 30px 0;

    }



    .t-sms-detail {

        text-align: center;

        padding: 10px;

        background-color: #e26d78;

        color: white;

    }



    .c-items {

        border: 1px solid #c9c9c9;

        border-radius: 5px;

    }



    .onword-flight {

        text-align: left;

        color: white;

        padding: 10px;

        background-color: #21b1e7;

    }



    .t-note {

        padding: 13px;

        margin-top: -8px;

        font-size: 16px;

        border-left: 5px solid #21b1e7;

    }



    .flight-gif {

        width: 50px;

    }



    .pass-detail {

        text-align: center;

        padding: 10px;

        color: white;

        background-color: #e26d78;

    }



    .pass-contact {

        text-align: center;

        padding: 10px;

        background-color: #e26d78;

        color: white;

    }



    .t-total {

        font-size: 17px;

    }



    .t-badge {

        background-color: #ff6981;

        color: white;

        padding: 5px 10px;

    }



    .t-fare {

        text-align: center;

        padding: 10px;

        background-color: #fa9e1b;

        color: white;

    }



    .t-comm {

        text-align: center;

        padding: 10px;

        background-color: #fa9e1b;

        color: white;

    }



    .t-badge-comm {

        padding: 5px 10px;

        border-radius: 20px;

        font-size: 13px;

        background-color: #21b1e7;

    }



    .Commission tr td {

        text-align: center;

    }

    .modal-header.h-bg {

        background: #31124b;

        padding: 10px 21px;

        border-bottom-color: white;

    }

    .modal-footer.f-bg {

        background: #e2e3ea;

    }

    p.t-id-info {

        margin-top: 10px;

        color: red !important;

        font-weight: 600;

    }



    .btn1 {

        font-size: 17px;

        font-weight: 500;

        color: #fff;

        text-transform: uppercase;

        background: #fa9e1b;

        border: none;

        outline: none;

        padding: 8px 17px;

        border-radius: 5px;

        cursor: pointer;

    }

</style>
 <style>
                                                        button.fh-save {
                                                            border: none;
                                                            border: 1px solid orange;
                                                            background: orange;
                                                            padding: 8px 23px;
                                                            color: white;
                                                            border-radius: 7px;
                                                            font-size: 18px;
                                                            display: flex;
                                                            font-weight: 500;
                                                            margin-left: auto;
                                                        }
                                                        button.fh-save:hover {
                                                            border: 1px solid orange;
                                                            background: transparent;
                                                            padding: 8px 23px;
                                                            color: orange;

                                                        }
                                                        .con {
                                                            display: inline;
                                                            position: relative;
                                                            padding-left: 27px;
                                                            padding-right: 37px;
                                                            width: 100%;
                                                            margin-bottom: 12px;
                                                            cursor: pointer;
                                                            font-size: 17px;
                                                            -webkit-user-select: none;
                                                            -moz-user-select: none;
                                                            -ms-user-select: none;
                                                            user-select: none;
                                                        }
                                                        label.con {
                                                            font-weight: 400;
                                                        }

                                                        /* Hide the browser's default checkbox */
                                                        .con input {
                                                            position: absolute;
                                                            opacity: 0;
                                                            cursor: pointer;
                                                            height: 0;
                                                            width: 0;
                                                        }

                                                        /* Create a custom checkbox */
                                                        .checkmark {
                                                            position: absolute;
                                                            top: 2px;
                                                            left: 0;
                                                            height: 20px;
                                                            width: 20px;
                                                            background-color: #e0e0e0;
                                                            border: 1px solid #b7b7b7;
                                                            border-radius: 3px;
                                                        }

                                                        /* On mouse-over, add a grey background color */
                                                        .con:hover input ~ .checkmark {
                                                            background-color: #ccc;
                                                        }

                                                        /* When the checkbox is checked, add a blue background */
                                                        .con input:checked ~ .checkmark {
                                                            background-color: #f67d27;
                                                            border: 1px solid #f67d27;
                                                        }

                                                        /* Create the checkmark/indicator (hidden when not checked) */
                                                        .checkmark:after {
                                                            content: "";
                                                            position: absolute;
                                                            display: none;
                                                        }

                                                        /* Show the checkmark when checked */
                                                        .con input:checked ~ .checkmark:after {
                                                            display: block;
                                                        }

                                                        /* Style the checkmark/indicator */
                                                        .con .checkmark:after {
                                                            left: 6px;
                                                            top: 2.4px;
                                                            width: 6px;
                                                            height: 11px;
                                                            border: solid white;
                                                            border-width: 0 3px 3px 0;
                                                            -webkit-transform: rotate(45deg);
                                                            -ms-transform: rotate(45deg);
                                                            transform: rotate(45deg);
                                                        }
                                                        .nav-tabs.fh-switch {
                                                            border-bottom: 1px solid  #bebebe;
                                                        }
                                                        .nav-item.show .nav-link, .nav-tabs .nav-link.active {
                                                            color: #e26d78;
                                                              background-color: #fff;
                                                              border: none;
                                                              border-bottom: 3px solid #e26d78;
                                                          }
                                                        .nav-item.show .nav-link, .nav-tabs .nav-link.active:hover {

                                                            color: #e26d78 !important;
                                                            background-color: #fff;
                                                            border: none;
                                                            border-bottom: 3px solid #e26d78;
                                                        }
                                                        .nav-tabs.fh-switch>li a{
                                                            color: #333;
                                                         }

                                                        .nav-tabs.fh-switch>li a:hover{
                                                            color: #e26d78 !important;
                                                        }
                                                        .nav-tabs>li a.active, .nav-tabs>li a.active:focus{
                                                            color: #e26d78;
                                                            cursor: default;
                                                            background-color: #fff;
                                                            border: none;
                                                            border-bottom: 3px solid #e26d78;
                                                        }
                                                        .nav-tabs .nav-link:hover {
                                                            border:none;
                                                        }
                                                        .form-group.f1 {
                                                            position: relative;
                                                            margin-bottom: 25px;
                                                            overflow: hidden;
                                                        }
                                                        label.e-lable {
                                                            position: absolute;
                                                            left: 5px;
                                                            top: 55%;
                                                            font-size: 20px;
                                                            transform: translateY(-50%) !important;
                                                            color: #e26d78;
                                                        }
                                                        label.e-lable1 {
                                                            position: absolute;
                                                            left: 5px;
                                                            top: 55%;
                                                            font-size: 20px;
                                                            transform: translateY(-50%) !important;
                                                            color:#21b1e7;
                                                        }
                                                        input.e-input {
                                                            width: 100%;
                                                            display: block;
                                                            border: none;
                                                            border-bottom: 1px solid #999;
                                                            padding: 6px 40px;
                                                            box-sizing: border-box;
                                                        }
                                                        input.e-input:focus {
                                                           outline: none;
                                                            box-shadow: none;
                                                        }
                                                    </style>
<!-- END HEAD -->



<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">

<div class="page-wrapper">

    <!-- start header -->@include ('admin.include.header')

<!-- end header -->

    <!-- start page container -->

    <div class="page-container">

        <!-- start sidebar menu -->@include ('admin.include.navbar')

    <!-- end sidebar menu -->

        <!-- start page content -->

        <div class="page-content-wrapper">

            <div class="page-content">

                <div class="page-bar">

                    <div class="page-title-breadcrumb">

                        <div class=" pull-left">

                            <div class="page-title"> Setting</div>

                        </div>

                        <ol class="breadcrumb page-breadcrumb pull-right">

                            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;<i class="fa fa-angle-right"></i> </li>

                            <li><a class="parent-item" href="">Setting</a>&nbsp;<i class="fa fa-angle-right"></i> </li>

                            <li class="active">All Setting</li>

                        </ol>

                    </div>

                </div>

                <div class="row">

                    <div class="col-md-12">

                        <div class="card card-box">

                            <div class="card-head">

                                <header>All Setting </header>

                                <div class="tools">

                                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>

                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>

                                </div>

                            </div>

                            <div class="card-body ">

                                <div class="row p-b-20">

                                    <div class="col-md-6 col-sm-6 col-6">

                                    </div>

                                </div>


                                <section class="section-start">

                                    <div class="container">

                                        <div class="row">

                                            <div class="col-sm-12">

                                                <div class=" c-items">

                                                    <h3 class="t-sms-detail">SMS Detail</h3>

                                                    <div class="container" style="padding: 30px 60px;">
                                                       <span style="color:green;display: none" id="smssuccess"></span>
                                                        <span style="color:red;display: none" id="smserror"></span>
        
                                                       
                                                            <div class="form-group f1">
                                                                <label style="margin-left: 5px;">Your User Id</label>
                                                                <label for="name" class="e-lable"><i class="fa fa-user"></i></label>
                                                                <input type="text" name="sms_userid" id="sms_userid" placeholder="Your User Id" value="{{$setting->sms_user_id}}" class="e-input">
                                                                <span style="color:red;visibility: hidden" id="sms_userid_err" ></span>
                                                            </div>
                                                            <div class="form-group f1">
                                                                <label style="margin-left: 5px;">Password</label>
                                                                <label for="name" class="e-lable"><i class="fa fa-eye"></i></label>
                                                                <input type="text" name="sms_password" id="sms_password" placeholder="Password" class="e-input" value="{{$setting->sms_password}}">
                                                                 <span style="color:red;visibility: hidden" id="sms_password_err" ></span>
                                                            </div>

                                                            <div class="form-group f1">
                                                                <label style="margin-left: 5px;">Sender Id</label>
                                                                <label for="name" class="e-lable"><i class="fa fa-user-plus"></i></label>
                                                                <input type="text" name="sms_sender" id="sms_sender" placeholder="Sender Id" class="e-input " value="{{$setting->sms_sender}}">
                                                                 <span style="color:red;visibility: hidden" id="sms_sender_err" ></span>
                                                            </div>
                                                            <div class="form-group f1">
                                                                <label style="margin-left: 5px;">SMS Url</label>
                                                                <label for="name" class="e-lable "><i class="fa fa-user-plus"></i></label>
                                                                <input type="text" name="sms_url" id="sms_url" placeholder="Sender Id" class="e-input sms_url" value="{{$setting->sms_url}}">
                                                                 <span style="color:red;visibility: hidden" id="sms_url_err" ></span>
                                                            </div>


                                                            <ul class="nav nav-tabs fh-switch">
                                                                <li class="nav-item"><a class="nav-link active" href="#i1" data-toggle="tab" style="font-size: 17px"><i class="fa fa-plane"></i>  Flights</a> </li>
                                                                <li class="nav-item"><a class="nav-link" href="#i2" data-toggle="tab" style="font-size: 17px"><i class="fa fa-hotel"></i> Hotels</a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                                <?php
                                                                
                                                                $fl = explode("-", $setting->sms_active); 
                                                                $hl = explode("-", $setting->hotel_sms_active); 
                                                                 ?>
                                                                <div class="tab-pane active" id="i1">
                                                                    <div class="" style="padding: 20px 5px;">
                                                                        <label class="con">Payment Order
                                                                            <input type="checkbox" name="flightchk" class="flightnchk" value="{{$fl[0]}}" <?php if($fl[0]=='1'){echo "checked" ; } ?> >
                                                                            <span class="checkmark"></span>
                                                                        </label>

                                                                        <label class="con">Ticket Booking
                                                                            <input type="checkbox" class="flightnchk" value="{{$fl[1]}}" <?php if($fl[1]=='1'){echo "checked" ; } ?> name="flightchk">
                                                                            <span class="checkmark"></span>
                                                                        </label>

                                                                        <label class="con">Cancellation 
                                                                            <input type="checkbox" class="flightnchk" name="flightchk" value="{{$fl[2]}}" <?php if($fl[2]=='1'){echo "checked" ; } ?>>
                                                                            <span class="checkmark"></span>
                                                                        </label>

                                                                        <label class="con">Refund
                                                                            <input type="checkbox" class="flightnchk" value="{{$fl[3]}}" <?php if($fl[3]=='1'){echo "checked" ; } ?> name="flightchk" >
                                                                            <span class="checkmark"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane" id="i2">
                                                                    <div class="" style="padding: 20px 5px;">
                                                                        <label class="con">Payment Order
                                                                            <input type="checkbox" name="hotelchk" class="hotelchk" value="{{$hl[0]}}" <?php if($hl[0]=='1'){echo "checked" ; } ?> >
                                                                            <span class="checkmark"></span>
                                                                        </label>

                                                                        <label class="con">Hotel Booking
                                                                            <input type="checkbox" name="hotelchk" class="hotelchk" value="{{$hl[1]}}" <?php if($hl[1]=='1'){echo "checked" ; } ?>>
                                                                            <span class="checkmark" ></span>
                                                                        </label>

                                                                        <label class="con">Cancellation
                                                                            <input type="checkbox" name="hotelchk" class="hotelchk" value="{{$hl[2]}}" <?php if($hl[2]=='1'){echo "checked" ; } ?> >
                                                                            <span class="checkmark"></span>
                                                                        </label>

                                                                        <label class="con">Refund
                                                                            <input type="checkbox" name="hotelchk" class="hotelchk" value="{{$hl[3]}}" <?php if($hl[3]=='1'){echo "checked" ; } ?> >
                                                                            <span class="checkmark"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6"></div>
                                                                <div class="col-sm-6">
                                                                    <button class="fh-save" id="sms_update">Update</button>
                                                                </div>
                                                            </div>
                                                       


                                                    </div>
                                                   
                                                </div>


                                                <section class="section-start">

                                                    <div class="c-items">

                                                        <h3 class="onword-flight">Flight Detail</h3>

                                                        <div class="container" style="padding: 30px 60px;">
                                                            <span style="color:green;display: none" id="flightsuccess"></span>
                                                        <span style="color:red;display: none" id="flighterror"></span>
                                                           
                                                                <div class="form-group f1">
                                                                    <label style="margin-left: 5px;">Meal Margin</label>
                                                                    <label for="name" class="e-lable1"><i class="fa fa-user"></i></label>
                                                                    <input type="text" name="mealmargin" id="mealmargin" placeholder="Meal Margin" class="e-input" value="{{$setting->flight_meal_margin}}">
                                                                    <span style="color:red;visibility: hidden" id="mealmargin_err" ></span>

                                                                </div>
                                                                <div class="form-group f1">
                                                                    <label style="margin-left: 5px;">Baggage Margin</label>
                                                                    <label for="name" class="e-lable1"><i class="fa fa-eye"></i></label>
                                                                    <input type="text" name="bagmargin" id="bagmargin" placeholder="Baggage Margin" class="e-input" value="{{$setting->flight_bag_margin}}">
                                                                    <span style="color:red;visibility: hidden" id="bagmargin_err" ></span>
                                                                </div>

                                                                <div class="form-group f1">
                                                                    <label style="margin-left: 5px;">Flight Margin</label>
                                                                    <label for="name" class="e-lable1"><i class="fa fa-user-plus"></i></label>
                                                                    <input type="text" name="flightmargin" id="flightmargin" placeholder="Flight Margin" class="e-input" value="{{$setting->flight_margin}}">
                                                                    <span style="color:red;visibility: hidden" id="flightmargin_err" ></span>
                                                                </div>

                                                              
                                                                <div class="row">
                                                                    <div class="col-sm-6"></div>
                                                                    <div class="col-sm-6">
                                                                        <button class="fh-save" id="flightupdate">Update</button>
                                                                    </div>
                                                                </div>

                                                           


                                                        </div>


                                                    </div>



                                                </section>

                                                <section class="section-start">

                                                    <div class="c-items">

                                                        <h3 class="t-sms-detail">Hotel Detail</h3>

                                                        <div class="container" style="padding: 30px 60px;">

                                                            <span style="color:green;display: none" id="hotelsuccess"></span>
                                                            <span style="color:red;display: none" id="hotelerror"></span>
                                                                <div class="form-group f1">
                                                                    <label style="margin-left: 5px;">Hotel Margin</label>
                                                                    <label for="name" class="e-lable"><i class="fa fa-user"></i></label>
                                                                    <input type="text" name="hotelmargin" id="hotelmargin" placeholder="Hotel Margin" value="{{$setting->hotel_margin}}" class="e-input">
                                                                    <span style="color:red;visibility: hidden" id="hotelmargin_err"></span>
                                                                </div>
                                                                


                                                                <div class="row">
                                                                    <div class="col-sm-6"></div>
                                                                    <div class="col-sm-6">
                                                                        <button class="fh-save"  id="hotelupdate">Update</button>
                                                                    </div>
                                                                </div>

                                                            


                                                        </div>


                                                    </div>



                                                </section>
                                            </div>


                                           {{-- <div class="col-sm-4">
                                                <div class="c-items">

                                                    <h3 class="t-fare">Fare Break-Up (Onword )</h3>

                                                    <div class="container">

                                                        <table class="table table-striped">

                                                            <tbody>

                                                            <tr>

                                                                <th>Base Fare </th>


                                                            </tr>

                                                            <tr>

                                                                <th>Tax</th>

                                                                <td><i class="fa fa-rupee"></i>  </td>

                                                            </tr>





                                                            <tr>

                                                                <th>Other Charges</th>


                                                            </tr>

                                                            </tbody>

                                                        </table>

                                                        <hr class="w-100">

                                                        <div class="t-total text-right">

                                                            <p>Total: <span style="font-size: 18px;width: 30px" class="t-badge">  </span>

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="c-items mt-5">

                                                    <h3 class="t-comm">Refund Amount</h3>

                                                    <div class="container">

                                                        <table class="table table-striped Commission">

                                                            <tbody>

                                                            <span style="color:red; display: none;font-size: 11px" id="refunderrorm"></span>

                                                            <span style="color:green; display: none;font-size: 11px" id="srefunderrorm"></span>






                                                            <tr>

                                                                <th>Refund Axdmount</th>

                                                                <td></td>

                                                            </tr>



                                                            <tr>

                                                                <th>Refund Date</th>

                                                                <td>


                                                                </td>

                                                            </tr>



                                                            </tbody>



                                                        </table>

                                                        <hr class="w-100">




                                                    </div>

                                                </div>
                                            </div>--}}


                                        </div>

                                    </div>

                                </section>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div class="modal fade text-center py-5" style="top:30px" id="myModal_charges">

            <div class="modal-dialog modal-md" role="document">

                <div class="modal-content" style="border-radius: 7px;">

                    <div class="modal-header h-bg">

                        <span class="close close-btn" data-dismiss="modal">&times;</span>



                    </div>

                    <div class="modal-body">



                        <h3 class="pt-3 mb-0 h-cancel success_modal"> </h3>



                        <button class="btn1 text-white mb-5 success_ok" style="margin-top:20px; margin-bottom: 0px !important">Submit</button>



                    </div>

                    <div class="modal-footer f-bg mt-3">

                        <span class="d-block mr-auto f-help "><i class="fa fa-phone"></i> Helpline:<a href="#"> 34356894454</a></span>

                        <span class="f-help"><img src="{{asset('assets/images/logo.png')}}" class="f-logo"></span>

                    </div>

                </div>

            </div>

        </div>

        <!-- end page content -->

    </div>

    <!-- end page container -->

    <!-- start footer -->@include ('admin.include.footer')

<!-- end footer -->

</div> @include ('admin.include.downjs')</body>

<script>

    $(document).ready(function () {



        $(".leadage").keypress(function (e) {



            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {

                return false;

            }

        });

    });



</script>

<script>

    $(document).on('click','#refundaccept',function(){

        $('#refunderrorm').hide();

        $('#refunderror').hide();

        $('#srefunderrorm').hide();

        var refundamount = $('.refundamount').val();

        var bookingid = $('#bookingid').val();

        var changereq = $('#changereq').val();

        var paxname =$('#paxname').val();

        var leadphone = $('#leadphone').val();

        var leademail = $('#leademail').val();

        if(refundamount=='')

        {

            $('#refunderror').text("Please Enter Refund Amount");

            $('#refunderror').show();

        }

        else

        {

            $('#refunderror').hide();

            $.ajax({

                url:"{{route('hotelrefund')}}",

                data : { 'refundamount' : refundamount,

                    'bookingid'      : bookingid,

                    'changereq' :changereq,

                    'paxname' :paxname,

                    'leadphone':leadphone,

                    'leademail':leademail,

                },

                type : "GET",

                success : function(response)

                {

                    if(response=='success')

                    {

                        $('#srefunderrorm').text("Successfully Refund Amount Submit");

                        $('#srefunderrorm').show();

                        $('#myModal_charges').modal('show');

                        $('.success_modal').text("Successfully Refund Amount Submit");



                    }

                    else

                    {

                        $('#refunderrorm').text("Unable to refund this amount");

                        $('#refunderrorm').show();

                    }

                }



            });



        }

    })

</script>

<script type="text/javascript">

    $(document).on('click', ".success_ok", function ()

    {

        $('#myModal_charges').modal('hide');

        window.location="{{url('admin/canceldetail')}}";

    });

</script>
<script>
    $(document).on('click','input[name="flightchk"]',function(){
        if($(this).prop("checked") == true){
            $(this).val(1);
              
            }
            else if($(this).prop("checked") == false){
                $(this).val(0);
                
            }
    });
    $(document).on('click','input[name="hotelchk"]',function(){
        if($(this).prop("checked") == true){
            $(this).val(1);
              
            }
            else if($(this).prop("checked") == false){
                $(this).val(0);
                
            }
    });
    $(document).on('click','#sms_update',function(){
        var sms_userid = $('#sms_userid').val();
        var sms_password =$('#sms_password').val();
        var sms_sender = $('#sms_sender').val();
        var sms_url=$('#sms_url').val();
        var smslist = "";
        $('input[name=flightchk]').each(function () {
            smslist+=$(this).val()+"-";
        });
        var hotellist = "";
        $('input[name=hotelchk]').each(function () {
            hotellist+=$(this).val()+"-";
        });
        
        if(sms_userid=='')
        {
            $('#sms_userid_err').text("Please Enter Sms User Id");
            $('#sms_userid_err').show();
        }
        else if(sms_password=='')
        {
            $('#sms_password_err').text("Please Enter Sms Password");
            $('#sms_password_err').show();
        }
        else if(sms_sender=='')
        {
            $('#sms_sender_err').text("Please Enter Sms Sender id");
            $('#sms_sender_err').show();
        }
        else if(sms_url=='')
        {
            $('#sms_url_err').text("Please Enter Sms Url");
            $('#sms_url_err').show();
        }
        else
        {
            $('#sms_userid_err').hide();
            $('#sms_password_err').hide();
            $('#sms_sender_err').hide();
            $('#sms_url_err').hide();
            $.ajax({
                    url : "{{route('smsrequest')}}",
                    data : {
                        'sms_userid' :sms_userid,
                        'sms_password' :sms_password,
                        'sms_sender':sms_sender,
                        'sms_url':sms_url,
                        'smslist':smslist,
                        'hotellist':hotellist,
                        },
                    type : 'GET',
                    success : function(response)
                    {
                        if(response=='success')
                       {
                            $('#smssuccess').text("Successfully updated");
                            $('#smssuccess').show();
                       }
                       else
                       {
                         $('#smserror').text("Unalbe to updated");
                            $('#smserror').show();
                       }
                    }
            })
        }

    })
</script>
<script>
    $(document).on('click','#hotelupdate',function(){
        var hotelmargin = $('#hotelmargin').val();
        if(hotelmargin=='')
        {
            $('#hotelmargin_err').text("Please Enter margin");
            $('#hotelmargin_err').show();
        }
        else
        {
            $('#hotelmargin_err').hide();
            $.ajax({
                    url : "{{route('hotelmargin')}}",
                    data : {
                        'hotelmargin' :hotelmargin,
                        
                        },
                    type : 'GET',
                    success : function(response)
                    {
                       if(response=='success')
                       {
                            $('#hotelsuccess').text("Successfully updated");
                            $('#hotelsuccess').show();
                       }
                       else
                       {
                         $('#hotelerror').text("Unalbe to updated");
                            $('#hotelerror').show();
                       }
                    }
            })
        }
    });
    //flight
      $(document).on('click','#flightupdate',function(){
        var mealmargin = $('#mealmargin').val();
        var bagmargin =$('#bagmargin').val();
        var flightmargin = $('#flightmargin').val();
       
       
        if(mealmargin=='')
        {
            $('#mealmargin_err').text("Please Enter Sms User Id");
            $('#mealmargin_err').show();
        }
        else if(bagmargin=='')
        {
            $('#bagmargin_err').text("Please Enter Sms Password");
            $('#bagmargin_err').show();
        }
        else if(flightmargin=='')
        {
            $('#flightmargin_err').text("Please Enter Sms Sender id");
            $('#flightmargin_err').show();
        }
        
        else
        {
            $('#flightmargin_err').hide();
            $('#bagmargin_err').hide();
            $('#mealmargin_err').hide();
            $.ajax({
                    url : "{{route('flightmargin')}}",
                    data : {
                        'mealmargin' :mealmargin,
                        'bagmargin' :bagmargin,
                        'flightmargin':flightmargin,
                        
                        },
                    type : 'GET',
                    success : function(response)
                    {
                       
                        if(response=='success')
                       {
                            $('#flightsuccess').text("Successfully updated");
                            $('#flightsuccess').show();
                       }
                       else
                       {
                         $('#flighterror').text("Unalbe to updated");
                            $('#flighterror').show();
                       }
                    }
            })
        }

    })
</script>
</script>


</html>