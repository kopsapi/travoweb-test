
@include ('admin.include.topcss')

 <?php

            use App\Http\Controllers\AdminController;

            ?>

    <!-- Datatables Js-->
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://shareurcodes.com/css/app-mdl.css"/>
     <link rel="stylesheet" type="text/css" href="https://shareurcodes.com/css/app-mdl.css"/>
     <link rel="stylesheet" type="text/css" href="https://shareurcodes.com/css/app.css">
    


<!-- search box container starts  -->
<div class="page-wrapper">
    @include ('admin.include.header')
    <div class="page-container">
        @include ('admin.include.navbar')
        <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Package  List</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">package  List</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Package  List</li>
                            </ol>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-box">
                                 <div class="card-head">
                                    <header>Package  List</header>
                                
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                 <div class="card-body ">
                                     <div class="table-scrollable" id="tag_container">
                                            <div class="row">
                                                <div class="col-lg-12 ">
                                                    <table id="table" class="table table-hover table-checkable order-column full-width">
                                                      <thead>
                                                        <tr>
                                                          <th>#</th>
                                                          <th class="center"> Package Id </th>
                                                          <th class="center"> Package Type </th>
                                                            <th class="center"> Country Name </th>
                                                            <th class="center"> Package Name </th>
                                                            <th class="center"> Package Price </th>
                                                            <th class="center"> Action </th>
                                                        </tr>
                                                      </thead>
                                                      <tbody id="tablecontents">
                                                        @if($data=='yes')
                                                        @foreach($pckglist as $task)
                                                        <?php
                                                        $cityname=AdminController::chkcityname($task->countryname);
                                                            if($task->countryvalue=='1')
                                                            {
                                                                $pckgname="Domestic";
                                                            }
                                                            
                                                            else
                                                            {
                                                                $pckgname="International";
                                                            }
                                                            
                                                           
                                                            ?>
                                                        <tr class="row1 rownew_{{$task->id}}" data-id="{{ $task->id }}">
                                                          <td>
                                                            <div style="color:rgb(124,77,255); padding-left: 10px; float: left; font-size: 20px; cursor: pointer;" title="change display order">
                                                            <i class="fa fa-ellipsis-v"></i>
                                                            <i class="fa fa-ellipsis-v"></i>
                                                            </div>
                                                          </td>
                                                          <td class="center">{{$task->pckg_id}} </a></td>
                                                        <td class="center">{{$pckgname}}</td>
                                                        <td class="center">{{$cityname}}</td>
                                                        <td class="center">{{$task->packagename}}</td>
                                                        <td class="center">{{$task->newprice}}</td>
                                                        <td class="center"><a class="book-id-btn btn btn-primary" href="{{url('admin/package_edit/'.$task->id)}}">Edit</a>  <button class="btn btn-danger delnew" id="{{$task->id}}">Delete</button></td>
                                                        </tr>

                                                        @endforeach
                                                        @else
                                                          <h4>No data Show</h4>
                                                        @endif
                                                      </tbody>                  
                                                    </table>
                                                </div>
                                            </div> 
                                           
                                            
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div> 
</div>
 @include ('admin.include.footer')
</div>
     @include ('admin.include.downjs')


  <!-- jQuery UI -->
<!--   <script type="text/javascript" src="https://shareurcodes.com/js/app.js"></script> -->
  <script type="text/javascript" src="//code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
 
  <!-- Datatables Js-->
  <script type="text/javascript" src="//cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>

  <script type="text/javascript">
  $(function () {
    $("#table").DataTable();

    $( "#tablecontents" ).sortable({
      items: "tr",
      cursor: 'move',
      opacity: 0.6,
      update: function() {
          sendOrderToServer();
      }
    });

    function sendOrderToServer() {

      var order = [];
      $('tr.row1').each(function(index,element) {
        order.push({
          id: $(this).attr('data-id'),
          position: index+1
        });
      });

      $.ajax({
        type: "POST", 
        dataType: "json", 
        url: "{{ route('sortpackagelist') }}",
        data: {
          order:order,
          _token: '{{csrf_token()}}'
        },
        success: function(response) {
            if (response.status == "success") {
              console.log(response);
            } else {
              console.log(response);
            }
        }
      });

    }
  });

</script>

<script>
  $(document).on('click','.delnew',function(){
    var id=this.id;
      swal({
           title: "Are you sure?",
           text: "You will not be able to recover this Package !",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           confirmButtonText: "Yes, delete it!",
           cancelButtonText: "No, cancel!",
           closeOnConfirm: false,
           closeOnCancel: false
            }, function(isConfirm) {
           if (isConfirm) 
           {
              $.ajax({
                      url:"{{route('tour_delete_pack')}}",
                      data:{'id':id},
                      type:"GET",
                      success:function(data)
                      {
                        if(data=="success")
                        {
                              swal({
                                     title: "Successfully Package Delete",
                                     text: "",
                                     type: "success",
                                     showCancelButton: false,
                                     confirmButtonColor: "#DD6B55",
                                     confirmButtonText: "Ok",
                                     cancelButtonText:false,
                                     closeOnConfirm: false,
                                     closeOnCancel: false
                                 }, function(isConfirm) {
                                     if (isConfirm) {
                                        $(".rownew_"+id).remove();
                                       window.location="{{route('package_list')}}";

                                   } 
                             });
                        } 
                        else
                        {
                            swal("Error !", "Unable to delete this package!", "error");
                        } 
                      }
              });
            }
            else
            {
               swal("Cancel !", "Your Package is Safe!", "error");
            }
            });

  })
</script>