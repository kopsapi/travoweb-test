@include ('admin.include.topcss')

<style type="text/css">

    .pagination>li>a,

    .pagination>li>span {

        position: relative;

        float: left;

        padding: 6px 12px;

        line-height: 1.42857143;

        color: #337ab7;

        text-decoration: none;

        background-color: #fff;

        border: 1px solid #ddd;

    }



    .pagination>.active>span {

        font-weight: bold;

        color: #fff;

        background-color: #337ab7;

    }



    ul.pagination {

        float: right;

        margin-right: 20px;

    }

</style>

<style>

    .section-start {

        padding: 30px 0;

    }



    .t-sms-detail {

        text-align: center;

        padding: 10px;

        background-color: #e26d78;

        color: white;

    }



    .c-items {

        border: 1px solid #c9c9c9;

        border-radius: 5px;

    }



    .onword-flight {

        text-align: left;

        color: white;

        padding: 10px;

        background-color: #21b1e7;

    }



    .t-note {

        padding: 13px;

        margin-top: -8px;

        font-size: 16px;

        border-left: 5px solid #21b1e7;

    }



    .flight-gif {

        width: 50px;

    }



    .pass-detail {

        text-align: center;

        padding: 10px;

        color: white;

        background-color: #e26d78;

    }



    .pass-contact {

        text-align: center;

        padding: 10px;

        background-color: #e26d78;

        color: white;

    }



    .t-total {

        font-size: 17px;

    }



    .t-badge {

        background-color: #ff6981;

        color: white;

        padding: 5px 10px;

    }



    .t-fare {

        text-align: center;

        padding: 10px;

        background-color: #fa9e1b;

        color: white;

    }



    .t-comm {

        text-align: center;

        padding: 10px;

        background-color: #fa9e1b;

        color: white;

    }



    .t-badge-comm {

        padding: 5px 10px;

        border-radius: 20px;

        font-size: 13px;

        background-color: #21b1e7;

    }



    .Commission tr td {

        text-align: center;

    }

    .modal-header.h-bg {

        background: #31124b;

        padding: 10px 21px;

        border-bottom-color: white;

    }

    .modal-footer.f-bg {

        background: #e2e3ea;

    }

    p.t-id-info {

        margin-top: 10px;

        color: red !important;

        font-weight: 600;

    }



    .btn1 {

        font-size: 17px;

        font-weight: 500;

        color: #fff;

        text-transform: uppercase;

        background: #fa9e1b;

        border: none;

        outline: none;

        padding: 8px 17px;

        border-radius: 5px;

        cursor: pointer;

    }

</style>
 <style>
                                                        button.fh-save {
                                                            border: none;
                                                            border: 1px solid orange;
                                                            background: orange;
                                                            padding: 8px 23px;
                                                            color: white;
                                                            border-radius: 7px;
                                                            font-size: 18px;
                                                            display: flex;
                                                            font-weight: 500;
                                                            margin-left: auto;
                                                        }
                                                        button.fh-save:hover {
                                                            border: 1px solid orange;
                                                            background: transparent;
                                                            padding: 8px 23px;
                                                            color: orange;

                                                        }
                                                        .con {
                                                            display: inline;
                                                            position: relative;
                                                            padding-left: 27px;
                                                            padding-right: 37px;
                                                            width: 100%;
                                                            margin-bottom: 12px;
                                                            cursor: pointer;
                                                            font-size: 17px;
                                                            -webkit-user-select: none;
                                                            -moz-user-select: none;
                                                            -ms-user-select: none;
                                                            user-select: none;
                                                        }
                                                        label.con {
                                                            font-weight: 400;
                                                        }

                                                        /* Hide the browser's default checkbox */
                                                        .con input {
                                                            position: absolute;
                                                            opacity: 0;
                                                            cursor: pointer;
                                                            height: 0;
                                                            width: 0;
                                                        }

                                                        /* Create a custom checkbox */
                                                        .checkmark {
                                                            position: absolute;
                                                            top: 2px;
                                                            left: 0;
                                                            height: 20px;
                                                            width: 20px;
                                                            background-color: #e0e0e0;
                                                            border: 1px solid #b7b7b7;
                                                            border-radius: 3px;
                                                        }

                                                        /* On mouse-over, add a grey background color */
                                                        .con:hover input ~ .checkmark {
                                                            background-color: #ccc;
                                                        }

                                                        /* When the checkbox is checked, add a blue background */
                                                        .con input:checked ~ .checkmark {
                                                            background-color: #f67d27;
                                                            border: 1px solid #f67d27;
                                                        }

                                                        /* Create the checkmark/indicator (hidden when not checked) */
                                                        .checkmark:after {
                                                            content: "";
                                                            position: absolute;
                                                            display: none;
                                                        }

                                                        /* Show the checkmark when checked */
                                                        .con input:checked ~ .checkmark:after {
                                                            display: block;
                                                        }

                                                        /* Style the checkmark/indicator */
                                                        .con .checkmark:after {
                                                            left: 6px;
                                                            top: 2.4px;
                                                            width: 6px;
                                                            height: 11px;
                                                            border: solid white;
                                                            border-width: 0 3px 3px 0;
                                                            -webkit-transform: rotate(45deg);
                                                            -ms-transform: rotate(45deg);
                                                            transform: rotate(45deg);
                                                        }
                                                        .nav-tabs.fh-switch {
                                                            border-bottom: 1px solid  #bebebe;
                                                        }
                                                        .nav-item.show .nav-link, .nav-tabs .nav-link.active {
                                                            color: #e26d78;
                                                              background-color: #fff;
                                                              border: none;
                                                              border-bottom: 3px solid #e26d78;
                                                          }
                                                        .nav-item.show .nav-link, .nav-tabs .nav-link.active:hover {

                                                            color: #e26d78 !important;
                                                            background-color: #fff;
                                                            border: none;
                                                            border-bottom: 3px solid #e26d78;
                                                        }
                                                        .nav-tabs.fh-switch>li a{
                                                            color: #333;
                                                         }

                                                        .nav-tabs.fh-switch>li a:hover{
                                                            color: #e26d78 !important;
                                                        }
                                                        .nav-tabs>li a.active, .nav-tabs>li a.active:focus{
                                                            color: #e26d78;
                                                            cursor: default;
                                                            background-color: #fff;
                                                            border: none;
                                                            border-bottom: 3px solid #e26d78;
                                                        }
                                                        .nav-tabs .nav-link:hover {
                                                            border:none;
                                                        }
                                                        .form-group.f1 {
                                                            position: relative;
                                                            margin-bottom: 25px;
                                                            overflow: hidden;
                                                        }
                                                        label.e-lable {
                                                            position: absolute;
                                                            left: 5px;
                                                            top: 55%;
                                                            font-size: 20px;
                                                            transform: translateY(-50%) !important;
                                                            color: #e26d78;
                                                        }
                                                        label.e-lable1 {
                                                            position: absolute;
                                                            left: 5px;
                                                            top: 55%;
                                                            font-size: 20px;
                                                            transform: translateY(-50%) !important;
                                                            color:#21b1e7;
                                                        }
                                                        input.e-input {
                                                            width: 100%;
                                                            display: block;
                                                            border: none;
                                                            border-bottom: 1px solid #999;
                                                            padding: 6px 40px;
                                                            box-sizing: border-box;
                                                        }
                                                        input.e-input:focus {
                                                           outline: none;
                                                            box-shadow: none;
                                                        }
                                                    </style>
<!-- END HEAD -->



<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">

<div class="page-wrapper">

    <!-- start header -->@include ('admin.include.header')

<!-- end header -->

    <!-- start page container -->

    <div class="page-container">

        <!-- start sidebar menu -->@include ('admin.include.navbar')

    <!-- end sidebar menu -->

        <!-- start page content -->

        <div class="page-content-wrapper">

            <div class="page-content">

                <div class="page-bar">

                    <div class="page-title-breadcrumb">

                        <div class=" pull-left">

                            <div class="page-title"> Add testimonial </div>

                        </div>

                        <ol class="breadcrumb page-breadcrumb pull-right">

                            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;<i class="fa fa-angle-right"></i> </li>

                            <li><a class="parent-item" href="">testimonial </a>&nbsp;<i class="fa fa-angle-right"></i> </li>

                            <li class="active">testimonial </li>

                        </ol>

                    </div>

                </div>

                <div class="row">

                    <div class="col-md-12">

                        <div class="card card-box">

                            <div class="card-head">

                                <header><meta http-equiv="Content-Type" content="text/html; charset=utf-8">testimonial  </header>

                                <div class="tools">

                                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>

                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>

                                </div>

                            </div>

                            <div class="card-body ">

                                <div class="row p-b-20">

                                    <div class="col-md-6 col-sm-6 col-6">

                                    </div>

                                </div>

                               
                                @if($data==0)
                                <section class="section-start">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class=" c-items">
                                                     <div class="container" style="padding: 30px 60px;">
                                                       <span style="color:green;display: none" id="success"></span>
                                                        <span style="color:red;display: none" id="error"></span>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                     <div class="form-group f1">
                                                                        <label style="margin-left: 5px;">Name</label>
                                                                       
                                                                        <input type="text" name="test_name" id="test_name" placeholder="" class="e-input " value="">
                                                                         <span style="color:red;visibility: hidden" id="test_name_err" ></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                     <div class="form-group f1">
                                                                        <label style="margin-left: 5px;">City</label>
                                                                       
                                                                        <input type="text" name="city_name" id="city_name" placeholder="" class="e-input " value="">
                                                                         <span style="color:red;visibility: hidden" id="city_name_err" ></span>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                             <div class="row">
                                                                <div class="col-md-6">
                                                                     <div class="form-group f1">
                                                                        <label style="margin-left: 5px;">Address</label>
                                                                        <textarea name="test_address" id="test_address" placeholder="" class="form-control " value="" ></textarea>
                                                                       
                                                                         <span style="color:red;visibility: hidden" id="test_address_err" ></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                     <form enctype="multipart/form-data" id="advertisment">
                                                                        <div class="form-group f1">
                                                                            <label style="margin-left: 5px;">Image</label>
                                                                            <label for="name" class="e-lable"><i class="fa fa-user"></i></label>

                                                                           <input type="hidden" class="e-input" id="adv_atchmnt_image" name="adv_atchmnt_image">
                                                                            <input type="file" class=" e-input" id="adv_atchmnt" name="adv_atchmnt" onchange="readURL(this);">&nbsp;&nbsp;<span id="bar" role="progressbar" style="width:0%;font-weight:bold;display:none; color:#88b93c;">0%</span>
                                                                            <span style="color:red;visibility: hidden" id="adv_err" ></span>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                                    <div class="col-xs-2" id="img_div" style="display: none;">
                                                                        <div class="form-group col-xs-9">
                                                                            &nbsp;
                                                                        </div>
                                                                        <div class="form-group col-xs-2">
                                                                            <img src="" alt=" Attachment" class="media-object" style="width: 50px;height: auto;border-radius: 4px;box-shadow: 0 1px 3px rgba(0,0,0,.15);" id="adv_image">
                                                                        </div>
                                                                        <div class="form-group col-xs-1">
                                                                            &nbsp;
                                                                        </div>
                                                                    </div>
                                                               
                                                                
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group f1">
                                                                        <label style="margin-left: 5px;">Content</label>
                                                                        <textarea name="test_content" id="test_content" placeholder="" style="height:200px" class="form-control " value="" ></textarea>
                                                                       
                                                                         <span style="color:red;visibility: hidden" id="test_content_err" ></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group f1">
                                                                     <label style="margin-left: 5px;margin-right: 20px;"><input type="radio" name="statusdata" value="0" class="form-control cstatus" checked style="display: inline !important; width: auto;"> Active</label>
                                                                  <label style="margin-left: 5px;margin-right: 20px;">  <input type="radio"  style="display: inline !important; width: auto;" name="statusdata" value="1" class="form-control cstatus"> De-Active</label>
                                                                </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <button class="fh-save" id="testi_submit">Submit</button>
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                           
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>

                                    </div>
                                </section>
                                @else
                               <section class="section-start">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class=" c-items">
                                                     <div class="container" style="padding: 30px 60px;">
                                                       <span style="color:green;display: none" id="success"></span>
                                                        <span style="color:red;display: none" id="error"></span>
                                                            <div class="row">
                                                                <input type="hidden" id="testid" value="{{$testidata->id}}">
                                                                <div class="col-md-6">
                                                                     <div class="form-group f1">
                                                                        <label style="margin-left: 5px;">Name</label>
                                                                       
                                                                        <input type="text" name="test_name" id="test_name1" placeholder="" class="e-input " value="{{$testidata->name}}">
                                                                         <span style="color:red;visibility: hidden" id="test_name1_err" ></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                     <div class="form-group f1">
                                                                        <label style="margin-left: 5px;">City</label>
                                                                       
                                                                        <input type="text" name="city_name" id="city_name1" placeholder="" class="e-input " value="{{$testidata->city}}">
                                                                         <span style="color:red;visibility: hidden" id="city_name1_err" ></span>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                             <div class="row">
                                                                <div class="col-md-6">
                                                                     <div class="form-group f1">
                                                                        <label style="margin-left: 5px;">Address</label>
                                                                        <textarea name="test_address" id="test_address1" placeholder="" class="form-control " value="" >{{$testidata->address}}</textarea>
                                                                       
                                                                         <span style="color:red;visibility: hidden" id="test_address1_err" ></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                       <form enctype="multipart/form-data" id="advertisment1">
                                                                            <div class="form-group f1">
                                                                                <label style="margin-left: 5px;">Image</label>
                                                                                <label for="name" class="e-lable"><i class="fa fa-user"></i></label>

                                                                               <input type="hidden" class="e-input" id="adv_atchmnt_image1" name="adv_atchmnt_image1" value="{{$testidata->image}}">
                                                                                <input type="file" class=" e-input" id="adv_atchmnt1" name="adv_atchmnt1" onchange=" return mainimageface();">&nbsp;&nbsp;<span id="bar1" role="progressbar" style="width:0%;font-weight:bold;display:none; color:#88b93c;">0%</span>
                                                                                <span style="color:red;visibility: hidden" id="adv_err1" ></span>
                                                                            </div>
                                                                        </form>
                                                                </div>
                                                                    <div class="col-xs-2" id="img_div">
                                                                        <div class="form-group col-xs-9">
                                                                            &nbsp;
                                                                        </div>
                                                                        <div class="form-group col-xs-2">
                                                                            <img src="{{asset('assets/uploads/')}}/{{$testidata->image}}" alt=" Attachment" class="media-object" style="width: 50px;height: auto;border-radius: 4px;box-shadow: 0 1px 3px rgba(0,0,0,.15);" id="adv_image1">
                                                                        </div>
                                                                        <div class="form-group col-xs-1">
                                                                            &nbsp;
                                                                        </div>
                                                                    </div>
                                                               
                                                                
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group f1">
                                                                        <label style="margin-left: 5px;">Content</label>
                                                                        <textarea name="test_content" id="test_content1" placeholder="" style="height:200px" class="form-control " value="" >{{$testidata->comment}}</textarea>
                                                                       
                                                                         <span style="color:red;visibility: hidden" id="test_content1_err" ></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group f1">
                                                                        @if($testidata->status==0)

                                                                     <label style="margin-left: 5px;margin-right: 20px;"><input type="radio" name="statusdata1" value="0" class="form-control cstatus" checked style="display: inline !important; width: auto;"> Active</label>
                                                                  <label style="margin-left: 5px;margin-right: 20px;">  <input type="radio"  style="display: inline !important; width: auto;" name="statusdata1" value="1" class="form-control cstatus"> De-Active</label>
                                                                  @else
                                                                  <label style="margin-left: 5px;margin-right: 20px;"><input type="radio" name="statusdata1" value="0" class="form-control cstatus"  style="display: inline !important; width: auto;"> Active</label>
                                                                  <label style="margin-left: 5px;margin-right: 20px;">  <input type="radio"  style="display: inline !important; width: auto;" name="statusdata1" checked value="1" class="form-control cstatus"> De-Active</label>
                                                                  @endif

                                                                </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <button class="fh-save" id="testi_update">Update</button>
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                           
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>

                                    </div>
                                </section>
                                @endif
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

      

        <!-- end page content -->

    </div>

    <!-- end page container -->

    <!-- start footer -->@include ('admin.include.footer')

<!-- end footer -->

</div> @include ('admin.include.downjs')</body>

<script>
      $(document).on('change', "#adv_atchmnt", function ()
        {
            var fileName = $("#adv_atchmnt").val();
            if(fileName == "") 
            {
                $('#adv_atchmnt_image').val("");
                $('#img_div').hide();
                $('#adv_image').attr('src', '');
                $('#bar').hide();
            }
            else
            {
                upload(this);
            }
        });
        function upload(img) 
        {
            var form_data = new FormData();
            form_data.append('file', img.files[0]);
            form_data.append('_token', '{{csrf_token()}}');
            $('#bar').show();
           
            $.ajax(
            {
                url: '{{route("advertisment_image_upload")}}',
                data: form_data,
                mimeType: "multipart/form-data",
                type: 'POST',
                contentType: false,
                processData: false,
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                             $('#bar').text(percentComplete + '%');
                            $('#bar').css('width', percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                success: function (data) 
                {

                    if(data=="no")
                    {
                        $('#adv_err').text('Attachment should be JPEG|JPG');
                        $('#adv_err').css('visibility', 'visible');
                        $('#adv_err').focus();
                    }
                    else
                    {
                        $('#adv_err').text('');
                        $('#adv_err').css('visibility', 'hidden');
                        $('#adv_atchmnt_image').val(data);
                        var path='assets/uploads/'+data;
                       $("#adv_image").attr('src',"{{asset('assets/uploads')}}/"+data);
                        $('#img_div').show();
                    }
                }
            });
        }
</script>
<script>
    $(document).on('click','#testi_submit',function(){
        var test_name=$('#test_name').val();
        var city_name=$('#city_name').val();
        var test_address=$('#test_address').val();
        var image=$('#adv_atchmnt_image').val();
        var test_content=$('#test_content').val();
        var statusdata = $("input[name='statusdata']:checked").val();
        var error=0;
        if(test_name=="")
        {   
            error++;
            $('#test_name_err').text("Please Enter Name");
            $('#test_name_err').css('visibility', 'visible');
        }
        else
        {
            $('#test_name_err').css('visibility', 'hidden');
        }
        if(test_content=="")
        {
            error++;
            $('#test_content_err').text("Please Enter Content");
            $('#test_content_err').css('visibility', 'visible');
        }
        else
        {
             $('#test_content_err').css('visibility', 'hidden');
        }
        if(error==0)
        {
            $.ajax({
                    url:"{{route('insert_testi')}}",
                    data:{'test_name':test_name,
                          'city_name':city_name,
                           'test_address':test_address,
                            'image':image,
                            'test_content':test_content,
                            'statusdata':statusdata,
                        },
                    type:'GET',
                    success:function(data)
                    {
                        if(data=="success")
                          {
                              swal({
                                   title: "Successfully Add Testimonial",
                                   text: "",
                                   type: "success",
                                   showCancelButton: false,
                                   confirmButtonColor: "#DD6B55",
                                   confirmButtonText: "Ok",
                                   cancelButtonText:false,
                                   closeOnConfirm: false,
                                   closeOnCancel: false
                               }, function(isConfirm) {
                                   if (isConfirm) {
                                    location.reload();
                                 } 
                           });
                           
                          }
                          else
                          {
                                swal("Error!", "Unable to Add Testimonial!", "error");
                          }
                    }
            })
        }

    })
</script>
<!-- update data -->
<script>
      $(document).on('change', "#adv_atchmnt1", function ()
        {
           
            var fileName = $("#adv_atchmnt1").val();
            if(fileName == "") 
            {
                $('#adv_image1').attr('src', '');
                $('#bar1').hide();
            }
            else
            {
                mainimageface(this);
            }
        });
        function mainimageface(img) 
        {

            var form_data = new FormData();
            form_data.append('file', img.files[0]);
            form_data.append('_token', '{{csrf_token()}}');
            $('#bar1').show();
           
            $.ajax(
            {

                url: '{{route("advertisment_image_upload")}}',
                data: form_data,
                mimeType: "multipart/form-data",
                type: 'POST',
                contentType: false,
                processData: false,
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                             $('#bar1').text(percentComplete + '%');
                            $('#bar1').css('width', percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                success: function (data) 
                {
                   
                    if(data=="no")
                    {
                        $('#adv_err1').text('Attachment should be JPEG|JPG');
                        $('#adv_err1').show();
                        $('#adv_err1').focus();
                    }
                    else
                    {
                        $('#adv_err1').text('');
                        $('#adv_err1').hide();
                        $('#adv_atchmnt_image1').val(data);
                        var path='assets/uploads/'+data;
                       $("#adv_image1").attr('src',"{{asset('assets/uploads')}}/"+data);
                        // $('#img_div').show();
                    }
                }
            });
        }
</script>

<script>
    $(document).on('click','#testi_update',function(){
        var test_name=$('#test_name1').val();
        var city_name=$('#city_name1').val();
        var test_address=$('#test_address1').val();
        var image=$('#adv_atchmnt_image1').val();
        var test_content=$('#test_content1').val();
        var statusdata = $("input[name='statusdata1']:checked").val();
        var testid=$('#testid').val();
        var error=0;
        if(test_name=="")
        {   
            error++;
            $('#test_name1_err').text("Please Enter Name");
            $('#test_name1_err').css('visibility', 'visible');
        }
        else
        {
            $('#test_name1_err').css('visibility', 'hidden');
        }
        if(test_content=="")
        {
            error++;
            $('#test_content1_err').text("Please Enter Content");
            $('#test_content1_err').css('visibility', 'visible');
        }
        else
        {
             $('#test_content1_err').css('visibility', 'hidden');
        }
        if(error==0)
        {
            $.ajax({
                    url:"{{route('update_testi')}}",
                    data:{'test_name':test_name,
                          'city_name':city_name,
                           'test_address':test_address,
                            'image':image,
                            'test_content':test_content,
                            'statusdata':statusdata,
                            'testid':testid,
                        },
                    type:'GET',
                    success:function(data)
                    {
                        if(data=="success")
                          {
                              swal({
                                   title: "Successfully Update Testimonial",
                                   text: "",
                                   type: "success",
                                   showCancelButton: false,
                                   confirmButtonColor: "#DD6B55",
                                   confirmButtonText: "Ok",
                                   cancelButtonText:false,
                                   closeOnConfirm: false,
                                   closeOnCancel: false
                               }, function(isConfirm) {
                                   if (isConfirm) {
                                    window.location="{{route('list-testimonial')}}";
                                 } 
                           });
                           
                          }
                          else
                          {
                                swal("Error!", "Unable to Update Testimonial!", "error");
                          }
                    }
            })
        }

    })
</script>
</html>