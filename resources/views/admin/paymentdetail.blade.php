
@include ('admin.include.topcss')
<style type="text/css">
    .pagination>li>a, .pagination>li>span 
    {
        position: relative;
        float: left;
        padding: 6px 12px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }
    .pagination>.active>span
    {
        font-weight:bold;
        color:#fff;
        background-color: #337ab7;
    }
    ul.pagination 
    {
        float: right;
        margin-right: 20px;
    }

</style>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
		@include ('admin.include.header')
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
			@include ('admin.include.navbar')
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">All Payments</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">Payments</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">All Payments</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-md-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header>All Payments </header>
                                
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                    <div class="row p-b-20">
                                        <div class="col-md-6 col-sm-6 col-6">
                                            <!-- <div class="btn-group">
                                                <a href="new_booking.html" id="addRow" class="btn btn-info">
                                                    Add New <i class="fa fa-plus"></i>
                                                </a>
                                            </div> -->
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-6">
                                            <div class="btn-group pull-right">
                                            	<div class="card-tools">
					                              <div class="input-group input-group-sm" style="width: 150px;">
					                                 <input type="text" name="search" id="search" class="form-control float-right" placeholder="Search" value="">
					                                 <div class="input-group-append">
					                                    <button type="button" id="search-btn" class="btn btn-default"><i class="fa fa-search"></i></button>
					                                 </div>
					                              </div>
					                           </div>
                                                <!-- <a class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>
                                                        <a href="javascript:;">
                                                            <i class="fa fa-print"></i> Print </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                    </li>
                                                </ul> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-scrollable" id="tag_container">
                                    <table class="table table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                            <tr>
                                            	<th class="center">Id</th>
                                                 <th class="center"> Order Id </th>
                                                <th class="center">Reference id</th>
                                                <th class="center">Amount</th>
                                                <th class="center">Booking Status </th>
                                                <th class="center">Booking Error </th>
                                               
                                                <th class="center"> Payment Mode </th>
                                                <th class="center"> Payment Date </th>
                                                <th class="center"> Payment Time </th>
                                                <th class="center"> Code </th>
                                               
                                                
                                               
                                                <!-- <th class="center"> Action </th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $chk=1;
                                            ?>
										    @foreach($paymentlist as $payment)
                                           
										    <tr class="odd gradeX">
												<td class="center">{{$chk++}}</td>
                                                <td class="center ">{{$payment->orderid}}</td>
                                               
                                                <td class="center">{{$payment->referenceid}}</td>
                                                <td class="center">{{$payment->amount}}</td>
                                                
                                                <td class="center">{{$payment->tx_status}}</td>
                                                <td class="center">{{$payment->response_msg}}</td>
                                                <td class="center">{{$payment->payment_mode}}</td>
                                                <td class="center">{{$payment->orderdate}}</td>
												
												
												<td class="center">{{$payment->ordertime}}</td>
												<td class="center">{{$payment->payment_code}}</td>
                                                
                                                
												
												
												<!-- <td class="center">
													<a href="edit_booking.html" class="btn btn-tbl-edit btn-xs">
														<i class="fa fa-pencil"></i>
													</a>
													<button class="btn btn-tbl-delete btn-xs">
														<i class="fa fa-trash-o "></i>
													</button>
												</td> -->
											</tr>
		                                    
		                                    @endforeach
											
											
										</tbody>
                                    </table>
                                  {!! $paymentlist->render() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page content -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
         @include ('admin.include.footer')
        <!-- end footer -->
    </div>
    @include ('admin.include.downjs')
  <script>
  	 $(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            }else{
                getData(page);
            }
        }
    });
    
    $(document).ready(function()
    {
        $(document).on('click', '.pagination a',function(event)
        {
            event.preventDefault();
  
            $('li').removeClass('active');
            $(this).parent('li').addClass('active');
  
            var myurl = $(this).attr('href');
            var page=$(this).attr('href').split('page=')[1];
  
            getData(page);
        });
  
    });
  
    function getData(page){
        $.ajax(
        {
            url: '?page=' + page,
            type: "get",
            datatype: "html"
        }).done(function(data){
        	var data1=jQuery(data).find('#tag_container').html();
            $("#tag_container").empty().html(data1);
            location.hash = page;
        }).fail(function(jqXHR, ajaxOptions, thrownError){
              alert('No response from server');
        });
    }
     $(document).on('keypress','#search',function(e)
    {
        if(e.which==13)
        {
            var search=$('#search').val();
            
            $.ajax(
            {
                url: '{{route("paymentdetaillist")}}',
                data: {
                    'search':search,
                    '_token':'{{csrf_token()}}'
                },
                type: 'POST',
                success: function (data) 
                {
                	
                    var data1=jQuery(data).find('#tag_container').html();
                    $("#tag_container").empty().html(data1);
                }
            });
            return false;
        }
    });
    $(document).on('click','#search-btn',function(e)
    {
        var search=$('#search').val();
        alert(search);
        $.ajax(
        {
            url: '{{route("paymentdetaillist")}}',
            data: {
                'search':search,
                '_token':'{{csrf_token()}}'
            },
            type: 'POST',
            success: function (data) 
            {
                var data1=jQuery(data).find('#tag_container').html();
                $("#tag_container").empty().html(data1);
            }
        });
    });
  </script>
</body>
</html>