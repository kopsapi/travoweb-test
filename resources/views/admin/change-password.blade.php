@include ('admin.include.topcss')
<!-- Loader -->
<link href="{{ asset('assets/admin/assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/admin/assets/plugins/iconic/css/material-design-iconic-font.min.css') }}" rel="stylesheet">
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>

<style>

    .section-start {
        padding: 30px 0;
    }

    .form-group.f1 label {
        color: #f79c23;
    }

    .c-items {
        border: 1px solid #c9c9c9;
        border-radius: 5px;
        background: white;
    }

    .onword-flight {

        text-align: center;
        color: white;
        padding: 10px;
        background-color: #3a3f51;

    }

    .Commission tr td {
        text-align: center;
    }

    .modal-footer.f-bg {
        background: #e2e3ea;
    }


</style>
<style>
    button.fh-save {
        border: none;
              background: #FF9800;
       /* background: linear-gradient(to right, #fa9e1b, #8d4fff);*/
        padding: 8px 44px;
        border: 1px solid #FF9800;
        color: white;
        border-radius: 5px;
        font-size: 18px;
        display: flex;
        font-weight: 500;
        margin-left: auto;
    }
    button.fh-save:hover {
        border: 1px solid #8d4fff;
        background: transparent;
        color: #8d4fff;

    }


    /* Hide the browser's default checkbox */
    .con input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }


    .nav-tabs.fh-switch>li a{
        color: #333;
    }

    .nav-tabs.fh-switch>li a:hover{
        color: #e26d78 !important;
    }

    .form-group.f1 {
        position: relative;
        margin-bottom: 25px;
        overflow: hidden;
    }

    label.e-lable1 {
        position: absolute;
        left: 5px;
        top: 69%;
        font-size: 20px;
        transform: translateY(-50%) !important;
        color: #21b1e7;
    }
    input.e-input {
        width: 100%;
        display: block;
        border: none;
        border-bottom: 1px solid #999;
        padding: 6px 40px;
        box-sizing: border-box;
    }
    input.e-input:focus {
        outline: none;
        box-shadow: none;
    }
    @media screen and (max-width:400px){
        .container.pass-res{
            padding: 28px 20px !important;
        }
    }
</style>
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
<div class="page-wrapper">
    <!-- start header -->
@include ('admin.include.header')
    <!-- end header -->

    <!-- start page container -->

    <div class="page-container">

        <!-- start sidebar menu -->

    @include ('admin.include.navbar')

    <!-- end sidebar menu -->
        <!-- start page content -->

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar"></div>

                <!-- start widget -->

                <div class="state-overview">
                    <div class="container">
                        <div class="chng-pass-div">
                            <section class="section-start">

                                <div class="c-items">

                                    <h3 class="onword-flight">Change Password</h3>

                                    <div class="container pass-res" style="padding: 30px 60px;">
                                        <span style="color:green;display: none" id="flightsuccess"></span>
                                        <span style="color:red;display: none" id="flighterror"></span>

                                        <div class="form-group f1">
                                            <label style="margin-left: 5px;">Old Password</label>
                                            <label for="name" class="e-lable1"><i class="fas fa-user-clock"></i></label>
                                            <input type="text"  placeholder="Enter Your Old Password" class="e-input">
                                            <span style="color:red;visibility: hidden" ></span>

                                        </div>
                                        <div class="form-group f1">
                                            <label style="margin-left: 5px;">New Password</label>
                                            <label for="name" class="e-lable1"><i class="fa fa-lock"></i></label>
                                            <input type="text"  placeholder="Enter Your New Password" class="e-input" >
                                            <span style="color:red;visibility: hidden" id="bagmargin_err"></span>
                                        </div>

                                        <div class="form-group f1">
                                            <label style="margin-left: 5px;">Confirm Password</label>
                                            <label for="name" class="e-lable1"><i class="fa fa-lock"></i></label>
                                            <input type="text" placeholder="Confirm Password" class="e-input">
                                            <span style="color:red;visibility: hidden"></span>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-6"></div>
                                            <div class="col-sm-6">
                                                <button class="fh-save" >Done</button>
                                            </div>
                                        </div>




                                    </div>


                                </div>



                            </section>
                        </div>
                    </div>
                    <div class="modal fade text-center py-5" style="top:30px;background-color: rgba(0,0,0,0); "
                         id="myModal_chargesfor">
                        <div class="modal-dialog modal-md" role="document">
                            <div class="modal-content modal-bg" style="margin-top: 12%; width: 350px">
                                <img src="{{asset('assets/images/green-tic-icon.png')}}" class="rp-done">
                                <div class="over-modal">


                                    <div class="modal-body">

                                        <span class="close close-btn1" data-dismiss="modal">&times;</span>


                                        <img src="{{asset('assets/images/icon-recover-password-512.png')}}"
                                             class="recover-pass-img">

                                        <h3 class="pt-3 mb-0 h-cancel success_modal alert alert-success"
                                            style="font-size: 16px;    margin: 15px 0 20px !important;">Reset Password
                                            Sent to this email</h3>
                                        <!-- <p class="t-id-info">Your Change Request Id:1234</p> -->
                                        <button class="btn2 text-white mb-5 success_ok"
                                                style="margin-top:45px; margin-bottom: 0px !important"
                                                data-dismiss="modal">OK
                                        </button>
                                        <!--   <a role="button" class="btn1 text-white mb-5 success_ok" href="https://www.sunlimetech.com" target="_blank">Submit</a> -->
                                    </div>
                                    <div class="modal-footer f-bg mt-3" style="background: rgba(49, 18, 75, 0.8);"
                                    >
                                        <span class="d-block mr-auto f-help " style="color: white;"><i
                                                    class="fa fa-phone"></i> Helpline:<a href="#" style="color:#FF9800"> 34356894454</a></span>
                                        <span class="f-help"><img src="{{asset('assets/images/logo.png')}}"
                                                                  class="f-logo"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <style>
                            .h-form {
                                background: url("{{asset('assets/images/modal-img.jpg')}}");
                                background-repeat: no-repeat;
                                background-size: cover;
                                width: 70%;
                                margin: 50px auto;
                                position: relative;

                            }

                            .over {
                                background: rgba(0, 0, 0, 0.5);
                            }

                            .btn-chn {
                                background: #fa9e1b;
                                display: block;
                                width: 100%;
                                border: none;
                                text-align: center;
                                padding: 7px 18px;
                                border-radius: 5px;
                                color: white;
                                font-size: 16px;
                                font-weight: 500;
                            }

                            .t-mail:-webkit-autofill {
                                background-color: transparent !important;
                            }

                            .t-mail {
                                background-color: transparent !important;
                                color: white !important;
                            }

                            label {
                                color: white;
                            }

                            .t-mail {
                                border: none;
                                border-bottom: 2px solid white;
                                color: white !important;
                                background-color: transparent;
                                border-radius: 0;
                            }

                            .t-mail::placeholder {

                                color: white !important;
                            }

                            h3.c-head {
                                text-align: center;
                                color: white;
                                text-transform: uppercase;
                                font-weight: bold;
                                padding: 30px 30px 0 30px;
                            }

                        </style>

                    </div>
                </div>
            </div>
        </div>
    </div>
@include ('admin.include.footer')

<!-- end footer -->

</div>

@include ('admin.include.downjs')

</body>







