
     <script src="{{ asset('assets/admin/assets/plugins/jquery/jquery.min.js') }}"></script>
     <script src="{{ asset('assets/admin/assets/plugins/popper/popper.min.js') }}"></script>
     <script src="{{ asset('assets/admin/assets/plugins/jquery-blockui/jquery.blockui.min.js') }}"></script>
     <script src="{{ asset('assets/admin/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
     <!-- bootstrap -->
     <script src="{{ asset('assets/admin/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
     <script src="{{ asset('assets/admin/assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
     <script src="{{ asset('assets/admin/assets/js/app.js') }}"></script>
      <!-- Common js-->
     <script src="{{ asset('assets/admin/assets/js/pages/sparkline/sparkline-data.js') }}"></script>
     <script src="{{ asset('assets/admin/assets/js/layout.js') }}"></script>
     <script src="{{ asset('assets/admin/assets/js/theme-color.js') }}"></script>
     <!-- material -->
     <script src="{{ asset('assets/admin/assets/plugins/material/material.min.js') }}"></script>
     <!-- animation -->
     <script src="{{ asset('assets/admin/assets/js/pages/ui/animations.js') }}"></script>
     <!-- morris chart -->
     <script src="{{ asset('assets/admin/assets/plugins/morris/raphael-min.js') }}"></script>
     <!-- end js include path -->
     <script src="{{ asset('assets/admin/assets/plugins/counterup/jquery.waypoints.min.js') }}"></script>
     <script src="{{ asset('assets/admin/assets/plugins/counterup/jquery.counterup.min.js') }}"></script>
      <script src="{{ asset('assets/admin/assets/js/editor.js') }}"></script>
      <script src="{{ asset('assets/sweetalert/sweetalert.min.js') }}"></script>
      <script src="{{ asset('assets/sweetalert/jquery.sweet-alert.custom.js') }}"></script>

      <script src="{{ asset('assets/ckedit/ckeditor.js') }}"></script>

