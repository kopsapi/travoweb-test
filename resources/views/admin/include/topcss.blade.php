<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Responsive Admin Template" />
    <meta name="author" content="SmartUniversity" />
    <title>Travoweb | Admin</title>
    <!-- google font -->
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="{{ asset('assets/admin/assets/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    
	<!--bootstrap -->
    <link href="{{ asset('assets/admin/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/assets/plugins/summernote/summernote.css') }}" rel="stylesheet">

	<!-- morris chart -->
    <link href="{{ asset('assets/admin/assets/plugins/morris/morris.css') }}" rel="stylesheet">
   
    <!-- Material Design Lite CSS -->
    <link href="{{ asset('assets/admin/assets/plugins/material/material.min.CSS') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/assets/css/material_style.css') }}" rel="stylesheet">
	
	<!-- animation -->
    <link href="{{ asset('assets/admin/assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="assets/css/pages/animate_page.css" rel="stylesheet">
	<!-- Template Styles -->
    <link href="{{ asset('assets/admin/assets/css/plugins.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/assets/css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/assets/css/theme-color.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/assets/css/editor.css') }}" rel="stylesheet">
	<!-- favicon -->
    <link rel="shortcut icon" href=" {{ asset('assets/admin/assets/img/favicon.ico') }}" /> 
    <link href="{{ asset('assets/sweetalert/sweetalert.css') }}" rel="stylesheet">
 </head>
 <style>
     ul.sidemenu.page-header-fixed.sidebar-hover.sidemenu-hover-submenu.p-t-20::-webkit-scrollbar {
    width: 5px;
}
ul.sidemenu.page-header-fixed.sidebar-hover.sidemenu-hover-submenu.p-t-20::-webkit-scrollbar-thumb {
    background: #28a745;
}

ul.sidemenu.page-header-fixed.sidebar-hover.sidemenu-hover-submenu.p-t-20::-webkit-scrollbar-track {
    background: #dcffe4;
}
 </style>