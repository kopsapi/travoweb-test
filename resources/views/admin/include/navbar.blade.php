<div class="sidebar-container">

	<div class="sidemenu-container navbar-collapse collapse fixed-menu">

		<div id="remove-scroll">

			<ul class="sidemenu page-header-fixed sidebar-hover sidemenu-hover-submenu p-t-20" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

				<li class="sidebar-toggler-wrapper hide">

					<div class="sidebar-toggler">

						<span></span>

					</div>

				</li>

				<li class="sidebar-user-panel">

					<div class="user-panel">

						<div class="row">

							<div class="sidebar-userpic">

								<img src="assets/img/dp.png" class="img-responsive" alt="">

							</div>

						</div>

						<div class="profile-usertitle">

							<div class="sidebar-userpic-name"> Admin </div>

							<div class="profile-usertitle-job"> CEO </div>

						</div>

						<div class="sidebar-userpic-btn">

							<a class="tooltips" href="#" data-placement="top" data-original-title="Profile">

								<i class="material-icons">person_outline</i>

							</a>

							<a class="tooltips" href="#" data-placement="top" data-original-title="Mail">

								<i class="material-icons">mail_outline</i>

							</a>

							<a class="tooltips" href="#" data-placement="top" data-original-title="Chat">

								<i class="material-icons">chat</i>

							</a>

							<a class="tooltips" href="{{route('adminlogout')}}" data-placement="top" data-original-title="Logout">

								<i class="material-icons">input</i>

							</a>

						</div>

					</div>

				</li>



				<li class="nav-item start active">

					<a href="{{url('admin/dashboard')}}" class="nav-link">

						<i class="material-icons">dashboard</i>

						<span class="title">Dashboard</span>

					</a>

				</li>



				<li class="nav-item">

					<a href="{{url('admin/user')}}" class="nav-link">

						<i class="material-icons">person_pin</i>

						<span class="title">Customer List</span>

					</a>

				</li>
				<li class="nav-item start active">
					<a href="#" class="nav-link nav-toggle">
						<i class="material-icons">flight_takeoff</i>
						<span class="title"> Flight Management <i class="fa fa-angle-down" style="float: right"></i> </span>
						<span class="selected"></span>

					</a>
					<ul class="sub-menu" style="">
						<li class="nav-item active n-link">
							<a href="{{url('admin/flightbooking')}}" class="nav-link ">
								<span class="title"><i class="fa fa-caret-right"></i> Flight Booking</span>
								<span class="selected"></span>
							</a>
						</li>
						<li class="nav-item ">
							<a href="{{url('admin/canceldetail')}}" class="nav-link ">
								<span class="title"><i class="fa fa-caret-right"></i> Cancelled Booking</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="nav-item start active">
					<a href="#" class="nav-link nav-toggle">
						<i class="material-icons">hotel</i>
						<span class="title">Hotel Management <i class="fa fa-angle-down" style="float: right"></i> </span>
						<span class="selected"></span>

					</a>
					<ul class="sub-menu">
						<li class="nav-item active">
							<a href="{{url('admin/hotelbooking')}}" class="nav-link ">
								<span class="title"><i class="fa fa-caret-right"></i> Hotel Booking</span>
								<span class="selected"></span>
							</a>
						</li>
						<li class="nav-item ">
							<a href="{{url('admin/adminhotelcancel')}}" class="nav-link ">
								<span class="title"><i class="fa fa-caret-right"></i> Cancelled Booking</span>
							</a>
						</li>

					</ul>
				</li>


				<li class="nav-item">

					<a href="{{url('admin/paymentdetail')}}" class="nav-link">

						<i class="material-icons">business_center</i>

						<span class="title">Payment Details</span>

					</a>

				</li>
				<li class="nav-item">

					<a href="{{url('admin/general-setting')}}" class="nav-link">

						<i class="material-icons">person_pin</i>

						<span class="title">Setting</span>

					</a>

				</li>
				
				<li class="nav-item start active">
					<a href="#" class="nav-link nav-toggle">
						<i class="material-icons">flight_takeoff</i>
						<span class="title"> Website <i class="fa fa-angle-down" style="float: right"></i> </span>
						<span class="selected"></span>

					</a>
					<ul class="sub-menu" style="">
						<li class="nav-item active n-link">
							<a href="{{url('admin/query-list')}}" class="nav-link ">
								<span class="title"><i class="fa fa-caret-right"></i> Website Enquiry</span>
								<span class="selected"></span>
							</a>
						</li>
						<li class="nav-item ">
							<a href="{{url('admin/add-testimonial')}}" class="nav-link ">
								<span class="title"><i class="fa fa-caret-right"></i> Add Testimonial</span>
							</a>
						</li>
						<li class="nav-item ">
							<a href="{{url('admin/list-testimonial')}}" class="nav-link ">
								<span class="title"><i class="fa fa-caret-right"></i> List Testimonial</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="nav-item">

					<a href="{{url('admin/package_enquiry_list')}}" class="nav-link">

						<i class="material-icons">person_pin</i>

						<span class="title">Package Enquiry</span>

					</a>

				</li>
				<li class="nav-item start active">
					<a href="#" class="nav-link nav-toggle">
						<i class="material-icons">flight_takeoff</i>
						<span class="title">Packages <i class="fa fa-angle-down" style="float: right"></i> </span>
						<span class="selected"></span>

					</a>
					<ul class="sub-menu" style="">
						<li class="nav-item active n-link">
							<a href="{{url('admin/package-country')}}" class="nav-link ">
								<span class="title"><i class="fa fa-caret-right"></i> Add Country</span>
								<span class="selected"></span>
							</a>
						</li>
						<li class="nav-item ">
							<a href="{{url('admin/package-country-list')}}" class="nav-link ">
								<span class="title"><i class="fa fa-caret-right"></i> Country List</span>
							</a>
						</li>
						<li class="nav-item ">
							<a href="{{url('admin/package-add')}}" class="nav-link ">
								<span class="title"><i class="fa fa-caret-right"></i> Add Packages</span>
							</a>
						</li>
						<li class="nav-item ">
							<a href="{{url('admin/package_list')}}" class="nav-link ">
								<span class="title"><i class="fa fa-caret-right"></i> Packages List</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="nav-item start active">
					<a href="#" class="nav-link nav-toggle">
						<i class="material-icons">flight_takeoff</i>
						<span class="title">Advertisement <i class="fa fa-angle-down" style="float: right"></i> </span>
						<span class="selected"></span>

					</a>
					<ul class="sub-menu" style="">
						<li class="nav-item active n-link">
							<a href="{{url('admin/advertisement-banner')}}" class="nav-link ">
								<span class="title"><i class="fa fa-caret-right"></i> Add Advertisement</span>
								<span class="selected"></span>
							</a>
						</li>
						<li class="nav-item ">
							<a href="{{url('admin/advertisement-banner-list')}}" class="nav-link ">
								<span class="title"><i class="fa fa-caret-right"></i> Advertisement List</span>
							</a>
						</li>
					</ul>
				</li>
				

			<!-- 	<li class="nav-item">

					<a href="#" class="nav-link nav-toggle">

						<i class="material-icons">email</i>

						<span class="title">Email</span>

						<span class="arrow"></span>

						<span class="label label-rouded label-menu label-danger">new</span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="email_inbox.html" class="nav-link ">

								<span class="title">Inbox</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="email_view.html" class="nav-link ">

								<span class="title">View Mail</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="email_compose.html" class="nav-link ">

								<span class="title">Compose Mail</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="nav-item">

					<a href="#" class="nav-link nav-toggle">

						<i class="material-icons">business_center</i>

						<span class="title">Booking</span>

						<span class="arrow"></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="new_booking.html" class="nav-link ">

								<span class="title">New Booking</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="view_booking.html" class="nav-link ">

								<span class="title">View Booking</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="edit_booking.html" class="nav-link ">

								<span class="title">Edit Booking</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="nav-item">

					<a href="#" class="nav-link nav-toggle">

						<i class="material-icons">vpn_key</i>

						<span class="title">Rooms</span>

						<span class="arrow"></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="add_room.html" class="nav-link ">

								<span class="title">Add Room Details</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="all_rooms.html" class="nav-link ">

								<span class="title">View All Rooms</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="edit_room.html" class="nav-link ">

								<span class="title">Edit Room Details</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="nav-item">

					<a href="#" class="nav-link nav-toggle">

						<i class="material-icons">group</i>

						<span class="title">Staff</span>

						<span class="arrow"></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="add_staff.html" class="nav-link ">

								<span class="title">Add Staff Details</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="all_staffs.html" class="nav-link ">

								<span class="title">View All Staffs</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="edit_staff.html" class="nav-link ">

								<span class="title">Edit Staff Details</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="nav-item">

					<a href="#" class="nav-link nav-toggle">

						<i class="material-icons">local_taxi</i>

						<span class="title">Transportation</span>

						<span class="arrow"></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="add_vehicle.html" class="nav-link ">

								<span class="title">Add Vehicle Details</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="all_vehicles.html" class="nav-link ">

								<span class="title">View All Vehicle</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="edit_vehicle.html" class="nav-link ">

								<span class="title">Edit Vehicle Details</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="menu-heading m-t-20">

					<span>-- Layout, Apps &amp; Widget</span>

				</li>

				<li class="nav-item">

					<a href="#" class="nav-link nav-toggle"> <i class="material-icons">desktop_mac</i>

						<span class="title">Layout</span> <span class="arrow"></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="layout_boxed.html" class="nav-link "> <span class="title">Boxed</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="layout_full_width.html" class="nav-link "> <span class="title">Full Width</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="layout_collapse.html" class="nav-link "> <span class="title">Collapse Menu</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="layout_right_sidebar.html" class="nav-link "> <span class="title">Right Sidebar</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="layout_sidebar_hover_menu.html" class="nav-link "> <span class="title">Hover Sidebar Menu</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="layout_mega_menu.html" class="nav-link "> <span class="title">Mega Menu</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="nav-item">

					<a href="#" class="nav-link nav-toggle"> <i class="material-icons">grain</i>

						<span class="title">Apps</span> <span class="arrow"></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="chat.html" class="nav-link "> <span class="title">Chat</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="contact_list.html" class="nav-link "> <span class="title">Contact List</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="contact_grid.html" class="nav-link "> <span class="title">Contact Grid</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="gallery.html" class="nav-link "> <span class="title">Gallery</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="support.html" class="nav-link "> <span class="title">Support</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="calendar.html" class="nav-link "> <span class="title">Calendar</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="nav-item">

					<a href="widget.html" class="nav-link nav-toggle"> <i class="material-icons">widgets</i>

						<span class="title">Widget</span> 

					</a>

				</li>

				<li class="menu-heading m-t-20">

					<span>-- Elements, Forms &amp; Tables</span>

				</li>

				<li class="nav-item">

					<a href="#" class="nav-link nav-toggle">

						<i class="material-icons">dvr</i>

						<span class="title">UI Elements</span> 

						<span class="label label-rouded label-menu label-warning">new</span>

						<span class="arrow"></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="buttons.html" class="nav-link ">

								<span class="title">Buttons</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="alert.html" class="nav-link ">

								<span class="title">Alert</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="tabs_accordions_navs.html" class="nav-link ">

								<span class="title">Tabs &amp; Accordions</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="typography.html" class="nav-link ">

								<span class="title">Typography</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="notification.html" class="nav-link ">

								<span class="title">Notifications</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="sweet_alert.html" class="nav-link ">

								<span class="title">Sweet Alert</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="panels.html" class="nav-link ">

								<span class="title">Panels</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="grid.html" class="nav-link ">

								<span class="title">Grids</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="tree.html" class="nav-link ">

								<span class="title">Tree View</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="carousel.html" class="nav-link ">

								<span class="title">Carousel</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="animation.html" class="nav-link ">

								<span class="title">Animations</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="nav-item">

					<a href="#" class="nav-link nav-toggle">

						<i class="material-icons">store</i>

						<span class="title">Material Elements</span> 

						<span class="arrow"></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="material_button.html" class="nav-link ">

								<span class="title">Buttons</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="material_tab.html" class="nav-link ">

								<span class="title">Tabs</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="material_chips.html" class="nav-link ">

								<span class="title">Chips</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="material_grid.html" class="nav-link ">

								<span class="title">Grid</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="material_form.html" class="nav-link ">

								<span class="title">Form</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="material_datepicker.html" class="nav-link ">

								<span class="title">DatePicker</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="material_select.html" class="nav-link ">

								<span class="title">Select Item</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="material_loading.html" class="nav-link ">

								<span class="title">Loading</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="material_menu.html" class="nav-link ">

								<span class="title">Menu</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="material_slider.html" class="nav-link ">

								<span class="title">Slider</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="material_tables.html" class="nav-link ">

								<span class="title">Tables</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="material_toggle.html" class="nav-link ">

								<span class="title">Toggle</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="material_badges.html" class="nav-link ">

								<span class="title">Badges</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="nav-item">

					<a href="javascript:;" class="nav-link nav-toggle">

						<i class="material-icons">subtitles</i>

						<span class="title">Forms </span>

						<span class="arrow"></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="layouts_form.html" class="nav-link ">

								<span class="title">Form Layout</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="advance_form.html" class="nav-link ">

								<span class="title">Advance Component</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="validation_form.html" class="nav-link ">

								<span class="title">Form Validation</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="steps.html" class="nav-link ">

								<span class="title">Wizard</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="file_upload.html" class="nav-link ">

								<span class="title">File Upload</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="editable_form.html" class="nav-link ">

								<span class="title">Editor</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="nav-item">

					<a href="javascript:;" class="nav-link nav-toggle">

						<i class="material-icons">list</i>

						<span class="title">Data Tables</span>

						<span class="arrow"></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="basic_table.html" class="nav-link ">

								<span class="title">Basic Tables</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="advanced_table.html" class="nav-link ">

								<span class="title">Advance Tables</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="editable_table.html" class="nav-link ">

								<span class="title">Editable Tables</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="group_table.html" class="nav-link ">

								<span class="title">Grouping</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="table_data.html" class="nav-link ">

								<span class="title">Tables With Sourced Data</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="menu-heading m-t-20">

					<span>-- Extra Components</span>

				</li>

				<li class="nav-item">

					<a href="javascript:;" class="nav-link nav-toggle">

						<i class="material-icons">timeline</i>

						<span class="title">Charts</span>

						<span class="arrow"></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="charts_echarts.html" class="nav-link ">

								<span class="title">eCharts</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="charts_morris.html" class="nav-link ">

								<span class="title">Morris Charts</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="charts_chartjs.html" class="nav-link ">

								<span class="title">Chartjs</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="nav-item">

					<a href="javascript:;" class="nav-link nav-toggle">

						<i class="material-icons">map</i>

						<span class="title">Maps</span>

						<span class="arrow"></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="google_maps.html" class="nav-link ">

								<span class="title">Google Maps</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="vector_maps.html" class="nav-link ">

								<span class="title">Vector Maps</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="nav-item">

					<a href="#" class="nav-link nav-toggle"> <i class="material-icons">album</i>

						<span class="title">Icons</span> <span class="arrow"></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="fontawesome_icons.html" class="nav-link "> <span class="title">Font Awesome</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="simpleline_icons.html" class="nav-link "> <span class="title">Simple Line Icon</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="material_icons.html" class="nav-link "> <span class="title">Material Icon</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="nav-item">

					<a href="javascript:;" class="nav-link nav-toggle"> <i class="material-icons">description</i>

					<span class="title">Extra pages</span> 

					<span class="arrow"></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="timeline.html" class="nav-link "> <span class="title">Timeline</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="login.html" class="nav-link "> <span class="title">Login</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="sign_up.html" class="nav-link "> <span class="title">Registration</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="user_profile.html" class="nav-link "><span class="title">Profile</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="pricing.html" class="nav-link "><span class="title">Pricing</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="contactus.html" class="nav-link "> <span class="title">Contact Us</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="invoice.html" class="nav-link "><span class="title">Invoice</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="faq.html" class="nav-link "><span class="title">Faq</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="lock_screen.html" class="nav-link "> <span class="title">Lock Screen</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="page-404.html" class="nav-link "> <span class="title">404 Page</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="page-500.html" class="nav-link "> <span class="title">500 Page</span>

							</a>

						</li>

						<li class="nav-item">

							<a href="blank_page.html" class="nav-link "> <span class="title">Blank Page</span>

							</a>

						</li>

					</ul>

				</li>

				<li class="nav-item">

					<a href="javascript:;" class="nav-link nav-toggle">

						<i class="material-icons">slideshow</i>

						<span class="title">Multi Level Menu</span>

						<span class="arrow "></span>

					</a>

					<ul class="sub-menu">

						<li class="nav-item">

							<a href="javascript:;" class="nav-link nav-toggle">

								<i class="fa fa-university"></i> Item 1

								<span class="arrow"></span>

							</a>

							<ul class="sub-menu">

								<li class="nav-item">

									<a href="javascript:;" class="nav-link nav-toggle">

										<i class="fa fa-bell-o"></i> Arrow Toggle

										<span class="arrow "></span>

									</a>

									<ul class="sub-menu">

										<li class="nav-item">

											<a href="javascript:;" class="nav-link">

												<i class="fa fa-calculator"></i> Sample Link 1</a>

										</li>

										<li class="nav-item">

											<a href="#" class="nav-link">

												<i class="fa fa-clone"></i> Sample Link 2</a>

										</li>

										<li class="nav-item">

											<a href="#" class="nav-link">

												<i class="fa fa-cogs"></i> Sample Link 3</a>

										</li>

									</ul>

								</li>

								<li class="nav-item">

									<a href="#" class="nav-link">

										<i class="fa fa-file-pdf-o"></i> Sample Link 1</a>

								</li>

								<li class="nav-item">

									<a href="#" class="nav-link">

										<i class="fa fa-rss"></i> Sample Link 2</a>

								</li>

								<li class="nav-item">

									<a href="#" class="nav-link">

										<i class="fa fa-hdd-o"></i> Sample Link 3</a>

								</li>

							</ul>

						</li>

						<li class="nav-item">

							<a href="javascript:;" class="nav-link nav-toggle">

								<i class="fa fa-gavel"></i> Arrow Toggle

								<span class="arrow"></span>

							</a>

							<ul class="sub-menu">

								<li class="nav-item">

									<a href="#" class="nav-link">

										<i class="fa fa-paper-plane"></i> Sample Link 1</a>

								</li>

								<li class="nav-item">

									<a href="#" class="nav-link">

										<i class="fa fa-power-off"></i> Sample Link 1</a>

								</li>

								<li class="nav-item">

									<a href="#" class="nav-link">

										<i class="fa fa-recycle"></i> Sample Link 1

									 </a>

								</li>

							</ul>

						</li>

						<li class="nav-item">

							<a href="#" class="nav-link">

								<i class="fa fa-volume-up"></i> Item 3 </a>

						</li>

					</ul>

				</li> -->

			</ul>

		</div>

	</div>

</div>
<style>


	.sidemenu.sidemenu-hover-submenu li.active .sub-menu, .sidemenu.sidemenu-hover-submenu li.open .sub-menu {
		display: none !important;
		position: relative !important;
		padding-top: 5px;
		padding-bottom: 5px;
		border-radius: 0;
	}
	.sidemenu.sidemenu-hover-submenu li a>.arrow {
		display: block !important;
	}
	.sidemenu.sidemenu-hover-submenu>li:hover>.sub-menu {
		display: block !important;
		margin-left: 0px !important;
		margin-top: 0px !important;
	}

	.sidemenu-container .sidemenu .sub-menu, .sidemenu-closed.sidemenu-container-fixed .sidemenu-container:hover .sidemenu .sub-menu {
		list-style: none;
		display: none;
		padding: 0 0 15px 0;
		margin: -2px 0px 0px 0px;
		width: 100% !important;
		background-color: #1e293c;
	}
	.dark-sidebar-color .sidemenu-container .sidemenu .sub-menu>li.active>a, .dark-sidebar-color .sidemenu-container .sidemenu .sub-menu>li.open>a, .dark-sidebar-color .sidemenu-container .sidemenu .sub-menu>li:hover>a, .dark-sidebar-color .sidemenu-closed.sidemenu-container-fixed .sidemenu-container:hover .sidemenu .sub-menu>li.active>a, .dark-sidebar-color .sidemenu-closed.sidemenu-container-fixed .sidemenu-container:hover .sidemenu .sub-menu>li.open>a, .dark-sidebar-color .sidemenu-closed.sidemenu-container-fixed .sidemenu-container:hover .sidemenu .sub-menu>li:hover>a {
		color: #fff;
		font-weight: bold;
	}

	.sidemenu-container:hover .sidemenu .sub-menu>li>a {
		color: #fff;
		font-weight: 500 !important;
	}
	 ul.sub-menu li.nav-item:hover, ul.sub-menu li.nav-item:hover a{
		background: #1e293c !important;
		color: white !important;
	}

	@media screen and (min-width: 992px) {

		.sidemenu-closed .sidemenu-container .sidemenu.sidemenu-closed>li:active>a.nav-link.nav-toggle:hover>.sub-menu {
			display: block !important;
		}
		.sidemenu.sidemenu-closed>li:hover
		{
			    background-color: #2e3344 !important;
		}
	}

</style>