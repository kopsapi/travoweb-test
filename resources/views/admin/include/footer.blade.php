<div class="page-footer">
	<div class="page-footer-inner"> &copy; 2019 TravoWeb. All Rights Reserved.</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>