@include ('admin.include.topcss')
<style type="text/css">

    .pagination>li>a, .pagination>li>span
    {
        position: relative;
        float: left;
        padding: 6px 12px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }
    .pagination>.active>span
             {
        font-weight:bold;
        color:#fff;
        background-color: #337ab7;
    }
    ul.pagination
    {
        float: right;
        margin-right: 20px;
    }
</style>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
<div class="page-wrapper">
    <!-- start header -->
@include ('admin.include.header')
<!-- end header -->
    <!-- start page container -->
    <div class="page-container">
        <!-- start sidebar menu -->
    @include ('admin.include.navbar')
    <!-- end sidebar menu -->
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">All Flight Booking</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li><a class="parent-item" href="">Flight Booking</a>&nbsp;<i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">All Flight Booking</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-box">
                            <div class="card-head">
                                <header>All Flight Booking </header>
                                <div class="tools">
                                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                </div>
                            </div>
                            <div class="card-body ">
                                <div class="p-b-20">
                                    <!-- <div class="btn-group">
                               <a href="new_booking.html" id="addRow" class="btn btn-info">
                            Add New <i class="fa fa-plus"></i>
                                  </a>
                                 </div> -->
                                    <!-- <a class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">
                                    Tools

                                   <i class="fa fa-angle-down"></i>
                                        </a>
                                       <ul class="dropdown-menu pull-right">
                                     <li>
                                   <a href="javascript:;">
                                 <i class="fa fa-print"></i> Print </a>
                                   </li>
                                                    <li>
                                       <a href="javascript:;">
                                     <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                         </li>  <li>   <a href="javascript:;">
                                          <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                    </li>
                                     </ul> -->
                                    <section class="section-start">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <h3 class="t-book-detail">Booking Details</h3>
                                                    <div class="c-items1" >

                                                        <table class="table table-bordered">
                                                            <tbody>
                                                            <tr>
                                                                <th>Reference Id:</th>
                                                                <td>SCTF- 41788</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Booking Id:</th>
                                                                <td>30081814</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Hotel Name:</th>
                                                                <td>Hotel Ritz Plaza</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Confirmation No:</th>
                                                                <td>TOB0005541413</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Total Charge (Pay by U ser):</th>
                                                                <td><i class="fa fa-rupee"></i> 3035</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Country:</th>

                                                                <td>IN</td>
                                                            </tr>
                                                            <tr>
                                                                <th>City:</th>
                                                                <td>Amritsar</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Type:</th>
                                                                <td>domestic</td>
                                                            </tr>
                                                            <tr>
                                                                <th>CheckIn:</th>
                                                                <td>15-08-2018</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Check Out:</th>
                                                                <td>16-08-2018</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Room:</th>
                                                                <td>1</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Booking Date:</th>
                                                                <td>August 14, 2018, 04:39 PM</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="c-items">
                                                        <h3 class="pass-detail">Guest Detail Details</h3>
                                                        <div class="container">
                                                            <table class="table table-striped">
                                                                <thead>
                                                                <tr>
                                                                    <th>Title</th>
                                                                    <th>Name</th>
                                                                    <th>Type</th>
                                                                    <th>Age</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td>Mr</td>
                                                                    <td>Danish</td>
                                                                    <td>Adult</td>
                                                                    <td>25</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Mr</td>
                                                                    <td>Danish</td>
                                                                    <td>Adult</td>
                                                                    <td>25</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="mt-5 c-items">
                                                        <h3 class="pass-detail">Contact Details</h3>
                                                        <div class="container">
                                                            <table class="table table-striped">
                                                                <thead>
                                                                <tr>
                                                                    <th>Email</th>
                                                                    <th>Mobile</th>

                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td>dss.sydney@yahoo.com</td>
                                                                    <td>6239775233</td>

                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="mt-5 c-items">
                                                        <h3 class="pass-detail">Status</h3>
                                                        <div class="container">
                                                            <table class="table table-striped">



                                                                <tbody>
                                                                <tr>
                                                                    <th>Current Status</th>
                                                                    <td>Confirmed</td>


                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="mt-5 c-items">
                                                        <h3 class="pass-detail">Commission & Profit</h3>
                                                        <div class="container">
                                                            <table class="table">



                                                                <tbody>
                                                                <tr>
                                                                    <th>Paid By User</th>
                                                                    <td>4555</td>
                                                                </tr>
                                                                <tr  class="bg-ok">
                                                                    <th>Convenience Fee</th>
                                                                    <td >7897</td>
                                                                </tr><tr>
                                                                    <th>Coupon Discount</th>
                                                                    <td>55</td>
                                                                </tr><tr>
                                                                    <th>Admin Fare</th>
                                                                    <td>55</td>
                                                                </tr>
                                                                <tr  class="bg-ok">
                                                                    <th>Commission Earned</th>
                                                                    <td>0</td>
                                                                </tr>




                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page content -->
</div>
<style>
    .section-start{
        padding: 30px 0;
    }
    .t-book-detail{
        text-align: center;
        padding: 10px;
        background-color: #59d999;
        color: white;
        margin-bottom: 0;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;

    }
    .c-items{
        border: 1px solid #9a6de2;
        border-radius: 5px;
    }
    .c-items1{
        border: 1px solid #59d999;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
        padding: 10px;
    }

    .pass-detail{
        text-align: center;
        padding: 10px;
        color: white;
        background-color: #9a6de2;
    }


    .bg-ok{
        background-color: #d0e9c6;
        color: black;
    }
    .Commission tr td{
        text-align: center;
    }
</style>
<!-- end page container -->
<!-- start footer -->
      @include ('admin.include.footer')
<!-- end footer -->
</div>    @include ('admin.include.downjs')
</body>
</html>