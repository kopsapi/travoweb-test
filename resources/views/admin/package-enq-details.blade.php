@include ('admin.include.topcss')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#instdate" ).datepicker({
            autoclose: true,
            todayHighlight: true,
            dateFormat: 'dd/mm/yy',
            minDate: 0,
            });

    } );
</script>
<style type="text/css">
    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }
    
    .pagination>.active>span {
        font-weight: bold;
        color: #fff;
        background-color: #337ab7;
    }
    
    ul.pagination {
        float: right;
        margin-right: 20px;
    }
</style>
<style>
    .section-start {
        padding: 30px 0;
    }
    
    .t-tran-detail {
        text-align: center;
        padding: 10px;
        background-color: #e26d78;
        color: white;
    }
    
    .c-items {
        border: 1px solid #c9c9c9;
        border-radius: 5px;
    }
    
    .onword-flight {
        text-align: left;
        color: white;
        padding: 10px;
        background-color: #21b1e7;
    }
    
    .t-note {
        padding: 13px;
        margin-top: -8px;
        font-size: 16px;
        border-left: 5px solid #21b1e7;
    }
    
    .flight-gif {
        width: 50px;
    }
    
    .pass-detail {
        text-align: center;
        padding: 10px;
        color: white;
        background-color: #e26d78;
    }
    
    .pass-contact {
        text-align: center;
        padding: 10px;
        background-color: #e26d78;
        color: white;
    }
    
    .t-total {
        font-size: 17px;
    }
    
    .t-badge {
        background-color: #ff6981;
        color: white;
        padding: 5px 10px;
    }
    
    .t-fare {
        text-align: center;
        padding: 10px;
        background-color: #fa9e1b;
        color: white;
    }
    
    .t-comm {
        text-align: center;
        padding: 10px;
        background-color: #fa9e1b;
        color: white;
    }
    
    .t-badge-comm {
        padding: 5px 10px;
        border-radius: 20px;
        font-size: 13px;
        background-color: #21b1e7;
    }
    
    .Commission tr td {
        text-align: center;
    }
</style>
<!-- END HEAD -->

<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->@include ('admin.include.header')
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
            <!-- start sidebar menu -->@include ('admin.include.navbar')
            <!-- end sidebar menu -->
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">All Package Enquiry</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;<i class="fa fa-angle-right"></i> </li>
                                <li><a class="parent-item" href="">Package Enquiry</a>&nbsp;<i class="fa fa-angle-right"></i> </li>
                                <li class="active">All Package Enquiry</li>
                            </ol>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header>All Package Enquiry </header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                    <div class="row p-b-20">
                                        <div class="col-md-6 col-sm-6 col-6">
                                            </div>
                                    </div>
                                   
                                    <section class="section-start">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class=" c-items">
                                                        <h3 class="t-tran-detail">Enquiry Detail</h3>
                                                        <div class="container">
                                                            <table class="table">
                                                                <?php
                                                                if($booklistdetails->enq_id=='1')
                                                                {
                                                                    $sta='Pending';
                                                                }
                                                                else if($booklistdetails->enq_id=='2')
                                                                {
                                                                    $sta='Amount Pending';
                                                                }
                                                                else if($booklistdetails->enq_id=='3')
                                                                {
                                                                    $sta='Installment';
                                                                }
                                                                else
                                                                {
                                                                    $sta='Approved';
                                                                }
                                                                ?>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>Enq Id:</th>
                                                                        <td>{{$booklistdetails->enq_id}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Enq Status:</th>
                                                                        <td style="background-color: rgba(255,29,43,0.55)">
                                                                           {{$sta}}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Name:</th>
                                                                        <td>{{ucfirst($booklistdetails->firstname)}} {{ucfirst($booklistdetails->lastname)}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Email</th>
                                                                        <td>{{$booklistdetails->email}} </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <th>Mobile</th>
                                                                        <td>{{$booklistdetails->mobile}} </td>
                                                                    </tr>
                                                                   <tr>
                                                                        <th>Total Adult</th>
                                                                        <td>{{$booklistdetails->adult}} </td>
                                                                    </tr>
                                                                     <tr>
                                                                        <th>Total Child</th>
                                                                        <td>{{$booklistdetails->child}} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Traveler Date</th>
                                                                        <td>{{$booklistdetails->pckgdate}} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Traveler Enquiry</th>
                                                                        <td>{{$booklistdetails->travel_msg}} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Enq Time:</th>
                                                                        <td style="background-color: rgba(255,29,43,0.55)">
                                                                           {{$booklistdetails->travel_type}}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    @if($booklistdetails->travel_type=='Specific Time')
                                                    <section class="section-start">
                                                        <div class="c-items">
                                                            <h3 class="onword-flight">Specific Time</h3>
                                                            
                                                            <table class="table">
                                                                <tbody>
                                                                    <tr>

                                                                        <th> <img class="flight-gif" src="" style="width:150px"></th>
                                                                        <td>
                                                                            <table class="table table-borderless">
                                                                                
                                                                                <tr>
                                                                                    <td>Date :  </td>
                                                                                    <td>{{$booklistdetails->specific_date}}</td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td>Time : </td>
                                                                                    <td>{{$booklistdetails->specific_time}}</td>
                                                                                </tr>
                                                                               
                                                                            </table>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                   
                                                               
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        
                                                    </section>
                                                    @endif
                                                </div>
                                               <?php
                                               if($booklistdetails->currency=='INR')
                                               {
                                                $curicon='fa-inr';
                                               }
                                               else
                                               {
                                                $curicon='fa-usd';
                                               }
                                               ?>
                                               
                                                <div class="col-sm-6">
                                                    <div class="c-items">
                                                        <h3 class="t-fare">Packages Detail</h3>
                                                        <div class="container">
                                                            <table class="table table-striped">
                                                                <tbody>
                                                                    <tr>
                                                                        <th>Package Id </th>
                                                                        <td>{{$booklistdetails->pckg_id}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Package Name </th>
                                                                        <td> {{$booklistdetails->pckg_name}}</td>
                                                                    </tr>
                                                                   
                                                                      
                                                                     <tr>
                                                                        <th>Package Price </th>
                                                                        <td><i class="fa {{$curicon}}"></i> {{$booklistdetails->pckg_price}}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="c-items">
                                                        <h3 class="t-comm">Offer Price</h3>
                                                        <div class="container">
                                                            <table class="table table-striped Commission">
                                                                <tbody>
                                                                    <input type="hidden" id="pckgid" value="{{$booklistdetails->pckg_id}}" >
                                                                    <input type="hidden" id="enqid" value="{{$booklistdetails->enq_id}}" >
                                                                    <input type="hidden" id="tblid" value="{{$booklistdetails->id}}" >
                                                                    <input type="hidden" id="currency" value="{{$booklistdetails->currency}}" >
                                                                    <?php
                                                                    if(!empty($bookbal))
                                                                    {
                                                                        for($j=0;$j<count($bookbal);$j++)
                                                                        {
                                                                        ?>

                                                                        <tr>
                                                                            <th>Message</th>
                                                                            <td>{{ $bookbal[$j]['offer_msg']}} </td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Offer Amount</th>
                                                                            <td><i class="fa {{$curicon}}"> {{ $bookbal[$j]['offer_main_price']}} </td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Balance Amount</th>
                                                                            <td><i class="fa {{$curicon}}"> {{ $bookbal[$j]['offer_balance']}} </td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <?php
                                                                        if($bookbal[$j]['offer_balance'] !=0 )
                                                                        {
                                                                        ?>
                                                                        <tr>
                                                                            <th>Installment date</th>
                                                                            <td> 
                                                                                <?php echo  date("d-m-Y", strtotime($bookbal[$j]['intallmendate'])); ?>
                                                                            </td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <?php 
                                                                        } 
                                                                        ?>

                                                                   <?php 
                                                                         }        
                                                                    }
                                                                    if(!empty($paymenttable))
                                                                    {
                                                                    ?>
                                                                    <tr>
                                                                        <th>Order Id</th>
                                                                        <th>Invoice No</th>
                                                                        <th>Invoice Date</th>
                                                                        
                                                                    </tr>
                                                                    <?php  
                                                                        for($t=0;$t<count($paymenttable);$t++)
                                                                        {
                                                                    ?>
                                                                    <tr>
                                                                        <td>{{$paymenttable[$t]['orderid']}}</td>
                                                                        <td>{{$paymenttable[$t]['referenceid']}}</td>
                                                                        <td><?php echo  date("d-m-Y", strtotime($paymenttable[$t]['orderdate'])); ?></td>
                                                                    </tr>
                                                                    <?php
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                    ?>
                                                                    <tr>
                                                                        <th>Invoice</th>
                                                                        <td>Pending</td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                    <?php } 
                                                                    if(!empty($bookbalnew))
                                                                    {
                                                                       $bal = $bookbalnew->offer_balance;
                                                                    }
                                                                    else
                                                                    {
                                                                        $bal=1;
                                                                    }

                                                                    if($bal =='0')
                                                                    {

                                                                    }
                                                                    else
                                                                    {

                                                                    ?>
                                                                    <tr>
                                                                        <th>Enter Message</th>
                                                                        <td><textarea class="form-controll" id="offer_msg"></textarea> </td>
                                                                        <td><span id="offer_msg_error" style="color:red; display:none"></span></td>
                                                                    </tr>
                                                                    <?php
                                                                    if(!empty($bookbalnew))
                                                                    {

                                                                        
                                                                    ?>
                                                                    <tr>
                                                                        <th>Enter offer Amount</th>
                                                                        <td><i class="fa {{$curicon}}"> <input type="text" name="offer_mainprice" id="offer_mainprice" class="form-controll leadage" readonly  value="{{$bookbalnew->offer_balance}} "> </td>
                                                                        
                                                                    </tr>
                                                                <?php } else { ?>
                                                                    <tr>
                                                                        <th>Enter offer Amount</th>
                                                                        <td><i class="fa {{$curicon}}"> <input type="text" name="offer_mainprice" id="offer_mainprice" class="form-controll leadage"  > </td>
                                                                        <td><span id="offer_mainprice_error" style="color:red; display:none"></span></td>
                                                                    </tr>
                                                                    <?php }
                                                                    ?>
                                                                    <tr>
                                                                        <th>Enter Pay Amount</th>
                                                                        <td><i class="fa {{$curicon}}"> <input type="text" name="offerprice" id="offer_price" class="form-controll leadage"  value="0" > </td>
                                                                        <td><span id="offer_error" style="color:red; display:none"></span></td>
                                                                    </tr>
                                                                    <?php
                                                                    if(!empty($bookbalnew))
                                                                    {
                                                                      
                                                                    ?>
                                                                    <tr>
                                                                        <th>Balance Amount</th>
                                                                        <td><i class="fa {{$curicon}}"> <input type="text" name="balprice" id="offer_balprice" class="form-controll leadage" value="{{$bookbalnew->offer_balance}} " readonly  > </td>
                                                                        <td><span id="bal_error" style="color:red; display:none"></span></td>
                                                                    </tr>
                                                                    <tr style="display:none" id="showins">
                                                                        <th>Installment Date</th>
                                                                        <td><input type="text" name="packgvalid" id="instdate" placeholder="Enter Installment Date" class="e-input filltext baldate ">
                                                                          </td>
                                                                         <td><span id="inst_error" style="color:red; display:none"></span></td>
                                                                      
                                                                    </tr>
                                                                <?php } else { ?>
                                                                    <tr>
                                                                        <th>Balance Amount</th>
                                                                        <td><input type="text" name="balprice" id="offer_balprice" class="form-controll leadage" value="0" readonly  > </td>
                                                                      
                                                                    </tr>
                                                                    <tr style="display:none" id="showins">
                                                                        <th>Installment Date</th>
                                                                        <td><input type="text" name="packgvalid" id="instdate" placeholder="Enter Installment Date" class="e-input filltext baldate "></td>
                                                                        <td> <span id="inst_error" style="color:red; display:none"></span> </td>
                                                                      
                                                                    </tr>

                                                                <?php } ?>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td><button type="submit" id="offer_submit" class="btn btn-primary">Submit</button></td>
                                                                    </tr>
                                                                <?php } ?>
                                                                    <!-- <tr>
                                                                        <th>Markup </th>
                                                                        <td><span class="bagde badge-success t-badge-comm"><i class="fa fa-rupee"></i> 455 </span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Commission Earned </th>
                                                                        <td><span class="bagde badge-success t-badge-comm"><i class="fa fa-rupee"></i> 12 </span></td>
                                                                    </tr> -->
                                                                </tbody>
                                                            </table>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page content -->
        </div>
        <!-- end page container -->
        <!-- start footer -->@include ('admin.include.footer')
        <!-- end footer -->
    </div> 
    <script src="{{ asset('assets/admin/assets/plugins/popper/popper.min.js') }}"></script>
<script src="{{ asset('assets/admin/assets/plugins/jquery-blockui/jquery.blockui.min.js') }}"></script>
<script src="{{ asset('assets/admin/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- bootstrap -->
<script src="{{ asset('assets/admin/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/admin/assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('assets/admin/assets/js/app.js') }}"></script>
<!-- Common js-->
<script src="{{ asset('assets/admin/assets/js/pages/sparkline/sparkline-data.js') }}"></script>
<script src="{{ asset('assets/admin/assets/js/layout.js') }}"></script>
<script src="{{ asset('assets/admin/assets/js/theme-color.js') }}"></script>
<!-- material -->
<script src="{{ asset('assets/admin/assets/plugins/material/material.min.js') }}"></script>
<!-- animation -->
<script src="{{ asset('assets/admin/assets/js/pages/ui/animations.js') }}"></script>
<!-- morris chart -->
<script src="{{ asset('assets/admin/assets/plugins/morris/raphael-min.js') }}"></script>
<!-- end js include path -->
<script src="{{ asset('assets/admin/assets/plugins/counterup/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('assets/admin/assets/plugins/counterup/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('assets/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/sweetalert/jquery.sweet-alert.custom.js') }}"></script>
</body>
<script>
    $(document).ready(function () {
  
  $(".leadage").keypress(function (e) {
    
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       return false;
    }
   });
});

</script>
<script>
    $(document).on('click','#offer_submit',function(){
     
        var offer_msg=$('#offer_msg').val();
        var pckgid=$('#pckgid').val();
        var enqid=$('#enqid').val();
        var tblid=$('#tblid').val();
        var currency=$('#currency').val();
        var offermainprice=$('#offer_mainprice').val();
        var offer_price=$('#offer_price').val();
        var offer_bal_price = $('#offer_balprice').val();
        var instdate=$('#instdate').val();
        if(parseInt(offer_bal_price) >=0 )
        {
            instda=$('#instdate').val();
        }
        else
        {
            instda="";
        }
       
        if(offermainprice==" ")
        {
            $('#offer_mainprice_error').text('Please Enter Offer Price');
            $('#offer_mainprice_error').show();
        }
        else if(offer_price=="0")
        {
            $('#offer_error').text('Please Enter Amount Price');
            $('#offer_error').show();
        }
        else if(parseInt(offermainprice) < parseInt(offer_price))
        {
            $('#offer_error').text('Payment is exceeded by '+(parseInt(offermainprice)-parseInt(offer_price)));
            $('#offer_error').show();
        }
        else if(offer_msg=="")
        {
            $('#offer_msg_error').text('Please Enter Message');
            $('#offer_msg_error').show();
        }
        else if(parseInt(offer_bal_price) > 0 &&  instdate=="" )
        {
            
                $('#inst_error').text("Please Enter Installment  Date");
                $('#inst_error').show();
        }
        else
        {
            
            $('#offer_error').hide();
            $.ajax({
                    url:"{{route('offerprice_insert')}}",
                    data : {'offer_price' :offer_price,
                            'offer_msg':offer_msg,
                            'pckgid':pckgid,
                            'enqid':enqid,
                            'tblid':tblid,
                            'currency':currency,
                            'offermainprice':offermainprice,
                            'offer_bal_price':offer_bal_price,
                            'instda':instda,
                             },
                    type:'GET',
                    success:function(data)
                    {
                        if(data=='success')
                        {
                            swal({
                                       title: "Successfully Send offer",
                                       text: "",
                                       type: "success",
                                       showCancelButton: false,
                                       confirmButtonColor: "#DD6B55",
                                       confirmButtonText: "Ok",
                                       cancelButtonText:false,
                                       closeOnConfirm: false,
                                       closeOnCancel: false
                                   }, function(isConfirm) {
                                       if (isConfirm) {
                                        location.reload();
                                     } 
                               });
                           
                        }
                    }
            })
        }
    })
</script>
<script>
    $(document).on('keyup','#offer_mainprice,#offer_price',function(){

        var offermainprice=$('#offer_mainprice').val();
        var offer_price=$('#offer_price').val();
       $('#offer_balprice').val(offermainprice);
        var mainprice=parseInt(offermainprice)- parseInt(offer_price );
           
             $('#offer_balprice').val(mainprice);
             if(mainprice <=0)
            {
               
                $('#showins').hide();
            }
            else
            {
                $('#showins').show();
            }
    })
</script>
<script>
    $(document).on('keyup','#offer_price',function(){

        var offermainprice=$('#offer_mainprice').val();
        var offer_price=$('#offer_price').val();
        if(parseInt(offermainprice) < parseInt(offer_price))
        {
            $("#offer_price").css("border", "1px solid #fd0000");
            $('#offer_error').text('Payment is exceeded by '+(parseInt(offermainprice)-parseInt(offer_price)));
            $('#offer_error').show();
        }
        else
        {
            $("#offer_price").css("border", "1px solid #aaa");
             $('#offer_error').text("");
             $('#offer_error').hide();
             if(offer_price=="")
             {
               var offer_price1=0;
             }
             else
             {
               var offer_price1=offer_price;
             }
            var mainprice=parseInt(offermainprice)- parseInt(offer_price1 );
             $('#offer_balprice').val(mainprice);
                if(mainprice <=0)
                {
                   
                    $('#showins').hide();
                }
                else
                {
                    $('#showins').show();
                }
            
        }
       
    })
</script>
</html>