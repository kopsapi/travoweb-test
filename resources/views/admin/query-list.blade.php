@include ('admin.include.topcss')

<style type="text/css">

    .pagination>li>a, .pagination>li>span 

    {

        position: relative;

        float: left;

        padding: 6px 12px;

        line-height: 1.42857143;

        color: #337ab7;

        text-decoration: none;

        background-color: #fff;

        border: 1px solid #ddd;

    }

    .pagination>.active>span

    {

        font-weight:bold;

        color:#fff;

        background-color: #337ab7;

    }

    ul.pagination 

    {

        float: right;

        margin-right: 20px;

    }
     .c-btn1 {
        padding: 10px 15px;
        border: none;
        color: white;
        background: #fa9e1b;
        width: 100%;
        border-radius: 5px;
    }

.book-id-btn {
        color: #21b1e7;
        background-color: white;
        border: 1px solid #21b1e7;
        border-radius: 5px !important;
        padding: 5px 10px !important;
        white-space: pre;

    }
    .modal-header.h-bg {
        background: #31124b;
        padding: 10px 21px;
        border-bottom-color: white;
    }
    .modal-footer.f-bg {
        background: #e2e3ea;
    }
    p.t-id-info {
        margin-top: 10px;
        color: red !important;
        font-weight: 600;
    }

    .btn1 {
        font-size: 17px;
        font-weight: 500;
        color: #fff;
        text-transform: uppercase;
        background: #fa9e1b;
        border: none;
        outline: none;
        padding: 8px 17px;
        border-radius: 5px;
        cursor: pointer;
    }
</style>

<!-- END HEAD -->

<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">

    <div class="page-wrapper">

        <!-- start header -->

        @include ('admin.include.header')

        <!-- end header -->

        <!-- start page container -->

        <div class="page-container">

            <!-- start sidebar menu -->

            @include ('admin.include.navbar')

            <!-- end sidebar menu -->

            <!-- start page content -->

            <div class="page-content-wrapper">

                <div class="page-content">

                    <div class="page-bar">

                        <div class="page-title-breadcrumb">

                            <div class=" pull-left">

                                <div class="page-title">All Enquiry</div>

                            </div>

                            <ol class="breadcrumb page-breadcrumb pull-right">

                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;<i class="fa fa-angle-right"></i>

                                </li>

                                <li><a class="parent-item" href=""> Enquiry</a>&nbsp;<i class="fa fa-angle-right"></i>

                                </li>

                                <li class="active">All Enquiry</li>

                            </ol>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">

                            <div class="card card-box">

                                <div class="card-head">

                                    <header>All Enquiry </header>



                                    <div class="tools">

                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>

                                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

                                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>

                                    </div>

                                </div>

                                <div class="card-body ">

                                    <div class="row p-b-20">

                                        <div class="col-md-6 col-sm-6 col-6">
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-6">

                                            <div class="btn-group pull-right">

                                            	<div class="card-tools">

                                                   <div class="input-group input-group-sm" style="width: 150px;">

                                                      <input type="text" name="search" id="search" class="form-control float-right" placeholder="Search" value="">

                                                      <div class="input-group-append">

                                                         <button type="button" id="search-btn" class="btn btn-default"><i class="fa fa-search"></i></button>

                                                     </div>

                                                 </div>

                                             </div>

                                               

                                            </div>

                                        </div>

                                    </div>

                                    <div class="table-scrollable" id="tag_container">

                                        <table class="table table-hover table-checkable order-column full-width" id="example4">

                                            <thead>

                                                <tr>

                                                 <th class="center">Id</th>
                                
                                                 <th class="center"> Name </th>
                                                 <th class="center">Email</th>
                                                 <th class="center">Contact No </th>
                                                <th class="center"> Subject</th>
                                                 <th class="center">Address</th>
                                                <th class="center"> Enq Date</th>
                                                 <!-- <th class="center"> View Details</th> -->
                  


                                                 <!-- <th class="center"> Action </th> -->

                                             </tr>

                                         </thead>

                                         <tbody>
                                        <?php
                                        $ht=1;
                                        ?>
                                          @foreach($pckglist as $pckg)

                                          <tr class="odd gradeX">

                                            <td class="center">{{$ht++}}</td>
                                            
                                            <td class="center">{{ucfirst($pckg->contact_name)}}</td>
                                            <td class="center">{{$pckg->contact_email}}</td>
                                            <td class="center">{{$pckg->contact_phone}}</td>
                                            <td class="center">{{$pckg->contact_subject}}</td>
                                            <td>{{$pckg->contact_message}}</td>
                                            <td class="center"><?php  echo date('d-M-Y',strtotime($pckg->create_date)); 
                                                ?></td>
                                           <!--  <td colspan="2" class="center " style="margin-top: 13px;" id="{{$pckg->id}}"><a class="book-id-btn" href="{{url('admin/hotel-booking-details/'.$pckg->id)}}">View Details</a></td> -->
                              


										    	</tr>
                                            


                                          @endforeach





                                      </tbody>

                                  </table>
                                 {!! $pckglist->render() !!}
                             

                              </div>

                          </div>

                      </div>

                  </div>

              </div>

          </div>

      </div>


<!-- Modal -->

  <!-- end page container -->

  <!-- start footer -->

  @include ('admin.include.footer')

  <!-- end footer -->

</div>

@include ('admin.include.downjs')

<script>

    $(window).on('hashchange', function() {

        if (window.location.hash) {

            var page = window.location.hash.replace('#', '');

            if (page == Number.NaN || page <= 0) {

                return false;

            }else{

                getData(page);

            }

        }

    });

    

    $(document).ready(function()

    {

        $(document).on('click', '.pagination a',function(event)

        {

            event.preventDefault();



            $('li').removeClass('active');

            $(this).parent('li').addClass('active');



            var myurl = $(this).attr('href');

            var page=$(this).attr('href').split('page=')[1];



            getData(page);

        });



    });



    function getData(page){

        $.ajax(

        {

            url: '?page=' + page,

            type: "get",

            datatype: "html"

        }).done(function(data){

        	var data1=jQuery(data).find('#tag_container').html();

            $("#tag_container").empty().html(data1);

            location.hash = page;

        }).fail(function(jqXHR, ajaxOptions, thrownError){

          alert('No response from server');

      });

    }

    $(document).on('keypress','#search',function(e)

    {

        if(e.which==13)

        {

            var search=$('#search').val();

            

            $.ajax(

            {

                url: '{{route("query-list")}}',

                data: {

                    'search':search,

                    '_token':'{{csrf_token()}}'

                },

                type: 'POST',

                success: function (data) 

                {

                	var data1=jQuery(data).find('#tag_container').html();

                    $("#tag_container").empty().html(data1);

                }

            });

            return false;

        }

    });

    $(document).on('click','#search-btn',function(e)

    {

        var search=$('#search').val();

      

        $.ajax(

        {

            url: '{{route("query-list")}}',

            data: {

                'search':search,

                '_token':'{{csrf_token()}}'

            },

            type: 'POST',

            success: function (data) 

            {

                var data1=jQuery(data).find('#tag_container').html();

                $("#tag_container").empty().html(data1);

            }

        });

    });

</script>


</body>

</html>