@include('pages.include.header')
<!-- Loader -->
<style>
    .loader {
        display: block;
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: #CCEAF7 url('{{ asset('assets/images/flight-loader.gif')}}') no-repeat center center;
        text-align: center;
        color: #999;
        opacity: 0.9;
    }

    ul.header-nav .current a,
    ul.header-nav .active:hover,
    ul.header-nav .active:focus {
        background-color: transparent !important;
        border: none;
        color: #005285 !important;
    }

    ul.header-nav a {
        border-bottom: none !important;
    }

    ul.header-nav a.active {
        border-bottom: 2px solid #fa9e1b !important;
        border-top: none !important;
        border-right: none !important;
        border-left: none !important;
    }

    ul.header-nav {
        list-style-type: none;
        margin: 0;
        padding: 0px;
        overflow: hidden;
        background-color: #ffffff;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
        box-shadow: 0 2px 20px 0 rgba(0, 0, 0, .1);
    }

    .header-nav li {
        float: left;
        position: relative;
    }

    .header-nav li a {
        display: block;
        color: #005285;
        font-weight: bold;
        font-size: 18px;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
    }


</style>
<style>
    .rp-done {
        height: 80px;
        position: absolute;
        top: -13px;
        left: -16px;
        z-index: 99;
    }
    .over-modal {
        background: #00000061;
        overflow: hidden;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    img.recover-pass-img {
        height: 125px;
        display: block;
        margin: 0 auto 0 32%;
    }

    .modal-bg {
        border-radius: 7px;
        margin: 18% auto;
        background-image: url("{{asset('assets/admin/assets/img/pr-img.png')}}") !important;
        background-repeat: no-repeat;
        background-size: cover
    }
    .btn2 {
        font-size: 17px;
        font-weight: 500;
        color: #fff;
        border: 1px solid black;
        text-transform: uppercase;
        background: rgba(0, 0, 0, 0.8);
        outline: none;
        padding: 8px 17px;
        border-radius: 5px;
        display: block;
        margin: auto;
        width: 50%;
        cursor: pointer;
    }

    .btn2:hover {

        color: white !important;
        font-weight: 600 !important;
        background: transparent;
        border: 1px solid black;

    }
    img.f-logo {
        height: 22px;
    }
    input.t-email:-webkit-autofill,
    input.t-email:-webkit-autofill:hover,
    input.t-email:-webkit-autofill:focus,
    input.t-email:-webkit-autofill:active {
        transition: background-color 5000s ease-in-out 0s;

        -webkit-text-fill-color:#fff;

    }
    textarea.t-email1:-webkit-autofill,
    textarea.t-email1:-webkit-autofill:hover,
    textarea.t-email1:-webkit-autofill:focus,
    textarea.t-email1:-webkit-autofill:active ,
    textarea.t-email1:-webkit-autofill:placeholder-shown,
    textarea.t-email1:-webkit-autofill::placeholder{
        transition: background-color 5000s ease-in-out 0s !important;

        -webkit-text-fill-color:#fff !important;

    }



    input.t-email:focus,input.t-email1:focus{
        box-shadow: none;
    }

    button.t-btn {
        padding: 8px 20px;
        width: 70%;
        margin-left: auto;
        color: white;
        margin-right: auto;
        cursor: pointer;
        /* border-radius: 20px; */
        background-color: #fa9e1b;
        transition: all .5s ease;
    }
    .modal-login .form-control {
        padding-left: 40px;
    }
    button.t-btn:hover {

        width: 70%;

    }



    input.t-email,textarea.t-email{
        border: none;
        border-bottom: 1px solid #ffffff;
        width: 200px;
        color: white!important;
        text-align: center;
        margin-left: auto;
        margin-right: auto;
        border-radius: 0;
        background-color: transparent;
        outline: none !important;
        transition: all .5s ease;
    }


    .modal-header {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: start;
        align-items: flex-start;
        -ms-flex-pack: justify;
        justify-content: space-between;
        padding: 15px;
        border-bottom: 1px solid #fa9e1b;
        border-top-left-radius: .3rem;
        border-top-right-radius: .3rem;
    }
    .modal-header h3{
        color: white;
    }
    .modal-login i {
        position: absolute;
        left: 13px;
        top: 11px;
        font-size: 18px;
    }
    .modal{
        background-color: rgba(0,0,0,0.5);
    }


    .form-group {
        padding-bottom: 10px;
    }

    p.forgot-p>a {
        color: #fa9e1b;
        border: none!important;
        text-decoration: underline;
        transition: all .5s ease;
    }
    p.forgot-p a:hover {
        color: #fa9e1b ;
        background: none;
        border: none!important;
    }


    .modal-content{
        margin-top: 20%;
        box-shadow: 0 4px 8px 0 rgb(0, 0, 0);
    }


    @media screen and (max-width: 576px){

        .modal-header h3 {
            color: white;
            font-size: 18px;
            margin: 0 !important;
        }
        .modal-header .close {
            padding: 15px;
            margin: -20px -15px -15px auto;
        }
        .modal-login .form-control {
            padding-left: 10px; !important;
        }

        .modal-content {
            padding: 0;
            margin: 0;
        }

    }
    .s-icon a:hover{
        color: white;
        text-decoration: none;
    }
    .s-icon a span{
        transition: all .5s ease;
    }







    span.close.close-btn {
        color: white;
        font-size: 26px;
        font-weight: 600;
        cursor: pointer;
    }

    span.close.close-btn1 {
        color: #000000;
        font-size: 26px;
        font-weight: 600;
        cursor: pointer;
        text-shadow: 0 1px 0 #000000;
    }


    @media screen and (max-width: 650px) {

        .modal-bg{
            width: auto !important;
        }


    }
</style>
<!-- Loader -->

<body>
<div class="loader"></div>
<div class="super_container">
    <!-- Header -->@include('pages.include.topheader')
    <div class="home" style="height:20vh;">
    <!-- <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/about_background.jpg')}}"></div> -->
        <div class="home_content">
            <div class="home_title">Results</div>
        </div>
    </div>

    <!-- Intro -->


    <div class="container" style="margin: 50px auto 200px auto;">


        <div class="h-form" style="border-radius: 6px">
            <div class="over" style="border-radius: 6px">
                <h3 class="c-head pt-5">Change Password</h3>
                <div class="container" style="padding: 36px 100px 0;">
                    <div class="alert alert-danger alert-dismissible" id="error_div" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="icon fa fa-ban"></i><span id="error"></span>
                    </div>
                    <div class="alert alert-success alert-dismissible" id="success_div" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="icon fa fa-check"></i><span id="success"></span>
                    </div>
                </div>
                <input name="_token" type="hidden" class="csrf_token" id="csrf_token" value="{{ csrf_token() }}"/>

                <form id="reset-form" method="post" style="padding: 50px 100px;" autocomplete="off">
                {{ csrf_field() }}

                <!-- <div class="form-group mb-4">
                        <label for="email">Email address:</label>
                        <input type="email" class="form-control t-mail" id="email" placeholder="Enter your Password">
                    </div> -->
                    <input type="hidden" name="id" value="">
                    <div class="form-group mb-4">
                        <label for="pwd">New Password:</label>
                        <input type="password" name="password" class="form-control t-mail textfield" id="password" placeholder="Enter your Password">
                        <span class="text-danger err-msg password textfield_error"  id="password_error" style="display:none"></span>
                    </div>

                    <div class="form-group mb-4">
                        <label for="pwd">Confirm Password:</label>
                        <input type="password" name="repassword" class="form-control t-mail textfield" id="retype_password" placeholder="Enter your Password">
                        <span  class="text-danger err-msg repassword textfield_error" id="retype_password_error" style="display:none"></span>
                    </div>
                    <!-- <div class="form-group form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox"> Remember me
                        </label>
                    </div> -->
                    <button type="submit" name="submit" id="reset-button" class="btn-chn">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>



<div class="modal fade text-center py-5" style="top:30px;background-color: rgba(0,0,0,0); " id="myModal_chargesfor">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content modal-bg" style="margin-top: 12%; width: 350px">
            <img src="{{asset('assets/images/green-tic-icon.png')}}" class="rp-done">
            <div class="over-modal">


                <div class="modal-body">

                    <span class="close close-btn1" data-dismiss="modal">&times;</span>


                    <img src="{{asset('assets/images/icon-recover-password-512.png')}}" class="recover-pass-img">

                    <h3 class="pt-3 mb-0 h-cancel success_modal alert alert-success" style="font-size: 16px;    margin: 15px 0 20px !important;">Reset Password Sent to this email</h3>
                    <!-- <p class="t-id-info">Your Change Request Id:1234</p> -->
                    <button class="btn2 text-white mb-5 success_ok"
                            style="margin-top:45px; margin-bottom: 0px !important" data-dismiss="modal">OK
                    </button>
                    <!--   <a role="button" class="btn1 text-white mb-5 success_ok" href="https://www.sunlimetech.com" target="_blank">Submit</a> -->
                </div>
                <div class="modal-footer f-bg mt-3" style="background: rgba(49, 18, 75, 0.8);"
                >
                    <span class="d-block mr-auto f-help " style="color: white;"><i class="fa fa-phone"></i> Helpline:<a href="#" style="color:#FF9800"> 34356894454</a></span>
                    <span class="f-help"><img src="{{asset('assets/images/logo.png')}}" class="f-logo"></span>
                </div>
            </div>
        </div>
    </div>
    <style>
        .h-form{
            background: url("{{asset('assets/images/modal-img.jpg')}}");
            background-repeat: no-repeat;
            background-size: cover;
            width: 70%;
            margin:50px auto;
            position: relative;

        }
        .over {
            background: rgba(0,0,0,0.5);
        }
        .btn-chn {
            background: #fa9e1b;
            display: block;
            width: 100%;
            border: none;
            text-align: center;
            padding: 7px 18px;
            border-radius: 5px;
            color: white;
            font-size: 16px;
            font-weight: 500;
        }
        .t-mail:-webkit-autofill{
            background-color: transparent !important;
        }
        .t-mail{
            background-color: transparent !important;
            color: white !important;
        }
        label{
            color: white;
        }
        .t-mail{
            border: none;
            border-bottom: 2px solid white;
            color: white !important;
            background-color: transparent;
            border-radius: 0;
        }
        .t-mail::placeholder{

            color: white !important;
        }
        h3.c-head {
            text-align: center;
            color: white;
            text-transform: uppercase;
            font-weight: bold;
            padding: 30px 30px 0 30px;
        }

    </style>

</div>
<!-- Footer -->@include('pages.include.footer')
<!-- Copyright -->@include('pages.include.copyright')

