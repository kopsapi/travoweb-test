<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="description" content="Responsive Admin Template"/>
    <meta name="author" content="SmartUniversity"/>
    <title>TravoWeb | Admin</title>    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css"/>
    <!-- icons -->
    <link href="{{ asset('assets/admin/assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/assets/plugins/iconic/css/material-design-iconic-font.min.css') }}"
          rel="stylesheet">
    <link href="{{ asset('assets/admin/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/assets/css/pages/extra_pages.css') }}" rel="stylesheet">
    <!-- favicon -->
    <link rel="shortcut icon" href=" {{ asset('assets/admin/assets/img/favicon.ico') }}"/>
    <style>        label.cstm-label {
            color: white !important;
            font-size: 13px !important;
        }

        .forgot-link {
            color: #ff9800;
        }

        .modal-backdrop.show {
            opacity: 0;
            display: none;
        }

        input.t-email:-webkit-autofill, input.t-email:-webkit-autofill:hover, input.t-email:-webkit-autofill:focus, input.t-email:-webkit-autofill:active {
            transition: background-color 5000s ease-in-out 0s;
            -webkit-text-fill-color: #fff;
        }

        span#login_email_err, span#login_password_err {
            width: 100%;
            display: block;
            text-align: center !important;
        }

        span.err-msg {
            width: 100%;
            display: block;
            text-align: center !important;
        }

        .rgba-stylish-strong {
            background-color: rgba(62, 69, 81, .7);
        }

        input.t-email:focus {
            box-shadow: none;
            width: 100% !important;
        }

        button.t-btn {
            padding: 8px 20px;
            width: 70%;
            margin-left: auto;
            color: white;
            margin-right: auto;
            cursor: pointer; /* border-radius: 20px; */
            background-color: #fa9e1b;
            transition: all .5s ease;
        }

        .modal-login .form-control {
            padding-left: 40px;
        }

        button.t-btn:hover {
            width: 70%;
        }

        input.t-email::placeholder, input.t-email1::placeholder, textarea.t-email1::placeholder {
            color: white !important;
        }

        input.t-email, textarea.t-email {
            border: none;
            border-bottom: 1px solid #ffffff;
            width: 200px;
            color: white !important;
            text-align: center;
            margin-left: auto;
            margin-right: auto;
            border-radius: 0;
            background-color: transparent;
            outline: none !important;
            transition: all .5s ease;
        }

        .container.login-or {
            margin-top: 30px; /* margin-bottom: 47px; */
        }

        input.t-email1, textarea.t-email1 {
            border: none;
            color: white !important;
            border-bottom: 1px solid #ffffff;
            width: 100%;
            text-align: center;
            margin-left: auto;
            margin-right: auto;
            border-radius: 0;
            background-color: transparent;
            outline: none !important;
            transition: all .5s ease;
        }

        input.t-email:focus, input.t-email1:focus, textarea.t-email1:focus {
            border-bottom: 1px solid #fff !important;
            outline: none !important;
            background-color: transparent;
            width: 100% !important;
        }

        textarea.t-email1 {
            padding-bottom: 5px;
        }

        .modal-header {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: start;
            align-items: flex-start;
            -ms-flex-pack: justify;
            justify-content: space-between;
            padding: 15px;
            border-bottom: 1px solid #fa9e1b;
            border-top-left-radius: .3rem;
            border-top-right-radius: .3rem;
        }

        .modal-header h3 {
            color: white;
        }

        .modal-login i {
            position: absolute;
            left: 13px;
            top: 11px;
            font-size: 18px;
        }

        .modal {
            background-color: rgba(0, 0, 0, 0.5);
        }

        .modal-header .close1 {
            color: white;
            border: none;
            cursor: pointer;
            outline: none;
            font-size: 30px;
            position: absolute;
            top: -30px;
            right: -19px;
            font-weight: bolder;
        }

        .modal-header .close2 {
            color: white;
            border: none;
            cursor: pointer;
            outline: none;
            font-size: 30px;
            position: absolute;
            top: -11px;
            right: -24px;
            font-weight: bolder;
        }

        .form-group {
            padding-bottom: 10px;
        }

        #loginForm, #registerForm {
            color: white;
            font-size: 17px;
        }

        p.forgot-p {
            color: white;
            font-size: 17px;
            margin-top: -10px;
            margin-bottom: 0;
            text-align: right;
        }

        p.forgot-p > a {
            color: #fa9e1b;
            border: none !important;
            text-decoration: underline;
            transition: all .5s ease;
        }

        p.forgot-p a:hover {
            color: #fa9e1b;
            background: none;
            border: none !important;
        }

        .modal-content {
            margin-top: 20%;
            box-shadow: 0 4px 8px 0 rgb(0, 0, 0);
        }

        .fb {
            background-color: #3B5998;
            color: white;
            display: block;
            position: relative;
            width: 100%;
            text-align: left;
            transition: all .5s ease;
            padding-left: 17px;
        }

        .google {
            background: #dd4b39;
            color: white;
            display: block;
            position: relative;
            width: 100%;
            transition: all .5s ease;
            text-align: left;
            padding-left: 19px;
        }

        .s-icon:hover .fb, .s-icon:hover .google {
            padding-left: 75px;
        }

        .s-icon:hover div.fb-icon-l {
            right: 93px;
            transform: translateY(-50%) rotate(360deg);
        }

        @media screen and (max-width: 576px) {
            .s-icon:hover div.fb-icon-l {
                right: 78%;
            }

            .s-icon:hover .fb, .s-icon:hover .google {
                padding-left: 80px;
            }

            p.forgot-p {
                color: white;
                font-size: 17px;
                margin-top: 25px;
                margin-bottom: 0;
                text-align: right;
            }
        }

        .s-icon a:hover {
            color: white;
            text-decoration: none;
        }

        .s-icon a span {
            transition: all .5s ease;
        }

        i.fa.fa-facebook.fa-fw, i.fa.fa-google.fa-fw {
            font-size: 25px;
            vertical-align: center;
            margin-top: 20px;
            margin-bottom: auto;
        }

        .vl {
            position: absolute;
            left: 50%;
            transform: translate(-50%);
            border: 0.5px solid #f8f9fa;
            width: 100%;
        }

        div.fb-icon-l {
            position: absolute;
            width: 65px;
            height: 65px;
            border-radius: 34px;
            transition: all .4s ease;
            text-align: center;
            background-color: #3B5998;
            color: white; /* border: 1px solid; */
            top: 50%;
            transform: translateY(-50%) rotate(0deg);
            right: 10px;
        }

        .google div.fb-icon-l {
            position: absolute;
            width: 65px;
            height: 65px;
            transition: all .4s ease;
            border-radius: 34px;
            text-align: center;
            color: white;
            background-color: #dd4b39;
            top: 50%;
            transform: translateY(-50%);
            right: 10px;
        }

        .vl-innertext {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 40px;
            transform: translate(-50%, -50%);
            background-color: #f8f9fa;
            border: 1px solid #fa9e1b;
            border-radius: 50%;
            text-align: center;
            color: #fa9e1b;
            font-weight: 600;
            padding: 5px 5px 5px 5px;
            height: 40px;
        }

        span.close.close-btn {
            color: white;
            font-size: 26px;
            font-weight: 600;
            cursor: pointer;
        }

        p.t-id-info {
            margin-top: 10px;
            color: red !important;
            font-weight: 600;
        }

        .btn1 {
            font-size: 17px;
            font-weight: 500;
            color: #fff;
            text-transform: uppercase;
            background: #fa9e1b;
            border: none;
            outline: none;
            padding: 8px 17px;
            border-radius: 5px;
            display: block;
            width: 100%;
            cursor: pointer;
        }    </style>
    <style>        .rp-done {
            height: 80px;
            position: absolute;
            top: -13px;
            left: -16px;
            z-index: 99;
        }

        .over-modal {
            background: #00000061;
            overflow: hidden;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
        }

        img.recover-pass-img {
            height: 125px;
            display: block;
            margin: 0 auto 0 32%;
        }

        .modal-bg {
            border-radius: 7px;
            margin: 18% auto;
            background-image: url("{{asset('assets/admin/assets/img/pr-img.png')}}") !important;
            background-repeat: no-repeat;
            background-size: cover
        }

        .btn2 {
            font-size: 17px;
            font-weight: 500;
            color: #fff;
            border: 1px solid black;
            text-transform: uppercase;
            background: rgba(0, 0, 0, 0.8);
            outline: none;
            padding: 8px 17px;
            border-radius: 5px;
            display: block;
            margin: auto;
            width: 50%;
            cursor: pointer;
        }

        .btn2:hover {
            color: white !important;
            font-weight: 600 !important;
            background: transparent;
            border: 1px solid black;
        }

        img.f-logo {
            height: 22px;
        }

        input.t-email:-webkit-autofill, input.t-email:-webkit-autofill:hover, input.t-email:-webkit-autofill:focus, input.t-email:-webkit-autofill:active {
            transition: background-color 5000s ease-in-out 0s;
            -webkit-text-fill-color: #fff;
        }

        textarea.t-email1:-webkit-autofill, textarea.t-email1:-webkit-autofill:hover, textarea.t-email1:-webkit-autofill:focus, textarea.t-email1:-webkit-autofill:active, textarea.t-email1:-webkit-autofill:placeholder-shown, textarea.t-email1:-webkit-autofill::placeholder {
            transition: background-color 5000s ease-in-out 0s !important;
            -webkit-text-fill-color: #fff !important;
        }

        input.t-email:focus, input.t-email1:focus {
            box-shadow: none;
        }

        button.t-btn {
            padding: 8px 20px;
            width: 70%;
            margin-left: auto;
            color: white;
            margin-right: auto;
            cursor: pointer; /* border-radius: 20px; */
            background-color: #fa9e1b;
            transition: all .5s ease;
        }

        .modal-login .form-control {
            padding-left: 40px;
        }

        button.t-btn:hover {
            width: 70%;
        }

        input.t-email, textarea.t-email {
            border: none;
            border-bottom: 1px solid #ffffff;
            width: 200px;
            color: white !important;
            text-align: center;
            margin-left: auto;
            margin-right: auto;
            border-radius: 0;
            background-color: transparent;
            outline: none !important;
            transition: all .5s ease;
        }

        .modal-header {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: start;
            align-items: flex-start;
            -ms-flex-pack: justify;
            justify-content: space-between;
            padding: 15px;
            border-bottom: 1px solid #fa9e1b;
            border-top-left-radius: .3rem;
            border-top-right-radius: .3rem;
        }

        .modal-header h3 {
            color: white;
        }

        .modal-login i {
            position: absolute;
            left: 13px;
            top: 11px;
            font-size: 18px;
        }

        .modal {
            background-color: rgba(0, 0, 0, 0.5);
        }

        .form-group {
            padding-bottom: 10px;
        }

        p.forgot-p > a {
            color: #fa9e1b;
            border: none !important;
            text-decoration: underline;
            transition: all .5s ease;
        }

        p.forgot-p a:hover {
            color: #fa9e1b;
            background: none;
            border: none !important;
        }

        .modal-content {
            margin-top: 20%;
            box-shadow: 0 4px 8px 0 rgb(0, 0, 0);
        }

        @media screen and (max-width: 576px) {
            .modal-header h3 {
                color: white;
                font-size: 18px;
                margin: 0 !important;
            }

            .modal-header .close {
                padding: 15px;
                margin: -20px -15px -15px auto;
            }

            .modal-login .form-control {
                padding-left: 10px;
            !important;
            }

            .modal-content {
                padding: 0;
                margin: 0;
            }
        }

        .s-icon a:hover {
            color: white;
            text-decoration: none;
        }

        .s-icon a span {
            transition: all .5s ease;
        }

        span.close.close-btn {
            color: white;
            font-size: 26px;
            font-weight: 600;
            cursor: pointer;
        }

        span.close.close-btn1 {
            color: #000000;
            font-size: 26px;
            font-weight: 600;
            cursor: pointer;
            text-shadow: 0 1px 0 #000000;
        }

        @media screen and (max-width: 650px) {
            .modal-bg {
                width: auto !important;
            }
        }    </style>
</head>
<body>
<div class="limiter">
    <div class="container-login100 page-background">
        <div class="wrap-login100">
            <form class="login100-form validate-form">
                {{csrf_field()}}
                <span class="login100-form-logo">
                    <!-- <i class="zmdi zmdi-flower"></i> -->
                    <img src="{{ asset('assets/admin/assets/img/logo.png') }}" class="img-responsive" style="width:auto; box-shadow:none;">
                </span>

                <div class="alert alert-danger alert-dismissible" id="error_div" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="icon fa fa-ban"></i>
                    <span id="error"></span>
                </div>
                <div class="alert alert-success alert-dismissible" id="success_div" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="icon fa fa-check"></i>
                    <span id="success"></span>
                </div>
                <span class="login100-form-title p-b-34 p-t-27">Log in</span>
                <div class="wrap-input100 validate-input" data-validate="Enter username">
                    <input class="input100 textfield" type="text" name="username" placeholder="Email/Username" id="username">
                    <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    <span class="textfield_error" id="username_error"></span></div>
                <div class="wrap-input100 validate-input" data-validate="Enter password">
                    <input class="input100 textfield" type="password" name="pass" placeholder="Password" id="password">
                    <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    <span class="textfield_error" id="password_error"></span>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="contact100-form-checkbox">
                            <input class="input-checkbox100 remember" id="ckb1" type="checkbox" name="remember-me">
                            <label class="label-checkbox100" for="ckb1"> Remember me </label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="forgot-pass-div"><label class="cstm-label" for="ckb1"> Forgot Password <a
                                        href="#forget2" class="forgot-link" data-toggle="modal">Click Here</a> </label>
                        </div>
                    </div>
                </div>
                <div class="container-login100-form-btn">
                    <button type="button" class="login100-form-btn" id="login_button"> Login</button>
                </div>
                <!-- <div class="text-center p-t-27">
                 <a class="txt1" href="#">	Forgot Password?</a>
              </div> -->
            </form>
        </div>
        <div class="modal fade text-center py-5" style="" id="forget2">
            <div class="modal-dialog modal-login" role="document">
                <div class="modal-content" style="background: rgb(53, 55, 64);    padding: 2em;">
                    <div class="modal-header h-bg">
                        <h3>Change Password</h3>
                        <span class="close close-btn" data-dismiss="modal">&times;</span>
                    </div>
                    <input name="_token" type="hidden" class="csrf_token" id="csrf_token" value="{{ csrf_token() }}"/>
                    <div class="modal-body">
                        <!-- login form  -->
                        <form id="forgotForm">
                            {{ csrf_field() }}
                            <h5 id="successMessage1" class="alert alert-success" style="display: none"></h5>
                            <h5 id="failMessage1" class="alert alert-danger" style="display: none"></h5>
                            <div class="form-group md-form mb-4">
                                <input type="email" name="forgotemail" id="forgotemail" class="form-control t-email forgotemail" placeholder="Your Email" autofocus>
                                <span id="login_email_err" class="text-danger eerror" style="display:none"></span>
                            </div>
                            <div class="form-group">
                                <button type="button" name="submit" class="form-control t-btn"
                                        data-target="#myModal_adminchargesnew" data-toggle="modal"> Submit
                                </button>
                            </div>
                        </form>
                        <!-- end loginform -->

                        <!-- <div class="form-group md-form mt-3">
                          <input type="email" name="forgotemail" id="forgotemail" class="form-control t-email" placeholder="Your Email" autofocus>                                                                                        <span id="login_email_err" class="text-danger eerror" style="display:none" ></span>                                                                                                                    </div> -->
                    </div>
                    <!-- <div class="modal-footer" style="border: none">
                    <button type="button" class="btn1 text-white" id="forgot-button" >Submit</button>                         </div> -->
                </div>
            </div>
        </div>
        <div class="modal fade text-center py-5" style="top:30px;background-color: rgba(0,0,0,0); " id="myModal_adminchargesnew">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content modal-bg" style="margin-top: 12%; width: 350px">
                    <img src="{{asset('assets/images/green-tic-icon.png')}}" class="rp-done">
                    <div class="over-modal">
                        <div class="modal-body"><span class="close close-btn1" data-dismiss="modal">&times;</span>
                            <img src="{{asset('assets/images/icon-recover-password-512.png')}}" class="recover-pass-img">
                            <h3 class="pt-3 mb-0 h-cancel success_modal alert alert-success" style="font-size: 16px;    margin: 15px 0 20px !important;">Reset Password Sent to this email</h3>
                            <!-- <p class="t-id-info">Your Change Request Id:1234</p> -->
                            <button class="btn2 text-white mb-5 success_ok"
                                    style="margin-top:45px; margin-bottom: 0px !important" data-dismiss="modal">OK
                            </button>
                            <!--   <a role="button" class="btn1 text-white mb-5 success_ok" href="https://www.sunlimetech.com" target="_blank">Submit</a> -->
                        </div>
                        <div class="modal-footer f-bg mt-3" style="background: rgba(49, 18, 75, 0.8);">
                            <span class="d-block mr-auto f-help " style="color: white;">
                                <i class="fa fa-phone"></i> Helpline:<a href="#" style="color:#FF9800"> 34356894454</a>
                            </span>
                            <span class="f-help">
                                <img src="{{asset('assets/images/logo.png')}}" class="f-logo">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    <!-- start js include path -->
<script src="{{ asset('assets/admin/assets/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/admin/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/admin/assets/js/pages/extra_pages/login.js') }}"></script>
<script type="text/javascript">        $(document).on('blur', '.textfield', function () {
        var id = this.id;
        if ($('#' + id).val().trim() != "") {
            $('#' + id + '_error').text('');
            $('#' + id + '_error').hide();
        }
        if (id == "username" && $('#' + id).val().trim() == "") {
            $('#' + id + '_error').text('Please Enter Username');
            $('#' + id + '_error').show();
        } else if (id == "password" && $('#' + id).val().trim() == "") {
            $('#' + id + '_error').text('Please Enter Password');
            $('#' + id + '_error').show();
        }
    });    </script>
<script type="text/javascript">        $(document).on('click', '#login_button', function () {
        $('.textfield_error').text('');
        $('.textfield_error').hide();
        $('#error').text('');
        $('#error_div').hide();
        $('#success').text('');
        $('#success_div').hide();
        $('.textfield_error').hide();
        if ($('#username').val().trim() == "") {
            $('#username_error').text('Please Enter Username');
            $('#username_error').show();
        }
        if ($('#password').val().trim() == "") {
            $('#password_error').text('Please Enter Password');
            $('#password_error').show();
        }
        if ($('#username').val().trim() == "") {
            $('#username').focus();
        } else if ($('#password').val().trim() == "") {
            $('#password').focus();
        } else {
            var username = $('#username').val().trim();
            var password = $('#password').val().trim();
            var remember = $('.remember').val().trim();
            $.ajax({
                url: '{{route("login_check")}}',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "username": username,
                    "password": password,
                    "remember": remember,
                },
                success: function (result) {
                    if (result == "fail") {
                        $('#error').text('Invalid Login');
                        $('#error_div').show();
                    } else if (result == "success") {
                        $('#success').text('Done');
                        $('#success_div').show();
                        window.location.href = '{{route("dashboard")}}';
                    } else {
                        $('#error').text('Invalid Login');
                        $('#error_div').show();
                    }
                }
            });
        }
    });
    $(document).on('keypress', '#password,#username', function (e) {
        if (e.which == 13) {
            $('.textfield_error').text('');
            $('.textfield_error').hide();
            $('#error').text('');
            $('#error_div').hide();
            $('#success').text('');
            $('#success_div').hide();
            $('.textfield_error').hide();
            if ($('#username').val().trim() == "") {
                $('#username_error').text('Please Enter Username');
                $('#username_error').show();
            }
            if ($('#password').val().trim() == "") {
                $('#password_error').text('Please Enter Password');
                $('#password_error').show();
            }
            if ($('#username').val().trim() == "") {
                $('#username').focus();
            } else if ($('#password').val().trim() == "") {
                $('#password').focus();
            } else {
                var username = $('#username').val().trim();
                var password = $('#password').val().trim();
                var remember = $('.remember').val().trim();
                $.ajax({
                    url: '{{route("login_check")}}',
                    type: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "username": username,
                        "password": password,
                        "remember": remember,
                    },
                    success: function (result) {
                        if (result == "fail") {
                            $('#error').text('Invalid Login');
                            $('#error_div').show();
                        } else if (result == "success") {
                            $('#success').text('Done');
                            $('#success_div').show();
                            window.location.href = '{{route("dashboard")}}';
                        } else {
                            $('#error').text('Invalid Login');
                            $('#error_div').show();
                        }
                    }
                });
            }
        }
    });
        </script>
<!-- end js include path -->
</body>
</html>