
@include ('admin.include.topcss')



    <!-- Datatables Js-->
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://shareurcodes.com/css/app-mdl.css"/>
     <link rel="stylesheet" type="text/css" href="https://shareurcodes.com/css/app-mdl.css"/>
     <link rel="stylesheet" type="text/css" href="https://shareurcodes.com/css/app.css">
    


<!-- search box container starts  -->
<div class="page-wrapper">
    @include ('admin.include.header')
    <div class="page-container">
        @include ('admin.include.navbar')
        <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Package Country List</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="">package Country List</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Package Country List</li>
                            </ol>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-box">
                                 <div class="card-head">
                                    <header>Package Country List</header>
                                
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                 <div class="card-body ">
                                     <div class="table-scrollable" id="tag_container">
                                            <div class="row">
                                                <div class="col-lg-12 ">
                                                    <table id="table" class="table table-hover table-checkable order-column full-width">
                                                      <thead>
                                                        <tr>
                                                          <th>#</th>
                                                          <th class="center"> Country/State </th>
                                                            <th class="center"> Package Type </th>
                                                            <th class="center"> Date </th>
                                                            <th class="center"> Action </th>
                                                        </tr>
                                                      </thead>
                                                      <tbody id="tablecontents">
                                                        @foreach($pckgcounttylist as $task)
                                                        <?php
                                                            if($task->packge_type=='1')
                                                            {
                                                                $pckgname="Domestic";
                                                            }
                                                            
                                                            else
                                                            {
                                                                $pckgname="International";
                                                            }
                                                            
                                                           
                                                            ?>
                                                        <tr class="row1" data-id="{{ $task->id }}">
                                                          <td>
                                                            <div style="color:rgb(124,77,255); padding-left: 10px; float: left; font-size: 20px; cursor: pointer;" title="change display order">
                                                            <i class="fa fa-ellipsis-v"></i>
                                                            <i class="fa fa-ellipsis-v"></i>
                                                            </div>
                                                          </td>
                                                          <td class="center">{{$task->statename}} </a></td>
                                                        <td class="center">{{$pckgname}}</td>
                                                        <td class="center">{{$task->create_date}}</td>
                                                        <td class="center"><a class="book-id-btn" href="{{url('admin/pckgcountrylistedit/'.$task->id)}}">Edit</a></td>
                                                        </tr>
                                                        @endforeach
                                                      </tbody>                  
                                                    </table>
                                                </div>
                                            </div> 
                                           
                                            
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div> 
</div>
 @include ('admin.include.footer')
</div>
     @include ('admin.include.downjs')


  <!-- jQuery UI -->
<!--   <script type="text/javascript" src="https://shareurcodes.com/js/app.js"></script> -->
  <script type="text/javascript" src="//code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
 
  <!-- Datatables Js-->
  <script type="text/javascript" src="//cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>

  <script type="text/javascript">
  $(function () {
    $("#table").DataTable();

    $( "#tablecontents" ).sortable({
      items: "tr",
      cursor: 'move',
      opacity: 0.6,
      update: function() {
          sendOrderToServer();
      }
    });

    function sendOrderToServer() {

      var order = [];
      $('tr.row1').each(function(index,element) {
        order.push({
          id: $(this).attr('data-id'),
          position: index+1
        });
      });

      $.ajax({
        type: "POST", 
        dataType: "json", 
        url: "{{ route('sortabledatatable') }}",
        data: {
          order:order,
          _token: '{{csrf_token()}}'
        },
        success: function(response) {
            if (response.status == "success") {
              console.log(response);
            } else {
              console.log(response);
            }
        }
      });

    }
  });

</script>
