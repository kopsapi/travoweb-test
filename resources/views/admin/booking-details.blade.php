@include ('admin.include.topcss')
    <style type="text/css">    .pagination > li > a, .pagination > li > span {
            position: relative;
            float: left;
            padding: 6px 12px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }

        .pagination > .active > span {
            font-weight: bold;
            color: #fff;
            background-color: #337ab7;
        }

        ul.pagination {
            float: right;
            margin-right: 20px;
        }</style>
    <style>    .section-start {
            padding: 30px 0;
        }

        .t-tran-detail {
            text-align: center;
            padding: 10px;
            background-color: #e26d78;
            color: white;
        }

        .c-items {
            border: 1px solid #c9c9c9;
            border-radius: 5px;
        }

        .onword-flight {
            text-align: left;
            color: white;
            padding: 10px;
            background-color: #21b1e7;
        }

        .t-note {
            padding: 13px;
            margin-top: -8px;
            font-size: 16px;
            border-left: 5px solid #21b1e7;
        }

        .flight-gif {
            width: 50px;
        }

        .pass-detail {
            text-align: center;
            padding: 10px;
            color: white;
            background-color: #e26d78;
        }

        .pass-contact {
            text-align: center;
            padding: 10px;
            background-color: #e26d78;
            color: white;
        }

        .t-total {
            font-size: 17px;
        }

        .t-badge {
            background-color: #ff6981;
            color: white;
            padding: 5px 10px;
        }

        .t-fare {
            text-align: center;
            padding: 10px;
            background-color: #fa9e1b;
            color: white;
        }

        .t-comm {
            text-align: center;
            padding: 10px;
            background-color: #fa9e1b;
            color: white;
        }

        .t-badge-comm {
            padding: 5px 10px;
            border-radius: 20px;
            font-size: 16px;
            background-color: #21b1e7;
        }

        .Commission tr td {
            text-align: center;
        }
        th{
        	font-weight: 500;
        }
    </style><!-- END HEAD -->
    <body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
    <div class="page-wrapper">        <!-- start header -->@include ('admin.include.header')        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
            <!-- start sidebar menu -->@include ('admin.include.navbar')            <!-- end sidebar menu -->
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">All Flight Booking</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;<i
                                            class="fa fa-angle-right"></i></li>
                                <li><a class="parent-item" href="">Flight Booking</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">All Flight Booking</li>
                            </ol>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header>All Flight Booking</header>
                                    <div class="tools"><a class="fa fa-repeat btn-color box-refresh"
                                                          href="javascript:;"></a> <a
                                                class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a> <a
                                                class="t-close btn-color fa fa-times" href="javascript:;"></a></div>
                                </div>
                                <div class="card-body ">
                                    <div class="row p-b-20">
                                        <div class="col-md-6 col-sm-6 col-6"></div>
                                    </div> <?php                                                  $chekall = unserialize($booklistdetails->flightdata);                                                                                                          ?>
                                    <section class="section-start">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class=" c-items"><h3 class="t-tran-detail">Transaction Detail</h3>
                                                        <div class="container">
                                                            <table class="table">
                                                                <tbody>
                                                                <tr>
                                                                    <th>Booking Id:</th>
                                                                    <td>{{$chekall['FlightItinerary']['BookingId']}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Ticket Status:</th>
                                                                    <td style="background-color: rgba(255,29,43,0.55);color:white;"> {{$chekall['FlightItinerary']['Segments'][0]['FlightStatus']}}</td>
                                                                </tr>
                                                                @if(!empty($orderdetails))
                                                                <tr>
                                                                    <th>Transaction Date And Time:</th>
                                                                    <td>{{$orderdetails->tx_time}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Payment Status:</th>
                                                                    <td>{{$orderdetails->tx_status}}</td>
                                                                </tr>
                                                                @endif
                                                                
                                                                <tr>
                                                                    <th>Onward IsRefundable:</th>
                                                                    <td>{{$booklistdetails->refund}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Fare Rule:</th>
                                                                    <td>
                                                                        <button class="btn btn-info">Fare Rule</button>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div> <?php                                                    for ($seg_i = 0; $seg_i < count($chekall['FlightItinerary']['Segments']); $seg_i++) {
                                                        $desticode = $chekall['FlightItinerary']['Segments'][$seg_i]['Destination']['Airport']['AirportCode'];
                                                        $destiairportname = $chekall['FlightItinerary']['Segments'][$seg_i]['Destination']['Airport']['AirportName'];
                                                        $destiairportcity = $chekall['FlightItinerary']['Segments'][$seg_i]['Destination']['Airport']['CityName'];
                                                        $destidate1 = explode('T', $chekall['FlightItinerary']['Segments'][$seg_i]['Destination']['ArrTime']);
                                                        $destitime = date("H:s", strtotime($destidate1[1]));
                                                        $destidate = date("d-M-y", strtotime($destidate1[0]));
                                                    }                                                    $origndate1 = explode('T', $chekall['FlightItinerary']['Segments'][0]['Origin']['DepTime']);                                                    $origntime = date("H:s", strtotime($origndate1[1]));                                                    $origndate = date("d-M-y", strtotime($origndate1[0]));                                                    ?>
                                                    <section class="section-start">
                                                        <div class="c-items"><h3 class="onword-flight">Onword Flight
                                                                Detail</h3>
                                                            <div class="t-note">{{$chekall['FlightItinerary']['Segments'][0]['Origin']['Airport']['AirportCode']}}
                                                                ({{$chekall['FlightItinerary']['Segments'][0]['Origin']['Airport']['AirportName']}}
                                                                ,{{$chekall['FlightItinerary']['Segments'][0]['Origin']['Airport']['CityName']}}
                                                                ), {{ $desticode}}({{$destiairportname}}
                                                                ,{{$destiairportcity}})
                                                            </div>
                                                            <table class="table">
                                                                <tbody>
                                                                <tr>
                                                                    <th><img class="flight-gif"
                                                                             src="{{ asset('assets/images/flight-gif.gif') }}">{{$chekall['FlightItinerary']['Segments'][0]['Airline']['AirlineName']}} {{$chekall['FlightItinerary']['Segments'][0]['Airline']['AirlineCode']}}
                                                                        -{{$chekall['FlightItinerary']['Segments'][0]['Airline']['FlightNumber']}}
                                                                    </th>
                                                                    <td>
                                                                        <table class="table table-borderless">
                                                                            <tr>
                                                                                <td>Departure
                                                                                    : {{$chekall['FlightItinerary']['Segments'][0]['Origin']['Airport']['AirportCode']}} </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Arrival : {{ $desticode}}</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td>
                                                                        <table class="table table-borderless">
                                                                            <tr>
                                                                                <td>{{$origntime}}, {{$origndate}} </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>{{$destitime}}, {{$destidate}}</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="mt-4 c-items"><h3 class="pass-detail">Passenger
                                                                Details</h3>
                                                            <div class="container">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Pax-Name</th>
                                                                        <th>Pax</th>
                                                                        <th>Ticket Id</th>
                                                                        <th>Ticket No</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>                                                                                <?php                                                                                   $passengers = unserialize($booklistdetails->passenger_detail);                                                                                    for($pass = 0;$pass < count($passengers);$pass++)                                                                            {                                                                                if ($passengers[$pass]['PaxType'] == '1') {
                                                                        $passcheck = 'Adults';
                                                                    } else if ($passengers[$pass]['PaxType'] == '2') {
                                                                        $passcheck = 'Child';
                                                                    } else if ($passengers[$pass]['PaxType'] == '3') {
                                                                        $passcheck = 'Infant';
                                                                    } else {
                                                                        $passcheck = '';
                                                                    }                                                                            ?>
                                                                    <tr>
                                                                        <td>{{$passengers[$pass]['Title']}}
                                                                            . {{ucfirst($passengers[$pass]['FirstName'])}} {{ucfirst($passengers[$pass]['LastName'])}}</td>
                                                                        <td>{{$passcheck}}</td>
                                                                         @if(!empty($passengers[$pass]['Ticket']['TicketId']))

                                                                            <td>{{$passengers[$pass]['Ticket']['TicketId']}}
                                                                            </td>

                                                                            <td>{{$passengers[$pass]['Ticket']['TicketNumber']}}
                                                                            </td>                                                                                        @else
                                                                            <td>{{$booklistdetails->pnrno}}</td>                                                                                        @endif
                                                                    </tr> 

                                                                <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="mt-4 c-items"><h3 class="pass-contact">Passenger
                                                                Contact</h3>
                                                            <div class="container">
                                                                <table class="table table-striped table-responsive">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <th>Contact Email</th>
                                                                        <th>Contact Phone</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>{{$booklistdetails->pname}}</td>
                                                                        <td>{{$booklistdetails->pemail}}</td>
                                                                        <td>{{$booklistdetails->pmobile}}</td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="mt-8 c-items">
                                                            <h3 class="pass-contact">Emergency Contact</h3>
                                                            <div class="container">
                                                                <table class="table table-striped table-responsive">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <th style="padding:20px 65px"></th>
                                                                            <th>Contact Phone</th>
                                                                            <th style="padding:20px 65px"></th>
                                                                            <th>Relation</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>{{$booklistdetails->eme_name}}</td>
                                                                            <td></td>
                                                                            <td>{{$booklistdetails->ema_phone}}</td>
                                                                            <td></td>
                                                                            <td>{{$booklistdetails->eme_relation}}</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div> <?php                                                    $flightfare = unserialize($booklistdetails->fare);                                                    if ($booklistdetails->mealprice != '' || $booklistdetails->mealprice == '0') {
                                                    $mealprice = $booklistdetails->mealprice;
                                                    $mealnetprice = $booklistdetails->meal_orgprice;
                                                } else {
                                                    $mealprice = 0;
                                                    $mealnetprice = 0;
                                                }                                                     if ($booklistdetails->bag_price != '' || $booklistdetails->bag_price == '0') {
                                                    $baggprice = $booklistdetails->bag_price;
                                                    $baggnetprice = $booklistdetails->bag_orgprice;
                                                } else {
                                                    $baggprice = 0;
                                                    $baggnetprice = 0;
                                                }                                                    $othercharges = $booklistdetails->flightothercharges;                                                     // round($flightfare['OtherCharges'] +  $flightfare['TdsOnPLB'] + $flightfare['TdsOnIncentive']+$flightfare['TdsOnCommission']);                                                    
                                                $basefare = $booklistdetails->flightmarginprice;								                    $taxfare = $booklistdetails->flighttax;								                    
                                                $totalamount =  $basefare +  $taxfare + $othercharges + $mealprice + $baggprice ;								                    // $gst =( $totalamount1 * 5 ) /100;                                                     // $totalamount = $totalamount1 + $gst;                                                       
                                                  $iconcur = $booklistdetails->currency_icon;								                    ?>
                                                <div class="col-sm-4">
                                                    <div class="c-items"><h3 class="t-fare">Fare Break-Up (Onword )</h3>
                                                        <div class="container">
                                                            <table class="table table-striped">
                                                                <tbody>
                                                                <tr>
                                                                    <th>Base Fare</th>
                                                                    <td>
                                                                        <i class="fa {{$iconcur}}"></i> {{number_format($basefare,2)}}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Tax</th>
                                                                    <td>
                                                                        <i class="fa {{$iconcur}}"></i> {{number_format($taxfare,2)}}
                                                                    </td>
                                                                </tr> @if($baggprice!='')
                                                                    <tr>
                                                                        <th>Baggage Charges</th>
                                                                        <td>
                                                                            <i class="fa {{$iconcur}}"></i> {{number_format($baggprice)}}
                                                                        </td>
                                                                    </tr> @endif @if($mealprice!='0')
                                                                    <tr>
                                                                        <th>Meal Fee</th>
                                                                        <td>
                                                                            <i class="fa {{$iconcur}}"></i> {{number_format($mealprice,2)}}
                                                                        </td>
                                                                    </tr> @endif
                                                                <tr>
                                                                    <th>Other Charges</th>
                                                                    <td>
                                                                        <i class="fa {{$iconcur}}"></i> {{number_format($othercharges,2)}}
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <hr class="w-100">
                                                            <div class="t-total text-right"><p>Total: <span
                                                                            style="font-size: 18px;width: 30px"
                                                                            class="t-badge">                                                                      <i
                                                                                class="fa {{$iconcur}}"></i> {{number_format($totalamount,2)}}</span>
                                                                </p></div>
                                                        </div>
                                                    </div>
                                                    <div class="c-items mt-5"><h3 class="t-comm">Commission(Onword )</h3>
                                                        <div class="container">
                                                            <table class="table table-striped Commission">
                                                                <tbody>
                                                                <tr>
                                                                    <th>Flight Margin Price</th>
                                                                    <td><span class="bagde badge-success t-badge-comm"><i
                                                                                    class="fa {{$iconcur}}"></i> {{$booklistdetails->flightmarginprice}} </span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Flight Main Price</th>
                                                                    <td><span class="bagde badge-success t-badge-comm"><i
                                                                                    class="fa {{$iconcur}}"></i> {{$booklistdetails->flightmainprice}} </span>
                                                                    </td>
                                                                </tr> @if($mealprice!='0')
                                                                    <tr>
                                                                        <th>Meal Margin Price</th>
                                                                        <td>
                                                                            <span class="bagde badge-success t-badge-comm"><i
                                                                                        class="fa {{$iconcur}}"></i> {{$mealprice}} </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Meal Price</th>
                                                                        <td>
                                                                            <span class="bagde badge-success t-badge-comm"><i
                                                                                        class="fa {{$iconcur}}"></i> {{$mealnetprice}} </span>
                                                                        </td>
                                                                    </tr> @endif @if($baggprice!='')
                                                                    <tr>
                                                                        <th>Baggage Margin Price</th>
                                                                        <td>
                                                                            <span class="bagde badge-success t-badge-comm"><i
                                                                                        class="fa {{$iconcur}}"></i> {{$baggprice}} </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Baggage Price</th>
                                                                        <td>
                                                                            <span class="bagde badge-success t-badge-comm"><i
                                                                                        class="fa {{$iconcur}}"></i> {{$baggnetprice}} </span>
                                                                        </td>
                                                                    </tr> @endif </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            <!-- end page content -->        </div>        <!-- end page container -->
        <!-- start footer -->@include ('admin.include.footer')        <!-- end footer -->
    </div> @include ('admin.include.downjs')</body></html>