@include ('admin.include.topcss')

<style type="text/css">

    .pagination>li>a,

    .pagination>li>span {

        position: relative;

        float: left;

        padding: 6px 12px;

        line-height: 1.42857143;

        color: #337ab7;

        text-decoration: none;

        background-color: #fff;

        border: 1px solid #ddd;

    }



    .pagination>.active>span {

        font-weight: bold;

        color: #fff;

        background-color: #337ab7;

    }



    ul.pagination {

        float: right;

        margin-right: 20px;

    }

</style>

<style>

    .section-start {

        padding: 30px 0;

    }



    .t-sms-detail {

        text-align: center;

        padding: 10px;

        background-color: #e26d78;

        color: white;

    }



    .c-items {

        border: 1px solid #c9c9c9;

        border-radius: 5px;

    }



    .onword-flight {

        text-align: left;

        color: white;

        padding: 10px;

        background-color: #21b1e7;

    }



    .t-note {

        padding: 13px;

        margin-top: -8px;

        font-size: 16px;

        border-left: 5px solid #21b1e7;

    }



    .flight-gif {

        width: 50px;

    }



    .pass-detail {

        text-align: center;

        padding: 10px;

        color: white;

        background-color: #e26d78;

    }



    .pass-contact {

        text-align: center;

        padding: 10px;

        background-color: #e26d78;

        color: white;

    }



    .t-total {

        font-size: 17px;

    }



    .t-badge {

        background-color: #ff6981;

        color: white;

        padding: 5px 10px;

    }



    .t-fare {

        text-align: center;

        padding: 10px;

        background-color: #fa9e1b;

        color: white;

    }



    .t-comm {

        text-align: center;

        padding: 10px;

        background-color: #fa9e1b;

        color: white;

    }



    .t-badge-comm {

        padding: 5px 10px;

        border-radius: 20px;

        font-size: 13px;

        background-color: #21b1e7;

    }



    .Commission tr td {

        text-align: center;

    }

    .modal-header.h-bg {

        background: #31124b;

        padding: 10px 21px;

        border-bottom-color: white;

    }

    .modal-footer.f-bg {

        background: #e2e3ea;

    }

    p.t-id-info {

        margin-top: 10px;

        color: red !important;

        font-weight: 600;

    }



    .btn1 {

        font-size: 17px;

        font-weight: 500;

        color: #fff;

        text-transform: uppercase;

        background: #fa9e1b;

        border: none;

        outline: none;

        padding: 8px 17px;

        border-radius: 5px;

        cursor: pointer;

    }

</style>
 <style>
                                                        button.fh-save {
                                                            border: none;
                                                            border: 1px solid orange;
                                                            background: orange;
                                                            padding: 8px 23px;
                                                            color: white;
                                                            border-radius: 7px;
                                                            font-size: 18px;
                                                            display: flex;
                                                            font-weight: 500;
                                                            margin-left: auto;
                                                        }
                                                        button.fh-save:hover {
                                                            border: 1px solid orange;
                                                            background: transparent;
                                                            padding: 8px 23px;
                                                            color: orange;

                                                        }
                                                        .con {
                                                            display: inline;
                                                            position: relative;
                                                            padding-left: 27px;
                                                            padding-right: 37px;
                                                            width: 100%;
                                                            margin-bottom: 12px;
                                                            cursor: pointer;
                                                            font-size: 17px;
                                                            -webkit-user-select: none;
                                                            -moz-user-select: none;
                                                            -ms-user-select: none;
                                                            user-select: none;
                                                        }
                                                        label.con {
                                                            font-weight: 400;
                                                        }

                                                        /* Hide the browser's default checkbox */
                                                        .con input {
                                                            position: absolute;
                                                            opacity: 0;
                                                            cursor: pointer;
                                                            height: 0;
                                                            width: 0;
                                                        }

                                                        /* Create a custom checkbox */
                                                        .checkmark {
                                                            position: absolute;
                                                            top: 2px;
                                                            left: 0;
                                                            height: 20px;
                                                            width: 20px;
                                                            background-color: #e0e0e0;
                                                            border: 1px solid #b7b7b7;
                                                            border-radius: 3px;
                                                        }

                                                        /* On mouse-over, add a grey background color */
                                                        .con:hover input ~ .checkmark {
                                                            background-color: #ccc;
                                                        }

                                                        /* When the checkbox is checked, add a blue background */
                                                        .con input:checked ~ .checkmark {
                                                            background-color: #f67d27;
                                                            border: 1px solid #f67d27;
                                                        }

                                                        /* Create the checkmark/indicator (hidden when not checked) */
                                                        .checkmark:after {
                                                            content: "";
                                                            position: absolute;
                                                            display: none;
                                                        }

                                                        /* Show the checkmark when checked */
                                                        .con input:checked ~ .checkmark:after {
                                                            display: block;
                                                        }

                                                        /* Style the checkmark/indicator */
                                                        .con .checkmark:after {
                                                            left: 6px;
                                                            top: 2.4px;
                                                            width: 6px;
                                                            height: 11px;
                                                            border: solid white;
                                                            border-width: 0 3px 3px 0;
                                                            -webkit-transform: rotate(45deg);
                                                            -ms-transform: rotate(45deg);
                                                            transform: rotate(45deg);
                                                        }
                                                        .nav-tabs.fh-switch {
                                                            border-bottom: 1px solid  #bebebe;
                                                        }
                                                        .nav-item.show .nav-link, .nav-tabs .nav-link.active {
                                                            color: #e26d78;
                                                              background-color: #fff;
                                                              border: none;
                                                              border-bottom: 3px solid #e26d78;
                                                          }
                                                        .nav-item.show .nav-link, .nav-tabs .nav-link.active:hover {

                                                            color: #e26d78 !important;
                                                            background-color: #fff;
                                                            border: none;
                                                            border-bottom: 3px solid #e26d78;
                                                        }
                                                        .nav-tabs.fh-switch>li a{
                                                            color: #333;
                                                         }

                                                        .nav-tabs.fh-switch>li a:hover{
                                                            color: #e26d78 !important;
                                                        }
                                                        .nav-tabs>li a.active, .nav-tabs>li a.active:focus{
                                                            color: #e26d78;
                                                            cursor: default;
                                                            background-color: #fff;
                                                            border: none;
                                                            border-bottom: 3px solid #e26d78;
                                                        }
                                                        .nav-tabs .nav-link:hover {
                                                            border:none;
                                                        }
                                                        .form-group.f1 {
                                                            position: relative;
                                                            margin-bottom: 25px;
                                                            overflow: hidden;
                                                        }
                                                        label.e-lable {
                                                            position: absolute;
                                                            left: 5px;
                                                            top: 55%;
                                                            font-size: 20px;
                                                            transform: translateY(-50%) !important;
                                                            color: #e26d78;
                                                        }
                                                        label.e-lable1 {
                                                            position: absolute;
                                                            left: 5px;
                                                            top: 55%;
                                                            font-size: 20px;
                                                            transform: translateY(-50%) !important;
                                                            color:#21b1e7;
                                                        }
                                                        input.e-input {
                                                            width: 100%;
                                                            display: block;
                                                            border: none;
                                                            border-bottom: 1px solid #999;
                                                            padding: 6px 40px;
                                                            box-sizing: border-box;
                                                        }
                                                        input.e-input:focus {
                                                           outline: none;
                                                            box-shadow: none;
                                                        }
                                                    </style>
<!-- END HEAD -->



<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">

<div class="page-wrapper">

    <!-- start header -->@include ('admin.include.header')

<!-- end header -->

    <!-- start page container -->

    <div class="page-container">

        <!-- start sidebar menu -->@include ('admin.include.navbar')

    <!-- end sidebar menu -->

        <!-- start page content -->

        <div class="page-content-wrapper">

            <div class="page-content">

                <div class="page-bar">

                    <div class="page-title-breadcrumb">

                        <div class=" pull-left">

                            <div class="page-title"> Country Packages </div>

                        </div>

                        <ol class="breadcrumb page-breadcrumb pull-right">

                            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;<i class="fa fa-angle-right"></i> </li>

                            <li><a class="parent-item" href="">Country Packages </a>&nbsp;<i class="fa fa-angle-right"></i> </li>

                            <li class="active">Country Packages </li>

                        </ol>

                    </div>

                </div>

                <div class="row">

                    <div class="col-md-12">

                        <div class="card card-box">

                            <div class="card-head">

                                <header><meta http-equiv="Content-Type" content="text/html; charset=utf-8">Country Packages  </header>

                                <div class="tools">

                                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>

                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>

                                </div>

                            </div>

                            <div class="card-body ">

                                <div class="row p-b-20">

                                    <div class="col-md-6 col-sm-6 col-6">

                                    </div>

                                </div>

                               
                                @if($data==0)
                                <section class="section-start">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class=" c-items">
                                                     <div class="container" style="padding: 30px 60px;">
                                                       <span style="color:green;display: none" id="success"></span>
                                                        <span style="color:red;display: none" id="error"></span>
                                                            <div class="form-group f1">
                                                                <label style="margin-left: 5px;">Country/State Name</label>
                                                               
                                                                <input type="text" name="country_name" id="country_name" placeholder="" class="e-input " value="">
                                                                 <span style="color:red;visibility: hidden" id="country_name_err" ></span>
                                                            </div>
                                                            <div class="row" style="display: none">
                                                                
                                                                    <div class="col-sm-3">
                                                                        <input name="country" type="radio" class="checkradio" id="domestic" value="1" />
                                                                         <label for="domestic">Domestic</label>
                                                                     </div>
                                                                     <div class="col-sm-3">
                                                                        <input name="country" type="radio" id="international" value="2" class="checkradio" checked />
                                                                         <label for="international">International</label>
                                                                     </div>
                                                                     <span style="color:red;visibility: hidden" id="country_type_err" ></span>
                                                                     <div class="col-sm-6"></div>
                                                                
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6"></div>
                                                                <div class="col-sm-6">
                                                                    <button class="fh-save" id="adv_submit">Submit</button>
                                                                </div>
                                                            </div>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>

                                    </div>
                                </section>
                                @else
                                <section class="section-start">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class=" c-items">
                                                     <div class="container" style="padding: 30px 60px;">
                                                       <span style="color:green;display: none" id="success"></span>
                                                        <span style="color:red;display: none" id="error"></span>
                                                            <div class="form-group f1">
                                                                <label style="margin-left: 5px;">Country/State Name</label>
                                                               <input type="hidden" id="pckgid" name="pckgid" value="{{$pckgcountrydetail->id}}">
                                                                <input type="text" name="country_name1" id="country_name1" placeholder="" class="e-input " value="{{$pckgcountrydetail->statename}}">
                                                                 <span style="color:red;visibility: hidden" id="country_name_err1" ></span>
                                                            </div>
                                                            <div class="row" style="display: none">
                                                                   
                                                                    <div class="col-sm-3">
                                                                        <input name="country1" type="radio" class="checkradio1" id="domestic1" value="1" <?php if($pckgcountrydetail->packge_type=='1'){ echo "checked";} ?>>
                                                                         <label for="domestic1">Domestic</label>
                                                                     </div>
                                                                     <div class="col-sm-3">
                                                                        <input name="country1" type="radio" id="international1" value="2" class="checkradio1" <?php if($pckgcountrydetail->packge_type=='2'){ echo "checked";} ?> />
                                                                         <label for="international">International</label>
                                                                     </div>
                                                                     <span style="color:red;visibility: hidden" id="country_type_err" ></span>
                                                                     <div class="col-sm-6"></div>
                                                                
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6"></div>
                                                                <div class="col-sm-6">
                                                                    <button class="fh-save" id="adv_update">Update</button>
                                                                </div>
                                                            </div>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>

                                    </div>
                                </section>
                                @endif
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

      

        <!-- end page content -->

    </div>

    <!-- end page container -->

    <!-- start footer -->@include ('admin.include.footer')

<!-- end footer -->

</div> @include ('admin.include.downjs')</body>



<script>
    $(document).on('click','#adv_submit',function(){
        var country_name=$('#country_name').val();
       var countryvalue = $("input[name='country']:checked").val();

       if(countryvalue)
       {
        $('#country_type_err').css('visibility', 'hidden');
        var country_type=$("input[name='country']:checked").val();
       }
       else
       {
        $('#country_type_err').text('Please Select Packages Type');
         $('#country_type_err').css('visibility', 'visible');
         var country_type="";
       }
        if(country_name=="")
        {
            $('#country_name_err').text('Please Select banner Image');
            $('#country_name_err').css('visibility', 'visible');
        }
        else if(country_type=="")
        {
            $('#country_type_err').text('Please Select Packages Type');
            $('#country_type_err').css('visibility', 'visible');
        }
        
        else
        {
           
            $.ajax({
                url : "{{route('insert_package_country')}}",
                data :{
                        'country_name':country_name,
                        'country_type':country_type,
                },
                type:'Get',
                success:function(result)
                {
                  if(result=="success")
                  {

                    swal({
                                   title: "Successfully Add State/Country",
                                   text: "",
                                   type: "success",
                                   showCancelButton: false,
                                   confirmButtonColor: "#DD6B55",
                                   confirmButtonText: "Ok",
                                   cancelButtonText:false,
                                   closeOnConfirm: false,
                                   closeOnCancel: false
                               }, function(isConfirm) {
                                   if (isConfirm) {
                                    location.reload();
                                 } 
                           });
                    // $('#success').text('Successfully Insert');
                    // $('#success').show();
                    // $('#country_name').val("");
                    
                    // $(".checkradio").prop("checked", false);
                    // location.reload();
                  }
                  else
                  {
                        $('#error').text('Unable to update');
                        $('#error').show();
                  }
                    
                },
            });
        }
    })
</script>
<!-- update logic -->

<script>
    $(document).on('click','#adv_update',function(){
        var country_name=$('#country_name1').val();
       var countryvalue = $("input[name='country1']:checked").val();
       var pckgid=$('#pckgid').val();

       if(countryvalue)
       {
        $('#country_type_err1').css('visibility', 'hidden');
        var country_type=$("input[name='country1']:checked").val();
       }
       else
       {
        $('#country_type_err1').text('Please Select Packages Type');
         $('#country_type_err1').css('visibility', 'visible');
         var country_type="";
       }
        if(country_name=="")
        {
            $('#country_name_err1').text('Please Select banner Image');
            $('#country_name_err1').css('visibility', 'visible');
        }
        else if(country_type=="")
        {
            $('#country_type_err1').text('Please Select Packages Type');
            $('#country_type_err1').css('visibility', 'visible');
        }
        
        else
        {
           
            $.ajax({
                url : "{{route('update_package_country')}}",
                data :{
                        'country_name':country_name,
                        'country_type':country_type,
                        'pckgid':pckgid,
                },
                type:'Get',
                success:function(result)
                {
                  if(result=="success")
                  {
                    swal({
                                   title: "Successfully Update State/Country",
                                   text: "",
                                   type: "success",
                                   showCancelButton: false,
                                   confirmButtonColor: "#DD6B55",
                                   confirmButtonText: "Ok",
                                   cancelButtonText:false,
                                   closeOnConfirm: false,
                                   closeOnCancel: false
                               }, function(isConfirm) {
                                   if (isConfirm) {
                                     window.location="{{route('package-country-list')}}";
                                 } 
                           });
                    // $('#success').text('Successfully Updated');
                    // $('#success').show();
                    // $('#country_name').val("");

                    // $(".checkradio").prop("checked", false);
                    //  window.location="{{route('package-country-list')}}";
                  }
                  else
                  {
                        $('#error').text('Unable to update');
                        $('#error').show();
                  }
                    
                },
            });
        }
    })
</script>
</script>

</html>