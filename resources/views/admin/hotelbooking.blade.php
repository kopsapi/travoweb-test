@include ('admin.include.topcss')

<style type="text/css">

    .pagination>li>a, .pagination>li>span 

    {

        position: relative;

        float: left;

        padding: 6px 12px;

        line-height: 1.42857143;

        color: #337ab7;

        text-decoration: none;

        background-color: #fff;

        border: 1px solid #ddd;

    }

    .pagination>.active>span

    {

        font-weight:bold;

        color:#fff;

        background-color: #337ab7;

    }

    ul.pagination 

    {

        float: right;

        margin-right: 20px;

    }
     .c-btn1 {
        padding: 10px 15px;
        border: none;
        color: white;
        background: #fa9e1b;
        width: 100%;
        border-radius: 5px;
    }

.book-id-btn {
        color: #21b1e7;
        background-color: white;
        border: 1px solid #21b1e7;
        border-radius: 5px !important;
        padding: 5px 10px !important;

    }
    .modal-header.h-bg {
        background: #31124b;
        padding: 10px 21px;
        border-bottom-color: white;
    }
    .modal-footer.f-bg {
        background: #e2e3ea;
    }
    p.t-id-info {
        margin-top: 10px;
        color: red !important;
        font-weight: 600;
    }

    .btn1 {
        font-size: 17px;
        font-weight: 500;
        color: #fff;
        text-transform: uppercase;
        background: #fa9e1b;
        border: none;
        outline: none;
        padding: 8px 17px;
        border-radius: 5px;
        cursor: pointer;
    }
</style>

<!-- END HEAD -->

<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">

    <div class="page-wrapper">

        <!-- start header -->

        @include ('admin.include.header')

        <!-- end header -->

        <!-- start page container -->

        <div class="page-container">

            <!-- start sidebar menu -->

            @include ('admin.include.navbar')

            <!-- end sidebar menu -->

            <!-- start page content -->

            <div class="page-content-wrapper">

                <div class="page-content">

                    <div class="page-bar">

                        <div class="page-title-breadcrumb">

                            <div class=" pull-left">

                                <div class="page-title">All Hotel Booking</div>

                            </div>

                            <ol class="breadcrumb page-breadcrumb pull-right">

                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;<i class="fa fa-angle-right"></i>

                                </li>

                                <li><a class="parent-item" href="">Hotel Booking</a>&nbsp;<i class="fa fa-angle-right"></i>

                                </li>

                                <li class="active">All Hotel Booking</li>

                            </ol>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">

                            <div class="card card-box">

                                <div class="card-head">

                                    <header>All Hotel Booking </header>



                                    <div class="tools">

                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>

                                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

                                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>

                                    </div>

                                </div>

                                <div class="card-body ">

                                    <div class="row p-b-20">

                                        <div class="col-md-6 col-sm-6 col-6">
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-6">

                                            <div class="btn-group pull-right">

                                            	<div class="card-tools">

                                                   <div class="input-group input-group-sm" style="width: 150px;">

                                                      <input type="text" name="search" id="search" class="form-control float-right" placeholder="Search" value="">

                                                      <div class="input-group-append">

                                                         <button type="button" id="search-btn" class="btn btn-default"><i class="fa fa-search"></i></button>

                                                     </div>

                                                 </div>

                                             </div>

                                               

                                            </div>

                                        </div>

                                    </div>

                                    <div class="table-scrollable" id="tag_container">

                                        <table class="table table-hover table-checkable order-column full-width" id="example4">

                                            <thead>

                                                <tr>

                                                 <th class="center">Id</th>

                                                 <th class="center"> Booking Id </th>

                                                 <th class="center"> Hotel Name </th>
                                                 <th class="center"> Destination </th>
                                                <th class="center">Booking Date </th>
                                                 <th class="center"> Check in </th>

                                                 <th class="center"> Check out </th>

                                                 <th class="center"> Lead Name </th>

                                                 <th class="center"> Mobile </th>

                                                 <th class="center"> Email </th>

                                                 <th class="center"> Rooms </th>

                                                 <th class="center"> Adults </th>

                                                 <th class="center"> Child </th>

                                                 
                                                 <th class="center">Booking Time </th>
                                                <th class="center">Cancel </th>


                                                 <!-- <th class="center"> Action </th> -->

                                             </tr>

                                         </thead>

                                         <tbody>
                                        <?php
                                        $ht=1;
                                        ?>
                                          @foreach($hotelbookinglist as $hotels)

                                          <tr class="odd gradeX">

                                            <td class="center">{{$ht++}}</td>
                                            <td class="center " style="margin-top: 13px;" id="{{$hotels->id}}"><a class="book-id-btn" href="{{url('admin/hotel-booking-details/'.$hotels->id)}}">{{$hotels->bookingid}}</a></td>
                                          

                                            <td class="center">{{$hotels->hotel_name}}</td>

                                            <td class="center">{{$hotels->hotel_city}}</td>
                                            <td class="center"><?php 
                                               
                                                echo date('d-M-Y',strtotime($hotels->create_date)); 
                                                ?></td>
                                            <td class="center"><?php echo date('d-M-Y',strtotime($hotels->check_in_date)); ?></td>

                                            <td class="center"><?php echo date('d-M-Y',strtotime($hotels->check_out_date)); ?></td>

                                            <td class="center">{{$hotels->lead_name}}</td>

                                            <td class="center"><a href="tel:{{$hotels->lead_mobile}}">

                                              {{$hotels->lead_mobile}} </a></td>

                                              <td class="center"><a href="mailto:{{$hotels->lead_email}} ">

                                                {{$hotels->lead_email}} </a></td>



                                                <td class="center">{{$hotels->no_of_rooms}}</td>

                                                <td class="center">{{$hotels->total_adult_count}}</td>

                                                <td class="center">{{$hotels->total_child_count}}</td>
                                                
                                                <td class="center">
                                                    <?php 
                                                $bookingdate=explode('T',$hotels->booking_date);
                                                echo date('h:i a',strtotime($hotels->create_time)); 
                                                ?>
                                                    
                                                </td>
                                                <?php $canbok= $hotels->bookingid."%".$hotels->lead_email ?>
                                                <td class="center " style="margin-top: 13px;"><a href="#" class="book-id-btn htcancel cancelbtn-{{$hotels->bookingid}}" id="{{$canbok}}" >CANCEL</a>

                                                </td>
                              


										    	</tr>
                                            


                                          @endforeach





                                      </tbody>

                                  </table>

                                {!! $hotelbookinglist->render() !!}

                              </div>

                          </div>

                      </div>

                  </div>

              </div>

          </div>

      </div>

 <div class="modal fade show" id="darkModalForm" style="    background-color: rgba(0,0,0,-1.5);">
      <div class="modal-dialog form-dark1">
          <!--Content-->
          <div class="modal-content card card-image" style="background-repeat:no-repeat;background-size: cover; background-position:left top; background-image: url('{{asset('assets/images/modal-img.jpg')}}');">
              <div class="text-white rgba-stylish-strong p-modal">

                  <div class="modal-header text-center pb-4" style="border-bottom: 1px solid #fa9e1b!important;">
                      <h3 class="modal-title w-100 white-text font-weight-bold" id="myModalLabel"><strong>CANCELLATION</strong></h3>

                      <span class="close2 white-text" data-dismiss="modal"style="color: #fa9e1b!important;">&times;</span>

                  </div>
                    <div class="alert alert-success mainsuccess" style="display: none"></div>
                    <div class="alert alert-danger mainerror" style="display: none"></div>
                  <div class="modal-body">

                      <form class="s-input" id="formcancel">
                          <div class="md-form mb-3">
                              <label>Booking Id</label>
                              <input type="text"  id="Form-email5" class="form-control validate white-text mbookingid" value="" name="bookingid" readonly="" style="background-color:transparent; ">
                              <input type="hidden" name="email" class="useremail" id="useremail">
                              <span class="errorbooking" style="color:red;font-size: 12px"></span>
                          </div>

                          <div class="md-form mb-3">
                              <label>Request Type</label>
                              <select class="form-control requesttype">
                                  <option value="4">Hotel Cancel</option>

                              </select>
                              <span class="errorrequest" style="color:red;font-size: 12px"></span>
                          </div>



                          <div class="md-form mb-3">
                              <label>Remark</label>
                              <textarea class="form-control mremark" id="mremark" ></textarea>
                              <span class="errorremark" style="color:red;font-size: 12px"></span>
                          </div>

                          <div class="row d-flex align-items-center ">
                              <div class="text-center  col-md-12">
                                  <button type="button" id="" class="c-btn1 hotelcancle">Submit</button>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
      <!-- end page content -->

  </div>
           <div class="modal fade text-center py-5" style="top:30px" id="myModal_charges">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content" style="border-radius: 7px;">
                            <div class="modal-header h-bg">
                                <span class="close close-btn" data-dismiss="modal">&times;</span>

                            </div>
                            <div class="modal-body">

                                <h3 class="pt-3 mb-0 h-cancel success_modal"> </h3>
                                <!-- <p class="t-id-info">Your Change Request Id:1234</p> -->
                                <button class="btn1 text-white mb-5 success_ok" style="margin-top:20px; margin-bottom: 0px !important">Submit</button>
                              <!--   <a role="button" class="btn1 text-white mb-5 success_ok" href="https://www.sunlimetech.com" target="_blank">Submit</a> -->
                            </div>
                            <div class="modal-footer f-bg mt-3">
                                <span class="d-block mr-auto f-help "><i class="fa fa-phone"></i> Helpline:<a href="#"> 34356894454</a></span>
                                <span class="f-help"><img src="{{asset('assets/images/logo.png')}}" class="f-logo"></span>
                            </div>
                        </div>
                    </div>
                </div>
<!-- Modal -->

  <!-- end page container -->

  <!-- start footer -->

  @include ('admin.include.footer')

  <!-- end footer -->

</div>

@include ('admin.include.downjs')

<script>

    $(window).on('hashchange', function() {

        if (window.location.hash) {

            var page = window.location.hash.replace('#', '');

            if (page == Number.NaN || page <= 0) {

                return false;

            }else{

                getData(page);

            }

        }

    });

    

    $(document).ready(function()

    {

        $(document).on('click', '.pagination a',function(event)

        {

            event.preventDefault();



            $('li').removeClass('active');

            $(this).parent('li').addClass('active');



            var myurl = $(this).attr('href');

            var page=$(this).attr('href').split('page=')[1];



            getData(page);

        });



    });



    function getData(page){

        $.ajax(

        {

            url: '?page=' + page,

            type: "get",

            datatype: "html"

        }).done(function(data){

        	var data1=jQuery(data).find('#tag_container').html();

            $("#tag_container").empty().html(data1);

            location.hash = page;

        }).fail(function(jqXHR, ajaxOptions, thrownError){

          alert('No response from server');

      });

    }

    $(document).on('keypress','#search',function(e)

    {

        if(e.which==13)

        {

            var search=$('#search').val();

            

            $.ajax(

            {

                url: '{{route("hotelbooklist")}}',

                data: {

                    'search':search,

                    '_token':'{{csrf_token()}}'

                },

                type: 'POST',

                success: function (data) 

                {

                	var data1=jQuery(data).find('#tag_container').html();

                    $("#tag_container").empty().html(data1);

                }

            });

            return false;

        }

    });

    $(document).on('click','#search-btn',function(e)

    {

        var search=$('#search').val();

        alert(search);

        $.ajax(

        {

            url: '{{route("hotelbooklist")}}',

            data: {

                'search':search,

                '_token':'{{csrf_token()}}'

            },

            type: 'POST',

            success: function (data) 

            {

                var data1=jQuery(data).find('#tag_container').html();

                $("#tag_container").empty().html(data1);

            }

        });

    });

</script>
<script>
  $(document).on('click','.htcancel',function(){
    var bookid1=this.id;
    var chk=bookid1.split('%');
    var bookid=chk[0];
    var useremail=chk[1];
    alert(useremail);
    $('.mbookingid').val(bookid);
    $('.useremail').val(useremail);
    $('#darkModalForm').modal('show');

  })
</script>
<script>
    $(document).on('click','.hotelcancle',function(){
        alert('submit')
        var requesttype= $('.requesttype').val();
        var remark= $('.mremark').val();
        var bookingid=$('.mbookingid').val();
        var useremail=$('.useremail').val();
        
        if(bookingid=='')
        {
            $('.errorbooking').text("Please Enter Booking Id");
        }
        if(remark=='')
        {
           $('.errorremark').text("Please Enter Remark");
        }
        else
        {
            $.ajax({
                url :'{{route("hotelcancel")}}',
                data : { 'requesttype' :requesttype,
                        'remark' :remark,
                        'bookingid' :bookingid,
                        'useremail':useremail,

                      },
                type : 'get',
                success : function(response)
                {
                  alert(response);
                    if(response=='fail')
                    {
                        $('.mainerror').text("Please Enter Booking Id");
                        $('.mainerror').show();

                    }
                    else
                    {
                        $('.mainerror').hide();
                        var chkst = response.split('%%');
                        var errmsg = chkst[1];
                        var chgrequest=chkst[2];
                        if(errmsg !='')
                        {
                            $('.mainerror').text(errmsg);
                            $('.mainerror').show();
                        }
                        else
                        {
                            $('.mainerror').hide();
                            $('.mainsuccess').text("Successfully Hotel Cancel. Your Change Request Id "+ chgrequest);
                            $('.mainsuccess').show();
                            $('#darkModalForm').modal('hide');
                            $('#myModal_charges').modal('show');
                            $('.success_modal').text("Successfully Hotel Cancelled. Your Change Request Id "+ chgrequest);
                           
                        }
                        
                    }
                }
            })
        }

    })

</script>
<script type="text/javascript">
$(document).on('click', ".success_ok", function ()
{
    $('#myModal_charges').modal('hide');
    location.reload();
});
</script>
<script>
    $(document).on('click','.modalopen',function(){
        $('#myModal_charges').modal('show');
    })
</script>

</body>

</html>