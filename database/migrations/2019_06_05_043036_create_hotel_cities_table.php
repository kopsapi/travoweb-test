<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_hotel_city', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cityid')->nullable();
            $table->string('Destination')->nullable();
            $table->string('stateprovince')->nullable();
            $table->string('StateProvinceCode')->nullable();
            $table->string('country')->nullable();
            $table->string('countrycode')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_hotel_city');
    }
}
