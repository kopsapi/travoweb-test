<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCityCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_city_code', function (Blueprint $table) {
            $table->increments('id');
            $table->string('city_code')->nullable();
            $table->string('city_name')->nullable();
            $table->string('country_code')->nullable();
            $table->string('chk_airport')->nullable();
            
            $table->integer('status')->default('1');
            $table->timestamps()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_city_code', function (Blueprint $table) {
             Schema::dropIfExists('tbl_city_code');
        });
    }
}
