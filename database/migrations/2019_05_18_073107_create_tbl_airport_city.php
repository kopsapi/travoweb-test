<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAirportCity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_airport_city', function (Blueprint $table) {
            $table->increments('id');
            $table->string('airport_name')->nullable();
            $table->string('airport_code')->nullable();
            $table->string('city_name')->nullable();
            $table->string('city_code')->nullable();
            $table->string('country_name')->nullable();
            $table->string('country_code')->nullable();
            $table->string('nationality')->nullable();
            $table->string('currency')->nullable();
            $table->integer('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_airport_city', function (Blueprint $table) {
             Schema::dropIfExists('tbl_airport_city');
        });
    }
}
