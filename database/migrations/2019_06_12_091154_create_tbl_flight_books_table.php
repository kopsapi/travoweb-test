<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblFlightBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_flight_books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userid')->nullable();
            $table->string('pnrno')->nullable();
            $table->string('bookingId')->nullable();
            $table->string('domestic')->nullable();
            $table->string('orign')->nullable();
            $table->string('destination')->nullable();
            $table->string('lcc')->nullable();
            $table->string('fareType')->nullable();
            $table->text('fare')->nullable();
            $table->text('segment')->nullable();
            $table->text('passenger_detail')->nullable();
            $table->string('farebasiccode')->nullable();
            $table->string('farefamilycode')->nullable();
            $table->string('status')->nullable();
            $table->string('booking_date')->nullable();
            $table->string('booking_time')->nullable();
            $table->timestamp('servertime');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_flight_books');
    }
}
